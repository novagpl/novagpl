"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


@get_logged_in
def REPORT_NAME(request):
    if request.method == "GET":
        form = REPORT_FORM()

        return render(request, "report/REPORT_NAME.html", {
            "title": "REPORT_NAME Report",
            "form": form,
        })

    else:
        form = REPORT_FORM(request.POST)


        #set_autocomplete(None, form, "client", Client, client_id)

        query = Q()

        results = MODEL.objects.filter(query)

       
        csv_results = []
        for item in results:
            csv_results.append({
                "FIELD": "VALUE",
            })
        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(
            csv_results, cls=DjangoJSONEncoder)


        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/REPORT_NAME.html", {
                "title": "REPORT_NAME Report",
                "form": form,
                "results": results,
                "report_id": report_id,
                "pdf_report_id": pdf_report_id,
            })


        return render(request, "report/REPORT_NAME.html", {
            "title": "REPORT_NAME Report",
            "form": form,
            "results": results,
            "report_id": report_id,
            "pdf_report_id": pdf_report_id,
        })