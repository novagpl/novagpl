"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import re
import timeit
import calendar
import csv
import datetime as dt
import inspect
import json
import math
import zipfile
from difflib import SequenceMatcher
from decimal import Decimal, getcontext
getcontext().prec = 28


import accounting.accounting_choices as accounting_choices
import accounting.models
import client.client_choices as client_choices
import client.models
import config.models
import report.models
import report_list




import django
from annoying.functions import get_object_or_None
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.sessions.models import Session
from django.core import serializers
from django.core.exceptions import PermissionDenied
from django.core.serializers.json import DjangoJSONEncoder
from django.core.signing import BadSignature, TimestampSigner
from django.db import models
from django.apps import apps
from django.core.mail import send_mail
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.utils import timezone
from permissions import check_perm












def new_or_closing_episode_notification(emrepisode, new_or_closing):
    # This is typically called by the EmrEpisode.save()
    # this function handles sending out emails and notifications to any users
    # who are subscribed to episodes opening or closing in their UserConfig
    # new_or_closing is required to send the correct notifications
    # options for new_or_closing are (you guessed it) "new" and "closing"
    if new_or_closing not in ["new", "closing"]:
        raise Exception(f"'{new_or_closing}' must be either 'new' or 'closing'")

    # Building some strings to be used in the notification
    nova_url = get_app_config("url")
    userconfig_url = f"{nova_url}/config/user_config/"
    # The notification uses a different url because users will be
    # clicking the link from inside Nova so we dont need the domain
    # like we do for an email
    episode_url = f"{nova_url}/client/emr_episodes/{emrepisode.client.id}/{emrepisode.id}/"

    # Gathers users who have opted into these notifications and notify
    # them. Don't need to send notification to the user who saved the episode
    # Most users will only be subscribed to receive notifications and
    # emails for a single episode type. Here a couple strings get
    # dynamically created and they are used for the filters
    # Notify field has to transition to the userconfig model but the
    # email field is already using userconfig as the base for the query.


    notify_verb = ""
    if new_or_closing == "new":
        notify_verb = "created"

        notify_field = f"userconfig__notify_on_new_{emrepisode.episode_type}_episode"
        email_field = f"email_on_new_{emrepisode.episode_type}_episode"

        notify_users = apps.get_model("auth", "User").objects.filter(
            Q(userconfig__notify_on_new_episode=True) | Q(**{notify_field: True}),
        ).exclude(userconfig=emrepisode.opened_by.userconfig).exclude(userconfig__active=False)

        email_users = apps.get_model("config", "UserConfig").objects.filter(
            Q(email_on_new_episode=True) | Q(**{email_field: True}),
        ).exclude(active=False).exclude(id=emrepisode.opened_by.userconfig.id)

    else:
        notify_verb = "closed"

        notify_field = f"userconfig__notify_on_closing_{emrepisode.episode_type}_episode"
        email_field = f"email_on_closing_{emrepisode.episode_type}_episode"

        notify_users = apps.get_model("auth", "User").objects.filter(
            Q(userconfig__notify_on_closing_episode=True) | Q(**{notify_field: True}),
        ).exclude(userconfig=emrepisode.closed_by.userconfig).exclude(userconfig__active=False)

        email_users = apps.get_model("config", "UserConfig").objects.filter(
            Q(email_on_closing_episode=True) | Q(**{email_field: True}),
        ).exclude(active=False).exclude(id=emrepisode.closed_by.userconfig.id)


    notify(notify_users, f'''
        <a href='/client/emr_episodes/{emrepisode.client.id}/{emrepisode.id}/'>
            {emrepisode.get_episode_type_pretty()} episode has been {notify_verb}.
        </a>
    ''')



    # if we're in debug mode, send the email to me only
    if settings.DEBUG:
        emails = [settings.DEBUG_DEV_EMAIL, ]
    else:
        emails = [x.user.email for x in email_users]

    subject = f"{emrepisode.get_episode_type_pretty()} Episode {notify_verb.capitalize()}"

    message = f'''
        Hello,<br>
        This email is to inform you that a {emrepisode.get_episode_type_pretty()} episode
        has been {notify_verb} in Nova.<br>
        <a href='{episode_url}'>Click here to view it.</a>
        <br><br>
        Sincerely,<br>
        - The Nova Alerts Team
        <br><br><br>
        You are receiving this email notification because you have
        enabled the 'Send me an email when an EMR episode is {notify_verb}'
        option in your Nova user config. If you do not want to receive
        these emails you may update your user config in Nova
        <a href='{userconfig_url}'>here</a>.
    '''

    message = message.replace("\n", "")

    try:
        send_mail(
            subject=subject,
            html_message=message,
            message=message.replace("<br>", "\n"),
            recipient_list=emails,
            from_email=f"Nova Alert <{settings.NOVA_SYSTEM_EMAIL}>",
        )
    except Exception as e:
        print(f"Failed to send email\n{e}")









def duplicate_name_detect(first_name, last_name, birthdate=None):
    # The first part is pre-processing the name. The name is split by spaces
    # and special characters. Each split component of the name will be treated
    # as a name to be checked. This should handle a bunch of edge cases
    # including names with spaces, hyphenated last names, etc..
    # these are all stored in the fmt_first_name_list and fmt_last_name_list
    # Those lists are then used to find all the variant names in our similar
    # names lists

    # The second part is looking into our similar names tables and pulling
    # all the names which are similar to our name components and using those
    # to generate a large OR query.

    # Finally we use the large OR query to check our own database for names
    # that match any of the similar names we've found.

    # Tracking performance because I don't want this feature to get out of hand
    start_time = timeit.default_timer()

    #print("Checking database for existing Last: {}, First: {} DOB: {}".format(
    #    last_name, first_name, birthdate
    #))

    # The FMT lists are all 'potential' strings that we're parsing out of
    # the name inputs. The goal here is to sanitize and attempt to help
    # with typos and other edge cases before we start requesting data from
    # the similar names database
    fmt_first_name_list = set()
    first_name_parts = re.split(r'[\W]+', first_name)
    if len(first_name_parts) > 1:
        fmt_first_name_list.add("".join(first_name_parts))
    fmt_first_name_list.add(first_name)

    # if the name is 5 length or longer, lop off the first, the last
    # and the first and last letters and add to our lists. This should help
    # with typos
    for part in first_name_parts:
        if len(part) >= 5:
            fmt_first_name_list.add(part[1:])
            fmt_first_name_list.add(part[:-1])
            fmt_first_name_list.add(part[1:-1])

    fmt_last_name_list = set()
    last_name_parts = re.split(r'[\W]+', last_name)
    if len(last_name_parts) > 1:
        fmt_last_name_list.add("".join(last_name_parts))
    fmt_last_name_list.add(last_name)

    for part in last_name_parts:
        if len(part) >= 5:
            fmt_last_name_list.add(part[1:])
            fmt_last_name_list.add(part[:-1])
            fmt_last_name_list.add(part[1:-1])


    # These potential variants are lists of all the potential names we can
    # parse out of the input including all the searching we've done in
    # the similar names database
    first_name_potential_variants = set()
    last_name_potential_variants = set()

    # We can add the formatted and sanitized items to the list right
    # away.
    first_name_potential_variants.update(fmt_first_name_list)
    last_name_potential_variants.update(fmt_last_name_list)

    #print("First to check:\n", first_name_potential_variants)
    #print("Last to check:\n", last_name_potential_variants)



    # The call to the similar names database. We check if there are any names
    # in the database that match any of the names in our lists. Then we add
    # all of the variants to our potential variants list.
    first_variants = client.models.SimFirstVar.objects.filter(name__value__in=fmt_first_name_list).values("value")
    for variant in first_variants:
        if variant["value"] not in first_name_potential_variants:
            first_name_potential_variants.add(variant["value"])

    last_variants = client.models.SimLastVar.objects.filter(name__value__in=fmt_last_name_list).values("value")
    for variant in last_variants:
        if variant["value"] not in last_name_potential_variants:
            last_name_potential_variants.add(variant["value"])


    #print(f"Checking last name variants:\n{last_name_potential_variants}\n")
    #print(f"Checking first name variants:\n{first_name_potential_variants}")


    # Check if there are any names in the database where the first and the last
    # match our lists of names and variants, if there are, we need to return
    # the most similar matches to the user.

    # We add the last name to the first name and vice versa because
    # sometimes users aren't paying attention and are entering the names
    # in the wrong fields. Those silly users!

    # Also check if the is just a client with the same last name and the
    # same birthday

    last_query = Q()
    last_dob_query = Q()
    first_query = Q()

    for name in first_name_potential_variants:
        first_query.add(Q(fmt_first_name__iexact=name), Q.OR)
        last_query.add(Q(fmt_last_name__iexact=name), Q.OR)

        if birthdate:
            last_dob_query.add(Q(fmt_last_name__iexact=name) & Q(birthdate=birthdate), Q.OR)


    for name in last_name_potential_variants:
        last_query.add(Q(fmt_last_name__icontains=name), Q.OR)
        first_query.add(Q(fmt_first_name__iexact=name), Q.OR)

        if birthdate:
            last_dob_query.add(Q(fmt_last_name__iexact=name) & Q(birthdate=birthdate), Q.OR)



    full_query = Q()
    full_query.add(first_query & last_query, Q.OR)
    full_query.add(last_dob_query, Q.OR)

    matches = client.models.Client.objects.filter(full_query)

    match_data = []
    if matches:
        for match in matches:
            first_ratio = SequenceMatcher(
                None, match.first_name.lower(), first_name).ratio()

            last_ratio = SequenceMatcher(
                None, match.last_name.lower(), last_name).ratio()

            # Get a ratio of how close the name matches are, this is used
            # for sorting the results from most similar to least
            total_ratio = (first_ratio + last_ratio) / 2

            #print(f"{match}: {first_ratio} + {last_ratio} = {total_ratio}")
            same_birthdate = False
            if birthdate and match.birthdate == birthdate:
                same_birthdate = True

            match_data.append({
                "first_name": match.first_name,
                "last_name": match.last_name,
                "birthdate": None if not match.birthdate else match.birthdate.strftime(
                    settings.HUMAN_DATE_FORMAT),
                "same_birthdate": same_birthdate,
                "id": match.id,
                "first_ratio": first_ratio,
                "last_ratio": last_ratio,
                "total_ratio": total_ratio,
            })


    # Sorting the match data by same birthdate then match ratio
    sorted_match_data = sorted(
            match_data, key=lambda d: (not d["same_birthdate"], d["total_ratio"]), reverse=False)


    #print(f"Time to run duplicate name detection: {timeit.default_timer() - start_time}")

    return sorted_match_data












def lock_financial_fields(emrformform):
    pass






def get_next_batch_name(batch_type, fiscal_year):
    # Looks at the name field in the transaction batches and finds the
    # next out that matches the pattern: {INDEX_NUMBER}{BATCH_TYPE}
    # IMPORTANT: the index number is independent of the batch type.
    # so it would look like 101AR, 102MISC, 104WISC. There will not be a
    # 101MISC and a 101AR

    # The logic for this is very simple. Get a count of all the batches
    # for this year, add one and that's the name.
    batch_count = accounting.models.TransactionLot.objects.filter(
        year=fiscal_year).count()

    new_batch_name = f"{batch_count}{batch_type.upper()}"

    return new_batch_name








def human_elapsed(start, end=None):
    # Converts and start and end time elapsed to a human-friendly format
    if end is None:
        end = timezone.now()

    fmt_dt = ""
    now = timezone.now()
    today = now.date()
    elapsed = now - start


    # if time is within the last minute have it say "just now"
    if elapsed.total_seconds() <= 60:
        fmt_dt = "Just now"

    # if time is within the last 5 minutes say "minutes ago"
    elif elapsed.total_seconds() <= 60 * 5:
        fmt_dt = "A few minutes ago"

    # if time is 5 - 59 minutes say the number of minutes
    elif elapsed.total_seconds() <= 60 * 60:
        #import pdb; pdb.set_trace()
        fmt_dt = f"{round(elapsed.total_seconds() / 60)} minutes ago"

    # if time is 1-3 hours ago say the number of hours and minutes
    elif elapsed.total_seconds() <= 60 * 60 * 3:
        total_minutes = elapsed.total_seconds() // 60
        hours = int(total_minutes // 60)
        remain = int(total_minutes % 60)

        hour_word = "hours"
        if hours == 1:
            hour_word = "hour"

        minute_word = "minutes"
        if remain == 1:
            minute_word = "minute"

        fmt_dt = f"{hours} {hour_word} and {remain} {minute_word} ago"

    # if time is 4 hours to 12 hours say the number of hours ago
    elif elapsed.total_seconds() <= 60 * 60 * 12:
        hours = round(elapsed.total_seconds() / 3600)
        fmt_dt = f"{hours} hours ago"

    elif today == start.date():
        fmt_dt = "Earlier today"

    elif (today - dt.timedelta(days=1) == start.date()):
        fmt_dt = "Yesterday"

    else:
        fmt_dt = start.strftime("%b %-d, %Y")


    return fmt_dt







def notify(users, message, route=None):
    # Generates notifications that will be shown to users. Expect a user
    # or list of users.
    # If the users is blank, then a notification will be sent to all users
    #print(f"Sending notify to: {users}\nMessage: {message}\nRoute: {route}")
    if isinstance(users, list) is False and isinstance(users, User):
        users = [users, ]

    elif users is None:
        # Sent the notification to all active users except for auditors
        users = User.objects.filter(
            userconfig__active=True).exclude(userconfig__user_type=4)

    for user in users:
        if isinstance(user, config.models.UserConfig):
            user = user.user
        # Generate the notifications
        #print(f"New notification: '{user}'\n'{message}'")

        note = config.models.Notification(
            receiver=user,
            message=message.strip(),
            route=route,
        )

        note.save()










def get_emrform_instance(emr_form_id=None, episode=None, emr_form_type=None):
    """ This helper function takes an emr_form_id and returns the proper
        child emr form type. Like the get_subform() function this should
        really only be used for emr forms

        If no emr_form_id is passed, send back a fresh instance
    """

    if emr_form_id is None and (episode is None or emr_form_type is None):
        raise Exception("Need to pass either the emr_form_id or the episode and emr_form_type")



    if emr_form_id:
        inst = get_object_or_404(client.models.EmrForm, id=emr_form_id)
        epi_type = inst.emrepisode.episode_type.lower()
        form_type = inst.emrformtype.name.lower()
    else:
        epi_type = episode.episode_type.lower()
        form_type = emr_form_type.name.lower()


    # Depending on the emrformtype, re-query with the proper child class
    if epi_type == "clinic_preadmit" and form_type == "clinic wait list":
        if emr_form_id:
            return get_object_or_404(client.models.EmrClinicWaitlistForm, id=emr_form_id)
        else:
            return client.models.EmrClinicWaitlistForm()

    if epi_type == "clts" and form_type == "annual case note":
        if emr_form_id:
            return get_object_or_404(
                client.models.EmrCltsAnnualCaseNoteForm, id=emr_form_id)
        else:
            return client.models.EmrCltsAnnualCaseNoteForm()


    if epi_type == "clts" and form_type == "6 month case note":
        if emr_form_id:
            return get_object_or_404(
                client.models.EmrClts6MonthCaseNoteForm, id=emr_form_id)
        else:
            return client.models.EmrClts6MonthCaseNoteForm()


    if epi_type == "clts" and form_type == "enrollment case note":
        if emr_form_id:
            return get_object_or_404(
                client.models.EmrCltsEnrollmentCaseNoteForm, id=emr_form_id)
        else:
            return client.models.EmrCltsEnrollmentCaseNoteForm()


    if epi_type == "clts" and form_type == "clts/cst assessment":        
        if emr_form_id:
            return get_object_or_404(client.models.EmrCltsCstAssessmentForm, id=emr_form_id)
        else:
            # CLTS/CST Assessment is a bit different. If there is already
            # an existing assessment, that means the user wants to create an
            # annual update from the existing assessment. In this case, 
            # we need to create a copy of the data but in a new record.
            # TODO MOVE THIS TO THE MODEL OR VIEW
            if episode:
                found_existing = client.models.EmrCltsCstAssessmentForm.objects.filter(
                    emrepisode=episode
                ).order_by("-assessment_date").first()

                if found_existing:
                    print("Found existing assessment form. Using its data as a template")
                    found_existing.prep_for_copy()
                    found_existing.assessment_date = timezone.now()
                    found_existing.assessment_type = "annual"
                    return found_existing

            return client.models.EmrCltsCstAssessmentForm()



    if epi_type == "crisis" and form_type == "crisis alert":
        if emr_form_id:
            return get_object_or_404(client.models.EmrCrisisAlertForm, id=emr_form_id)
        else:
            return client.models.EmrCrisisAlertForm()


    if epi_type == "crisis" and form_type == "crisis plan":
        if emr_form_id:
            return get_object_or_404(client.models.EmrCrisisPlanForm, id=emr_form_id)
        else:
            # Similar to the CLTS/CST assessment, we want the newly created
            # crisis plan to copy data from the latest existing one
            if episode:
                found_existing = client.models.EmrCrisisPlanForm.objects.filter(
                    emrepisode=episode).order_by("-date").first()

                if found_existing:
                    found_existing.prep_for_copy()
                    return found_existing


                return client.models.EmrCrisisPlanForm()

    if epi_type == "crisis" and form_type == "nwc assessment and response plan":
        if emr_form_id:
            return get_object_or_404(client.models.EmrNwcAssesResponseForm, id=emr_form_id)
        else:
            return client.models.EmrNwcAssesResponseForm()

    if epi_type == "crisis" and form_type == "nwc note":
        if emr_form_id:
            return get_object_or_404(client.models.EmrCrisisNwcNoteForm, id=emr_form_id)
        else:
            return client.models.EmrCrisisNwcNoteForm()

    if epi_type == "crisis" and form_type == "out of county assessment and response plan":
        if emr_form_id:
            return get_object_or_404(client.models.EmrOutOfCountyAssesResponseForm, id=emr_form_id)
        else:
            return client.models.EmrOutOfCountyAssesResponseForm()

    if epi_type == "crisis" and form_type == "response plan review":
        if emr_form_id:
            return get_object_or_404(client.models.EmrCrisisResponsePlanReviewForm, id=emr_form_id)
        else:
            return client.models.EmrCrisisResponsePlanReviewForm()

    if epi_type == "crisis" and form_type == "county assessment and response plan":
        if emr_form_id:
            return get_object_or_404(client.models.EmrJacksonCountyAssesResponseForm, id=emr_form_id)
        else:
            return client.models.EmrJacksonCountyAssesResponseForm()

    if epi_type == "crisis" and form_type == "placement":
        if emr_form_id:
            return get_object_or_404(client.models.EmrPlacementForm, id=emr_form_id)
        else:
            return client.models.EmrPlacementForm()

    if epi_type == "cst" and form_type == "pps mental health admission":
        if emr_form_id:
            return get_object_or_404(client.models.EmrCstPpsMhAdmissionForm, id=emr_form_id)
        else:
            return client.models.EmrCstPpsMhAdmissionForm()

    if epi_type == "cst" and form_type == "pps mental health discharge":
        if emr_form_id:
            return get_object_or_404(client.models.EmrCstPpsMhDischargeForm, id=emr_form_id)
        else:
            return client.models.EmrCstPpsMhDischargeForm()


    if epi_type == "randi" and form_type == "intake":
        if emr_form_id:
            return get_object_or_404(client.models.EmrRandiIntakeForm, id=emr_form_id)
        else:
            # Like the CLTS/CST Assessment, we want the newly created intake
            # to copy the data from the latest existing one
            if episode:
                found_existing = client.models.EmrRandiIntakeForm.objects.filter(
                    emrepisode__client=episode.client
                ).order_by("-date").first()

                if found_existing:
                    found_existing.prep_for_copy()
                    return found_existing


            return client.models.EmrRandiIntakeForm()

    if epi_type == "randi" and form_type == "internal referral":
        if emr_form_id:
            return get_object_or_404(client.models.EmrRandiReferralForm, id=emr_form_id)
        else:
            return client.models.EmrRandiReferralForm()

    if epi_type == "randi" and form_type == "external referral":
        if emr_form_id:
            return get_object_or_404(client.models.EmrRandiExternalReferralForm, id=emr_form_id)
        else:
            return client.models.EmrRandiExternalReferralForm()

    if epi_type == "tcm" and form_type == "placement":
        if emr_form_id:
            return get_object_or_404(client.models.EmrPlacementForm, id=emr_form_id)
        else:
            return client.models.EmrPlacementForm()

    if epi_type == "tcm" and form_type == "tcm assessment":
        if emr_form_id:
            return get_object_or_404(client.models.EmrTcmAssessmentForm, id=emr_form_id)
        else:
            return client.models.EmrTcmAssessmentForm()

    if epi_type == "tcm" and form_type == "pps mental health admission":
        if emr_form_id:
            return get_object_or_404(client.models.EmrTcmPpsMhAdmissionForm, id=emr_form_id)
        else:
            return client.models.EmrTcmPpsMhAdmissionForm()

    if epi_type == "tcm" and form_type == "pps mental health discharge":
        if emr_form_id:
            return get_object_or_404(client.models.EmrTcmPpsMhDischargeForm, id=emr_form_id)
        else:
            return client.models.EmrTcmPpsMhDischargeForm()

    if epi_type == "tcm" and form_type == "pps aoda admission":
        if emr_form_id:
            return get_object_or_404(client.models.EmrTcmPpsAodaAdmissionForm, id=emr_form_id)
        else:
            return client.models.EmrTcmPpsAodaAdmissionForm()

    if epi_type == "tcm" and form_type == "pps aoda discharge":
        if emr_form_id:
            return get_object_or_404(client.models.EmrTcmPpsAodaDischargeForm, id=emr_form_id)
        else:
            return client.models.EmrTcmPpsAodaDischargeForm()






    if form_type == "case note":
        if emr_form_id:
            return get_object_or_404(client.models.EmrCaseNoteForm, id=emr_form_id)
        else:
            return client.models.EmrCaseNoteForm()


    raise Exception(f"epi_type {epi_type} and form_type {form_type} not implemented")










def calculate_units(emr_form, minutes=None):
    # Calculate the units for this emr form if needed. using the
    # form's episode and emrformtype to decide what type of calculation
    # to perform against the minutes to come up with the units

    # I thought about putting this in the EmrForm class but we may need to
    # calculate units before saving the form so its best to keep it here

    if hasattr(emr_form, "emrepisode") is False:
        return None


    epi_type = emr_form.emrepisode.episode_type.lower()

    # If no minutes are passed, use what's saved on the form, otherwise
    # use the minutes that were passed
    if minutes is None:
        minutes = emr_form.total_minutes


    units = 0


    if epi_type == "crisis":
        units = calculate_crisis_units(minutes, emr_form)

    elif epi_type in ["clts", "clts_preadmit", "tcm"]:
        units = calculate_clts_units(minutes)

    elif epi_type == "b3":
        units = calculate_b3_units(minutes)


    else:
        pass
        #print(f"No unit calculation implemented for episode type '{epi_type}'")


    return units











def calculate_clts_units(minutes):
    getcontext().prec = 28


    """
        CLTS units are calculated by 1 unit every 15 minutes.
        Minutes    Units
        1-15        1
        16-30       2
        31-45       3
        46-60       4
    """

    # Similar to crisis units below, divide by 60 without remainders then
    # use the remainder to calculate the additional units
    if minutes == 0:
        return 0
    if minutes >= 1 and minutes <= 15:
        return 1
    if minutes >= 16 and minutes <= 30:
        return 2
    if minutes >= 31 and minutes <= 45:
        return 3
    if minutes >= 46 and minutes <= 60:
        return 4

    # If the service is more than an hour, figure out what the remainder
    hours = Decimal(math.floor(Decimal(minutes) / Decimal('60.0')))

    remain = Decimal(minutes) - (hours * Decimal('60.0'))

    if remain == 0:
        return hours * 4
    if remain >= 1 and remain <= 15:
        return 1 + (hours * 4)
    if remain >= 16 and remain <= 30:
        return 2 + (hours * 4)
    if remain >= 31 and remain <= 45:
        return 3 + (hours * 4)
    if remain >= 46 and remain <= 60:
        return 4 + (hours * 4)







def calculate_b3_units(minutes):
    getcontext().prec = 28

    minutes = Decimal(minutes)

    # B-3 Unites are calculated to 1 unit every 15 minutes with rounding
    # for less than 15 minutes:
    # Minutes    Units
    # 1-5        0.3
    # 6-10       0.7
    # 11-15      1.0

    # Should be pretty straightforward. If the minutes are over 15, divide by
    # 15 and process the remainder according to the table above
    if minutes == 0:
        return 0

    if minutes >= 1 and minutes <= 5:
        return Decimal('0.3')
    if minutes >= 6 and minutes <= 10:
        return Decimal('0.7')
    if minutes >= 11 and minutes <= 15:
        return Decimal('1.0')

    quotient = minutes // 15
    remain = minutes % 15

    if remain == 0:
        return quotient

    if remain >= 1 and remain <= 5:
        return quotient + Decimal('0.3')
    if remain >= 6 and remain <= 10:
        return quotient + Decimal('0.7')
    if remain >= 11 and remain <= 15:
        return quotient + Decimal('1.0')













def calculate_crisis_units(minutes, emr_form=None):
    getcontext().prec = 28
    """ Crisis units calculation as of 2023. Units are 15 minutes long
        and if the minutes are 7 or less, that's zero units
        Minutes     Units
        0-7         0
        8-22        1
        23-37       2
        38-52       3
        ...
    """

    # The billing code H0030 is a set rate, if it's used, it should always
    # return a single unit
    if emr_form and emr_form.billing_code == "H0030":
        return 1


    if minutes < 8:
        return 0
    else:
        return 1 + (minutes - 8) // 15








def save_cc_report_files(cc_report_id, html_data, report_data):

    cc_report = get_object_or_None(report.models.CCReport, id=cc_report_id)
    csv_data = generate_csv_from_json(report_data)


    # TODO update the report watcher to show that the report was empty
    # TODO figure out how to save this in memory, or the database or soemthing
    # instead of saving as a file!
    # This should be pretty easy! Probably use the cc_report_id to find the
    # instance, save this html to the instance, and of course once the user
    # loggs out, delete all the cc_report instances they have

    with open("/srv/nova/cc_reports/{}".format(cc_report.get_csv_filename()), "w") as csv_file:
        writer = csv.writer(csv_file)
        if csv_data:
            writer.writerow(csv_data[0].keys())

            for row in csv_data:
                writer.writerow(row.values())

    with open("/srv/nova/cc_reports/{}".format(cc_report.get_html_filename()), "w") as html_file:
        html_file.write(html_data)  









def create_cc_report(request):
    rpt_name = inspect.stack()[1][3]  # This makes me nervous, is there a better option?

    ccreport = report.models.CCReport(
        userconfig=request.user.userconfig,
        status="submitted",
        post=json.dumps(request.POST, cls=DjangoJSONEncoder),
        path=request.path,
        submitted=timezone.now(),
        report_name=rpt_name,
    )

    ccreport.save()










def get_cc_report_assets():
    css = ""
    js = ""
    with open("/srv/nova/static/bs/css/bootstrap.min.css") as css_file:
        css += css_file.read()
    with open("/srv/nova/static/fontawesome/all.css") as css_file:
        css += css_file.read()
    with open("/srv/nova/static/google_fonts.css") as css_file:
        css += css_file.read()                             
    with open("/srv/nova/static/nova.css") as css_file:
        css += css_file.read()
    with open("/srv/nova/static/nova_responsive.css") as css_file:
        css += css_file.read()
    with open("/srv/nova/static/cc_report.css") as css_file:
        css += css_file.read()


    with open("/srv/nova/static/jquery-3.3.1.min.js") as js_file:
        js += js_file.read()
        js += "\n"
    with open("/srv/nova/static/jquery-ui.min.js") as js_file:
        js += js_file.read()
        js += "\n"
    with open("/srv/nova/static/jquery.multiselect.js") as js_file:
        js += js_file.read()
        js += "\n"
    with open("/srv/nova/static/bs/js/bootstrap.min.js") as js_file:
        js += js_file.read()
        js += "\n"
    with open("/srv/nova/static/popper.min.js") as js_file:
        js += js_file.read()
        js += "\n"
    with open("/srv/nova/static/cc_report.js") as js_file:
        js += js_file.read()
        js += "\n"
    with open("/srv/nova/static/datatables.min.js") as js_file:
        js += js_file.read()
        js += "\n"

    # script is served as a view now.
    #with open("/srv/nova/static/script.js") as js_file:
    #    js += js_file.read()
    #    js += "\n"

    #home_url = get_app_config("url")
    #js += "$(document).ready(function(){ $('.navbar-brand').attr('href','"+home_url+"').attr('target', '_blank'); });\n"    

    return "<style>{}</style>\n<script>{}</script>".format(css, js)





def get_pdf_report_assets():
    css = ""


    with open("/srv/nova/static/bs/css/bootstrap.min.css") as css_file:
        css += css_file.read()
    #with open("/srv/nova/static/fontawesome/all.css") as css_file:
    #    css += css_file.read()
    with open("/srv/nova/static/google_fonts.css") as css_file:
        css += css_file.read()
    with open("/srv/nova/static/nova.css") as css_file:
        css += css_file.read()
    with open("/srv/nova/static/nova_responsive.css") as css_file:
        css += css_file.read()
    with open("/srv/nova/static/cc_report.css") as css_file:
        css += css_file.read()


    return "<style>{}</style>".format(css)







def generate_csv_from_json(data, report_name=None):    
    # General cleanup before sending to writer
    for row in data:
        # Look for any choices that need translating
        if "trans_type" in row:
            row["trans_type"] = accounting_choices.TRANS_TYPE[int(row["trans_type"])][1]

        if "gender" in row and row["gender"].isdigit():
            row["gender"] = client_choices.GENDER[int(row["gender"])][1]

        if "race" in row:
            row["race"] = client_choices.RACE[int(row["race"])][1]

        if "hispanic" in row:
            row["hispanic"] = client_choices.HISPANIC[int(row["hispanic"])][1]

        if "lot" in row:
            lot = get_object_or_None(accounting.models.TransactionLot, pk=row["lot"])
            row["batch"] = str(lot)

        if "unit_type" in row and row["unit_type"]:
            row["unit_type"] = accounting_choices.TRANS_UNIT[int(row["unit_type"])][1]

        if "account__type" in row:
            row["account__type"] = accounting_choices.ACCOUNT_TYPE[int(row["account__type"])][1]


        # Replace HTML <br> tags with /n
        for cell in row:
            try:
                if isinstance(row[cell], str) is False:
                    continue
            except TypeError:  # Figure out why this is needed!!!
                continue

            if "<br>" in row[cell] or "<br/>" in row[cell] or "<br />" in row[cell]:
                row[cell] = row[cell].replace(
                    "<br>", "\n").replace(
                    "<br/>", "\n").replace(
                    "<br />", "\n")



        # Cleanup Per-Report if needed
        if report_name == "Client_statement":
            row["transaction_date"] = row.pop("date")

        elif report_name == "Budget_export":
            row["utilization"] = get_object_or_None(accounting.models.BudgetItem, id=row["id"]).get_usage_fancy()

        elif report_name == "Clients_report":
            address = get_object_or_None(client.models.Client, id=row["id"]).get_address()
            if address:
                row["address__care_of"] = address.care_of
                row["address__street"] = address.street
                row["address__apt_suite"] = address.apt
                row["address__city"] = address.city
                row["address__state"] = address.state
                row["address__zip"] = address.zip
            else:
                row["address__care_of"] = ""
                row["address__street"] = ""
                row["address__apt_suite"] = ""
                row["address__city"] = ""
                row["address__state"] = ""
                row["address__zip"] = ""

        elif report_name == "Accounts":
            row["type"] = accounting_choices.ACCOUNT_TYPE[int(row["type"])][1]   


    return data



def parse_date(date):
    """ 
        This function is used in reports and other places where dates
        may or may not get cleaned

        TODO maybe look into using dateutil to make this more robust?
    """

    if "-" in date:
        date = date.replace("-","/")

    if "/" in date:
        try:
            return dt.datetime.strptime(date, settings.DATE_FORMAT)
        except:
            pass

        try:
            return dt.datetime.strptime(date, "%Y/%m/%d")
        except:
            pass

   
    
    if len(date) == 8:
        try:
            return dt.datetime.strptime(date, "%m%d%Y")
        except:
            pass

        try:
            return dt.datetime.strptime(date, "%Y%m%d")
        except:
            pass

    raise Exception("Could not parse date string '{}'".format(date))





def generate_export_acs_text(transactions):
    '''
        0-7     : YYYYMMDD - status date (now)
        8-16    : YYYYMMDD - JE date?
        17-43   : county account number (fund account)
        44-52   : YYYYMMDD - transaction date maybe?
        53-61   : User Id/app name?
        62-84   : Description
        85-94   : Amount
        95-96   : Plus/minus symbol
        97-112  : ??? no clue maybe project code
        113-136 : Vendor name
        137-142 : Vendor number
        143     : Not sure blank space?? maybe vendor number?
        144-148 : Not sure, just 5 zeros
        149     : not sure, blank space
        150-163 : Lot number 
        164-169 : looks like a JE number may extend all the way to 209
        170-209 : Currently just whitespace
    '''
    
    lines = []
    for t in transactions.order_by("vendor__name"):
        if t.account.cash_account:
            continue # skip cash accounts. they are for our records, not the county's


        # If the trans is not natrual to the account, flip the sign +/-
        # NOTE these are actually 'reversed' because we're exporting to the 
        # county system. I know thats not a great explination but its better
        # than nothing.
        if t.account.type == '0':
            if t.trans_type == '0':
                symbol = "-"
            else:
                symbol = "+"
        
        elif t.account.type == '1':
            if t.trans_type == '1':
                symbol = "+"
            else:
                symbol = "-"   

        if t.vendor:
            vendor_name = t.vendor.name[:24].ljust(24, ' ')
            if t.vendor.vendor_number:
                vendor_number = t.vendor.vendor_number[:6].ljust(6, ' ')
            else:
                vendor_number = "".ljust(6, ' ')

            
        else:
            vendor_name = "".ljust(24, ' ')
            vendor_number = "".ljust(6, ' ')


        if t.invoice:
            invoice = t.invoice[:14].ljust(14, ' ')
        else:
            invoice = "".ljust(14, ' ')

        if t.description:
            description = t.description[:24].ljust(24, ' ').upper()
        else:
            description = "".ljust(24, ' ')

        if t.account.county_name:
            county_name = t.account.county_name.ljust(28, ' ')
        else:
            county_name = "".ljust(28, ' ')

        text = ""
        text += dt.datetime.now().strftime("%Y%m%d") # status date
        text += t.journal_entry.date.strftime("%Y%m%d") # JE date
        text += county_name # county account number
        text += t.date.strftime("%Y%m%d") # Transaction date maybe
        text += "NOVA".ljust(8, ' ') 
        text += description
        text += str(int(abs(t.amount * 100))).rjust(11, '0')
        text += symbol        
        text += "".ljust(16, ' ') # maybe project code, right now just blank spaces
        text += vendor_name.upper()
        text += vendor_number
        text += "00000 " # not sure
        text += invoice.upper()
        text += t.journal_entry.get_je_number().ljust(46, ' ')      


        text += "\n"

        lines.append(text)


    response = HttpResponse(lines, content_type="text/csv")
    response["Content-Disposition"] = "attachment; filename={}".format(transactions.first().journal_entry.get_export_name("txt"))

    return response





def generate_export_acs_csv(transactions):
    ''' 
        DO NOT USE. CURRENTLY DOES NOT WORK
    '''
    return 
    response = HttpResponse(content_type="text/csv")
    response["Content-Disposition"] = "attachment; filename={}".format(transactions.first().journal_entry.get_export_name("csv"))
    writer = csv.writer(response)

    # Fields:
    # 0 Status date - now basically (YYYYMMDD)
    # 1 Posting date - accounting period/date (YYYYMMDD)
    # 2 Fund account - believe this is the county account
    # 3 Transaction Date - date on the journal entry
    # 4 User id - optional
    # 5 Detail description - description on journal entry
    # 6 Amount - decimal with 2 digits
    # 7 Amount sign - puls or minus (+/-)
    # 8 Project code - optional
    # 9 Gen Description  - batch description i think?
    # 10 Directory code - blank
    # 11 Reference code - optional reference code for journal entry
    # 12 Invoice code - blank
    # 13 Voucher code - blank
    # 14 PO code - blank
    # 15 1099 flag - blank


    for t in transactions:
        row = []

        row.append(dt.datetime.now().strftime(settings.HUMAN_DATE_FORMAT))
        row.append(dt.datetime.now().strftime(settings.HUMAN_DATE_FORMAT))
        row.append(t.account.county_name)
        row.append(t.journal_entry.date.strftime(settings.HUMAN_DATE_FORMAT))
        row.append("")
        row.append(t.description.replace("'",""))
        row.append("{:.2f}".format(t.amount))
        if t.amount >= 0:
            row.append("+")
        else:
            row.append("-")
        row.append("")
        row.append(t.invoice)
        row.append("")
        row.append("")
        row.append("")
        row.append("")
        row.append("")
        row.append("")

        writer.writerow(row)


    return response



def generate_units(casenote):
    # !!! The EMR Case Notes the calculate_units() function !!!
    # This is still used on older PPS/B3 case notes.

    # cleanup in case the values are None
    if casenote.service is None:
        return 0
    if hasattr(casenote.service, "pps_unit") == False:
        return 0

    getcontext().prec = 28


    # convert minutes to hours and minutes
    hours = 0
    if casenote.minutes:
        if casenote.minutes >= 60:
            hours =  math.floor(casenote.minutes / 60)
            minutes = casenote.minutes - (hours * 60)
            print("MORE THAN 60. Start: {}  End: {}:{}".format(casenote.minutes, hours, minutes.__str__().rjust(2,'0')))
        else:
            minutes = casenote.minutes
        
    else:
        minutes = Decimal('0.00')


    if casenote.hours:
        hours += casenote.hours
    else:
        hours += 0


    if casenote.service.pps_unit == "1":
        units = Decimal(hours)

        # this is from the state I think its for pps units:
        # minutes   billed
        # 1-6       0.1
        # 7-12      0.2
        # 13-18     0.3
        # 19-24     0.4
        # 25-30     0.5
        # 31-36     0.6
        # 37-42     0.7
        # 43-48     0.8
        # 49-54     0.9
        # 55-60     1.0

        if minutes >= 1 and minutes <= 6:
            units += Decimal('0.1')
        elif minutes >= 7 and minutes <= 12:
            units += Decimal('0.2')
        elif minutes >= 13 and minutes <= 18:
            units += Decimal('0.3')
        elif minutes >= 19 and minutes <= 24:
            units += Decimal('0.4')
        elif minutes >= 25 and minutes <= 30:
            units += Decimal('0.5')
        elif minutes >= 31 and minutes <= 36:
            units += Decimal('0.6')
        elif minutes >= 37 and minutes <= 42:
            units += Decimal('0.7')
        elif minutes >= 43 and minutes <= 48:
            units += Decimal('0.8')
        elif minutes >= 49 and minutes <= 54:
            units += Decimal('0.9')
        elif minutes >= 55 and minutes <= 60:
            units += Decimal('1.0')

        print("{} {}".format(minutes, round(units,1)))
        return round(units,1)

    else:
        return 0




def get_authorized_users():
    active_sessions = Session.objects.filter(expire_date__gte=timezone.now())
    user_list = []
    for session in active_sessions:
        data = session.get_decoded()
        user_id = data.get("_auth_user_id")

        results = config.models.UserConfig.objects.filter(user__id=user_id)

        if results:
            user_config = results[0]
            if user_config not in user_list:
                user_list.append(user_config)
        else:
            if user_id not in user_list:
                user_list.append("Unconfigured user {}".format(user_id))

        

    return user_list




def get_pps_epi(uuid):
    aoda_match = get_object_or_None(client.models.PpsAodaEpi, pps_epi_uuid=uuid)
    if aoda_match:
        return aoda_match

    mh_match = get_object_or_None(client.models.PpsMhEpi, pps_epi_uuid=uuid)
    if mh_match:
        return mh_match

    core_match = get_object_or_None(client.models.PpsCoreEpi, pps_epi_uuid=uuid)
    if core_match:
        return core_match

    b3_match = get_object_or_None(client.models.PpsB3Epi, pps_epi_uuid=uuid)
    if b3_match:
        return b3_match

    return None





def sign_data(data, salt=None):
    signer = TimestampSigner(salt=salt)
    signed = signer.sign(json.dumps(data))
    return signed


def verify_signature(signed_data, salt=None):
    # TODO Rename this because theres already a function with this name
    # on the EmrForm!
    signer = TimestampSigner(salt=salt)
    try:
        original = signer.unsign(signed_data)        
        return True
    except BadSignature:
        print("Bad signature")
        return False


def compare_signatures(signed1, signed2, salt=False):
    signer = TimestampSigner(salt=salt)
    try:
        original1 = signer.unsign(signed1)
    except BadSignature:
        print("Bad signature for signed1")
        return False

    try:
        original2 = signer.unsign(signed2)
    except BadSignature:
        print("Bad signature for signed2")
        return False

    if original1 != original2:
        #print("Signatures do not match")
        #print(original1)
        #print("\n\n")
        #print(original2)
        return False
    else:
        #print("Signatures match")
        #print(original1)
        return True


def get_model(model_name, id=None):
    # Takes a model name and an ID and returns the instance

    # TODO look at everywhere this gets called and see if using apps.get_model()
    # can work instead.
    if model_name in ("journal_entry", "journalentry"):
        if id is None:
            return accounting.models.JournalEntry()
        return get_object_or_None(accounting.models.JournalEntry, id=id)

    elif model_name == "transaction":
        if id is None:
            return accounting.models.Transaction()
        return get_object_or_None(accounting.models.Transaction, id=id)

    elif model_name == "contract":
        if id is None:
            return accounting.models.Contract()
        return get_object_or_None(accounting.models.Contract, id=id)

    elif model_name == "EmrForm":
        return client.models.EmrForm.objects.get(id=id)

    elif model_name == "EmrDoc":
        return client.models.EmrDoc.objects.get(id=id)

    elif model_name == "auditorclient":
        return config.models.AuditorClient.objects.get(id=id)

    elif model_name == "userconfig":
        return config.models.UserConfig.objects.get(id=id)

    elif model_name == "client":
        return client.models.Client.objects.get(id=id)

    else:
        raise Exception(f"model_name: '{model_name}' not implemented for get_model")



def link_models(request, parent, child):
    # Crosswalk to link foregin keys quickly and easily
    parent_name = ContentType.objects.get_for_model(parent).model
    child_name = ContentType.objects.get_for_model(child).model

    if parent_name == "journalentry":
        child.journal_entry = parent
        return save_audit(request, child)

    elif parent_name == "emrform":
        parent.linked_docs.add(child)
        return save_audit(request, parent)

    elif parent_name == "contract" and child_name == "service":
        parent.services.add(child)
        return save_audit(request, parent)

    elif parent_name == "userconfig" and child_name == "client":
        parent.auditor_clients.add(child)
        return save_audit(request, parent)

    else:
        raise Exception("Could not find match in crosswalk")


def render_form(form, locked=False, hide_all_optional=False):
    #print(f"render_form(locked={locked}, hide_all_optional={hide_all_optional}")
    '''Add classes and modify forms before they are sent to the template'''

    # If the user is view_only then disable the form controls. This is just a
    # visual thing, as the user is already prevented from saving the form.
    if hasattr(form, "request") and hasattr(form.request, "user") and (form.request.user.userconfig.view_only is True or form.request.user.userconfig.user_type == "4"):
        locked = True


    for form_input in form:
        if hasattr(form_input, "field") is False:
            continue
        if hasattr(form_input.field, "widget") is False:
            continue
        if hasattr(form_input.field.widget, "attrs") is False:
            continue

        if 'class' not in form_input.field.widget.attrs:
            form_input.field.widget.attrs['class'] = 'form-control'
        else:
            form_input.field.widget.attrs['class'] += ' form-control'

        if locked:
            form_input.field.widget.attrs['disabled'] = True

        # Boolean fields are never optional
        if isinstance(form_input.field, django.forms.fields.BooleanField):
            form_input.field.widget.attrs['class'] += ' optional-off'
        # Select boxes are never optional
        if isinstance(form_input.field, django.forms.fields.ChoiceField):
            form_input.field.widget.attrs['class'] += ' optional-off'

        # Override option, hide all the optional tags
        if hide_all_optional:
            if "optional-off" not in form_input.field.widget.attrs["class"]:
                form_input.field.widget.attrs["class"] += " optional-off"



    # Form auto-fill. Remembers how the user last filled in this form and
    # sets initial values.
    # Also saves the initial values if the form is being POSTed

    # Somehow check the user's form history and set initial values
    form.user_form = False
    # In case we want to have this remember what people filled in on
    # other screens, say the transaction view filter
    non_report_paths = [
        "/accounting/transaction/",
    ]

    if hasattr(form, "request") and form.request is not None and (form.request.path.startswith("/report/") or form.request.path in non_report_paths):
        print("UserForm enabled on this view")
        if form.request.method == "POST":
            # save the POST data into the session
            # Because of CC reports, we cannot save this in the form.request session            
            userform = config.models.UserForm.objects.filter(
                userconfig=form.request.user.userconfig, 
                form_class=form.__class__).first()

            if userform:
                userform.form_data = form.request.POST
            else:
                userform = config.models.UserForm(
                    userconfig=form.request.user.userconfig)

            userform.form_class = form.__class__
            # build up a json object that we can read from. Because some
            # stuff wont show up in a POST if its not "on" or True
            # (checkboxes), lets run this against the form fields..
            form_data = {}

            for field in form.fields:
                # Build up the form_data by looping through all the fields
                # in the POST request. Depending on the data type, some fields
                # may require extra parsing before getting dumped into JSON
                # If some fields are not in the POST, those can be set to
                # None, or if they are boolean, False
                if field in form.request.POST:
                    if form.request.POST[field].strip():
                        form_data[field] = form.request.POST[field]

                else:
                    if isinstance(form.fields[field], django.forms.fields.BooleanField):
                        form_data[field] = False
                    else:
                        form_data[field] = None


            userform.form_data = json.dumps(form_data, cls=DjangoJSONEncoder)
            userform.save()


        if form.request.method == "GET":
            userform = config.models.UserForm.objects.filter(
                userconfig=form.request.user.userconfig, 
                form_class=form.__class__).first()
            # User has used this form before, auto fill the data
            if userform:
                # Set this bit so we can decide oh the view
                # if we want to prevent initial values 
                form.user_form = True 
                form_data = json.loads(userform.form_data)
                form.userform_data = form_data 
                #print("Form data:\n{}".format(form_data))
                for key in form_data:
                    if key == "csrfmiddlewaretoken":
                        continue

                    if key in form.fields:
                        #print("{} {}".format(key, form_data[key]))
                        form.fields[key].initial = form_data[key]
            else:
                # Even if there is no userform_data, make it a blank dict
                # this makes coding the view a lot easier
                form.userform_data = {}


    form.required_css_class = 'required'
    form.label_suffix = ''
    form.locked = locked


    return form


def set_autocomplete(instance, form, field, model=None, model_id=None, target_field_override=None):
    """ Sets initial value for form fields that use autocomplete lookup.
        Basically, autocomplete is 2 fields. The friendly, human-readable
        field that gets auto filled, and then a hidden field with the actual ID of the
        record. When editing a record, we want both the fields set to the
        existing data.
        Use in EDIT view and not in NEW views Also returns the field_record object

        If you have a null instance but still want to fill the field, just pass
        the model and model_id.

        You can also override the target friendly field if you're using
        non-standard naming. Standard naming would be: client, client_lookup
    """
    # If we're coming from a filter or another form that doesn't persist an instance object
    # We need to pass along the model and it's id to generate an instance

    # First, check if the form even has this field. Maybe it's an inherited
    # form or something that doesn't share all the same fields with other forms
    # that inherit from the same parent. Regardless, print a warning and
    # then just do nothing
    if not form or hasattr(form, "fields") is False or field not in form.fields:
        #print(f"Field '{field}'' not found in form. Skipping")
        return form



    if model and model_id:
        field_record = get_object_or_None(model, id=model_id)
    else:
        field_record = getattr(instance, field, None)


    if field_record:
        lookup_field = "{}_lookup".format(field)
        if lookup_field not in form.fields:
            #print(f"Lookup field '{lookup_field}'' not found in form. Skipping")
            return form

        if target_field_override:
            form.fields[target_field_override].initial = field_record.get_auto_name()
        else:
            form.fields[lookup_field].initial = field_record.get_auto_name()


        # autocomplete JS will look at this attr to make sure the input and the hidden input match
        form.fields[field].widget.attrs['auto_name'] = field_record.get_auto_name()


        # If this is a filter or non-persistent form, we also need to set the non-lookup fields
        if model and model_id:
            form.fields[field].initial = model_id

    return form







def docx_replace(form_name, rep, user, client):
    #print "WHERE WE AT?!", inspect.stack()[0]
    media_root = "{}/{}/{}".format(settings.MEDIA_ROOT, user.id, client.id)
    print("MEDIA ROOT: {}".format(media_root))
    old_file = 'static/forms/'+form_name +".docx"
    new_file = 'static/forms/'+form_name.split('.')[0] +"_" + client.last_name +"_"+str(user.id)+"_"+str(dt.datetime.now().date())+".docx"

    try:
        zin = zipfile.ZipFile (old_file, 'r')
    except(IOError):
        zin = zipfile.ZipFile ("/srv/nova/staticfiles" + old_file, 'r')

    try:
        zout = zipfile.ZipFile (new_file, 'w')
    except(IOError):
        zout = zipfile.ZipFile ("/srv/nova/staticfiles" + new_file, 'w')

    for item in zin.infolist():
        buffer = zin.read(item.filename)
        if item.filename == 'word/document.xml':
            res = buffer.decode("utf-8")
            for r in rep:
                res = res.replace('{{'+r+'}}', rep[r])
            buffer = res.encode("utf-8")
        zout.writestr(item, buffer)
    zout.close()
    zin.close()
    #print "ZOUT", dir(zout)
    #print "FILE", zout.filename
    return "static/forms/" + str(zout.filename.split('/')[-1])


def get_alerts():
    #check for overbudget contracts
    contracts = accounting.models.Contract.objects.filter(year=dt.datetime.now().year)
    users = User.objects.all()
    budgets = accounting.models.Budget.objects.filter(year=dt.datetime.now().year)
    msg = []


    for user in users:
        uc = config.models.UserConfig.objects.filter(user=user).first()
        if uc is None:
            msg.append(['/admin/config/userconfig/add/?User=%s'%(user.id), 'No User Config found for the user: %s' % (user)])

    for budget in budgets:
        for budget_item in budget.budget_item_set.all():
            percent = budget_item.get_usage()[2]
            url = '/accounting/budget/' + str(budget_item.budget.id)
            if percent > 100:
                msg.append([url, 'Budget for account %s is over limit' % (budget_item.account)])
            elif percent == 100:
                msg.append([url, 'Budget for account %s is at limit' % (budget_item.account)])
            elif percent >= 80:
                msg.append([url, 'Budget for account %s is near limit' % (budget_item.account)])



    for con in contracts:
        percent = con.get_usage()[2]
        url = 'javascript:showPopup("/accounting/contract/' + str(con.id) +'");'
        if percent > 100:
            msg.append([url, 'Contract with %s is over limit' % (con.vendor)])
        elif percent == 100:
            msg.append([url, 'Contract with %s is at limit' % (con.vendor)])
        elif percent >= 80:
            msg.append([url, 'Contract with %s is near limit' % (con.vendor)])

    #transactions missing accounts
    missing_accounts = accounting.models.Transaction.objects.filter(account=None)
    for trans in missing_accounts:
        url = 'javascript:showPopup("/accounting/transaction/{}")'.format(trans.id)
        msg.append([url, "Transaction #{} with a date of {} is missing an account".format(trans.id, trans.date)])
        #something
    if msg:
        return msg
    return ''


def delete_audit(request, instance):
    print(f"DELETE_AUDIT {type(instance)} ({instance.id})")
    check_perm(request)

    #create a new AuditTrail record and save it
    action = "DELETE"
    uc = get_user_config(request)
    audit = config.models.AuditTrail()
    serial = serializers.serialize("json", (instance,))

    audit.date_time = timezone.now()
    if uc is not None:
        audit.user_name = "%s (%s) | %s (%s)" % (str(request.user), str(request.user.id), str(uc.full_name), str(uc.id))
    else:
        audit.user_name = "Not logged in"

    audit.action_type = action
    audit.model_type = type(instance)
    audit.raw_uri = request.build_absolute_uri()
    audit.model_data = serial
    audit.ip_address = get_client_ip(request)

    audit.save()
    instance.delete()


def save_audit(request, obj):
    # Creates an audit trail item and then saves the record.
    check_perm(request)

    if hasattr(obj, "instance") and hasattr(obj.instance, "id") and obj.instance.id:
        is_new = False
    elif hasattr(obj, "id") and obj.id:
        is_new = False
    else:
        is_new = True


    record = obj.save()

    if record is None:
        # TODO figure out why this is needed in some cases.
        record = obj

    if is_new:
        print(f"SAVE_AUDIT_NEW {type(record)} ({record.id})")
        action = "NEW"
    else:
        print(f"SAVE_AUDIT_EDIT {type(record)} ({record.id})")
        action = "EDIT"



    uc = get_user_config(request)
    audit = config.models.AuditTrail()
    serial = serializers.serialize("json", (record,))

    audit.date_time = timezone.now()
    if uc is not None:
        audit.user_name = "{} ({}) | {} ({})".format(str(request.user), str(request.user.id), str(uc.full_name), str(uc.id))
    else:
        audit.user_name = "Not logged in"

    audit.action_type = action
    audit.model_type = type(record)
    audit.form_type = type(record)
    audit.raw_uri = request.build_absolute_uri()
    audit.model_data = serial
    audit.ip_address = get_client_ip(request)
    audit.generate_signature(save=True)
    audit.save()

    return record


def view_audit(request, action):
    if (request.path == '/config/audit_trail'
        or '/config/lookup' in request.path
        or 'data' in request.path):
        return

    #print(f"View audit: {action}")

    # This is a fake request item to help with the cc_reports logic
    # no need to record a view audit on this because we already record
    # the vewing of the report page and the report results when the 
    # user goes to download them
    if request.build_absolute_uri() == "requestlike":
        return

    uc = get_user_config(request)
    audit = config.models.AuditTrail()

    audit.date_time = timezone.now()
    if uc is not None:
        audit.user_name = "{} ({}) | {} ({})".format(
            request.user, request.user.id, uc.full_name, uc.id)
    else:
        audit.user_name = "Not logged in"
    audit.action_type = action
    audit.raw_uri = request.build_absolute_uri()
    audit.ip_address = get_client_ip(request) or '0.0.0.0'
    audit.generate_signature(save=True)

    audit.save()




def get_client_bills_or_payments(client_id, type, year_to,period_to):
    query = Q()

    fiscal_to = int(f"{int(year_to)}{int(period_to):02}")
    #print(f"Fiscal_to: {fiscal_to}")

    query.add(Q(fiscal__lte=fiscal_to), Q.AND)
    query.add(Q(client=client_id), Q.AND)

    if type == "bills":
        query.add(Q(client_billable=True), Q.AND)
    elif type == "payments":
        query.add( (Q(client_payment=True) | Q(atp_writeoff=True)), Q.AND)
    else:
        raise Exception('Need to specify to count either bills or payments')


    transactions = accounting.models.Transaction.objects.filter(query)

    total_debit = Decimal('0.00')
    total_credit = Decimal('0.00')

    for t in transactions:
    #    print("FISCAL YEAR {}".format(t.fiscal_year))
    #    print("FISCAL PERIOD {}".format(t.fiscal_period))

        if t.trans_type == '0': #Credit
            total_credit += t.amount
            #print(f"Credit: {total_credit}")
        elif t.trans_type == '1': #Debit
            total_debit += t.amount
            #print(f"Debit: {total_debit}")
        else:
            raise Exception("Invalid Transaction type")


    if type == "bills":
        return total_debit - total_credit
    if type == "payments":
        return total_credit - total_debit
    




def get_account_balance_date(account, datefrom=None, dateto=None):
    #calculate account's balance, dates are optional
    # Maybe in the future move this to the account model
    if datefrom is None:
        datefrom = dt.datetime(1900, 1, 1)
    if dateto is None:
        dateto = dt.datetime.now()

    transactions = accounting.models.Transaction.objects.filter(account=account, date__gte=datefrom, date__lte=dateto)

    total_debit = Decimal('0.00')
    total_credit = Decimal('0.00')

    for t in transactions:
    #   print "TransType:", t.TransType
    #   print "TransData:", t.Account, t.Description
        if t.trans_type == '0': #Credit
            total_credit += t.amount
        elif t.trans_type == '1': #Debit
            total_debit += t.amount
        else:
            raise Exception("Invalid Transaction type")

    #depending on what type of account this is, do the math
    #print "ACT TYPE", account.Type
    if account.type == '0':
        return total_credit - total_debit
    elif account.type == '1':
        return total_debit - total_credit
    else:
        raise Exception('Invalid Account type')





def get_account_balance_up_to_fiscal(account, fiscal_year, fiscal_period):
    # THIS APPEARS TO NOT BE USED ANYWHERE
    # TODO DELETE THIS ONE DAY!

    # calculate account's balance, dates are optional
    # Maybe in the future move this to the account model
    query = Q()

    query.add(Q(fiscal_year=fiscal_year), Q.AND)
    query.add(Q(fiscal_period__lte=fiscal_period), Q.AND)


    transactions = accounting.models.Transaction.objects.filter(query, account=account)

    total_debit = Decimal('0.00')
    total_credit = Decimal('0.00')

    for t in transactions:
        # print "TransType:", t.TransType
        # print "TransData:", t.Account, t.Description
        if t.trans_type == '0':  # Credit
            total_credit += t.amount
        elif t.trans_type == '1':  # Debit
            total_debit += t.amount
        else:
            raise Exception("Invalid Transaction type")

    #depending on what type of account this is, do the math
    #print "ACT TYPE", account.Type
    if account.type == '0':
        return total_credit - total_debit
    elif account.type == '1':
        return total_debit - total_credit
    else:
        raise Exception('Invalid Account type')






def get_account_balance_fiscal(account, fiscal_year=None, fiscal_period=None, make_credit_negative=False):
    # calculate account's balance, dates are optional
    # Maybe in the future move this to the account model
    query = Q()

    if fiscal_year:
        query.add(Q(fiscal_year=fiscal_year), Q.AND)
    if fiscal_period:
        query.add(Q(fiscal_period=fiscal_period), Q.AND)


    transactions = accounting.models.Transaction.objects.filter(query, account=account)

    total_debit = Decimal('0.00')
    total_credit = Decimal('0.00')

    for t in transactions:
    #   print "TransType:", t.TransType
    #   print "TransData:", t.Account, t.Description
        if t.trans_type == '0':  # Credit
            total_credit += Decimal(t.amount)
        elif t.trans_type == '1':  # Debit
            total_debit += Decimal(t.amount)
        else:
            raise Exception("Invalid Transaction type")

    #depending on what type of account this is, do the math
    #print "ACT TYPE", account.Type
    if account.type == '0':
        if make_credit_negative:
            return (Decimal(total_credit) - Decimal(total_debit)) * Decimal('-1.00')
        else:
            return Decimal(total_credit) - Decimal(total_debit)

    elif account.type == '1':
        return Decimal(total_debit) - Decimal(total_credit)
    else:
        raise Exception('Invalid Account type')


def get_agency_address(dept_name=None):
    if dept_name:
        fmt = "{}<br>{}<br>{}<br>{} {} {}".format(
            get_app_config("agency_name"),
            dept_name,
            get_app_config("agency_street"),
            get_app_config("agency_city"),
            get_app_config("agency_state"),
            get_app_config("agency_zip")
        )

    else:
        fmt = "{}<br>{}<br>{} {} {}".format(
            get_app_config("agency_name"),
            get_app_config("agency_street"),
            get_app_config("agency_city"),
            get_app_config("agency_state"),
            get_app_config("agency_zip")
        )

    return fmt






def get_fiscal_year(request=None):
    '''
        Gets the fiscal year. Uses the user's preference if possible
    '''
    if request and request.user.userconfig.fiscal_year:
        return request.user.userconfig.fiscal_year
    return int(get_app_config("fiscal_year_open_from"))


def get_fiscal_period(request=None):
    '''
        Gets the fiscal period. Uses the user's preference if possible
    '''
    if request and request.user.userconfig.fiscal_period:
        return request.user.userconfig.fiscal_period
    return int(get_app_config("fiscal_period_open_from"))


def get_fiscal_year_and_period(request=None):
    return get_fiscal_year(request), get_fiscal_period(request)


def fiscal_to_date(fiscal_year, fiscal_period):
    last_day = calendar.monthrange(int(fiscal_year), int(fiscal_period))[1]
    
    fy_date = dt.datetime(int(fiscal_year), int(fiscal_period), last_day)

    return fy_date


def get_user_config(request):
    # TODO: figure out if this function is really needed
    # 2020-8-18 update: no, this is not needed, remvoe it!!!
    # some things still call this so, you know, fix that first
    # Update 2022-02-26 yeah, this function is not needed. Clean it up!
    # Update 2023-12-27 this function is referenced in several places and
    # even though its wholly redundant, those references need to be updated
    # and tested before this can be removed.
    try:
        uc = config.models.UserConfig.objects.filter(user=request.user).first()
    except(TypeError):
        uc = None
    return uc


def get_app_config(key):
    res = get_object_or_None(config.models.AppConfig, key=key)
    if res is None:
        return None
    elif res.value.lower() == "none":
        return None
    elif res.value.lower() == "true":
        return True
    elif res.value.lower() == "false":
        return False
    else:
        return res.value


def get_app_config_object(key):
    # check if key exists, if it does not, create it
    res = get_object_or_None(config.models.AppConfig, key=key)
    if res is None:
        res = config.models.AppConfig()

    return res








def access_control_auditor(request):
    uc = request.user.userconfig
    if uc.user_type == "4" and uc.auditor_episode_type is not None:

        episode_urls = [
            "emr_episodes", "emr_form", "emr_doc", "emr_casenote",
        ]

        is_episode_url = False
        for epi_url in episode_urls:
            if epi_url in request.path:
                is_episode_url = True

        if is_episode_url is True and kwargs:
            found_epi = None

            if "episode_id" in kwargs:
                found_epi = get_object_or_None(
                    client.models.EmrEpisode, id=kwargs["episode_id"])

            elif "emr_form_id" in kwargs:
                found_form = get_object_or_None(
                    client.models.EmrForm, id=kwargs["emr_form_id"])
                if found_form:
                    found_epi = found_form.emrepisode

            elif "emr_doc_id" in kwargs:
                found_doc = get_object_or_None(
                    client.models.EmrDoc, id=kwargs["emr_doc_id"])
                if found_doc:
                    found_epi = found_doc.emrepisode

            elif "emr_casenote_id" in kwargs:
                found_note = get_object_or_None(
                    client.models.EmrCaseNoteForm, id=kwargs["emr_casenote_id"])
                if found_note:
                    found_epi = found_note.emrepisode


            # The user is viewing an episode resource and they do not have
            # that episode type specified.
            if found_epi and found_epi.episode_type != uc.auditor_episode_type:

                view_audit(request, "WARN")
                raise PermissionDenied





def access_control_userconfig(request):
    # This checks that the user's configuration is set up properly and
    # returns an intentionally generic error screen if it's not.
    uc = request.user.userconfig
    failmsg = "User not configured. Please contact an admin.<br><a href='/login'>Go to login</a>"
    if not uc:
        # User config doesn't exist for this user yet. Let them know
        # but don't provide any further information.
        view_audit(request, "UC MISSING")
        print("No UserConfig record found")
        return HttpResponse(failmsg)

    if uc.user_type is None or uc.user_type == '0':
        print("UserType not defined")
        return HttpResponse(failmsg)

    if uc.expires_at and uc.expires_at <= timezone.now().date():
        view_audit(request, "UC EXPIRED")
        return HttpResponse(failmsg)



def access_control_url_restrictions(request):
    # checks if the user is trying to access a url they have been
    # explicitly restricted from
    url_restricts = config.models.RestrictUrl.objects.filter(
        userconfig=request.user.userconfig)

    if url_restricts:
        for restrict in url_restricts:
            if restrict.url in request.path:
                view_audit(request, "WARN")
                raise PermissionDenied


def access_control_client_restricts(request, kwargs):
    uc = request.user.userconfig
    client_restricts = config.models.RestrictClient.objects.filter(
        userconfig=uc)

    if client_restricts or uc.user_type == "4":
        #print("Client_restricts")
        # Client restricts are a bit more complicated than straight URL
        # restricts. We need to not only check against the client URL,
        # but also against any notes or docs
        # First, make sure we're hitting a client-centric URL, if we
        # are then we need to check on the data/model and make sure it
        # is not restricted
        client_urls = [
            "/client/", "/client/set_active/", "/client/set_inactive/"
            "/client/client_settings/", "/client/emr_epsiodes/",
            "/client/new_emr_episode/", "/client/emr_form",
            "/client/emr_doc", "/client/emr_casenote",
            "/client/pps_episodes", "/client/case_notes_index",
            "/client/state_forms/", "/client/address/", "/client/contact/",
            "/client/assignedstaff/", "/client/casenote/",
            "/client/casenote_file/", "/client/casenote/view_all/",
            "/client/casenote/reopen/"
        ]

        is_client_url = False
        for client_url in client_urls:
            if client_url in request.path:
                is_client_url = True
                break

        if is_client_url is True and kwargs:
            # We're in a client view, so let's try to figure out
            # who the client is. Most of the views the client will
            # be the PK being passed, right?
            # TODO maybe move this to a helper function in utils
            found_client = None

            if "client_id" in kwargs:
                found_client = get_object_or_None(
                    client.models.Client, id=kwargs["client_id"])

            elif "emr_form_id" in kwargs:
                found_emrform = get_object_or_None(
                    client.models.EmrForm, id=kwargs["emr_form_id"])
                if found_emrform:
                    found_client = found_emrform.emrepisode.client

            elif "episode_id" in kwargs:
                found_epi = get_object_or_None(
                    client.models.EmrEpisode, id=kwargs["episode_id"])
                if found_epi:
                    found_client = found_epi.client

            elif "emr_casenote_id" in kwargs:
                found_casenote = get_object_or_None(
                    client.models.EmrCaseNoteForm, id=kwargs["emr_casenote_id"])
                if found_casenote:
                    found_client = found_casenote.emrepisode.client

            elif "address_id" in kwargs:
                found_address = get_object_or_None(
                    client.models.Address, id=kwargs["address_id"])
                if found_address:
                    found_client = found_address.client

            elif "contact_id" in kwargs:
                found_contact = get_object_or_None(
                    client.models.Contact, id=kwargs["contact_id"])
                if found_contact:
                    found_contact = found_contact.client

            elif "assigned_staff_id" in kwargs:
                as_found_client = get_object_or_None(
                    client.models.Client, id=kwargs["assigned_staff_id"])
                if as_found_client:
                    found_client = as_found_client

            elif "casenote_id" in kwargs:
                found_casenote = get_object_or_None(
                    client.models.CaseNote, id=kwargs["casenote_id"])
                if found_casenote:
                    found_casenote = found_casenote.client

            elif "casenote_file_id" in kwargs:
                found_casenote_file = get_object_or_None(
                    client.models.CaseNoteFile, id=kwargs["casenote_file_id"])
                if found_casenote_file:
                    found_casenote_file = found_casenote_file.case_note.client

            # TODO make reverse logic if the logged in user is an auditor
            # need to check the auditor's list of accessable clients and
            # make sure this client is part of that list
            if found_client:
                for restrict in client_restricts:
                    if restrict.client == found_client:
                        print("Restricted!")
                        view_audit(request, "WARN")
                        raise PermissionDenied

            # If  the user is an auditor we need to check and make sure
            # this found_client is in the auditor's auditor_clients list
            if uc.user_type == "4" and found_client:
                if found_client not in uc.auditor_clients.all():
                    print("Restricted!")
                    view_audit(request, "WARN")
                    raise PermissionDenied






def access_control_log_report(request):
    if request.path.startswith("/report") and request.path != "/report/":
        report = report_list.match_from_path(request.path)

        if report:
            # Log a report view. This is for tracking which reports the
            # user uses by number of uses and the last used date. The
            # idea is that in the report index we want the user to see
            # lists of their most viewed and their most recent reports

            # First, see if the user already has a record for this report, if they
            # do then update the counter and the datetime.
            # If they don't then create a new one starting at 1 and save it with datetime
            report_view = config.models.ReportView.objects.filter(
                userconfig=request.user.userconfig,
                report_url=report.url,
            ).first()

            if not report_view:
                report_view = config.models.ReportView(
                    userconfig=request.user.userconfig,
                    report_url=report.url
                )

            # I decided to actively update the report name. It's possible
            # report names might get updated or changed, but a lot less
            # likely their URLs will be changed.
            report_view.report_name = report.name
            report_view.count += 1
            report_view.last_access = timezone.now()

            report_view.save()




def access_control(*args):
    # Access control is a decorator applied to every view and it controls which
    # types of users may access views.
    # Provided args are user types and config bit fields in string format.
    # If no user types are set, then the view will default to
    #   admin, caseworker, financial
    # this is a good shortcut for a view that has a userconfig bit like the
    # user management view. We want all of the primary user accounts to be
    # able to access this view if they have the proper bit set, so only
    # the userconfig bit needs to be passed to this function.
    #
    # There are a few basic user_types:
    #   ('0', 'Inactive'),
    #   ('1', 'Admin'),
    #   ('2', 'Caseworker'),
    #   ('3', 'Financial'),
    #   ('4', "Auditor"),
    #
    # There are bit fields on the userconfig model that are used
    # to give users access to views or actions:
    #   access_settings: Grants access to the nova settings view
    #   access_restrictions: Grants access to the user restriction view
    #   access_user_mgmt: Grants access to create/remove/edit nova users easily
    #   unlock_forms: Grants ability to re-open forms and case notes
    #   edit_contracts: Grants ability to edit contracts and auths
    #   view_all_sharedlinks: Grants ability to view shared links from all users, not just the current
    #   auditor_mgmt: Grants access to the auditor management view
    #   auditor_clients: List of clients that an auditor has permission to view
    #   auditor_episode_type: List of episode types, used to restrict an auditor to a specific episode type
    #   je_unverify: Grants ability to unverify a journal entry
    #
    # The principle behind the user_types and bits is exactly the same. If
    # a user_type or bit is specified for that view, the user must have
    # that corresponding user_type AND bit set in their userconfig model,
    # otherwise we return a 403 and report the incident.
    # User_types and bits can be used independently or together.


    # This is some pre-processing of the args passed. We want to:
    #   1. Parse out the strings or numbers for the user_type or bit fields
    #   2. Make sure those user_types and bit fields exist
    #   3. Save the parsed out values into lists to be used later by wrapper()
    user_types = []
    uc_bits = []
    for _access_string in args:
        # We only accept string values
        if not isinstance(_access_string, str):
            raise Exception(f"access_control args must be strings. '{_access_string}' will not work.")

        access_string = _access_string.lower().strip()

        if access_string.isnumeric():
            user_types.append(access_string)
        else:
            # Attempt to match human-friendly user to the user_type_choices
            # in the userconfig model
            found = [
                x for x in config.models.UserConfig.user_type_choices
                if x[1].lower() == access_string.lower().strip()
            ]

            if found:
                user_types.append(found[0][0])
            else:
                # A user type matching this string hasn't been found, so it
                # may be a bit on the UserConfig model.
                bitfield = hasattr(config.models.UserConfig, access_string.lower().strip())
                if not bitfield:
                    raise Exception(f"Could not find a user_type or bit field matching '{access_string}'")
                else:
                    uc_bits.append(access_string.lower().strip())

    # No user types were supplied, that means we can use the default users
    if not user_types:
        user_types = ["1", "2", "3"]




    def decorator(view_func):

        def wrapper(request, *args, **kwargs):
            #print("Allowed user types    :", user_types)
            #print("Required user bits    :", uc_bits)
            #print("Logged in user type   :", request.user.userconfig.user_type)
            #print("Access control view   :", view_func)
            #print("Access control wrapper:", request, args, kwargs)

            # This takes any errors that are stored in the user's session
            # and adds them to the request for easier printout in the
            # templates
            if 'error' in request.session:
                request.error = request.session.pop('error')

            # if the user is not logged in. Record where they are trying to go
            # and send them to the login screen. They will get redirected
            # after they log in
            if not request.user.is_authenticated:
                #provide redirect to the login url
                view_audit(request, "LOGIN")
                return HttpResponseRedirect(f"/login/?next={request.path}")


            failmsg = access_control_userconfig(request)
            if failmsg:
                return HttpResponse(failmsg)


            # Basic access controls. If the user type is not in user_types
            # and the user doesn't have any corresponding bits set to true
            # in their userconfig then return a 403.
            if request.user.userconfig.user_type not in user_types:
                view_audit(request, "WARN")
                raise PermissionDenied

            # Loop through the userconfig bits and make sure this user has
            # a value of True set for each one
            for uc_bit in uc_bits:
                if not getattr(request.user.userconfig, uc_bit, False):
                    view_audit(request, "WARN")
                    raise PermissionDenied




            # User-specific restrictions. Admins who have the access_restrictions
            # may decide to restrict some users from a client or a URL.
            access_control_url_restrictions(request)

            # Logic for the auditor_episode_type field in the userconfig.
            # Auditors may need to be restricted to a specific episode type
            access_control_auditor(request)

            # Client restrictions can be used to prevent a user from accessing
            # information about a specific client.
            access_control_client_restricts(request, kwargs)

            # This should be moved to its own decorator but for the sake of the
            # scope of today's (2024-6-26) refactor, it will stay here.
            access_control_log_report(request)


            # Everything looks normal, so long as this path is not being excluded
            # from the audit, record the view in the audit trail.
            # Some views/requests should not be included in the audit trail
            # these are ones that repeat like the functions that check for
            # new notifications or watch report statuses
            exclude_audit = [
                "/config/ajax_check_new_notifications/",
                "/cc_report/watcher_ajax/",
            ]
            if request.path not in exclude_audit:
                view_audit(request, "VIEW")


            # TODO This line of code was carried over from the refactored
            # get_logged_in function. It's used as a shortcut to access the
            # user_type in the base.html template (and maybe others)
            request.user_type = request.user.userconfig.user_type

            return view_func(request, *args, **kwargs)


        wrapper.__doc__ = view_func.__doc__
        wrapper.__name__ = view_func.__name__
        wrapper.__get_logged_in = True
        wrapper.__access_control__ = True
        return wrapper

    return decorator







#decorator
def get_logged_in(function):
    """
        *** TODO: this function needs to be refactored ***
        This function is important! It's used as a decorator on almost every
        view in Nova. This handles routing users to where they should go as
        well as handling security and redirecting the user when they log
        in if they were logged out from inactivity
    """
    def wrap(request, *args, **kwargs):
        #print("get_logged_in {}".format(request))
        # if the session has an error, pass that to the request and clear session error
        if 'error' in request.session:
            request.error = request.session.pop('error')

        # TODO you use the error code above and create a new way to send
        # messages for example, if the user wants to clear out their report
        # history, it might be helpful for them to see a message pop up
        # and let them know instead of just having the page refresh

        if not request.user.is_authenticated:
            # The user is not logged in. Record where they are trying to go
            # and send them to the login screen. They will get redirected
            # after they log in
            view_audit(request, "VIEW")
            #provide redirect to the login url
            view_audit(request, "LOGIN")
            return HttpResponseRedirect(f"/login/?next={request.path}")




        uc = request.user.userconfig
        failmsg = "User not configured. Please contact an admin.<br><a href='/login'>Go to login</a>"

        if not uc:
            # Userconfig doesn't exist for this user yet. Let them know
            # but don't provide any further information.
            view_audit(request, "UC MISSING")
            print("No UserConfig record found")
            return HttpResponse(failmsg)

        if uc.user_type is None or uc.user_type == '0':
            print("UserType not defined")
            return HttpResponse(failmsg)

        if uc.expires_at and uc.expires_at <= timezone.now().date():
            view_audit(request, "UC EXPIRED")
            return HttpResponse(failmsg)


        #basic security redirects
        #1=Admin, 2=Caseworker, 3=Financial, 4=Auditor
        if uc.user_type == '2':
            accounting_exceptions = ["/accounting/contract/", ]

            if 'accounting' in request.path and request.path not in accounting_exceptions and request.path.startswith("/accounting/contract/") is False:
                view_audit(request, "WARN")
                raise PermissionDenied

        if uc.user_type == '3':
            if 'client' in request.path:
                view_audit(request, "WARN")
                raise PermissionDenied


        if uc.user_type == "4":
            # auditors should be restricted from account and reports views
            if 'accounting' in request.path:
                view_audit(request, "WARN")
                raise PermissionDenied

            if "report" in request.path:
                view_audit(request, "WARN")
                raise PermissionDenied



        if uc.user_type != '1':
            #nobody but admin should be in these places

            # These are the exceptions for config/ locations
            config_exceptions = [
                '/config/user_config/',
                '/config/user_config_data/', '/config/link_pl/',
                "/config/lookup/", "/config/changelog/",
                "/config/ajax_verify_password/", "/config/popup_lookup/",
                "/config/ajax_get_notifications/",
                "/config/ajax_check_new_notifications/",
                "/config/ajax_read_notification/", "/config/comment/",
                "/config/supervisor_sign/", "/config/ajax_initial_value/",
                "/config/ajax_check_warn/",
            ]

            if ('config' in request.path
                and request.path not in config_exceptions
                and request.path.startswith("/config/ajax_read_notification/") is False
                and request.path.startswith("/config/route/") is False
                and request.path.startswith("/config/ajax_route_complete/") is False
                and request.path.startswith("/config/ajax_route_uncomplete/") is False
                and request.path.startswith("/config/supervisor_sign/") is False
                and request.path.startswith("/ajax_read_notification/") is False
                and request.path.startswith("/ajax_verify_password/") is False
            ):

                view_audit(request, "WARN")
                raise PermissionDenied


        # Any explicit security settings can be set here
        # Check if the user has access to the Nova settings page
        if uc.access_settings is not True:
            if request.path == "/config/app_config/":
                view_audit(request, "WARN")
                raise PermissionDenied

        if uc.access_user_mgmt is not True:
            if "/config/user_mgmt/" in request.path:
                view_audit(request, "WARN")
                raise PermissionDenied


        # Check if the user has access to the user access restrictions page
        if uc.access_restrictions is not True:
            user_access_urls = [
                "/config/user_restrict_index",
                "/config/restrict_client",
                "/config/restrict_url",
            ]
            for ua_url in user_access_urls:
                if ua_url in request.path:
                    view_audit(request, "WARN")
                    raise PermissionDenied

        # User-specific restrictions. Admins who have the access_restrictions
        # may decide to restrict some users from a client or a URL.
        url_restricts = config.models.RestrictUrl.objects.filter(
            userconfig=uc)

        if url_restricts:
            #print("Url restricts")
            for restrict in url_restricts:
                if restrict.url in request.path:
                    view_audit(request, "WARN")
                    raise PermissionDenied




        # Client restrictions can be used to prevent a user from accessing
        # information about a specific client.
        client_restricts = config.models.RestrictClient.objects.filter(
            userconfig=uc)

        if uc.user_type == "4" and uc.auditor_episode_type is not None:
            # Logic for the auditor_episode_type field in the userconfig.
            # Auditors may need to be restricted to a specific episode type
            episode_urls = [
                "emr_episodes", "emr_form", "emr_doc", "emr_casenote",
            ]

            is_episode_url = False
            for epi_url in episode_urls:
                if epi_url in request.path:
                    is_episode_url = True

            if is_episode_url is True and kwargs:
                found_epi = None

                if "episode_id" in kwargs:
                    found_epi = get_object_or_None(
                        client.models.EmrEpisode, id=kwargs["episode_id"])

                elif "emr_form_id" in kwargs:
                    found_form = get_object_or_None(
                        client.models.EmrForm, id=kwargs["emr_form_id"])
                    if found_form:
                        found_epi = found_form.emrepisode

                elif "emr_doc_id" in kwargs:
                    found_doc = get_object_or_None(
                        client.models.EmrDoc, id=kwargs["emr_doc_id"])
                    if found_doc:
                        found_epi = found_doc.emrepisode

                elif "emr_casenote_id" in kwargs:
                    found_note = get_object_or_None(
                        client.models.EmrCaseNoteForm, id=kwargs["emr_casenote_id"])
                    if found_note:
                        found_epi = found_note.emrepisode


                # The user is viewing an episode resource and they do not have
                # that episode type specified.
                if found_epi and found_epi.episode_type != uc.auditor_episode_type:
                    print("Restricted!")
                    view_audit(request, "WARN")
                    raise PermissionDenied









        if client_restricts or uc.user_type == "4":
            #print("Client_restricts")
            # Client restricts are a bit more complicated than straight URL
            # restricts. We need to not only check against the client URL,
            # but also against any notes or docs
            # First, make sure we're hitting a client-centric URL, if we
            # are then we need to check on the data/model and make sure it
            # is not restricted
            client_urls = [
                "/client/", "/client/set_active/", "/client/set_inactive/"
                "/client/client_settings/", "/client/emr_epsiodes/",
                "/client/new_emr_episode/", "/client/emr_form",
                "/client/emr_doc", "/client/emr_casenote",
                "/client/pps_episodes", "/client/case_notes_index",
                "/client/state_forms/", "/client/address/", "/client/contact/",
                "/client/assignedstaff/", "/client/casenote/",
                "/client/casenote_file/", "/client/casenote/view_all/",
                "/client/casenote/reopen/"
            ]

            is_client_url = False
            for client_url in client_urls:
                if client_url in request.path:
                    is_client_url = True
                    break

            if is_client_url is True and kwargs:
                # We're in a client view, so let's try to figure out
                # who the client is. Most of the views the client will
                # be the PK being passed, right?
                # TODO maybe move this to a helper function in utils
                found_client = None

                if "client_id" in kwargs:
                    found_client = get_object_or_None(
                        client.models.Client, id=kwargs["client_id"])

                elif "emr_form_id" in kwargs:
                    found_emrform = get_object_or_None(
                        client.models.EmrForm, id=kwargs["emr_form_id"])
                    if found_emrform:
                        found_client = found_emrform.emrepisode.client

                elif "episode_id" in kwargs:
                    found_epi = get_object_or_None(
                        client.models.EmrEpisode, id=kwargs["episode_id"])
                    if found_epi:
                        found_client = found_epi.client

                elif "emr_casenote_id" in kwargs:
                    found_casenote = get_object_or_None(
                        client.models.EmrCaseNoteForm, id=kwargs["emr_casenote_id"])
                    if found_casenote:
                        found_client = found_casenote.emrepisode.client

                elif "address_id" in kwargs:
                    found_address = get_object_or_None(
                        client.models.Address, id=kwargs["address_id"])
                    if found_address:
                        found_client = found_address.client

                elif "contact_id" in kwargs:
                    found_contact = get_object_or_None(
                        client.models.Contact, id=kwargs["contact_id"])
                    if found_contact:
                        found_contact = found_contact.client

                elif "assigned_staff_id" in kwargs:
                    as_found_client = get_object_or_None(
                        client.models.Client, id=kwargs["assigned_staff_id"])
                    if as_found_client:
                        found_client = as_found_client

                elif "casenote_id" in kwargs:
                    found_casenote = get_object_or_None(
                        client.models.CaseNote, id=kwargs["casenote_id"])
                    if found_casenote:
                        found_casenote = found_casenote.client

                elif "casenote_file_id" in kwargs:
                    found_casenote_file = get_object_or_None(
                        client.models.CaseNoteFile, id=kwargs["casenote_file_id"])
                    if found_casenote_file:
                        found_casenote_file = found_casenote_file.case_note.client

                # TODO make reverse logic if the logged in user is an auditor
                # need to check the auditor's list of accessable clients and
                # make sure this client is part of that list
                if found_client:
                    for restrict in client_restricts:
                        if restrict.client == found_client:
                            print("Restricted!")
                            view_audit(request, "WARN")
                            raise PermissionDenied

                # If  the user is an auditor we need to check and make sure
                # this found_client is in the auditor's auditor_clients list
                if uc.user_type == "4" and found_client:
                    if found_client not in uc.auditor_clients.all():
                        print("Restricted!")
                        view_audit(request, "WARN")
                        raise PermissionDenied






        # We only want admins to be able to unverify journal entries
        # This may get moved to a setting at some point
        if request.path.startswith("/accounting/journal_entry/unverify/"):
            if uc.user_type != '1':
                view_audit(request, "WARN")
                raise PermissionDenied


        if request.path.startswith("/report") and request.path != "/report/":

            report = report_list.match_from_path(request.path)

            if report:
                # Log a report view. This is for tracking which reports the
                # user uses by number of uses and the last used date. The
                # idea is that in the report index we want the user to see
                # lists of their most viewed and their most recent reports

                # First, see if the user already has a record for this report, if they
                # do then update the counter and the datetime.
                # If they don't then create a new one starting at 1 and save it with datetime
                report_view = config.models.ReportView.objects.filter(
                    userconfig=request.user.userconfig,
                    report_url=report.url,
                ).first()

                if not report_view:
                    report_view = config.models.ReportView(
                        userconfig=request.user.userconfig,
                        report_url=report.url
                    )

                # I decided to actively update the report name. It's possible
                # report names might get updated or changed, but a lot less
                # likely their URLs will be changed.
                report_view.report_name = report.name
                report_view.count += 1
                report_view.last_access = timezone.now()

                report_view.save()







        # Everything looks normal, so long as this path is not being excluded
        # from the audit, record the view

        exclude_audit = [
            "/config/ajax_check_new_notifications/"
        ]
        if request.path not in exclude_audit:
            view_audit(request, "VIEW")


        request.user_type = uc.user_type
        return function(request, *args, **kwargs)



    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    wrap.__get_logged_in = True
    return wrap


def get_client_ip(request):
    x_forwarded_for = request.headers.get('x-forwarded-for')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def format_errors(request, form=None, custom_error=None, custom_redirect=None):
    if custom_error:
        # If the custom error contains a dict with values and errors
        # then we can highlight specific fields even if it's not a
        # form-generated error list. The msg key should contain a list of
        # error messages. The code here formats the custom errors in the
        # same way that django does for its forms so they will work with
        # all the existing code that formats them on the templates.
        # the values key should contain a dict of fields and their values
        # and the errors should contain a dict of fields and a list of their
        # validation errors
        if isinstance(custom_error, dict):
            instance_errors = ""

            for field, error_list in custom_error["errors"].items():
                field_errors = ""
                for error_msg in error_list:
                    field_errors += f"<li>{error_msg}</li>"

                instance_errors += f"<li>{field}<ul class='errorlist'>{field_errors}</ul></li>"

            error_msg_html = f"<ul class='errorlist'>{instance_errors}</ul>"

            request.session['error'] = {
                'msg': error_msg_html,
                'values': custom_error["values"],
                "errors": custom_error["errors"]
            }

        else:
            request.session['error'] = {'msg': custom_error, 'values': None}
    elif form:
        # build a serializable list of the form's fields and their value
        # to be passed back to the form so the user doesn't have to refill
        data = {}


        # using form.data instead of form.cleaned_data gives us all the field
        # values (including ones with validation errors) so we can re-fill the
        # field values on the form
        for key, value in form.data.items():
            # If the key is something we don't need to re-fill, like the token
            # we can skip that.
            if key == "csrfmiddlewaretoken":
                continue

            # If this key doesn't exist as one of the form's fields, we
            # can skip that, too.
            if key not in form.fields:
                print(f"Cannot format error for form field '{key}' as it's not in the fields for '{form}'")
                continue


            if value is None:
                value = ''

            # if the value is a class, return the ID instead
            if issubclass(type(value), models.Model):
                data[key] = str(value.id)
            else:
                if isinstance(value, dt.date):
                    # The html date field needs to be in this format, even
                    # though it displays in mm/dd/yyyy
                    data[key] = value.strftime("%Y-%m-%d")
                else:
                    data[key] = str(value)
                    #print(f"{key}: {value}")

            # Rename the ALL form errors to the name of the model
            if '__all__' in form.errors:
                form.errors[form.Meta.model.__name__] = form.errors['__all__']
                form.errors.pop('__all__')


        request.session['error'] = {'msg': form.errors.as_ul(), 'values': data, "errors": form.errors}
    else:
        request.session['error'] = {'msg': "Unknown error, please contact support.", 'values': None}

    if custom_redirect:
        return HttpResponseRedirect(custom_redirect)
    else:
        return HttpResponseRedirect(request.get_full_path())


def save_confirm(request, instance, path_override=None, popup_override=None):
    """ Redirects to existing path or override. Saves history item in session """
    if path_override:
        redirect = path_override
    else:
        if request.path.endswith("new/"):
            redirect = request.path.replace('new/', instance.id.__str__())
        elif request.path.endswith("new"):
            redirect = request.path.replace('new', instance.id.__str__())
        else:
            redirect = request.path

    model_name = ContentType.objects.get_for_model(instance).model
    # Renaming model names to make it easier for users to understand
    if model_name == 'AppConfig':
        model_name = 'NOVA Config'


    if request.POST.get('popup') == 'true' or popup_override:
        # This popup value is not the same as popup == popup. This is set
        # in the form to tell save_confirm, not for front-end styling
        # Crosswalk? to the parent record and then show the popup
        # need for: Address, Contact, ID Number, Assigned Staff,
        # Budget Item, Journal Transaction

        # As of 2022-10-04 I've disabled the session history in base.html
        # TODO remove or comment out all the session history code to save
        # CPU cycles...


        app_label = ContentType.objects.get_for_model(instance).app_label
        print("MODEL NAME {}".format(model_name))
        # minor cleanup where model names don't quite match the urls
        if model_name == 'budgetitem':
            model_name = 'budget_item'
        elif model_name == 'contactinfo':
            model_name = 'contact_info'
        elif model_name == 'assignedstaff':
            model_name = 'assigned_staff'
        elif model_name == 'casenotefile':
            model_name = 'casenote_file'
        elif model_name == 'transaction' and hasattr(instance, 'journal_entry') and instance.journal_entry:
            model_name = 'journal_trans'
        elif model_name == "abilitytopay":
            model_name = "atp"
            app_label = "client"  # atp is an accounting model so also update app label so we can still use the url in the client app


        raw_href = "/{0}/{4}/{3}?history_popup=/{0}/{1}/{2}".format(app_label, model_name, instance.id, "{0}", "{1}")

        if model_name in ('address', 'contact', 'assignedstaff', "atp"):
            # Items that are children of client don't need to include parent name, because the parent is the same as the app
            raw_href = "/{0}/{3}?history_popup=/{0}/{1}/{2}".format(app_label, model_name, instance.id, "{0}")
            href = raw_href.format(instance.client.id)  # parent id
        elif model_name == 'casenote_file':
            href = raw_href.format(instance.case_note.id, "casenote")  # parent id, parent name
        elif model_name == 'budget_item':
            href = raw_href.format(instance.budget.id, 'budget')  # parent id, parent name
        elif model_name == 'journal_trans':
            href = raw_href.format(instance.journal_entry.id, 'journal_entry')  # parent id, parent name
        else:
            href = "javascript:showPopup('{}')".format(redirect)

        response = render(request, 'save_confirm.html', {
            'object': model_name,
            'popup': 'popup',
            'redirect': redirect,
        })

    else:
        href = redirect
        response = HttpResponseRedirect(redirect)

    if 'history' not in request.session:
        request.session['history'] = []
    # prepend the session action history, limit to 20 items (maybe make this a setting later)
    # expand this in the future to say stuff like "Saved Address for Client: blah blah"
    print("SAVING HREF = {}".format(href))
    history_item = {
        'action': 'Saved',
        'model': model_name,
        'name': instance.__str__(),
        'time': dt.datetime.now().time().strftime(settings.TIME_FORMAT),
        'href': href,
    }

    request.session['history'] = [history_item] + request.session['history'][:20]
    return response


def delete_confirm(request, instance=None, path_override=None):
    """Redirects to existing path or override. Use override if not popup"""
    if path_override:
        redirect = path_override
    else:
        redirect = request.path

    if request.POST.get('popup') == 'true':
        response = render(request, 'delete_confirm.html', {
            'objecttype': str(type(instance)),
            'popup': request.POST.get('popup'),
        })
    else:
        response = HttpResponseRedirect(redirect)

    model_name = str(type(instance)).split(".")[-1].replace("'>", '')

    if 'history' not in request.session:
        request.session['history'] = []
    # prepend the session action history, limit to 20 items (maybe make this a setting later)
    # expand this in the future to say stuff like "Saved Address for Client: blah blah"
    history_item = {
        'action': 'Deleted',
        'model': model_name,
        'name': instance.__str__(),
        'time': dt.datetime.now().time().strftime(settings.TIME_FORMAT),
        #'href': href,
    }
    request.session['history'] = [history_item] + request.session['history'][:20]

    return response



