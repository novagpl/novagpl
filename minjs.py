"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import sys
import datetime
import jsmin

def minify_js(file_path):
    # Open the original file
    with open(file_path, 'r') as file:
        original_code = file.read()

    # Minify the code
    minified_code = jsmin.jsmin(original_code)

    # Write the minified code to the same file
    with open(f"{file_path.replace('.js', '')}.min.js", 'w') as file:
        file.write(minified_code)






if __name__ == "__main__":
    start = datetime.datetime.now()

    if len(sys.argv) != 2:
        print("The only argument must be a filename")


    else:
        minify_js(sys.argv[1])




    print(f"Operation complete. Total time: {datetime.datetime.now() - start}")





