# NovaGPL

NovaGPL (also called just 'Nova') is an Open Source Software as a Service (SaaS) designed to address the unique challenges faced by county governments in Wisconsin.

NovaGPL is developed using CI/CD techniques and new features are constantly being added.

---

## Features
NovaGPL is constantly improved via CI/CD practices, updates are frequent. A list of categorized features as of 2023-09-20 follow below.
*Please note: This list is not comprehensive and may be missing some features*

### Web-based
 - Can be deployed internally or externally
 - Updates and bug fixes are automatic and require no intervention from IT or users
 - Cross-platform, will work in any modern web browser
    - Windows, Apple, Android, Linux
    - Firefox, Chrome, Edge, Safari
 - Users can access Nova in the field with cellular internet
 - Users can share links to locations in Nova via email or instant message without exposing PHI

 ### User Interface
 - Autocomplete lookups
 - Most views with lists are easily searchable without the need for a report
 - Uncomplicated interface that follows modern design patterns
 - Dark mode option






### Access Management
- User Management System
    - Role-based access
        - Active/Inactive users
        - Caseworker
        - Financial
        - Admin
        - View-only
    - Explicitly set access per-user for advanced features
        - Access to user restrictions
        - Access to user management
        - Ability to unlock finalized forms and case notes
        - Ability to view and manage all externally shared links
        - Ability to edit contracts and authorizations
        - Access to auditor management
        - Ability to assign users as 'supervisor signers' on a per-episode basis
    - User Restrictions
        - Restrict a user from accessing data for individual clients
        - Restrict a user from accessing any individual page in Nova
- Auditor access
    - Auditors can be give access to specific clients and be limited to a specific episode type
    - Auditor accounts automatically expire
    - Auditors accounts are view-only and may not make any changes to the data
- Idle logout timer logs uses out after 15 minutes of inactivity
- Incident reporting: when a user receives an access denied error detailed emails are automatically sent to admin
- Audit Trail tracks all views, edits, and access denied incidents


### Real-Time Error Reporting
- Error reports are sent to administrators the moment they are encountered


### Form Routing and Workflows
- Routes are workflows for forms and case notes in Nova
- Users may assign routes to each other with different action types
    - Some actions are completed automatically
        - Finalizing or signing forms
        - Request for supervisor signature when form is finalized or closed
    - Some actions are completed manually
        - Responding to requests to review forms and case notes


### Notifications and Alerts
- Intuitive notification interface and new notification indicators
- Notifications contain links to referenced screens in Nova
- Users receive notifications when actions are completed in Nova or actions are required of the user





### Accounting
- Ability-to-pay and voucher printout
- Financial export to county systems
- Chart of Accounts
- Rates by year and episode type
- Budget by year
- Contract
    - Tracks provider, services, and authorizations
    - Shows usage summary and utilization percentages by service and authorization
- Payroll Distribution
- Transaction
    - Payroll transactions
    - Client payments
    - ATP Auto Discharge
    - Easy grouping and selection of transactions to be added to journal entries
    - Incorrect account type warnings
- Transaction Batches
- Journal Entry
    - Auto-balancing offset option
    - Unequal balance indicator
    - Easily add existing transactions
    - Export journal entry directly to the county system's format
- Funding Profile





### Client Management
- Episode-based case noting
    - Clinic and Clinic Pre-Admit
    - CLTS and CLTS Pre-Admit
    - Crisis
    - CST
    - Referral and Intake
    - TCM
    - Birth to 3
- Client face sheet and demographics
    - Support for different address types (billing, mailing, physical, homeless)
    - Tracking of additional client contacts by type
    - Insurance information
- Support for client past legal names
- Duplicate user detection and hints
    - Detects client duplicates by name, related names, previous names, and birthdate
- Digital forms and case notes
    - Export forms to PDF and Excel formats
    - Auto-save feature automatically saves forms and case notes as they are being edited
    - Automatic unit calculation
    - Forms and case notes can be digitally signed
    - Validation of forms and case notes
    - Closing billable case notes can generate transactions depending on rate
    - Case note boilerplate text can be pre-filled depending on what data is put into what fields
    - Document upload
        - Upload any type of scanned document or digital file directly to a client's chart
        - Ability to attach uploaded documents to digital forms
- Shared Links
    - Users may share documents and forms to external parties
    - Tracks access and views
    - Automatic expiration date
- Supervisor Signers
    - Set specific forms and case notes to require a supervisor signature in addition to the caseworker's
    - When a form is finalized or closed a route is automatically created to alert a supervisor to review and provide their signature on the form
- Electronic Signatures
    - Topaz signature pads
    - Tablet or mouse
    - Simple typed




### Reports
- Data in reports is clickable, allowing users to navigate to the record shown in the report
- Export reports to PDF or CSV (Excel)
- Export case notes and forms in report format to PDF
- Financial reports
    - AP/AR County
    - Budget
    - Client Statement
    - Contract
    - DCS 942
    - Funding Profile
    - Transaction
    - Trial Balance
- Program reports
    - Admission
    - Client
    - Clinic Pre-Admit
    - Current Roster
    - Discharge
    - Case Note
    - Case Note Time
    - Open Case Notes for the currently logged in user
    - Length of Service
    - Referral and Intake external and internal referral reports






