"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 4.2.1 on 2023-06-01 17:12

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("external", "0004_sharedlinkaccess_userconfig"),
    ]

    operations = [
        migrations.AddField(
            model_name="sharedlinkaccess",
            name="downloaded_file",
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name="sharedlinkaccess",
            name="file_name",
            field=models.TextField(null=True),
        ),
    ]
