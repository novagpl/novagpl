"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from django.db import models
from django.utils import timezone
from django.apps import apps


from utils import get_client_ip








class SharedLink(models.Model):
    uuid = models.UUIDField(primary_key=False)
    created_by = models.ForeignKey("config.UserConfig", related_name="sharedlink_created_by", on_delete=models.PROTECT)
    created_at = models.DateField()
    revoked_at = models.DateField(null=True)
    revoked_by = models.ForeignKey("config.UserConfig", related_name="sharedlink_revoked_by", null=True, on_delete=models.PROTECT)
    expires_at = models.DateField()
    app_name = models.CharField(max_length=255, null=True)
    model_name = models.CharField(max_length=255, null=True)
    ref_id = models.IntegerField()



    def can_access(self, provide_reason=False):
        access_result = False
        deny_reason = None

        if self.revoked_at is not None:
            access_result = False
            deny_reason = "revoked"

        elif timezone.now().date() >= self.expires_at:
            access_result = False
            deny_reason = "expired"

        else:
            access_result = True



        if provide_reason is True:
            return access_result, deny_reason
        else:
            return access_result



    def get_status(self):
        if self.revoked_at is not None:
            return "Revoked"
        elif timezone.now().date() >= self.expires_at:
            return "Expired"
        else:
            return "Active"


    def get_access_history(self):
        found = self.sharedlinkaccess_set.all().order_by("-accessed_at")

        return found


    def get_external_viewed(self):
        # See if there are any access logs that are not views from the
        # person that created the link
        found = self.sharedlinkaccess_set.filter(userconfig=self.created_by)

        if found:
            return True
        else:
            return False



    def get_ref_instance(self):
        if self.app_name and self.model_name:
            model = apps.get_model(self.app_name, self.model_name)
            return model.objects.get(id=self.ref_id)
        else:
            return None


    def get_client(self):
        # Depending on the type of app + model, figure out how to get
        # client model
        if self.app_name == "client" and self.model_name == "emrdoc":
            return self.get_ref_instance().emrepisode.client

        else:
            return None


    def get_link_number(self):
        # This is to provide a way to identify links of there are multiples
        # of the same type. It just gives back a number to put in front of
        # the links when they are in a list view or something similar
        # This only returns active links
        found = SharedLink.objects.filter(
            app_name=self.app_name,
            model_name=self.model_name,
            ref_id=self.ref_id,
            revoked_at__isnull=True,
            expires_at__gt=timezone.now().date(),
        ).order_by("-created_at").values("id")

        found_list = [x["id"] for x in found]

        # Adding one because users don't expect things to start at 0
        return found_list.index(self.id) + 1



    def log_access(self, request, filename=None, warning=False):
        access = SharedLinkAccess(
            sharedlink=self,
            ip_address=get_client_ip(request) or '0.0.0.0',
            accessed_at=timezone.now(),
        )

        if request.user.is_anonymous is False:
            access.userconfig = request.user.userconfig

        if filename:
            access.filename = filename
            access.downloaded_file = True

        access_result, reason = self.can_access(provide_reason=True)
        if access_result is not True:
            access.denied = True
            access.deny_reason = reason

        access.save()




class SharedLinkAccess(models.Model):
    deny_choices = (
        ("expired", "Expired"),
        ("revoked", "Revoked"),
    )

    sharedlink = models.ForeignKey("external.SharedLink", on_delete=models.PROTECT)
    accessed_at = models.DateTimeField()
    ip_address = models.GenericIPAddressField()
    denied = models.BooleanField(default=False)
    deny_reason = models.CharField(max_length=255, choices=deny_choices)
    userconfig = models.ForeignKey("config.UserConfig", null=True, blank=True, on_delete=models.PROTECT)
    downloaded_file = models.BooleanField(default=False)
    filename = models.TextField(null=True)

    def get_result(self):
        # Returns a human-readable string if the access was granted or not
        if self.denied is True:
            return f"Deny - {self.get_deny_reason_display()}"
        else:
            if self.downloaded_file is True:
                return "File downloaded"
            else:
                return "View"
