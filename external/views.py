"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import uuid
import os

from django.shortcuts import render
from django.http import HttpResponseRedirect, FileResponse
from django.utils import timezone
from django.apps import apps
from django.conf import settings
from django.shortcuts import get_object_or_404



from utils import get_client_ip, view_audit, get_logged_in, get_agency_address
from permissions import warn

from .models import SharedLink, SharedLinkAccess
from client.models import EmrDoc



@get_logged_in
def create_sharedlink(request, app_name, model_name, link_id):
    tz_now = timezone.now()
    tz_expires = tz_now + timezone.timedelta(days=30)

    link = SharedLink(
        model_name=model_name,
        app_name=app_name,
        ref_id=link_id,
        created_by=request.user.userconfig,
        created_at=tz_now,
        expires_at=tz_expires,
        uuid=uuid.uuid4(),
    )

    link.save()

    return HttpResponseRedirect(request.headers.get("referer"))




# User does not need to be logged in for this view
def sharedlink(request, uuid):
    # Normal view for an external visitor using a shared link
    instance = None
    link = SharedLink.objects.get(uuid=uuid)

    link.log_access(request)



    if link.can_access() is True:
        if link.app_name and link.model_name:
            instance = apps.get_model(
                link.app_name, link.model_name).objects.get(id=link.ref_id)
        else:
            # TODO if there are sharedlinks that don't correspond directly to a
            # app + model then they can be figured out here
            pass


        return render(request, "external/sharedlink_view.html", {
            "link": link,
            "instance": instance,
            "agency_address": get_agency_address(dept_name=None),

        })

    else:
        return render(request, "external/sharedlink_expired.html", {

        })






# User does not need to be logged in for this view
def download_sharedlink_file(request, uuid):
    # Download interface for getting the file
    # 1. check that the shared link being requested has can_access() == True
    # 2. Serve up the file
    link = SharedLink.objects.get(uuid=uuid)


    if link.can_access() is True:
        instance = apps.get_model(
                link.app_name, link.model_name).objects.get(id=link.ref_id)

        if link.model_name == "emrdoc":
            file_path = os.path.join(settings.MEDIA_ROOT, f"{instance.doc_file.name}")

            link.log_access(request, filename=instance.doc_file.name)

            if os.path.exists(file_path):
                return FileResponse(open(file_path, "rb"))

        else:
            raise Exception(f"{link.model_name} is not implemented for downloads")
    else:
        link.log_access(request)


        return render(request, "external/sharedlink_expired.html", {

        })




@get_logged_in
def manage_sharedlinks(request):
    # Only show shared links for the logged in user unless they have the
    # view_all_sharedlinks permission
    if request.user.userconfig.view_all_sharedlinks is True:
        links = SharedLink.objects.all().order_by("-created_at")
    else:
        links = SharedLink.objects.filter(created_by=request.user.userconfig).order_by("-created_at")


    return render(request, "external/sharedlink_mgmt.html", {
        "title": "Shared Link Management",
        "links": links,
    })



@get_logged_in
def revoke_sharedlink(request, link_id):
    # WHEN REVOKING THE EXPIRED_AT MUST BE SET TO THE SAME AS THE REVOKE TIME
    # unless user is admin, they can only delete their own shared links!
    link = get_object_or_404(SharedLink, id=link_id)

    if request.user.userconfig.view_all_sharedlinks is not True:
        if request.user.userconfig != link.created_by:
            return HttpResponseRedirect("/external/sharedlink/manage/")

    tz_now = timezone.now()

    link.revoked_at = tz_now
    link.revoked_by = request.user.userconfig
    link.expires_at = tz_now

    link.save()

    return HttpResponseRedirect("/external/sharedlink/manage/")


@get_logged_in
def sharedlink_log_index(request, link_id):
    # This view displays the access log for the selected shared link.
    # It's  a simple view with no extra functions
    # Need to make sure that users can only view logs from their own shared
    # links unless they have the view_all_sharedlinks permission
    link = get_object_or_404(SharedLink, id=link_id)

    if request.user.userconfig.view_all_sharedlinks is not True:
        if request.user.userconfig != link.created_by:
            return HttpResponseRedirect("/external/sharedlink/manage/")

    popup = ""
    if request.GET.get('popup') and request.GET.get('popup') == "true":
        popup = "popup"

    log = SharedLinkAccess.objects.filter(sharedlink=link).order_by("-accessed_at")

    return render(request, "external/sharedlink_log_index.html", {
        "title": "Shared Link Access Log",
        'popup': popup,
        "link": link,
        "log": log,
        "instance": link.get_ref_instance(),

    })
