"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from django.urls import path

from . import views


app_name = 'external'

urlpatterns = [
    path("sharedlink/create/<str:app_name>/<str:model_name>/<int:link_id>/", views.create_sharedlink, name="create_sharedlink"),
    path("sharedlink/manage/", views.manage_sharedlinks, name="manage_sharedlink"),
    path("sharedlink/download/<str:uuid>/", views.download_sharedlink_file, name="download_sharedlink_file"),
    path("sharedlink/view/<str:uuid>/", views.sharedlink, name="sharedlink"),
    path("sharedlink/revoke/<int:link_id>/", views.revoke_sharedlink, name="revoke_sharedlink"),
    path("sharedlink/log/<int:link_id>/", views.sharedlink_log_index, name="sharedlink_log"),
]
