"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from django.test import TestCase

from .urls import urlpatterns




class GetNotLoggedInViewTests(TestCase):
    print("Testing redirect for not logged in report URLs")
    # Pretty much all of the normal urls should return a 302

    def setUp(self):
        pass



not_logged_in_urls = [
    "sharedlink/create/app-name/model-name/1",
    "sharedlink/manage",
    "sharedlink/revoke/1",
]

def create_not_logged_in_test_method(url):
    def test_to_add(self):
        response = self.client.get(f"/external/{url}/")
        self.assertEqual(response.status_code, 302)

    return test_to_add

# Build methods for each URL to test so the output is easier to read
# This is dynamically adding methods to the test case class, pretty neat
for url in not_logged_in_urls:
    test_method = create_not_logged_in_test_method(url)
    test_method.__name__ = f"test_not_logged_in_redirect__{url.replace('/', '-')}"
    setattr(GetNotLoggedInViewTests, test_method.__name__, test_method)



class GetLoggedInDecoratorTest(TestCase):
    print("Checking that all required views have the @get_logged_in decorator")
    # Loops through all the urlpatterns for this app and checks that each
    # function is properly wrapped by the get_logged_in decorator
    # Some views (external ones probably) may not need the get_logged_in wrapper
    # so they can be put into the get_logged_in_exceptions list

    def setUp(self):
        pass


def create_get_logged_in_test_methods(url):
    def get_logged_in_test_to_add(self):
        found = getattr(url.callback, "__get_logged_in", False)
        self.assertEqual(found, True)

    return get_logged_in_test_to_add


get_logged_in_exceptions = [
    # shared links are supposed to be accessed non logged in users
    "sharedlink",
    "download_sharedlink_file"
]
for url in urlpatterns:
    if url.name in get_logged_in_exceptions:
        continue

    test_method = create_get_logged_in_test_methods(url)
    test_method.__name__ = f"test_get_logged_in_decorator_exists__{url.name}"
    setattr(GetLoggedInDecoratorTest, test_method.__name__, test_method)



