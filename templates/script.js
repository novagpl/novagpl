/*

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/


//{% load static %}
//{% load nova_tags %}

var placeSearch, autocomplete;
var state = '';
var street_number = '';
var street = '';
var city = '';
var zip = '';

var EMR_FORM_ID = null;
var TWO_COLUMNS = [];
var EMR_FORM_IS_FINALIZED = false;
var USER_CAN_UNLOCK = false;

var AUTOCOMPLETE_DELAY = 200;
var EASE_IN = 100;
var EASE_OUT = 400;
var EMR_FORM_ID = null;
var EMR_FORM_AUTOSAVE_INTERVAL = 10000;
var EMR_FORM_IS_CLOSED = false;


$(document).ready(function(){
    datatables();
    render_autocomplete();
//    initAutocomplete(); //maps api
    renderInputWidths();
    clear_form();
    multi_select();
    date_picker();
    phoneMask();
    time_24hr_helper();
    render_fileupload();
    //render_history();  // this is not in use any more
    render_popup();
    renderFormSubmit();
    renderModalSubmit();
    render_optional();
    renderFormCheckbox();
    history_popup();
    renderTableSearch();

    formatExpandFields();
    renderChkToggleRelated();
    renderSelectShow();
    //createFormIndex();  // moved to JS base
    //renderTwoColumns();  // moved to JS base
    renderSelectMultiple();
    renderDatalist();
    renderLowercaseInput();

    scrollToResults();
    checkForNewNotifications();

    setToggleElements();

    //renderNoEdit();  This is to be called on the template itself. look at the client_edit template
    // render_anchor_tags(); disabled for now. TODO disable this forever?



    $('.popup-wrapper').draggable();


    $(".validation-error").focusin(function(){
        $(this).siblings("label").removeClass('validation-error');
        $(this).removeClass('validation-error');
    });

    $('body').css('visibility','visible');

    //navigateToAnchor(); // moved to JS base

    focusFirstField(); //keep this last


});



function setToggleElements() {
    let toggleEles = document.querySelectorAll(".toggle");
    toggleEles.forEach(function(ele) {
        ele.addEventListener("click", function() {
            document.getElementById(
                ele.getAttribute("data-toggle-element")).classList.toggle("toggled");
        });
    });
}




function checkForTransTypeMismatch(element) {
    if (element.attr("id") == "id_trans_type"){
        var target_to_revert = $(this);
        var value_to_revert_to = $(this).attr("auto-update-value");
        window.parent.showModal(
            titleText="Credit/Debit Mismatch",
            bodyText="The transaction type and the account type do not match. Are you sure you want to continue?",
            primaryText="Go back",
            secondaryText="Keep mismatch",
            primaryAction=function(){
                target_to_revert.val(value_to_revert_to);
                $("label[for="+ target_to_revert.attr("id")+"]").removeClass("changed-from-auto");
                window.parent.hideModal();
                return;
            },
            secondaryAction=window.parent.hideModal,
            opacity=0.8
        )
    }
}



function ajaxInitialValue(formName, instanceName, instanceId, sourceFieldId, warnOnChange) {
    const sourceField = document.getElementById(sourceFieldId);

    if (!sourceField) {
        console.log("Source field '" + sourceFieldId + "' does not exist");
        return;
    }

    sourceField.addEventListener("change", function(e) {
        //console.log("changed value on ", sourceFieldId, "to", e.target.value);

        $.ajax({
            method: "POST",
            url: "/config/ajax_initial_value/",
            data: {
                csrfmiddlewaretoken: "{{ csrf_token }}",
                instance_name: instanceName,
                instance_id: instanceId,
                form_name: formName,
                source_field: sourceFieldId,
                source_value: e.target.value,
            },
            success: function(data) {
                console.log(data);

                for(var key in data) {
                    const value = data[key];

                    let targetEle = document.getElementById(key);
                    if (targetEle) {
                        targetEle.value = value;
                    }
                }

                // TODO something to indicate to the user that the values
                // on the fields have been changed or auto-added

                // TODO logic so that if the field is changed from this default
                // value a modal pops up to warn the user.
                // (this modal already exists with debit/credit in transaction!
                // so probably look at that logic)


            }
        }).fail(function(data) {
            console.log("Failed to get ajax initial data");
        });
    });


}


function timeToMinutes(time) {
    // Takes a 24hr time string in HH:MM format and returns the minutes
    // This is used to calculate the total minutes in a timespan
    const [hours, minutes] = time.split(':').map(Number);
    return hours * 60 + minutes;
}


function renderTimeEntries(hiddenEle) {
    // Parse out the values in the hidden widget and make them look nice in
    // the pretty widget
    const prettyEle = document.getElementById(hiddenEle.id + "_pretty");
    if (!hiddenEle.value) {
        console.error("No value for hiddenEle: ", hiddenEle);
        return;
    }

    const entries = JSON.parse(hiddenEle.value).sort((a,b) => {
        if (a.start < b.start) {
            return -1;
        }
        if (a.start > b.start) {
            return 1;
        }
        return 0;
    });

    // make sure the entries are in order and re-apply them to the hidden ele
    hiddenEle.value = JSON.stringify(entries);


    let html = "";

    totalMinutes = 0;
    entries.forEach(item => {
        // Generates the HTML to display the timespans nicely for the user
        html += `<span onclick='javascript:removeTimeEntry(this);' class="multiple-entry-item">${item.start} - ${item.end}</span>`;

        // Calculates the total minutes of this timespan and adds to the total.
        const startMinutes = timeToMinutes(item.start);
        const endMinutes = timeToMinutes(item.end);
        const duration = endMinutes - startMinutes;

        totalMinutes += duration;
    });

    const itemsEle = prettyEle.querySelector(".multiple-entry-items");
    itemsEle.innerHTML = html;

    // apply the total minutes to the total_id element
    const totalEle = document.getElementById(prettyEle.getAttribute("total_id"));
    totalEle.value = totalMinutes;

    // Make sure the entry fields are cleared out, too.
    const startEle = document.getElementById(prettyEle.getAttribute("start_time"));
    const endEle = document.getElementById(prettyEle.getAttribute("end_time"));

    if (startEle.value) {
        startEle.value = null;
    }
    if (endEle.value) {
        endEle.value = null;
    }

    getTotalMinutes();

}


function removeTimeEntry(ele) {
    // Parse out the JSON in the hiddenEle, find the time entry that needs
    // to be removed, remove it and then output the JSON to the value of
    // the hidden ele and force a redraw with renderTimeEntries


    // Get the value from this item.
    const timeToRemove = ele.innerText;
    const ttrStart = timeToRemove.split("-")[0].trim();
    const ttrEnd = timeToRemove.split("-")[1].trim();

    // Get the parent's ID from this element
    const parentId = ele.parentNode.parentNode.id;

    // Use the parent's ID to find the proper hiddenInput (remove the _pretty)
    const hiddenEle = document.getElementById(parentId.replace("_pretty", ""));

    // Remove this value from the JSON values in the hiddenInput's value
    const curTimes = JSON.parse(hiddenEle.value);
    const newTimes = curTimes.filter(item => !(item.start == ttrStart && item.end == ttrEnd));


    hiddenEle.value = JSON.stringify(newTimes);

    // Re-render the pretty element
    renderTimeEntries(hiddenEle);
}

function validateTimeEntry(startEle, endEle) {
    // simple validation for if the content is blank or start time is greater
    // than the end time.
    startEle.classList.remove('validation-error');
    startEle.labels[0].classList.remove('validation-error');
    endEle.classList.remove('validation-error');
    endEle.labels[0].classList.remove('validation-error');


    if (!startEle.value && !endEle.value) {
        startEle.classList.add('validation-error');
        startEle.labels[0].classList.add('validation-error');
        endEle.classList.add('validation-error');
        endEle.labels[0].classList.add('validation-error');
        return false;
    }


    if (!startEle.value) {
        startEle.classList.add('validation-error');
        startEle.labels[0].classList.add('validation-error');
        return false;
    }

    if (!endEle.value) {
        endEle.classList.add('validation-error');
        endEle.labels[0].classList.add('validation-error');
        return false;
    }

    if (startEle.value > endEle.value) {
        startEle.classList.add('validation-error');
        startEle.labels[0].classList.add('validation-error');
        endEle.classList.add('validation-error');
        endEle.labels[0].classList.add('validation-error');
        return false;
    }

    return true;
}



function getDayOfWeek(year, month, day) {
    // Zeller's congruence to figure out the day of the week
    // https://en.wikipedia.org/wiki/Zeller's_congruence
    if (month < 3) {
        month += 12;
        year -= 1;
    }
    const K = year % 100;
    const J = Math.floor(year / 100);
    const formulaResult = day + Math.floor((month + 1) * 26 / 10) + K + Math.floor(K / 4) + Math.floor(J / 4) + 5 * J;
    const dayIndex = formulaResult % 7;
    const days = ['saturday', 'sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday'];

    return days[dayIndex];
}




function setDayOfWeek(id) {
    // called on a date field with the attribute onchange='setDayOfWeek(id)'
    // and sets the corresponding day of the week option on the day of the
    // week select's ID. So for example if using this from a django model
    // form it would look like this:
    /*
    "date": DateInput(format=settings.DATE_FORMAT, attrs={
        "class": "datepicker",
        "onchange": "setDayOfWeek('id_day_of_week');",
    }),
    */

    // Get the input and select elements
    var dateInput = document.getElementById('id_date');
    var select = document.getElementById(id);

    // Check if both elements exist
    if (dateInput && select) {
        var [year, month, day] = dateInput.value.split('-').map(Number);
        var dayOfWeek = getDayOfWeek(year, month, day);

        select.value = dayOfWeek;
    }
    else {
      console.error('Required elements not found');
    }
}




function renderLowercaseInput(id=null) {
    document.querySelectorAll('.lowercase-input').forEach(function(input) {
        input.addEventListener('input', function(e) {
            e.target.value = e.target.value.toLowerCase();
        });
    });

    if (id != null) {
        document.getElementById(id).addEventListener('input', function(e) {
            if (!e.target.classList.contains("lowercase-input")) {
                e.target.value = e.target.value.toLowerCase();
            }
        });
    }
}




function showClearReportModal() {
    // Shows a modal and if the user decides to go forward redirects to the
    // /report/cc_report/clear/ endpoint which clears out the pending and completed
    // reports for that user
    showModal(
        titleText="Clear Report List",
        bodyText="Are you sure you want to clear out all pending and completed reports?",
        primaryText="Clear Reports",
        secondaryText="Go back",
        primaryAction=function() { window.location="/report/cc_report/clear/"; },
        secondaryAction=hideModal,
    )

}



function showCompletedRoutes() {
    // find all the route-item classed TRs and remove the d-none class.
    $("tr.route-item").each(function() {
        $(this).removeClass("d-none");
    });

    $('#show-complete-routes-toggle').fadeOut();
}



function completeRoute(routeId) {
    // complete or uncomplete a route using ajax
    // if the route-routeId has the class "complete" then we want to uncomplete
    // the route. If the complete class is not present, then the user is
    // manually completing the route

    if ($("#route-" + routeId).hasClass("complete")) {
        // User is un-completing a route task. They probably
        // accidentally completed it and want to undo
        var url = "/config/ajax_route_uncomplete/" + routeId +"/";

        $("#route-" + routeId).removeClass("complete");
        $("#route-" + routeId + " span.fa-undo").removeClass("fa-undo").addClass("fa-check");
        $("#route-" + routeId + "-status").text("Pending");
    }
    else {
        // User is manually marking a route action as complete
        var url = "/config/ajax_route_complete/" + routeId +"/";

        $("#route-" + routeId).addClass("complete");
        $("#route-" + routeId + " span.fa-check").addClass("fa-undo").removeClass("fa-check");
        $("#route-" + routeId + "-status").text("Complete");
    }

    $.ajax({
        method: "POST",
        url: url,
        data: { csrfmiddlewaretoken: "{{ csrf_token }}", },
        success: function(data) {
            // Update the classes on the item to reflect that it's complete
            // also need to update the href to allow the user to undo if
            // they accidentally clicked complete
            if (data == "OK") {

            }
            else {
                console.log("Failed to update route");
            }

        }
    }).fail(function(data) {
        console.log("Failed to update route");
    });
}



function toggleRoutes() {
    // Just shows and hides the routes on this page
    $("#route-wrapper").fadeToggle(EASE_IN);
}



function readNotification(id) {
    // Marks the notification as read using an AJAX post
    $.ajax({
        method: "POST",
        url: "/config/ajax_read_notification/" + id + "/",
        data: { csrfmiddlewaretoken: "{{ csrf_token }}", },
        success: function(data) {
            // apply the read class so the notification shows more gray
            $("#notify-" + id).addClass("isread");

            // Subtract from the unread count which is a number that shows
            // to the right of the little notification icon
            let unreadCnt = parseInt($("#unread-count").text());
            unreadCnt--;
            if (unreadCnt == 0) {
                $("#unread-count").html("");
            }
            else {
                $("#unread-count").html("&nbsp;" + unreadCnt);
            }


        }
    }).fail(function(data) {
        console.log("Failed to mark notification as read:\n", data);
    });


}

// How often to check if new notifications exist
let NEW_NOTE_CHECK_TIMEOUT = 3000;
function checkForNewNotifications() {
    // At this point, only need to check for notifications if the user
    // is logged in
    {% if request.user.is_authenticated is False %}
        return;
    {% endif %}
    // Looks for notifications that have the is_new bit, if they exist
    // there needs to be a highlighted icon showing
    // This is the only ajax call that will be on a timer and it only checks
    // for new notifications.
    // TODO move this to websockets

    $.ajax({
        method: "GET",
        url: "/config/ajax_check_new_notifications/",
        success: function(data) {
            try {
                var jsonData = JSON.parse(data);
                jsonData[0];
            }
            catch {
                console.log("Failed to parse JSON for checkForNewNotifications()");
                var jsonData = null;
            }
            // If there are new notifications, show a highlight on the
            // notification icon(s)
            if (jsonData && jsonData[0] == true) {
                $("#notify-icon > span").addClass("orange font-weight-bold fas");
                $("#notify-icon > span").removeClass("far")

                // if there are more than 20 notifications just show '20+'
                if (jsonData[1] > 20) {
                    var unreadCount = "20+";
                }
                else {
                    var unreadCount = jsonData[1];
                }

                $("#unread-count").html("&nbsp;" + unreadCount);

                // if the user has the notify list expanded, refresh it
                if ($("#notify-list-wrapper").is(":visible")) {
                    ajaxGetNotifications();
                }
            }
            else {
                $("#notify-icon span").removeClass("orange font-weight-bold");
            }

        }
    }).fail(function(data) {
        console.log("Failed to check for new notifications:\n", data);
    });

    setTimeout(checkForNewNotifications, NEW_NOTE_CHECK_TIMEOUT);
}


function showNotifications() {
    // Toggles the sidebar which lists the user's notifications.
    // This is separated from ajaxGetNotifications because there are times
    // that we want to get the notifications and populate the list without
    // doing any toggle logic. for example, when a new notification comes in
    // while the user has the list expanded.
    $("#notify-list-wrapper").fadeToggle(EASE_IN, function(){
        if ($(this).is(":visible")) {
            ajaxGetNotifications();
        };
    });
}


function ajaxGetNotifications() {
    // The ajax function that populates the notification list view that the
    // user can expand on the right side of the base template
    $.ajax({
        method: "GET",
        url: "/config/ajax_get_notifications/",
        success: function(data) {
            var unreadCnt = 0;
            let jsonData = JSON.parse(data);

            let html = "";
            for (var key in jsonData) {
                let item = jsonData[key];

                var isread = "";
                if (item.is_read) {
                    var isread = "isread";
                }
                else {
                    unreadCnt ++;
                }
                var isnew = "";
                if (item.is_new) {
                    isnew = "isnew";
                }

                html += `
                    <div id='notify-`+item.id+`' class='notify-item `+isread+` `+isnew+`'>
                        <div class='notify-message'>`+item.message+`</div>
                        <div class='notify-footer'>
                            <span class='notify-created'>`+item.created_at+`</span>
                            <span class='notify-controls hover-show-gray small'>
                                <a href='javascript:readNotification(`+item.id+`);'>Mark as read <span class='fas fa-check'></span></a>
                            </span>
                        </div>
                    </div>`;
            }

            if (html) {
                $("#notify-list").html(html);
            }

            // clears out the new notification icon's orange highlight, updates
            // the unread counter
            if (unreadCnt == 0) {
                $("#unread-count").html("");
            }
            else {
                $("#unread-count").html("&nbsp;" + unreadCnt);
            }

            $("#notify-icon > span").removeClass("orange font-weight-bold fas");
            $("#notify-icon > span").addClass("far");
            //("#notify-count").remove();

        }
    }).fail(function(data) {
        console.log("Failed to get notifications:\n", data);
    });
}




function copyToClipboard(ele, data) {
    navigator.clipboard.writeText(window.location.origin + data).then(function() {
        console.log('Copying to clipboard was successful!');
    }, function(err) {
        console.error('Could not copy text: ', err);
    });

    let savedText = $(ele).text();
    $(ele).text("Link copied");
    setTimeout(function() {$(ele).text(savedText);}, 3000);
}




function renderDatalist() {
    $(".datalist").each(function() {
        let datalist_items = $(this).attr("datalist").split(",");
        let datalist_options = "";
        let datalist_id = $(this).attr("id") + "_datalist";
        for (const item of datalist_items) {
            datalist_options += "<option value='" + item.trim() + "'</option>";
        }

        let datalist_html = "<datalist id='"+ datalist_id + "'>" + datalist_options + "</datalist>";

        $(".datalist").after(datalist_html);
        $(".datalist").attr("list", datalist_id);

    });
}




function scrollToResults() {
    // Automatically scroll to report results ONLY if there are no
    // validation errors.
    if ($("#report-results").length > 0 && $("#error-msg").length == 0) {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#report-results").offset().top
        }, EASE_IN);
    }
}




function renderSelectMultiple() {
    // If any selectMultiple fields have the use_popup_lookup attribute
    // this code will override their UI to use the popup_lookup feature
    // This feature requires the global value EMR_FORM_ID to have a value


    $("*[use_popup_lookup]").each(function(count, field) {

        // Unfortunately I have no idea why this is needed but sometimes the
        // form P element is given a display: none inline styline so we need
        // to unhide it to show the linked docs feature.
        $(field).parent().css("display", "block");
        console.log("renderSelectMultiple()", field);

        var parentModel = $(field).attr("use_popup_lookup");
        var childModel = $(field).attr("popup_lookup_child_model");
        // pull the parent ID out of the URL
        splitUrl = window.location.href.split("/");
        var parentId = splitUrl[splitUrl.length - 2];

        $(field).parent().css("margin-bottom", "2rem");

        var anchors = []

        // this is used below to help deicde if the the 'add documents' button
        // shoulw be shown or not, as well as add some help text to explain
        // to the user what's going on
        var optionCnt = $("#" + field.id +" option").length;


        $("#" + field.id +" option").each(function(idx, option) {
            if ($(option).prop("selected")) {
                // TODO update this anchor so it's agnostic to emr_docs and
                // files and can work with any type of many-to-many object

                // Only need to show the unlink button if the form is not closed
                if (EMR_FORM_IS_CLOSED === true) {
                    anchors.push(
                        `<a href='/client/emr_doc/` + $(option).val() + `/file/' target='_blank'>`+$(option).text()+`</a>`
                    );
                }

                else {
                    anchors.push(
                        `<a class='emr-doc-unlink' href='/client/emr_doc/`+$(option).val()+`/`+parentId+`/unlink/'><span class="fas fa-unlink"></span></a>` +
                        `<a href='/client/emr_doc/` + $(option).val() + `/file/' target='_blank'>`+$(option).text()+`</a>`
                    );
                }


            }
        });


        // If there are linked docs, show them.
        // If there are not, only show the button that launches the popup_lookup
        // Remove the related multiselect fields
        $("label[for=" + field.id + "]").remove();

        var linkedDocsTitle = "";
        // Do not show the 'add document' button if the form is closed
        // or if there are no options to select from
        if (EMR_FORM_IS_CLOSED === true) {
            if (anchors.length > 0) {
                linkedDocsTitle = `<label for="`+field.id+`" class="hide-after mb-2">Linked documents</label><br>`
            }
            if (optionCnt == 0) {
                linkedDocsTitle = "<label class='hide-after mb-2'>Linked documents</label><p class='mt-3'>No documents have been uploaded to this episode.</p>"
            }

        }
        else {
            if (optionCnt == 0) {
                linkedDocsTitle = `<label for="`+field.id+`" class="hide-after mb-2">Linked documents</label><p class='mt-3'>No documents have been uploaded to this episode.</p><br>`;

            }
            else {
                linkedDocsTitle = `<label for="`+field.id+`" class="hide-after mb-2">Linked documents</label> <a class='btn btn-secondary btn-sm margin-left-2' href="javascript:showPopup('/config/popup_lookup/?mod=`+childModel+`&pmod=`+parentModel+`&pid=`+EMR_FORM_ID+`', 'wider');">Add Document</a><br>`;
            }
        }


        if (anchors.length > 0) {
            $("#" + field.id).parent().append(
                linkedDocsTitle +
                anchors.join("<br>")
            );
        }
        else {
            $("#" + field.id).parent().append(
                linkedDocsTitle
            );
        }

        $(".ms-options-wrap").remove();
        $("#" + field.id).remove();

    });


}







function navigateToAnchor() {
    // If there's an anchor tag in the URL, do a scrollTo that element
    // if it exists
    var anchor = window.location.hash.substr(1);

    if (anchor) {
        var anchorEle = $("#" + anchor);

        if (anchorEle.length > 0) {
            //console.log("AnchorEle:", anchorEle);
            $([document.documentElement, document.body]).animate({
                scrollTop: $(anchorEle).offset().top
            }, EASE_IN);
            highlightElement(anchor);
        }
        else {
            console.log("Could not scroll to element with ID:", anchor);
        }
    }
}




function highlightElement(id, fadeOverride=2000) {
    // This function just does a quick highlight and fade on the background
    // of the passed element's id
    $("#" + id).css("background", "#FF7733");
    $("#" + id).fadeIn(EASE_IN);
    $("#" + id).animate({"background-color": "transparent"}, fadeOverride);


}


function throttle(f, delay){
    var timer = null;
    return function(){
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = window.setTimeout(function(){
            f.apply(context, args);
        },
        delay || 500);
    };
}



function renderTwoColumns() {
    // Finds all the fields with the "twocols" attribute, generates a list, and
    // feeds that into the twoColumns function below
    var col1 = [];
    var col2 = [];
    var sectionHeaders = {};

    $("[twocols]").each(function() {
        let colNum = $(this).attr("twocols");
        let fieldId = $(this).attr("id");
        let sectionHeader = null;

        var parentClasses = $(this).parent().attr("class").split(/\s+/);
        for (var i = 0; i < parentClasses.length; i++) {
            //console.log("Parent class: ", parentClasses[i]);
            if (parentClasses[i].startsWith("form_section_")) {

                // Check if the data exist in the dict, if not create an
                // entry for this header
                // #TODO update so the sectionHeader can be overridden if
                // the insertbelow attribute is provided. Basically,
                // the insertbelow value will replace the parentClass as
                // the section header.
                if (sectionHeaders[parentClasses[i]] == undefined) {
                    sectionHeaders[parentClasses[i]] = {
                        col1: [],
                        col2: [],
                    };
                }

                // add the field to the proper column under the header
                if (String(colNum) === "1") {
                    sectionHeaders[parentClasses[i]]["col1"].push(fieldId);
                }

                else if (String(colNum) === "2") {
                    sectionHeaders[parentClasses[i]]["col2"].push(fieldId);
                }

                else {
                    console.log("Unknown colNum:", colNum);
                }
            }
        }
    });


    for (const [key, value] of Object.entries(sectionHeaders)) {
        var topEle = $("#" + key);

        // generate the html to be inserted after the topEle
        col1Html = $("<div class='col-md-6'></div>");
        col2Html = $("<div class='col-md-6'></div>");

        rowHtml = "<div class='row'><div id='col1_" + key + "' class='col-md-6'></div><div id='col2_" + key + "' class='col-md-6'></div></div>";
        topEle.after(rowHtml);

        for (var i = 0; i < sectionHeaders[key]["col1"].length; i++) {
            var field = sectionHeaders[key]["col1"][i];
            var parent = $("#" + field).parent().detach();
            parent.css({"float": null});
            parent.appendTo("#col1_" + key);
        }

        for (var i = 0; i < sectionHeaders[key]["col2"].length; i++) {
            var field = sectionHeaders[key]["col2"][i];
            var parent = $("#" + field).parent().detach();
            parent.css({"float": null});
            parent.appendTo("#col2_" + key);
        }
    }
}



function twoColumns(col1, col2) {
    // Insert a bootstrap row and columns, then move each field's parent
    // into these columns based on which arry they are in
    // The BS row needs to be inserted right below the insertBelow (if provided)
    // or the first field's section header.
    console.log("twoColumns()");
    //console.log(col1);
    //console.log(col2);

    console.log("Col1, Col2:", col1, col2);





    if (insertAfter === null || insertAfter == undefined) {

        var parent = $("#" + col1[0]).parent();
        classList = parent.attr("class").split(/\s+/);

        for (var i = 0; i < classList.length; i++) {
            //console.log("Class:", classList[i]);
            if (classList[i].startsWith("form_section_")) {
                topEle = $("#" + classList[i]);
                console.log("topEle:", topEle);
            }
        }
    }

    else {
        topEle = $("#" + insertAfter);
    }




}












// Check for form sections and if there are any, generate an index
// on the left side.
// There are 2 types of form sections. The normal .form-section and the
// .form-section.subsection-only. We only care about the main sections
// at least for now...
// TODO update this so it doesn't run on emr form generate report views
function createFormIndex() {
    console.log("createFormIndex()");
    var indexItems = [];
    $(".form-section").each(function(index, obj) {

        if ($(this).hasClass("subsection-only")) {
            console.log("Skpping due to subsection-only");
            return;
        }

        if (!$(this).find("h4").length) {
            return;
        }

        var section = $(this).find("h4").html().split("<small>")[0];

        var newId = "form_section_" + index.toString();

        $(this).attr("id", newId);

        var hideFromIndex = false;
        if ($(this).hasClass("hide-from-index")) {
            hideFromIndex = true;
        }

        //console.log("OBJECT CLASS");
        //console.log(obj);
        indexItems.push({
            section: section,
            id: newId,
            index: index,
            hideFromIndex: hideFromIndex,
        });
    });

    // Stop working if there are no items in the list
    if (indexItems.length != 0) {
        $("#form-container").addClass("container-fluid");
        $("#form-container").removeClass("container");

        $("#form-row").addClass("justify-content-left");
        $("#form-column").addClass("col-md-8");
        $("#form-column").removeClass("col-12");

        $("#form-row").prepend("<div id='form-index' class='col-md-2 print-hide'></div>");

        $("#form-index").append("<ol id='form-index-list'></ol>");

        // If there are index items, generate the html that will appear
        // as the navigation menu on the left.
        for (let i = 0; i < indexItems.length; i++) {
            let item = indexItems[i];

            // If hideFromIndex is true, we still want to generate a
            // form section but we do not want this item to appear in the
            // index on the left
            if (item.hideFromIndex === true) {
                continue;
            }
            else {
                $("#form-index-list").append(`<li><a id='form_section_`+item.index+`_anchor' href='#form_section_` + item.index + `' onclick='javascript:highlightElement("form_section_`+item.index+`");'>` + item.section + `</a></li>`);
            }
            //console.log("Processing item:", item);

            //$("#form-index-list").append("<li><a href='?form_section="+item.index+"'>" + item.section + "</a></li>");

        }
    }

    // loop through the form's elements and attach the proper
    // section class so they can be quickly accessed
    var lastSection = null;
    $("#frm").children().each(function(idx, ele) {

        // if it's the form save/delete buttons, ignore them.
        if ($(this).hasClass("btn-group-wrapper") === true){
            return true;  // continue
        }

        if ($(this).attr("id") != undefined) {
            // Loop through the form's elements and sections. Add the
            // section class to the elements so we can find the fields
            // under each section for future analysis
            if ($(this).attr("id").startsWith("form_section_") === true) {
                lastSection = $(this).attr('id');
            }
        }

        else {
            if (lastSection != null) {
                $(this).addClass(lastSection);
            }
        }

    });

    // If there's a finalize button, we also need to add that to the index
    var finalizeBtn = $("#btn-finalize");
    if (finalizeBtn.length > 0 && finalizeBtn.hasClass("hide-from-index") === false) {
        $("#form-index-list").append(`<li><a id='form_section_finalize_anchor' href='#btn-finalize'>Finalize</a></li>`);
    }

}












function phoneMask() {
    $(".phone").each(function () {
        $(this).usPhoneFormat({
            format: "(xxx) xxx-xxxx",
        });
    });
}



function renderSelectShow() {
    //console.log("renderSelectShow()");
    // Looks at a drop down and shows hidden fields depending on the value
    // selected in the drop down. This function deprecates
    // renderSelectToggleRelated and selectToggleRelated
    $('[selectshow]').each(function() {
        var selId = $(this).attr("id");
        /*
            The data will come in lists separated by a semicolon
            Each list contains a select value separated by a pipe
            and the fields that will be shown when that value is selected
            For example, if we want some fields to show when the user selects
            'yes' from the dropdown it would look like this:
                "yes|field1_name, field2_name"

            If there are multiple fields and values those lists can be separated
            by a semicolon. So if we had a dropdown that needs to show different
            fields when different values are selected, it could look like this:
                "yes|field1_name, field2_name;no|field3_name, field4_name"

            If there are multiple values, lets say 'yes' and 'other' that
            can cause the same fields to be shown, then the string could
            look like this:
                "yes,other|field1_name, field2_name"

        */
        var lists = $(this).attr("selectshow").split(";");

        // Loop through the list of values and their fields to build up
        // a dictionary, selectData to be used later
        let selectData = {};

        // The allFields list is just used to track all the fields that
        // could be affected by changing the values of the drop down and it's
        // used to hide them all by default when the page renders.
        let allFields = [];


        // This is the string that links fields to a selected value
        // It should look like: "yes|field1_name, field2_name"
        for (var selectShowDataString of lists) {
            if (selectShowDataString == "") {
                continue;
            }

            // This parses out the field names themselves. These are the fields
            // that will be shown if the select's value matches
            let fields = selectShowDataString.split("|")[1].trim().split(",").map(str => str.trim());

            // Build up the allFields list
            for (let x = 0; x < fields.length; x ++) {
                if (allFields.includes(fields[x]) === false) {
                    allFields.push(fields[x]);
                }
            }

            // showValue is the value that when selected will cause the
            // specified field(s) to appear. Splitting by a comma at the end
            // to handle the case where more than 1 value on the dropdown
            // can trigger the same set of fields
            let showValues = selectShowDataString.split("|")[0].trim().split(",");
            for(const showValue of showValues) {
                // Creating an entry in the dictionary that matches up the value
                // and the fields that will be shown when the values is selected
                // from the dropdown by the user
                selectData[showValue.trim()] = fields;
            }
        }


        // This hides all the related fields so they are all in the same state
        // when it comes time to decide which ones to show
        for (const hidemeId of allFields) {
            $("#id_" + hidemeId).parent().css("display", "none");
            $("label[for=id_" + hidemeId + "]").parent().css("display", "none");
        }

        // show the fields if the value matches the showValue. This happens
        // right when the page loads and the form is unmodified. e.g. the
        // form already has the value selected that would show some fields
        let selectedValue = $("#" + selId).val();
        if(selectedValue in selectData) {
            for (const  fieldId of selectData[$("#" + selId).val()]) {
                $("#id_" + fieldId).parent().css("display", "block");
                $("label[for=id_" + fieldId + "]").parent().css("display", "inline-block");
            }
        }

        //console.log("Select data:", selectData);
        //console.log("selectID:", selId);

        // If selectData contains "notnull" then the related fields should show
        // so long as the selected value is not null
        if ($("#" + selId).val().length > 0 && "notnull" in selectData ) {
            for (const fieldId of selectData["notnull"]) {
                $("#id_" + fieldId).parent().css("display", "block");
                $("label[for=id_" + fieldId + "]").parent().css("display", "inline-block");
            }
        }


        // Now when the value of the select is changed, we do 2 things.
        // Fist reset all the related fields by hiding them again
        // Second, depending on the selected value, show the related fields
        // do this using the dictionary that was created above.
        $("#" + selId).change(function() {
            let selectedValue = $("#" + selId).val();

            for (const fieldId of allFields) {
                $("#id_" + fieldId).val("");
                $("#id_" + fieldId).parent().css("display", "none");
                $("label[for=id_" + fieldId + "]").parent().css("display", "none");
            }

            if(selectedValue in selectData) {
                for (const fieldId of selectData[selectedValue]) {
                    $("#id_" + fieldId).parent().css("display", "block");
                    $("label[for=id_" + fieldId + "]").parent().css("display", "inline-block");
                }

                parent.resizeIframe();
            }

            // Notnull special case, if the selected value is anything but
            // null then we can show the related field(s)
            if (selectedValue.length > 0 && "notnull" in selectData ) {
                for (const fieldId of selectData["notnull"]) {
                    $("#id_" + fieldId).parent().css("display", "block");
                    $("label[for=id_" + fieldId + "]").parent().css("display", "inline-block");
                }

                parent.resizeIframe();
            }
        });

    });
}



function selectToggleRelated(selId, toggleValue, fields) {
    if (toggleValue.includes($("#" + selId).val())) {
        for (var i = 0; i < fields.length; i++) {
            $("#id_" + fields[i]).parent().css("display", "block");
            $("label[for=id_" + fields[i] + "]").parent().css("display", "inline-block");
        }
    }

    else {
        for (var i = 0; i < fields.length; i++) {
            $("#id_" + fields[i]).parent().css("display", "none");
            $("label[for=id_" + fields[i] + "]").parent().css("display", "none");
        }
    }

    $("#" + selId).change(function () {
        if (toggleValue.includes($("#" + selId).val())) {
            for (var i = 0; i < fields.length; i++) {
                $("#id_" + fields[i]).parent().css("display", "block");
                $("label[for=id_" + fields[i] + "]").parent().css("display", "inline-block");
            }
        }

        else {
            for (var i = 0; i < fields.length; i++) {
                $("#id_" + fields[i]).val("");
                $("#id_" + fields[i]).parent().css("display", "none");
                $("label[for=id_" + fields[i] + "]").parent().css("display", "none");
            }
        }
    });
}




function renderChkToggleRelated() {
    // This function gathers up all the form elements that have the attribute
    // "chkrelated" with a value other than "None". If the value has a comma
    // separated list of fields, those will get parsed out as the related
    // fields. If there is a single field, it also must be in a list, if there
    // is no field, that is a standard "_dtl" suffix.
    $("[chkrelated]").each(function() {
        var attr = $(this).attr("chkrelated");
        //console.log("chk attribute found:", attr);

        if (attr.toLowerCase() === "none") {
            //console.log("Attr is NONE!, not doing anything for this one.");
            return;
        }
        else if (attr.toLowerCase() === "") {
            // Default/normal chk related there is a _dtl field to use
            chkToggleRelated($(this).attr("id"), null);
        }
        else if (attr.includes(",")) {
            // Advanced chk related - comma separated list of field names
            chkToggleRelated($(this).attr("id"), attr.split(","));
        }
        else {
            chkToggleRelated($(this).attr("id"), [attr.trim(), ]);
        }

    });


}



function chkToggleRelated(chkId, fields=null) {
    // Provide the ID of the checkbox and a list of IDs for the fields and this
    // function will assign onchange listeners to show/hide the related fields
    // if the checkbox is checked or not.
    // If no fields are passed, this function assumes the fields are the same
    // name and the popup field has a suffix of _dtl
    //console.log("chkToggleRelated(", chkId,")");
    //console.log("Fields:", fields);


    // This code block pre-renders the related fields for this checkbox
    // if the checkbox is UNchecked then the related fields will be hidden
    // and shown if this checkbox is checked.
    if ($("#" + chkId).prop("checked") === false) {
        if (fields === null) {
            // No fields passed with this checkbox so look for a field with
            // the same ID but ending in '_dtl'
            $("#" + chkId + "_dtl").parent().css("display", "none");
            $("label[for=" + chkId + "_dtl" + "]").parent().css("display", "none");
        }
        else {
            for (const relatedField of fields) {
                if (relatedField === undefined || relatedField.trim() === "") {
                    continue;
                }

                $("#id_" + relatedField.trim()).parent().css("display", "none");
                $("label[for=id_" + relatedField.trim() + "]").parent().css("display", "none");
            }
        }
    }


    // Applies the event listener to this checkbox that shows or hides is
    // related fields
    $("#"+chkId).change(function () {
        if ($(this).prop("checked") === true) {
            if (fields === null) {
                $("#" + chkId + "_dtl").parent().fadeIn().css("display", "block");
                $("label[for=" + chkId + "_dtl" + "]").parent().fadeIn(EASE_IN).css("display", "inline-block");
            }
            else {
                for (const relatedField of fields) {
                    if (relatedField === undefined || relatedField.trim() === "") {
                        continue;
                    }

                    $("#id_" + relatedField.trim()).parent().fadeIn().css("display", "block");
                    $("label[for=id_" + relatedField.trim() + "]").parent().fadeIn(EASE_IN).css("display", "inline-block");
                }
            }

        }

        else {
            if (fields === null) {
                $("#" + chkId + "_dtl").val("");
                $("#" + chkId + "_dtl").parent().fadeOut();
                $("label[for=" + chkId + "_dtl" + "]").parent().fadeOut(EASE_OUT);
            }
            else {
                for (const relatedField of fields) {
                    if (relatedField === undefined || relatedField.trim() === "") {
                        continue;
                    }

                    $("#id_" + relatedField.trim()).val("");
                    $("#id_" + relatedField.trim()).parent().fadeOut();
                    $("label[for=id_" + relatedField.trim() + "]").parent().fadeOut(EASE_OUT);
                }
            }
        }

    });

}



function formatExpandFields() {
    $("[expfield]").each(function() {
        var _fields = $(this).attr("expfield").split(",");
        var rowCount = parseInt($(this).attr("expcount"));
        var fields = [];

        //console.log("_Fields:", _fields);
        //console.log("Rowcount:", rowCount);

        for (var i = 0; i < _fields.length; i++) {
            let field = _fields[i].trim();

            if (field != undefined && field.trim != "" && field != null){
                fields.push(field);
            }
        }

        expandFields(null, rowCount, fields);
    });


}


function expandFields(grpName=null, rowCount, rowFields) {
    //console.log("formatExpandFields()");

    // This function takes a list of fields that have multiple lines, for
    // example, diagnosis information on the r & i intake form, and hides the
    // extras until the user clicks a plus icon for each line.

    // Each field that can be expanded needs to be grouped with other
    // fields on the same line. to do this, we set the attributes in
    // the django form and we'll detect those using this function

    var grpName = rowFields[0];

    var iconExists = false;

    for (var i = 2; i <= rowCount; i++) {
        var rowElements = [];

        for (x = 0; x < rowFields.length; x++) {
            var rowField = rowFields[x];

            if(rowField.endsWith("_lookup")) {
                let fmtFieldId = rowField.slice(0, -7);
                rowElements.push($("#id_" + fmtFieldId + "" + i + "_lookup"));
                rowElements.push($("label[for=id_"+fmtFieldId+"" + i + "_lookup" + "]"));
            }
            else {
                rowElements.push($("#id_" + rowField + "" + i));
                rowElements.push($("label[for=id_"+rowField+"" + i + "]"));
            }
        }

        // Looping through the extra fields. We have a few things to do
        // 1. check for data in the field, if there's data, do not hide
        // the whole row
        // 2. Hide the row if none of the fields have data
        // 3. Apply a plus or add icon to the right side of the row

        // apply the row ID to all the fields, we'll use this later to
        // show/hide rows
        for (var y = 0; y < rowElements.length; y++) {
            var field = rowElements[y];
            field.addClass(grpName + "-expand-row-"+i);
        }


        // checking for data in the field and if we need to hide the row or not
        var hideRow = true;
        for (var y = 0; y < rowElements.length; y++) {
            var field = rowElements[y];

            if (field.val() != undefined && field.val() != null && field.val() != "") {
                hideRow = false;
                break;
            }
        }

        // The row needs to be hidden and an icon needs to be applied
        if (hideRow === true) {
            for (var y = 0; y < rowElements.length; y++) {
                var field = rowElements[y];
                field.attr("expand", true);
                field.css("display", "none");
            }

            // add the plus button
            var lastField = rowElements[rowElements.length - 1]

            // We only need to display 1 'next row' at a time, and if we're on
            // the last row, we don't want to show it at all.
            {% if instance.is_finalized is False %}
                if (i <= rowCount) {
                    if (iconExists === false) {
                        lastField.parent().prepend(`<div id='`+grpName+`-next-row-btn-`+i+`' onclick='javascript:showNextRow("`+grpName+`", `+i+`)' class='float-right  next-row-btn `+grpName+`-expand-row-`+(i-1)+`'>Next row</div>`);
                        iconExists = true;
                    }
                    else {
                        lastField.parent().prepend(`<div id='`+grpName+`-next-row-btn-`+i+`' onclick='javascript:showNextRow("`+grpName+`", `+i+`);' class='float-right next-row-btn  `+grpName+`-expand-row-`+(i-1)+` d-none'>Next row</div>`);
                    }

                }
            {% endif %}

        }

    }

}

function showNextRow(grpName, rowId) {
    var rowClass = grpName + "-expand-row-" + rowId;
    var btnId = "#" + grpName + "-next-row-btn-" + rowId;

   // console.log("Showing row with class of:", rowClass);

    // show the next row and the next button
    $("."+ rowClass).each(function () {
        $(this).css("display", "inline-block");
        $(this).removeClass("d-none");
    });


    // Hide the button that was just pressed
    $(btnId).css('display', 'none');
}





function renderFormCheckbox() {
    // This adjusts the textboxes and their labels so they are on the same
    // line. The idea is this just happens to checkboxes and the rest of the
    // inputs are unaffected
    //console.log("renderFormCheckbox");
    var checkboxes = $("input[type=checkbox]").each(function(){
        //console.log($(this));
        // this should be easy, just need to move the checkbox in front
        // of the label within the parent
        var parent = $(this).parent();

        // this excludes checkboxes that are part of a multiselect
        if (parent.prop("nodeName") == "LABEL") {
            //console.log("Skpping where parent element is a label");
            return;
        }

        // just move the input before the label
        parent.prepend($(this));
    });
}



//var AUTOSAVE_FIRST_TIME = true;
function formAutoSave(token, save_url, form_id) {
    // Autoasave feature for EMR forms (may be used elsewhere) takes the form
    // data and sends a POST to the server to save at set intervals
    //console.log("Auto save");
    //console.log("token:", token);
    //console.log("save_url:", save_url);
    //console.log("for_id:", form_id);


    $.ajax({
        method: "POST",
        url: save_url,
        data: $("#frm").serialize() + "&autosave=autosave",
        success: function(data) {
            //console.log("Autosave response data:", data);
            if (data == "OK") {

                console.log("Autosave success");

                var now = new Date();
                /*
                if (AUTOSAVE_FIRST_TIME === true) {
                    AUTOSAVE_FIRST_TIME = false;
                    highlightElement("autosave-status");
                }
                */
                highlightElement("autosave-status", fadeOverride=1000);
                $("#autosave-status").text("Form saved at " + now.getHours() + ":" + ("0" + now.getMinutes()).slice(-2) + ":" + ("0" + now.getSeconds()).slice(-2) );

            }
            else {
                // Form didn't save probably because of
                // it being a case note and having a validation error. the
                // form auto save feature is not officially supported in case
                // notes as of 2022-11-22 but, if there are no validation
                // errors it works
            }

        },
    }).fail(function(err_data) {
        console.log("Autosave error. Could not automatically save the form.");
        //console.log(err_data);
    });

}



function verifyPassword(token, inputId, sigType=null, instanceId=null) {
    // Uses ajax to check the user entered their password properly to
    // sign an emr form.
    //console.log("verifyPassword");


    var pwd = $("#" + inputId).val();

    if (pwd == undefined || pwd == null || pwd == "") {
        alert("Please enter a password and try again");
        $("#nova-modal-primary").prop("disabled", false);

        return false;
    }


    $.ajax({
        method: "POST",
        url:"/config/ajax_verify_password/",
        data: {
            csrfmiddlewaretoken: token,
            pwd: pwd,
            sig_type: sigType,
            instance_id: instanceId
        },
        success: function(data){
            // The response will be a URL to redirect to the proper signature
            // endpoint. as of now (2023-09-20) there are 2 signature types,
            // normal caseworker and supervisor
            window.location = data;
        }

    }).fail(function(data){
        //alert(data);
        alert("Incorrect password");
        $("#" + inputId).val("");
        $("#nova-modal-primary").prop("disabled", false);
    });



}



function set_field_value(id, value) {
    //console.log("set_field_value:", id, value);
    if (id == "id_service_start" || id == "id_service_end") {
        // Need to strip out the extra seconds zeroes because the time
        // fields are expecting hour:minute
        value = value.substring(0, value.length - 3);
    }
    console.log("Setting", id, "to", value, typeof(value));
    let field = document.getElementById(id);

    // Need to do some special parsing for checkbox values.
    if (field.type == "checkbox") {
        if (value.toLowerCase() == "true") {
            field.checked = true;
        }
        else if (value.toLowerCase() == "false") {
            field.checked = false;
        }
    }
    else {
        if (value.toLowerCase() == "none") {
            value = null;
        }
        field.value = value;
    }

}

function set_field_error(id) {
    //console.log("field error for id:", id);
    document.getElementById("id_" + id).classList.add("validation-error");

    // First check if the element exists. If it does not, it may be a
    // radio list, so we'll try adding a '_0' to the ID and try again...
    try {
        var label = document.querySelector("label[for='id_" + id + "']");
        label.classList.add("validation-error");
    }
    catch {
        var label = document.querySelector("label[for='id_" + id + "_0']");
    }

    if (label != null && label != undefined) {
        label.classList.add("validation-error");

        // Update the error message so the field names match the label, not id
        let error_msg = document.getElementById("error-msg");
        error_msg.innerHTML = error_msg.innerHTML.replace(id, label.textContent);
    }










}



function renderNoEdit() {
    // This is to be called on the template itself.
    // look at the client_edit template


    // In some cases, like the emr_episode page, we don't actually want to
    // disable everything. In fact, we only want to disable a few fields
    if (window.location.pathname.includes("/client/emr_episodes/")) {
        let ids = [
            "id_start",
            "id_end",
            "id_clts_target_group",
            "id_closing_reason",
            "id_closing_reason2",
            "id_closing_reason3",
            "id_pps_mh_end_reason",
            "btn-return",
            "id_notes",
        ];

        for (var i = 0; i < ids.length; i++) {
            let ele_id = "#" + ids[i];

            //console.log("renderNoEdit() Disabling:", ele_id);

            $(ele_id).attr("disabled", "disabled");
            $(ele_id).parent().addClass("edit-false");
        }


        //$("#id_closing_reason").parent().css("display", "none");
        $(".btn-delete").css("display", "none");


        return;
    }



    $("input, select, .btn").each(function(idx, object) {
        // We must check for the override class exclude-edit-false
        // This class allows us to exlcude the element from this script
        // a good example is the b-3 case notes at the bottom of the client
        // face sheet. To even see the b-3 list, the user must opt in via
        // a setting in the userconfig. The idea is that the users want to be
        // able to add new case notes from this view without having to unlock
        // the client demographics which makes sense because the case notes
        // will open to a different view.
        if ($(object).hasClass("exclude-edit-false")) {
            return;
        }

        //console.log(idx, object);
        $(object).attr("disabled", "disabled");
        $(object).parent().addClass("edit-false");

        //console.log($(object).prop('nodeName') );

        if ($(object).prop('nodeName') == "SELECT") {
            // If the selected value is just hyphens (null)
            // show nothing
            if ($(object).find(":selected").text() == "----------") {
                $(object).find(":selected").text('');
            }
        }

        if ($(object).attr('type') == "checkbox") {
            // Render the checkboxes as yes or no text
            //console.log($(object).attr("checked"));
            $(object).attr("type", "textbox");
            if ($(object).attr("checked") == "checked") {
                $(object).val("Yes");
            }
            else {
               $(object).val("No");
            }
        }
    });

    // Remove the hover effects and such for any listview POPUP links
    // We only want to do this on popups because they are within the same
    // view. For something like case notes that take us to a different view,
    // we still want that to work because it's not editing anything in this
    // view
    $("table a").each(function(idx, object) {
        if ($(object).attr("href").includes("showPopup")) {
            $(object).addClass("listview-no-click");
            $(object).attr("href", 'javascript:void(0)');
        }
    });

}




function time_24hr_helper() {
    // This function looks at what a user is typing into a time field and
    // helps them by applying the AM or PM while they are typing. This is a bit
    // smoother than the default browser behavior and was requested by the users
    $("input[type='time']").each(function(index, object){
        var last_time_id = null;
        var time_value = null;

        $(object).keyup(function(e){
            const value = $(this).val();
            //console.log("Timeinput value:", value);
            //console.log("time_value:", time_value);

            // Don't want to force calculations on a field that's already
            // been set or if the user didn't press a number
            if (value || isNaN(e.key)) {
                //console.log("Already has a value of ", value);
                return;
            }

            if ($(this).attr("id") != last_time_id || time_value == null) {
                //console.log("New time id detected");
                last_time_id = $(this).attr("id");
                time_value = e.key;
            }
            else {
                time_value += e.key;
            }


            if (time_value.length == 2) {
                const itime = parseInt(time_value);

                // by passing this 'valid' time to the widget, the browser
                // will automatically apply the am or pm
                // Feels like a bit of a hack but sending the first 2 digits,
                // the hours, and setting the minutes to zero forces the browser
                // to apply the am/pm tag and conveniently still leaves the
                // user right at the minutes section ready to be filled in
                $(this).val(time_value+":00");

                if (itime > 12) {
                    //console.log("Setting 24h time");
                    $(this).val(time_value+":00");
                }
                else {
                    // The users have asked that AM be automatically applied
                    // with the caveat that they now must use the 24 hour format
                    // instead of being able to use 12 or 24 hour
                    $(this).val(time_value+":00");
                }

                time_value = null;
            }
            else if (time_value.length > 2) {
                time_value = null;
            }

        });
    });
}



function get_time_difference(start_time, end_time) {
    // This function takes a start and end time string in 24 hour format
    // and returns a value in minutes.
    //console.log("get_time_difference", start_time, end_time);

    if (start_time == undefined || end_time == undefined)
        return 0;

    if (start_time.split(':').length != 2 || end_time.split(':').length != 2)
        return 0;


    var start_date = new Date()
    start_date.setHours(start_time.split(':')[0]);
    start_date.setMinutes(start_time.split(':')[1]);

    var end_date = new Date()
    end_date.setHours(end_time.split(':')[0]);
    end_date.setMinutes(end_time.split(':')[1]);

    var total_minutes = (end_date - start_date) / 1000 / 60

    if (total_minutes < 0)
        return 0;

    return total_minutes;
}



function check_if_exist(model,field, value, model_id=null) {
    // Checks if a field in a model already exists and if it does,
    // shows a message on the field's label
    let found_field = $("#id_" + field);
    let found_label = $("label[for='id_name']");


    if (found_label.attr("orig_label") == undefined || found_label.attr("orig_label") == "") {
        var orig_label = found_label.text();
    }
    else {
        var orig_label = found_label.attr("orig_label");
    }


    $.ajax({
        method: "GET",
        url : "/config/ajax_check_if_exist/" + model + "/" + field + "/" + value +"/" + model_id,
        success:function(data){
            let parsed_data = JSON.parse(data);
            //console.log("Parsed data~! " + parsed_data);

            // Find the field and set some sort of notification...
            if (parsed_data === true) {

                found_label.text(orig_label + " '" + value + "' already in use.");
                found_label.attr("orig_label", orig_label);

                //console.log("Found Field: " + found_field);
                //console.log("Found label: " + found_label);
                //console.log(orig_label);
            }
            else {
                found_label.text(orig_label);
            }
        },
    });
}

function formSection(labelId, data){
    // A way to add sections to a model form
    // The labelId is the first label of the section, the title will
    // appear above that
    //console.log("FormSection labelID:", labelId);
    //console.log("FormSection data:", data);
    if (labelId == "report_header_row") {
        return;
    }


    var hideFromIndex = "";
    if (data.hideFromIndex === true) {
        hideFromIndex = "hide-from-index";
    }

    if (labelId == "finalize_form") {
        // The finalize section is different. It's really just a single button
        // added to the bottom of the form.
        $("#btn-return").after(`<input type='button' onclick="javascript:renderPasswordModal()" class='btn btn-secondary ` + hideFromIndex + ` name='btn-finalize' id='btn-finalize' value='Finalize'>`);
    }
    else if (labelId == "id_linked_docs") {
        // Linked docs is also special. It uses popup lookup and doesn't get
        // an id assigned like the other form inputs
        $("label[for='id_linked_docs']").parent().before("<div class='form-section " + hideFromIndex + " '><h4>" + data.title + "</h4><hr></div>");
    }
    else if (data.subtitle === null || data.subtitle == undefined){
        $("#"+labelId).parent().before("<div class='form-section " + hideFromIndex + " '><h4>" + data.title + "</h4><hr></div>");
    }
    else if (data.subtitle != null && data.title === null) {
        //console.log("Subsection only");
        $("#"+labelId).parent().before("<div class='form-section subsection-only'><h5>" + data.subtitle + "</h5></div>");
    }
    else {
        //console.log("Section and Subsection");
        $("#"+labelId).parent().before("<div class='form-section " + hideFromIndex + "'><h4>" + data.title + "</h4><hr><h5>" + data.subtitle + "</h5></div>");
    }


    // TODO add a 'hidden' or blank section that can be created when title
    // and subtitle are null
}



function emrFormReportSection(labelId, data){
    // A way to add sections to a model form
    // The labelId is the first label of the section, the title will
    // appear above that
    // This is ONLY for the EMR report view.
    // Normal reports can use formSection()
    // It's also important to update the HREF in the download as pdf
    // button so the form's JSON data can be referenced so we know which
    // sections to apply to the PDF download


    // if the form section is called "finalize_form" then we need to create
    // a special custom section that just has the finalize button
    if (labelId == "finalize_form") {
        return false;
    }

    // If the field JSON has the value reportHide then we obviously need to
    // not show this section. The reportHide value is typically used when
    // the report view is different than the edit view and we may have extra
    // fields at the top, for example the client's biographics, and they may be
    // part of the same section as the subsequent fields and we don't want
    // duplicated section headers
    if (data.reportHide === true) {
        return false;
    }

    if (data.title != null){
        $("#"+labelId).before("<div class='form-section'><h4>" + data.title + "</h4><hr></div>");
    }

    // Dont really want to show subtitels at all in the report views
    /*
    else if (data.subtitle != null && data.title == null) {

        $("#"+labelId).before("<div class='form-section subsection-only'><h5>" + data.subtitle + "</h5></div>");
    }
    */

}


function updateEmrFormDownloadPdfLink(formName) {
    // This updates the 'Download as PDF' button on the EMR Form Report view
    // and adds the name of the form. This allows the PDF rendering function
    // to apply the proper sections when it creates the PDF.
    var btn = $("#emr-form-pdf-download");
    var newHref = $(btn).attr("href") + "&formname=" + formName
    $(btn).attr("href", newHref);
}


function renderTableFieldClearButtons(){
    /* For now, only call this within reports */
    $("form label").each(function(ele){
        let input_type = $(this).next().attr('type');
        if (input_type == "text" || input_type == "number" || input_type == "" || input_type == undefined){
            $(this).append("<span class='field-clear-icon'>˟</span>");
        }
    });

    $(".field-clear-icon").click(function(){
        $(this).parent().next().val(null);
    });

    $("form p").hover(function(){
        $(this).find(".field-clear-icon").fadeIn(300);
    }, function(){
        $(this).find(".field-clear-icon").fadeOut(200)
    });
}



function renderTableSearch(){
    // this is kind of wonky, but idk maybe we can detect
    // if there is a date filter active on the page.
    // kinda sucks that this is implemented in a fractured way.
    // TODO relook at the filter and pageination interactions on
    // the authorization page before other pages use this
    var urlFilter = getUrl("filter");


    if (urlFilter){
        $("#table-filter").val(urlFilter);
        $("#table-filter").focus();
    }

    var pageNum = getUrl("page");
    if (!pageNum) {
        pageNum = "1";
    }

    //console.log(pageNum);

    // checking if the date-filter (or maybe other filters) are active
    // on this page's template
    var yearfilter = null;
    if ($("#year-filter").length) {
        if (getUrl("year")) {
            $("#year-filter").val(getUrl("year"));
            if ($("#year-filter").val() != getUrl("year")) {
                $("#year-filter").val("");
            }
        }
        yearFilter = $("#year-filter").val();
    }

    $("#table-filter").keydown(function(e){
        //console.log(e.keyCode);
        if (e.keyCode == 13){
            applyTableFilter();
        }
    });
}

function applyTableFilter() {
    var url = "";

    if (getUrl("filter") != $("#table-filter").val()
        || getUrl("year") != $("#year-filter").val()) {

        url = setUrl("page", "1", url);
    }

    url = setUrl("filter", $("#table-filter").val(), url);
    url = setUrl("year", $("#year-filter").val(), url);
    document.location = url;
}



function showModal(titleText, bodyText, primaryText, secondaryText, primaryAction, secondaryAction=null, opacity=1, loadAction=null,  customHtml=null){
    // ALL modal popups will eventually use this
    //console.log("showModal");
    var wrapper = document.getElementById("nova-modal-wrapper");
    var title = document.getElementById("nova-modal-title");
    var body = document.getElementById("nova-modal-body");
    var primaryBtn = document.getElementById("nova-modal-primary");
    var secondaryBtn = document.getElementById("nova-modal-secondary");

    title.innerHTML = titleText;
    body.innerHTML = bodyText;
    primaryBtn.innerHTML = primaryText;
    secondaryBtn.innerHTML = secondaryText;

    if (customHtml){
        body.innerHTML = body.innerHTML + customHtml;
    }

    wrapper.style.backgroundColor = "rgba(56,63,81,"+opacity+")";
    $("#nova-modal-wrapper").fadeIn(EASE_IN);


    // this clears out event handlers so we're not accidently
    // doing things more than once
    primaryBtn.outerHTML = primaryBtn.outerHTML;
    secondaryBtn.outerHTML = secondaryBtn.outerHTML;
    primaryBtn = document.getElementById("nova-modal-primary");
    secondaryBtn = document.getElementById("nova-modal-secondary");

    primaryBtn.addEventListener("click", function() {
        primaryBtn.disabled = true;
        secondaryBtn.disabled = true;
        primaryAction();

    });
    secondaryBtn.addEventListener("click", function() {
        primaryBtn.disabled = true;
        secondaryBtn.disabled = true;
        secondaryAction();
    });

    primaryBtn.disabled = false;
    secondaryBtn.disabled = false;

    if (primaryText == null){
        primaryBtn.style.display = "none";
    }
    else {
        primaryBtn.style.display = "inline-block";
    }

    if (secondaryText == null) {
        secondaryBtn.style.display = "none";
    }
    else {
        secondaryBtn.style.display = "inline-block";
    }

    if (loadAction != null){
        console.log("Load action is not null!:", loadAction);
        loadAction()
    }
}


function hideModal(){
    //console.log("hideModal");
    $("#nova-modal-wrapper").fadeOut(EASE_OUT);
}

function renderModalSubmit() {
    var primary = $("#nova-modal-primary");
    var secondary = $("#nova-modal-secondary");

    primary.click(function(){
        $(this).prop("disabled", true);
        //console.log("DISABLED PRIAMRY!");
    });

    secondary.click(function(){
        $(this).prop("disabled", true);
        //console.log("DISABLED SECONDARY!");

    });
}


function revokeSharedlinkModal(link_id) {
    showModal(
        titleText="Revoke Shared Link",
        bodyText="Are you sure you want to revoke access to this shared link?<br>External users will no longer have access.",
        primaryText="Go back",
        secondaryText="Revoke Shared Link",
        primaryAction=hideModal,
        secondaryAction=function() { window.location="/external/sharedlink/revoke/" + link_id +"/"; },
    )

}




function unlinkEmrDoc(emr_doc_id) {
    showModal(
        titleText="Unlink EMR Document",
        bodyText="Are you sure you want to unlink this EMR document?",
        primaryText="Go back",
        secondaryText="Unlink EMR Document",
        primaryAction=hideModal,
        secondaryAction=function(){ window.location="/client/emr_doc/" + emr_doc_id + "/unlink/"; },
    );
}



function unlinkAuditorClient(auditor_id, client_id) {
    showModal(
        titleText="Remove Client Access",
        bodyText="Are you sure you want to remove this client access?",
        primaryText="Go back",
        secondaryText="Remove Client Access",
        primaryAction=hideModal,
        secondaryAction=function(){ window.location="/config/auditor_client/remove/" + auditor_id + "/" + client_id + "/"; },
    );
}




function newBudget(){
    showModal(
        titleText="New Budget",
        bodyText="Would you like to have this budget start with the same accounts and amounts as the current budget?",
        primaryText="Yes",
        secondaryText="No, Create a Blank Budget",
        primaryAction=function(){ window.location="/accounting/budget/copy_existing/" },
        secondaryAction=function(){ window.location="/accounting/budget/new/"; },
        opacity=0.8,
        );
}

function newEmrEpisode(client_id){
    showModal(
        titleText=null,
        bodyText=`<div class='emr-epi-type-modal'>
            <a href='/client/new_emr_episode/b3/`+client_id+`/'>Birth to 3</a>
            <a href='/client/new_emr_episode/b3_preadmit/`+client_id+`/'>Birth to 3 Pre-Admit</a>
            <a href='/client/new_emr_episode/clinic_preadmit/`+client_id+`/'>Clinic Pre-Admit</a>
            <a href='/client/new_emr_episode/clts_preadmit/`+client_id+`/'>CLTS Pre-Admit</a>
            <a href='/client/new_emr_episode/clts/`+client_id+`/'>CLTS</a>
            <a href='/client/new_emr_episode/crisis/`+client_id+`/'>Crisis</a>
            <a href='/client/new_emr_episode/cst/`+client_id+`/'>CST</a>
            <a href='/client/new_emr_episode/randi/`+client_id+`/'>R & I</a>
            <a href='/client/new_emr_episode/tcm/`+client_id+`/'>TCM</a>
        </div>
        `,
        primaryText=null,
        secondaryText="Go back",
        primaryAction=null,
        secondaryAction=hideModal,
        opacity=0.8,
        )

    // This prevents folks from double-clicking on the create episode button
    // and then creating duplicated episodes
    $('.emr-epi-type-modal > a').click(function(e) {
        e.preventDefault();

        window.location = $(this).attr("href");

        $(this).attr("href", '');
        $(this).text("Loading...");

        $(this).siblings().each(function() {
            $(this).attr("href", '');
            $(this).text("Loading...");
        });
    });

}

function verifyJournalEntry(){
    showModal(
        titleText="Verify Journal Entry",
        bodyText="Are you sure you want to verify this journal entry?<br>This action can only be undone by a user with the proper permissions.",
        primaryText="Verify Journal Entry",
        secondaryText="Go back",
        primaryAction=function(){
            document.getElementById("id_verified").checked = true;
            document.getElementById("btn-return").click();
            hideModal();
        },
        secondaryAction=function(){
            document.getElementById("id_verified").checked = false;
            hideModal();
        },
        opacity=1.0,
        loadAction=null,
        customHtml=null,
        );
}


function confirmJEReview(journalId){
    let url = "/accounting/journal_entry/review/" + journalId + "/";
    showModal(
        titleText="Review Journal Entry",
        bodyText="By marking this Journal Entry as reviewed, you acknowledge that you have thoroughly verified its content for accuracy and completeness.",
        primaryText="Mark as Reviewed",
        secondaryText="Cancel",
        primaryAction=function(){ window.location = url; },
        secondaryAction=hideModal,
        opacity=0.8,
        loadAction=null,
        customHtml=null,
    );
}




function unverifyJournalEntry(journalId){
    let url = "/accounting/journal_entry/unverify/" + journalId + "/";
    showModal(
        titleText="Unverify Journal Entry",
        bodyText="This journal entry <strong>has already been exported</strong> and its data may exist in other systems outside of Nova. Modifying an exported journal entry should be done with caution.",
        primaryText="Go back",
        secondaryText="Unverify Journal Entry",
        primaryAction=hideModal,
        secondaryAction=function(){ window.location = url; },
        opacity=0.8,
        loadAction=null,
        customHtml=null,
    );
}

function payrollJe(parent_id, employee_id){
    showModal(
        titleText="Payroll Journal Entry",
        bodyText="Please enter the total amount for the payroll journal entry:",
        primaryText="Create Payroll Journal Entry",
        secondaryText="Go back",
        primaryAction=function(){
            var amount = Math.round(document.getElementById("je_amount").value * 100);
            //console.log(amount);
            window.location = "/accounting/payroll_je/new/" + parent_id + "/" + employee_id + "/" + amount;
        },
        secondaryAction=hideModal,
        opacity=0.8,
        loadAction=function(){
            document.getElementById("je_amount").select();
        },
        customHtml="<p class='mt-4 text-center'><input type='number' min=0 id='je_amount' class='form-control mr-1 right' style='max-width:8rem; margin:auto;  display: inline-block;'></input></p>",

    );
}


/*
let idleTimeout = 60 * 1000 * 15;  // 15 minutes
let idleWarningTimeout = idleTimeout + 60000;  // 60 seconds
let idleTimer;
let idleWarningTimer;
window.addEventListener("mousemove", resetIdleTimer);
window.addEventListener("click", resetIdleTimer);
window.addEventListener("keydown", resetIdleTimer);


function resetIdleTimer() {
    clearTimeout(idleTimer);
    clearTimeout(idleWarningTimer);

    idleTimer = setTimeout(function() {
        showModal(
            titleText="About to be logged out",
            bodyText="You are about to be logged out of Nova out due to inactivity.",
            primaryText="Stay logged in",
            secondaryText="Log out",
            primaryAction=sessionStay,
            secondaryAction=sessionLeave);
    }, idleTimeout);

    idleWarningTimer = setTimeout(function () {
        window.location = "/logged_out/";
    }, idleWarningTimeout);
}


function sessionStay(){
    resetIdleTimer();
    hideModal();
}
function sessionLeave(){
    window.location = "/logged_out/";
}
*/

function atpAutoDischarge(redirect){

    showModal(
        titleText="ATP Auto Discharge",
        bodyText="You are about to create the Ability to Pay discharge transactions.",
        primaryText="Create ATP transactions",
        secondaryText="Go back",
        primaryAction=function() {
            if (redirect != undefined && redirect != null){
                window.location = "/accounting/atp_discharge/?next=" + redirect;
            }
            else {
                window.location = "/accounting/atp_discharge/";
            }

        },
        secondaryAction=hideModal,
        opacity=0.8
    );

}


function render_anchor_tags(){
    // Looks at all the anchor tags on the screen and applys changes if needed
    // First thing this looks at is applying a popout icon for any anchor
    // tags that have target="_blank"
    anchors = document.getElementsByTagName("a");
    for (var i = 0; i < anchors.length; i++){
        //console.log(anchors[i].getAttribute("target"));
        if (anchors[i].getAttribute("target") == "_blank"){
            if (anchors[i].classList != undefined){
                anchors[i].classList.add("anchor-blank");
            }
            else {
                // element doesn't have a class. so create one
                anchors[i].className = "anchor-blank";
            }
        }

    }

}



function showSignatureStatus(){
    $(".signature-status").fadeIn();
}


function render_optional(){
    $('.optional-off').each(function(){
        $(this).siblings('label').addClass('hide-after');
    });

    $('.dataTables_filter label').each(function(){
        $(this).addClass('hide-after');
    });

    $('.dataTables_length label').each(function(){
        $(this).addClass('hide-after');
    });
}




function unlinkTransaction(id){
    showModal(
        titleText="Unlink Transaction",
        bodyText="Are you sure you want to unlink this transaction?",
        primaryText="Unlink transaction",
        secondaryText="Go back",
        primaryAction=function(){ window.location = "/accounting/transaction/" + id + "/unlink"; },
        secondaryAction=hideModal,
        opacity=0.8
        )
}


function unlinkAllJournalTransactions(id){
    showModal(
        titleText="Unlink All Transactions",
        bodyText="Are you sure you would like to unlink all the transactions from this journal entry?",
        primaryText="Unlink all",
        secondaryText="Go back",
        primaryAction=function(){ window.location = "/accounting/journal_entry/" + id + "/unlink_all"; },
        secondaryAction=hideModal,
        opacity=0.8
        )
}


function focusFirstField() {
    var urlFilter = getUrl("filter");
    //console.log("urlFilter: " + urlFilter);
    if (urlFilter || urlFilter == ''){
        $("#table-filter").val(urlFilter);
        $("#table-filter").focus();
    }
    else {
        $('#frm :input:enabled:visible:first').focus();

    }
}


function resizeIframe(){
    // This also needs to be called when the vewport changes!
    var frame = document.getElementById('popup-frame');
    frame.style.height = frame.contentWindow.document.body.scrollHeight + 20 + "px";
    frame.style.maxHeight = (window.innerHeight - 150) + "px";
}


function render_history(){
    $('.history-toggle').click(function(){
        //console.log("FIRST!");
        if($(this).attr('expanded') != 'true'){
            //$(this).text($(this).text().replace("+","-"));
            $(this).attr('expanded', 'true');
            $(this).siblings('.history-item:not(:first)').slideDown();
            $(this).text("-");
        }
        else{
            //$(this).text($(this).text().replace("-","+"));
            $(this).attr('expanded', 'false');
            $(this).siblings('.history-item:not(:first)').slideUp();
            $(this).text("+");
        }

    });
}





function renderInputWidths(){
    //Finishes rendering the form's inputs based on class information
    //console.log("renderInputWidths");
    $('.form-control').each(function(){

        // The rules here are slightly different for radio select
        if ($(this).hasClass("radio-select")) {
            // puts the radio select div wrapper into the preceding paragraph
            // that all form fields should be inside, also remove the extra
            // blank one
            // THIS MUST HAPPEN FIRST
            let possibleEmptyP = $(this).next("p");
            if (possibleEmptyP.text().trim() === '') {
                possibleEmptyP.remove();
            }

            $(this).prev().append($(this));

            // ---------------------------------------------------------

            var main_label_text = $(this).parent().find("label").first().text().toLowerCase();

            var parent = $(this).parent("p");
            var width = $(this).find("input").first().attr("width");
            var clear = $(this).find("input").first().attr("clear");

            //console.log("This", $(this));
            //console.log("Parent", parent);
            //console.log("Width", width);
            //console.log("Clear", clear);

            // Remove the width information from the inputs, we don't want
            // render_widths to see that
            // also need to move the input next to the label, not inside it...
            $(this).find("input").each(function() {
                $(this).removeAttr("width");

                $(this).parent().parent().append($(this));

                // -----------------------------------------------------

                if ($(this).siblings("label").first().text().includes("---------") === true) {
                    $(this).siblings("label").first().text($(this).siblings("label").first().text().replace("---------", "No " + main_label_text));
                }
            });

            // apply inline css to all the labels and the inputs etc
            $(this).find("label").each(function() {
                $(this).css({
                    "display": "inline-block",
                    "margin-left": "2rem",
                    "margin-top": "0.5rem",
                    "margin-bottom": "0.5rem",
                    "white-space": "normal",
                });
            });

            $(this).find("input").each(function() {
                $(this).css({
                    "display": "inline-block",
                    "width": "2rem",
                    "position": "absolute",
                    "left": "-2.0rem",
                    "top": "0.1rem",
                });
            });


            parent.css({
                'padding-left':'0.25rem',
                'padding-right':'0.25rem',
                "overflow": "auto",
                //"width": "100%",
            });

            if (width){
                if (width.includes("rem")){
                    parent.css('width', width);
                }
                else{
                    // Set width %class, override to 100% for mobile
                    // apply standard width% if this is a popup
                    var is_popup = parent.siblings("input[name='popup']");
                    if (is_popup && is_popup.attr('value') == 'true'){
                        parent.css('width', width + "%");
                    }
                    else{
                        parent.addClass('w' + width);
                    }
                }

                if (clear){
                    parent.css({
                        'clear':clear,
                        'float':'left',
                    });
                }
                else{
                    parent.css({
                        'float' : 'left',
                    });
                }
            }
        }


        else {

            var parent = $(this).parent();
            var width = $(this).attr('width');
            var clear = $(this).attr('clear');
            var ml = $(this).attr("ml");
            var mr = $(this).attr("mr");

            parent.css({'padding-left':'0.25rem', 'padding-right':'0.25rem'});

            if (ml) {
                if (ml.includes("rem")){
                    parent.css('margin-left', ml);
                }
                else {
                    parent.css('margin-left', ml + "%");
                }
            }

            if (mr) {
                if (mr.includes("rem")){
                    parent.css('margin-left', mr);
                }
                else {
                    parent.css('margin-left', mr + "%");
                }
            }

            if (width) {
                if (width.includes("rem")){
                    parent.css('width', width);
                }
                else {
                    // Set width %class, override to 100% for mobile
                    // apply standard width% if this is a popup
                    var is_popup = parent.siblings("input[name='popup']");
                    if (is_popup && is_popup.attr('value') == 'true'){
                        parent.css('width', width + "%");
                    }
                    else{
                        parent.addClass('w' + width);
                    }
                }
            }

            if (clear){
                    parent.css({
                        'clear':clear,
                        'float':'left',
                    });
                }
                else{
                    // exclude list items from the float left...
                    //console.log($(this));
                    //console.log(parent);
                    if (parent.prop("nodeName") != "LI") {
                        parent.css({
                            'float' : 'left',
                        });
                    }
                }
        }

    });
}


function render_fileupload(){
    var upload_p = $('label[for=id_cn_file], label[for=id_doc_file], label[for=id_jf_file], label[for=id_trans_file], label[for=id_af_file]').parent();
    var anchor = upload_p.find('a');
    var label = upload_p.first('label');
    var new_anchor = "<a class='file-upload' target='_blank' href='"+ anchor.text() +"'>"+ anchor.text().split(/[/]+/).pop() +"</a>"
    if (upload_p.html() != null){
        if(upload_p.html().includes('Currently:')){
            upload_p.html("<label class='hide-after'>File:</label><div class='form-input'>" + new_anchor + "</div>");
        }
    }
}


function date_picker(){
    $(".datepicker").each(function(){

        $(this).attr("type", "date");

        /*
        if (!$(this).attr('disabled')){
            $(this).datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: "1900:" + new Date().getFullYear(),
                dateFormat: 'mm/dd/yy',
        	    defaultDate: new Date(Date.parse($(".datepicker").val())),
                buttonImage : "/static/images/calendar-alt-solid.svg",
                showOn :'button',
                buttonImageOnly : true,
                onClose : function() { $(this).focus(); }, //return focus to the textbox
            });
        }
        */
    });

    $('.datepicker').keyup(function(e){
      if (e.key.toUpperCase() == "T"){
        $(this).val(getDate());

        // dispatch event in case any other features are expecting an onchange
        // for example the feature that sets the day of the week on the NWC
        // crisis assessment form
        $(this)[0].dispatchEvent(new Event("change"));
      }
      else{
          if($(this).val().length >= 10){
              e.preventDefault();
              return false;
          }
      }
    });

    //$('.datepicker').attr("autocomplete", "off");
    //$('.datepicker').parent().css('position', 'relative');
}


function getDate(){
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();


    if (dd<10) {
        dd='0'+dd
    }

    if (mm<10) {
        mm='0'+mm
    }

    return yyyy + "-" + mm + "-" + dd;

}

function getTime(){
    var d = new Date();
    var hours = d.getHours();
    var minutes = d.getMinutes();
    var seconds = d.getSeconds();

    if (minutes < 10){
        minutes = "0" + minutes;
    }
    if (seconds < 10){
        seconds = "0" + seconds
    }

    return hours + ":" + minutes + ":" + seconds
}



function multi_select(){
    columns = 2;
    url = window.document.location.toString();

    if (url.includes("/client/client_settings")){
        columns = 2;
    }
    else if (url.includes("/client/emr_form")){
        columns = 1;
    }

    $('select[multiple]').multiselect({
		columns: columns,
		/*minHeight: 335,*/
		search: true,
	});

    $(".ms-search > input").addClass("form-control");
    $(".ms-options-wrap").parent().css("width", "100%");
}




function clear_form(){
    $('.btn-clear').click(function(){
		var textboxes = $(this).closest("form").find('input');
		var selects = $(this).closest("form").find("select");
        var checks = $(this).closest("form").find("input[type=checkbox]");

        textboxes.each(function(o){
            if ($(this).attr("name") != "csrfmiddlewaretoken"){
                $(this).val('');
            }});

        selects.each(function(o){
            if ($(this).attr("name") != "csrfmiddlewaretoken"){
                var first_opt = $(this).find("option:first-child").val()
                $(this).val(first_opt);
            }});

        checks.each(function(o){
            if ($(this).attr("name") != "csrfmiddlewaretoken"){
                $(this).prop("checked", false);
            }});

	});
}


function datatables(){
    $('.listview-table').DataTable(
		{
			paging: false,
			info:false,
		}
	);

	$('.listview-table-page-no-order').DataTable(
		{
			paging: true,
			ordering: false,
			info:false,
		}
	);

	$('.listview-table-page').DataTable(
		{
			paging: true,
      "pageLength": 100,
			info:false,
		}
	);

    $('.listview-table-page-10').DataTable(
		{
			paging: true,
            "pageLength": 10,
            ordering:true,
			info:false,
		}
	);

	$('.listview-table-no-order').DataTable(
		{
			ordering:false,
			searching:true,
			paging:false,
			info:false,
		});
	$('.listview-table-no-order-no-search').DataTable(
		{
			ordering:false,
			searching:false,
			paging:false,
			info:false,
		}
	);

    $('.listview-table-no-search').DataTable(
		{
			ordering:true,
			searching:false,
			paging:false,
			info:false,
		}
	);

	$('.listview-table-no-order-no-search-no-info').DataTable(
		{
			ordering:false,
			searching:false,
			paging:false,
			info:false,
		}
	);

    $('table').each(function(ele){
		$(this).addClass('table table-striped table-hover table-condensed');
	});

    /*
    This has been disabled in favor of a pure-css solution
        tr:hover > td > a

    $('tr').hover(function(){
        $(this).find('a').css({
            'color' : 'black',
            'text-decoration' : 'underline',
            })

    }, function(){
        $(this).find('a').css({
            'color' : '#333',
            'text-decoration' : 'none',
            })
    });
    */

    $('#select-all').click(function(){
        if ($(this).prop('checked')){
            $(this).parents('table').find('.list-check').not('.list-check[disabled=disabled]').prop('checked', true);
        }
        else{
            $(this).parents('table').find('.list-check').not('.list-check[disabled=disabled]').prop('checked', false);
        }
    });

    $('.list-check').click(function(){
        if($(this).prop('checked') == false){
            $('#select-all').prop('checked', false);
        }
    });
}


function closeCaseNote(){
    showModal(
        titleText="Close Case Note",
        bodyText="Are you sure you want to close this case note?\nOnce closed, this case note will be read-only and you will not be able to make any changes.",
        primaryText="Close case note",
        secondaryText="Go back",
        primaryAction=function(){
            $('.frm-emr-form').prepend("<input type='hidden' name='close_casenote' value='close_casenote'></input>");
            $('.frm-emr-form').submit();
        },
        secondaryAction=hideModal,
        opacity=0.8
    );
}


function reopenCaseNote(id, type=null){
    let customHtml = '';
    if (type == 'emr') {
        customHtml = `
            <form id='reopen_casenote_form' method='post' action='/client/emr_casenote/reopen/`+ id +`/'>
                <label class='hide-after' for='reopen_reason'>Reopen reason</label>
                <textarea id="id_reopen_reason" name="reopen_reason" required cols='2' class='form-control'></textarea>
                {% csrf_token %}
            </form>
        `;
    }


    showModal(
        titleText="Reopen Case Note",
        bodyText="Please indicate why the note needs to be reopened.",
        primaryText="Reopen Case Note",
        secondaryText="Cancel",
        primaryAction=function(){
            if (type == 'emr') {
                const isValid = $("#reopen_casenote_form").get()[0].reportValidity();
                if (!isValid) {
                    //alert("A reason is required to reopen a case note");
                    $("#nova-modal-primary").removeAttr("disabled");
                }
                else {
                    $("#reopen_casenote_form").submit();
                }

            }
            else {
                window.location = "/client/casenote/reopen/" + id + "/";
            }

        },
        secondaryAction=hideModal,
        opacity=0.8,
        loadAction=null,
        customHtml=customHtml,
    );
}


function showPopup(url, width=null){
    // This function renders the popup modal which is an I frame that gets
    // opened to the URL passed to this function
    // Can specify a width. If the screen is in desktop mode (1200px and up),
    // the popup can be wider. available widths are, "Wide", "Wider", "Widest"

	var overlay = $('#popup-overlay');
	var frame = $('.popup-frame');

    if (width != null){
        $('.popup-wrapper').addClass(width);
    }
    else {
        $('.popup-wrapper').removeClass('wide');
        $('.popup-wrapper').removeClass('wider');
        $('.popup-wrapper').removeClass('widest');
    }

	if(url.indexOf("?") != -1){
		frame.attr('src', url + "&popup=true");
	}
	else{
		frame.attr('src', url + "?popup=true");
	}
    $('body').addClass('modal-open');
	overlay.fadeIn(EASE_IN, function(){
		frame.focus();
	});

}

// This is not being used because the session history list feature
// isn't being used
function history_popup(){
    if (getUrl('history_popup')){
        // Find the matching popup command and trigger it?
        showPopup(getUrl('history_popup'));
    }
}

function render_popup(){
    //this function is called from the popup iframe
    //and puts its hidden title text into the iframe's wrapper
    //console.log("render_popup");
    if (getUrl('popup') == "true"){
		var title = $('h3').first();
		window.parent.setPopTitle(
            title.text(),
            hideDelete={% if hide_delete is True %}true{% else %}false{% endif %},
            hideCancel={% if hide_cancel is True %}true{% else %}false{% endif %},
            hideSave={% if hide_save is True %}true{% else %}false{% endif %},
        );
		title.css('display', 'none');
		$('body').css('background', 'white');
	}
}

// This function should be renamed because it does more than just set the title
function setPopTitle (title, hideDelete=false, hideCancel=false, hideSave=false){
    console.log("setPopTitle('" + title  +"')", hideDelete, hideCancel, hideSave);
    if (title == null || title == undefined){
        return;
    }
	//console.log("SetPopTitle("+title+")");
	var poptitle = $('.popup-title>h3').html(title);
    if(title.startsWith('New')){
        $('.btn-group-popup .btn-delete').css('display', 'none');
    }

    // TODO update these to use the new hideDelete, hideCancel, hideSave
    else if (title.startsWith('Select')){
        $('#popup-save, #btn-delete, #btn-delete-pretty').css('display', 'none');
    }
    else if (title.startsWith('Change Open Fiscal From')){
        $('.btn-group-popup .btn-delete, #btn-delete-pretty').css('display', 'none');
    }
    else if (title == "Edit Message") {
        $('.btn-group-popup .btn-delete, #btn-delete-pretty').css('display', 'none');
    }
    else if (title == "Route to Mental Health Professional") {
        $('.btn-group-popup #btn-delete, #btn-delete-pretty').css('display', 'none');
        $('.btn-group-popup #btn-cancel').css('display', 'none');
    }
    else{
        $('.btn-group-popup .btn-delete, #btn-delete-pretty').css('display', 'block');
    }


    // This is the new and much better way of doing things, just provide
    // hide_delete: True to hide the delete button, same for cancel and save
    // in the view
    if (hideDelete) {
        $('.btn-group-popup #btn-delete, #btn-delete-pretty').css('display', 'none');
    }
    if (hideCancel) {
        $('.btn-group-popup #btn-cancel').css('display', 'none');
    }
    if (hideSave) {
        $('#popup-save').css('display', 'none');
    }

    let s_title = title.split(' ');
    if (title == "Route to Mental Health Professional") {
        $('#popup-save').text("Send");
    }
    else if (s_title.length > 1){
        //console.log("s_title", s_title);
        $('#popup-save').text("Save " + s_title.splice(1).join(' '));
    }
    else {
        $('#popup-save').text("Save");
    }

    $('.popup-footer').css('visibility', 'visible');
    $('.btn-group-popup').css('display', 'block');
}


function closePopup(){
	var overlay = $('#popup-overlay');
	var frame = $('.popup-frame');

    frame.attr('src', 'about:blank');

    $('.btn-group-popup').css('display', 'none');

	overlay.fadeOut(EASE_OUT, function(){
		$('.popup-title>h3').html('')
        $('.btn-group-popup .btn-delete').css('display', 'none');
        $('#popup-save').text("");
        $('.popup-footer').css('visibility', 'hidden');
	});

  $('body').removeClass('modal-open');
}






function _lockPopup(){
    document.getElementById('popup-save').style.display = 'none';
}

//used by the popup to save and close
function saveFormPopup(){
    //console.log("saveFormPopup()");
	document.getElementById('popup-frame').contentWindow._saveFormPopup();
}


//because we are reaching into the iframe from the popup window
function _saveFormPopup(){
	//console.log("_saveFormPopup()");
	var frm = $('#frm');
    $("#btn-return").click();

    // There seems to be an issue with edge browser where it's sending this
    // request more than once.
    frm.submit(function() {
        $("#btn-return").prop("disabled", true);
    });

    $("#btn-return").prop("disabled", true);

    // Give a short time out in case there are validation issues
    setTimeout(function() {
        $("#btn-return").prop("disabled", false);
    }, 500)

    return false;
}


function renderFormSubmit() {
    var frm = $('#frm');
    if (frm.length) {
        frm.submit(function() {
            $("#btn-return").prop("disabled", true);
        });

        $("#btn-return").prop("disabled", true);

        // Give a short time out in case there are validation issues
        setTimeout(function() {
            $("#btn-return").prop("disabled", false);
        }, 500)
    }
}

function deleteFormPopup(){
    //Outside of the iFrame, going into the iFrame
	//console.log("deleteFormPopup()");
	document.getElementById('popup-frame').contentWindow._deleteFormPopup();
}


function _deleteFormPopup(){
    //Within the iFrame
	//console.log("_deleteFormPopup()");
	deleteDialog(true);
}

function deleteForm(){
	//console.log("deleteForm()");
	deleteDialog();
}

function deleteDialog(isPopup){
    //console.log("deleteDialog");
    window.parent.showModal(
        titleText="Delete Record",
        bodyText="Are you sure you want to delete this record?<br><br><div class='center'><strong>THIS ACTION CANNOT BE UNDONE</strong></div>",
        primaryText="Go back",
        secondaryText="Delete record",
        primaryAction=parent.hideModal,
        deleteDialogDelete,
        opacity=0.8
        )
}

function deleteDialogDelete(){
    window.parent.hideModal();
    var parent_form = $("input[name='delete']").parents('form').first();
    parent_form.attr('novalidate', '');
    $("input[name='delete']").click();

}




// Send ajax command and put the result into an element which is the selector param
// Used for things like the toolbar
function ajaxGet(url, errorMsg, selector){
    if (!errorMsg){
        errorMsg = "Ajax Error for URL: " + url;
    }

    // Check if the element even exists (it won't for cc_reports)
    // If it doesn't do nothing
    if (document.getElementById("toolbar") == null) {
        return;
    }


    $.ajax({
        method: "GET",
        url:url,
        success: function(data){
            $(selector).html(data);
        }

    }).fail(function(data){
        $(selector).html(data);
    });
}


function render_autocomplete(){
    $('.lookup').each(function(){
        // TODO move the keypress focusout code to a seperate single function

        let ele = $(this);
        ele.focusout(function(){
            value_target = $('#id_' + $(this).attr('target'));
            //console.log("TARGET    : " + value_target.attr("id"));
            // prevent user from selecting a non existing lookup item
            //console.log("AUTO_NAME : " + value_target.attr('auto_name'));
            //console.log("THIS VALUE: " + $(this).val());

            if (value_target.attr('auto_name') != undefined
                &&
                    (value_target.attr('auto_name').trim().localeCompare($(this).val().trim()) == 0
                        ||
                        value_target.attr('auto_name').trim() == $(this).val().trim()
                    )

                ){
                //console.log("MATCH!");
            }
            else {
                //console.log("Auto name does not match the selected value. Erasing!");
                value_target.val('');
                value_target.attr('auto_name','');
                $(this).val('');
                // if this is an account, make sure to clear out any trans_types
                if (value_target.attr("id") == "id_account"){
                    $("#id_trans_type").removeAttr("auto-update-value");
                }

                // if this is a vendor and we're in the auth screen,
                // clear out the contract select
                if (window.location.pathname.includes("/accounting/authorization") && value_target.attr("id") == "id_vendor"){
                    $("#id_contract").html("<option value selected>---------</option>");
                }
            }
        });

        ele.keydown(function(e){
            if (e.keyCode == 13){
                value_target = $('#id_' + $(this).attr('target'));
                // prevent user from selecting a non existing lookup item

                if (value_target.attr('auto_name') != undefined
                &&
                    (value_target.attr('auto_name').trim().localeCompare($(this).val().trim()) == 0
                        ||
                        value_target.attr('auto_name').trim() == $(this).val().trim()
                    )

                    ){
                //console.log("MATCH!");
                }
                else{
                    value_target.val('');
                    value_target.attr('auto_name','');
                    $(this).val('');
                    // if this is an account, make sure to clear out any trans_types
                    if (value_target.attr("id") == "id_account"){
                        $("#id_trans_type").removeAttr("auto-update-value");
                    }

                    // if this is a vendor and we're in the auth screen,
                    // clear out the contract select
                    if (window.location.pathname.includes("/accounting/authorization") && value_target.attr("id") == "id_vendor"){
                        $("#id_contract").html("<option value selected>---------</option>");
                    }
                    return false;
                }
            }
        });

        ele.autocomplete({
    		autoFocus: false,
            delay: AUTOCOMPLETE_DELAY,
            minLength: 1,
            position : { collision: "flip"},
    		source: function(request, response){
                //console.log("REQUEST TERM");
                //console.log(request.term);
                if (ele.attr("parent_id") != undefined && ele.attr("parent_id") != null && ele.attr("parent_mod") != undefined && ele.attr("parent_mod") != null && ele.attr("parent_app") != undefined && ele.attr("parent_app") != null){
                    var url = "/config/lookup?mod="+ele.attr("mod")+"&fields="+ele.attr("fields")+"&app="+ele.attr("app")+"&order="+ele.attr("order")+"&query="+escape(request.term)+"&parent_id="+ele.attr("parent_id")+"&parent_mod="+ele.attr("parent_mod")+"&parent_app="+ele.attr("parent_app")+"&path="+window.location.pathname;
                }
                else {
                    var url = "/config/lookup?mod="+ele.attr("mod")+"&fields="+ele.attr("fields")+"&app="+ele.attr("app")+"&order="+ele.attr("order")+"&query="+escape(request.term)+"&path="+window.location.pathname;
                }

    			$.ajax({
    				method: "GET",
    				url : url,
    				success:function(data){
    					parsed_data = JSON.parse(data);
    					//console.log(parsed_data);
    					response(parsed_data);
    				},
    			});
    		},
    		select: function(event, ui){
                event.preventDefault();

                var value_target = $('#id_' + ele.attr('target'));
                //console.log("VALUE_TARGET: " + ele.attr('target'));

                // If the item value is null, we can stop right here
                //console.log("Selected value:", ui.item.value);
                //console.log("Selected label:", ui.item.label);
                //console.log(ui.item);
                if (ui.item.value == "No results found") {
                    //console.log("Bailing and setting value to blank");
                    //value_target.val("");
                    return;
                }


                ele.val(ui.item.label);
                ele.trigger("change");

                value_target.val(ui.item.value);
                value_target.attr('auto_name', ui.item.label);
                value_target.trigger("change");




                //console.log("VALUE_TARGET: " + ele.attr('target'));
                //console.log("SETTING AUTO_NAME TO: " +ui.item.label);
                //console.log("AUTO NAME SET TO: " + value_target.attr('auto_name'));

                // This is an exception to the normal operation. There may be
                // some cases where we want to use this lookup but have it be
                // independent of a form and we want to be taken directly
                // to the record once it's selected. In this case, the id of
                // the textbox needs to start with 'instant'
                // A good example of this being used is in the caseworker
                // login screen where they can quick-access a client
                //console.log("This id!: ", $(this).attr("id"));
                if ($(this).attr("id").startsWith("instant")) {
                    // Depending on the model type, route the user in here
                    if ($(this).attr("mod") == "Client") {
                        console.log("Redirecitng to client with id of", ui.item.value);
                        window.location.href = "/client/" + ui.item.value;
                    }
                }


                // Sets a value on a 2nd field
                // Also sets a attribute on the altered field
                if (ui.item.c_target != null && ui.item.c_target != undefined && ui.item.c_value != null && ui.item.c_value != undefined){
                  $(ui.item.c_target).val(ui.item.c_value);
                  $(ui.item.c_target).attr("auto-update-value", ui.item.c_value);
                  $(ui.item.c_target).trigger("change");

                  // If the user changes the value from the auto-selected value, show a warning
                  $(ui.item.c_target).unbind("change");
                  $(ui.item.c_target).on("change", function(e){
                      if ($(this).attr("auto-update-value") && $(this).val() != $(this).attr("auto-update-value") ){
                        // by default, just show a warning icon.
                        // if it's a transaction type, do a popup
                        // if they do, then do nothing, otherwise revert
                        //console.log("THE ID: " + $(this).attr("id"));

                        checkForTransTypeMismatch($(this));


                          $("label[for="+$(this).attr("id")+"]").addClass("changed-from-auto");
                      }
                      else{
                          $("label[for="+$(this).attr("id")+"]").removeClass("changed-from-auto");
                      }

                  });
                }
                else if (ui.item.s_target != null && ui.item.s_target != undefined && ui.item.s_value != null && ui.item.s_value != undefined){
                    // Making a selection at this lookup will change the
                    // option values in a related select element
                    // don't forget to set the queryset on the edit view!!
                    var sele_target = ui.item.s_target;
                    //console.log("SELE TARGET" + sele_target);
                    var options = JSON.parse(ui.item.s_value);
                    var html = "<option value selected>---------</option>";
                    for (var i = 0; i < options.length; i++){
                        var opt = options[i];
                        html += "<option value='"+opt.id+"'>"+opt.text+"</option>";
                        //console.log(opt);
                    }
                    $(sele_target).html(html);
                    //console.log("OPTIONS: " + options);
                    //console.log(options);
                }
                else if (ui.item.multi_fill != null) {
                    // This is a newer, updated way to auto-fill multiple related
                    // fields. This is coded in the lookup_data def
                    for (var i = 0; i < ui.item.multi_fill.length; i++){
                        var mi = ui.item.multi_fill[i];

                        $(mi.target).val(mi.value);
                        $(mi.target).trigger("change");

                        //$(mi.target).attr("auto_name", mi.value);

                        // if this is a lookup, make sure to set it's
                        // hidden target's auto_name value!
                        if (mi.target.includes("_lookup")){
                            var hidden_field = $(mi.target.replace("_lookup", ""));
                            //console.log(hidden_field.attr("id"));
                            hidden_field.attr("auto_name", mi.value);
                        }




                    }


                }
    		},
            close: function(event, ui){

            },
    	});
    });
}

var DND_ID_FIRST = "";
var DND_ID_LAST = "";
var DND_ID_BIRTHDATE = "";
function duplicateNameDetect(id_first, id_last, id_birthdate) {
    console.log("Duplicate name detection active");
    DND_ID_LAST = "#" + id_last;
    DND_ID_FIRST = "#" + id_first;
    DND_ID_BIRTHDATE = "#" + id_birthdate;


    $(DND_ID_LAST).keyup(throttle(runDuplicateNameDetect, 300));
    $(DND_ID_FIRST).keyup(throttle(runDuplicateNameDetect, 300));

    $(DND_ID_BIRTHDATE).keyup(throttle(runDuplicateNameDetect, 300));
    $(DND_ID_BIRTHDATE).change(throttle(runDuplicateNameDetect, 300));
    $(DND_ID_BIRTHDATE).focusout(runDuplicateNameDetect);
}



function runDuplicateNameDetect(event) {
    console.log("duplicateNameDetect(" + event.target.id + " - " + event.type + ")");

    post_data = {};
    post_data["csrfmiddlewaretoken"] = "{{ csrf_token }}";
    post_data["last_name"] = $(DND_ID_LAST).val().trim();
    post_data["first_name"] = $(DND_ID_FIRST).val().trim();
    post_data["birthdate"] = $(DND_ID_BIRTHDATE).val();

    // Ignore incomplete name data
    if (post_data["last_name"].length < 3 && post_data["first_name"].length < 3) {
        resetDuplicateNameNotify();
        return;
    }

    //console.log(post_data);

    $.ajax({
        method: "POST",
        url: "/client/ajax_duplicate_name_detect/",
        data: post_data,
        success: function(data) {
            // parse json response and decide what to do
            //console.log("Duplicate detect request success");

            if (data != "OK") {
                // possible matches found
                json_data = JSON.parse(data);
                duplicateNameNotify(json_data);
            }
            else {
                // No match found in the database, reset the alert.
                resetDuplicateNameNotify();
            }

        },
    }).fail(function(err_data) {
        console.log("Duplicate detect error");
        console.log(err_data);
    });
}

var DUPLICATE_NAME_MODAL_BODY = "";
var DUPLICATE_NAME_MORE_THAN_ONE_FOUND = false;
function duplicateNameNotify(data) {
    let label_first = $("label[for=" + DND_ID_FIRST.replace("#", "") + "]");
    let label_last = $("label[for=" + DND_ID_LAST.replace("#", "") + "]");

    resetDuplicateNameNotify();

    label_first.text("");
    label_last.text("");

    label_last.addClass("hide-after");
    label_first.addClass("hide-after");

    DUPLICATE_NAME_MODAL_BODY += "<strong>You entered:</strong><br>'"+$(DND_ID_LAST).val()+", "+$(DND_ID_FIRST).val()+"'<br><br>";

    console.log("Data length", data.length);
    if (data.length > 1) {
        DUPLICATE_NAME_MORE_THAN_ONE_FOUND = true;
    }

    if (DUPLICATE_NAME_MORE_THAN_ONE_FOUND === true) {
        DUPLICATE_NAME_MODAL_BODY += "<strong>Similar client names found:</strong><br>"
    }
    else {
        DUPLICATE_NAME_MODAL_BODY += "<strong>Similar client name found:</strong><br>"
    }

    for (const key in data) {
        let client = data[key];

        // If the detection is triggered on a duplicated birthdate, make
        // that underlined so it's obvious to the user
        let birthdate = "";
        if (client.same_birthdate === true) {
            birthdate = "<u>" + client.birthdate + "</u>";
        }
        else {
            birthdate = client.birthdate;
        }

        DUPLICATE_NAME_MODAL_BODY += "<a target='_blank' href='/client/" + client.id + "/'>" + client.last_name + ", "
        + client.first_name +" - "+ birthdate + "</a><br>";
    }

    label_last.prepend(
        "<span id='duplicate-name-notify' class='duplicate-name-notify'>" +
        "    <a href='javascript:showDuplicateNameModal();'>This client may already exist</a>" +
        "</span>"
    );

    // Also need to hide the form save button and replace it with a dummy
    // button that will fire up a modal that requires user interaction
    $("#btn-return").before("<a id='faux-save-btn' href='javascript:showDuplicateNameSaveModal()' class='btn btn-primary'>Save</a>");
    $("#btn-return").css("display", "none");
}

function showDuplicateNameModal() {
    showModal(
        titleText="This Client May Already Exist",
        bodyText=null,
        primaryText="Go Back",
        secondaryText=null,
        primaryAction=hideModal,
        secondaryAction=null,
        opacity=0.8,
        loadAction=null,
        customHtml=DUPLICATE_NAME_MODAL_BODY
    );
}

function showDuplicateNameSaveModal() {
    // This is shown when the user attempts to save a client who
    // may already exist in Nova
    showModal(
        titleText="This Client May Already Exist",
        bodyText=null,
        primaryText="Go Back",
        secondaryText="Proceed and Save New Client",
        primaryAction=hideModal,
        secondaryAction=function() { $("#frm").submit() },
        opacity=0.8,
        loadAction=null,
        customHtml=DUPLICATE_NAME_MODAL_BODY
    );
}

function resetDuplicateNameNotify() {
    DUPLICATE_NAME_MODAL_BODY = "";
    DUPLICATE_NAME_MORE_THAN_ONE_FOUND = false;
    let label_first = $("label[for=id_first_name]");
    let label_last = $("label[for=id_last_name]");
    $("#duplicate-name-notify").remove();

    label_first.text("First");
    label_last.text("Last");

    label_last.removeClass("hide-after");
    label_first.removeClass("hide-after");

    $("#btn-return").css("display", "inline-block");
    $("#faux-save-btn").remove();
}







/* Google Map API scripts. Currently not in use */
{% comment %}
/*
function initAutocomplete() {
    if ($('#add_street').length){
        autocomplete = new google.maps.places.Autocomplete((document.getElementById('add_street')), {types: ['geocode']});
        autocomplete.setComponentRestrictions({'country': ['us',]});
        autocomplete.addListener('place_changed', fillInAddress);
    }
}

function fillInAddress() {
    //console.log("fillInAddress()");
    // Get the place details from the autocomplete object.
    var place = autocomplete.getPlace();
    // Get each component of the address from the place details
    // and fill the corresponding field on the form.
    for (var i = 0; i < place.address_components.length; i++) {
        var addressType = place.address_components[i].types[0];
        if (addressType == 'street_number'){
            street_number = place.address_components[i]['short_name'];
            //console.log("street_number: " + street_number);
        }
        else if (addressType == 'route'){
            street = place.address_components[i]['long_name'];
            //console.log("street: " + street);
        }
        else if (addressType == 'locality'){
            city = place.address_components[i]['long_name'];
            //console.log("city: " + city);
        }
        else if (addressType == 'administrative_area_level_1'){
            state = place.address_components[i]['short_name'];
            //console.log("state: " + state);
        }
        else if (addressType == 'postal_code'){
            zip = place.address_components[i]['short_name'];
            //console.log("zip: " + zip);
        }
    }

    $('#add_state').val(state);
    $('#add_street').val(street_number + " " + street);
    $('#add_city').val(city);
    $('#add_zip').val(zip);
    $('#add_apt').val('');
}

function geolocate() {
    //console.log("geolocate()");
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
        });
    }
}
*/
{% endcomment %}


function getUrl(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if (results == null)
        return null;
    else
        return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function setUrl(paramName, paramValue, _url=null)
{
    //console.log(paramName, paramValue, url);
    if (_url) {
        var url = _url
    }
    else {
        var url = window.location.href;
    }


    if (paramValue == null) {
        paramValue = '';
    }
    var pattern = new RegExp('\\b('+paramName+'=).*?(&|#|$)');
    if (url.search(pattern)>=0) {
        return url.replace(pattern,'$1' + paramValue + '$2');
    }
    url = url.replace(/[?#]$/,'');
    return url + (url.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue;
}
