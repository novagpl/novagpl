import urllib.parse

from django.core.paginator import Paginator
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from annoying.functions import get_object_or_None


from utils import (
    access_control, save_confirm, save_audit, delete_confirm, delete_audit,
    format_errors, set_autocomplete)

from .models import (SacCheckWriteFile, SacCheckWriteLine, SacPerson, SacServiceType)
from client.models import Client, Service
from accounting.models import Vendor
from .forms import (
    SacwisReplPersonFilterForm, SacwisLinkPersonToClientFilterForm,
    SacwisReplServiceTypeFilterForm, SacwisNovaServiceFilterForm,
    SacwisNovaServiceForm, SacwisLinkProviderIdToVendorFilterForm,
)







@access_control("sacwis_import_mgmt")
def create_vendor_from_sac_checkline(request):
    repl_person_id = request.POST.get("repl_person_id")
    instance = get_object_or_404(SacPerson, id=repl_person_id)

    vendor_name = ""
    if instance.first_name and instance.last_name and instance.middle_initial:
        vendor_name = f"{instance.last_name}, {instance.first_name} {instance.middle_initial}"
    elif instance.last_name and instance.first_name:
        vendor_name = f"{instance.last_name}, {instance.first_name}"
    else:
        if not instance.last_name:
            raise Exception("Cannot create vendor without a name")
        vendor_name = instance.last_name

    vendor = Vendor(
        name=vendor_name,
    )

    save_audit(request=request, obj=vendor)

    instance.nova_vendor = vendor
    instance.save()


    return save_confirm(
        request=request,
        instance=save_audit(request=request, obj=instance),
        path_override=f"/sacwis/link_to_vendor_mgmt/{repl_person_id}/")






@access_control("sacwis_import_mgmt")
def unlink_sac_provider_id_from_vendor(request):
    # Removes the relationship between the Nova Vendor and the REPL person
    if not request.POST:
        raise Exception("Incorrect request type")

    sac_check_line_id = request.POST.get("sac_check_line_id")
    nova_vendor_id = request.POST.get("nova_vendor_id")

    vendor = get_object_or_404(Vendor, id=nova_vendor_id)
    checkline = get_object_or_404(SacCheckWriteLine, id=sac_check_line_id)

    vendor.sac_provider_id = None

    return save_confirm(
        request=request,
        instance=save_audit(request=request, obj=vendor),
        path_override=f"/sacwis/link_to_vendor_mgmt/{checkline.id}/?filter_name={checkline.payee_name}")






@access_control("sacwis_import_mgmt")
def link_sac_provider_id_to_vendor(request):
    # This function performs the linking of a Nova vendor to a REPL Person
    # this function only accepts POST requests and redirects back to the
    # REPL person's link mgmt page
    if not request.POST:
        raise Exception("Incorrect request type")

    sac_check_line_id = request.POST.get("sac_check_line_id")
    nova_vendor_id = request.POST.get("nova_vendor_id")

    if not sac_check_line_id or not nova_vendor_id:
        raise Exception("Missing repl <-> nova vendor data")

    checkline = get_object_or_404(SacCheckWriteLine, id=sac_check_line_id)
    nova_vendor = get_object_or_404(Vendor, id=nova_vendor_id)

    nova_vendor.sac_provider_id = checkline.provider_id


    return save_confirm(
        request=request,
        instance=save_audit(request=request, obj=nova_vendor),
        path_override=f"/sacwis/link_to_vendor_mgmt/{sac_check_line_id}/")







@access_control("sacwis_import_mgmt")
def link_sac_provider_id_to_vendor_mgmt(request, sac_check_write_line_id):
    # This is a view to help manage linking a REPL provider_id to an existing
    # Nova Vendor. We use the check write line as the 'source' for this vendor
    instance = get_object_or_404(SacCheckWriteLine, id=sac_check_write_line_id)

    vendor = get_object_or_None(Vendor, sac_provider_id=instance.provider_id)

    title = "WiSACWIS Provider ID to Nova Vendor Link"

    page_number = request.GET.get("page")

    req_data = None
    if request.method == "GET":
        req_data = request.GET
    else:
        req_data = request.POST

    filter_name = req_data.get("filter_name")

    form = SacwisLinkProviderIdToVendorFilterForm(req_data, request=request)



    query = Q()

    if filter_name:
        query.add(Q(name__istartswith=filter_name), Q.AND)


    data = Vendor.objects.filter(query).order_by(
        "name")


    paginator = Paginator(data, 100)
    page = paginator.get_page(page_number)

    # Build a URI for the pages:
    params = {}
    if filter_name and filter_name != "None":
        params["filter_name"] = filter_name


    return render(request, "sacwis/link_sac_provider_id_to_vendor_mgmt.html", {
        "title": title,
        "instance": instance,
        "vendor": vendor,
        "page": page,
        "form": form,
        "filter": urllib.parse.urlencode(params),
        "query_string": urllib.parse.urlencode(params),
    })




@access_control("sacwis_import_mgmt")
def nova_service_edit(request, nova_service_id):
    if nova_service_id:
        instance = get_object_or_404(Service, id=nova_service_id)
        title = "Nova Service"

    if request.method == "GET":
        form = SacwisNovaServiceForm(instance=instance)

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"

        form = set_autocomplete(instance, form, "funding_profile")


        return render(request, "sacwis/nova_service_edit.html", {
            'form': form,
            'title': title,
            'popup': popup,
            "instance": instance,
            "hide_delete": True,
        })


    elif request.method == "POST":
        # Delete is not enabled for this view. If a service needs to be deleted
        # it should happen from the config.service view
        form = SacwisNovaServiceForm(request.POST, instance=instance)
        if form.is_valid():
            return save_confirm(request, save_audit(request, form))
        else:
            return format_errors(request, form=form)



@access_control("sacwis_import_mgmt")
def create_service_from_repl(request):
    repl_service_type_id = request.POST.get("repl_service_type_id")
    instance = get_object_or_404(SacServiceType, id=repl_service_type_id)

    service = Service(
        active=True,
        name=instance.description,
        spc_code=instance.spc_code,
    )

    save_audit(request=request, obj=service)

    instance.nova_service = service
    instance.save()

    return save_confirm(
        request=request,
        instance=save_audit(request=request, obj=instance),
        path_override=f"/sacwis/service_type/{repl_service_type_id}/")





@access_control("sacwis_import_mgmt")
def unlink_service_type(request):
    # POST request to unlink a REPL service type and a Nova service
    repl_service_type_id = request.POST.get("repl_service_type_id")
    repl_service_type = get_object_or_404(SacServiceType, id=repl_service_type_id)

    repl_service_type.nova_service = None
    repl_service_type.save()

    return save_confirm(
        request=request,
        instance=save_audit(request=request, obj=repl_service_type),
        path_override=f"/sacwis/service_type/{repl_service_type_id}/")



@access_control("sacwis_import_mgmt")
def link_service_type(request):
    # POST request to link a REPL service type and a Nova service
    if not request.POST:
        raise Exception("Incorrect request type")

    repl_service_type_id = request.POST.get("repl_service_type_id")
    nova_service_id = request.POST.get("nova_service_id")

    if not repl_service_type_id or not nova_service_id:
        raise Exception("Missing repl <-> nova service data")

    repl_service_type = get_object_or_404(SacServiceType, id=repl_service_type_id)
    nova_service = get_object_or_404(Service, id=nova_service_id)

    repl_service_type.nova_service = nova_service
    repl_service_type.save()

    return save_confirm(
        request=request,
        instance=save_audit(request=request, obj=repl_service_type),
        path_override=f"/sacwis/service_type/{repl_service_type_id}/"
    )


@access_control("sacwis_import_mgmt")
def sacservicetype_dtl(request, service_type_code):

    # Redirects to the service_type_dtl view but uses the unique sacwis
    # service code to find the service type
    found = get_object_or_404(SacServiceType, code=service_type_code)

    return HttpResponseRedirect(f"/sacwis/service_type/{found.id}/")


@access_control("sacwis_import_mgmt")
def service_type_dtl(request, service_type_id):
    # Interface for users to inspect repl service types and look up and link
    # them to Nova service. Also have the ability to create a Nova service
    # from the data in a repl service type
    instance = get_object_or_404(SacServiceType, id=service_type_id)
    page_number = request.GET.get("page")

    title = f"eWiSACWIS Service Type: {instance}"

    if request.method == "GET":
        form = SacwisNovaServiceFilterForm(request.GET, request=request)
        filter_data = request.GET
    else:
        form = SacwisNovaServiceFilterForm(request.POST, request=request)
        filter_data = request.POST
        page_number = 1


    filter_name = filter_data.get("filter_name")
    filter_spc_code = filter_data.get("filter_spc_code")
    filter_procedure_code = filter_data.get("filter_procedure_code")

    query = Q()
    query.add(Q(active=True), Q.AND)

    if filter_name:
        query.add(Q(name__icontains=filter_name), Q.AND)

    if filter_spc_code:
        query.add(Q(spc_code__istartswith=filter_spc_code), Q.AND)

    if filter_procedure_code:
        query.add(Q(procedure_code__istartswith=filter_procedure_code), Q.AND)

    data = Service.objects.filter(query).order_by(
        "name", "spc_code", "procedure_code")

    paginator = Paginator(data, 100)
    page = paginator.get_page(page_number)

    params = {}
    if filter_name and filter_name != "None":
        params["filter_name"] = filter_name
    if filter_spc_code and filter_spc_code != "None":
        params["filter_spc_code"] = filter_spc_code
    if filter_procedure_code and filter_procedure_code != "None":
        params["filter_procedure_code"] = filter_procedure_code


    return render(request, "sacwis/repl_service_type_dtl.html", {
        "title": title,
        "instance": instance,
        "form": form,
        "filter": urllib.parse.urlencode(params),
        "query_string": urllib.parse.urlencode(params),
        "page": page,
    })



@access_control("sacwis_import_mgmt")
def service_type_index(request):
    # Standard filter view of the repl service types
    title = "eWiSACWIS Service Type"
    page_number = request.GET.get("page")

    if request.method == "GET":
        form = SacwisReplServiceTypeFilterForm(request.GET, request=request)
        filter_data = request.GET
    else:
        form = SacwisReplServiceTypeFilterForm(request.POST, request=request)
        filter_data = request.POST
        page_number = 1



    filter_code = filter_data.get("filter_code")
    filter_category = filter_data.get("filter_category")
    filter_subcategory = filter_data.get("filter_subcategory")
    filter_description = filter_data.get("filter_description")
    filter_spc_code = filter_data.get("filter_spc_code")
    filter_order_by = filter_data.get("filter_order_by")
    filter_order_direction = filter_data.get("filter_order_direction")

    query = Q()

    query.add(Q(status='A'), Q.AND)

    if filter_code:
        query.add(Q(code__istartswith=filter_code), Q.AND)

    if filter_category:
        query.add(Q(category__istartswith=filter_category), Q.AND)

    if filter_subcategory:
        query.add(Q(subcategory__istartswith=filter_subcategory), Q.AND)

    if filter_description:
        query.add(Q(description__istartswith=filter_description), Q.AND)

    if filter_spc_code:
        query.add(Q(spc_code__istartswith=filter_spc_code), Q.AND)



    # This is the default order by. If the user selects something different
    # move the matching item to the front. If the user selects descending order
    # then change just the first one (the one they selected)
    order_by = ["nova_service", "code", "category", "subcategory", "description"]
    if filter_order_by in order_by:
        order_by.remove(filter_order_by)
        order_by.insert(0, filter_order_by)


    if filter_order_direction == "desc":
        order_by[0] = f"-{order_by[0]}"


    data = SacServiceType.objects.filter(query).order_by(*order_by)

    paginator = Paginator(data, 100)
    page = paginator.get_page(page_number)


    params = {}

    if filter_code and filter_code != "None":
        params["filter_code"] = filter_code
    if filter_category and filter_category != "None":
        params["filter_category"] = filter_category
    if filter_subcategory and filter_subcategory != "None":
        params["filter_subcategory"] = filter_subcategory
    if filter_description and filter_description != "None":
        params["filter_description"] = filter_description
    if filter_spc_code and filter_spc_code != "None":
        params["filter_spc_code"] = filter_spc_code
    if filter_order_by and filter_order_by != "None":
        params["filter_order_by"] = filter_order_by
    if filter_order_direction and filter_order_direction != "None":
        params["filter_order_direction"] = filter_order_direction


    return render(request, "sacwis/repl_service_type_index.html", {
        "title": title,
        "page": page,
        "form": form,
        "filter": urllib.parse.urlencode(params),
        "query_string": urllib.parse.urlencode(params),
    })



@access_control("sacwis_import_mgmt")
def create_client_from_repl(request):
    repl_person_id = request.POST.get("repl_person_id")
    instance = get_object_or_404(SacPerson, id=repl_person_id)

    client = Client(
        first_name=instance.first_name,
        middle_name=instance.middle_initial,
        last_name=instance.last_name,
        birthdate=instance.birthdate
    )

    save_audit(request=request, obj=client)

    instance.nova_client = client
    instance.save()


    return save_confirm(
        request=request,
        instance=save_audit(request=request, obj=instance),
        path_override=f"/sacwis/link_to_client_mgmt/{repl_person_id}/")



@access_control("sacwis_import_mgmt")
def unlink_repl_from_client(request):
    # Removes the relationship between the Nova client and the REPL person
    repl_person_id = request.POST.get("repl_person_id")
    repl_person = get_object_or_404(SacPerson, id=repl_person_id)

    repl_person.nova_client = None


    return save_confirm(
        request=request,
        instance=save_audit(request=request, obj=repl_person),
        path_override=f"/sacwis/link_to_client_mgmt/{repl_person_id}/")




@access_control("sacwis_import_mgmt")
def link_repl_to_client(request):
    # This function performs the linking of a Nova client to a REPL Person
    # this function only accepts POST requests and redirects back to the
    # REPL person's link mgmt page
    if not request.POST:
        raise Exception("Incorrect request type")

    repl_person_id = request.POST.get("repl_person_id")
    nova_client_id = request.POST.get("nova_client_id")

    if not repl_person_id or not nova_client_id:
        raise Exception("Missing repl <-> nova client data")

    repl_person = get_object_or_404(SacPerson, id=repl_person_id)
    nova_client = get_object_or_404(Client, id=nova_client_id)

    repl_person.nova_client = nova_client


    return save_confirm(
        request=request,
        instance=save_audit(request=request, obj=repl_person),
        path_override=f"/sacwis/link_to_client_mgmt/{repl_person_id}/")




@access_control("sacwis_import_mgmt")
def sac_link_repl_person_to_client_mgmt(request, sacid):
    # This takes the person's sacid, grabs the Id of that record and redirects
    # to the normal link_repl_person_to_client_mgmt
    sacperson = get_object_or_404(SacPerson, sacid=sacid)

    return HttpResponseRedirect(f"/sacwis/link_to_client_mgmt/{sacperson.id}/")


@access_control("sacwis_import_mgmt")
def link_repl_person_to_client_mgmt(request, person_id):
    # This is a view to help manage linking a REPL person to an existing
    # client in nova. The view uses some tricks to try to assist the user
    # in performing a client lookup, but there is also a straight up lookup
    # or filter for the user to do a direct lookup as well.

    instance = get_object_or_404(SacPerson, id=person_id)
    suggested = instance.get_nova_matches()

    title = "Link eWiSACWIS Person to Nova Client"

    page_number = request.GET.get("page")
    req_data = {}

    if request.method == "GET":
        form = SacwisLinkPersonToClientFilterForm(request.GET, request=request)
        req_data = request.GET
    else:
        form = SacwisLinkPersonToClientFilterForm(request.POST, request=request)
        req_data = request.POST

    filter_first_name = req_data.get("filter_first_name")
    filter_last_name = req_data.get("filter_last_name")
    filter_birthdate_from = req_data.get("filter_birthdate_from")
    filter_birthdate_to = req_data.get("filter_birthdate_to")

    query = Q()

    if filter_first_name:
        query.add(Q(first_name__istartswith=filter_first_name), Q.AND)
    if filter_last_name:
        query.add(Q(last_name__istartswith=filter_last_name), Q.AND)
    if filter_birthdate_from:
        query.add(Q(birthdate__gte=filter_birthdate_from), Q.AND)
    if filter_birthdate_to:
        query.add(Q(birthdate__lte=filter_birthdate_to), Q.AND)


    data = Client.objects.filter(query).order_by(
        "last_name", "first_name", "middle_name", "birthdate")

    if instance.nova_client:
        data = data.exclude(id=instance.nova_client.id)



    paginator = Paginator(data, 100)
    page = paginator.get_page(page_number)

    # Build a URI for the pages:
    params = {}
    if filter_first_name and filter_first_name != "None":
        params["filter_first_name"] = filter_first_name

    if filter_last_name and filter_last_name != "None":
        params["filter_last_name"] = filter_last_name

    if filter_birthdate_from and filter_birthdate_from != "None":
        params["filter_birthdate_from"] = filter_birthdate_from

    if filter_birthdate_to and filter_birthdate_to != "None":
        params["filter_birthdate_to"] = filter_birthdate_to



    return render(request, "sacwis/link_repl_person_to_client_mgmt.html", {
        "title": title,
        "instance": instance,
        "page": page,
        "form": form,
        "filter": urllib.parse.urlencode(params),
        "query_string": urllib.parse.urlencode(params),
        "suggested": suggested,
    })






@access_control("sacwis_import_mgmt")
def sacperson_dtl(request, sacid):
    # Just redirects to the person detail using the sacid instead of the
    # django ORM primary key
    instance = get_object_or_404(SacPerson, sacid=sacid)

    return HttpResponseRedirect(f"/sacwis/person/{instance.id}/")




@access_control("sacwis_import_mgmt")
def person_dtl(request, person_id):
    instance = get_object_or_404(SacPerson, id=person_id)

    title = f"eWiSACWIS Person {instance.get_name()}"

    return render(request, "sacwis/sac_person_dtl.html", {
        "title": title,
        "instance": instance,
    })



@access_control("sacwis_import_mgmt")
def person_index(request):
    title = "eWiSACWIS Person"
    page_number = request.GET.get("page")

    if request.method == "GET":
        form = SacwisReplPersonFilterForm(request.GET, request=request)

        filter_first_name = request.GET.get("filter_first_name")
        filter_last_name = request.GET.get("filter_last_name")
        filter_birthdate_from = request.GET.get("filter_birthdate_from")
        filter_birthdate_to = request.GET.get("filter_birthdate_to")
        filter_order_by = request.GET.get("filter_order_by")
        filter_order_direction = request.GET.get("filter_order_direction")
        filter_has_episodes = bool(request.GET.get("filter_has_episodes") == 'on')
        filter_is_linked = bool(request.GET.get("filter_is_lined") == 'on')
        # orderby choices include - last_name, birthdate

    else:
        form = SacwisReplPersonFilterForm(request.POST, request=request)

        filter_first_name = request.POST.get("filter_first_name")
        filter_last_name = request.POST.get("filter_last_name")
        filter_birthdate_from = request.POST.get("filter_birthdate_from")
        filter_birthdate_to = request.POST.get("filter_birthdate_to")
        filter_order_by = request.POST.get("filter_order_by")
        filter_order_direction = request.POST.get("filter_order_direction")
        filter_has_episodes = bool(request.POST.get("filter_has_episodes") == 'on')
        filter_is_linked = bool(request.POST.get("filter_is_linked") == 'on')
        # orderby choices include - last_name, birthdate
        page_number = 1



    query = Q()

    if filter_first_name:
        query.add(Q(first_name__istartswith=filter_first_name), Q.AND)
    if filter_last_name:
        query.add(Q(last_name__istartswith=filter_last_name), Q.AND)
    if filter_birthdate_from:
        query.add(Q(birthdate__gte=filter_birthdate_from), Q.AND)
    if filter_birthdate_to:
        query.add(Q(birthdate__lte=filter_birthdate_to), Q.AND)
    if filter_has_episodes:
        query.add(Q(sacepisode__isnull=False), Q.AND)
    if filter_is_linked:
        query.add(Q(nova_client__isnull=False), Q.AND)


    if not filter_order_by or filter_order_by == 'None':
        order_by = ["last_name", "first_name", "birthdate", "repl_update", "repl_insert"]
    else:
        if filter_order_by == "birthdate":
            order_by = ["birthdate", "last_name", "first_name", "repl_update", "repl_insert"]

        elif filter_order_by == "last_name":
            order_by = ["last_name", "first_name", "birthdate", "repl_update", "repl_insert"]

        elif filter_order_by == "repl_insert":
            order_by = ["repl_insert", "last_name", "first_name", "birthdate", "repl_update"]

        elif filter_order_by == "repl_update":
            order_by = ["repl_update", "last_name", "first_name", "birthdate", "repl_insert"]
        else:
            raise Exception(f"Order by of '{filter_order_by}' is not a valid choice")

    if filter_order_direction == "desc":
        order_by[0] = f"-{order_by[0]}"

    data = SacPerson.objects.filter(query).order_by(*order_by).distinct()


    paginator = Paginator(data, 100)
    page = paginator.get_page(page_number)

    # Build a URI for the pages:
    params = {}
    if filter_first_name and filter_first_name != "None":
        params["filter_first_name"] = filter_first_name

    if filter_last_name and filter_last_name != "None":
        params["filter_last_name"] = filter_last_name

    if filter_birthdate_from and filter_birthdate_from != "None":
        params["filter_birthdate_from"] = filter_birthdate_from

    if filter_birthdate_to and filter_birthdate_to != "None":
        params["filter_birthdate_to"] = filter_birthdate_to

    if filter_order_by and filter_order_by != "None":
        params["filter_order_by"] = filter_order_by

    if filter_order_direction and filter_order_direction != "None":
        params["filter_order_direction"] = filter_order_direction


    return render(request, "sacwis/sac_person_index.html", {
        "title": title,
        "page": page,
        "form": form,
        "filter": urllib.parse.urlencode(params),
        "query_string": urllib.parse.urlencode(params),
    })
    



@access_control("sacwis_import_mgmt")
def sac_check_write_index(request):
    check_data = SacCheckWriteFile.objects.all()

    return render(request, 'sacwis/sac_check_write_index.html', {
        'title': 'eWiSACWIS Check Write Files',
        'check_data': check_data,
    })


@access_control("sacwis_import_mgmt")
def sac_check_write_dtl(request, check_write_id):
    # Displays the status of the check write file along with all of its
    # lines in a list below and provides tools and shortcuts for users to help
    # manage auths
    write_file = get_object_or_404(SacCheckWriteFile, id=check_write_id)

    title = f"eWiSACWIS Check Write File"


    return render(request, "sacwis/sac_check_write_dtl.html", {
        #'form': form,
        'title': title,
        "instance": write_file,
    })
