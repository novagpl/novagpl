from django.conf import settings
from django import forms
from django.forms import (
    ModelForm, DateInput, DateField, Select, SelectMultiple,
    CharField, Textarea, TextInput, HiddenInput,
    ClearableFileInput, CheckboxInput, NumberInput,
    TimeInput, EmailInput, RadioSelect, BooleanField,
)

from utils import render_form, get_app_config

from client.models import Service



NULL_CHOICE = (None, "----------", )






class SacwisLinkProviderIdToVendorFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super().__init__(*args, **kwargs)

        render_form(self, hide_all_optional=True)


    filter_name = CharField(
        label="Name",
        required=False,
        widget=TextInput(attrs={
            "width": 30,
        })
    )





class SacwisNovaServiceForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        render_form(self, hide_all_optional=True)

    funding_profile_lookup = CharField(label='Funding profile', required=False, widget=TextInput(attrs={
        'width': 100,
        'clear': 'both',
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'FundingProfile',
        'fields': 'name, description',
        'order': 'name',
        'target': 'funding_profile',
        'autocomplete': 'off',
    }))

    class Meta:
        model = Service
        fields = (
            'active',
            'name',
            "funding_profile_lookup",
            'funding_profile',
            'spc_code',
            'procedure_code',
            'default_unit',
            "pps_brief_service",
            'pps_aoda',
            'pps_mh',
            "pps_core",
            "pps_b3",
            "pps_unit",
        )

        labels = {
            "spc_code": "SPC code",
        }

        widgets = {
            'active': CheckboxInput(attrs={'width': 50, 'clear': 'both'}),
            'name': TextInput(attrs={'width': 100, "clear": "left"}),
            'spc_code': TextInput(attrs={'width': 33.3}),
            'procedure_code': TextInput(attrs={'width': 33.3}),
            'default_unit': NumberInput(attrs={'width': 33.3}),
            "pps_brief_service": CheckboxInput(attrs={"width": 33}),
            "pps_aoda": CheckboxInput(attrs={"width": 33}),
            "pps_mh": CheckboxInput(attrs={"width": 33}),
            "pps_core": CheckboxInput(attrs={"width": 33}),
            "pps_b3": CheckboxInput(attrs={"width": 33}),
            "pps_unit": Select(attrs={"width": 33, "clear": "left"}),
            "funding_profile": HiddenInput(),
        }






class SacwisNovaServiceFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super().__init__(*args, **kwargs)
        render_form(self, hide_all_optional=True)


    filter_name = CharField(
        label="Name",
        required=False,
        widget=TextInput(attrs={"width": 20, }))

    filter_spc_code = CharField(
        label="SPC Code",
        required=False,
        widget=TextInput(attrs={"width": 20, }))

    filter_procedure_code = CharField(
        label="Procedure Code",
        required=False,
        widget=TextInput(attrs={"width": 20, }))







class SacwisReplServiceTypeFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super().__init__(*args, **kwargs)
        render_form(self, hide_all_optional=True)


    filter_code = CharField(
        label="Code",
        required=False,
        widget=TextInput(attrs={"width": 20, }))

    filter_category = CharField(
        label="Category",
        required=False,
        widget=TextInput(attrs={"width": 20, }))

    filter_subcategory = CharField(
        label="Subcategory",
        required=False,
        widget=TextInput(attrs={"width": 20, }))

    filter_description = CharField(
        label="Description",
        required=False,
        widget=TextInput(attrs={"width": 20, }))

    filter_spc_code = CharField(
        label="SPC code",
        required=False,
        widget=TextInput(attrs={"width": 20, }))

    order_by_choices = (
        ("code", "Code"),
        ("category", "Category"),
        ("subcategory", "Subcategory"),
        ("description", "Description"),
        ("spc_code", "SPC code"),
        ("repl_insert", "Created"),
        ("repl_update", "Updated")
    )
    filter_order_by = CharField(
        label="Order by",
        required=False,
        widget=Select(
            choices=order_by_choices,
            attrs={
                "width": 25,
                "clear": "left"
            })
    )

    filter_order_direction_choices = (
        ("asc", "Ascending"),
        ("desc", "Descending"),
    )
    filter_order_direction = CharField(
        label="Order direction",
        required=False,
        widget=Select(
            choices=filter_order_direction_choices,
            attrs={
                "width": 25,
            })
    )












class SacwisLinkPersonToClientFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super().__init__(*args, **kwargs)

        render_form(self, hide_all_optional=True)



    filter_last_name = CharField(
        label="Last name",
        required=False,
        widget=TextInput(attrs={
            "width": 30,
        })
    )

    filter_first_name = CharField(
        label="First name",
        required=False,
        widget=TextInput(attrs={
            "width": 30,
        })
    )

    filter_birthdate_from = DateField(
        label="Birthdate from",
        required=False,
        widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "clear": "left",
                "width": "auto",
                "class": "datepicker", }))

    filter_birthdate_to = DateField(
        label="Birthdate to",
        required=False,
        widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": "auto",
                "class": "datepicker", }))









class SacwisReplPersonFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super().__init__(*args, **kwargs)

        render_form(self, hide_all_optional=True)


    filter_last_name = CharField(
        label="Last name",
        required=False,
        widget=TextInput(attrs={
            "width": 30,
        })
    )

    filter_first_name = CharField(
        label="First name",
        required=False,
        widget=TextInput(attrs={
            "width": 30,
        })
    )

    filter_birthdate_from = DateField(
        label="Birthdate from",
        required=False,
        widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "clear": "left",
                "width": "auto",
                "class": "datepicker", }))

    filter_birthdate_to = DateField(
        label="Birthdate to",
        required=False,
        widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": "auto",
                "class": "datepicker", }))

    filter_has_episodes = BooleanField(
        label="Only show persons that have episodes",
        required=False,
        widget=CheckboxInput(attrs={
            "clear": "left",
            "width": "auto",
        })
    )

    filter_is_linked = BooleanField(
        label="Only show persons that are linked in Nova",
        required=False,
        widget=CheckboxInput(attrs={
            "width": "auto",
        })
    )


    order_by_choices = (
        ("last_name", "Last name"),
        ("birthdate", "Birthdate"),
        ("repl_insert", "Created"),
        ("repl_update", "Updated")
    )

    filter_order_by = CharField(
        label="Order by",
        required=False,
        widget=Select(
            choices=order_by_choices,
            attrs={
                "width": 25,
                "clear": "left"
            })
    )

    filter_order_direction_choices = (
        ("asc", "Ascending"),
        ("desc", "Descending"),
    )

    filter_order_direction = CharField(
        label="Order direction",
        required=False,
        widget=Select(
            choices=filter_order_direction_choices,
            attrs={
                "width": 25,
            })
    )


