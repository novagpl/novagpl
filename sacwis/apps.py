from django.apps import AppConfig


class SacwisConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sacwis'
