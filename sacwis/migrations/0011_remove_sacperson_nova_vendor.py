# Generated by Django 5.1.6 on 2025-03-06 20:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sacwis', '0010_sacperson_dad_id_sacperson_grandparent1_id_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sacperson',
            name='nova_vendor',
        ),
    ]
