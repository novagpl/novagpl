# Generated by Django 3.2.25 on 2025-02-18 17:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0667_remove_client_sacfk'),
        ('sacwis', '0005_rename_client_sacperson_nova_client'),
    ]

    operations = [
        migrations.AddField(
            model_name='sacservicetype',
            name='nova_service',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='client.service'),
        ),
    ]
