# Generated by Django 3.2.25 on 2025-02-13 22:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0667_remove_client_sacfk'),
        ('sacwis', '0003_alter_sacperson_client'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sacperson',
            name='client',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='client.client'),
        ),
    ]
