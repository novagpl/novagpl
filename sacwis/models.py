from collections import OrderedDict

from dateutil.relativedelta import relativedelta
from django.db import models
from django.db.models import Q
from django.conf import settings
from django.utils import timezone

from annoying.functions import get_object_or_None

from client.models import Client
from accounting.models import Vendor

import client.client_choices as CLIENT_C
import accounting.accounting_choices as ACCOUNTING_C

from utils import duplicate_name_detect



class SacPerson(models.Model):
    nova_client = models.OneToOneField("client.Client", on_delete=models.PROTECT, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    sacid = models.IntegerField(unique=True)
    repl_insert = models.DateTimeField(verbose_name="REPL insert")
    repl_update = models.DateTimeField(verbose_name="REPL update")
    first_name = models.CharField(max_length=255, null=True, blank=True, verbose_name="First name")
    last_name = models.CharField(max_length=255, null=True, blank=True, verbose_name="Last name")
    middle_initial = models.CharField(max_length=255, null=True, blank=True, verbose_name="Middle initial")
    birthdate = models.DateField(null=True, verbose_name="Birthdate")

    # Sacwis family member IDs
    mom_id = models.IntegerField(null=True)
    dad_id = models.IntegerField(null=True)
    grandparent1_id = models.IntegerField(null=True)
    grandparent2_id = models.IntegerField(null=True)
    legal_cust1_id = models.IntegerField(null=True)
    legal_cust2_id = models.IntegerField(null=True)

    # TODO ADD THE OTHER PERSON IDs and LOOKUPS


    def __str__(self):
        if self.birthdate is None:
            return f"{self.last_name}, {self.first_name} - ({self.id})"
        else:

            dob = self.birthdate.strftime(settings.HUMAN_DATE_FORMAT)
            return f"{self.last_name}, {self.first_name} ({self.id}) - {dob} ({self.get_age()})"



    def get_nova_matches(self):
        # Returns a list of potential Nova client matches from most likely
        # to least likely.
        matches = OrderedDict()


        full_matches = Client.objects.filter(
            first_name__iexact=self.first_name,
            last_name__iexact=self.last_name,
            birthdate=self.birthdate,
        )

        # If this client doesn't have a middle initial, we can totally exclude
        # middle from the query
        if self.middle_initial:
            full_matches = full_matches.filter(middle_name__istartswith=self.middle_initial)


        # Name and birthdate are exact matches. If there is only 1 result
        # we know this is very likely a match.
        for full_match in full_matches:
            matches[full_match] = "Exact name and birthdate match"

        # Exact first and last names and birthdate match but the Nova client
        # has no middle name to compare against
        non_middle_matches = Client.objects.filter(
            Q(Q(middle_name__isnull=True) | Q(middle_name='')),
            first_name__iexact=self.first_name,
            last_name__iexact=self.last_name,
            birthdate=self.birthdate,
        )
        for non_middle in non_middle_matches:
            if non_middle not in matches:
                matches[non_middle] = "First and last names along with birthdate match, but the Nova client has no middle name"

        # Exact first and last names and birthdate but the middle names are
        # different
        if self.middle_initial:
            diff_middle_matches = Client.objects.filter(
                first_name__iexact=self.first_name,
                last_name__iexact=self.last_name,
                birthdate=self.birthdate,
            )

            for diff_middle in diff_middle_matches:
                if diff_middle not in matches:
                    matches[diff_middle] = "First and last names along with the birthdate match, but the middle names are different"


        # Duplicate name detection tool results. Kind of a last-ditch effort
        # to try and suggest matching clients. May be useful to check against
        # the total_ratio match ratio value to prevent low-quality answers
        sorted_dnd_match_data = duplicate_name_detect(
            first_name=self.first_name,
            last_name=self.last_name,
            birthdate=self.birthdate
        )

        if sorted_dnd_match_data:
            dnd_match_ids = [
                x["id"] for x in sorted_dnd_match_data if x["first_ratio"] > 0.5 and x["last_ratio"] > 0.5]

            # Lookup each client individually to preserve the sorted
            # ratio order
            for matched_id in dnd_match_ids:
                dnd_client = get_object_or_None(Client, id=matched_id)
                if dnd_client and dnd_client not in matches:
                    matches[dnd_client] = "Potential match"

        # We don't want existing matched client to show up in the suggested
        if self.nova_client and self.nova_client in matches:
            matches.pop(self.nova_client)
        return matches



    def get_name(self):
        name = f"{self.last_name}, {self.first_name}"
        if self.middle_initial:
            name = f"{name} {self.middle_initial}."

        return name


    def get_age(self):
        today = timezone.now().date()
        age = relativedelta(today, self.birthdate)

        if age.years == 0 and age.months < 1:
            return f"{age.days} day{'s' if age.days > 1 else ''}"

        if age.years < 2:
            total_months = (age.years * 12) + age.months
            return f"{total_months} month{'s' if total_months > 1 else ''}"

        return f"{age.years} year{'s' if age.years > 1 else ''}"


    def get_mom(self):
        return get_object_or_None(SacPerson, sacid=self.mom_id)

    def get_dad(self):
        return get_object_or_None(SacPerson, sacid=self.dad_id)

    def get_grandparent1(self):
        return get_object_or_None(SacPerson, sacid=self.grandparent1_id)

    def get_grandparent2(self):
        return get_object_or_None(SacPerson, sacid=self.grandparent2_id)

    def get_legal_cust1(self):
        return get_object_or_None(SacPerson, sacid=self.legal_cust1_id)

    def get_legal_cust2(self):
        return get_object_or_None(SacPerson, sacid=self.legal_cust2_id)

    def get_family(self):
        # Returns a dict of any existing family members for this person
        members = {}

        # These are people who may be caring for this person
        if self.get_mom():
            members["Mother"] = self.get_mom()
        if self.get_dad():
            members["Father"] = self.get_dad()
        if self.get_grandparent1():
            members["Grandparent 1"] = self.get_grandparent1()
        if self.get_grandparent2():
            members["Grandparent 2"] = self.get_grandparent2()
        if self.get_legal_cust1():
            members["Legal guardian 1"] = self.get_legal_cust1()
        if self.get_legal_cust2():
            members["Legal guardian 2"] = self.get_legal_cust2()

        # These are people who this person may be caring for
        child_or = Q()
        child_or.add(Q(mom_id=self.sacid), Q.OR)
        child_or.add(Q(dad_id=self.sacid), Q.OR)
        children = SacPerson.objects.filter(child_or)
        for idx, child in enumerate(children):
            if (len(children)) > 1:
                members[f"Child {idx + 1}"] = child
            else:
                members["Child"] = child


        grandchild_or = Q()
        grandchild_or.add(Q(grandparent1_id=self.sacid), Q.OR)
        grandchild_or.add(Q(grandparent2_id=self.sacid), Q.OR)
        grandchildren = SacPerson.objects.filter(grandchild_or)
        for idx, grandchild in enumerate(grandchildren):
            if (len(grandchildren)) > 1:
                members[f"Grandchild {idx + 1}"] = grandchild
            else:
                members["Grandchild"] = grandchild


        ward_or = Q()
        ward_or.add(Q(legal_cust1_id=self.sacid), Q.OR)
        ward_or.add(Q(legal_cust2_id=self.sacid), Q.OR)
        wardren = SacPerson.objects.filter(ward_or)
        for idx, ward in enumerate(wardren):
            if (len(wardren)) > 1:
                members[f"Ward {idx + 1}"] = ward
            else:
                members["Ward"] = ward


        return members







class SacAddress(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    sacid = models.IntegerField(unique=True)
    repl_insert = models.DateTimeField(verbose_name="REPL insert")
    repl_update = models.DateTimeField(verbose_name="REPL update")

    street = models.CharField(max_length=100, null=True, blank=True, verbose_name="Street")
    apt = models.CharField(null=True, blank=True, max_length=10, verbose_name="Apt/suite")
    city = models.CharField(max_length=50, null=True, blank=True, verbose_name="City")
    state = models.CharField(max_length=2, choices=CLIENT_C.STATE, null=True, blank=True, verbose_name="State")
    zipcode = models.CharField(max_length=10, null=True, blank=True, verbose_name="Zip")
    phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    county = models.CharField(max_length=3, choices=CLIENT_C.WI_COUNTY, null=True, blank=True, verbose_name="County")
    email = models.CharField(max_length=255, null=True, blank=True, verbose_name="Email")
    address_group = models.CharField(max_length=255, null=True, blank=True, verbose_name="Address group")



class SacCase(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    sacid = models.IntegerField(unique=True)
    repl_insert = models.DateTimeField(verbose_name="REPL insert")
    repl_update = models.DateTimeField(verbose_name="REPL update")

    start = models.DateField(null=True, verbose_name="Case start date")
    end = models.DateField(null=True, verbose_name="Case end date")


class SacMedicaid(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    sacid = models.IntegerField(unique=True)
    repl_insert = models.DateTimeField(verbose_name="REPL insert")
    repl_update = models.DateTimeField(verbose_name="REPL update")

    person_id = models.CharField(max_length=255, null=True, blank=True)
    ma_number = models.CharField(max_length=255, null=True, blank=True, verbose_name="Medicaid number")



class SacServiceType(models.Model):
    nova_service = models.ForeignKey("client.Service", null=True, blank=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    repl_insert = models.DateTimeField(verbose_name="REPL insert")
    repl_update = models.DateTimeField(verbose_name="REPL update")

    code = models.CharField(max_length=255, null=True, blank=True, verbose_name="Service code")
    category = models.CharField(max_length=255, null=True, blank=True, verbose_name="Category")
    subcategory = models.CharField(max_length=255, null=True, blank=True, verbose_name="Sub-category")
    description = models.CharField(max_length=255, null=True, blank=True, verbose_name="Description")
    spc_code = models.CharField(max_length=255, null=True, blank=True, verbose_name="SPC code")
    status = models.CharField(max_length=255, null=True, blank=True, verbose_name="Status")



    def __str__(self):
        return f'{self.code} - {self.category} - {self.subcategory} - "{self.description}"'


class SacEpisode(models.Model):
    sacservicetype = models.ForeignKey(SacServiceType, null=True, blank=True, on_delete=models.SET_NULL)
    saccase = models.ForeignKey(SacCase, null=True, blank=True, on_delete=models.SET_NULL)
    sacclient = models.ForeignKey(SacPerson, null=True, blank=True, on_delete=models.SET_NULL)
    sacaddress = models.ForeignKey(SacAddress, null=True, blank=True, on_delete=models.SET_NULL)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    sacid = models.IntegerField(unique=True)
    repl_insert = models.DateTimeField(verbose_name="REPL insert")
    repl_update = models.DateTimeField(verbose_name="REPL update")

    case_id = models.IntegerField(null=True)
    person_id = models.IntegerField(null=True)
    address_group = models.IntegerField(null=True)
    provider_org_id = models.IntegerField(null=True)

    start = models.DateField(null=True, verbose_name="Episode start date")
    end = models.DateField(null=True, verbose_name="Episode end date")
    target_group = models.CharField(max_length=255, null=True, blank=True, verbose_name="Target group")


    def link_sacwis_fks(self):
        # Attempts to use the sacwis id fields to complete the relations
        sac_case = get_object_or_None(SacCase, sacid=self.case_id)
        sac_client = get_object_or_None(SacPerson, sacid=self.person_id)
        sac_address = SacAddress.objects.filter(address_group=self.provider_org_id).order_by("-repl_update").first()
        sac_servicetype = get_object_or_None(SacServiceType, code=self.service_id)

        self.sacservicetype = sac_servicetype
        self.saccase = sac_case
        self.sacclient = sac_client
        self.sacaddress = sac_address




'''
class SacImportRow(models.Model):
    nova_client = models.ForeignKey(Client, on_delete=models.CASCADE, null=True, blank=True)
    nova_client_suggest = models.ManyToManyField(Client, related_name="sac_import_row_nova_client_suggest", blank=True)
    sac_person = models.ForeignKey(SacPerson, on_delete=models.CASCADE, null=True, blank=True)
    client_msg = models.TextField(null=True, blank=True, verbose_name="Client error message")

    nova_address = models.ForeignKey(Address, on_delete=models.SET_NULL, null=True, blank=True)
    sac_address = models.ForeignKey(SacAddress, on_delete=models.CASCADE, null=True, blank=True)

    nova_episode = models.ForeignKey(EmrEpisode, on_delete=models.SET_NULL, null=True, blank=True)
    sac_episode = models.ForeignKey(SacEpisode, on_delete=models.CASCADE)
    episode_msg = models.TextField(null=True, blank=True, verbose_name="Episode error message")
    sac_case = models.ForeignKey(SacCase, on_delete=models.CASCADE, null=True, blank=True)

    nova_service = models.ForeignKey(EmrForm, on_delete=models.SET_NULL, null=True, blank=True)
    service_msg = models.TextField(null=True, blank=True, verbose_name="Service error message")

    sac_service_type = models.ForeignKey(SacServiceType, on_delete=models.CASCADE, null=True, blank=True)
    service_type_msg = models.TextField(null=True, blank=True, verbose_name="Service type error message")

    sac_medicaid = models.ForeignKey(SacMedicaid, on_delete=models.SET_NULL, null=True, blank=True)
'''




class SacCheckWriteFile(models.Model):
    filename = models.CharField(max_length=255, editable=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    raw_data = models.TextField(editable=False)
    lines_generated = models.BooleanField(default=False, verbose_name="Individual check write lines have been created")
    complete = models.BooleanField(default=False, verbose_name="Import complete")


    def __str__(self):
        return f"{self.filename} - {self.created_at.strftime('%m/%d/%Y')} ({self.id})"


    def get_line_count(self):
        return len(self.get_lines())

    def get_lines(self):
        return self.raw_data.split("\r\n")

    def get_status(self):
        if self.complete:
            return "Import complete"

        else:
            return "Pending"


class SacCheckWriteLine(models.Model):
    saccheckwritefile = models.ForeignKey(SacCheckWriteFile, on_delete=models.CASCADE)
    authorization = models.ForeignKey("accounting.Authorization", on_delete=models.PROTECT, null=True, blank=True)
    transaction = models.ForeignKey("accounting.Transaction", on_delete=models.PROTECT, null=True, blank=True)

    provider_id = models.CharField(max_length=10, verbose_name="Provider ID")
    payee_id = models.CharField(max_length=10, verbose_name="Payee ID")
    payee_name = models.CharField(max_length=30, null=True, blank=True, verbose_name="Payee name")
    financial_inst_name = models.CharField(max_length=32, null=True, blank=True, verbose_name="Financial institution name")
    is_direct_deposit = models.BooleanField(default=False, verbose_name="Direct deposit")
    account_number = models.CharField(max_length=10, null=True, blank=True, verbose_name="Account number")
    child_id = models.CharField(max_length=14, null=True, blank=True, verbose_name="Child ID")
    child_last = models.CharField(max_length=25, null=True, blank=True, verbose_name="Child last name")
    child_first = models.CharField(max_length=15, null=True, blank=True, verbose_name="Child first name")
    child_middle = models.CharField(max_length=1, null=True, blank=True, verbose_name="Child middle initial")
    child_suffix = models.CharField(max_length=3, null=True, blank=True, verbose_name="Child suffix")
    mail_street = models.CharField(max_length=40, null=True, blank=True, verbose_name="Street 1")
    mail_street2 = models.CharField(max_length=30, null=True, blank=True, verbose_name="Street 2")
    mail_care_of = models.CharField(max_length=30, null=True, blank=True, verbose_name="Mail in care of")
    mail_city = models.CharField(max_length=22, null=True, blank=True, verbose_name="Mail city")
    mail_state = models.CharField(max_length=2, null=True, blank=True, choices=CLIENT_C.STATE, verbose_name="Mail state")
    mail_zipcode = models.CharField(max_length=5, null=True, blank=True, verbose_name="Mail zipcode")
    mail_zipplus = models.CharField(max_length=255, null=True, blank=True, verbose_name="Mail zipcode + 4")
    mail_foreign = models.CharField(max_length=15, null=True, blank=True, verbose_name="Mail foreign address")
    rem_street = models.CharField(max_length=40, null=True, blank=True, verbose_name="Street 1")
    rem_street2 = models.CharField(max_length=30, null=True, blank=True, verbose_name="Street 2")
    rem_care_of = models.CharField(max_length=30, null=True, blank=True, verbose_name="Mail in care of")
    rem_city = models.CharField(max_length=22, null=True, blank=True, verbose_name="Mail city")
    rem_state = models.CharField(max_length=2, null=True, blank=True, choices=CLIENT_C.STATE, verbose_name="Mail state")
    rem_zipcode = models.CharField(max_length=5, null=True, blank=True, verbose_name="Mail zipcode")
    rem_zipplus = models.CharField(max_length=255, null=True, blank=True, verbose_name="Mail zipcode + 4")
    rem_foreign = models.CharField(max_length=15, null=True, blank=True, verbose_name="Mail foreign address")
    payment_type = models.CharField(max_length=2, choices=ACCOUNTING_C.SAC_PAYMENT_TYPE, null=True, blank=True, verbose_name="Payment type")
    payment_start = models.DateField(null=True, verbose_name="Payment state")
    num_days = models.IntegerField(null=False, blank=True, verbose_name="Number of days")
    amount = models.DecimalField(max_digits=7, decimal_places=2)
    vouch_num = models.CharField(max_length=5, null=True, blank=True, verbose_name="Voucher number")
    vouch_type = models.CharField(max_length=2, null=True, blank=True, default="IM", verbose_name="Voucher type code")
    vouch_date = models.DateField(null=True, verbose_name="Voucher date")
    fund = models.CharField(max_length=2, null=True, blank=True, default="05", verbose_name="Fund")
    department = models.CharField(max_length=3, null=True, blank=True, default="435", verbose_name="Department")
    service_desc = models.CharField(max_length=40, null=True, blank=True, verbose_name="Service description")
    court_num = models.CharField(max_length=8, null=True, blank=True, verbose_name="Court number")
    county = models.CharField(max_length=3, choices=CLIENT_C.WI_COUNTY, null=True, blank=True)
    county_person_id = models.CharField(max_length=20, null=True, blank=True, verbose_name="County person ID")
    county_case_id = models.CharField(max_length=20, null=True, blank=True, verbose_name="County case ID")
    county_provider_id = models.CharField(max_length=20, null=True, blank=True, verbose_name="County provider ID")
    rpt_cat = models.CharField(max_length=5, null=True, blank=True, verbose_name="RPT CAT")  # Report category??
    episode_id = models.CharField(max_length=14, null=True, blank=True, verbose_name="Episode ID")
    payment_id = models.CharField(max_length=14, null=True, blank=True, verbose_name="Payment id")
    county_payee_id = models.CharField(max_length=20, null=True, blank=True, verbose_name="Payee ID")
    service_code = models.CharField(max_length=9, null=True, blank=True, verbose_name="Service code")
    target_pop = models.CharField(max_length=5, null=True, blank=True, choices=ACCOUNTING_C.SAC_TARGET_POP, verbose_name="Target population")  # Is this the same as target group?
    spc = models.CharField(max_length=5, null=True, blank=True, verbose_name="SPC")
    payee_name2 = models.CharField(max_length=30, null=True, blank=True, verbose_name="Payee name 2")
    rmb_cat = models.CharField(max_length=2, null=True, blank=True, choices=ACCOUNTING_C.SAC_RMB_CAT, verbose_name="Reimbursement category")
    payment_subcode = models.CharField(max_length=1, null=True, blank=True, verbose_name="Payment sub code")
    filler = models.CharField(max_length=7, null=True, blank=True, verbose_name="Filler")



    def __str__(self):
        return f"SacCheckWriteLine({self.id})"


    def match_auth_status(self):
        # Attempt to match with a Nova auth, runs a few checks first to provide
        # users with a useful reason as to why the auth can't be connected
        # Returns True if the match is good otherwise returns an error message
        client = self.get_client()

        reasons = []

        if client is None:
            reasons.append("Could not find child")
        elif client is False:
            reasons.append("Client not linked")

        service = self.get_service()
        if service is None:
            reasons.append("Could not find service")
        elif service is False:
            reasons.append("Service not linked")

        vendor = self.get_vendor()
        if vendor is False:
            reasons.append("Could not find vendor")
        elif vendor is False:
            reasons.append("Vendor not linked")


        if reasons:
            return "<br>".join(reasons)

        else:
            return True



    def get_vendor(self):
        # Slightly different than get_client or get_service. The vendor must
        # be 1:1 linked by a provider_id that exists on the Nova Vendor model
        # as sac_provider_id. There is no checking the REPL database for the
        # provider_id - at least that I am aware of at this time.
        vendor = get_object_or_None(Vendor, sac_provider_id=self.provider_id)

        return vendor or False



    def get_client(self):
        # Checks to see if the child_id matches a person in the repl database
        # and if that person is linked to a nova client. If they are, return
        # the client. If the sacwis person exists but is unlinked return
        # a False and if the person isn't even found in the sacwis database
        # which should not happen - return a None
        sacperson = get_object_or_None(SacPerson, sacid=self.child_id)

        if not sacperson:
            return None

        return sacperson.nova_client or False


    def get_service(self):
        # Matches to a repl service type - returns error msg if more than 1
        # found. Same as get_client() - returns None if it can't even match
        # to a REPL service type, False if the service type isn't linked to a
        # Nova service and returns the Nova service if it's linked.

        # The check files contain the unique service code for the sacwis
        sacservice = get_object_or_None(SacServiceType, code=self.service_code)

        # Perhaps it would be worth it checking again for Inactive
        # service types if no active ones are found first?

        if not sacservice:
            return None

        return sacservice.nova_service or False




    def get_end_date(self):
        # takes the num_days and adds to the payment_start
        if not self.payment_start:
            return None

        if not self.num_days:
            return self.payment_start

        return self.payment_start + relativedelta(days=self.num_days)


