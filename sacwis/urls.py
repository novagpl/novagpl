from django.urls import path

from . import views


app_name = 'sacwis'

urlpatterns = [
    path("check_write/", views.sac_check_write_index, name="sac_check_write_index"),
    path("check_write/<int:check_write_id>/", views.sac_check_write_dtl, name="sac_check_write_dtl"),

    path("person_index/", views.person_index, name="person_index"),
    path("person/<int:person_id>/", views.person_dtl, name="person_dtl"),
    path("sacperson/<int:sacid>/", views.sacperson_dtl, name="sacperson_dtl"),


    path("link_to_client_mgmt/<int:person_id>/", views.link_repl_person_to_client_mgmt, name="link_to_client_mgmt"),
    path("sac_link_to_client_mgmt/<int:sacid>/", views.sac_link_repl_person_to_client_mgmt, name="sac_link_to_client_mgmt"),

    path("link_repl_to_client/", views.link_repl_to_client, name="link_repl_to_client"),
    path("unlink_repl_from_client/", views.unlink_repl_from_client, name="unlink_repl_from_client"),
    path("create_client_from_repl/", views.create_client_from_repl, name="create_client_from_repl"),

    # This whole link to vendor needs to be reworked completely...
    # for now, I think we can update the existing vendor view to match
    # the proper pagination/filter format as the rest of the app then copy
    # that view to here and pre-fill the filter fields etc..
    # May be able to modify these existing views, may be less work... im
    # not flipping sure... all we really need to do is match the provider id
    # to a vendor.. I think that can be done..
    # will need to provide further provider information so now im not sure
    # exactly how to transmit the data... holy shit we can just link in the damn
    # check write line!!
    # All of these functions need to be modified but it shouldn't be so bad...
    path("link_to_vendor_mgmt/<int:sac_check_write_line_id>/", views.link_sac_provider_id_to_vendor_mgmt, name="link_to_vendor_mgmt"),
    path("link_sac_provider_id_to_vendor/", views.link_sac_provider_id_to_vendor, name="link_sac_provider_id_to_vendor"),
    path("unlink_sac_provider_id_from_vendor/", views.unlink_sac_provider_id_from_vendor, name="unlink_sac_provider_id_from_vendor"),
    path("create_vendor_from_sac_checkline/", views.create_vendor_from_sac_checkline, name="create_vendor_from_sac_checkline"),

    path("service_type/", views.service_type_index, name="service_type_index"),
    path("service_type/<int:service_type_id>/", views.service_type_dtl, name="service_type_dtl"),
    path("link_service_type/", views.link_service_type, name="link_service_type"),
    path("unlink_service_type/", views.unlink_service_type, name="unlink_service_type"),
    path("create_service_from_repl/", views.create_service_from_repl, name="create_service_from_repl"),
    path("sacservicetype_dtl/<str:service_type_code>/", views.sacservicetype_dtl, name="sacservicetype_dtl"),

    path("nova_service/<int:nova_service_id>/", views.nova_service_edit, name="nova_service_edit"),

]
