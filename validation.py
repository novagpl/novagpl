"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import datetime as dt

from django.utils.html import format_html
from django.utils import timezone
from dateutil.parser import parse

from utils import (
    get_fiscal_year,
    get_fiscal_period,
    get_app_config,
    get_fiscal_year_and_period)


from client.models import Client
from accounting.models import (
    Account, AbilityToPay, BudgetItem, Vendor, Authorization,
)







def validate_emrform_sig(form):
    # Make sure there is at least a first and last name
    # Depending on the sig_type make sure the json data has the correct info
    data = form.cleaned_data

    signer_first = data.get("signer_first")
    signer_last = data.get("signer_last")
    sig_type = data.get("sig_type")

    if not signer_first:
        form.add_error("signer_first", "Signer's first name is required")

    if not signer_last:
        form.add_error("signer_last", "Signer's last name is required")

    if not sig_type:
        form.add_error("sig_type", "Signature type is required")


    if sig_type == "simple_typed":
        if "typed_sig" not in data or not data["typed_sig"]:
            form.add_error("typed_sig", "A typed signature is required")

        elif len(data["typed_sig"].strip()) < 2:
            form.add_error("typed_sig", "Typed signature is too short")









def validate_emr_episode(form):
    data = form.cleaned_data

    # The check for an existing open episode already happens in the view
    # new_emr_episode in client/views.py

    # First, check that the open date is before the close date
    start = data.get("start")
    end = data.get("end")
    closing_reason = data.get("closing_reason")

    if not start:
        form.add_error("start", "Start date is required.")

    if start and end and end < start:
        form.add_error("start", "Start date must be before the end date.")

    if end and not closing_reason:
        form.add_error("closing_reason", "Closing reason is required when end date is provided.")

    if closing_reason and not end:
        form.add_error("end", "End date is required when closing reason is provided.")


    # Episode-specific
    if form.instance.episode_type == 'randi':
        closing_reason2 = data.get("closing_reason2")
        closing_reason3 = data.get("closing_reason3")

        if closing_reason2 and not closing_reason:
            form.add_error("closing_reason", "Closing reason is required if 2nd closing reason is provided.")

        if closing_reason3 and not closing_reason2:
            form.add_error("closing_reason2", "2nd closing reason is required if 3rd closing reason is provided.")

        if closing_reason and closing_reason2 and closing_reason == closing_reason2:
            form.add_error("closing_reason2", "Closing reason and 2nd closing reason cannot be the same.")

        if closing_reason and closing_reason3 and closing_reason == closing_reason3:
            form.add_error("closing_reason3", "Closing reason and 3rd closing reason cannot be the same.")

        if closing_reason2 and closing_reason3 and closing_reason2 == closing_reason3:
            form.add_error("closing_reason3", "2nd closing reason and 3rd closing reason cannot be the same.")




def validate_vendorform(form):
    data = form.cleaned_data

    # Check if the vendor number already eixsts, show error if it does
    found = Vendor.objects.filter(vendor_number=data["vendor_number"]).exclude(
        id=form.instance.id
    )


    if found and data["vendor_number"] != "0":
        form.add_error(
            "vendor_number", "Vendor number '{}' already exists for: {}".format(
                data["vendor_number"], found[0]))


def validate_crisis_data_report(request):
    # Validation for the crisis data report. Mostly just checking that
    # dates aren't overlapping.
    report_error = []

    contact_date_from = request.POST.get("contact_date_from")
    contact_date_to = request.POST.get("contact_date_to")

    birthdate_from = request.POST.get("client_birthdate_from")
    birthdate_to = request.POST.get("client_birthdate_to")

    if contact_date_from > contact_date_to:
        report_error.append({
            "field": "contact_date_from",
            "msg": ["Contact date from cannot be greater than Contact date to", ]
        })

    if birthdate_from > birthdate_to:
        report_error.append({
            "field": "client_birthdate_from",
            "msg": ["Birthdate from cannot be greater than Birthdate to", ]
        })



    return report_error




def validate_report_fiscal(request):
    # *** This is not a standard form validation but it's used for the report
    # filters 

    # Essentially, if the user wants to search by fiscal they need to input
    # the fiscal year and period, not just one or the other

    report_error = []    

    fiscal_year_from = request.POST.get("fiscal_year_from")
    fiscal_period_from = request.POST.get("fiscal_period_from")

    fiscal_year_to = request.POST.get("fiscal_year_to")
    fiscal_period_to = request.POST.get("fiscal_period_to")

    if fiscal_year_from and not fiscal_period_from:
        report_error.append({
            "field": "fiscal_period_from",
            "msg": ["Fiscal year is required when using a fiscal period.", ]
        })

    if fiscal_period_from and not fiscal_year_from:
        report_error.append({
            "field": "fiscal_year_from",
            "msg": ["Fiscal period is required when using a fiscal year.", ]
        })

    if fiscal_year_to and not fiscal_period_to:
        report_error.append({
            "field": "fiscal_period_to", 
            "msg": ["Fiscal year is required when using a fiscal period.", ]
        })

    if fiscal_period_to and not fiscal_year_to:
        report_error.append({
            "field": "fiscal_year_to",
            "msg": ["Fiscal period is required when using a fiscal year.", ]
        })

    return report_error



def validate_budgetitem(form):
    data = form.cleaned_data
    # look for any duplicate accounts with the same header.
    dupe = BudgetItem.objects.filter(
        account=data.get("account"), budget=data.get("budget")).exclude(id=form.instance.id)

    if dupe:
        form.add_error(
            "account", "That account has already been added to this budget.")



def validate_ability_to_pay(form):
    year = form.cleaned_data.get("year")
    month = form.cleaned_data.get("month")
    client = form.cleaned_data.get("client")

    matches = AbilityToPay.objects.filter(year=year, month=month, client=client).exclude(id=form.instance.id)


    if matches:
        form.add_error(None, "An Ability to Pay already exists for this client for {}/{}.".format(year, month))




def validate_account_name(form):
    # try to find another account with this one's name.
    name = form.cleaned_data.get("name").strip()
    dupe = Account.objects.filter(name=name).exclude(id=form.instance.id)

    if dupe:
        form.add_error("name", format_html(
            "An account with that name already exists here: <a href='/accounting/account/{}' target='_blank'>{}</a>.", dupe[0].id, dupe[0]))




def validate_writeoff_account(form):
    writeoff_account = form.cleaned_data.get("writeoff_account")

    # there should only be 1 writeoff account!
    if writeoff_account is True:
        result = Account.objects.filter(writeoff_account=True).exclude(id=form.instance.id)

        if result:
            form.add_error(
                "writeoff_account",
                format_html("A writeoff account already exists here: <a href='/accounting/account/{}' target='_blank'>{}</a> ", result[0].id, result[0]))



def validate_date(date, form):
    if not date:
        form.add_error(
            "date",
            "Date must be in month/day/year format.")


def validate_birthdate(date, form):
    if date is None:
        return

    if not date:
        form.add_error(
            "birthdate",
            "Date must be in month/day/year format.")
        return

    if date > dt.datetime.now().date():
        form.add_error('birthdate', "Birthdate cannot be a future date.")



def validate_client_name(client_id, first_name, last_name, birthdate, form):
    dupe = Client.objects.exclude(
        id=client_id).filter(
            first_name=first_name, last_name=last_name, birthdate=birthdate)
    if dupe:
        form.add_error(
            None, format_html(
                "A client with the same name and birthdate already exists here: <a href='/client/{}' target='_blank'>{}</a>.",
                dupe[0].id, dupe[0])
        )



def validate_ssn(form):
    number = form.cleaned_data.get("number")
    if len(number.replace('-', '')) != 9:
        form.add_error("number", "Social Security number needs to be 9 digits, excluding dashes.")
    else:
        dirty_ssn = number.replace('-', '')
        ssn1 = dirty_ssn[:3]
        ssn2 = dirty_ssn[3:5]
        ssn3 = dirty_ssn[5:]
        form.clean_data['number'] = "{}-{}-{}".format(ssn1, ssn2, ssn3)




def validate_transaction_date(form):
    # Disabled for the new year per Carly TODO change this to allow dates greater
    # than but not less than the fiscal year/period
    return
    data = form.cleaned_data

    fy, fp = get_fiscal_year_and_period(form.request)
    # Using an integer to represent the fiscal year and period because if we
    # try to convert it into a date, periods 13-18 would cause the date
    # parse to crash.
    fiscal_cutoff = int(f"{fy}{fp}")

    date = data.get("date")

    if date and int(date.strftime("%Y%m")) < fiscal_cutoff:
        form.add_error("date", f"Date cannot be before the open fiscal year and period of {fy}/{fp}")




    return

    fiscal_year = get_fiscal_year(form.request)
    data = form.cleaned_data

    validate_date(data.get("date"), form)

    if data.get("date") and data.get("date").year != fiscal_year:
        form.add_error("date", "Transaction year must be in the same year as the fiscal year.")

    if data.get("lot") and data.get("lot").year != fiscal_year:
        form.add_error("lot", "Batch year must match fiscal year.")

    form = validate_fiscal(form)



def validate_transaction_amount(form):
    data = form.cleaned_data
    amount = data.get("amount") or 0

    if amount < 0:
        form.add_error("amount", "Transaction amount cannot be less than zero.")


def validate_journal_entry_date(form):
    return  # disabled per Carly. TODO change this to allow dates greater
    # than but not less than the fiscal year/period


    data = form.cleaned_data
    validate_date(data.get("date"), form)

    if data.get("date") and data.get("date").year != get_fiscal_year(form.request):
        form.add_error('date', 'Journal Entry year must be in the same year as the fiscal year.')

    form = validate_fiscal(form)


def validate_journal_trans(form):
    return  # disabled per Carly. TODO change this to allow dates greater
    # than but not less than the fiscal year/period
    data = form.cleaned_data

    if data.get("fiscal_year") != data.get("journal_entry").fiscal_year:
        form.add_error("fiscal_year", "Fiscal year must match the journal entry's fiscal year.")
    if data.get("fiscal_period") != data.get("journal_entry").fiscal_period:
        form.add_error("fiscal_period", "Fiscal period must match the journal entry's fiscal period.")





def validate_casenote_close(form):
    """
        Validation for closing a case note or PPS service.

        Validates a filled-in electronic sig against the logged in user.
        Only care about this when closing the casenote!!!
        form needs to have the request set on it in the view: instance.request = request <-- THIS CAN BE CLEANED UP TO MATCH THE NEW WAY OF PASSING REQUEST TO THE FORM!!
        form = CaseNoteForm(instance)

        And then in the view: self.instance = kwargs.get("instance")
    """

    data = form.cleaned_data
    if form.instance.pps_epi_uuid is None:
        if get_app_config("casenote_requires_signature"):
            if data.get("electronic_signature") is None:
                form.add_error("electronic_signature", 
                    "When closing a case note, the electronic signature must match your full name as it is in your user config.")

            elif data["electronic_signature"] != form.instance.request.user.userconfig.full_name:
                form.add_error("electronic_signature",
                    "When closing a case note, the electronic signature must match your full name as it is in your user config.")

    else:
        # when closing pps, the end date and units are required.
        # end date cannot be before start date, etc..
        if  data.get("pps_start") is None:
            form.add_error("pps_start", "PPS Start date is required.")
        if data.get("pps_end") is None:
            form.add_error("pps_end", "PPS End date is required.")

        validate_date(form, data.get("pps_start"))
        validate_date(form, data.get("pps_end"))

        if data.get("pps_start") and data.get("pps_end") and data.get("pps_end") < data.get("pps_start"):
            form.add_error("pps_end", "PPS End date cannot be before the PPS Start date.")

        if not data.get("pps_units") or data.get("pps_units") == "0":
            form.add_error("pps_units", "PPS Units are required and cannot be zero.")


def validate_userconfig(form):
    # a little different than normal. don't pass the request object to 
    # the get_fiscal defs because those check against the user's fiscal
    # and we're setting the user's fiscal right here. So validate
    # against the Nova settings.
    fiscal_year = get_fiscal_year() 
    fiscal_period = get_fiscal_period()
    data = form.cleaned_data

    from_year = data["fiscal_year"]
    from_period = data["fiscal_period"]

    if not from_year or not from_period:
        if not from_year:
            form.add_error("fiscal_year", f"Fiscal year is required and must be equal or greater than Nova's fiscal year of {fiscal_year}.")

        if not from_period:
            form.add_error("fiscal_period", f"Fiscal period is required and must be equal or greater than Nova's fiscal period of {fiscal_period}.")

    else:
        if from_year < fiscal_year:
            form.add_error("fiscal_year", f"Transaction fiscal year must be equal or greater than Nova's fiscal year of {fiscal_year}.")

        if from_period < fiscal_period and from_year <= fiscal_year:
            form.add_error("fiscal_period", f"Transaction fiscal period must be equal or greater than Nova's fiscal period of {fiscal_period}.")





def validate_fiscal(form):
    data = form.cleaned_data

    if data.get("fiscal_year") < get_fiscal_year(form.request):
        form.add_error("fiscal_year", "Fiscal year must be equal or greater than your fiscal year.")

    if data.get("fiscal_period") < get_fiscal_period(form.request) and data.get("fiscal_year") <= get_fiscal_year(form.request):
        form.add_error("fiscal_period", "Fiscal period must be equal or greater than your fiscal period.")




def validate_auth(form):
    # Validation for the 'new' auth system (2024-08-08)
    # Handles validation for the auth itself as well as the optional
    # vendor and client.
    data = form.cleaned_data

    # Auth validation
    # valid date and service date overlaps
    if data["auth_date_from"] > data["auth_date_to"]:
        form.add_error("auth_date_from", "Auth dates cannot overlap")
        form.add_error("auth_date_to", "Auth dates cannot overlap")

    if data["auth_service_date_from"] > data["auth_service_date_to"]:
        form.add_error("auth_service_date_from", "Service dates cannot over lap")
        form.add_error("auth_service_date_to", "Service dates cannot over lap")


    # Optional new client validation.
    # 1. Make sure the auth_client is null
    # 2. Make sure all required fields are filled
    client_first_name = data.get("client_first_name")
    client_last_name = data.get("client_last_name")
    client_middle_name = data.get("client_middle_name")
    client_birthdate = data.get("client_birthdate")
    client_gender = data.get('client_gender')
    auth_client = data.get("auth_client")

    if (client_first_name or client_last_name or client_middle_name
            or client_birthdate or client_gender):

        # 1. Check if there is new client data -fields used to create a new client
        # on the fly when creating the new auth- and if there is a client selected
        # in the lookup. There cannot be both. The auth must be tied to a single
        # client so it's gotta be the lookup or the new client being entered.
        if auth_client:
            form.add_error("auth_client", "Cannot have new client data and an existing client at the same time")

            if client_first_name:
                form.add_error("client_first_name", "Cannot have new client data and an existing client at the same time")

            if client_last_name:
                form.add_error("client_last_name", "Cannot have new client data and an existing client at the same time")

            if client_middle_name:
                form.add_error("client_middle_name", "Cannot have new client data and an existing client at the same time")

            if client_middle_name:
                form.add_error("client_middle_name", "Cannot have new client data and an existing client at the same time")

            if client_gender:
                form.add_error("client_gender", "Cannot have new client data and an existing client at the same time")

        # 2. Checking that all the required client data is entered.
        # all we really need are the first and last names.
        # However, I think if we only enter first and last names, they must
        # be unique. I don't think we should be able to have duplicated
        # first and last names with no birthdate!
        if not client_first_name:
            form.add_error("client_first_name", "Client first name is required")

        if not client_last_name:
            form.add_error("client_last_name", "Client last name is required")

        # If no birthdate is provided and the client first and last name are
        # already in the database, then tell the user to add a birthdate
        if not client_birthdate and (client_first_name and client_last_name):
            found = Client.objects.filter(
                first_name__iexact=client_first_name,
                last_name__iexact=client_last_name)

            if found:
                form.add_error("client_birthdate", "Client with the same first and last name already exists. Please provide a birthdate")

        elif not client_birthdate and (client_birthdate and client_first_name and client_last_name):
            # If the client has the same first and last and birthdate then we need
            # to ask for the middle initial or name.
            found = Client.objects.filter(
                first_name__iexact=client_first_name,
                last_name__iexact=client_last_name,
                birthdate=client_birthdate)

            if found:
                form.add_error("client_middle_name", "Client with the same first name, last name, and birthdate already exists. Please provide a middle name or initial")

        elif client_birthdate and client_first_name and client_last_name and client_middle_name:
            found = Client.objects.filter(
                first_name__iexact=client_first_name,
                last_name__iexact=client_last_name,
                middle_name__iexact=client_middle_name,
                birthdate=client_birthdate)

            if found:
                form.add_error("client_last_name", "Client already exists")










