"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.20 on 2024-05-07 18:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('config', '0085_alter_userconfig_recent_clients'),
    ]

    operations = [
        migrations.AddField(
            model_name='userconfig',
            name='email_on_new_b3_episode',
            field=models.BooleanField(default=False, verbose_name='Email me when a Birth to 3 episode is created'),
        ),
        migrations.AddField(
            model_name='userconfig',
            name='email_on_new_clinic_preadmit_episode',
            field=models.BooleanField(default=False, verbose_name='Email me when a Clinic Pre-Admit episode is created'),
        ),
        migrations.AddField(
            model_name='userconfig',
            name='email_on_new_clts_episode',
            field=models.BooleanField(default=False, verbose_name='Email me when a CLTS episode is created'),
        ),
        migrations.AddField(
            model_name='userconfig',
            name='email_on_new_clts_preadmit_episode',
            field=models.BooleanField(default=False, verbose_name='Email me when a CLTS Pre-Admit episode is created'),
        ),
        migrations.AddField(
            model_name='userconfig',
            name='email_on_new_crisis_episode',
            field=models.BooleanField(default=False, verbose_name='Email me when a Crisis episode is created'),
        ),
        migrations.AddField(
            model_name='userconfig',
            name='email_on_new_cst_episode',
            field=models.BooleanField(default=False, verbose_name='Email me when a CST episode is created'),
        ),
        migrations.AddField(
            model_name='userconfig',
            name='email_on_new_randi_episode',
            field=models.BooleanField(default=False, verbose_name='Email me when a R&I episode is created'),
        ),
        migrations.AddField(
            model_name='userconfig',
            name='email_on_new_tcm_episode',
            field=models.BooleanField(default=False, verbose_name='Email me when a TCM episode is created'),
        ),
        migrations.AddField(
            model_name='userconfig',
            name='notify_on_new_b3_episode',
            field=models.BooleanField(default=False, verbose_name='Notify me when a Birth to 3 episode is created'),
        ),
        migrations.AddField(
            model_name='userconfig',
            name='notify_on_new_clinic_preadmit_episode',
            field=models.BooleanField(default=False, verbose_name='Notify me when a Clinic Pre-Admit episode is created'),
        ),
        migrations.AddField(
            model_name='userconfig',
            name='notify_on_new_clts_episode',
            field=models.BooleanField(default=False, verbose_name='Notify me when a CLTS episode is created'),
        ),
        migrations.AddField(
            model_name='userconfig',
            name='notify_on_new_clts_preadmit_episode',
            field=models.BooleanField(default=False, verbose_name='Notify me when a CLTS Pre-Admit episode is created'),
        ),
        migrations.AddField(
            model_name='userconfig',
            name='notify_on_new_crisis_episode',
            field=models.BooleanField(default=False, verbose_name='Notify me when a Crisis episode is created'),
        ),
        migrations.AddField(
            model_name='userconfig',
            name='notify_on_new_cst_episode',
            field=models.BooleanField(default=False, verbose_name='Notify me when a CST episode is created'),
        ),
        migrations.AddField(
            model_name='userconfig',
            name='notify_on_new_randi_episode',
            field=models.BooleanField(default=False, verbose_name='Notify me when a R&I episode is created'),
        ),
        migrations.AddField(
            model_name='userconfig',
            name='notify_on_new_tcm_episode',
            field=models.BooleanField(default=False, verbose_name='Notify me when a TCM episode is created'),
        ),
        migrations.AlterField(
            model_name='userconfig',
            name='email_on_new_episode',
            field=models.BooleanField(default=False, verbose_name='Email me when any episode is created'),
        ),
        migrations.AlterField(
            model_name='userconfig',
            name='notify_on_new_episode',
            field=models.BooleanField(default=False, verbose_name='Notify me when a any episode is created'),
        ),
    ]
