"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 2.1.4 on 2018-12-16 01:31

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('accounting', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AppConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.CharField(max_length=100, unique=True)),
                ('value', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='AppInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('county_name', models.CharField(default='', max_length=255)),
                ('agency_name', models.CharField(default='', max_length=255)),
                ('agency_phone', models.CharField(default='', max_length=255)),
                ('fiscal_cutoff', models.DateField()),
                ('financial_export_format', models.CharField(default='', max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='AuditTrail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_time', models.DateTimeField()),
                ('user_name', models.CharField(max_length=255)),
                ('action_type', models.CharField(max_length=10)),
                ('model_type', models.CharField(max_length=255)),
                ('form_type', models.CharField(max_length=255)),
                ('raw_uri', models.TextField()),
                ('ip_address', models.CharField(max_length=16)),
                ('model_data', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='UserConfig',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('active', models.BooleanField(default=True)),
                ('full_name', models.CharField(max_length=200, verbose_name='Full Name')),
                ('phone', models.CharField(blank=True, max_length=15, null=True)),
                ('email', models.CharField(blank=True, max_length=100, null=True)),
                ('quick_picks', models.TextField(blank=True, max_length=10240, null=True, verbose_name='Quick Picks')),
                ('user_type', models.CharField(choices=[('0', 'Inactive'), ('1', 'Admin'), ('2', 'Caseworker'), ('3', 'Financial')], default='0', max_length=1, verbose_name='User Type')),
                ('title', models.CharField(blank=True, max_length=255, null=True)),
                ('payroll_account', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='accounting.Account', verbose_name='Pay Account')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
