"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


'''
    Changes come in 4 types: Fix, Feature, Style, Docs, Performance, Flow
    Changes have 4 fields: date, scope, change_type, description
'''
def get_changes():

    changes = []


    changes.append([
        "2025/02/25",
        "Account",
        "feature",
        "Inactive accounts will show up in lookups and are indicated by a prefix."
    ])

    '''
    changes.append([
        "2024/11/7",
        "Batch",
        "feature",
        "Added new batch type column to the transaction batch index view"
    ])


    changes.append([
        "2024/11/7",
        "Batch",
        "feature",
        "Added new batch type 'ACH'"
    ])


    changes.append([
        "2024/9/26",
        "Nova Settings",
        "feature",
        "Added new optional setting called 'agency_id' which can be used to specify an alphanumeric ID code for the agency. This setting can be used to pre-fill form data."
    ])

    changes.append([
        "2024/9/26",
        "Nova Settings",
        "feature",
        "Added new optional setting called 'agency_npi_wpi' which can be used to specify an alphanumeric NPI or WPI code for the agency. This setting can be used to pre-fill form data."
    ])


    changes.append([
        "2024/9/11",
        "Authorization",
        "feature",
        "Added 'week' field to Authorization"
    ])


    changes.append([
        "2024/9/5",
        "Authorization",
        "feature",
        "New auth views and logic",
    ])


    changes.append([
        "2024/5/2",
        "Document Search",
        "feature",
        "Added Document Search to the financial reports"
    ])


    changes.append([
        "2024/4/24",
        "Journal Transaction",
        "fix",
        "Saving a journal transaction now redirects to the parent journal entry"
    ])


    changes.append([
        "2024/4/24",
        "Vendor",
        "feature",
        "Added 'ETF payment' checkbox"
    ])


    changes.append([
        "2024/4/18",
        "Journal Entry",
        "feature",
        "Added pagination and filter to the supporting documentation list"
    ])


    changes.append([
        "2024/4/16",
        "Journal Transaction",
        "flow",
        "Clicking on transactions in the Journal Entry view will open the transaction in full screen and no longer use a popup"
    ])


    changes.append([
        "2024/4/16",
        "Journal Entry",
        "feature",
        "Added transaction documents to the support document list view for transactions linked to the JE that also have supporting documents"
    ])

    changes.append([
        "2024/4/9",
        "Journal Transaction",
        "feature",
        "Added list of supporting documentation to the bottom of popup view"
    ])


    changes.append([
        "2024/4/9",
        "Journal Entry",
        "feature",
        "Export Journal Entry function is disabled after supervisor review"
    ])

    changes.append([
        "2024/4/9",
        "Journal Entry",
        "feature",
        "Export Journal Entry function no longer requires supervisor review"
    ])


    changes.append([
        "2024/3/14",
        "Journal Entry",
        "feature",
        "New supervisor review system for Journal Entries"
    ])


    changes.append([
        "2024/3/12",
        "Transaction",
        "flow",
        "Updated transaction flow. Creating a transaction no longer happens in a popup and when saving a transaction after the initial save will redirect back to the transaction index view"
    ])


    changes.append([
        "2024/3/12",
        "Transaction",
        "feature",
        "Option to upload supporting documentation"
    ])


    changes.append([
        "2024/3/5",
        "Batch",
        "feature",
        "Batch names are now automatically assigned"
    ])

    changes.append([
        "2024/3/5",
        "Batch",
        "feature",
        "Batch year is set to the user's fiscal year"
    ])


    changes.append([
        "2024/3/5",
        "Journal Entry",
        "feature",
        "Option to upload supporting documentation"
    ])

    changes.append([
        "2024/1/25",
        "Transaction Report",
        "fix",
        "Fixed crash when encountering a client without a birthdate"
    ])


    changes.append([
        "2024/1/16",
        "Authorization",
        "feature",
        "Update Authorization form to automatically fill in rate and unit type based on which service is selected"
    ])


    changes.append([
        "2024/1/16",
        "Authorization",
        "feature",
        "Changed rate field to a decimal field"
    ])



    changes.append([
        "2023/11/27",
        "Contract Service",
        "feature",
        "Added description field to Contract Service model and views"
    ])

    changes.append([
        "2023/11/27",
        "Contract Report",
        "feature",
        "Added contract type filter to the Contract Report"
    ])

    changes.append([
        "2023/11/27",
        "Transaction Report",
        "feature",
        "Added employee name fields to the CSV export on the Transaction Report"
    ])


    changes.append([
        "2023/9/29",
        "Reports",
        "fix",
        "Issue where a footer was causing an extra page to be printed"
    ])


    changes.append([
        "2023/9/28",
        "User Config",
        "feature",
        "Added setting for users to have their saved form data cleared when they log out"
    ])


    changes.append([
        "2023/9/25",
        "User Config",
        "feature",
        "Added credentials field"
    ])


    changes.append([
        "2023/7/27",
        "Sitewide",
        "fix",
        "Form buttons not returning to their initial state when using the back and forward buttons on the browser"
    ])


    changes.append([
        "2023/6/7",
        "Transaction Report",
        "feature",
        "Added funding profile to results",
    ])

    changes.append([
        "2023/5/25",
        "Idle Timer",
        "feature",
        "15 minute idle timer reimplemented"
    ])

    changes.append([
        "2023/5/22",
        "Contract Report",
        "feature",
        "Added contract report"
    ])

    changes.append([
        "2023/5/17",
        "Transaction Index",
        "fix",
        "Moved checkbox select field to the first position in the results table"
    ])

    changes.append([
        "2023/5/17",
        "Transaction Index",
        "feature",
        "Added invoice filter"
    ])

    changes.append([
        "2023/5/17",
        "Transaction Report",
        "fix",
        "Changed invoice filter to be case insensitive"
    ])


    changes.append([
        "2023/5/17",
        "Transaction Report",
        "fix",
        "Fixed formatting issue in totals section"
    ])


    changes.append([
        "2023/5/16",
        "Transaction Report",
        "feature",
        "Added invoice filter"
    ])


    changes.append([
        "2023/5/15",
        "Transaction Report",
        "feature",
        "Added invoice field to the report list views"
    ])


    changes.append([
        "2023/5/12",
        "Journal Entry Export",
        "feature",
        "Replaced batch field with invoice field on the ACS export"
    ])


    changes.append([
        "2023/5/2",
        "Vendor",
        "fix",
        "The vendor_number field should be required"
    ])


    changes.append([
        "2023/5/2",
        "Journal Entry",
        "style",
        "Updated text and order of action buttons for creating and adding transactions to the journal entry"
    ])


    changes.append([
        "2023/4/6",
        "Reports",
        "style",
        "Updated styling for reports to make them more consistent"
    ])


    changes.append([
        "2023/4/6",
        "Transaction popup",
        "style",
        "Made the popup wider and adjusted the fields so the popup has less height"
    ])


    changes.append([
        "2023/3/15",
        "Transaction Index",
        "fix",
        "Issue where button text wasn't showing up as 'Apply Filter'"
    ])


    changes.append([
        "2023/3/1",
        "Transaction Report",
        "style",
        "Updated styling when report is printed"
    ])

    changes.append([
        "2023/2/28",
        "Contract Index",
        "fix",
        "Removed duplicated entries in the contract index"
    ])


    changes.append([
        "2023/1/12",
        "Contract",
        "fix",
        "All contract types should show the authorization list"
    ])


    changes.append([
        "2023/1/12",
        "Contract",
        "feature",
        "Added SPC code to the contract index view"
    ])


    changes.append([
        "2023/1/10",
        "Rate",
        "fix",
        "Fixed inconsistent styling on the rate edit form"
    ])


    changes.append([
        "2023/1/9",
        "Case Note Rate",
        "fix",
        "Case Note should not close if a rate cannot be found"
    ])


    changes.append([
        "2023/1/9",
        "Case Note Rate",
        "feature",
        "Updated error message if a rate cannot be found when closing a case note"
    ])


    changes.append([
        "2022/12/6",
        "Contract & Authorization",
        "fix",
        "Contract and Authorization system updated"
    ])

    changes.append([
        "2022/12/1",
        "Budget",
        "style",
        "Updated form styling to make it more consistent"
    ])

    changes.append([
        "2022/12/1",
        "Budget Item",
        "style",
        "Updated form styling to make it more consistent"
    ])

    changes.append([
        "2022/12/1",
        "Journal Entry",
        "style",
        "Updated form styling to make it more consistent"
    ])


    changes.append([
        "2022/12/1",
        "Journal Transaction",
        "style",
        "Updated form styling to make it more consistent"
    ])


    changes.append([
        "2022/12/1",
        "Transaction Batch",
        "style",
        "Updated form styling to make it more consistent"
    ])


    changes.append([
        "2022/12/1",
        "Funding Profile",
        "style",
        "Updated form styling to make it more consistent"
    ])


    changes.append([
        "2022/11/30",
        "Authorization",
        "style",
        "Updated form styling to make it more consistent"
    ])

    changes.append([
        "2022/11/30",
        "Contract",
        "style",
        "Updated styling on notes field to be full width"
    ])


    changes.append([
        "2022/11/30",
        "Contract and Authorization",
        "style",
        "Made table filter styling consistent",
    ])

    changes.append([
        "2022/9/16",
        "Transaction Report",
        "feature",
        "Added amount filter"
    ])

    changes.append([
        "2022/9/16",
        "Transaction Report",
        "style",
        "Slightly updated filter form field sizes and ordering"
    ])

    changes.append([
        "2022/9/16",
        "Transaction Report",
        "feature",
        "Added billing code starts with filter"
    ])


    changes.append([
        "2022/8/31",
        "Case Note Transactions",
        "fix",
        "Case notes with unbillable billing codes will no longer link to a rate"
    ])


    changes.append([
        "2022/4/18",
        "Client Insurance",
        "feature",
        "Added insurance to client face sheet"
    ])


    changes.append([
        "2022/3/15",
        "EMR Episode",
        "feature",
        "Added closing reason dropdown and notes section"
    ])


    changes.append([
        "2022/3/14",
        "Transaction Reports",
        "feature",
        "Added show cfda column option",
    ])

    changes.append([
        "2022/3/11",
        "Transaction Report",
        "fix",
        "Fixed incorrect totals offset"
    ])


    changes.append([
        "2022/3/10",
        "Rate",
        "fix",
        "CLTS Preadmit needs to use the CLTS rate"
    ])


    changes.append([
        "2022/3/1",
        "EMR Episodes",
        "feature",
        "Like the face sheet, emr episodes are non-editable by default"
    ])


    changes.append([
        "2022/3/1",
        "Client Address",
        "feature",
        "Added homeless to address type"
    ])

    changes.append([
        "2022/3/1",
        "EMR Case Note Report",
        "feature",
        "Added place of service filter"
    ])

    changes.append([
        "2022/2/26",
        "Transaction Report",
        "feature",
        "CFDA number shows after account number"
    ])


    changes.append([
        "2022/2/14",
        "Transaction Report",
        "feature",
        "Added employee filter"
    ])


    changes.append([
        "2022/2/9",
        "EMR Case Note Time Report",
        "feature",
        "Added created by filter"
    ])


    changes.append([
        "2022/2/8",
        "EMR Case Note Time Report",
        "feature",
        "Updated to include totals for each program by worker and grand totals"
    ])

    changes.append([
        "2022/1/31",
        "Face Sheet",
        "feature",
        "Address has been updated to use address types, existing addresses have been converted"
    ])


    changes.append([
        "2022/1/27",
        "Reports",
        "feature",
        "Reports have been reorganized and now provide users with a list of their most recent and their most frequent reports"
    ])

    changes.append([
        "2022/1/26",
        "EMR Case Notes",
        "feature",
        "Case Note list views have been updated to support ordering and searching, notes longer than 100 characters can be clicked on to expand"
    ])


    changes.append([
        "2022/1/26",
        "Face Sheet",
        "fix",
        "Contact Info on face sheet should not say 'None' when data is empty"
    ])


    changes.append([
        "2022/1/26",
        "Face Sheet",
        "feature",
        "Added phone number to the contact info list view"        
    ])


    changes.append([
        "2022/1/19",
        "Face Sheet",
        "feature",
        "Face sheet is no longer editable by default, to edit a client's face sheet the user must click on 'Face Sheet' to unlock the view"
    ])


    changes.append([
        "2022/1/19",
        "EMR Case Note Time Report",
        "feature",
        "Added client filter"
    ])


    changes.append([
        "2022/1/18",
        "Face Sheet",
        "style",
        "Updated styling for the contact information"
    ])


    changes.append([
        "2022/1/13",
        "User Config",
        "feature",
        "A new option in the user config view allows users to opt-in to receive notification emails when EMR episodes are created "
    ])


    changes.append([
        "2022/1/13",
        "User Toolbar",
        "feature",
        "User Toolbar in the top right shows open case notes, clicking on the icon takes the user to a list of their open notes"
    ])


    changes.append([
        "2022/1/13",
        "EMR Case Note Report",
        "feature",
        "Clicking on emr case note result will take user to the emr case note"
    ])


    changes.append([
        "2022/1/13",
        "Client Face Sheet",
        "feature",
        "Expanded contact information"
    ])


    changes.append([
        "2022/1/13",
        "Client Face Sheet",
        "style",
        "Updated styling on the client face sheet",
    ])


    changes.append([
        "2022/1/12",
        "EMR Case Note",
        "fix",
        "EMR case notes should show service date before created dates"
    ])


    changes.append([
        "2022/1/10",
        "Payroll Transaction",
        "feature",
        "View for creating payroll-specific transactions from the main transaction screen"
    ])


    changes.append([
        "2022/1/7",
        "Transaction Report",
        "fix",
        "Fixed issue where the place_of_service filter was not working"
    ])


    changes.append([
        "2022/1/6",
        "Emr Notes",
        "feature",
        "When entering a time in 24hr format, PM is automatically applied"
    ])


    changes.append([
        "2022/1/6",
        "Client Face Sheet",
        "feature",
        "Added phone and email fields"
    ])


    changes.append([
        "2022/1/5",
        "EMR episode case note list view",
        "fix",
        "Case notes should show by newest first in the list view"
    ])


    changes.append([
        "2022/1/5",
        "EMR Case Note Report",
        "feature",
        "Added note status filter to allow filtering case notes by all, open, or closed"
    ])


    changes.append([
        "2021/12/22",
        "EMR Case Note Time Report",
        "feature",
        "Report now uses a date range instead of a single date"
    ])


    changes.append([
        "2021/12/20",
        "Client Contact",
        "feature",
        "Updated client contact information under the client's Face Sheet"
    ])


    changes.append([
        "2021/12/17",
        "Transaction Report",
        "fix",
        "Duplicate billing codes"
    ])


    changes.append([
        "2021/12/17",
        "EMR Case Note",
        "fix",
        "Fixed issue where case notes were not always being saved"
    ])


    changes.append([
        "2021/12/16",
        "Transaction Report",
        "feature",
        "Added billing code filter"
    ])


    changes.append([
        "2021/12/8",
        "EMR Case Note Time Report",
        "feature",
        "Added EMR Case Note Time Report"
    ])


    changes.append([
        "2021/12/8",
        "EMR Case Note and Report",
        "feature",
        "Added button on EMR case notes that allows users to send that individual case note to the EMR Case Note Report"
    ])


    changes.append([
        "2021/12/7",
        "EMR Case Note",
        "fix",
        "Collateral and record keeping options are missing from the contact type dropdowns"
    ])


    changes.append([
        "2021/12/2",
        "EMR Case Note Report",
        "fix",
        "Fill case note info should be shown on the report results"
    ])


    changes.append([
        "2021/12/2",
        "EMR Case Notes",
        "fix",
        "Service minutes defaults to blank which requires user to input a value even if it is zero"
    ])


    changes.append([
        "2021/12/2",
        "CLTS Case Note",
        "feature",
        "Added place of service dropdown as a required field"
    ])


    changes.append([
        "2021/6/25",
        "Trial Balance Report",
        "feature",
        "Trial Balance report only calculates against journaled transactions",
    ])

    changes.append([
        "2021/5/24",
        "Transaction Report",
        "style",
        "Updated report filters header styling"
    ])

    changes.append([
        "2021/5/3",
        "Vendor",
        "fix",
        "Skip vendor number validation if the number is set to '0'"
    ])

    changes.append([
        "2021/5/3",
        "Authorization",
        "feature",
        "Added a button to download the data in the view as a CSV"
    ])

    changes.append([
        "2021/5/1",
        "Vendor Config",
        "feature",
        "Show notification if the vendor name already exists"
    ])

    changes.append([
        "2021/5/1",
        "Vendor Config",
        "fix",
        "Do not allow duplicate vendor numbers"
    ])

    changes.append([
        "2021/5/1",
        "Employee Config",
        "fix",
        "Need to show inactive employees at the bottom"
    ])

    changes.append([
        "2021/5/1",
        "AR/AP County Report",
        "fix",
        "Report should not include cash accounts"
    ])

    changes.append([
        "2021/5/1",
        "AR/AP County Report",
        "feature",
        "Batch name added to top of report"
    ])

    changes.append([
        "2021/4/26",
        "DCS-942 Report",
        "fix",
        "Report not including all SPCs in SPC group",
    ])

    changes.append([
        "2021/4/26",
        "Funding Profile Report",
        "fix",
        "All credit accounts should show negative values"
    ])

    changes.append([
        "2021/4/21",
        "Funding Profile Report",
        "feature",
        "Added ability to exclude inactive accounts"
    ])

    changes.append([
        "2021/4/21",
        "Funding Profile Report",
        "fix",
        "Show all periods instead of a total from 13-18"
    ])

    changes.append([
        "2021/4/19",
        "Transaction Report",
        "feature",
        "Completed report shows the filter vales used to generate it at the top."
    ])

    changes.append([
        "2021/4/14",
        "Trial Balance Report",
        "fix",
        "Credit and debit fields are not needed."
    ])

    changes.append([
        "2021/4/14",
        "Transaction Report",
        "fix",
        "Cash accounts should not be included in the report"
    ])

    changes.append([
        "2021/4/14",
        "Transaction Report",
        "feature",
        "Added the ability to hide client and vendor from the report"
    ])

    changes.append([
        "2021/4/13",
        "Transaction Report",
        "feature",
        "Replaced 'include journal transactions' checkbox with a dropdown options for all, journaled, and unjournaled."
    ])

    changes.append([
        "2021/4/13",
        "Transaction View",
        "feature",
        "Replaced 'include journal transactions' checkbox with a dropdown options for all, journaled, and unjournaled."
    ])

    changes.append([
        "2021/4/13",
        "Authorization",
        "fix",
        "Filter now works with auth number as expected."
    ])


    changes.append([
        "2021/4/12",
        "Client Statement",
        "feature",
        "Added ability to select which clients to print."
    ])

    changes.append([
        "2021/4/10",
        "Transaction Report",
        "fix",
        "Removed check date filters",
    ])

    changes.append([
        "2021/4/10",
        "Journal Entry",
        "feature",
        "Added year filter",
    ])

    changes.append([
        "2021/4/10",
        "Transaction Batch",
        "feature",
        "Batches now show the current year by default and a button was added to allow for viewing all batches."
    ])

    changes.append([
        "2021/4/5",
        "Payroll",
        "feature",
        "Added total to payroll journal entries list.",
    ])

    changes.append([
        "2021/4/5",
        "Authorization",
        "feature",
        "Added year dropdown and the default year is the current fiscal year that the user is in."
    ])


    changes.append([
        "2021/4/5",
        "Contract",
        "feature",
        "Contracts now show the current year by default and a button was added to allow for vewing all contract years."
    ])

    changes.append([
        "2021/3/1",
        "My Notes report",
        "feature",
        "Report allows users to view their case notes filtered by date period to meet state reporting requirements"
        ])


    changes.append([
        "2021/2/23",
        "Client view",
        "style",        
        "Updated to split up the different sections which are now accessable from a menu near the top of the view."
        ])

    changes.append([
        "2021/2/15",
        "Case Note",
        "feature",        
        "Case note now shows closed timestamp"
        ])

    changes.append([
        "2021/2/15",
        "Journal Entry",
        "feature",
        "Once verified, journal entries may not be edited and can only be unverified by an admin user"
        ])

    changes.append([
        "2021/1/25",
        "Client Statement",
        "feature",
        "Checkbox added to toggle showing clients with zero balance"
        ])

    changes.append([
        "2020/12/2",
        "Budget",
        "feature",
        "When creating a new budget the user is given the option to have the new budget start off with the same values as the current budget."
        ])

    changes.append([
        "2020/12/1",
        "AR/AP County Report",
        "style",
        "Remove filter form from printed view",
        ])

    changes.append([
        "2020/12/1",
        "AR/AP County Report",
        "feature",
        "Include AP in the AR County Report and update the report name to AR/AP County Report"
        ])

    changes.append([
        "2020/12/1",
        "AR County Report",
        "feature",
        "Added 'Show Fund Totals' checkbox"
        ])

    changes.append([
        "2020/12/1",
        "AR County Report",
        "style",
        "Widened report results view"
        ])

    changes.append([
        "2020/12/1",
        "AR County Report",
        "style",
        "Report total now at end of report, not bottom of each page"
        ])


    changes.append([
        "2020/10/21",
        "User Forms",
        "feature",
        "User Forms are now cleared out when the user logs off"
        ])


    changes.append([
        "2020/10/21",
        "AR County Report",
        "feature",
        "Added grouping and totals by account fund"
        ])

    changes.append([
        "2020/10/21",
        "Clear Report Button",
        "fix",
        "Checkboxes and dropdowns were not getting cleared"
        ])

    
    changes.append([
        "2020/10/15",
        "AR County Report",
        "feature",
        "AR County Report is now live"
        ])
    


    changes.append([
        "2020/9/29",
        "Transaction Report",
        "fix",
        "Groups not being sorted alphabetically"
        ])

    changes.append([
        "2020/9/17",
        "User Form",
        "feature",
        "User Form feature is live. This feature allows Nova to remember the information input into the report form fields. This feature is currently live only for the Transaction Report."
        ])

    changes.append([
        "2020/9/15",
        "CC Reports",
        "feature",
        "CC Reports list clears out when the user logs out"
        ])

    changes.append([
        "2020/9/15",
        "Transaction Report",
        "fix",
        "Report not returing results when caseworker is selected in the filter"
        ])

    changes.append([
        "2020/9/11",
        "Report Forms",
        "feature",
        "Report form fields will now show a gray 'x' icon. If that icon is clicked, it will clear out the value in that field."
        ])


    changes.append([
        "2020/8/31",
        "CC Reports",
        "feature",
        "When a report is finished and the user is at the report watcher view, the user will be taken to the completed report automatically."
        ])

    changes.append([
        "2020/8/25",
        "CC Reports",
        "feature",
        "CC Reports is a new feature in Nova that lets users run reports in the background."
        ])

    changes.append([
        "2020/8/20",
        "Client Statement",
        "fix",
        "Client statement not showing clients that did not have transaction activity for the selected period."
        ])

    changes.append([
        "2020/8/7",
        "Client View",
        "feature",
        "All clients are now shown as default."
        ])

    changes.append([
        "2020/8/7",
        "Case Note",
        "fix",
        "When reopening a case note, if there is a transaction linked to this case note it should be deleted. If the transaction is in a journal entry, a validation error should be shown."
        ])

    changes.append([
        "2020/8/6",
        "Transaction View",
        "feature",
        "Added Service Code field to the list view."
        ])

    changes.append([
        "2020/7/28",
        "Funding Profile Report",
        "fix",
        "Report should calculate up to and through the specified fiscal period"
        ])

    changes.append([
        "2020/7/21",
        "Payroll Distribution",
        "feature",
        "Better error messages."
        ])

    changes.append([
        "2020/7/20",
        "Budget Report",
        "feature",
        "Period filter got a more verbose label."
        ])

    changes.append([
        "2020/7/20",
        "Budget Report",
        "fix",
        "Fiscal period filter not working."
        ])

    changes.append([
        "2020/7/20",
        "Budget Report",
        "fix",
        "Incorrect date calculation, should be using fiscal period instead."
        ])

    changes.append([
        "2020/7/1",
        "DCS-942 Report",
        "feature",
        "DCS-942 Report is now live!"
        ])

    changes.append([
        "2020/6/23",
        "Trial Balance Report",
        "fix",
        "Needs to filter by fiscal year and period insetad of transaction date."
        ])

    changes.append([
        "2020/6/23",
        "Trial Balance Report",
        "feature",
        "Balance column added to the CSV export."
        ])

    changes.append([
        "2020/6/23",
        "Trial Balance Report",
        "feature",
        "Credits now have a negative symbol on the CSV export."
        ])

    changes.append([
        "2020/6/22",
        "Account Transaction Report",
        "fix",
        "Report may crash when Date_from field is not populated."
        ])

    changes.append([
        "2020/6/22",
        "Reports",
        "fix",
        "Update transaction amount calculation from get_real_amount() to get_natural_amount()."
        ])

    changes.append([
        "2020/6/8",
        "Transaction Report",
        "fix",
        "Text is going into the margin and is unreadable when printing to an actual printer."
        ])

    changes.append([
        "2020/6/4",
        "Authorization",
        "style",
        "Authorization main list view is now full-width."
        ])


    changes.append([
        "2020/6/4",
        "Transaction Report",
        "fix",
        "Debits and credits are not showing as positive and negative amounts."
        ])

    changes.append([
        "2020/6/4",
        "Sitewide",
        "style",
        "Font sized reduced any time a view is printed."
        ])

    changes.append([
        "2020/6/2",
        "Journal Entry Export",
        "fix",
        "Non-natural transactions are showing incorrect positive or negative signs."
        ])

    changes.append([
        "2020/6/2",
        "Funding Profile Report",
        "style",
        "Decreased font size on the full-year report."
        ])

    changes.append([
        "2020/5/27",
        "Transaction Report",
        "feature",
        "Added SPC filter."
        ])

    changes.append([
        "2020/5/20",
        "Funding Profile Report",
        "style",
        "Report is now full-width."
        ])

    changes.append([
        "2020/5/20",
        "Case Note Index",
        "feature",
        "Added caseworker field to the list view."
        ])

    changes.append([
        "2020/5/20",
        "Transaction Index",
        "fix",
        "Date to field not retaining its value after the filter is applied."
        ])

    changes.append([
        "2020/5/19",
        "Account Transaction Report",
        "fix",
        "Date related error when running the account transaction report."
        ])
    

    changes.append([
        "2020/5/6",
        "Login Sort",
        "flow",
        "Administrative users are now taken to the changelog when logging in instead of the user config screen."
        ])


    changes.append([
        "2020/4/28",
        "Transaction Report",
        "feature",
        "Now shows summaries by period, transaction date, or service date based on the order by selection."
        ])

    changes.append([
        "2020/4/27",
        "Client Statement",
        "feature",
        "Added ATP Auto Discharge button to the client statement screen."
        ])

    changes.append([
        "2020/4/27",
        "Case Note",
        "feature",
        "Target Group field added."
        ])


    changes.append([
        "2020/4/15",
        "Transaction Report",
        "feature",
        "Can now order by service date."
        ])


    changes.append([
        "2020/4/14",
        "Client Statement report",
        "fix",
        "Description should show the vendor name."
        ])

    changes.append([
        "2020/4/14",
        "Client Statement report",
        "fix",
        "Transactions are not in chronological order."
        ])

    changes.append([
        "2020/4/14",
        "Client Statement report",
        "fix",
        "When used, the client filter is including clients that have no transactions but have a starting balance"
        ])

    changes.append([
        "2020/4/13",
        "Transaction",
        "feature",
        "When an auth is selected, the account, transaction type, clinet, vendor, serivce, and unit type fields will now auto populate."
        ])

    changes.append([
        "2020/4/13",
        "Vendor Index",
        "feature",
        "Vendor index can now be downloaded as a CSV."
        ])


    changes.append([
        "2020/4/13",
        "User Config",
        "style",
        "Moved the user config form above the alerts."
        ])


    changes.append([
        "2020/4/8",
        "Client Statement report",
        "fix",
        "Report not showing billing address when one is avilable."
        ])


    changes.append([
        "2020/4/7",
        "Client Statement report",
        "fix",
        "Report not showing clients with a starting balance if they don't have any transactions."
        ])


    changes.append([
        "2020/4/3",
        "Autocomplete Lookup",
        "fix",
        "Autocomplete lookup function not working when extra whitespace is present in a record's name or description fields."
        ])

    changes.append([
        "2020/4/3",
        "Client billing",
        "fix",
        "Case note transactions are showing up as client billable transactions."
        ])

    changes.append([
        "2020/4/2",
        "Transaction Report",
        "feature",
        "Added a filter that allows the report to only show transactions that are have been associated with a batch."
        ])

    changes.append([
        "2020/4/2",
        "Transaction",
        "fix",
        "Error caused if the transaction amount is set to zero."
        ])

    changes.append([
        "2020/3/31",
        "Case Note",
        "style",
        "Added a close case note button to the bottom of the case note so users can quickly sign and close without having to scorll around."
        ])


    changes.append([
        "2020/3/31",
        "Case Note",
        "feature",
        "Case notes can now be reopened and unsigned. This event is tracked in the audit log."
        ])


    changes.append([
        "2020/3/31",
        "Case Note listview",
        "feature",
        "Added time column to the case note list view."
        ])

    changes.append([
        "2020/3/31",
        "Delete function",
        "fix",
        "Fixed problem  where deleting a record would cause an error in some instances."
        ])

    changes.append([
        "2020/3/30",
        "Authorization",
        "feature",
        "Partial units are now allowed on authorzations."
        ])

    changes.append([
        "2020/3/18",
        "Payroll Distribution",
        "feature",
        "Added Paycheck Date column to the payroll distribution index view."
        ])

    changes.append([
        "2020/3/11",
        "Case Note Time report",
        "fix",
        "Fixed issue where the report was not properly calculating hours."
        ])

    changes.append([
        "2020/3/11",
        "Autocomplete lookup",
        "fix",
        "Fixed issue where in some cases a client's name would not get properly looked up."
        ])

    changes.append([
        "2020/3/5",
        "Journal Entry",
        "feature",
        "Added batch code column to the transaction list view."
        ])

    changes.append([
        "2020/3/5",
        "Journal Entry",
        "style",
        "Made the journal transaction list view full-width."
        ])

    changes.append([
        "2020/3/3",
        "Trial Balance report",
        "feature",
        "Sorting by column in the report is now supported."
        ])

    changes.append([
        "2020/3/3",
        "Trial Balance report",
        "fix",
        "Fixed column ordering not matching the report when exporting to CSV."
        ])

    changes.append([
        "2020/3/3",
        "Reports",
        "style",
        "Reports restyled slightly to be more consistent."
        ])

    changes.append([
        "2020/3/3",
        "Reports",
        "feature",
        "All relevant reports now have a final total at the bottom."
        ])

    changes.append([
        "2020/2/27",
        "Transaction Report",
        "feature",
        "Added account number startswith and endswith filters to the report."
        ])

    changes.append([
        "2020/2/25",
        "Transaction",
        "feature",
        "PPS Units are now automatically calculated based on the hours and minutes entered into the case note."
        ])

    changes.append([
        "2020/2/25",
        "Transaction Report",
        "fix",
        "Fixed issue where the report was not properly grouping by account."
        ])

    changes.append([
        "2020/2/24",
        "Transaction Report",
        "feature",
        "When grouping by client, the client's birthdate now shows below the client name."
        ])

    changes.append([
        "2020/2/24",
        "Transaction Report",
        "feature",
        "When grouping by account, the account description now shows below the account name."
        ])

    changes.append([
        "2020/2/21",
        "Funding Profile Report",
        "feature",
        "The report not operates in two modes. The first is period-only mode that shows totals for the selected fiscal year and period. The second mode is activated when the fiscal period is left out and shows a summary for the whole year with totals."
        ])

    changes.append([
        "2020/2/20",
        "Transaction Report",
        "fix",
        "Fixed issue that when grouping by client, incorrect columns would display."
        ])

    changes.append([
        "2020/2/20",
        "Case Note Transaction",
        "fix",
        "Fixed issue where transactions being generated from case notes were put into the incorrect fiscal period."
        ])

    changes.append([
        "2020/2/18",
        "Payroll Distribution",
        "feature",
        "When saving a payroll journal entry, it is now automatically verified."
        ])

    changes.append([
        "2020/2/18",
        "Payroll Distribution",
        "fix",
        "Fixed issue where payroll distribution would not round properly causing an error."
        ])

    changes.append([
        "2020/2/17",
        "Transaction View and Transaction Report",
        "feature",
        "Added caseworker filter."
        ])


    changes.append([
        "2020/2/17",
        "Transaction View and Transaction Report",
        "feature",
        "Changed the date filters to service date filters."
        ])

    changes.append([
        "2020/2/17",
        "Journal Transaction",
        "feature",
        "When creating a new journal transaction, its fiscal data is now inherited from the parent journal entry."
        ])

    changes.append([
        "2020/2/17",
        "Sitewide",
        "fix",
        "Fixed issue where some cached browser data was causing inconsistent URLs."
        ])


    changes.append([
        "2020/2/17",
        "Journal Entry",
        "fix",
        "Fixed issue where the journal entry export would not work if an account in the export was missing the county account name field."
        ])


    changes.append([
        "2020/2/14",
        "Transaction Report",
        "feature",
        "Added account type filter and added the account type column to the CSV export."
        ])

    changes.append([
        "2020/2/14",
        "Transaction",
        "feature",
        "Added account type filter."
        ])

    changes.append([
        "2020/2/13",
        "Case Note",
        "fix",
        "Date field label updated to reflect that its a service date."
        ])

    changes.append([
        "2020/2/12",
        "Case Note",
        "feature",
        "Hours field now avilable on the case note."
        ])

    changes.append([
        "2020/2/11",
        "Payroll Distribution",
        "feature",
        "Payroll distribution interface is now live!"
        ])

    changes.append([
        "2020/2/10",
        "Funding Profile Report",
        "fix",
        "Fixed issue where the report was only showing the absolute values for the accounts, not their actual register values. This is also fixed on the CSV export."        
        ])

    changes.append([
        "2020/2/8",
        "Case Note Time Report",
        "feature",
        "Added the Case Note Time report. If the user is admin, it shows the total time case noted for each user for the selected date. If the user is not admin, it shows the total time case noted for the logged in user for the selected date."
        ])

    changes.append([
        "2020/2/7",
        "Transaction",
        "fix",
        "Service date is now shown instead of transaction date."
        ])

    changes.append([
        "2020/2/6",
        "Transaction Report",
        "fix",
        "Service date is now the deafult date instead of the transaction date."
        ])

    changes.append([
        "2020/2/6",
        "Transaction Report",
        "feature",
        "Added checkbox to allow the transaction report to show only case note transactions."
        ])

    changes.append([
        "2020/2/6",
        "Sitewide",
        "fix",
        "Fixed issue where printing in FireFox would cause the pages and data to be cutoff."
        ])

    changes.append([
        "2020/2/5",
        "User Config & Case Notes",
        "feature",
        "Users can now select a default service in their config screen. This service will be automatically applied to any new case notes. The default_service can only be applied to PPS case notes for the same PPS episode type."
        ])

    changes.append([
        "2020/2/4",
        "Transaction Report",
        "fix",
        "Fixed issue where the choice fields were incorrectly showing the 'optional' helper tag."
        ])

    changes.append([
        "2020/2/4",
        "Transaction Report",
        "feature",
        "Added units and unit_type to transaction report and CSV download."
        ])

    changes.append([
        "2020/2/4",
        "Vendor",
        "feature",
        "Added instruction to the name field on the vendor model that explains if the vendor is an individual to set the name format as last, first."
        ])

    changes.append([
        "2020/2/4",
        "Rate",
        "feature",
        "Added account field to rate listview."
        ])

    changes.append([
        "2020/2/4",
        "Journal Entry",
        "fix",
        "Fixed issue where in some instances creating a new journal entry would cause an error."
        ])

    changes.append([
        "2020/1/31",
        "Journal Entry",
        "feature",
        "Checkboxes have been added to the journal transaction listvew that allow them to be grouped and an offset transaction to be generated for the sum of the selected journal transactions."
        ])

    changes.append([
        "2020/1/31",
        "Journal Entry",
        "feature",
        "Added batch field to the transaction lookup listview within the journal entry view."
        ])

    changes.append([
        "2020/1/31",
        "Journal Entry",
        "style",
        "Updated styling on journal transaction listview in the journal entry screen to be more consistent with the rest of Nova."
        ])


    changes.append([
        "2020/1/31",
        "Journal Entry",
        "feature",
        "Added disbursement total to the totals section of the journal entry."
        ])

    changes.append([
        "2020/1/30",
        "Journal Entry",
        "style",
        "Added verified and entries fields to Journal Entry index view."
        ])

    changes.append([
        "2020/1/29",
        "Sitewide",
        "feature",
        "Now, when a user's session timer is about to expire, a modal is displayed asking the user if they would like to log out our continue working."
        ])

    changes.append([
        "2020/1/29",
        "Account",
        "fix",
        "Fixed issue where accounts with ampersands would cause the lookup to be too general and display incorrect acocunts."
        ])

    changes.append([
        "2020/1/29",
        "Account",
        "fix",
        "Fixed issue where some field values on the account CSV export were not displaying properly."
        ])

    changes.append([
        "2020/1/28",
        "Journal Entry",
        "feature",
        "Added a 'Create Offset' button that will assist in creating an offset transaction for the journal entry. The transaction will have its amount pre-filled and all non-required fields are hidden."
        ])

    changes.append([
        "2020/1/28",
        "Account",
        "style",
        "Account listview no longer groups by 100."
        ])

    changes.append([
        "2020/1/28",
        "Account",
        "feature",
        "Added CSV download option for accounts."
        ])

    changes.append([
        "2020/1/28",
        "Transaction",
        "feature",
        "Added authorization field to transaction"
        ])

    changes.append([
        "2020/1/27",
        "Sitewide",
        "style",
        "Changed styling for get_utilization_fancy functions."
    ])

    changes.append([
        "2020/1/27",
        "Budget Report",
        "feature",
        "Added new report, Budget Report, which shows all accounts, their amunts, and utlizations if they are in a budget."
    ])

    changes.append([
        "2020/1/17",
        "Client lookup",
        "fix",
        "Fixed issue where the lookup algorithm for client records would be too general and return too many records even when a first and last name were input."
    ])

    changes.append([
        "2020/1/16",
        "Transaction Report",
        "fix",
        "Fixed issue where the transaction report was not filtering by batch."
    ])

    changes.append([
        "2020/1/15",
        "Transaction View",
        "feature",
        "Transaction view now supports ordering by column."
    ])

    changes.append([
        "2020/1/15",
        "Transaction Report",
        "feature",
        "CSV export now follows the ordering set in the report."
    ])

    changes.append([
        "2020/1/15",
        "Transaction Report",
        "feature",
        "Added vendor number to CSV export."
    ])

    changes.append([
        "2020/1/14",
        "Lookups",
        "fix",
        "Lookups now use starts with instead of contains for the query."
    ])

    changes.append([
        "2020/1/14",
        "Account",
        "feature",
        "If a duplicate account number is entered, a validation error will show."
    ])

    changes.append([
        "2020/1/14",
        "Authorization",
        "feature",
        "Added unit type and units fields to the authorization model."
    ])

    changes.append([
        "2020/1/9",
        "Budget",
        "feature",
        "Budget can now be exported to CSV file."
    ])

    changes.append([
        "2020/1/9",
        "Budget",
        "feature",
        "Budget is now print-friendly."
    ])

    changes.append([
        "2020/1/8",
        "Journal Entry Export",
        "feature",
        "Once a journal entry is verified it can then be exported to the county's financial system."
    ])

    changes.append([
        "2020/1/2",
        "Ability to Pay Auto Discharge",
        "feature",
        "Ability to Pay Auto Discharge feature added. Allows the automatica creation of writeoff transactions for a client with an ability to pay record."
    ])

    changes.append([
        "2020/1/2",
        "Transaction Report",
        "style",
        "Results were widened to full screen to allow for the additional result fields."
    ])

    changes.append([
        "2020/1/2",
        "Transaction Report",
        "feature",
        "County account description now included in the CSV export."
    ])

    changes.append([
        "2020/1/2",
        "Transaction Report",
        "feature",
        "County account result field added to Transaction Report."
    ])

    changes.append([
        "2020/1/2",
        "Transaction Report",
        "feature",
        "Batch filter and result field added to Transaction Report."
    ])

    changes.append([
        "2020/1/2",
        "Vendor",
        "feature",
        "Vendor address now shows on lookup in the transaction view."
    ])

    changes.append([
        "2019/12/30",
        "Transaction",
        "fix",
        "Units field now visible on the transaction form."
    ])

    changes.append([
        "2019/12/30",
        "Transaction",
        "feature",
        "If a transaction type and account don't match the user is now prompted with a dialog asking if they'd like to continue."
    ])

    changes.append([
        "2019/12/30",
        "Client Payment",
        "fix",
        "Fixed issue where client payment edit view didn't always repopulate the field data."
    ])


    changes.append([
        "2019/12/28",
        "Authorization",
        "feature",
        "Authorization number is now displayed on the authorzation list view and the authroization edit view."
    ])

    changes.append([
        "2019/12/28",
        "Reports",
        "feature",
        "All reports can now be downloaded in CSV format."
    ])

    changes.append([
        "2019/12/28",
        "Reports",
        "style",
        "All reports now hide their forms when being printed from the browser."
    ])

    changes.append([
        "2019/12/28",
        "Client Statement",
        "feature",
        "Client statement updated to include payment and billing history, also accounts for starting balance, and when printed goes into a letter-friendly format."
    ])


    changes.append([
        "2019/12/27",
        "Client Payment",
        "feature",
        "Added new Client Payment form to the Transaction page. This form allows a quick and easy way to associate a client bill payment with a client."
    ])


    changes.append([
        "2019/12/27",
        "Client",
        "feature",
        "Added 'starting_balance' field to the client record. This field is only visible to admin and financial workers."
    ])

    changes.append([
        "2019/12/26",
        "Transaction",
        "feature",
        "When a user changes the debit/credit field to a non-natural selection compared to the selected account, a warning icon is now shown."
    ])

    changes.append([
        "2019/12/26",
        "Transaction Report",
        "feature",
        "Can now filter by fiscal year and period start/end range."
    ])

    changes.append([
        "2019/12/26",
        "Client",
        "fix",
        "Removed 'ma_billable' field."
    ])

    changes.append([
        "2019/12/26",
        "Service Config",
        "fix",
        "Capitalized the episode types."
    ])

    changes.append([
        "2019/12/26",
        "Fiscal Year and Period",
        "fix",
        "Fixed issue where the Delete button was be visibile when it should be invisible."
    ])

    changes.append([
        "2019/12/26",
        "Rate",
        "fix",
        "Rate is now disconnected from Users. Any user can pick any rate."
    ])

    changes.append([
        "2019/12/26",
        "Client Statement Report",
        "fix",
        "Non-billable transactions are no longer included"
    ])

    changes.append([
        "2019/12/26",
        "Client Statement Report",
        "fix",
        "Non-client transactions are no longer included"
    ])

    changes.append([
        "2019/12/26",
        "Transaction Report",
        "fix",
        "Added description field to the report."
    ])

    changes.append([
        "2019/12/4",
        "Nova Settings",
        "feature",
        "New interface to change Nova's fiscal year and period limits. When changing the fiscal year and period limts, any users who are in the wrong fiscal year or period are automatically updated."
    ])

    changes.append([
        "2019/12/4",
        "Nova Settings",
        "feature",
        "There's now a list of authenticated and logged in users."
    ])

    changes.append([
        "2019/12/4",
        "Login Sort",
        "feature",
        "Admin users are now directed to their user profile when logging in."
    ])

    changes.append([
        "2019/12/4",
        "Sitewide",
        "fix",
        "Fixed issue where lookup autocomplete dropdowns would sometimes require scrolling to see. They now expand upward or downward depending on the avilable space."
    ])

    changes.append([
        "2019/12/4",
        "Transaction Filter",
        "fix",
        "Changed the filter 'Verified' to 'Include journal transactions'. Now any transactions that are already in a journal entry will not be included in the default transaction view."
    ])

    changes.append([
        "2019/12/4",
        "Address",
        "feature",
        "Billing status now shown on address listview in the client view."
    ])

    changes.append([
        "2019/12/4",
        "Address",
        "feature",
        "2nd address line now shows up in the client list report and the address listview in the client view."
    ])

    changes.append([
        "2019/12/4",
        "Journal Entry",
        "feature",
        "Journal transactions are now sorted by account number."
    ])

    changes.append([
        "2019/12/3",
        "Trial Balance Report",
        "fix",
        "Fixed issue where the totals were in the wrong columns."
    ])

    changes.append([
        "2019/12/3",
        "Address",
        "feature",
        "Added a 2nd line for street/care of."
    ])

    changes.append([
        "2019/12/3",
        "Address",
        "feature",
        "Added checkboxes to signify if the address is for billing or is a client's physical address"
    ])

    changes.append([
        "2019/12/2",
        "Payroll Distribution",
        "feature",
        "Added payroll distribution screen."
    ])

    changes.append([
        "2019/12/2",
        "UserConfig",
        "fix",
        "Fixed issue where the password change interface appears to change the user's password but does not. Now the when the user changes their password, they are redirected to the login."
    ])

    changes.append([
        "2019/12/1",
        "Episode",
        "fix",
        "Target group number appened to the beginning of the description."
    ])

    changes.append([
        "2019/12/1",
        "Service",
        "fix",
        "Fixed issue where the service edit screen was not a popup."
    ])

    changes.append([
        "2019/12/1",
        "Client",
        "fix",
        "Fixed issue that would cause a crash when downlading forms from a client and the client has no addresses."
    ])

    changes.append([
        "2019/12/1",
        "Service",
        "fix",
        "'SPC' in 'SPC code' has been capitalized throughout the application."
    ])

    changes.append([
        "2019/12/1",
        "Transaction Lot/Transaction Batch",
        "feature",
        "Transaction Lot has been changed to 'Transaction Batch'"
    ])

    changes.append([
        "2019/12/1",
        "Client",
        "fix",
        "Fixed issue that would cause a crash when creating a new client."
    ])

    changes.append([
        "2019/12/1",
        "Client statement report",
        "feature",
        "Client is now optional on the report and the report will show more " \
        "than 1 client but is still grouped by client+account."
        ])

    changes.append([
        "2019/11/30",
        "Changelog",
        "feature",
        "Changelog created."
    ])
    '''

    return changes
