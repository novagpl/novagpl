"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from django.db import models
from django import forms
from django.forms import ModelForm, DateInput, Textarea, CharField, TextInput, HiddenInput, Select, PasswordInput, NumberInput, BooleanField, CheckboxInput, ChoiceField, IntegerField
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
from .models import *
from client.models import *
from accounting.models import *
from report.models import *
#from django.utils.translation import gettext_lazy as _  # fix this.
from utils import render_form, format_errors

import validation


ACTIVE_FILTER_CHOICES = (
    (None, 'All'),
    (True, 'Active'),
    (False, 'Inactive')
)







class RouteForSignatureForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["assigned_to"].label_from_instance = self.assigned_to_label
        self.fields["assigned_to"].label = "Assign to"
        self.fields["action_type"].required = False
        self.fields["action_type"].disabled = True
        self.fields["action_type"].widget.choices = (
            ("supervisor_sign", "Supervisor sign"),
        )


        render_form(self)


    def assigned_to_label(self, user):
        return str(user.userconfig)



    class Meta:
        model = Route

        fields = [
            "action_type",
            "assigned_to",
            "message",
        ]


        widgets = {
            "action_type": HiddenInput(),
            "assigned_to": Select(attrs={"width": 60}),
            "message": Textarea(attrs={"width": 100, "rows": 3, "clear": "left"}),
        }



class RouteForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields["assigned_to"].label_from_instance = self.assigned_to_label

        self.fields["action_type"].widget.choices = (
            ("review", "Review"),
        )

        render_form(self)


    def assigned_to_label(self, user):
        return str(user.userconfig)


    class Meta:
        model = Route

        fields = [
            "action_type",
            "assigned_to",
            "message",
        ]

        widgets = {
            "action_type": Select(attrs={"width": 40}),
            "assigned_to": Select(attrs={"width": 60}),
            "message": Textarea(attrs={"width": 100, "rows": 3, "clear": "left"}),
        }




class AuditorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AuditorForm, self).__init__(*args, **kwargs)

        self.fields["expires_at"].label = "Auditor access expire date"

        render_form(self)


    class Meta:
        model = UserConfig

        fields = [
            "active",
            "expires_at",
            "auditor_episode_type",
            "user_type",
            "full_name",
            "phone",

        ]

        widgets = {
            "active": CheckboxInput(attrs={"width": 100}),
            "expires_at": TextInput(attrs={"class": "date"}),
            "user_type": Select(attrs={"width": 20}),
            "auditor_episode_type": Select(attrs={"width": 20, "clear": "left"}),
            "full_name": TextInput(attrs={"width": 40}),
            "phone": TextInput(attrs={"width": 20, "class": "phone"})
        }





class UserMgmtForm(ModelForm):
    def __init__(self, *args, **kwargs): 
        super(UserMgmtForm, self).__init__(*args, **kwargs)

        self.fields["access_settings"].label = "Settings"
        self.fields["access_restrictions"].label = "Restrictions"
        self.fields["access_user_mgmt"].label = "User Management"


        if hasattr(self, "instance") and self.instance.user_type == "4":
            self.fields["view_only"].disabled = True


        render_form(self)

    class Meta:
        model = UserConfig

        fields = (
            "full_name",
            "user_type",
            "active",   
            "view_only",         
            "access_settings",
            "access_restrictions",
            "access_user_mgmt",
            "unlock_forms",
            "view_all_sharedlinks",
            "edit_contracts",
            "je_unverify",
            "auditor_mgmt",
            "sacwis_import_mgmt",
            "super_clinic_preadmit",
            "super_clts",
            "super_clts_preadmit",
            "super_crisis",
            "super_cst",
            "super_randi",
            "super_tcm",
            "super_b3",
            "super_b3_preadmit",
            "super_je",
            "super_auth"
        )

        widgets = {
            "full_name": TextInput(attrs={"width": 60}),
            "user_type": Select(attrs={"width": 40}),
            "active": CheckboxInput(attrs={"width": 50}),
            "view_only": CheckboxInput(attrs={"width": 50}),
            "access_settings": CheckboxInput(attrs={"width": 50, "clear": "left"}),
            "access_restrictions": CheckboxInput(attrs={"width": 50}),
            "access_user_mgmt": CheckboxInput(attrs={"width": 50}),
            "unlock_forms": CheckboxInput(attrs={"width": 50}),
            "view_all_sharedlinks": CheckboxInput(attrs={"width": 50}),
            "edit_contracts": CheckboxInput(attrs={"width": 50}),
            "je_unverify": CheckboxInput(attrs={"width": 50}),
            "auditor_mgmt": CheckboxInput(attrs={"width": 50}),
            "sacwis_import_mgmt": CheckboxInput(attrs={"width": 50}),

            # supervisor signer bits for Behavioral Health (client app)
            "super_clinic_preadmit": CheckboxInput(attrs={"width": 50, "clear": "both"}),
            "super_clts": CheckboxInput(attrs={"width": 50}),
            "super_clts_preadmit": CheckboxInput(attrs={"width": 50}),
            "super_crisis": CheckboxInput(attrs={"width": 50}),
            "super_cst": CheckboxInput(attrs={"width": 50}),
            "super_randi": CheckboxInput(attrs={"width": 50}),
            "super_tcm": CheckboxInput(attrs={"width": 50}),
            "super_b3": CheckboxInput(attrs={"width": 50}),
            "super_b3_preadmit": CheckboxInput(attrs={"width": 50}),

            # Supervisor review bits for Business Services (accounting app)
            "super_je": CheckboxInput(attrs={"width": 50}),
            "super_auth": CheckboxInput(attrs={"width": 50}),
        }




class RestrictUrlForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RestrictUrlForm, self).__init__(*args, **kwargs)
        render_form(self)

    class Meta:
        model = RestrictUrl

        fields = (
            "userconfig",
            "url",
        )

        widgets = {
            "url": TextInput(),
        }




class RestrictClientForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RestrictClientForm, self).__init__(*args, **kwargs)
        render_form(self)


    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        "width": 100,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name, birthdate',
        'order': 'last_name, first_name',
        'target': 'client',
        'autocomplete': 'off',
    }))


    class Meta:
        model = RestrictClient

        fields = (
            "userconfig",
            "client",
            "client_lookup",
        )

        widgets = {
            "client": HiddenInput(),
        }




class EmployeeCompForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(EmployeeCompForm, self).__init__(*args, **kwargs)
        render_form(self)

    account_lookup = CharField(label="Account", required=True, widget=TextInput(attrs={
        'width' : 100,
        'class' : 'lookup',
        'app' : 'accounting',
        'mod' : 'Account',
        'fields' : 'name, description, county_name, county_description',
        'order' : 'name',
        'target' : 'account',
        'autocomplete' : 'off',
        }))

    class Meta:
        model = EmployeeComp
        fields = (
            "employee",
            "comp_type",
            "percent",
            "account",
            )

        labels = {}

        widgets = {
            "employee" : HiddenInput(),
            "account" : HiddenInput(),
            "comp_type" : Select(attrs={"width" : 50}),
            "percent" : NumberInput(attrs={"width" : 50}),

        }




class EmployeeConfigForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(EmployeeConfigForm, self).__init__(*args, **kwargs)
        render_form(self)

    home_cash_account_lookup = CharField(label="Home cash account", required=False, widget=TextInput(attrs={
        'width' : 100,
        'class' : 'lookup',
        'app' : 'accounting',
        'mod' : 'Account',
        'fields' : 'name, description, county_name, county_description',
        'order' : 'name',
        'target' : 'home_cash_account',
        'autocomplete' : 'off',
        }))

    class Meta:
        model = Employee
        fields = (
            "active",
            "last_name",
            "first_name",            
            "home_cash_account",
            "home_cash_account_lookup", # not required because it's defined above!!
            )

        labels = {}

        widgets = {
            "first_name" : TextInput(attrs={"width" : 50}),
            "last_name" : TextInput(attrs={"width" : 50}),
            "home_cash_account" : HiddenInput(),

        }





class AppConfigForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AppConfigForm, self).__init__(*args, **kwargs)
        render_form(self)

    class Meta:
        model = AppConfig
        fields = ('key', 'value')


        widgets = {
            "key": TextInput(attrs={"width": 100}),
            "value": TextInput(attrs={"width": 100}),
        }



class UpdateFiscalForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(UpdateFiscalForm, self).__init__(*args, **kwargs)
        render_form(self)

    fiscal_year_open_from = IntegerField(required=True, widget=NumberInput(attrs={ "width" : "15rem" }))
    fiscal_period_open_from = IntegerField(required=True, widget=NumberInput(attrs={ "width" :"15rem" }))







class ServiceConfigForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ServiceConfigForm, self).__init__(*args, **kwargs)
        render_form(self)

    funding_profile_lookup = CharField(label='Funding profile', required=False, widget=TextInput(attrs={
        'width': 100,
        'clear': 'both',
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'FundingProfile',
        'fields': 'name, description',
        'order': 'name',
        'target': 'funding_profile',
        'autocomplete': 'off',
    }))

    class Meta:
        model = Service
        fields = (
            'active',
            'name',
            "funding_profile_lookup",
            'funding_profile',
            'spc_code',
            'procedure_code',
            'default_unit',

            #"pps_brief_service",
            #'pps_aoda',
            #'pps_mh',
            #"pps_core",
            #"pps_b3",
            #"pps_unit",
        )

        labels = {
            "spc_code": "SPC code",
        }

        widgets = {
            'active': CheckboxInput(attrs={'width': 50, 'clear': 'both'}),
            'name': TextInput(attrs={'width': 100, "clear": "left"}),
            'spc_code': TextInput(attrs={'width': 33.3}),
            'procedure_code': TextInput(attrs={'width': 33.3}),
            'default_unit': NumberInput(attrs={'width': 33.3}),
            #"pps_brief_service": CheckboxInput(attrs={"width": 33}),
            #"pps_aoda": CheckboxInput(attrs={"width": 33}),
            #"pps_mh": CheckboxInput(attrs={"width": 33}),
            #"pps_core": CheckboxInput(attrs={"width": 33}),
            #"pps_b3": CheckboxInput(attrs={"width": 33}),
            #"pps_unit": Select(attrs={"width": 33, "clear": "left"}),
            "funding_profile": HiddenInput(),
        }

class ServiceFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(ServiceFilterForm, self).__init__(*args, **kwargs)
        render_form(self)

    active = ChoiceField(required=False, choices=ACTIVE_FILTER_CHOICES, widget=Select(attrs={'width':50, 'clear':'both'}))
    name = CharField(required=False, widget=TextInput(attrs={'width':50, 'clear':'both'}))
    spc_code = CharField(required=False, label="SPC code", widget=TextInput(attrs={'width':50, 'clear' : 'both'}))
    procedure_code = CharField(required=False, widget=TextInput(attrs={'width':50, 'clear' : 'both'}))
    funding_profile = CharField(required=False, widget=HiddenInput())
    funding_profile_lookup = CharField(label='Funding profile', required=False, widget=TextInput(attrs={
        'width' : 50,
        'clear' : 'both',
        'class' : 'lookup',
        'app' : 'accounting',
        'mod' : 'FundingProfile',
        'fields' : 'name, description',
        'order' : 'name',
        'target' : 'funding_profile',
        'autocomplete' : 'off',
    }))

    class Meta:
        fields = (
            'name',
            'spc_code',
            'procedure_code',
            'funding_profile',
            'funding_profile_lookup',
            )


class DiagnosisConfigForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DiagnosisConfigForm, self).__init__(*args, **kwargs)
        render_form(self)

    class Meta:
        model = Diagnosis
        fields = (
            "active", 
            "code", 
            "name",
        )

        widgets = {
            "active": CheckboxInput(attrs={"width": "10rem"}),
            "code": TextInput(attrs={"width": "10rem"}),
            "name": Textarea(attrs={"clear": "both", "rows": 2}),
        }


class DiagnosisFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(DiagnosisFilterForm, self).__init__(*args, **kwargs)
        render_form(self)

    active = ChoiceField(required=False, choices=ACTIVE_FILTER_CHOICES, widget=Select(attrs={'width': 50, 'clear': 'both'}))
    code = CharField(required=False, widget=TextInput(attrs={'width': 50, 'clear': 'both'}))
    name = CharField(required=False, widget=TextInput(attrs={'width': 50, 'clear': 'both'}))





class UserConfigForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(UserConfigForm, self).__init__(*args, **kwargs)

        # hide some fields from auditors. They cant use these features
        # anyway
        if hasattr(self, "instance") and self.instance.user_type == "4":
            self.fields["fiscal_period"].widget = HiddenInput()
            self.fields["fiscal_year"].widget = HiddenInput()
            self.fields["default_service"].widget = HiddenInput()
            self.fields["login_url"].widget = HiddenInput()
            self.fields["notes_under_demo"].widget = HiddenInput()
            self.fields["send_email_when_report_ready"].widget = HiddenInput()
            self.fields["report_email_threshold"].widget = HiddenInput()
            self.fields["email_on_new_episode"].widget = HiddenInput()
            self.fields["notify_on_new_episode"].widget = HiddenInput()
            self.fields["quick_picks"].widget = HiddenInput()


        render_form(self)




    def clean(self):
        self.cleaned_data = super(UserConfigForm, self).clean()
        validation.validate_userconfig(self)


    class Meta:
        model = UserConfig
        fields = (
            "fiscal_year",
            "fiscal_period",
            'full_name',
            'title',
            "credentials",
            'phone',
            "default_service",
            "login_url",
            "enable_dark_mode",
            "clear_form_data",
            "notes_under_demo",
            "send_email_when_report_ready",
            "report_email_threshold",
            'quick_picks',

            "email_on_new_episode",
            "email_on_new_clinic_preadmit_episode",
            "email_on_new_clts_episode",
            "email_on_new_clts_preadmit_episode",
            "email_on_new_crisis_episode",
            "email_on_new_cst_episode",
            "email_on_new_randi_episode",
            "email_on_new_tcm_episode",
            "email_on_new_b3_episode",
            "email_on_new_b3_preadmit_episode",

            "email_on_closing_episode",
            "email_on_closing_clinic_preadmit_episode",
            "email_on_closing_clts_episode",
            "email_on_closing_clts_preadmit_episode",
            "email_on_closing_crisis_episode",
            "email_on_closing_cst_episode",
            "email_on_closing_randi_episode",
            "email_on_closing_tcm_episode",
            "email_on_closing_b3_episode",
            "email_on_closing_b3_preadmit_episode",

            "notify_on_new_episode",
            "notify_on_new_clinic_preadmit_episode",
            "notify_on_new_clts_episode",
            "notify_on_new_clts_preadmit_episode",
            "notify_on_new_crisis_episode",
            "notify_on_new_cst_episode",
            "notify_on_new_randi_episode",
            "notify_on_new_tcm_episode",
            "notify_on_new_b3_episode",
            "notify_on_new_b3_preadmit_episode",

            "notify_on_closing_episode",
            "notify_on_closing_clinic_preadmit_episode",
            "notify_on_closing_clts_episode",
            "notify_on_closing_clts_preadmit_episode",
            "notify_on_closing_crisis_episode",
            "notify_on_closing_cst_episode",
            "notify_on_closing_randi_episode",
            "notify_on_closing_tcm_episode",
            "notify_on_closing_b3_episode",
            "notify_on_closing_b3_preadmit_episode",

        )

        labels = {
            "send_email_when_report_ready": "Send me an email when a report is ready to view",
            "report_email_threshold": "Do not send me emails for reports that complete in less than X seconds",
        }

        widgets = {
            "fiscal_year": NumberInput(attrs={"width": 50}),
            "fiscal_period": NumberInput(attrs={"width": 50}),
            'full_name': TextInput(attrs={'width': 50}),
            'title': TextInput(attrs={'width': 50}),
            "credentials": Select(attrs={"width": 50}),
            'phone': TextInput(attrs={'width': 50}),
            "default_service": Select(attrs={"width": 50}),
            "login_url": TextInput(attrs={'width': 100}),
            "clear_form_data": CheckboxInput(attrs={"width": 100}),
            "notes_under_demo": CheckboxInput(attrs={"width": 100}),
            "send_email_when_report_ready": CheckboxInput(attrs={"width": 100, "clear": "both"}),
            "report_email_threshold": NumberInput(attrs={"width": 15}),
            'quick_picks': Textarea(attrs={"width": 100, 'rows': 3}),


            "email_on_new_episode": CheckboxInput(attrs={"width": 50, "clear": "both"}),
            "email_on_new_clinic_preadmit_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_new_clts_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_new_clts_preadmit_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_new_crisis_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_new_cst_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_new_randi_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_new_tcm_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_new_b3_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_new_b3_preadmit_episode": CheckboxInput(attrs={"width": 50}),

            "email_on_closing_episode": CheckboxInput(attrs={"width": 50, "clear": "both"}),
            "email_on_closing_clinic_preadmit_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_closing_clts_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_closing_clts_preadmit_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_closing_crisis_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_closing_cst_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_closing_randi_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_closing_tcm_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_closing_b3_episode": CheckboxInput(attrs={"width": 50}),
            "email_on_closing_b3_preadmit_episode": CheckboxInput(attrs={"width": 50}),

            "notify_on_new_episode": CheckboxInput(attrs={"width": 50, "clear": "both"}),
            "notify_on_new_clinic_preadmit_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_new_clts_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_new_clts_preadmit_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_new_crisis_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_new_cst_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_new_randi_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_new_tcm_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_new_b3_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_new_b3_preadmit_episode": CheckboxInput(attrs={"width": 50}),

            "notify_on_closing_episode": CheckboxInput(attrs={"width": 50, "clear": "both"}),
            "notify_on_closing_clinic_preadmit_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_closing_clts_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_closing_clts_preadmit_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_closing_crisis_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_closing_cst_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_closing_randi_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_closing_tcm_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_closing_b3_episode": CheckboxInput(attrs={"width": 50}),
            "notify_on_closing_b3_preadmit_episode": CheckboxInput(attrs={"width": 50}),

            "email_enable_dark_mode": CheckboxInput(attrs={"width": 100}),
        }



class VendorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(VendorForm, self).__init__(*args, **kwargs)
        render_form(self)

    def clean(self):
        self.cleaned_data = super(VendorForm, self).clean()
        validation.validate_vendorform(self)


    class Meta:
        model = Vendor
        fields = (
            'active',
            "etf_payment",
            'name',
            'street',
            'suite',
            'city',
            'state',
            'zipcode',
            'vendor_number',
            'wisconsin_provider_id',
            'national_provider_id',
            "sac_provider_id",
            "notes"
        )

        widgets = {
            "active": CheckboxInput(attrs={"width": 100}),
            "etf_payment": CheckboxInput(attrs={"width": 100}),
            "name": TextInput(attrs={"width": 60}),
            'street': TextInput(attrs={'width': 70, 'id': 'add_street'}),
            'suite': TextInput(attrs={'width': 30}),
            'city': TextInput(attrs={'width': 40, 'id': 'add_city'}),
            'state': Select(attrs={'width': 40, 'id': 'add_state'}),
            'zipcode': TextInput(attrs={'width': 20, 'id': 'add_zip'}),
            'vendor_number': TextInput(attrs={'width': 50}),
            'wisconsin_provider_id': TextInput(attrs={'width': 50}),
            'national_provider_id': TextInput(attrs={"width": 50}),
            'sac_provider_id': TextInput(attrs={"width": 50}),
            "notes": Textarea(attrs={"rows": 2, "width": 100}),
        }



class VendorFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(VendorFilterForm, self).__init__(*args, **kwargs)
        render_form(self)

    active = ChoiceField(required=False, choices=ACTIVE_FILTER_CHOICES, widget=Select(attrs={'width': 50, 'clear': 'both'}))
    name = CharField(required=False, widget=TextInput(attrs={'width':50, 'clear':'both'}))
    vendor_number = CharField(required=False, label='Vendor #', widget=TextInput(attrs={'width':50, 'clear':'both'}))

    class Meta:
        fields = ('active', 'name', 'vendor_number')
