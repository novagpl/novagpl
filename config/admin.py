"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from django.contrib import admin
from django.conf import settings
from django.contrib.auth.models import Group, User

from .models import *


admin.site.unregister(Group)

useradmin_exclude = (
    "groups", "is_superuser", "email",
    "user_permissions", "last_login", "default_service", "first_name", 
    "last_name", "date_joined",
)

userconfig_exclude = (
    "quick_picks",
)


if settings.DEBUG:
    class UserConfigAdmin(admin.ModelAdmin):
        exclude = useradmin_exclude

        list_display = (
            "user", "active", "user_type", "view_only", "access_settings",
            "access_restrictions", "access_user_mgmt", "unlock_forms", "full_name"
        )


        def has_delete_permission(self, request, obj=None):
            return False

    class UserAdmin(admin.ModelAdmin):
        exclude = useradmin_exclude

        def has_delete_permission(self, request, obj=None):
            return False

    # Register your models here.
    admin.site.register(UserConfig, UserConfigAdmin)
    admin.site.register(AppConfig)
    admin.site.unregister(User)
    admin.site.register(User, UserAdmin)


else:
    class UserConfigAdmin(admin.ModelAdmin):
        exclude = useradmin_exclude
        list_display = (
            "user", "active", "user_type", "view_only", "access_settings",
            "access_restrictions", "full_name"
        )


        def has_delete_permission(self, request, obj=None):
            return False

    class UserAdmin(admin.ModelAdmin):
        exclude = useradmin_exclude

        def has_delete_permission(self, request, obj=None):
            return False

    admin.site.register(UserConfig, UserConfigAdmin)
    admin.site.unregister(User)
    admin.site.register(User, UserAdmin)
