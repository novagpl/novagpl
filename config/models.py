"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import json
from django.db import models
from django.apps import apps
import datetime
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.conf import settings
from django.core.signing import BadSignature, TimestampSigner

import pytz


# Create your models here.

import client.client_choices
from utils import (
    sign_data, compare_signatures, notify, get_app_config, human_elapsed
)


# override the User model's str function without using a custom user model.
'''
def get_uc_name(self):
    if hasattr(self, "userconfig") and self.userconfig:
        return self.userconfig.__str__()
    else:
        return self.__str__()

User.add_to_class("__str__", get_uc_name)
'''

















class Route(models.Model):
    # Resource routes are simple models that allow for attaching a resource,
    # a sender user, receiver user, and a message/action.
    # The idea is to allow users to be able to send each other docs/forms etc
    # in Nova and either request action be taken or provide a informational
    # message.
    # A Resource Route must be assigned to someone but the sender is optional
    # if the sender is blank, it will default to 'Nova system'
    # If the route has a non-null action_type then it can also track
    # the action_status. At the point of writing, there are the following
    # actions that can be applied:
    # sign/finalize,
    # review form
    # The sign/finalize action can be completed automatically by a function
    # in the emrform model, but the review action type will require manual
    # completion of clicking on a check icon either on the list of actions
    # on the emrform or the user's (caseworker) login screen

    # Instead of using a bunch of different FK fields for each type of resource
    # we can use the django apps.get_model() function to dynamically pull
    # the instance if we need to. The idea is less maintenance
    # TODO see if this can be changed to use Django's Generic Relations
    ACTION_TYPE_CHOICES = (
        ("finalize", "Review, Sign, and Finalize"),
        ("review", "Review"),
        ("supervisor_sign", "MHP review"),
        ("read_msg", "Message"),
    )

    resource_id = models.IntegerField()
    resource_app = models.CharField(max_length=255)
    resource_model = models.CharField(max_length=255)


    created_by = models.ForeignKey("auth.User", on_delete=models.CASCADE, null=True)
    assigned_to = models.ForeignKey("auth.User", related_name="assigned_to", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    completed_at = models.DateTimeField(null=True)
    action_type = models.CharField(max_length=255, choices=ACTION_TYPE_CHOICES)
    message = models.TextField(null=True, blank=True)



    def get_created_by(self):
        # Returns a string to be used in the templates
        if self.created_by is None:
            return "Nova System"
        else:
            return self.created_by.userconfig


    def get_details(self):
        # Returns a details string to be used in shorter format items like
        # listview items
        resource = self.get_resource()

        if not resource:
            return None


        if self.action_type == "finalize":
            return f"Finalize {resource.notify_name()}"

        elif self.action_type == "review":
            return f"Review {resource.notify_name()}"

        elif self.action_type == "read_msg":
            return f"Message regarding {resource.notify_name()}"

        elif self.action_type == "supervisor_sign":
            return f"Supervisor signature needed on {resource.notify_name()}"


    def get_resource(self):
        # returns the instance of the resource that this route is linked to
        try:
            return apps.get_model(
                    self.resource_app,
                    self.resource_model
            ).objects.get(id=self.resource_id)
        except client.models.EmrForm.DoesNotExist:
            return None



    def get_status(self):
        # Returns pending or completed if the completed_at is null or not
        if not self.completed_at:
            return "Pending"
        else:
            return f"Completed on {self.completed_at.strftime('%m/%d/%Y')}"


    def get_human_elapsed(self):
        return human_elapsed(self.created_at, timezone.now())


    def save(self, *args, **kwargs):
        # Fire off any notifications that we need, do not send a notification
        # to the user that created this comment
        # In theory, these comments are only saved once, not updated, but
        # to be sure, only send the notifications if the comment is new

        # Checking if this is a newly created route or one being updated
        # the is_new will be used to decide what kind of notifications to
        # generate.
        if not self.pk:
            is_new = True
            existing = None
            incomplete = None
        else:
            is_new = False
            # The existing instance of this pre-save data will be used to check
            # if this is the first time this route is getting the completed_at
            # value. Checking for this is needed because a notification needs
            # to get sent out on a newly completed route.
            existing = Route.objects.get(id=self.id)

            if existing.completed_at is None:
                incomplete = True
            else:
                incomplete = False



        super().save(*args, **kwargs)

        # Processing notifications after the save because we need to attach
        # the route to the notification. That allows the notification to be
        # easily accessed if the user uncompletes a route and the notification
        # needs to be deleted. Assigning the route as an FK to the notification
        # will not work until the route is saved first.
        resource = self.get_resource()
        if not resource:
            # This tends to be a edge-case that I've noticed that comes
            # up when manually moving an episode in the django shell.
            return

        if is_new:
            # Depending on the action_type, sent out appropriate notification
            if self.action_type == "finalize":
                notify_msg = f"""
                    <a href='{resource.default_url()}'>
                        {resource.notify_name()}</a>
                     is ready to be finalized
                    """

            elif self.action_type == "review":
                notify_msg = f"""
                    <a href='{resource.default_url()}'>
                        {resource.notify_name()}</a>
                     is ready for review and your signature
                """

            elif self.action_type == "supervisor_sign":
                notify_msg = f"""
                    <a href='{resource.default_url()}'>
                        {resource.notify_name()}</a>
                     is ready for a supervisor's signature
                    """

            elif self.action_type == "read_msg":
                notify_msg = f"""
                    {self.created_by.userconfig.full_name} has sent you a
                    message regarding
                    <a href='{resource.default_url()}'>
                        {resource.notify_name()}</a>
                """

            notify(
                users=self.assigned_to,
                message=notify_msg,
                route=self,
            )

        else:
            # Grab the existing record and see if it doesn't have a completed_at
            # value and if this new save has one, then we know it's newly
            # complete and will send a notify to the creator
            # TODO make the alert copy text specific to each action type
            if self.completed_at and incomplete is True:

                # If this is completed, but the existing instance of this
                # model is not completed then we know this is newly completed
                # and need to send a notification.

                if self.action_type == "finalize":
                    notify_msg = f"""
                        {resource.notify_name()} has been
                        electronically signed by {self.assigned_to.userconfig.full_name}
                    """

                elif self.action_type == "supervisor_sign":
                    notify_msg = f"""
                        {resource.notify_name()} has been
                        signed by {self.assigned_to.userconfig.full_name}
                    """

                elif self.action_type == "review":
                    notify_msg = f"""
                        {resource.notify_name()} has been
                        reviewed by {self.assigned_to.userconfig.full_name}
                    """

                elif self.action_type == "read_msg":
                    notify_msg = f"""
                        Your message on {resource.notify_name()} has been
                        read by {self.assigned_to.userconfig.full_name}
                    """


                notify(
                    users=self.created_by,
                    message=notify_msg,
                    route=self,
                )









class Notification(models.Model):
    receiver = models.ForeignKey("auth.User", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    message = models.TextField()
    # This bit gets cleared when the user clicks the little icon in the top
    # right to show the list of notifications
    is_new = models.BooleanField(default=True)
    # This bit gets set by the user when they click a check icon on the
    # notification
    is_read = models.BooleanField(default=False)
    # This bit gets set when the user clicks the archive button on the
    # notification. Archived notifications are not shown on the main list
    # and are only shown when the user clicks "See all"
    is_archived = models.BooleanField(default=False)

    # Many notifications are generated by routes so it's helpful to track
    # especially when a user wants to 'uncomplete' a route and the notification
    # needs to be deleted.
    route = models.ForeignKey("config.Route", null=True, blank=True, on_delete=models.SET_NULL)








class RestrictClient(models.Model):
    userconfig = models.ForeignKey("config.UserConfig", on_delete=models.CASCADE)
    client = models.ForeignKey("client.Client", on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.userconfig} {self.client}"


class RestrictUrl(models.Model):
    userconfig = models.ForeignKey("config.UserConfig", on_delete=models.CASCADE)
    url = models.TextField(null=False, blank=False)

    def __str__(self):
        return f"{self.userconfig} {self.url}"



class ReportView(models.Model):
    # This model is used to track each user's access to the reporting system
    # so we can give them a list of popular and last-accessed reports
    report_url = models.CharField(max_length=255)
    report_name = models.CharField(max_length=255)
    userconfig = models.ForeignKey("config.UserConfig", on_delete=models.CASCADE)
    count = models.IntegerField(default=0)
    last_access = models.DateTimeField()






class AuditTrail(models.Model):
    date_time = models.DateTimeField()
    user_name = models.CharField(max_length=255)
    action_type = models.CharField(max_length=10)
    model_type = models.CharField(max_length=255)
    form_type = models.CharField(max_length=255)
    raw_uri = models.TextField()
    ip_address = models.CharField(max_length=16)
    model_data = models.TextField()
    signature = models.TextField(null=True, blank=False)

    def get_sig_items(self):
        sig_items = {
            "date_time": str(self.date_time),
            "user_name": str(self.user_name),
            "action_type": str(self.action_type),
            "model_type": str(self.model_type),
            "form_type": str(self.form_type),
            "raw_uri": str(self.raw_uri),
            "ip_address": str(self.ip_address),
            "model_data": json.loads(self.model_data) if self.model_data else "",
        }
        return sig_items

    def generate_signature(self, save=False):
        signed_data = sign_data(self.get_sig_items())
        if save:
            self.signature = signed_data
        return signed_data

    def verify_signature(self):
        return compare_signatures(self.generate_signature(), self.signature)

    def get_short_username(self):
        if self.user_name is None:
            return ''
        sar = str(self.user_name).split('|')
        if len(sar) >= 2:
            return sar[1].split('(')[0]
        else:
            return self.user_name

    def get_short_modeltype(self):
        sar = str(self.model_type).split("<class '")
        if len(sar) > 1:
            return sar[1].replace("'>", '').replace('.models.', '.')
        else:
            return self.model_type

    def get_short_formtype(self):
        sar = str(self.form_type).split("<class '")
        if len(sar) > 1:
            return sar[1].replace("'>", '').replace('.forms.', '.')
        else:
            return self.form_type

    def __str__(self):
        return "({}) {} {} {} {} {} {} {}".format(
            self.id,
            str(self.date_time),
            str(self.user_name),
            str(self.action_type),
            str(self.model_type),
            str(self.form_type),
            str(self.raw_uri),
            str(self.ip_address))


class AppConfig(models.Model):
    key = models.CharField(max_length=100, unique=True)
    value = models.CharField(max_length=100)


class UserConfig(models.Model):
    user_type_choices = (
        ('0', 'Inactive'),
        ('1', 'Admin'),
        ('2', 'Caseworker'),
        ('3', 'Financial'),
        ('4', "Auditor"),
    )

    user = models.OneToOneField(User, primary_key=False, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    expires_at = models.DateField(null=True, blank=True)
    default_service = models.ForeignKey("client.Service", null=True, blank=True, on_delete=models.SET_NULL, verbose_name="Default B-3 service")
    full_name = models.CharField(max_length=200, verbose_name="Full name")
    credentials = models.CharField(max_length=255, null=True, blank=True, choices=client.client_choices.STAFF_CRED_CHOICES)
    phone = models.CharField(max_length=15, null=True, blank=True)
    quick_picks = models.TextField(max_length=10240, null=True, blank=True, verbose_name="Quick picks")
    user_type = models.CharField(max_length=1, null=False, blank=False, verbose_name='User type', choices=user_type_choices, default='0')
    title = models.CharField(max_length=255, null=True, blank=True)
    fiscal_year = models.IntegerField(blank=True, null=True)
    fiscal_period = models.IntegerField(blank=True, null=True)
    login_url = models.CharField(max_length=255, null=True, blank=True)
    recent_clients = models.JSONField(null=True, default=list)

    # Explicit security settings here
    # Grants access to the nova settings view
    access_settings = models.BooleanField(default=False)
    # Grants access to the user restriction view
    access_restrictions = models.BooleanField(default=False)
    # Grants access to create/remove/edit nova users easily
    access_user_mgmt = models.BooleanField(default=False, verbose_name="Access user management")
    # Grants ability to re-open forms and case notes
    unlock_forms = models.BooleanField(default=False) 
    # Grants ability to edit contracts and auths
    edit_contracts = models.BooleanField(default=False)
    # Grants ability to view shared links from all users, not just their own
    view_all_sharedlinks = models.BooleanField(default=False, verbose_name="All shared links")
    # Grants access to the auditor management view
    auditor_mgmt = models.BooleanField(default=False, verbose_name="Auditor management")
    # List of clients that an auditor has permission to view
    auditor_clients = models.ManyToManyField("client.Client", related_name="auditor_clients", blank=True)
    # List of episode types, used to restrict an auditor to a specific episode type
    auditor_episode_type = models.CharField(max_length=255, null=True, blank=True, choices=client.client_choices.EPISODE_TYPE)
    # Grants ability to unverify a journal entry
    je_unverify = models.BooleanField(default=False, verbose_name="Journal unverify")
    # Sacwis mgmt
    sacwis_import_mgmt = models.BooleanField(default=False, verbose_name="eWiSACWIS Import")

    # Other options and bits
    view_only = models.BooleanField(default=False)

    # User-settable bits
    clear_form_data = models.BooleanField(default=False, verbose_name="Clear saved form data when logging out")

    email_on_new_episode = models.BooleanField(default=False, verbose_name="When any episode is created")
    email_on_new_clinic_preadmit_episode = models.BooleanField(default=False, verbose_name="When a Clinic pre-admit episode is created")
    email_on_new_clts_episode = models.BooleanField(default=False, verbose_name="When a CLTS episode is created")
    email_on_new_clts_preadmit_episode = models.BooleanField(default=False, verbose_name="When a CLTS pre-admit episode is created")
    email_on_new_crisis_episode = models.BooleanField(default=False, verbose_name="When a Crisis episode is created")
    email_on_new_cst_episode = models.BooleanField(default=False, verbose_name="When a CST episode is created")
    email_on_new_randi_episode = models.BooleanField(default=False, verbose_name="When a R&I episode is created")
    email_on_new_tcm_episode = models.BooleanField(default=False, verbose_name="When a TCM episode is created")
    email_on_new_b3_episode = models.BooleanField(default=False, verbose_name="When a Birth to 3 episode is created")
    email_on_new_b3_preadmit_episode = models.BooleanField(default=False, verbose_name="When a Birth to 3 pre-admit episode is created")

    email_on_closing_episode = models.BooleanField(default=False, verbose_name="When any episode is closed")
    email_on_closing_clinic_preadmit_episode = models.BooleanField(default=False, verbose_name="When a Clinic pre-admit episode is closed")
    email_on_closing_clts_episode = models.BooleanField(default=False, verbose_name="When a CLTS episode is closed")
    email_on_closing_clts_preadmit_episode = models.BooleanField(default=False, verbose_name="When a CLTS pre-admit episode is closed")
    email_on_closing_crisis_episode = models.BooleanField(default=False, verbose_name="When a Crisis episode is closed")
    email_on_closing_cst_episode = models.BooleanField(default=False, verbose_name="When a CST episode is closed")
    email_on_closing_randi_episode = models.BooleanField(default=False, verbose_name="When a R&I episode is closed")
    email_on_closing_tcm_episode = models.BooleanField(default=False, verbose_name="When a TCM episode is closed")
    email_on_closing_b3_episode = models.BooleanField(default=False, verbose_name="When a Birth to 3 episode is closed")
    email_on_closing_b3_preadmit_episode = models.BooleanField(default=False, verbose_name="When a Birth to 3 pre-admit episode is closed")

    notify_on_new_episode = models.BooleanField(default=False, verbose_name="When any episode is created")
    notify_on_new_clinic_preadmit_episode = models.BooleanField(default=False, verbose_name="When a Clinic pre-admit episode is created")
    notify_on_new_clts_episode = models.BooleanField(default=False, verbose_name="When a CLTS episode is created")
    notify_on_new_clts_preadmit_episode = models.BooleanField(default=False, verbose_name="When a CLTS pre-admit episode is created")
    notify_on_new_crisis_episode = models.BooleanField(default=False, verbose_name="When a Crisis episode is created")
    notify_on_new_cst_episode = models.BooleanField(default=False, verbose_name="When a CST episode is created")
    notify_on_new_randi_episode = models.BooleanField(default=False, verbose_name="When a R&I episode is created")
    notify_on_new_tcm_episode = models.BooleanField(default=False, verbose_name="When a TCM episode is created")
    notify_on_new_b3_episode = models.BooleanField(default=False, verbose_name="When a Birth to 3 episode is created")
    notify_on_new_b3_preadmit_episode = models.BooleanField(default=False, verbose_name="When a Birth to 3 pre-admit episode is created")

    notify_on_closing_episode = models.BooleanField(default=False, verbose_name="When any episode is closed")
    notify_on_closing_clinic_preadmit_episode = models.BooleanField(default=False, verbose_name="When a Clinic pre-admit episode is closed")
    notify_on_closing_clts_episode = models.BooleanField(default=False, verbose_name="When a CLTS episode is closed")
    notify_on_closing_clts_preadmit_episode = models.BooleanField(default=False, verbose_name="When a CLTS pre-admit episode is closed")
    notify_on_closing_crisis_episode = models.BooleanField(default=False, verbose_name="When a Crisis episode is closed")
    notify_on_closing_cst_episode = models.BooleanField(default=False, verbose_name="When a CST episode is closed")
    notify_on_closing_randi_episode = models.BooleanField(default=False, verbose_name="When a R&I episode is closed")
    notify_on_closing_tcm_episode = models.BooleanField(default=False, verbose_name="When a TCM episode is closed")
    notify_on_closing_b3_episode = models.BooleanField(default=False, verbose_name="When a Birth to 3 episode is closed")
    notify_on_closing_b3_preadmit_episode = models.BooleanField(default=False, verbose_name="When a Birth to 3 pre-admit episode is closed")

    send_email_when_report_ready = models.BooleanField(default=False)
    report_email_threshold = models.IntegerField(default=10)
    notes_under_demo = models.BooleanField(default=False, verbose_name="Show  case notes under client face sheet")
    enable_dark_mode = models.BooleanField(default=False, verbose_name="Enable dark mode (beta)")

    # Supervisor signature episode types
    super_clinic_preadmit = models.BooleanField(default=False, verbose_name="Supervisor signer for Clinic Pre-Admit")
    super_clts = models.BooleanField(default=False, verbose_name="Supervisor signer for CLTS")
    super_clts_preadmit = models.BooleanField(default=False, verbose_name="Supervisor signer for CLTS Pre-Admit")
    super_crisis = models.BooleanField(default=False, verbose_name="Supervisor signer for Crisis")
    super_cst = models.BooleanField(default=False, verbose_name="Supervisor signer for CST")
    super_randi = models.BooleanField(default=False, verbose_name="Supervisor signer for R&I")
    super_tcm = models.BooleanField(default=False, verbose_name="Supervisor signer for TCM")
    super_b3 = models.BooleanField(default=False, verbose_name="Supervisor signer for B to 3")
    super_b3_preadmit = models.BooleanField(default=False, verbose_name="Supervisor signer for B to 3 Pre-Admit")

    # Supervisor review for financial areas
    super_je = models.BooleanField(default=False, verbose_name="Supervisor reviewer for Journal Entries")
    super_auth = models.BooleanField(default=False, verbose_name="Supervisor signer for auths")



    def __str__(self):
        name = ""
        if self.title is not None and self.title != '':
            name = f"{str(self.title)} {str(self.full_name)}"
        else:
            name = str(self.full_name)

        if self.credentials:
            name += f", {self.get_credentials_display()}"

        return name


    def save(self, *args, **kwargs):
        # If the user is being saved as an auditor, then always set their
        # view_only to true. Auditors will never have edit rights.
        if self.user_type == '4':
            self.view_only = True
            # remove existing permissions
            self.access_settings = False
            self.access_restrictions = False
            self.access_user_mgmt = False
            self.unlock_forms = False
            self.edit_contracts = False
            self.view_all_sharedlinks = False
            self.auditor_mgmt = False
            self.email_on_new_episode = False
            self.supervisor_signer = False



        # IF the active bit is set to false or the user type is set to inactive
        # the other should be properly set.
        # TODO probably deprecate the active bit and just use user_type
        if self.active is False:
            self.user_type = '0'
        if self.user_type == '0':
            self.active = False

        super().save(*args, **kwargs)



    def delete(self):
        # To delete this model, delete the User model it's one-to-one linked to
        pass


    def get_auto_name(self):
        return self.__str__()


    def default_url(self):
        return f"/config/user_mgmt/{self.id}/"


    def get_notifications(self, include_read=False, include_archived=False, order_by=None, get_all=None):
        notifys = Notification.objects.filter(
            receiver=self.user,
        )

        if include_read is False:
            notifys = notifys.filter(is_read=False)

        if include_archived is False:
            notifys = notifys.filter(is_archived=False)

        if order_by:
            notifys = notifys.order_by(order_by)
        else:
            notifys = notifys.order_by("-created_at")

        if get_all is None:
            return notifys[:20]
        else:
            return notifys





    def has_unread_notifications(self):
        query = Notification.objects.filter(receiver__id=self.id, is_read=False)
        return query.exists(), query.count()

    def has_new_notifications(self):
        # A bit misleading but this function returns a bool if there are new
        # notifications and also returns the number of unread (not new)
        # notifications
        query = Notification.objects.filter(receiver=self.user)
        return query.filter(is_new=True).exists(), query.filter(is_read=False).count()


    def add_recent_client(self, client):
        # recent_clients
        # For now, it's an arbitrary limit of 20 clients.
        # Steps for this logic:
        #   1. Check if the client already exists in the list, if it does
        #       then just move it to the front and save
        #   2. If the client isn't in the list then add it to the top
        #   3. If there are more than 20 ids in the list, pop them off
        #       the bottom until there are 20 and then save

        if self.recent_clients is None:
            self.recent_clients = []


        client_data = {"id": client.id, "name": str(client)}

        if client_data in self.recent_clients:
            self.recent_clients.remove(client_data)

        self.recent_clients.insert(0, client_data)

        if len(self.recent_clients) > 20:
            self.recent_clients = self.recent_clients[:20]


        self.save()







    def toolbar_data(self):
        fiscal_year = self.fiscal_year or int(get_app_config("fiscal_year_open_from"))
        fiscal_period = self.fiscal_period or int(get_app_config("fiscal_period_open_from"))
        agency_name = get_app_config("agency_name") or ''

        data = []

        data.append(agency_name)
        data.append(f"Fiscal {fiscal_year}/{fiscal_period}")
        data.append(f"<a href='/config/user_config/'><span class='fas fa-user'></span>&nbsp;&nbsp;{self.full_name }</a>")

        # Check if the user has any unread notifications, even if they have zero
        # still show the icon and give them the ability to view read and
        # archived notifications.
        # If there are unread notifications we need to show a brightly
        # colored icon
        notifys = self.get_notifications(include_read=True)
        has_new, new_count = self.has_new_notifications()
        # Need to use list comprehension because get_notificaitons() returns
        # a sliced list, otherwise we could just requery and use .count()
        unread_count = len([x for x in notifys if x.is_read is False])

        if unread_count != 0:
            unread_count = f"&nbsp{unread_count}"
        else:
            unread_count = ""


        if notifys:
            if has_new:
                data.append(f"<a id='notify-icon' href='javascript:showNotifications();'><span class='fas fa-bell orange'></span><span id='unread-count'>{unread_count}</span></a>")
            else:
                data.append(f"<a id='notify-icon' href='javascript:showNotifications();'><span class='far fa-bell'></span><span id='unread-count'>{unread_count}</span></a>")



        # Check if the user has any open case notes, so they can be displayed
        # to the user in the toolbar. clicking on the open case notes icon
        # takes the user to a report that shows all their open notes
        open_notes = apps.get_model("client", "EmrCaseNoteForm").objects.filter(
            created_by=self.user, is_open=True)
        if open_notes:
            data.append(f"<a href='/report/emr_case_note_report_my_open/'><span class='far fa-clipboard'></span>&nbsp;&nbsp;{len(open_notes)}</a>")


        # Only staff can have access to the admin panel
        if self.user.is_staff and self.user_type == '1':
            data.append("<a href='/admin/' target='_blank'>Admin</a>")

        data.append("<a href='/logout/' class='fas fa-sign-out-alt'></a>")

        return "&nbsp;&nbsp;|&nbsp;&nbsp;".join(data)





class UserForm(models.Model):
    """ This model is used to store the way the user last filled
        any forms. This is saved and retrieved in the render_form function
    """
    userconfig = models.ForeignKey("config.UserConfig", on_delete=models.CASCADE)
    form_class = models.CharField(max_length=255)
    form_data = models.TextField()




