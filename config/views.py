"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import json
import csv
import re
import uuid
import time

import django
from django.core.serializers.json import DjangoJSONEncoder
from django.shortcuts import render, get_object_or_404
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse, Http404, JsonResponse
from django.contrib.auth.signals import user_logged_out
from django.conf import settings
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.core.exceptions import PermissionDenied
from django.utils import timezone
from django.apps import apps



from annoying.functions import get_object_or_None


from .forms import (
    VendorFilterForm, VendorForm, ServiceFilterForm, ServiceConfigForm,
    DiagnosisConfigForm, DiagnosisFilterForm, EmployeeConfigForm,
    EmployeeCompForm, UserConfigForm, AppConfigForm, UpdateFiscalForm,
    RestrictClientForm, RestrictUrlForm, UserMgmtForm, AuditorForm,
    RouteForm, RouteForSignatureForm)

from client.models import (
    Service, Client, Diagnosis, PpsAodaEpi, PpsMhEpi, PpsCoreEpi,
    PpsB3Epi, EmrForm, EmrDoc)

from report.models import CCReport

from utils import (
    access_control, save_confirm, save_audit, delete_confirm,
    delete_audit, set_autocomplete, get_fiscal_year, get_fiscal_period,
    get_app_config, get_authorized_users, get_app_config_object, get_model,
    link_models, format_errors, view_audit, human_elapsed, get_next_batch_name)

from accounting.models import (
    Employee, Vendor, Authorization,
    TransactionLot, Account, FundingProfile, EmployeeComp, Contract,
    Transaction, ContractService)

from accounting.accounting_choices import TRANS_UNIT

from .models import (
    UserConfig, AppConfig, AuditTrail, UserForm, RestrictClient, RestrictUrl,
    Notification, Route)


import config.changelog



@access_control("admin", "caseworker", "financial")
def ajax_check_warn(request):
    # Depending on the command this function checks something and if the
    # conditions are met, returns a little warning for the user
    # Typically, only an ID of a model will get sent, but sometimes the
    # payload will be more complex.
    # All payloads MUST HAVE A COMMAND

    if request.method != "POST":
        raise HttpResponse(status=400)


    cmd = request.POST.get("command")
    if not cmd:
        raise HttpResponse(status=400)


    if cmd == "check_contract_warning":
        # Expect an ID of the contract
        auth_id = request.POST.get("auth_id")
        if not auth_id.isdigit():
            return HttpResponse(status=400)

        auth = get_object_or_None(apps.get_model("accounting.Authorization"), id=auth_id)

        if auth and auth.contract:
            # Check if the utilization is greater than the contract limit in
            # app settings. If it's not in app settings, the default value
            # can be 80%
            limit_string = get_app_config("contract_warning")
            if limit_string and limit_string.isdigit():
                limit = int(limit_string)
            else:
                limit = 80

            contract_usage = auth.contract.get_usage()[2]
            if contract_usage >= limit:
                return JsonResponse({
                    "usage": round(contract_usage),
                    "limit": limit,
                })

        return HttpResponse("OK")

    else:
        raise HttpResponse(status=400)






@access_control("admin", "caseworker", "financial")
def ajax_initial_value(request):
    if request.method != "POST":
        raise HttpResponse(status=400)


    # This function takes an ajax request containing information about a model
    # field on a form in a template and returns a list of fields and their
    # new values.
    # The general idea is that there needs to be a way for fields to receive
    # 'initial' values when another field is changed. There is some similar
    # functionality in the lookup_data utility function, but it only works
    # with FKs and lookup fields. This can work with any HTML element.

    # Depending on the field and model the instance values may not be used
    form_name = request.POST.get("form_name")
    source_field = request.POST.get("source_field")
    source_value = request.POST.get("source_value")
    instance_name = request.POST.get("instance_name")
    instance_id = request.POST.get("instance_id")

    # Data contains a dictionary list of the IDs of the fields and their
    # values that will be sent back to the form
    data = {}


    if form_name == "authorization":
        if source_field == "id_service" and source_value:
            # Can't directly look up contractservice, will need to go through
            # the contract and its contract services set first
            contract = get_object_or_None(Contract, id=instance_id)
            contractservice = contract.contractservice_set.filter(service__id=source_value).first()



            if contractservice:
                data["id_rate"] = contractservice.rate

                # Attempting to match the contractservice's typed in unit to
                # the unit types available in TRANS_UNIT choices
                # This is because the TRANS_UNIT choices use
                # numbers instead of useful text as its value key
                if contractservice.unit:
                    for transunit in TRANS_UNIT:
                        if transunit[1].lower() == contractservice.unit.lower():
                            data["id_unit_type"] = transunit[0][0]
                            break

    elif form_name == "transaction_lot":
        if source_field == "id_batch_type":
            next_batch_name = get_next_batch_name(
                source_value.upper(), get_fiscal_year(request))

            if next_batch_name:
                data["id_name"] = next_batch_name
            else:
                data["id_name"] = ""







    return JsonResponse(data)





@access_control("admin", "caseworker", "financial")
def sig_edit(request, route_id=None, resource_app=None, resource_model=None, resource_id=None):
    # I don't think this function needed. TODO Look at removing
    pass


@access_control("admin", "caseworker", "financial")
def ajax_route_complete(request, route_id):
    # Happens when the user clicks the complete button/icon on a route.
    route = get_object_or_404(Route, id=route_id)
    if route.assigned_to != request.user:
        raise Exception("Route must be completed by the assignee")

    route.completed_at = timezone.now()

    save_audit(request, route)

    return HttpResponse("OK")


@access_control("admin", "caseworker", "financial")
def ajax_route_uncomplete(request, route_id):
    # Happens when the user clicks the complete button/icon on a route that
    # has already been completed. (ie, a mistake and they need to uncomplete it)
    route = get_object_or_404(Route, id=route_id)
    if route.assigned_to != request.user:
        raise Exception("Route must be uncompleted by the assignee")

    route.completed_at = None

    # Attempt to locate the pending notification for the creator of this
    # route (creators of routes are notified when routes are completed)
    # and delete it because the user is uncompleting it.
    found = Notification.objects.filter(
        receiver=route.created_by,
        route=route,
    ).order_by("-created_at").first()

    if found:
        found.delete()

    save_audit(request, route)

    return HttpResponse("OK")




@access_control("admin", "caseworker", "financial")
def route_viewall(request, resource_app=None, model_name=None, resource_id=None):
    # Returns all the routes under a model for the logged in user
    routes = Route.objects.filter(
        resource_app=resource_app,
        resource_model=model_name,
        resource_id=resource_id,
        assigned_to=request.user,
    ).order_by("-created_at")

    return render(request, "config/route_viewall.html", {
        "routes": routes
    })

    return routes



@access_control("admin", "caseworker", "financial")
def route_supervisor_sign_emrform(request, emrform_id):
    # This view is shown after a user finalizes an EmrForm which requires a
    # supervisor signature. It's very similar to the normal route_edit except
    # that it's specifically set up for the task of routing a finalized emrform
    # to a supervisor. The user is only given a dropdown of possible users
    # who have the supervisor_signer bit set to true for this type of episode.

    emrform = get_object_or_404(EmrForm, id=emrform_id)
    title = "Route to Mental Health Professional"


    if request.method == "GET":
        form = RouteForSignatureForm()

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"

        #form = set_autocomplete(emrform, form, "FIELD_NAME")


        form.fields["action_type"].initial = "supervisor_sign"

        # Only people who have the proper bit set should show up on the list
        # of options of who to route this form to for a signature. The code
        # below handles that. It uses a cool trick to use a string for the
        # field name.
        if emrform.emrformtype.needs_supervisor_signature:
            bit_string = f"userconfig__super_" + emrform.emrepisode.episode_type

        signers = User.objects.filter(
            **{bit_string: True},
            userconfig__active=True,
        )

        form.fields["assigned_to"].queryset = signers

        return render(request, "config/supervisor_sign_emrform.html", {
            'form': form,
            'title': title,
            'popup': popup,
            "emrform": emrform,
            "hide_delete": True,
        })


    elif request.method == "POST":
        # User's cant delete these types of routes themselves, so the delete
        # logic is commented out/not implemented
        '''
        if request.POST.get('delete'):
            delete_audit(request, emrform)
            return delete_confirm(request, emrform, f"/APP/MODEL_URL")
        '''

        form = RouteForSignatureForm(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)

            obj.created_by = request.user
            obj.resource_app = "client"
            obj.resource_id = emrform.id
            obj.resource_model = "emrform"
            obj.action_type = "supervisor_sign"



            return save_confirm(
                request,
                save_audit(request, obj),
                f"/client/emr_episodes/{emrform.emrepisode.client.id}/{emrform.emrepisode.id}/")
        else:
            return format_errors(request, form=form)










@access_control("admin", "caseworker", "financial")
def route_edit(request, route_id=None, resource_app=None, resource_model=None, resource_id=None):
    if route_id:
        instance = get_object_or_404(Route, id=route_id)
        title = "Route"
    else:
        instance = None
        title = "New Route"


    if request.method == "GET":
        form = RouteForm(instance=instance)

        # Only want to be able to assign routes to users that are enabled,
        # that are not financial or auditors, and not the user creating
        # this route.
        form.fields["assigned_to"].queryset = User.objects.filter(
            userconfig__active=True,
            userconfig__user_type__in=["1", "2"],
        ).exclude(id=request.user.id).order_by("userconfig__full_name")

        # The route should default to the user who created this resource
        # (assuming the resource has a created_by field)
        resource_instance = get_object_or_None(apps.get_model(
            resource_app, resource_model), id=resource_id)

        if hasattr(resource_instance, "created_by"):
            form.fields["assigned_to"].initial = resource_instance.created_by


        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"


        return render(request, "config/route_edit.html", {
            'form': form,
            'title': title,
            'popup': popup,
            "instance": instance,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance)

        form = RouteForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save(commit=False)
            # Get data about the resource that is being commented on
            resource = apps.get_model(
                resource_app, resource_model).objects.get(id=resource_id)

            obj.created_by = request.user
            obj.resource_app = resource_app
            obj.resource_model = resource_model
            obj.resource_id = resource_id
            if obj.assigned_to is None and hasattr(resource, "created_by"):
                obj.assigned_to = resource.created_by

            return save_confirm(request, save_audit(request, obj))
        else:
            return format_errors(request, form=form)







@access_control("admin", "caseworker", "financial")
def ajax_read_notification(request, notify_id):
    if request.method == "POST":
        # Change the is_read bit to true on the notification
        note = get_object_or_404(Notification, id=notify_id)
        note.is_read = True
        note.save()
        return HttpResponse("OK")
    else:
        return HttpResponse(status=400)




@access_control("admin", "caseworker", "financial", "auditor")
def ajax_check_new_notifications(request):
    # A bit misleading but this function returns a bool if there are new
    # notifications and also returns the number of unread (not new)
    # notifications
    has_new, count = request.user.userconfig.has_new_notifications()
    return HttpResponse(json.dumps([has_new, count]))



@access_control("admin", "caseworker", "financial", "auditor")
def ajax_get_notifications(request):
    if request.user.userconfig.user_type == "4":
        return HttpResponse("")


    # Gets a list of unread and unarchived notifications for the current user
    notifications = request.user.userconfig.get_notifications(
        include_read=True)


    '''notifications = Notification.objects.filter(
        receiver=request.user,
        is_read=False,
        is_archived=False,
    ).order_by("-created_at")
    '''

    data = []
    for item in notifications:
        fmt_dt = human_elapsed(item.created_at)

        data.append({
            "id": item.id,
            "created_at": f"{fmt_dt}",
            "message": item.message,
            "is_read": item.is_read,
            "is_new": item.is_new,
            "is_archived": item.is_archived,
        })

        # Making sure to set the is_new to false so these
        # don't show up in the new list again
        item.is_new = False
        item.save()


    return HttpResponse(json.dumps(data))







@access_control("admin", "caseworker", "financial")
def ajax_verify_password(request):
    # This endpoint is called by verifyPassword() in script.js
    # this function handles two things, verifying that the user entered
    # the correct password. and then preparing the emr form for a signature
    # part of preparing the form is setting the pwd_provided bit and the
    # user who is signing the note. Then return the URL for applying the actual
    # encrypted signature data to the form.
    if request.method == "POST":

        # Verify the password and return a OK or an error
        if request.user.check_password(request.POST.get("pwd")):
            # Route the user depending on the instanceType or InstanceId
            # (or potentially just return a 200 if the password is good)
            sig_type = request.POST.get("sig_type")
            instance_id = request.POST.get("instance_id")
            if sig_type and instance_id:
                if sig_type == "emr_form":
                    # Save the pwd_provided bit on this model so we can know
                    # it's been provided and safely sign/close the note
                    emr_form = EmrForm.objects.get(id=instance_id)

                    # If this form is already signed, and the user is a supervisor
                    # with rights to sign, emr_form.supervisor_can_sign() is True
                    if emr_form.supervisor_can_sign(request.user):
                        emr_form.supervisor_pwd_provided = True
                        emr_form.supervisor_signer = request.user
                        emr_form.save()

                        redirect_url = f"/client/emr_form/{emr_form.id}/supervisor_sign/"

                    else:
                        emr_form.pwd_provided = True
                        emr_form.signed_by = request.user
                        emr_form.save()

                        redirect_url = f"/client/emr_form/{emr_form.id}/sign/"

                    return HttpResponse(redirect_url)

                elif sig_type == "auth_user" or sig_type == "auth_super":
                    auth = Authorization.objects.get(id=instance_id)

                    if sig_type == "auth_user":
                        auth.user_pwd_provided = True
                        auth.user_signed_by = request.user
                        auth.user_signed_date = timezone.now()
                        auth.save()
                        auth.save_history("user_sign", request)

                    elif sig_type == "auth_super":
                        auth.super_pwd_provided = True
                        auth.super_signed_by = request.user
                        auth.super_signed_date = timezone.now()
                        auth.save()
                        auth.save_history("super_sign", request)

                    else:
                        raise Exception(f"Unknown sig_type '{sig_type}'")


                    redirect_url = f"/accounting/auth/{auth.id}/"
                    return HttpResponse(redirect_url)

                else:
                    raise Exception(f"Unknown sig_type '{sig_type}'")

            else:
                return HttpResponse("")


        else:
            time.sleep(3)
            return HttpResponse(status=401)




@access_control("auditor_mgmt")
def auditor_edit(request, auditor_id=None):
    if auditor_id:
        instance = get_object_or_404(
            UserConfig, id=auditor_id, user_type='4')

        # If the user is not an auditor then redirect to the user mgmt page
        if instance.user_type != "4":
            return HttpResponseRedirect("/config/user_mgmt/")

        title = "Auditor"
    else:
        instance = None
        title = "New Auditor"


    if request.method == "GET":
        form = AuditorForm(instance=instance)

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"

        #form = set_autocomplete(instance, form, "FIELD_NAME")


        return render(request, "config/auditor_edit.html", {
            'form': form,
            'title': title,
            'popup': popup,
            "instance": instance,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, "/config/user_mgmt/")

        form = AuditorForm(request.POST, instance=instance)
        if form.is_valid():
            return save_confirm(request, save_audit(request, form))
        else:
            return format_errors(request, form=form)





@access_control("auditor_mgmt")
def auditor_client_remove(request, auditor_id, client_id):
    if request.user.userconfig.auditor_mgmt is not True:
        view_audit(request, "WARN")
        raise PermissionDenied
    # Unlink an userconfig + client many-to-many and return to the
    # auditor edit view
    userconfig = get_object_or_404(UserConfig, id=auditor_id)
    client = get_object_or_404(Client, id=client_id)

    userconfig.auditor_clients.remove(client)

    return HttpResponseRedirect(f"/config/auditor_edit/{auditor_id}/")














@access_control("access_user_mgmt")
def user_mgmt_index(request):
    # This view is for creating/editing/disabling users in nova
    # if a user is created by accident, the only way to delete them
    # is to contact someone who has root access to the database

    # This view will consist of a listivew of users with simple actions
    # and a small form for creating users
    # (maybe in the future a view to edit the user's name and email)
    form = UserMgmtForm()
    title = "User Management"

    users = UserConfig.objects.all().order_by("-active", "full_name")


    return render(request, "config/user_mgmt_index.html", {
        "form": form,
        "title": title,
        "users": users,
    })



@access_control("access_user_mgmt")
def user_mgmt_edit(request, user_mgmt_id):
    instance = get_object_or_404(UserConfig, id=user_mgmt_id)

    # If this is a auditor, redirect to the auditor edit view
    if instance.user_type == '4':
        return HttpResponseRedirect(f"/config/auditor_edit/{user_mgmt_id}/")

    frm = UserMgmtForm


    title = "Manage User"

    if request.method == "GET":
        form = frm(instance=instance)

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"


        return render(request, "config/user_mgmt_edit.html", {
            'form': form,
            'title': title,
            'popup': popup,
            "instance": instance,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, "/config/user_mgmt/")

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            return save_confirm(request, save_audit(request, form))
        else:
            return format_errors(request, form=form)


@access_control("access_user_mgmt")
def user_mgmt_create_user(request, email, full_name, user_type):
    fy = get_app_config("fiscal_year_open_from")
    fp = get_app_config("fiscal_period_open_from")

    password = User.objects.make_random_password()

    user = User.objects.create_user(
        username=email,
        email=email,
    )
    user.set_password(password)
    user.save()


    config = UserConfig()
    config.user = user
    config.full_name = full_name
    config.email = email
    config.user_type = user_type
    config.fiscal_period = fp
    config.fiscal_year = fy
    if user_type == '4':
        config.view_only = True
        config.expires_at = timezone.now() + timezone.timedelta(days=30)
    config.save()

    print("New Nova user created: {}".format(config))


    html = f"""
        Hi {config.full_name},
        <br><br>
        Your account on Nova is active. To log in you need to set up a
        password which can be done here:<br>
        <a href='{get_app_config("url")}/accounts/password_reset/'>
            {get_app_config("url")}/accounts/password_reset/
        </a>
        <br><br>
        <strong>Your login is your work email address in lowercase.</strong><br>
        <strong>The login URL is
            <a href='{get_app_config("url")}'>{get_app_config("url")}</a>
        </strong>
        <br><br><br>
        - Sincerely, the Nova Login Team



    """

    try:
        send_mail(
            subject=f"Nova login information for {config}",
            html_message=html,
            message=html,
            recipient_list=[settings.DEBUG_DEV_EMAIL, email, ],
            from_email=f"Nova <{settings.NOVA_SYSTEM_EMAIL}>",
        )
    except Exception as e:
        print(f"Failed to send email\n{e}")


    return HttpResponseRedirect("/config/user_mgmt/")

















@access_control("access_restrictions")
def restrict_client_edit(request, pk=None):
    frm = RestrictClientForm

    if pk:
        instance = get_object_or_404(RestrictClient, id=pk)
        title = "Client Restriction"
    else:
        instance = None
        title = "New Client Restriction"


    if request.method == "GET":
        form = frm(instance=instance)

        form.fields["userconfig"].queryset = UserConfig.objects.all().order_by("full_name")

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"

        form = set_autocomplete(instance, form, "client")


        return render(request, "config/restrict_client_edit.html", {
            'form': form,
            'title': title,
            'popup': popup,
            "instance": instance,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, "/config/user_restrict")

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            return save_confirm(request, save_audit(request, form))
        else:
            return format_errors(request, form=form)






@access_control("access_restrictions")
def restrict_url_edit(request, pk=None):
    frm = RestrictUrlForm

    if pk:
        instance = get_object_or_404(RestrictUrl, id=pk)
        title = "URL Restriction"
    else:
        instance = None
        title = "New URL Restriction"


    if request.method == "GET":
        form = frm(instance=instance)

        form.fields["userconfig"].queryset = UserConfig.objects.all().order_by("full_name")


        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"


        return render(request, "config/restrict_url_edit.html", {
            'form': form,
            'title': title,
            'popup': popup,
            "instance": instance,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, "/config/user_restrict")

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            return save_confirm(request, save_audit(request, form))
        else:
            return format_errors(request, form=form)


@access_control("access_restrictions")
def user_restrict_index(request):
    url_data = RestrictUrl.objects.all()
    client_data = RestrictClient.objects.all()

    return render(request, "config/user_restrict_index.html", {
        "title": "User Restrictions",
        "table_class": "listview-table",
        "url_data": url_data,
        "client_data": client_data,
    })



@access_control()
def check_if_exist(request, model_name, field, value, model_id=None):
    # This is an ajax call that will return a true or false depending
    # if the specified model + field + value exists in the database
    model_name = model_name.lower()

    #import pdb; pdb.set_trace()

    if model_name == "vendor" and field == "name":
        if model_id:
            found = Vendor.objects.filter(name__iexact=value).exclude(id=model_id)
        else:
            found = Vendor.objects.filter(name__iexact=value)

    if found:
        return HttpResponse("true")
    else:
        return HttpResponse("false")






@access_control("admin", "financial")
def employee_index(request):
    data = Employee.objects.all().order_by("-active", "last_name", "first_name")

    return render(request, 'config/employee_index.html', {
        'title': 'Employees',
        'table_class': "listview-table",
        'employee_data': data,
    })


@access_control("admin", "financial")
def employee_edit(request, pk=None):
    frm = EmployeeConfigForm

    if pk:
        instance = get_object_or_404(Employee, id=pk)
        title = "Employee"
    else:
        instance = None
        title = "New Employee"

    if request.method == "GET":
        form = frm(instance=instance)

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"

        form = set_autocomplete(instance, form, "home_cash_account")

        comps = []
        comp_percent = 0
        if instance:
            comps = EmployeeComp.objects.filter(employee=instance)
            comp_percent = instance.get_comp_percent()



        return render(request, "config/employee_edit.html", {
            'form': form,
            'title': title,
            'popup': popup,
            "comp_data": comps,
            "instance": instance,
            "comp_percent": comp_percent,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, "/config/employee")

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            return save_confirm(request, save_audit(request, form))
        else:
            return format_errors(request, form=form)



@access_control("admin", "financial")
def employee_comp_edit(request, pk=None, parent_id=None):
    frm = EmployeeCompForm

    if pk:
        instance = get_object_or_404(EmployeeComp, id=pk)
        title = "Compensation"
    else:
        instance = None
        parent = get_object_or_None(Employee, id=parent_id)
        title = "New Compensation"

    if request.method == "GET":
        if instance:
            form = frm(instance=instance)
            form = set_autocomplete(instance, form, "account")
        else:
            form = frm()
            form.fields["employee"].initial = parent


        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"


        return render(request, "config/employee_comp_edit.html", {
            'form':form,
            'title':title,
            'popup' : popup,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance)

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            return save_confirm(request, save_audit(request, form))
        else:
            return format_errors(request, form=form)






@access_control()
def changelog(request):
    # TODO move this to some sort of external file or something
    '''
        Changes come in 4 types: Fix, Feature, Style, Docs, Performance
        Changes have 4 fields: date, scope, change_type, description
    '''
    changes = config.changelog.get_changes()

    date_group = {}
    scope_group = {}

    dates = {}
    for x in changes:
        date = x[0]
        scope_type = x[1]
        change_type = x[2]
        desc = x[3]

        if date not in dates:
            dates[date] = {}

        if scope_type not in dates[date]:
            dates[date][scope_type] = []

        dates[date][scope_type].append([change_type, desc])

    return render(request, "config/changelog.html", {
        'title': 'Changelog',
        "changes": dates,
    })



@access_control()
def link_pl(request):
    """
        Link Popup Lookup was originally designed as a way to link a
        transaction to a journal entry through a view called a popup lookup.
        This function is generic and uses some helper functions to
        do the appropriate link between the child (transaction) and the
        header (journal entry).
    """

    child_mod = request.GET.get("mod")
    parent_id = request.GET.get("pid")
    parent_mod = request.GET.get("pmod")
    selected_id = request.GET.get("sel")

    parent = get_model(parent_mod, parent_id)
    child = get_model(child_mod, selected_id)

    saved_child = link_models(request, parent, child)

    return save_confirm(request, saved_child, parent.default_url(), True)


@access_control()
def popup_lookup(request):
    # javascript:showPopup('/config/popup_lookup/?mod=transaction&pmod=journal_entry&pid={{instance.id}}', 'wider');

    child_mod = request.GET.get("mod")
    parent_id = request.GET.get("pid")
    parent_mod = request.GET.get("pmod")
    template_name = request.GET.get("template_name")  # override the standard template naming
    table_class = request.GET.get("table_class")  # override the standard table styling


    # TODO use the django.app.app.get_model one day
    parent = get_model(parent_mod, parent_id)


    print("PARENT {}".format(parent))

    if child_mod == 'transaction':
        items = Transaction.objects.filter(journal_entry=None).order_by("-date")[:100]

    elif child_mod == "EmrDoc":
        items = EmrDoc.objects.filter(
            emrepisode=parent.emrepisode
        ).exclude(
            emrform=parent
        ).order_by("-date")

    elif child_mod == "service":
        items = Service.objects.filter(active=True).order_by("name")

    elif child_mod == "client":
        items = Client.objects.filter(active=True).order_by(
            "last_name", "first_name", "birthdate").exclude(auditor_clients=parent)



    else:
        raise Exception(f"Child_mod: {child_mod} not implemented for popup_lookp")


    if template_name is None:
        template_name = "config/{}_lookup.html".format(child_mod.lower())

    if table_class is None:
        table_class = "lookup_popup listview-table-no-order"
    else:
        table_class = "lookup_popup {}".format(table_class)

    anchor = "/config/link_pl/?mod={}&pid={}&pmod={}&sel=".format(
        child_mod, parent_id, parent_mod)


    return render(request, template_name, {
        'parent': parent,
        'lookup': items,
        'child_mod': child_mod,
        'popup': 'popup',
        'popup_lookup': 'popup_lookup',
        'title': 'Select {}'.format(child_mod),
        'anchor': anchor,
        'table_class': table_class,
    })



@access_control("admin")
def service_index(request):
    form = ServiceFilterForm()

    if request.method == "POST":
        active = request.POST.get('active')
        name = request.POST.get('name')
        spc_code = request.POST.get('spc_code')
        procedure_code = request.POST.get('procedure_code')

        funding_profile = request.POST.get('funding_profile') or None
        form = set_autocomplete(None, form, 'funding_profile', FundingProfile, funding_profile)


        form.fields['active'].initial = active
        form.fields['name'].initial = name
        form.fields['spc_code'].initial = spc_code
        form.fields['procedure_code'].initial = procedure_code

        query = Q()
        if active != '':
            query.add(Q(active=active), Q.AND)
        if name:
            query.add(Q(name__icontains=name), Q.AND)
        if spc_code:
            query.add(Q(spc_code__icontains=spc_code), Q.AND)
        if procedure_code:
            query.add(Q(procedure_code__icontains=procedure_code), Q.AND)
        if funding_profile:
            query.add(Q(funding_profile=funding_profile), Q.AND)

        data = Service.objects.filter(query).order_by('name')[:500]
        table_class = 'listview-table'


    else:
        form.fields['active'].initial = True
        data = Service.objects.filter(active=True).order_by('name')[:500]  # make the 200 a setting
        table_class = 'listview-table'


    return render(request, 'config/service_index.html', {
        'title': 'Services',
        'table_class': table_class,
        'form': form,
        'data': data,
    })


@access_control("admin")
def service_edit(request, pk=None):
    app = "Config"
    mod = Service
    txt = "Service"
    frm = ServiceConfigForm

    if pk:
        instance = get_object_or_404(mod, id=pk)
        title = "{}".format(txt)
    else:
        instance = None
        title = "New {}".format(txt)

    if request.method == "GET":
        form = frm(instance=instance)

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"

        form = set_autocomplete(instance, form, "funding_profile")

        return render(request, "config/service_edit.html", {
            'form':form,
            'title':title,
            'popup' : popup,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance)

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            return save_confirm(request, save_audit(request, form))
        else:
            return format_errors(request, form=form)





@access_control("admin")
def diagnosis_index(request):
    form = DiagnosisFilterForm()

    if request.method == "POST":
        active = request.POST.get('active')
        code = request.POST.get('code')
        name = request.POST.get('name')

        form.fields['active'].initial = active
        form.fields['code'].initial = code
        form.fields['name'].initial = name


        query = Q()
        if active != '':
            query.add(Q(active=active), Q.AND)

        if code:
            query.add(Q(code__icontains=code), Q.AND)
        if name:
            query.add(Q(name__icontains=name), Q.AND)


        data = Diagnosis.objects.filter(query).order_by('code')[:500]
        table_class = 'listview-table-no-order-no-search'

    elif request.method == "GET":
        form.fields['active'].initial = True
        data = Diagnosis.objects.filter(active=True).order_by('code')[:500]  # todo make this 500 a setting
        table_class = 'listview-table'

    return render(request, 'config/diagnosis_index.html', {
        'title': 'Diagnosis',
        'form': form,
        'data': data,
        'table_class': table_class,
    })


@access_control("admin")
def diagnosis_edit(request, pk=None):
    app = "config"
    mod = Diagnosis
    txt = "Diagnosis"
    frm = DiagnosisConfigForm

    if pk:
        instance = get_object_or_404(mod, id=pk)
        title = "Diagnosis"
    else:
        instance = None
        title = "New Diagnosis"

    if request.method == "GET":
        form = frm(instance=instance)

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"


        return render(request, f"{app}/{txt.lower()}_edit.html", {
            'form': form,
            'title': title,
            'popup': popup,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance)

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            return save_confirm(request, save_audit(request, form))

        else:
            return format_errors(request, form=form)






@access_control("admin")
def app_config_index(request):
    app_config_data = AppConfig.objects.all().order_by('key')

    return render(request, 'config/app_config_index.html', {
        'title': 'Config',
        'app_config_data': app_config_data,
        'table_class': 'listview-table',
        "users": get_authorized_users(),
        "year_open": get_app_config("fiscal_year_open_from"),
        "period_open": get_app_config("fiscal_period_open_from"),
    })


@access_control("admin")
def app_config_edit(request, pk=None):
    mod = AppConfig
    frm = AppConfigForm

    if pk:
        instance = get_object_or_404(mod, id=pk)
        title = "Setting"
    else:
        instance = None
        title = "New Setting"

    if request.method == "GET":
        form = frm(instance=instance)

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"


        return render(request, 'config/app_config_edit.html', {
            'form': form,
            'title': title,
            'popup': popup,
        })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance)

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            return save_confirm(request, save_audit(request, form))

        else:
            return format_errors(request, form=form)



@access_control("admin")
def change_fiscal(request):
    if request.method == "GET":
        form = UpdateFiscalForm()

        form.fields["fiscal_year_open_from"].initial = get_app_config("fiscal_year_open_from")
        form.fields["fiscal_period_open_from"].initial = get_app_config("fiscal_period_open_from")

        return render(request, 'config/update_fiscal.html', {
            "popup" : "popup",
            'form':form,
            'title':'Change Open Fiscal From',
        })

    elif request.method == "POST":
        form = UpdateFiscalForm(request.POST)
        year = request.POST.get("fiscal_year_open_from")
        period = request.POST.get("fiscal_period_open_from")

        year_config = get_app_config_object("fiscal_year_open_from")
        period_config = get_app_config_object("fiscal_period_open_from")

        year_config.value = year
        period_config.value = period

        save_audit(request, year_config)
        save_audit(request, period_config)

        # Look for any workers that have a fiscal year or period
        # that are prior to these and fix
        concat = int("{}{}".format(year, period))

        for uc in UserConfig.objects.all():
            if uc.fiscal_year is None or uc.fiscal_period is None:
                uc.fiscal_year = year
                uc.fiscal_period = period
                uc.save()
                continue

            compare = int("{}{}".format(uc.fiscal_year, uc.fiscal_period))
            if compare < concat:
                uc.fiscal_year = year
                uc.fiscal_period = period
                uc.save()





        return render(request, 'save_confirm.html', {
            'object': "fiscal open from",
            'popup' : 'popup',
            'redirect' : "/config/app_config/",
        })







@access_control("admin")
def vendor_edit(request, pk=None):
    app = "config"
    mod = Vendor
    txt = "Vendor"
    frm = VendorForm

    if pk:
        instance = get_object_or_404(mod, id=pk)
        title = txt
    else:
        instance = None
        title = "New " + txt

    if request.method == "GET":
        form = frm(instance=instance)
        if instance is None:
            form['active'].initial = True
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"
        else:
            popup = ""

        return render(request, app + '/' + txt.lower() + '_edit.html', {
            'form': form,
            'title': title,
            'popup': popup,
            "instance": instance
            #'last_saved' : last_saved,
        })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance)

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            return save_confirm(request, save_audit(request, form))

        else:
            return format_errors(request, form=form)



@access_control("admin")
def vendor_index(request, pk=None):
    form = VendorFilterForm()

    if request.method == "POST":
        active = request.POST.get('active')
        name = request.POST.get('name')
        vendor_number = request.POST.get('vendor_number')

        query = Q()
        if active != '':
            query.add(Q(active=active), Q.AND)
        if vendor_number:
            query.add(Q(vendor_number=vendor_number), Q.AND)
        if name:
            query.add(Q(name__icontains=name), Q.AND)


        data = Vendor.objects.filter(query).order_by('name')

        form.fields['active'].initial = active
        form.fields['name'].initial = name
        form.fields['vendor_number'].initial = vendor_number
        table_class = 'listview-table'

    else:
        #just show the top 500 records
        #data = Vendor.objects.order_by('-transaction__Date', 'Active', 'Name').values('Active','Name').distinct()[:100]
        form.fields['active'].initial = True
        vendors = Vendor.objects.all().order_by("name")[:200] # TODO make this an setting?

        data = vendors
        table_class = 'listview-table'

    report_id = str(uuid.uuid4())
    report_data = []
    for ven in data:
        report_data.append({
            "active" : ven.active,
            "name" : ven.name,
            "vendor_number" : ven.vendor_number,
            "wisconsin_provider_id" : ven.wisconsin_provider_id,
            "national_provider_id" : ven.national_provider_id,
            "street" : ven.street,
            "suite" : ven.suite,
            "city" : ven.city,
            "state" : ven.state,
            "zipcode" : ven.zipcode,
            "notes" : ven.notes,
        })

    request.session[report_id] = json.dumps(
        report_data,
        cls=DjangoJSONEncoder)


    return render(request, 'config/vendor_index.html', {
        'title' : 'Vendor',
        'form' : form,
        'data' : data,
        'table_class' : table_class,
        'filter' : 'filter',
        "report_id" : report_id,
    })



@access_control()
def modeldata(request, pk=None):
    record = get_object_or_404(AuditTrail, id=pk)
    return HttpResponse(record.model_data)


@access_control("admin")
def base_test(request):
    return render(request, 'base.html')


@access_control("admin")
def audit_trail(request):
    audit_log = AuditTrail.objects.all().order_by('-date_time')[:3000]

    return render(request, 'config/audit.html', {
        'title' : 'Audit Trail',
        'results' : audit_log,
    })






@access_control("admin", "caseworker", "financial", "auditor")
def user_config_edit(request):
    frm = UserConfigForm
    pk = request.user.id

    if pk:
        user_instance = get_object_or_404(User, pk=pk)
        instance = user_instance.userconfig
    else:
        # you can only create new ones for yourself
        try:
            instance = UserConfig.objects.get(user=request.user)
        except UserConfig.DoesNotExist:
            print("NEW USERCONFIG FOR {}".format(request.user.username))
            instance = UserConfig(user=request.user)

    if request.method == "GET":
        form = frm(instance=instance)

        form.fields["fiscal_year"].initial = get_fiscal_year(request)
        form.fields["fiscal_period"].initial = get_fiscal_period(request)


        return render(request, "config/userconfig_edit.html", {
            'form': form,
            'title': request.user.username,
            #'alerts' : get_alerts(),
        })


    elif request.method == "POST":
        if request.POST.get('delete'):  # do not allow deletion
            return HttpResponseRedirect("/config/user_config/")
        if instance.user is None:
            instance.user = request.user

        form = frm(request.POST, instance=instance, request=request)

        if form.is_valid():
            return save_confirm(request, save_audit(request, form))


        else:
            return format_errors(request, form=form)







@access_control()
def lookup_data(request, pk=None):
    s_mod = request.GET.get("mod")
    fields = request.GET.get("fields")
    query = request.GET.get("query")
    app = request.GET.get("app")
    order = request.GET.get("order")
    parent_id = request.GET.get("parent_id")
    parent_app = request.GET.get("parent_app")
    s_parent_mod = request.GET.get("parent_mod")
    path = request.GET.get("path")

    max_results = int(get_app_config("lookup_max_results"))

    include_inactive = False

    if parent_id and parent_app and s_parent_mod:
        parent_mod = django.apps.apps.get_model(parent_app, s_parent_mod)
    else:
        parent_mod = None

    mod = django.apps.apps.get_model(app, s_mod)

    if s_mod is None or query is None or fields is None or app is None or order is None:
        print("Invalid Lookup Request")
        return HttpResponse("Invalid")

    #create dynamic OR query, maybe in the future we add more types of queries
    arr_fields = fields.split(',')
    queryfields = []
    for x in arr_fields:
        queryfields.append(x.strip())

    q_objects = Q()
    for field in queryfields:
        # do some custom stuff here (for now at least)
        # for client, there are just so many people with the same first
        # and last names, that the normal query will never work...

        # TODO: these specialized ones should be brought out of this loop!
        if mod == Client:
            #print("MOD CLIENT")
            words = query.replace(',', '').split(' ')

            if len(words) == 1:
                w1 = words[0].strip()
                #print("WORD LENGTH is ONE:", w1)

                if w1.isdigit():
                    q_objects.add(Q(id=w1), Q.AND)
                else:
                    q_objects.add(Q(last_name__istartswith=w1), Q.AND)

            elif len(words) == 2:
                #print("CLIENT LENGTH IS TWO")
                w1 = words[0].strip()
                w2 = words[1].strip()
                #print("WORDS: {} {}".format(w1, w2))
                # go both ways for now (first, last or last,first)
                # if we get too many people with matches, we can force
                # last,first throughout the whole app.
                q_objects.add((Q(first_name__istartswith=w1) & Q(last_name__istartswith=w2)), Q.OR)
                q_objects.add((Q(first_name__istartswith=w2) & Q(last_name__istartswith=w1)), Q.OR)

        elif mod == Authorization:
            #print("MOD AUTH")
            # Auths need to be looked up by their auth numbers
            # An auth number is really just the year the auth starts
            # and the auth's id. 201925
            words = query.replace(',', '').split(' ')

            # Auth can only be looked up by its id or year + id.
            # Maybe in the future we can add vendor name or something...
            if len(words[0].strip()) >= 5 and words[0].strip().isnumeric():
                q_objects.add((Q(date_from__year=int(words[0].strip()[:4])) & Q(id=int(words[0].strip()[4:].strip()))), Q.OR)



        elif mod == Account and re.match("[0-9]{3}(\\s){0,1}-(\\s){0,1}[0-9]{3}", query):
            # Special lookup per one of the power users.
            # They want a shorthand way of finding accounts for example:
            #   to find  027-20-55411-100
            #   we want to just type in 027-100
            # this is the regex: [0-9]{3}(\s){0,1}-(\s){0,1}[0-9]{3}

            chunks = query.split('-')
            act_start = chunks[0].strip()
            act_end = chunks[1].strip()

            q_objects.add(Q(name__istartswith=act_start), Q.AND)
            q_objects.add(Q(name__iendswith=act_end), Q.AND)

            include_inactive = True


        else:
            #print("Non-specialized query")
            q_objects.add(Q(**{field + "__istartswith": query}), Q.OR)


    if mod == Account:
        include_inactive = True


    # If the object has an active bit, we want to be sure to only active ones
    if include_inactive is not True and hasattr(mod, 'active'):
        q_objects.add(Q(active=True), Q.AND)

    # Add any custom queries here
    if mod == TransactionLot:
        q_objects.add(Q(year=get_fiscal_year(request)), Q.AND)





    # *** PPS Service Filtering ***
    if mod == Service and parent_mod == PpsAodaEpi:
        q_objects.add(Q(pps_aoda=True), Q.AND)
    elif mod == Service and parent_mod == PpsMhEpi:
        q_objects.add(Q(pps_mh=True), Q.AND)
    elif mod == Service and parent_mod == PpsCoreEpi:
        q_objects.add(Q(pps_core=True), Q.AND)
    elif mod == Service and parent_mod == PpsB3Epi:
        q_objects.add((Q(pps_b3=True) | Q(pps_core=True)), Q.AND)


    res = mod.objects.filter(q_objects)[:max_results]
    #print("q_objects:", q_objects)
    #print("QUERY:\n")
    #print(res.query)
    results_list = []
    # sent a c_target (change target) and a c_value (change value) to have another field auto-fill once the lookup is selected

    if mod == Service:
        for r in res:
            #update unit value on transaction to match the service's default unit
            # TODO this only needs to happen sometimes, like on the trasnaction.
            results_list.append({
                "label": r.get_auto_name(),
                "value": r.id,
                "c_target": "#id_units",
                "c_value": r.default_unit
            })

    elif mod == Account:
        for r in res:
            # if the account type is revenue (0), default to credit 0
            # if the account type is expense (1), default to debit 1
            results_list.append({
                "label": r.get_auto_name(),
                "value": r.id,
                "c_target": "#id_trans_type",
                "c_value": r.type
            })


    elif mod == Vendor and "/accounting/authorization" in path:
        # only want to allow selecting of contracts from the same vendor
        # because we're updating a select with a fresh queryset, its a q_value
        # if this becomes a data-hog, then it will have to be moved to a
        # separate javacsript call that happens AFTER the lookup is selected
        for r in res:
            contracts = Contract.objects.filter(vendor=r).order_by("-year", "vendor__name")
            s_values = []
            #print("S_VALUES")
            #print(s_values)
            for con in contracts:
                s_values.append({
                    "id": str(con.id),
                    "text": str(con)
                })

            results_list.append({
                "label": r.get_auto_name(),
                "value": r.id,
                "s_target": "#id_contract",
                "s_value": json.dumps(s_values)
            })


    elif mod == Authorization and "/accounting/transaction" in path:
        # this is a multi field setter. Allows Nova to set values for
        # multiple other fields based off of the selection a user makes
        # from one field.
        # this is actually much simpler and cleaner than the other methods
        # maybe one day switch the others to something more like this.
        # When building the results, just add whatever value you want
        # for whatever ID target you want. Super simple.
        for r in res:

            # build up some targets and values based off of what's selected
            multi_fill = []

            multi_fill.append({
                "target": "#id_vendor",
                "value": r.vendor.id,
            })
            multi_fill.append({
                "target": "#id_vendor_lookup",
                "value": r.vendor.get_auto_name()
            })

            multi_fill.append({
                "target": "#id_service",
                "value": r.service.id,
            })
            multi_fill.append({
                "target": "#id_service_lookup",
                "value": r.service.get_auto_name(),
            })

            if r.account:
                multi_fill.append({
                    "target": "#id_account",
                    "value": r.account.id,
                })
                multi_fill.append({
                    "target": "#id_account_lookup",
                    "value": r.account.get_auto_name(),
                })

                multi_fill.append({
                    "target": "#id_trans_type",
                    "value": r.account.type,
                })


            if r.client:
                multi_fill.append({
                    "target": "#id_client",
                    "value": r.client.id,
                })
                multi_fill.append({
                    "target": "#id_client_lookup",
                    "value": r.client.get_auto_name(),
                })

            if r.unit_type:
                multi_fill.append({
                    "target": "#id_unit_type",
                    "value": r.unit_type,
                })

            if r.target_group:
                multi_fill.append({
                    "target": "#id_target_group",
                    "value": r.target_group,
                })


            results_list.append({
                "label": r.get_auto_name(),
                "value": r.id,
                "multi_fill": multi_fill,
            })



    else:
        for r in res:
            results_list.append({
                "label": r.get_auto_name(),
                "value": r.id,
            })

    if len(results_list) == 0:
        # No results, return something to tell the user that
        results_list.append({
            "label": "No results found",
            "value": None,
        })

    fmt_results = json.dumps(results_list)
    return HttpResponse(fmt_results)






def user_logged_out_signal(sender, **kwargs):

    if "user" in kwargs and kwargs["user"]:
        # Per Chelesa, we want the CC reports to automatically delete when
        # the user logs out
        CCReport.objects.filter(userconfig=kwargs["user"].userconfig).delete()

        # Some users want their saved form data to reset once they log out
        if kwargs["user"].userconfig.clear_form_data:
            UserForm.objects.filter(userconfig=kwargs["user"].userconfig).delete()



user_logged_out.connect(user_logged_out_signal)




