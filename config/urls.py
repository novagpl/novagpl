"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from django.urls import path
from django.views.generic.base import RedirectView, TemplateView

from . import views


app_name = 'config'

urlpatterns = [
    path('', RedirectView.as_view(url='/loginsort/', permanent=False), name='config_root'),

    path('base_test/', views.base_test, name='base_test'),  # to test the base template with no other data
    path('audit_trail/', views.audit_trail, name="audit_trail"),
    path('model_data/<int:pk>/', views.modeldata, name="modeldata"),

    path('vendor/', views.vendor_index, name="vendor_index"),
    path('vendor/<int:pk>/', views.vendor_edit, name="vendor_edit"),
    path('vendor/new/', views.vendor_edit, name="vendor_new"),

    path('app_config/', views.app_config_index, name='app_config_index'),
    path('app_config/<int:pk>/', views.app_config_edit, name='app_config_edit'),
    path('app_config/new/', views.app_config_edit, name='app_config_new'),

    path('diagnosis/', views.diagnosis_index, name='diagnosis_index'),
    path('diagnosis/<int:pk>/', views.diagnosis_edit, name='diagnosis_edit'),
    path('diagnosis/new/', views.diagnosis_edit, name='diagnosis_new'),

    path('service/', views.service_index, name='service_index'),
    path('service/<int:pk>/', views.service_edit, name='service_edit'),
    path('service/new/', views.service_edit, name='service_new'),

    path('employee/', views.employee_index, name='employee_index'),
    path('employee/<int:pk>/', views.employee_edit, name='employee_edit'),
    path('employee/new/', views.employee_edit, name='employee_new'),

    path('employee_comp/<int:pk>/', views.employee_comp_edit, name='employee_comp_edit'),
    path('employee_comp/new/<int:parent_id>/', views.employee_comp_edit, name='employee_comp_new'),

    path('user_config/', views.user_config_edit, name='user_config_edit'),

    path("changelog/", views.changelog, name="changelog"),

    path("change_fiscal/", views.change_fiscal, name="change_fiscal"),

    path("user_restrict/", views.user_restrict_index, name="user_restrict_index"),
    path("restrict_client/", views.restrict_client_edit, name="restrict_client_edit"),
    path("restrict_client/<int:pk>/", views.restrict_client_edit, name="restrict_client_edit"),
    path("restrict_url/", views.restrict_url_edit, name="restrict_url_edit"),
    path("restrict_url/<int:pk>/", views.restrict_url_edit, name="restrict_url_edit"),

    path("user_mgmt/", views.user_mgmt_index, name="user_mgmt_index"),
    path("user_mgmt/<int:user_mgmt_id>/", views.user_mgmt_edit, name="user_mgmt_edit"),
    path("user_mgmt/create_user/<str:email>/<str:full_name>/<str:user_type>/", views.user_mgmt_create_user, name="user_mgmt_create_user"),

    path("auditor_edit/<int:auditor_id>/", views.auditor_edit, name="auditor_edit"),
    path("auditor_client/remove/<int:auditor_id>/<int:client_id>/", views.auditor_client_remove, name="auditor_client_remove"),
    #path("auditor_client_access/<int:auditor_id>/", views.auditor_client_access_edit, name="auditor_client_access_edit"),
    #path("auditor_mgmt/create_user/<str:email>/<str:full_name>/<str:user_type>/", views.auditor_mgmt_create_user, name="auditor_mgmt_create_user"),

    path('lookup/', views.lookup_data, name="lookup"),
    path('popup_lookup/', views.popup_lookup, name="popup_lookup"),
    path('link_pl/', views.link_pl, name="link_pl"),


    path("route/<int:route_id>/", views.route_edit, name="route_edit"),
    path("route/<str:resource_app>/<str:resource_model>/<int:resource_id>/", views.route_edit, name="route_new"),
    path("route/viewall/<str:resource_app>/<str:resource_model>/<int:resource_id>/", views.route_viewall, name="route_viewall"),
    path("supervisor_sign/emr_form/<int:emrform_id>/", views.route_supervisor_sign_emrform, name="route_supervisor_sign"),
    path("ajax_route_complete/<int:route_id>/", views.ajax_route_complete, name="route_complete"),
    path("ajax_route_uncomplete/<int:route_id>/", views.ajax_route_uncomplete, name="route_uncomplete"),

    path("ajax_check_if_exist/<str:model_name>/<str:field>/<str:value>/<int:model_id>", views.check_if_exist, name="check_if_exist"),
    path("ajax_check_if_exist/<str:model_name>/<str:field>/<str:value>/<int:model_id>/", views.check_if_exist, name="check_if_exist"),
    path("ajax_check_if_exist/<str:model_name>/<str:field>/<str:value>/", views.check_if_exist, name="check_if_exist"),
    path("ajax_verify_password/", views.ajax_verify_password, name="ajax_verify_password"),
    path("ajax_get_notifications/", views.ajax_get_notifications, name="ajax_get_notifications"),
    path("ajax_check_new_notifications/", views.ajax_check_new_notifications, name="ajax_check_new_notifications"),
    path("ajax_read_notification/<int:notify_id>/", views.ajax_read_notification, name="ajax_read_notification"),
    path("ajax_initial_value/", views.ajax_initial_value, name="ajax_initial_value"),
    path("ajax_check_warn/", views.ajax_check_warn, name="ajax_check_warn",)

    #path("blank/", TemplateView.as_view(template_name='config/blank.html'))

]
