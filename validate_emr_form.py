"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from django.utils import timezone
from django.conf import settings

from client.models import (
    EmrRandiIntakeForm, EmrRandiReferralForm, EmrRandiExternalReferralForm
)

from utils import get_fiscal_year_and_period, get_app_config



# TODO Think about moving this to the utils module
class CustomError:
    def __init__(self):
        self.errors = {
            "errors": {},
            "values": {},
        }

    def add(self, field, msg, value=None):
        self.errors["values"][field] = value

        if field in self.errors["errors"]:
            self.errors["errors"][field].append(msg)
        else:
            self.errors["errors"][field] = [msg, ]

    def has_errors(self):
        if self.errors["errors"]:
            return True
        else:
            return False




def validate_emr_casenote_form(instance, errors):
    # This function is only called from the validate_emr_form_finalize()
    # function.
    # Depending on the type of episode, different fields will be required.
    #import pdb; pdb.set_trace()




    if instance.emrepisode.episode_type == "b3":
        if instance.place_of_service is None:
            errors.add("place_of_service", "Place of service is required")

        if instance.service_minutes is None:
            errors.add("service_minutes", "Service minutes is required")

        if instance.service is None:
            errors.add("service", "Service is required")


    elif instance.emrepisode.episode_type in ["clts", "clts_preadmit"]:
        if instance.place_of_service is None:
            errors.add("place_of_service", "Place of service is required")

        if instance.service_minutes is None:
            errors.add("service_minutes", "Service minutes is required")

        if instance.service is None:
            errors.add("service", "Service is required")

        if instance.billing_code is None:
            errors.add("billing_code", "Billing code is required")


    elif instance.emrepisode.episode_type == "tcm":
        if instance.place_of_service is None:
            errors.add("place_of_service", "Place of service is required")

        if instance.service_minutes is None:
            errors.add("service_minutes", "Service minutes is required")

        if instance.service is None:
            errors.add("service", "Service is required")

        if instance.billing_code is None:
            errors.add("billing_code", "Billing code is required")


    elif instance.emrepisode.episode_type == "crisis":
        if instance.credentials is None:
            errors.add("credentials", "Credentials is required")

        if instance.crisis_type_of_contact is None:
            errors.add("crisis_type_of_contact", "Type of contact is required")

        if instance.diagnosis is None:
            errors.add("diagnosis_lookup", "Diagnosis is required")

        if instance.place_of_service is None:
            errors.add("place_of_service", "Place of service is required")

        if instance.total_minutes < 1:
            errors.add("total_minutes", "Total minutes must be greater than zero")

        if instance.billing_code is None:
            errors.add("billing_code", "Billing code is required")




    if not instance.note:
        errors.add("note", "Note is required")


    return errors





def validate_emr_crisis_alert_form(instance, errors):
    if not instance.date:
        errors.add("date", "Date is required")

    if not instance.alert_end_date:
        errors.add("alert_end_date", "Alert End Date is required")


    if instance.date and instance.alert_end_date:
        if instance.date > instance.alert_end_date:
            errors.add("alert_end_date", "Alert end date cannot be before the service date")

        if (instance.alert_end_date - instance.date).days > 30:
            errors.add("alert_end_date", "Alert end date cannot be greater than 30 days from the service date")



    return errors





def validate_emr_form_finalize(instance):
    errors = CustomError()

    fy, fp = get_fiscal_year_and_period()
    #fiscal_cutoff = timezone.datetime(fy, fp, 1).date()


    # The users may not want to use PPS forms as time tracking (they use case
    # notes instead) so they want to skip the date validation for their
    # pps forms.
    pps_date_skip = get_app_config("pps_forms_skip_date_validation")

    if pps_date_skip is True and "pps" in instance.emrformtype.get_name():
        print(f"Skipping date validation for '{instance}' because pps_forms_skip_date_validation is set to True")
        pass

    else:
        # Validation for the date
        if instance.date is None:
            errors.add(field="date", msg="Date cannot be blank", value=None)

            '''
            add_custom_error(
                custom_error=custom_error,
                fieldname="date",
                msg="Date cannot be blank",
                value=None,
            )
            '''

        elif instance.date > timezone.now().date():
            errors.add(
                field="date",
                msg="Date cannot be in the future",
                value=instance.date.strftime(settings.DATE_FORMAT))


        #elif instance.date < fiscal_cutoff:
        #    errors.add(
        #        field="date",
        #        msg=f"Service date cannot be earlier than the open fiscal year and period. Service date must be on or after {fiscal_cutoff.strftime('%m/%d/%Y')} ",
        #        value=instance.date.strftime(settings.DATE_FORMAT))



    if instance.emrformtype.get_name() == "case note":
        validate_emr_casenote_form(instance, errors)


    if instance.emrformtype.get_name() == "crisis alert":
        validate_emr_crisis_alert_form(instance, errors)



    if errors.has_errors():
        return errors.errors
    else:
        return None









def validate_emr_form(form):
    # 2023-03-08 This is mostly not being used as EMR forms only get validated
    # when finalizing and they use a different validation technique in
    # validate_emr_form_finalize()

    # At this point (2022-05-19) we want all EMR forms that are being validated
    # to have ALL fields with data.
    # Now (2022-09-20) we have some fields that are conditional. They can go
    # in the exclude_these_fields data and can be validated (or ignored)
    # You can access more info about the form by going to:
    #   form.instance.*
    # Now (2022-10-03) we have some forms that should not be validated at all
    #


    cleaned_data = form.clean()

    # Below here is the code that attempts to validate all fields for blank
    # values. This is really not used that often.
    # Do not need to validate blank fields on R & I forms
    if isinstance(form.instance, EmrRandiIntakeForm):
        return

    if isinstance(form.instance, EmrRandiReferralForm):
        return

    if isinstance(form.instance, EmrRandiExternalReferralForm):
        return

    if form.instance.is_finalized:
        return



    exclude_these_fields = ["linked_docs", ]

    errors = {}

    # Looping through the cleaned data, some fields we don't care if they
    # are blank. Checkboxes for example, so we can skip over those
    for key, value in cleaned_data.items():
        if key in exclude_these_fields:
            continue

        if hasattr(form.fields[key], "widget") and hasattr(form.fields[key].widget, "input_type"):
            #print(f"Input type: {form.fields[key].widget.input_type}")
            if form.fields[key].widget.input_type == "checkbox":
                continue


        # If there's no data for this field, add an error for it
        if not value:
            #import pdb; pdb.set_trace()
            errors[key] = "This field is required"


    for key, value in errors.items():
        form.add_error(key, value)






