"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import client.client_choices as client_choices
import client.pps_choices as pps_choices
import accounting.accounting_choices as accounting_choices
import django.db.utils
from client.models import *  # TODO clean this up
from config.models import UserConfig
from accounting.models import Employee
from django.conf import settings

from django import forms
from django.forms import (BooleanField, CharField, CheckboxInput, ChoiceField,
                          DateField, DateInput, HiddenInput, IntegerField,
                          ModelChoiceField, NumberInput, Select,
                          TextInput, DecimalField)
from utils import render_form, get_app_config

from .models import *  # TODO clean this  up

from client.models import (EmrRandiExternalReferralForm)




NULL_CHOICE = (None, "----------", )








class PpsCstReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super().__init__(*args, **kwargs)

        render_form(self, hide_all_optional=False)

    service_date_from = DateField(
        label='Service date from',
        required=True,
        widget=DateInput(format=settings.DATE_FORMAT, attrs={
            'width': 25,
            'class': 'datepicker'}))

    service_date_to = DateField(
        label='Service date to',
        required=True,
        widget=DateInput(format=settings.DATE_FORMAT, attrs={
            'width': 25,
            'class': 'datepicker'}))

    billing_code_choices = (NULL_CHOICE, ) + accounting_choices.BILLING_CODE
    billing_code = CharField(
        label="Billing code",
        required=False, widget=Select(choices=billing_code_choices, attrs={
            "width": 25, }))


    client = CharField(required=False, widget=HiddenInput())
    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        'width': 50,
        "clear": "left",
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name',
        'order': 'name',
        'target': 'client',
        'autocomplete': 'off',
    }))

    mh_admission = BooleanField(
        label="Mental Health Admission",
        required=False,
        widget=CheckboxInput(attrs={
            "width": 25,
            "clear": "left"
        }))

    mh_discharge = BooleanField(
        label="Mental Health Discharge",
        required=False,
        widget=CheckboxInput(attrs={
            "width": 25,
        }))
















class PpsTcmReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super().__init__(*args, **kwargs)

        render_form(self, hide_all_optional=False)

    service_date_from = DateField(
        label='Service date from',
        required=True,
        widget=DateInput(format=settings.DATE_FORMAT, attrs={
            'width': 25,
            'class': 'datepicker'}))

    service_date_to = DateField(
        label='Service date to',
        required=True,
        widget=DateInput(format=settings.DATE_FORMAT, attrs={
            'width': 25,
            'class': 'datepicker'}))

    billing_code_choices = (NULL_CHOICE, ) + accounting_choices.BILLING_CODE
    billing_code = CharField(
        label="Billing code",
        required=False, widget=Select(choices=billing_code_choices, attrs={
            "width": 25, }))


    client = CharField(required=False, widget=HiddenInput())
    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        'width': 50,
        "clear": "left",
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name',
        'order': 'name',
        'target': 'client',
        'autocomplete': 'off',
    }))

    aoda_admission = BooleanField(
        label="AODA Admission",
        required=False,
        widget=CheckboxInput(attrs={
            "clear": "left",
            "width": 25,
        }))

    aoda_discharge = BooleanField(
        label="AODA Discharge",
        required=False,
        widget=CheckboxInput(attrs={
            "width": 25,
        }))

    mh_admission = BooleanField(
        label="Mental Health Admission",
        required=False,
        widget=CheckboxInput(attrs={
            "width": 25,
        }))

    mh_discharge = BooleanField(
        label="Mental Health Discharge",
        required=False,
        widget=CheckboxInput(attrs={
            "width": 25,
        }))





class PpsCrisisReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super().__init__(*args, **kwargs)


        render_form(self, hide_all_optional=False)


    service_date_from = DateField(
        label='Service date from',
        required=True,
        widget=DateInput(format=settings.DATE_FORMAT, attrs={
            'width': 25,
            'class': 'datepicker'}))

    service_date_to = DateField(
        label='Service date to',
        required=True,
        widget=DateInput(format=settings.DATE_FORMAT, attrs={
            'width': 25,
            'class': 'datepicker'}))

    billing_code_choices = (NULL_CHOICE, ) + accounting_choices.BILLING_CODE
    billing_code = CharField(
        label="Billing code",
        required=False, widget=Select(choices=billing_code_choices, attrs={
            "width": 25, }))


    client = CharField(required=False, widget=HiddenInput())
    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        'width': 50,
        "clear": "left",
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name',
        'order': 'name',
        'target': 'client',
        'autocomplete': 'off',
    }))

    ref_source_choices = (NULL_CHOICE, ) + pps_choices.REF_SOURCE
    ref_source = CharField(
        label="Referral source",
        required=False,
        widget=Select(choices=ref_source_choices, attrs={
            "width": 25,
        }))





class ProgramBillingReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super().__init__(*args, **kwargs)


        render_form(self, hide_all_optional=False)


    service_date_from = DateField(
        label='Service date from',
        required=True,
        widget=DateInput(format=settings.DATE_FORMAT, attrs={
            'width': 25,
            'class': 'datepicker'}))

    service_date_to = DateField(
        label='Service date to',
        required=True,
        widget=DateInput(format=settings.DATE_FORMAT, attrs={
            'width': 25,
            'class': 'datepicker'}))

    billing_code_choices = (NULL_CHOICE, ) + accounting_choices.BILLING_CODE
    billing_code = CharField(
        label="Billing code",
        required=False, widget=Select(choices=billing_code_choices, attrs={
            "width": 25, }))

    episode_type_choices = (NULL_CHOICE, ) + client_choices.EPISODE_TYPE
    episode_type = CharField(
        required=False,
        widget=Select(choices=episode_type_choices, attrs={
            "clear": "left",
            "width": 25,
        }))

    caseworker = ModelChoiceField(
        required=False,
        queryset=UserConfig.objects.filter(user_type__in=["1", "2"]).order_by("full_name").exclude(user__email=settings.DEBUG_DEV_EMAIL),
        widget=Select(attrs={
            "width": 25,
        }))














class TimeToNoteReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super().__init__(*args, **kwargs)

        self.fields["caseworker"].label_from_instance = self.caseworker_label

        render_form(self, hide_all_optional=False)

    def caseworker_label(self, user):
        return user.full_name


    service_date_from = DateField(label='Service date from', required=True, widget=DateInput(format=settings.DATE_FORMAT, attrs={
        'width': 25,
        'class': 'datepicker'}))

    service_date_to = DateField(label='Service date to', required=True, widget=DateInput(format=settings.DATE_FORMAT, attrs={
        'width': 25,
        'class': 'datepicker'}))

    client = CharField(required=False, widget=HiddenInput())
    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        'width': 25,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name',
        'order': 'name',
        'target': 'client',
        'autocomplete': 'off',
    }))

    '''
    # These finalized date filters are not currently in use
    finalized_date_from = DateField(label='Finalized date from', required=False, widget=DateInput(format=settings.DATE_FORMAT, attrs={
        "clear": "left",
        'width': 25,
        'class': 'datepicker'}))

    finalized_date_to = DateField(label='Finalized date to', required=False, widget=DateInput(format=settings.DATE_FORMAT, attrs={
        'width': 25,
        'class': 'datepicker'}))
    '''


    episode_type_choices = ((None, "----------"),) + client_choices.EPISODE_TYPE
    episode_type = CharField(
        required=False,
        widget=Select(choices=episode_type_choices, attrs={
            "clear": "left",
            "width": 25,
        }))


    client = CharField(required=False, widget=HiddenInput())
    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        'width': 25,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name',
        'order': 'name',
        'target': 'client',
        'autocomplete': 'off',
    }))

    caseworker = ModelChoiceField(
        required=False,
        queryset=UserConfig.objects.filter(user_type__in=["1", "2"]).order_by("full_name").exclude(user__email=settings.DEBUG_DEV_EMAIL),
        widget=Select(attrs={
            "clear": "left",
            "width": 25,
        }))






class DocSearchReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super().__init__(*args, **kwargs)

        render_form(self, hide_all_optional=False)


    doc_type_choices = (
        ("all", "All"),
        ("trans_only", "Transaction docs"),
        ("journal_only", "Journal docs")
    )



    date_from = DateField(label='Date from', required=True, widget=DateInput(format=settings.DATE_FORMAT, attrs={
        'width': 25,
        'class': 'datepicker'}))

    date_to = DateField(label='Date to', required=True, widget=DateInput(format=settings.DATE_FORMAT, attrs={
        'width': 25,
        'class': 'datepicker'}))


    doc_type = CharField(
        label="Document type",
        required=False,
        widget=Select(choices=doc_type_choices, attrs={
            "width": 25,
            "clear": "left"}))


    lot = CharField(required=False, widget=HiddenInput())
    lot_lookup = CharField(label="Batch", required=False, widget=TextInput(attrs={
        'width': 25,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'TransactionLot',
        'fields': 'name, description',
        'order': 'name, description',
        'target': 'lot',
        'autocomplete': 'off',
    }))

    filename = CharField(
        label="Filename contains", required=False, widget=TextInput(attrs={
            "width": 50,
            "clear": "left",
        }))






class CrisisDataReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super().__init__(*args, **kwargs)

        render_form(self, hide_all_optional=True)


    ynu_choices = (
        NULL_CHOICE,
        ("yes", "Yes"),
        ("no", "No"),
    )

    contact_date_from = DateField(required=True, widget=DateInput(format=settings.DATE_FORMAT, attrs={
        "width": 20,
        "class": "datepicker", }))

    contact_date_to = DateField(required=True, widget=DateInput(format=settings.DATE_FORMAT, attrs={
        "width": 20,
        "class": "datepicker", }))

    day_choices = (
        NULL_CHOICE,
        (1, "Sunday"),
        (2, "Monday"),
        (3, "Tuesday"),
        (4, "Wednesday"),
        (5, "Thursday"),
        (6, "Friday"),
        (7, "Saturday"),
    )

    contact_day_of_week = CharField(
        required=False,
        widget=Select(choices=day_choices, attrs={
            "width": 20, }))

    type_of_contact = CharField(
        required=False,
        widget=Select(choices=(NULL_CHOICE,) + client_choices.CRISIS_TYPE_OF_CONTACT, attrs={
            "width": 20, }))

    contacts_by_age_choices = (
        NULL_CHOICE,
        ("youth_to_13", "Youth under 14"),
        ("youth_14_to_17", "Youth 14-17"),
        ("youth_under_18", "All youth under 18"),
        ("adult", "Adult"),
        ("unknown", "Unknown"),
    )
    contacts_by_age = CharField(
        required=False,
        widget=Select(choices=contacts_by_age_choices, attrs={
            "width": 20, }))

    client = CharField(required=False, widget=HiddenInput())
    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        'width': 40,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name',
        'order': 'name',
        'target': 'client',
        'autocomplete': 'off',
    }))

    client_birthdate_from = DateField(
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker", }))

    client_birthdate_to = DateField(
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker", }))

    gender = CharField(
        required=False,
        widget=Select(choices=(NULL_CHOICE,) + client_choices.GENDER, attrs={
            "width": 20, }))

    holds_choices = (
        NULL_CHOICE,
        ("5115", "51.15 (Initiated by Jackson County)"),
        ("55_pp", "55 Protective Placements"),
        ("5145_ncap", "51.45 Incap Hold"),
    )

    ed_placements = CharField(
        label="ED placements - name of facility",
        required=False,
        widget=TextInput(attrs={"width": 40, }))


    outcome_of_contact = CharField(
        required=False,
        widget=Select(choices=(NULL_CHOICE, ) + client_choices.CRISIS_OUTCOMES, attrs={
            "width": 40, }))


    # Extra county choices include 'Agency County' and 'Out of Agency County'
    county_name = [x[1] for x in client_choices.WI_COUNTY if x[0] == get_app_config("county_code")][0]

    extra_county_choices = (
        NULL_CHOICE,
        (get_app_config("county_code"), county_name),
        ("non_agency_county", f"Non-{county_name}"),
    )

    county_of_residence = CharField(required=False, widget=Select(
        choices=extra_county_choices + client_choices.WI_COUNTY, attrs={"width": 20}))

    county_of_incident = CharField(required=False, widget=Select(
        choices=extra_county_choices + client_choices.WI_COUNTY, attrs={"width": 20}))

    aoda_involved = CharField(
        required=False,
        label="AODA involved",
        widget=Select(choices=ynu_choices, attrs={
            "width": 20,
        }))

    law_involved = CharField(
        required=False,
        label="Law enforcement involved",
        widget=Select(choices=ynu_choices, attrs={
            "width": 20,
        }))


    law_dept_choices = (
        NULL_CHOICE,
        ("na", "Not applicable"),
        ("jcsd", "Jackson County Sheriff’s Dept."),
        ("brpd", "Black River Falls Police Department"),
        ("hcnpd", "Ho-Chunk Nation Police Department"),
        ("other", "Other"),
    )

    law_dept = CharField(
        required=False,
        label="Law enforcement dept",
        widget=Select(choices=law_dept_choices, attrs={
            "width": 20,
        }))

    part_of_existing = CharField(
        required=False,
        label="Include forms that are part of existing contacts",
        widget=Select(choices=ynu_choices, attrs={"width": 40}))


    # These are the only types of forms that will get queried for the report.
    # There are also 'All Assessments' option
    try:
        # This try/except is here because when adding a new field to the
        # emrformtype model, this function crashes the migration.
        crisis_formtypes = EmrFormType.objects.filter(
            type_crisis=True, active=True, name__in=[
                "NWC Note",
                "County Assessment and Response Plan",
                "NWC Assessment and Response Plan",
                "Out of County Assessment and Response Plan",
            ])

        formtype_choices = [NULL_CHOICE, ["all_assessments", "All Assessments"]]
        for formtype in crisis_formtypes:
            formtype_choices.append([formtype.id, formtype.name])

        #form_type = CharField(
        #    required=False, widget=Select(
        #        choices=formtype_choices, attrs={"width": 20}))
        ft_nwc_note = BooleanField(
            required=False,
            label="NWC Note",
            widget=CheckboxInput(attrs={"width": 50, "clear": "left"}))

        ft_county_rp = BooleanField(
            required=False,
            label="County Assessment and Response Plan",
            widget=CheckboxInput(attrs={"width": 50}))

        ft_nwc_rp = BooleanField(
            required=False,
            label="NWC Assessment and Response Plan",
            widget=CheckboxInput(attrs={"width": 50}))

        ft_ooc_rp = BooleanField(
            required=False,
            label="Out of County Assessment and Response Plan",
            widget=CheckboxInput(attrs={"width": 50}))
    except django.db.utils.ProgrammingError:
        pass


















class ContractReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(ContractReportForm, self).__init__(*args, **kwargs)

        render_form(self)

    year = IntegerField(required=False, widget=NumberInput(attrs={
        "width": 15,
    }))

    # These need to be matched to Contract model
    contract_types = (
        (None, "Any"),
        ("general", "General"),
        ("provider", "Provider"),
    )
    contract_type = CharField(required=False, widget=Select(choices=contract_types, attrs={
        "width": 15,
    }))

    vendor = CharField(required=False, widget=HiddenInput())
    vendor_lookup = CharField(label="Vendor", required=False, widget=TextInput(attrs={
        'width': 40,
        "clear": "left",
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'Vendor',
        'fields': 'name',
        'order': 'name',
        'target': 'vendor',
        'autocomplete': 'off',
    }))

    service = CharField(required=False, widget=HiddenInput())
    service_lookup = CharField(label="Service", required=False, widget=TextInput(attrs={
        'width': 25,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Service',
        'fields': "name, spc_code, procedure_code",
        'order': "name, spc_code, procedure_code",
        'target': 'service',
        'autocomplete': 'off',
    }))






class ClinicPreadmitReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(ClinicPreadmitReportForm, self).__init__(*args, **kwargs)


        render_form(self)


        self.fields["legal_status"].widget.choices = (
            NULL_CHOICE,) + client_choices.RANDI_LEGAL_STAT_CHOICES



    wait_list_date_from = DateField(
        label="Wait List Start From",
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": "auto",
                "class": "datepicker",
            }))

    wait_list_date_to = DateField(
        label="Wait List Start To",
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": "auto",
                "class": "datepicker",
            }))

    legal_status = CharField(required=False, widget=Select(attrs={
        "width": 50,
    }))

    pregnant = BooleanField(required=False, widget=CheckboxInput(attrs={
        "width": 100,
    }))

    iv_drug_use = BooleanField(required=False, label="IV drug use", widget=CheckboxInput(attrs={
        "width": 100,
    }))

    mh = BooleanField(required=False, label="MH", widget=CheckboxInput(attrs={
        "width": 20,
    }))

    aoda = BooleanField(required=False, label="AODA", widget=CheckboxInput(attrs={
        "width": 20,
    }))

    mh_and_aoda = BooleanField(required=False, label="MH and AODA", widget=CheckboxInput(attrs={
        "width": 20,
    }))








class RandiExternalReferralReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(RandiExternalReferralReportForm, self).__init__(*args, **kwargs)

        render_form(self)



        self.fields["referral_source"].widget.choices = (
            NULL_CHOICE,) + EmrRandiExternalReferralForm.ref_source_choices


        self.fields["legal_status"].widget.choices = (
            NULL_CHOICE,) + client_choices.RANDI_LEGAL_STAT_CHOICES



        self.fields["client_gender"].widget.choices = (
            NULL_CHOICE,) + client_choices.GENDER

        self.fields["closing_reason"].widget.choices = (
            NULL_CHOICE, ) + client_choices.EPISODE_CLOSE_REASON

        self.fields["closing_reason2"].widget.choices = (
            NULL_CHOICE, ) + client_choices.EPISODE_CLOSE_REASON

        self.fields["closing_reason3"].widget.choices = (
            NULL_CHOICE, ) + client_choices.EPISODE_CLOSE_REASON






    referral_date_from = DateField(
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }))

    referral_date_to = DateField(
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }))

    referral_source = CharField(required=False, widget=Select(attrs={
        "width": 20,
    }))

    agency_name = CharField(required=False, widget=TextInput(attrs={
        "width": 20
    }))

    legal_status = CharField(required=False, widget=Select(attrs={
        "width": 20,
    }))

    client_birthdate_from = DateField(
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }))

    client_birthdate_to = DateField(
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }))

    client_gender = CharField(required=False, widget=Select(attrs={
        "width": 20,
    }))

    client_has_previous_referrals = BooleanField(required=False, label="Previous R & I Episodes", widget=CheckboxInput(attrs={
        "width": 20,
        "clear": "left",
    }))



    closing_reason = CharField(required=False, widget=Select(attrs={
        "width": 20,
        "clear": "left",
    }))

    closing_reason2 = CharField(label="2nd closing reason", required=False, widget=Select(attrs={
        "width": 20
    }))

    closing_reason3 = CharField(label="3rd closing reason", required=False, widget=Select(attrs={
        "width": 20
    }))


    referral_close_date_from = DateField(
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }))

    referral_close_date_to = DateField(
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }))







class RandiInternalReferralReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(RandiInternalReferralReportForm, self).__init__(*args, **kwargs)

        render_form(self)



        self.fields["legal_status"].widget.choices = (
            NULL_CHOICE,) + client_choices.RANDI_LEGAL_STAT_CHOICES

        self.fields["client_gender"].widget.choices = (
            NULL_CHOICE,) + client_choices.GENDER

        self.fields["closing_reason"].widget.choices = (
            NULL_CHOICE, ) + client_choices.EPISODE_CLOSE_REASON

        self.fields["closing_reason2"].widget.choices = (
            NULL_CHOICE, ) + client_choices.EPISODE_CLOSE_REASON

        self.fields["closing_reason3"].widget.choices = (
            NULL_CHOICE, ) + client_choices.EPISODE_CLOSE_REASON






    referral_date_from = DateField(
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }))

    referral_date_to = DateField(
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }))

    client_birthdate_from = DateField(
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }))

    client_birthdate_to = DateField(
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }))



    legal_status = CharField(required=False, widget=Select(attrs={
        "clear": "left",
        "width": 20,
    }))

    client_gender = CharField(required=False, widget=Select(attrs={
        "width": 20,
    }))



    client_has_previous_referrals = BooleanField(required=False, label="Previous R & I Episodes", widget=CheckboxInput(attrs={
        "width": 20,
        "clear": "left",
    }))


    requested_clts = BooleanField(required=False, label="Requested CLTS", widget=CheckboxInput(attrs={
        "width": 20,
    }))

    requested_clts_preadmit = BooleanField(required=False, label="Requested CLTS Pre-admit", widget=CheckboxInput(attrs={
        "width": 20,
    }))

    requested_crisis = BooleanField(required=False, widget=CheckboxInput(attrs={
        "width": 20,
    }))

    requested_cst = BooleanField(required=False, label='Requested CST', widget=CheckboxInput(attrs={
        "width": 20,
    }))

    requested_randi = BooleanField(required=False, label="Requested R & I", widget=CheckboxInput(attrs={
        "width": 20,
    }))

    requested_mh = BooleanField(required=False, label="Requested MH", widget=CheckboxInput(attrs={
        "width": 20,
    }))

    requested_aoda = BooleanField(required=False, label="Requested AODA", widget=CheckboxInput(attrs={
        "width": 20,
    }))

    requested_dual = BooleanField(required=False, widget=CheckboxInput(attrs={
        "width": 20,
    }))

    closing_reason = CharField(required=False, widget=Select(attrs={
        "width": 20,
        "clear": "left",
    }))

    closing_reason2 = CharField(label="2nd closing reason", required=False, widget=Select(attrs={
        "width": 20
    }))

    closing_reason3 = CharField(label="3rd closing reason", required=False, widget=Select(attrs={
        "width": 20
    }))


    referral_close_date_from = DateField(
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }))

    referral_close_date_to = DateField(
        required=False, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }))











class CurrentRosterReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(CurrentRosterReportForm, self).__init__(*args, **kwargs)

        render_form(self)

        self.fields["episode_type"].widget.choices = ((None, "----------"),) + client_choices.EPISODE_TYPE


    episode_type = CharField(required=False, widget=Select(attrs={
        "width": 50,
    }))




# THIS IS NO LONGER USE AND WAS DISABLED IN REPORT_LIST.PY AND URLS.PY
class DocReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(DocReportForm, self).__init__(*args, **kwargs)
        render_form(self)


    date_from = DateField(
        required=True, 
        label="Episode opened from", 
        widget=DateInput(
            format=settings.DATE_FORMAT, 
            attrs={
                'width': 50,
                "clear": "left",
                'class': 'datepicker',
            }))

    date_to = DateField(
        required=True, 
        label="Episode opened to", 
        widget=DateInput(
            format=settings.DATE_FORMAT, 
            attrs={
                'width': 50,
                'class': 'datepicker'
            }))


    client = CharField(required=False, widget=HiddenInput())

    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        'width': 100,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name',
        'order': 'name',
        'target': 'client',
        'autocomplete': 'off',
    }))








class LengthOfServiceForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(LengthOfServiceForm, self).__init__(*args, **kwargs)
        render_form(self)



    client = CharField(required=True, widget=HiddenInput())

    client_lookup = CharField(label="Client", required=True, widget=TextInput(attrs={
        'width': 100,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name',
        'order': 'name',
        'target': 'client',
        'autocomplete': 'off',
    }))


    date_from = DateField(
        required=True, 
        label="Episode opened from", 
        widget=DateInput(
            format=settings.DATE_FORMAT, 
            attrs={
                'width': 50,
                "clear": "left",
                'class': 'datepicker',
            }))

    date_to = DateField(
        required=True, 
        label="Episode opened to", 
        widget=DateInput(
            format=settings.DATE_FORMAT, 
            attrs={
                'width': 50,
                'class': 'datepicker'
            }))





class DischargeReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(DischargeReportForm, self).__init__(*args, **kwargs)
        render_form(self)

        self.fields["episode_type"].widget.choices = ((None, "----------"), ) + client_choices.EPISODE_TYPE


    episode_type = CharField(required=False, widget=Select(attrs={
        "width": 50,
    }))


    end_date_from = DateField(
        required=True, 
        label="Episode closed from",
        widget=DateInput(
            format=settings.DATE_FORMAT, 
            attrs={
                'width': 50,
                "clear": "left",
                'class': 'datepicker',
            }))

    end_date_to = DateField(
        required=True, 
        label="Episode closed to",
        widget=DateInput(
            format=settings.DATE_FORMAT, 
            attrs={
                'width': 50,
                'class': 'datepicker'
            }))







class AdmissionReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(AdmissionReportForm, self).__init__(*args, **kwargs)
        render_form(self)

        self.fields["episode_type"].widget.choices = ((None, "----------"), ) + client_choices.EPISODE_TYPE


    episode_type = CharField(required=False, widget=Select(attrs={
        "width": 50,
    }))


    date_from = DateField(
        required=True, 
        label="Episode opened from", 
        widget=DateInput(
            format=settings.DATE_FORMAT, 
            attrs={
                'width': 50,
                "clear": "left",
                'class': 'datepicker',
            }))

    date_to = DateField(
        required=True, 
        label="Episode opened to", 
        widget=DateInput(
            format=settings.DATE_FORMAT, 
            attrs={
                'width': 50,
                'class': 'datepicker'
            }))







class EmrCaseNotesForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(EmrCaseNotesForm, self).__init__(*args, **kwargs)
        render_form(self)

        self.fields["episode_type"].widget.choices = ((None, "----------"), ) + client_choices.EPISODE_TYPE
        self.fields["crisis_toc"].widget.choices = ((None, "----------"), ) + client_choices.CRISIS_TYPE_OF_CONTACT

        note_status = (
            ("any", "Any"),
            ("open", "Open"),
            ("closed", "Closed"),
        )

        self.fields["note_status"].widget.choices = note_status


    date_from = DateField(required=True, widget=DateInput(
        format=settings.DATE_FORMAT, attrs={
            'width': "auto",
            'class': 'datepicker'
        }))

    date_to = DateField(required=True, widget=DateInput(
        format=settings.DATE_FORMAT, attrs={
            'width': "auto",
            'class': 'datepicker'
        }))


    note_status = CharField(required=True, widget=Select(attrs={
        "clear": "left",
        "width": 25,
    }))

    episode_type = CharField(required=False, widget=Select(attrs={
        "width": 25,
    }))

    #
    #contact_type = CharField(required=False, widget=Select(attrs={
    #    "width": 50,
    #}))
    ct_collateral = BooleanField(
        required=False,
        label="Collateral",
        widget=CheckboxInput(attrs={"clear": "left", "width": 25}))

    ct_direct = BooleanField(
        required=False,
        label="Direct",
        widget=CheckboxInput(attrs={"width": 25}))

    ct_face = BooleanField(
        required=False,
        label="Face to face",
        widget=CheckboxInput(attrs={"width": 25}))

    ct_home = BooleanField(
        required=False,
        label="Home visit",
        widget=CheckboxInput(attrs={"width": 25}))

    ct_record = BooleanField(
        required=False,
        label="Record keeping",
        widget=CheckboxInput(attrs={"width": 25}))

    ct_other = BooleanField(
        required=False,
        label="Other/unknown",
        widget=CheckboxInput(attrs={"width": 25}))

    crisis_toc = CharField(
        required=False,
        label="Crisis type of contact",
        widget=Select(attrs={"width": 25}))



    client = CharField(required=False, widget=HiddenInput())

    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        'width': 33,
        "clear": "left",
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name',
        'order': 'name',
        'target': 'client',
        'autocomplete': 'off',
    }))



    staff = ModelChoiceField(
        required=False, 
        queryset=UserConfig.objects.filter(user_type__in=["1", "2"]), 
        widget=Select(attrs={"width": 33}),
    )

    billable_choices = (
        (None, "Any"),
        ("billable", "Billable"),
        ("unbillable", "Unbillable"),
    )
    billable = CharField(
        required=False,
        widget=Select(
            choices=billable_choices,
            attrs={
                "width": 33,
            }))





class CaseNotesForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(CaseNotesForm, self).__init__(*args, **kwargs)
        render_form(self)


    client = CharField(required=False, widget=HiddenInput())


    caseworker = ModelChoiceField(
        required=False,
        queryset=UserConfig.objects.filter(
            user_type__in=["1", "2"]).order_by("full_name").exclude(user__email=settings.DEBUG_DEV_EMAIL),
        widget=Select(attrs={
            "width": 100,
        }))

    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        'width': 100,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name',
        'order': 'name',
        'target': 'client',
        'autocomplete': 'off',
    }))

    date_from = DateField(required=True, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                'width': 50,
                'class': 'datepicker'
            }))

    date_to = DateField(required=True, widget=DateInput(
            format=settings.DATE_FORMAT, attrs={
                'width': 50,
                'class': 'datepicker'
            }))





class ArCountyForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(ArCountyForm, self).__init__(*args, **kwargs)
        render_form(self)



    lot = CharField(required=True, widget=HiddenInput())
    lot_lookup = CharField(label="Batch", required=True, widget=TextInput(attrs={
        'width' : 50,
        'class' : 'lookup',
        'app' : 'accounting',
        'mod' : 'TransactionLot',
        'fields' : 'name, description',
        'order' : 'name, description',
        'target' : 'lot',
        'autocomplete' : 'off',
    }))

    show_fund_totals = BooleanField(label="Show Fund Totals", required=False, widget=CheckboxInput(attrs={
        "width" : 50,
        "clear" : "left",
        }))




class Dcs942Form(forms.Form):
    def __init__(self, *args, **kwargs):
        super(Dcs942Form, self).__init__(*args, **kwargs)
        render_form(self)

    fiscal_year_from = IntegerField(required=True, widget=NumberInput(attrs={"width":50}))
    fiscal_year_to = IntegerField(required=True, widget=NumberInput(attrs={"width":50}))
    fiscal_period_from = IntegerField(required=True, widget=NumberInput(attrs={"width":50, "clear" : "left"}))
    fiscal_period_to = IntegerField(required=True, widget=NumberInput(attrs={"width":50}))



class BudgetReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(BudgetReportForm, self).__init__(*args, **kwargs)
        render_form(self)

    year = CharField(label='Year', required=True, widget=NumberInput(attrs={
        'width': 15,
    }))

    period = CharField(label="Through period", required=False, widget=NumberInput(attrs={
        "width": 20,
    }))







class TransactionReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(TransactionReportForm, self).__init__(*args, **kwargs)
        render_form(self, False)

        group_choices = (
            ('account', 'Account'),
            ('client', 'Client'),
            ('vendor', 'Vendor'),
        )

        order_choices = (
            ('case_note__service__spc_code', 'SPC'),
            ('client__last_name', 'Client'),
            ('vendor__name', 'Vendor'),
            ("service_date", "Service Date"),
            ("fiscal_period", "Fiscal Period"),
        )

        journal_choices = (
            ("all", "All"),
            ("journaled", "Journaled"),
            ("unjournaled", "Unjournaled"),
        )

        self.fields['order_by'].widget.choices = order_choices
        self.fields['group_by'].widget.choices = group_choices
        self.fields["journal_option"].widget.choices = journal_choices

    account = CharField(required=False, widget=HiddenInput())
    client = CharField(required=False, widget=HiddenInput())
    vendor = CharField(required=False, widget=HiddenInput())
    service = CharField(required=False, widget=HiddenInput())
    funding_profile = CharField(required=False, widget=HiddenInput())
    lot = CharField(required=False, widget=HiddenInput())

    fiscal_year_from = IntegerField(required=False, widget=NumberInput(attrs={"width": 25}))
    fiscal_year_to = IntegerField(required=False, widget=NumberInput(attrs={"width": 25}))
    fiscal_period_from = IntegerField(required=False, widget=NumberInput(attrs={"width": 25}))
    fiscal_period_to = IntegerField(required=False, widget=NumberInput(attrs={"width": 25}))

    date_from = DateField(label='Service date from', required=False, widget=DateInput(format=settings.DATE_FORMAT, attrs={
        'width': 25,
        'class': 'datepicker'}))

    date_to = DateField(label='Service date to', required=False, widget=DateInput(format=settings.DATE_FORMAT, attrs={
        'width': 25,
        'class': 'datepicker'}))

    place_of_service = ChoiceField(
        required=False, widget=Select(attrs={"width": 25, "clear": "left"}))

    billing_code = ChoiceField(
        required=False, widget=Select(attrs={"width": 25}))

    billing_code_startswith = CharField(
        label="Billing code starts with", required=False, widget=TextInput(attrs={
            "width": 25
        }))

    employee = ModelChoiceField(
        required=False, 
        queryset=Employee.objects.filter(
            active=True, 
        ).order_by("last_name", "first_name"), 
        widget=Select(attrs={"width": 25})
    )


    account_lookup = CharField(label="Account", required=False, widget=TextInput(attrs={
        'width': 50,
        'clear': 'left',
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'Account',
        'fields': 'name, description ,county_name, county_description',
        'order': 'name',
        'target': 'account',
        'autocomplete': 'off',
    }))

    account_type = ChoiceField(label="Account type", required=False, widget=Select(attrs={
        "width": 25,
    }))

    amount = DecimalField(required=False, widget=NumberInput(attrs={"width": 25}))


    act_county = CharField(label="County", required=False, widget=TextInput(attrs={
        "width": 10,
        "clear": "left",
    }))

    act_dept = CharField(label="Dept", required=False, widget=TextInput(attrs={
        "width": 10,
    }))

    act_program = CharField(label="Program", required=False, widget=TextInput(attrs={
        "width": 10,
    }))

    act_object = CharField(label="Object", required=False, widget=TextInput(attrs={
        "width": 10,
    }))

    act_sub = CharField(label="Sub", required=False, widget=TextInput(attrs={
        "width": 10,
    }))

    # TODO use the normal choices from accounting_choices
    # and put this in the view
    account_type.choices = (
        (None, "---------"), 
        ('0', 'Revenue'),
        ('1', 'Expense'),
    )



    lot_lookup = CharField(label="Batch", required=False, widget=TextInput(attrs={
        'width': 25,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'TransactionLot',
        'fields': 'name, description',
        'order': 'name, description',
        'target': 'lot',
        'autocomplete': 'off',
    }))

    invoice = CharField(required=False, widget=TextInput(attrs={
        "width": 25,
    }))



    service_lookup = CharField(label="Service", required=False, widget=TextInput(attrs={
        'width': 25,
        "clear": "left",
        'class': 'lookup',
        'app': 'client',
        'mod': 'Service',
        'fields': "name, spc_code, procedure_code",
        'order': "name, spc_code, procedure_code",
        'target': 'service',
        'autocomplete': 'off',
    }))

    funding_profile_lookup = CharField(label="Funding profile", required=False, widget=TextInput(attrs={
        'width': 25,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'FundingProfile',
        'fields': 'name, description',
        'order': 'name',
        'target': 'funding_profile',
        'autocomplete': 'off',
    }))

    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        'width': 50,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name',
        'order': 'name',
        'target': 'client',
        'autocomplete': 'off',
    }))

    vendor_lookup = CharField(label="Vendor", required=False, widget=TextInput(attrs={
        'width': 50,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'Vendor',
        'fields': 'name',
        'order': 'name',
        'target': 'vendor',
        'autocomplete': 'off',
    }))

    caseworker = ModelChoiceField(
        label="Caseworker", required=False, widget=Select(attrs={"width": 50}),
        queryset=UserConfig.objects.filter(active=True, user__is_staff=False).order_by("full_name")
    )

    group_by = ChoiceField(label='Group by', required=False, widget=Select(attrs={'width': 33, "clear": "left"}))

    order_by = ChoiceField(label='Order by', required=False, widget=Select(attrs={'width': 33}))

    journal_option = ChoiceField(
        label="Journaled", widget=Select(attrs={"width": 33}))

    only_casenote = BooleanField(label="Case note", required=False, widget=CheckboxInput(attrs={"width": 25}))

    atp_only = BooleanField(required=False, label="ATP", widget=CheckboxInput(attrs={"width": 25}))

    only_batch = BooleanField(
        label="Batched",
        required=False,
        widget=CheckboxInput(attrs={
            "width": 25,
        }) 
    )

    

    hide_client = BooleanField(
        required=False, widget=CheckboxInput(attrs={"width": 25}))

    hide_vendor = BooleanField(
        required=False, widget=CheckboxInput(attrs={"width": 25}))


    show_billing_code = BooleanField(
        required=False, widget=CheckboxInput(attrs={"width": 25}))

    show_cfda_number = BooleanField(
        required=False, 
        widget=CheckboxInput(attrs={"width": 25}),
        label="Show CFDA number",
    )

    show_target_group = BooleanField(
        required=False, widget=CheckboxInput(attrs={"width": 25})
    )

    show_diagnosis_codes = BooleanField(
        required=False, widget=CheckboxInput(attrs={"width": 25})
    )

    show_place_of_service = BooleanField(
        required=False, widget=CheckboxInput(attrs={"width": 25})
    )

    show_type_of_contact = BooleanField(
        required=False, widget=CheckboxInput(attrs={"width": 25})
    )







class AccountTransactionForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(AccountTransactionForm, self).__init__(*args, **kwargs)
        render_form(self)

    account = CharField(required=True, widget=HiddenInput())
    date_from = DateField(label='Date trom', required=False, widget=DateInput(format=settings.DATE_FORMAT, attrs={'width':25, 'class':'datepicker'}))
    date_to = DateField(label='Date to', required=True, widget=DateInput(format=settings.DATE_FORMAT, attrs={'width':25, 'class':'datepicker',}))

    account_lookup = CharField(label="Account", required=True, widget=TextInput(attrs={
        'width':50,
        'clear':'left',
        'class' : 'lookup',
        'app' : 'accounting',
        'mod' : 'Account',
        'fields' : 'name, description, county_name, county_description',
        'order' : 'name',
        'target' : 'account',
        'autocomplete' : 'off',
        }))




class TrialBalanceForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(TrialBalanceForm, self).__init__(*args, **kwargs)
        render_form(self)
    
    fiscal_year_from = IntegerField(
        required=True, widget=NumberInput(attrs={"width":25}))

    fiscal_year_to = IntegerField(    
        required=True, widget=NumberInput(attrs={"width":25}))   
        

    fiscal_period_from = IntegerField(
        required=True, widget=NumberInput(attrs={"width":25, "clear" : "left"}))
    
    
    fiscal_period_to = IntegerField(
        required=True, widget=NumberInput(attrs={"width":25}))






class ClientListForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(ClientListForm, self).__init__(*args, **kwargs)
        render_form(self)

    ch_gender = list(client_choices.GENDER)
    ch_gender.append(('', 'Any'))

    client_active = BooleanField(initial=True, label="Active", required=False, widget=CheckboxInput(attrs={

    }))
    client_first = CharField(required=False, label="First name", max_length=100, widget=TextInput(attrs={
        'width':50,
        'clear' : 'left',
    }))
    client_last = CharField(required=False, label="Last name", max_length=100, widget=TextInput(attrs={
        'width':50,
    }))
    client_birthdate_from = DateField(required=False, label="DOB from", widget=DateInput(format=settings.DATE_FORMAT, attrs={
        'width':50,
        'class':'datepicker',
    }))
    client_birthdate_to = DateField(required=False, label="DOB to", widget=DateInput(format=settings.DATE_FORMAT, attrs={
        'width':50,
        'class':'datepicker',
    }))
    client_gender = ChoiceField(required=False, label="Gender", choices=tuple(ch_gender), widget=Select(attrs={
        'width':50,
        'clear':'both',
    }))



class ClientStatementForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(ClientStatementForm, self).__init__(*args, **kwargs)
        render_form(self)

    client = CharField(required=False, widget=HiddenInput())

    fiscal_year = IntegerField(required=True, label="Fiscal Year", widget=NumberInput(attrs={
        'width':50,
    }))

    fiscal_period = IntegerField(required=True, label="Fiscal Period", widget=NumberInput(attrs={
        'width':50,
    }))

    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        'width':100,
        'class' : 'lookup',
        'app' : 'client',
        'mod' : 'Client',
        'fields' : 'last_name, first_name',
        'order' : 'name',
        'target' : 'client',
        'autocomplete' : 'off',
        }))

    #show_zero_balances = BooleanField(required=False, widget=CheckboxInput(attrs={
    #    "width" : 100,
    #    }))



class FundingProfileReportForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(FundingProfileReportForm, self).__init__(*args, **kwargs)
        render_form(self)

    fiscal_year = IntegerField(
        required=True,
        label="Fiscal year",
        widget=NumberInput(attrs={
            'width': 50,
        })
    )

    fiscal_period = IntegerField(
        required=True,
        label="Through period",
        widget=NumberInput(attrs={
            'width': 50,
        })
    )

    hide_zeroes = BooleanField(
        label="Hide accounts with totals of zero",
        required=False,
        widget=CheckboxInput(attrs={
            "width": 50,
        })
    )

    hide_inactive = BooleanField(
        label="Hide inactive accounts",
        required=False,
        widget=CheckboxInput(attrs={
            "width": 50,
        })
    )




class CaseNoteTimeForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(CaseNoteTimeForm, self).__init__(*args, **kwargs)
        render_form(self)

    date = DateField(required=True, label="Date", widget=DateInput(format=settings.DATE_FORMAT, attrs={
        'width': "auto",
        'class': 'datepicker',
    }))


    show_inactive = BooleanField(required=False, widget=CheckboxInput(attrs={
        "width": "auto",
    }))



class EmrCaseNoteTimeForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(EmrCaseNoteTimeForm, self).__init__(*args, **kwargs)

        billable_choices = (
            (None, "Any"),
            ("billable", "Billable"),
            ("unbillable", "Unbillable"),
        )

        self.fields["billable"].widget.choices = billable_choices

        render_form(self)


    date_from = DateField(required=True, label="Date from", widget=DateInput(format=settings.DATE_FORMAT, attrs={
        'width': 50,
        'class': 'datepicker',
    }))

    date_to = DateField(required=False, label="Date to", widget=DateInput(format=settings.DATE_FORMAT, attrs={
        'width': 50,
        'class': 'datepicker',
    }))

    client = CharField(required=False, widget=HiddenInput())

    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        "width": 100,
        "class": "lookup",
        "app": "client",
        "mod": "Client",
        "fields": "last_name, first_name",
        "order": "name",
        "target": "client",
        "autocomplete": "off",
    }))



    billable = CharField(required=False, widget=Select(attrs={
        "width": 50,
    }))


    


    #show_inactive = BooleanField(required=False, widget=CheckboxInput(attrs={
    #    "width": "auto",
    #}))






