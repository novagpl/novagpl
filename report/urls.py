"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from django.views.generic.base import RedirectView
from django.urls import path

from . import views

app_name = 'report'

urlpatterns = [
    path('', views.report_index, name='report_index'),

    # Accounting reports
    path('account_transaction/', views.account_transaction, name='account_transaction'),
    path("ar_ap_county/", views.ar_ap_county, name="ar_ap_county"),
    path("budget/", views.budget_report, name="budget_report"),
    path("dcs_942/", views.dcs_942, name="dcs_942"),
    path('funding_profile/', views.funding_profile_report, name='funding_profile_report'),
    path('transaction/', views.transaction_report, name='transaction_report'),
    path('trial_balance/', views.trial_balance, name='trial_balance'),
    path("doc_search/", views.doc_search, name="doc_search"),

    # Program reports
    path("admission_report/", views.admission_report, name="admission_report"),    
    path("casenote_time/", views.casenote_time, name="casenote_time"),
    path("case_notes/", views.case_notes, name="case_notes"),
    path('client_list/', views.client_list, name='client_list'),
    path("client_statement/", views.client_statement, name="client_statement"), 
    path("current_roster_report/", views.current_roster_report, name="current_roster_report"),   
    path("discharge_report/", views.discharge_report, name="discharge_report"), 
    path("emr_case_note_report/", views.emr_case_note_report, name="emr_case_note_report"),
    path("emr_case_note_report/<int:pk>/", views.emr_case_note_report, name="emr_case_note_report_single"),
    path("emr_case_note_report_my_open/", views.emr_case_note_report_my_open, name="emr_case_note_report_my_open"),
    path("emr_case_note_time/", views.emr_case_note_time, name="emr_case_note_time_report"),
    path("length_of_service_report/", views.length_of_service_report, name="length_of_service_report"),
    # Users say they don't need the doc report, so just have it redirect to
    # the main report view
    #path("doc_report/", views.doc_report, name="doc_report"),
    #path("doc_report/", RedirectView.as_view(url="/report"), name="doc_report"),
    path("pps_crisis_report/", views.pps_crisis_report, name="pps_crisis_report"),
    path("pps_tcm_report/", views.pps_tcm_report, name="pps_tcm_report"),
    path("pps_cst_report/", views.pps_cst_report, name="pps_cst_report"),
    path("program_billing_report/", views.program_billing_report, name="program_billing_report"),
    path("randi_internal_referral_report/", views.randi_internal_referral_report, name="randi_internal_referral_report"),
    path("randi_external_referral_report/", views.randi_external_referral_report, name="randi_external_referral_report"),
    path("clinic_preadmit_report/", views.clinic_preadmit_report, name="clinic_preadmit_report"),
    path("contract_report/", views.contract_report, name="contract_report"),
    path("crisis_data_report/", views.crisis_data_report, name="crisis_data_report"),
    path("time_to_note_report/", views.time_to_note_report, name="time_to_note_report"),

    # This one doesn't need to be registered in report_list.py as it's run
    # right from the emr_form views
    path("emr_form/<int:emr_form_id>/", views.emr_form_report, name="emr_form_report"),

    # *** DO NOT FORGET TO REGISTER YOUR REPORT IN report_list.py! ***

    path("download_as_csv/", views.download_as_csv, name="download_as_csv"),
    path("download_as_pdf/", views.download_as_pdf, name="download_as_pdf"),
    path("cc_report/watcher/", views.cc_report_watcher, name="cc_report_watcher"),
    path("cc_report/watcher_ajax/", views.cc_report_watcher_ajax, name="cc_report_watcher_ajax"),
    path("cc_report/complete/<str:filename>/", views.cc_report, name="cc_report"),
    path("cc_report/clear/", views.cc_report_clear, name="cc_report_clear"),


]

'''
    url(r'^client_list$', views.client_list_view, name='client_list_view'),
    url(r'^TrialBalance$', views.trial_balance_view, name='trial_balance_view'),
    url(r'^AccountTransaction$', views.account_transaction_view, name='account_transaction_view'),
    url(r'^Transaction$', views.transaction_report_view, name='transaction_report_view'),
'''
