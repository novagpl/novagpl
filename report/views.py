"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import io
import calendar
import csv
import datetime as dt
import json
import math
import uuid
from bs4 import BeautifulSoup
import pdfkit


from decimal import Decimal, getcontext
getcontext().prec = 28

import report_list



from annoying.functions import get_object_or_None
from dateutil.relativedelta import relativedelta
from django.conf import settings
from django.contrib.auth.models import User
from django.core.serializers.json import DjangoJSONEncoder
from django.core.paginator import Paginator
from django.db.models import Q, Sum
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.forms.models import model_to_dict
from django.utils import timezone
from report.models import CCReport
from utils import (create_cc_report, generate_csv_from_json,
                   get_account_balance_date, get_account_balance_fiscal,
                   get_account_balance_up_to_fiscal, get_agency_address,
                   get_app_config, get_cc_report_assets,
                   get_client_bills_or_payments, get_fiscal_period,
                   get_fiscal_year, get_logged_in, access_control, parse_date,
                   save_cc_report_files, set_autocomplete, get_emrform_instance,
                   get_pdf_report_assets,
                   )

from .forms import (AccountTransactionForm, BudgetReportForm, CaseNoteTimeForm,
                    ClientListForm, ClientStatementForm, Dcs942Form,
                    FundingProfileReportForm, TransactionReportForm,
                    TrialBalanceForm, ArCountyForm, CaseNotesForm, 
                    EmrCaseNotesForm, EmrCaseNoteTimeForm,
                    AdmissionReportForm, DischargeReportForm,
                    LengthOfServiceForm, CurrentRosterReportForm,
                    RandiInternalReferralReportForm,
                    RandiExternalReferralReportForm, ClinicPreadmitReportForm,
                    ContractReportForm, CrisisDataReportForm, DocSearchReportForm,
                    TimeToNoteReportForm, ProgramBillingReportForm,
                    PpsCrisisReportForm, PpsTcmReportForm, PpsCstReportForm)

from validation import validate_report_fiscal, validate_crisis_data_report

import client.client_choices as client_choices
import accounting.accounting_choices as accounting_choices
from accounting.models import (Account, BudgetItem, FundingProfile,
                               Transaction, TransactionLot, Vendor, Employee,
                               Contract, TransactionFile, JournalFile, )
from client.models import (
    CaseNote, Client, Service, EmrEpisode, EmrForm,
    EmrRandiReferralForm, EmrRandiExternalReferralForm,
    EmrRandiEpisode, EmrClinicWaitlistForm, EmrClinicPreadmitEpisode,
    EmrCaseNoteForm,
)
from config.models import UserConfig, ReportView





@access_control("admin", "financial", "caseworker")
def cc_report_clear(request):
    # Clears out any pending or completed cc reports for this user and
    # sends them back to the report watcher view
    CCReport.objects.filter(userconfig=request.user.userconfig).delete()


    return HttpResponseRedirect("/report/cc_report/watcher/")


@access_control("admin", "financial", "caseworker")
def report_index(request):
    financial_reports = [
        x for x 
        in report_list.reports 
        if x.report_type == "financial"
    ]

    program_reports = [
        x for x 
        in report_list.reports 
        if x.report_type == "program"
    ]

    my_top = ReportView.objects.filter(
        userconfig=request.user.userconfig).order_by("-count", "-last_access")[:10]

    my_recent = ReportView.objects.filter(
        userconfig=request.user.userconfig).order_by("-last_access", "-count")[:10]


    return render(request, "report/report_index.html", {
        "title": "Report Index",
        "financial_reports": financial_reports,
        "program_reports": program_reports,
        "my_top": my_top,
        "my_recent": my_recent,
    })

    '''
    data = []

    data.append([reverse('report:client_list'), 'Clients',
                 'Allows seraching and filtering all clients within NOVA.',

                 'active, first name, last name, birthdate from, birthdate to, gender.'
                 ])

    data.append([reverse('report:trial_balance'), 'Trial Balance',
                 'The Trial Balance report lists all accounts in the system and '
                 'their debits and credits for the specified timeframe. '
                 'Clicking on an account in the report will take you to the '
                 'Account Transactions report for that account.',

                 'date from, date to.'
                 ])

    data.append([reverse('report:transaction_report'), 'Transaction Report',
                 'The Transaction report provides multiple ways to filter and sort transactions.',

                 'date from, date to, account, funding profile, client, vendor, group by, order by.'
                 ])


    data.append([reverse('report:account_transaction'), 'Account Transaction Report',
                 'Provides a list of transactions by account and timeframe. ',
                 'Clicking on the description field will give you a popup to edit the transaction.',

                 'date from, date to, account.'
                 ])

    return render(request, 'app_index.html', {
        'title' : 'Report Index',
        'data' : data,
    })
    '''








@access_control("admin", "financial", "caseworker")
def pps_cst_report(request):
    # This is used by the template to show all the 4 types of CST forms
    # only set to true when the user selects none of the form type filters
    show_all_formtypes = False

    if request.method == "GET":
        form = PpsCstReportForm(request=request)

        return render(request, "report/pps_cst_report.html", {
            "title": "PPS CST Report",
            "form": form,
        })

    else:
        form = PpsCstReportForm(request.POST, request=request)

        service_date_from = request.POST.get("service_date_from") or timezone.now().date()
        service_date_to = request.POST.get("service_date_to") or timezone.now().date()
        billing_code = request.POST.get("billing_code")
        client_id = request.POST.get("client")
        episode_status = request.POST.get("episode_status")
        mh_admission = bool(request.POST.get("mh_admission") == 'on')
        mh_discharge = bool(request.POST.get("mh_discharge") == 'on')

        query = Q()
        formtypes_query = Q()
        #formtypes_query.add(Q(emrcasenoteform__isnull=False), Q.OR)

        query.add(Q(is_finalized=True), Q.AND)

        if mh_admission:
            formtypes_query.add(Q(emrcstppsmhadmissionform__isnull=False), Q.OR)
        if mh_discharge:
            formtypes_query.add(Q(emrcstppsmhdischargeform__isnull=False), Q.OR)
        if not mh_admission and not mh_discharge:
            formtypes_query.add(Q(emrcstppsmhadmissionform__isnull=False), Q.OR)
            formtypes_query.add(Q(emrcstppsmhdischargeform__isnull=False), Q.OR)
            show_all_formtypes = True

        query.add(formtypes_query, Q.AND)

        query.add(Q(emrepisode__episode_type="cst"), Q.AND)
        query.add(Q(date__gte=service_date_from), Q.AND)
        query.add(Q(date__lte=service_date_to), Q.AND)


        if billing_code:
            query.add(Q(billing_code=billing_code), Q.AND)

        if client_id:
            query.add(Q(emrepisode__client__id=client_id), Q.AND)

        if episode_status == "discharged":
            query.add(Q(emrepisode__end__isnull=False), Q.AND)

        if episode_status == "open":
            query.add(Q(emrepisode__end__isnull=True), Q.AND)


        # The results on this report are grouped by episode and the episodes
        # should be ordered by date
        results = {}
        emrforms = EmrForm.objects.filter(query).order_by(
            "emrepisode__client__last_name",
            "emrepisode__client__first_name",
            "emrepisode__client__birthdate",
            "date",
            "emrformtype__name",
            "billing_code")


        for emrform in emrforms:
            if emrform.emrepisode not in results:
                results[emrform.emrepisode] = {
                    "episode": emrform.emrepisode,
                    "forms": [],
                    "total_units": 0,
                    "total_form_minutes": 0,
                    #"total_casenote_minutes": emrform.emrepisode.get_total_casenote_minutes(),
                }

            #results[emrform.emrepisode]["total_units"] += emrform.get_units()
            #results[emrform.emrepisode]["total_form_minutes"] += emrform.total_minutes


            if emrform.emrformtype.get_name() != "case note":
                results[emrform.emrepisode]["forms"].append(emrform.get_emrform_instance())


        return render(request, "report/pps_cst_report.html", {
            "title": "PPS CST Report",
            "form": form,
            "results": results,
            "mh_admission": mh_admission,
            "mh_discharge": mh_discharge,
            "show_all_formtypes": show_all_formtypes,
            #"report_id": report_id,
            #"pdf_report_id": pdf_report_id,
        })












@access_control("admin", "financial", "caseworker")
def pps_tcm_report(request):
    if request.method == "GET":
        form = PpsTcmReportForm(request=request)

        return render(request, "report/pps_tcm_report.html", {
            "title": "PPS TCM Report",
            "form": form,
        })

    else:
        form = PpsTcmReportForm(request.POST, request=request)

        service_date_from = request.POST.get("service_date_from") or timezone.now().date()
        service_date_to = request.POST.get("service_date_to") or timezone.now().date()
        billing_code = request.POST.get("billing_code")
        client_id = request.POST.get("client")
        episode_status = request.POST.get("episode_status")
        aoda_admission = bool(request.POST.get("aoda_admission") == 'on')
        aoda_discharge = bool(request.POST.get("aoda_discharge") == 'on')
        mh_admission = bool(request.POST.get("mh_admission") == 'on')
        mh_discharge = bool(request.POST.get("mh_discharge") == 'on')

        # These are used to track whether or not individual sections should
        # show (if they have results, they show) and this is used when the
        # user doesn't select any form types in the filter (so all for types)
        has_aoda_admission = False
        has_aoda_discharge = False
        has_mh_admission = False
        has_mh_discharge = False


        query = Q()
        query.add(Q(is_finalized=True), Q.AND)

        formtypes_query = Q()
        #formtypes_query.add(Q(emrcasenoteform__isnull=False), Q.OR)

        if aoda_admission:
            formtypes_query.add(Q(emrtcmppsaodaadmissionform__isnull=False), Q.OR)
        if aoda_discharge:
            formtypes_query.add(Q(emrtcmppsaodadischargeform__isnull=False), Q.OR)
        if mh_admission:
            formtypes_query.add(Q(emrtcmppsmhadmissionform__isnull=False), Q.OR)
        if mh_discharge:
            formtypes_query.add(Q(emrtcmppsmhdischargeform__isnull=False), Q.OR)
        if not aoda_admission and not aoda_discharge and not mh_admission and not mh_discharge:
            formtypes_query.add(Q(emrtcmppsaodaadmissionform__isnull=False), Q.OR)
            formtypes_query.add(Q(emrtcmppsaodadischargeform__isnull=False), Q.OR)
            formtypes_query.add(Q(emrtcmppsmhadmissionform__isnull=False), Q.OR)
            formtypes_query.add(Q(emrtcmppsmhdischargeform__isnull=False), Q.OR)




        query.add(formtypes_query, Q.AND)


        query.add(Q(emrepisode__episode_type="tcm"), Q.AND)
        query.add(Q(date__gte=service_date_from), Q.AND)
        query.add(Q(date__lte=service_date_to), Q.AND)


        if billing_code:
            query.add(Q(billing_code=billing_code), Q.AND)

        if client_id:
            query.add(Q(emrepisode__client__id=client_id), Q.AND)

        if episode_status == "discharged":
            query.add(Q(emrepisode__end__isnull=False), Q.AND)

        if episode_status == "open":
            query.add(Q(emrepisode__end__isnull=True), Q.AND)


        '''
        # Not sure if ref_source exists outside of crisis forms at this point
        if ref_source:
            rs_or = Q()
            rs_or.add(Q(emrtcmnwcnoteform__ref_source=ref_source), Q.OR)
            rs_or.add(Q(emrjacksoncountyassesresponseform__ref_source=ref_source), Q.OR)
            rs_or.add(Q(emrnwcassesresponseform__ref_source=ref_source), Q.OR)
            query.add(rs_or, Q.AND)
        '''

        # The results on this report are grouped by episode and the episodes
        # should be ordered by date
        results = {}
        emrforms = EmrForm.objects.filter(query).order_by(
            "emrepisode__client__last_name",
            "emrepisode__client__first_name",
            "emrepisode__client__birthdate",
            "date",
            "emrformtype__name",
            "billing_code")


        for emrform in emrforms:
            if emrform.emrepisode not in results:
                results[emrform.emrepisode] = {
                    "episode": emrform.emrepisode,
                    "forms": [],
                    "total_units": 0,
                    "total_form_minutes": 0,
                    "total_casenote_minutes": emrform.emrepisode.get_total_casenote_minutes(),
                    "has_aoda_admission": False,
                    "has_aoda_discharge": False,
                    "has_mh_admission": False,
                    "has_mh_discharge": False,
                }

            results[emrform.emrepisode]["total_units"] += emrform.get_units()
            results[emrform.emrepisode]["total_form_minutes"] += emrform.total_minutes


            if emrform.emrformtype.get_name() != "case note":
                results[emrform.emrepisode]["forms"].append(emrform.get_emrform_instance())

                if emrform.emrformtype.get_name() == "pps aoda admission":
                    results[emrform.emrepisode]["has_aoda_admission"] = True

                elif emrform.emrformtype.get_name() == "pps aoda discharge":
                    results[emrform.emrepisode]["has_aoda_discharge"] = True

                elif emrform.emrformtype.get_name() == "pps mental health admission":
                    results[emrform.emrepisode]["has_mh_admission"] = True

                elif emrform.emrformtype.get_name() == "pps mental health discharge":
                    results[emrform.emrepisode]["has_mh_discharge"] = True




        return render(request, "report/pps_tcm_report.html", {
            "title": "PPS TCM Report",
            "form": form,
            "results": results,
            "aoda_admission": aoda_admission,
            "aoda_discharge": aoda_discharge,
            "mh_admission": mh_admission,
            "mh_discharge": mh_discharge,
            #"report_id": report_id,
            #"pdf_report_id": pdf_report_id,
        })









@access_control("admin", "financial", "caseworker")
def pps_crisis_report(request):
    if request.method == "GET":
        form = PpsCrisisReportForm(request=request)

        return render(request, "report/pps_crisis_report.html", {
            "title": "PPS Crisis Report",
            "form": form,
        })

    else:
        form = PpsCrisisReportForm(request.POST, request=request)

        service_date_from = request.POST.get("service_date_from") or timezone.now().date()
        service_date_to = request.POST.get("service_date_to") or timezone.now().date()
        billing_code = request.POST.get("billing_code")
        client_id = request.POST.get("client")
        ref_source = request.POST.get("ref_source")


        #set_autocomplete(None, form, "client", Client, client_id)

        query = Q()
        query.add(Q(is_finalized=True), Q.AND)

        formtypes_query = Q()
        formtypes_query.add(Q(emrcasenoteform__isnull=False), Q.OR)
        formtypes_query.add(Q(emrcrisisnwcnoteform__isnull=False), Q.OR)
        formtypes_query.add(Q(emrjacksoncountyassesresponseform__isnull=False), Q.OR)
        formtypes_query.add(Q(emrnwcassesresponseform__isnull=False), Q.OR)
        query.add(formtypes_query, Q.AND)

        query.add(Q(emrepisode__episode_type="crisis"), Q.AND)
        query.add(Q(date__gte=service_date_from), Q.AND)
        query.add(Q(date__lte=service_date_to), Q.AND)



        if billing_code:
            query.add(Q(billing_code=billing_code), Q.AND)

        if client_id:
            query.add(Q(emrepisode__client__id=client_id), Q.AND)

        if ref_source:
            rs_or = Q()
            rs_or.add(Q(emrcrisisnwcnoteform__ref_source=ref_source), Q.OR)
            rs_or.add(Q(emrjacksoncountyassesresponseform__ref_source=ref_source), Q.OR)
            rs_or.add(Q(emrnwcassesresponseform__ref_source=ref_source), Q.OR)
            query.add(rs_or, Q.AND)




        # The results on this report are grouped by episode and the episodes
        # should be ordered by date
        results = {}
        emrforms = EmrForm.objects.filter(query).order_by(
            "emrepisode__client__last_name",
            "emrepisode__client__first_name",
            "emrepisode__client__birthdate",
            "date",
            "emrformtype__name",
            "billing_code")


        for emrform in emrforms:
            if emrform.emrepisode not in results:
                results[emrform.emrepisode] = {
                    "episode": emrform.emrepisode,
                    "forms": [],
                    "total_units": 0,
                    "total_form_minutes": 0,
                    "total_casenote_minutes": 0,
                    "nwc_note_minutes": 0,
                    "county_assessment_minutes": 0,
                    "nwc_assessment_minutes": 0,
                }

            results[emrform.emrepisode]["forms"].append(emrform)
            results[emrform.emrepisode]["total_units"] += emrform.get_units()


            if emrform.emrformtype.get_name() == "case note":
                results[emrform.emrepisode]["total_casenote_minutes"] += emrform.total_minutes
            else:
                results[emrform.emrepisode]["total_form_minutes"] += emrform.total_minutes


                if emrform.emrformtype.get_name() == "nwc note":
                    results[emrform.emrepisode]["nwc_note_minutes"] += emrform.total_minutes
                elif emrform.emrformtype.get_name() == "county assessment and response plan":
                    results[emrform.emrepisode]["county_assessment_minutes"] += emrform.total_minutes
                elif emrform.emrformtype.get_name() == "nwc assessment and response plan":
                    results[emrform.emrepisode]["nwc_assessment_minutes"] += emrform.total_minutes



        '''
        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/pps_crisis_report.html", {
                "title": "PPS Crisis Report",
                "form": form,
                "results": results,
                #"report_id": report_id,
                "pdf_report_id": pdf_report_id,
            })
        '''


        return render(request, "report/pps_crisis_report.html", {
            "title": "PPS Crisis Report",
            "form": form,
            "results": results,
            #"report_id": report_id,
            #"pdf_report_id": pdf_report_id,
        })








@access_control("admin", "caseworker", "financial")
def program_billing_report(request):
    if request.method == "GET":
        form = ProgramBillingReportForm(request=request)

        return render(request, "report/program_billing_report.html", {
            "title": "Program Billing Report",
            "form": form,
        })

    else:
        form = ProgramBillingReportForm(request.POST, request=request)

        service_date_from = request.POST.get("service_date_from") or timezone.now().date()
        service_date_to = request.POST.get("service_date_to") or timezone.now().date()
        episode_type = request.POST.get("episode_type")
        caseworker_id = request.POST.get("caseworker")
        billing_code = request.POST.get("billing_code")

        #set_autocomplete(None, form, "client", Client, client_id)

        query = Q()
        query.add(Q(date__gte=service_date_from), Q.AND)
        query.add(Q(date__lte=service_date_to), Q.AND)

        if episode_type:
            query.add(Q(emrepisode__episode_type=episode_type), Q.AND)

        if caseworker_id:
            caseworker = UserConfig.objects.get(id=caseworker_id)
            query.add(Q(created_by=caseworker.user), Q.AND)

        if billing_code:
            query.add(Q(billing_code=billing_code), Q.AND)


        results = EmrForm.objects.filter(query)


        # Not only builds the CSV file but also generates any sums
        grand_total_minutes = 0
        grand_total_units = 0
        csv_results = []
        for item in results:
            grand_total_minutes = item.total_minutes
            grand_total_units += item.get_units()

            csv_results.append({
                "client": item.emrepisode.client.name(),
                "client_dob": item.emrepisode.client.birthdate,
                "service_date": item.date,
                "total_minutes": item.total_minutes,
                "units": item.get_units(),
                "staff": item.created_by.userconfig.full_name,
                "episode": item.emrepisode.get_episode_type_display(),
                "form": item.emrformtype.name,
                "billing_code": "N0000" if item.billing_code == "unbillable" else item.billing_code,
            })
        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(
            csv_results, cls=DjangoJSONEncoder)


        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/program_billing_report.html", {
                "title": "Program Billing Report",
                "form": form,
                "results": results,
                "report_id": report_id,
                "pdf_report_id": pdf_report_id,
                "grand_total_minutes": grand_total_minutes,
                "grand_total_units": grand_total_units,
            })


        return render(request, "report/program_billing_report.html", {
            "title": "Program Billing Report",
            "form": form,
            "results": results,
            "report_id": report_id,
            "pdf_report_id": pdf_report_id,
            "grand_total_minutes": grand_total_minutes,
            "grand_total_units": grand_total_units,
        })



@access_control("admin", "financial")
def time_to_note_report(request):
    if request.method == "GET":
        form = TimeToNoteReportForm(request=request)

        return render(request, "report/time_to_note_report.html", {
            "title": "Time to Note Report",
            "form": form,
        })

    else:
        form = TimeToNoteReportForm(request.POST, request=request)

        service_date_from = request.POST.get("service_date_from") or timezone.now().date()
        service_date_to = request.POST.get("service_date_to") or timezone.now().date()
        # Finalized date is not currently being used as a filter
        #finalized_date_from = request.POST.get("finalized_date_from")
        #finalized_date_to = request.POST.get("finalized_date_to")
        client_id = request.POST.get("client")
        episode_type = request.POST.get("episode_type")
        caseworker_id = request.POST.get("caseworker")


        set_autocomplete(None, form, "client", Client, client_id)

        query = Q()

        # Only want to capture Case Notes and the Crisis Assessment forms
        formtypes_query = Q()
        formtypes_query.add(Q(emrcasenoteform__isnull=False), Q.OR)
        formtypes_query.add(Q(emrjacksoncountyassesresponseform__isnull=False), Q.OR)
        formtypes_query.add(Q(emrnwcassesresponseform__isnull=False), Q.OR)
        formtypes_query.add(Q(emroutofcountyassesresponseform__isnull=False), Q.OR)
        query.add(formtypes_query, Q.AND)

        query.add(Q(date__gte=service_date_from), Q.AND)
        query.add(Q(date__lte=service_date_to), Q.AND)



        # Finalized date is not currently being used as a filter
        #if finalized_date_from:
        #    query.add(Q(history__0__fields__finalized_date__gte=finalized_date_from), Q.AND)
        #if finalized_date_to:
        #    # Had to do this because the JSON finalized date field is actually a
        #    # datetime so to get LTE to work we just need to add a day
        #    finalized_date_to_plus_one = (dt.datetime.strptime(finalized_date_to, "%Y-%m-%d") + dt.timedelta(days=1)).strftime("%Y-%m-%d")
        #    query.add(Q(history__0__fields__finalized_date__lte=finalized_date_to_plus_one), Q.AND)

        if episode_type:
            query.add(Q(emrepisode__episode_type=episode_type), Q.AND)

        if client_id:
            query.add(Q(emrepisode__client__id=client_id), Q.AND)

        if caseworker_id:
            caseworker = UserConfig.objects.get(id=caseworker_id)
            query.add(Q(created_by=caseworker.user), Q.AND)


        results = EmrForm.objects.filter(query)

        # Since we're already doing a loop to generate the CSV results, we're
        # also going to record all the days to finalized so we can provide
        # an average in the summary.
        total_days_to_finalized = 0

        caseworker_totals = {}

        # Loop builds up the CSV and calculates some averages for the report
        csv_results = []
        for item in results:
            days_to_final = item.get_days_to_finalized_int()
            total_days_to_finalized += days_to_final

            if item.created_by not in caseworker_totals:
                caseworker_totals[item.created_by] = days_to_final
            else:
                caseworker_totals[item.created_by] += days_to_final



            csv_results.append({
                "service_date": item.date,
                "finalize_date": item.get_first_finalized()["fields"]["finalized_date"] if item.get_first_finalized() else None,
                "client": str(item.emrepisode.client),
                "episode_type": item.emrepisode.get_episode_type_pretty(),
                "form": str(item.emrformtype),
                "days_to_finalized": days_to_final,
                "caseworker": item.created_by.userconfig.full_name
            })
        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(
            csv_results, cls=DjangoJSONEncoder)


        '''
        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/time_to_note_report.html", {
                "title": "Time to Note Report",
                "form": form,
                "results": results,
                "report_id": report_id,
                "pdf_report_id": pdf_report_id,
            })
        '''


        if total_days_to_finalized == 0:
            avg_time_to_finalized = 0
        else:
            avg_time_to_finalized = round(total_days_to_finalized / len(results), 2)

        # Loop through the caseworkers and generate averages for all of them
        caseworker_avgs = {}
        for key, value in caseworker_totals.items():
            caseworker_avgs[key] = round(value / results.filter(created_by=key).count(), 2)


        return render(request, "report/time_to_note_report.html", {
            "title": "Time to Note Report",
            "form": form,
            "results": results,
            "report_id": report_id,
            "num_open_forms": results.filter(is_finalized=False).count(),
            "avg_days_to_finalized": avg_time_to_finalized,
            "caseworker_avgs": caseworker_avgs,
            #"pdf_report_id": pdf_report_id,
        })





@access_control("admin", "financial", "caseworker")
def doc_search(request):
    if request.method == "GET":
        form = DocSearchReportForm(request=request)

        return render(request, "report/doc_search_report.html", {
            "title": "Document Search",
            "form": form,
        })

    else:
        form = DocSearchReportForm(request.POST, request=request)

        date_from = request.POST.get("date_from") or timezone.now().date()
        date_to = request.POST.get("date_to") or timezone.now().date()
        doc_type = request.POST.get("doc_type")
        lot_id = request.POST.get("lot")
        filename = request.POST.get("filename")

        set_autocomplete(None, form, "lot", TransactionLot, lot_id)

        # Need to have on query for transactionfile and one for journalfile
        # almost all the fields are the exact same

        journal_query = Q()
        trans_query = Q()

        if date_from:
            trans_query.add(Q(date__gte=date_from), Q.AND)
            journal_query.add(Q(date__gte=date_from), Q.AND)

        if date_to:
            trans_query.add(Q(date__lte=date_to), Q.AND)
            journal_query.add(Q(date__lte=date_to), Q.AND)

        if lot_id:
            lot = get_object_or_404(TransactionLot, id=lot_id)
            trans_query.add(Q(transaction__lot=lot), Q.AND)
            # WHen searching by LOT, also check Journal Entry description
            # field. this is how the users are (currently) using the description
            # field in the Journal entry, they just have it say the same
            # batch name that all the transactions under the JE are using.
            journal_query.add(Q(journal_entry__description__iexact=lot.name), Q.AND)

        if filename:
            trans_query.add(Q(trans_file__icontains=filename), Q.AND)
            journal_query.add(Q(jf_file__icontains=filename), Q.AND)


        # There needs to be two sets of queries, they can use the same
        # query data but we need results from JournalFile and TransactionFile
        # Depending on the doc_type, run one or two queries and then combine
        # the results into a list for the template
        results = []
        trans_results = TransactionFile.objects.filter(trans_query)
        journal_results = JournalFile.objects.filter(journal_query)

        if doc_type == "all":
            for trans in trans_results:
                results.append(trans)
            for je in journal_results:
                results.append(je)
        elif doc_type == "trans_only":
            results = trans_results
        elif doc_type == "journal_only":
            results = journal_results


        csv_results = []
        for item in results:
            csv_results.append({
                "date": item.date.strftime("%Y/%m%d"),
                "type": "transaction" if hasattr(item, "transaction") else "journal",
                "details": str(item.transaction) if hasattr(item, "transaction") else str(item.journal_entry),
                "filename": str(item.trans_file) if hasattr(item, "transaction") else str(item.jf_file),
            })
        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(
            csv_results, cls=DjangoJSONEncoder)


        '''
        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/doc_search_report.html", {
                "title": "Document Search",
                "form": form,
                "results": results,
                "report_id": report_id,
                "pdf_report_id": pdf_report_id,
            })
        '''


        return render(request, "report/doc_search_report.html", {
            "title": "Document Search",
            "form": form,
            "results": results,
            "report_id": report_id,
            #"pdf_report_id": pdf_report_id,
        })






@access_control("admin", "caseworker")
def crisis_data_report(request):
    # Pulls data from the following crisis forms:
    # emrjacksoncountyassesresponseform
    # emroutofcountyassesresponseform
    # emrnwcassesresponseform
    # emrcrisisnwcnoteform

    results = []


    if request.method == "GET":
        form = CrisisDataReportForm(request=request)

        #form.fields["year"].initial = timezone.now().date().year

        return render(request, "report/crisis_data_report.html", {
            "title": "Crisis Data Report",
            "form": form,
        })

    else:
        totals = {}
        county_code = get_app_config("county_code")
        county_name = [
            x[1] for x in client_choices.WI_COUNTY if x[0] == county_code][0]

        form = CrisisDataReportForm(request.POST, request=request)


        type_of_contact = request.POST.get("type_of_contact")
        date_from = request.POST.get("contact_date_from") or timezone.now().date()
        date_to = request.POST.get("contact_date_to") or timezone.now().date()
        day_of_week = request.POST.get("contact_day_of_week")
        contacts_by_age = request.POST.get("contacts_by_age")
        birthdate_from = request.POST.get("client_birthdate_from")
        birthdate_to = request.POST.get("client_birthdate_to")
        gender = request.POST.get("gender")
        ed_placements = request.POST.get("ed_placements")
        outcome = request.POST.get("outcome_of_contact")
        form_type = request.POST.get("form_type")
        client = request.POST.get("client")
        county_of_residence = request.POST.get("county_of_residence")
        county_of_incident = request.POST.get("county_of_incident")
        aoda_involved = request.POST.get("aoda_involved")
        law_involved = request.POST.get("law_involved")
        law_dept = request.POST.get("law_dept")
        part_of_existing = request.POST.get("part_of_existing")

        ft_nwc_note = request.POST.get("ft_nwc_note")
        ft_county_rp = request.POST.get("ft_county_rp")
        ft_nwc_rp = request.POST.get("ft_nwc_rp")
        ft_ooc_rp = request.POST.get("ft_ooc_rp")



        request.report_error = validate_crisis_data_report(request)
        if hasattr(request, "report_error") and request.report_error:
            return render(request, 'report/crisis_data_report.html', {
                'title': "Crisis Data Report",
                'form': form,

            })


        query = Q()
        formtypes_query = Q()
        # We only want to pull data from the nwc note and the assessments
        # The user can select one or none of the form types. If the user
        # selects none, that is the same as selecting all of them
        if ft_nwc_note:
            formtypes_query.add(Q(emrcrisisnwcnoteform__isnull=False), Q.OR)
        if ft_county_rp:
            formtypes_query.add(Q(emrjacksoncountyassesresponseform__isnull=False), Q.OR)
        if ft_nwc_rp:
            formtypes_query.add(Q(emrnwcassesresponseform__isnull=False), Q.OR)
        if ft_ooc_rp:
            formtypes_query.add(Q(emroutofcountyassesresponseform__isnull=False), Q.OR)
        if not ft_nwc_note and not ft_county_rp and not ft_nwc_rp and not ft_ooc_rp:
            formtypes_query.add(Q(emrcrisisnwcnoteform__isnull=False), Q.OR)
            formtypes_query.add(Q(emrjacksoncountyassesresponseform__isnull=False), Q.OR)
            formtypes_query.add(Q(emrnwcassesresponseform__isnull=False), Q.OR)
            formtypes_query.add(Q(emroutofcountyassesresponseform__isnull=False), Q.OR)
        query.add(formtypes_query, Q.AND)

        exclude = Q()
        query.add(Q(date__gte=date_from), Q.AND)
        query.add(Q(date__lte=date_to), Q.AND)

        if type_of_contact:
            or_query = Q()
            or_query.add(Q(emroutofcountyassesresponseform__type_of_contact=type_of_contact), Q.OR)
            or_query.add(Q(emrnwcassesresponseform__type_of_contact=type_of_contact), Q.OR)
            or_query.add(Q(emrjacksoncountyassesresponseform__type_of_contact=type_of_contact), Q.OR)
            or_query.add(Q(emrcrisisnwcnoteform__type_of_contact=type_of_contact), Q.OR)
            query.add(or_query, Q.AND)

        if day_of_week:
            query.add(Q(date__week_day=int(day_of_week)), Q.AND)

        if contacts_by_age:
            if contacts_by_age == "youth_to_13":
                query.add(Q(client_age__lte=13), Q.AND)
            elif contacts_by_age == "youth_14_to_17":
                query.add(Q(client_age__gte=14), Q.AND)
                query.add(Q(client_age__lte=17), Q.AND)
            elif contacts_by_age == "youth_under_18":
                query.add(Q(client_age__lte=17), Q.AND)
            elif contacts_by_age == "adult":
                query.add(Q(client_age__gte=18), Q.AND)
            elif contacts_by_age == "unknown":
                query.add(Q(client_age__isnull=True), Q.AND)

        if client:
            query.add(Q(emrepisode__client=client), Q.AND)

        if birthdate_from:
            query.add(Q(emrepisode__client__birthdate__gte=birthdate_from), Q.AND)
        if birthdate_to:
            query.add(Q(emrepisode__client__birthdate__lte=birthdate_to), Q.AND)

        if gender:
            query.add(Q(emrepisode__client__gender=gender), Q.AND)

        if ed_placements:
            sub_query = Q()
            sub_query.add(Q(emrjacksoncountyassesresponseform__rp_outcome_facility__istartswith=ed_placements), Q.OR)
            sub_query.add(Q(emrnwcassesresponseform__hospital_location__istartswith=ed_placements), Q.OR)
            query.add(sub_query, Q.AND)

        if outcome:
            sub_query = Q()
            sub_query.add(Q(emrjacksoncountyassesresponseform__rp_outcome=outcome), Q.OR)
            sub_query.add(Q(emroutofcountyassesresponseform__outcome_of_contact=outcome), Q.OR)
            sub_query.add(Q(emrnwcassesresponseform__outcome_of_contact=outcome), Q.OR)
            sub_query.add(Q(emrcrisisnwcnoteform__outcome_of_contact=outcome), Q.OR)
            query.add(sub_query, Q.AND)

        if form_type:
            if form_type == "all_assessments":
                sub_query = Q()
                sub_query.add(Q(emrjacksoncountyassesresponseform__county_of_residence__isnull=False), Q.OR)
                sub_query.add(Q(emrnwcassesresponseform__county_of_residence__isnull=False), Q.OR)
                sub_query.add(Q(emroutofcountyassesresponseform__county_of_residence__isnull=False), Q.OR)
                query.add(sub_query, Q.AND)
            else:
                query.add(Q(emrformtype=form_type), Q.AND)


        if county_of_residence:
            if county_of_residence == "non_agency_county":
                sub_query = Q()
                # These two queries return all the non-agency county forms
                sub_query.add(Q(emrcrisisnwcnoteform__county_of_residence__isnull=False), Q.OR)
                sub_query.add(Q(emrjacksoncountyassesresponseform__county_of_residence__isnull=False), Q.OR)
                sub_query.add(Q(emrnwcassesresponseform__county_of_residence__isnull=False), Q.OR)
                sub_query.add(Q(emroutofcountyassesresponseform__county_of_residence__isnull=False), Q.OR)
                query.add(sub_query, Q.AND)

                exclude_query = Q()
                exclude_query.add(Q(emrcrisisnwcnoteform__county_of_residence=county_code), Q.OR)
                exclude_query.add(Q(emrjacksoncountyassesresponseform__county_of_residence=county_code), Q.OR)
                exclude_query.add(Q(emrnwcassesresponseform__county_of_residence=county_code), Q.OR)
                exclude_query.add(Q(emroutofcountyassesresponseform__county_of_residence=county_code), Q.OR)
                exclude.add(Q(exclude_query), Q.OR)

            else:
                sub_query = Q()
                sub_query.add(Q(emrcrisisnwcnoteform__county_of_residence=county_of_residence), Q.OR)
                sub_query.add(Q(emrjacksoncountyassesresponseform__county_of_residence=county_of_residence), Q.OR)
                sub_query.add(Q(emrnwcassesresponseform__county_of_residence=county_of_residence), Q.OR)
                sub_query.add(Q(emroutofcountyassesresponseform__county_of_residence=county_of_residence), Q.OR)
                query.add(sub_query, Q.AND)

                if county_of_incident == county_code:
                    # Even though the form doesn't have the county of
                    # incident field, all the county crisis forms will
                    # show up if the filter is set to the agency county
                    sub_query.add(Q(emrjacksoncountyassesresponseform__isnull=False), Q.OR)


        if county_of_incident:
            if county_of_incident == "non_agency_county":
                sub_query = Q()
                sub_query.add(Q(emrcrisisnwcnoteform__county_of_incident__isnull=False), Q.OR)
                sub_query.add(Q(emrnwcassesresponseform__county_of_incident__isnull=False), Q.OR)
                sub_query.add(Q(emroutofcountyassesresponseform__county_of_incident__isnull=False), Q.OR)
                query.add(sub_query, Q.AND)

                exclude_query = Q()
                exclude_query.add(Q(emrcrisisnwcnoteform__county_of_incident=county_code), Q.OR)
                exclude_query.add(Q(emrnwcassesresponseform__county_of_incident=county_code), Q.OR)
                exclude_query.add(Q(emroutofcountyassesresponseform__county_of_incident=county_code), Q.OR)
                exclude.add(Q(exclude_query), Q.OR)

            else:
                sub_query = Q()
                sub_query.add(Q(emrcrisisnwcnoteform__county_of_incident=county_of_incident), Q.OR)
                sub_query.add(Q(emrnwcassesresponseform__county_of_incident=county_of_incident), Q.OR)
                sub_query.add(Q(emroutofcountyassesresponseform__county_of_incident=county_of_incident), Q.OR)

                if county_of_incident == county_code:
                    # Even though the form doesn't have the county of
                    # incident field, all the county crisis forms will
                    # show up if the filter is set to the agency county
                    sub_query.add(Q(emrjacksoncountyassesresponseform__isnull=False), Q.OR)

                query.add(sub_query, Q.AND)



        if aoda_involved:
            sub_query = Q()
            sub_query.add(Q(emrcrisisnwcnoteform__alcohol_substance_factor=aoda_involved), Q.OR)
            sub_query.add(Q(emrnwcassesresponseform__alcohol_substance_factor=aoda_involved), Q.OR)
            sub_query.add(Q(emroutofcountyassesresponseform__alcohol_substance_factor=aoda_involved), Q.OR)
            sub_query.add(Q(emrjacksoncountyassesresponseform__alcohol_sa_factor=aoda_involved), Q.OR)
            query.add(sub_query, Q.AND)

        if law_involved:
            sub_query = Q()
            sub_query.add(Q(emrcrisisnwcnoteform__law_inv=law_involved), Q.OR)
            sub_query.add(Q(emrnwcassesresponseform__law_inv=law_involved), Q.OR)
            sub_query.add(Q(emroutofcountyassesresponseform__law_inv=law_involved), Q.OR)
            sub_query.add(Q(emrjacksoncountyassesresponseform__law_inv=law_involved), Q.OR)
            query.add(sub_query, Q.AND)

        if law_dept:
            sub_query = Q()
            sub_query.add(Q(emrcrisisnwcnoteform__law_dept=law_dept), Q.OR)
            sub_query.add(Q(emrnwcassesresponseform__law_dept=law_dept), Q.OR)
            sub_query.add(Q(emroutofcountyassesresponseform__law_dept=law_dept), Q.OR)
            sub_query.add(Q(emrjacksoncountyassesresponseform__law_dept=law_dept), Q.OR)
            query.add(sub_query, Q.AND)


        elif part_of_existing:
            # This one is a little different. There are in fact 3 states to this
            # boolean value that we can filter by:
            #   Null: default action is to include these forms along with the rest of the results
            #   Yes : only show forms that have been marked as part of existing
            #   No  : exclude any forms that have been marked as part of existing
            if part_of_existing == "yes":
                sub_query = Q()
                sub_query.add(Q(emrcrisisnwcnoteform__part_of_existing=True), Q.OR)
                sub_query.add(Q(emrnwcassesresponseform__part_of_existing=True), Q.OR)
                sub_query.add(Q(emrjacksoncountyassesresponseform__part_of_existing=True), Q.OR)
                query.add(sub_query, Q.AND)


            if part_of_existing == "no":
                exclude_query = Q()
                exclude_query.add(Q(emrcrisisnwcnoteform__part_of_existing=True), Q.OR)
                exclude_query.add(Q(emrnwcassesresponseform__part_of_existing=True), Q.OR)
                exclude_query.add(Q(emrjacksoncountyassesresponseform__part_of_existing=True), Q.OR)
                exclude.add(Q(exclude_query), Q.OR)



            #sub_query.add(Q(emrjacksoncountyassesresponseform__???="yes"), Q.OR)

            #sub_query.add(Q(emroutofcountyassesresponseform__alcohol_sa_factor="yes"), Q.OR)





        results = EmrForm.objects.filter(query).exclude(exclude)


        # Generate some summaries for the results


        # Unique clients
        totals["Unique Clients"] = results.values("emrepisode__client").distinct().count()

        # Find how many of the forms were assessments
        assess_query = Q()
        assess_query.add(Q(emrjacksoncountyassesresponseform__isnull=False), Q.OR)
        assess_query.add(Q(emrnwcassesresponseform__isnull=False), Q.OR)
        assess_query.add(Q(emroutofcountyassesresponseform__isnull=False), Q.OR)
        totals["Assessments"] = results.filter(assess_query).count()

        # Sum how many of the forms are NWC notes
        totals["NWC Notes"] = results.filter(emrcrisisnwcnoteform__isnull=False).count()

        law_q = Q()
        law_q.add(Q(emrcrisisnwcnoteform__law_dept__isnull=False), Q.OR)
        law_q.add(Q(emrnwcassesresponseform__law_dept__isnull=False), Q.OR)
        law_q.add(Q(emroutofcountyassesresponseform__law_dept__isnull=False), Q.OR)
        totals["Law Enforcement Involved"] = results.filter(law_q).count()

        aoda_q = Q()
        aoda_q.add(Q(emrnwcassesresponseform__alcohol_substance_factor='yes'), Q.OR)
        aoda_q.add(Q(emrjacksoncountyassesresponseform__alcohol_sa_factor='yes'), Q.OR)
        aoda_q.add(Q(emrcrisisnwcnoteform__alcohol_substance_factor='yes'), Q.OR)
        aoda_q.add(Q(emroutofcountyassesresponseform__alcohol_substance_factor='yes'), Q.OR)
        totals["AODA Involved"] = results.filter(aoda_q).count()



        # Generate summaries for all contact types in the following forms:
        # This should stay near the bottom because they are all grouped
        # together with an <HR> in the template
        #   emrjacksoncountyassesresponseform
        #   emroutofcountyassesresponseform
        #   emrnwcassesresponseform
        #   emrcrisisnwcnoteform
        for toc in client_choices.CRISIS_TYPE_OF_CONTACT:
            value, display = toc

            contact_q = Q()
            contact_q.add(Q(emrjacksoncountyassesresponseform__type_of_contact=value), Q.OR)
            contact_q.add(Q(emroutofcountyassesresponseform__type_of_contact=value), Q.OR)
            contact_q.add(Q(emrnwcassesresponseform__type_of_contact=value), Q.OR)
            contact_q.add(Q(emrcrisisnwcnoteform__type_of_contact=value), Q.OR)
            totals[f"{display} Contacts"] = results.filter(contact_q).count()

        # Total contacts, just the count of the forms/notes
        totals["All Contacts"] = results.count()



        csv_results = []
        report_id = str(uuid.uuid4())
        for _item in results:
            item = _item.get_emrform_instance()

            outcome = ""
            if hasattr(item, "rp_outcome"):
                outcome = item.get_rp_outcome_display()
            elif hasattr(item, "outcome_of_contact"):
                outcome = item.get_outcome_of_contact_display()

            outfac = ""
            if hasattr(item, "rp_outcome_facility"):
                outfac = item.rp_outcome_facility
            elif hasattr(item, "hospital_location"):
                outfac = item.hospital_location

            aoda_fac = ""
            if hasattr(item, "alcohol_sa_factor"):
                aoda_fac = item.get_alcohol_sa_factor_display()

            aoda_inv = ""
            if hasattr(item, "alcohol_substance_factor"):
                aoda_inv = item.get_alcohol_substance_factor_display()

            part_exist = ""
            if hasattr(item, "part_of_existing"):
                if item.part_of_existing:
                    part_exist = "Yes"
                else:
                    part_exist = "No"

            day_of_week = ""
            if hasattr(item, "day_of_week"):
                day_of_week = item.get_day_of_week_display()


            county_inc = ""
            if hasattr(item, "county_of_incident"):
                county_inc = item.get_county_of_incident_display()



            csv_results.append({
                "date": item.date.strftime(settings.DATE_FORMAT),
                "day": day_of_week,
                "client id": item.emrepisode.client.id,
                "client first name": item.emrepisode.client.first_name,
                "client last name": item.emrepisode.client.last_name,
                "gender": item.emrepisode.client.get_gender_display(),
                "birthdate": ("" if not item.emrepisode.client.birthdate else item.emrepisode.client.birthdate.strftime(settings.DATE_FORMAT)),
                "age": item.emrepisode.client.get_age(),
                "county of residence": item.get_county_of_residence_display(),
                "county of incident": county_inc,
                "form": item.emrformtype.name,
                "outcome": outcome,
                "ed placement/facility": outfac,
                "aoda involved": aoda_inv,
                "aoda factor": aoda_fac,
                "law enforcement involved": item.get_law_inv_display(),
                "law enforcement department": item.get_law_dept_display(),
                "other law enforcement department": item.law_dept_other,
                "part of existing contact": part_exist,
            })

        request.session[report_id] = json.dumps(
            csv_results, cls=DjangoJSONEncoder)


        '''
        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/crisis_data_report.html", {
                "title": "Crisis Data Report",
                "form": form,
                "results": results,
                "report_id": report_id,
                "pdf_report_id": pdf_report_id,
                "results": results,

            })
        '''


        return render(request, "report/crisis_data_report.html", {
            "title": "Crisis Data Report",
            "form": form,
            "results": results,
            "report_id": report_id,
            #"pdf_report_id": pdf_report_id,
            "results": results,
            "county_name": county_name,
            "county_code": county_code,
            "totals": totals,
        })





@access_control("admin", "financial", "caseworker")
def contract_report(request):
    if request.method == "GET":
        form = ContractReportForm()

        return render(request, "report/contract_report.html", {
            "title": "Contract Report",
            "form": form,
        })

    else:
        form = ContractReportForm(request.POST)

        year = request.POST.get("year")
        vendor = request.POST.get("vendor")
        service = request.POST.get("service")
        contract_type = request.POST.get("contract_type")


        query = Q()

        if year:
            query.add(Q(year=year), Q.AND)

        if vendor:
            query.add(Q(vendor__id=vendor), Q.AND)

        if service:
            query.add(Q(contractservice__service__id=service), Q.AND)

        if contract_type:
            query.add(Q(contract_type=contract_type), Q.AND)



        results = Contract.objects.filter(query)


        csv_results = []
        for item in results:
            total, limit, percent = item.get_usage()
            csv_results.append({
                "year": item.year,
                "contract_type": item.get_contract_type_display(),
                "vendor_name": item.vendor.name,
                "vendor_number": item.vendor.vendor_number,
                "vendor_street": item.vendor.street,
                "vendor_suite": item.vendor.suite,
                "vendor_city": item.vendor.city,
                "vendor_state": item.vendor.state,
                "vendor_zip": item.vendor.zip,
                "amount": item.amount,
                "used": total,
                "percent_used": percent,
                #"service": item.service,
            })
        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(
            csv_results, cls=DjangoJSONEncoder)


        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/contract_report.html", {
                "title": "Contract Report",
                "form": form,
                "results": results,
                "report_id": report_id,
                "pdf_report_id": pdf_report_id,
            })


        return render(request, "report/contract_report.html", {
            "title": "Contract Report",
            "form": form,
            "results": results,
            "report_id": report_id,
            "pdf_report_id": pdf_report_id,
        })






@access_control("admin", "caseworker")
def clinic_preadmit_report(request):
    if request.method == "GET":
        form = ClinicPreadmitReportForm()

        return render(request, "report/clinic_preadmit_report.html", {
            "title": "Clinic Pre-Admit Report",
            "form": form,
        })

    else:
        form = ClinicPreadmitReportForm(request.POST)
        date_from = request.POST.get("wait_list_date_from")
        date_to = request.POST.get("wait_list_date_to")
        pregnant = bool(request.POST.get("pregnant") == "on")
        iv_drug_use = bool(request.POST.get("iv_drug_use") == "on")
        mh = bool(request.POST.get("mh") == "on")
        aoda = bool(request.POST.get("aoda") == "on")
        mh_and_aoda = bool(request.POST.get("mh_and_aoda") == "on")
        legal_status = request.POST.get("legal_status")

        query = Q()
        if date_from:
            query.add(Q(date__gte=date_from), Q.AND)

        if date_to:
            query.add(Q(date__lte=date_to), Q.AND)

        if pregnant:
            query.add(Q(pregnant=pregnant), Q.AND)

        if iv_drug_use:
            query.add(Q(iv_drug_use=iv_drug_use), Q.AND)

        if mh:
            query.add(Q(mh=mh), Q.AND)

        if aoda:
            query.add(Q(aoda=aoda), Q.AND)

        if mh_and_aoda:
            query.add(Q(mh_and_aoda=mh_and_aoda), Q.AND)

        if legal_status:
            query.add(Q(legal_status=legal_status), Q.AND)



        results = EmrClinicWaitlistForm.objects.filter(query).filter(emrepisode__end__isnull=True)

        report_results = []

        for res in results:
            report_results.append({
                "waitlistform": res,
                "emrepisode": res.emrepisode,
                "client": res.emrepisode.client,
                "insurance_info": res.emrepisode.client.get_insurance_info_html(),
                "wait_list_start": res.date,
                "pregnant": res.pregnant,
                "iv_drug_use": res.iv_drug_use,
                "mh": res.mh,
                "aoda": res.aoda,
                "mh_and_aoda": res.mh_and_aoda,
                "url": f"/client/emr_form/{res.id}/",
                "legal_status": res.get_legal_status_display() or '',
            })


        # Also need to show clients who only have open Clinic Pre-Admit Episodes
        # but do not have the wait list form filled out.
        # If a user asks why some data on the report is missing, it's becuase
        # the client does not have an open episode with a filled out wait list
        # form. The client may have a wait list form, but it's likely under
        # a closed episode. e.g. the user has 2 preadmit episodes - one
        # closed and open and the closed episode has the wait list form but the
        # open episode does not. wonderful edge case...
        unfiltered_epi_results = EmrClinicPreadmitEpisode.objects.filter(
            end__isnull=True)

        if date_from:
            unfiltered_epi_results = unfiltered_epi_results.filter(start__gte=date_from)
        if date_to:
            unfiltered_epi_results = unfiltered_epi_results.filter(start__lte=date_to)

        for epi in unfiltered_epi_results:
            found = [x for x in report_results if x["emrepisode"].id == epi.id]
            if not found:
                # this is an episode that doesn't already exist in our
                # results so it can be added to the report results
                report_results.append({
                    "waitlistform": None,
                    "emrepisode": epi,
                    "client": epi.client,
                    "insurance_info": epi.client.get_insurance_info_html().replace("<br>", "\n"),
                    "wait_list_start": "",
                    "pregnant": "",
                    "iv_drug_use": "",
                    "mh": "",
                    "aoda": "",
                    "mh_and_aoda": "",
                    "url": f"/client/emr_episodes/{epi.client.id}/{epi.id}/",
                })



        report_results = sorted(report_results, key=lambda item: item["client"].last_name)



        csv_results = []
        for item in results:
            csv_results.append({
                "episode start date": item.emrepisode.start,
                "wait list start date": item.date,
                "pregnant": item.pregnant,
                "iv drug use": item.iv_drug_use,
                "mh": item.mh,
                "aoda": item.aoda,
                "mh_and_aoda": item.mh_and_aoda,
                "legal_status": item.get_legal_status_display() or '',
            })
        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(
            csv_results, cls=DjangoJSONEncoder)

        '''
        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/REPORT_NAME.html", {
                "title": "REPORT_NAME Report",
                "form": form,
                "results": results,
                "report_id": report_id,
                "pdf_report_id": pdf_report_id,
            })
        '''


        return render(request, "report/clinic_preadmit_report.html", {
            "title": "Clinic Pre-Admit Report",
            "form": form,
            "results": report_results,
            "report_id": report_id,
            #"pdf_report_id": pdf_report_id,
        })





@access_control("admin", "caseworker")
def randi_external_referral_report(request):

    if request.method == "GET":
        form = RandiExternalReferralReportForm(request=request)

        return render(request, "report/randi_external_referral_report.html", {
            "title": "R & I External Referral Report",
            "form": form,
        })

    else:
        form = RandiExternalReferralReportForm(request.POST, request=request)

        referral_date_from = request.POST.get("referral_date_from")
        referral_date_to = request.POST.get("referral_date_to")
        referral_source = request.POST.get("referral_source")
        agency_name = request.POST.get("agency_name")
        legal_status = request.POST.get("legal_status")
        gender = request.POST.get("client_gender")
        birthdate_from = request.POST.get("client_birthdate_from")
        birthdate_to = request.POST.get("client_brithdate_to")
        previous_referrals = request.POST.get("client_has_previous_referrals")
        closing_reason = request.POST.get("closing_reason")
        closing_reason2 = request.POST.get("closing_reason2")
        closing_reason3 = request.POST.get("closing_reason3")
        ref_close_date_from = request.POST.get("referral_close_date_from")
        ref_close_date_to = request.POST.get("referral_close_date_to")


        query = Q()

        must_have_previous_referrals = False

        if referral_date_from:
            query.add(Q(date__gte=referral_date_from), Q.AND)

        if referral_date_to:
            query.add(Q(date__lte=referral_date_to), Q.AND)

        if referral_source:
            query.add(Q(ref_source=referral_source), Q.AND)

        if agency_name:
            query.add(Q(ref_agency_name__iexact=agency_name), Q.AND)

        if legal_status:
            query.add(Q(legal_status=legal_status), Q.AND)

        if gender:
            query.add(Q(emrepisode__client__gender=gender), Q.AND)

        if birthdate_from:
            query.add(Q(emrepisode__client__birthdate__gte=birthdate_from), Q.AND)

        if birthdate_to:
            query.add(Q(emrepisode__client__birthdate__lte=birthdate_to), Q.AND)

        if previous_referrals == "on":
            must_have_previous_referrals = True





        query_closing_reason = Q()
        if closing_reason:
            query_closing_reason.add(Q(emrepisode__closing_reason=closing_reason), Q.OR)
            query_closing_reason.add(Q(emrepisode__closing_reason2=closing_reason), Q.OR)
            query_closing_reason.add(Q(emrepisode__closing_reason3=closing_reason), Q.OR)

        if closing_reason2:
            query_closing_reason.add(Q(emrepisode__closing_reason=closing_reason2), Q.OR)
            query_closing_reason.add(Q(emrepisode__closing_reason2=closing_reason2), Q.OR)
            query_closing_reason.add(Q(emrepisode__closing_reason3=closing_reason2), Q.OR)

        if closing_reason3:
            query_closing_reason.add(Q(emrepisode__closing_reason=closing_reason3), Q.OR)
            query_closing_reason.add(Q(emrepisode__closing_reason2=closing_reason3), Q.OR)
            query_closing_reason.add(Q(emrepisode__closing_reason3=closing_reason3), Q.OR)

        query.add(query_closing_reason, Q.AND)

        if ref_close_date_from:
            query.add(Q(end__gte=ref_close_date_from), Q.AND)
        if ref_close_date_to:
            query.add(Q(end__lte=ref_close_date_to), Q.AND)



        results = []

        external_refs = EmrRandiExternalReferralForm.objects.filter(query).order_by("-date")

        for ref in external_refs:
            # Need to check if client has had previous referrals.
            # If, in the future this report needs speeding up, create
            # a dictionary with the client as the key track if they've had
            # previous referrals that way we're not doing a lookup for
            # every single result.
            # Should be easy to track if the client has previous referrals,
            # if they have more than 1 referral of any type they have them

            # Also need to check if the client has previous discarges, this is
            # just closed R & I episodes


            # Getting previous discarges (closed R&I episodes)
            closed_epis = EmrRandiEpisode.objects.filter(
                client=ref.emrepisode.client,
                end__isnull=False
            )


            # If must_haveprevious_referrals is True then skip any that
            # do not have previous referrals,
            # same with must_have_previous_discharges
            if must_have_previous_referrals is True:
                if closed_epis.count() < 1:
                    continue


            results.append({
                "id": ref.id,
                "client": ref.emrepisode.client.name(),
                "ref_date": ref.date,
                "ref_source": ref.get_ref_source_display(),
                "ref_name": ref.ref_name,
                "ref_agency_name": ref.ref_agency_name,
                "legal_status": ref.get_legal_status_display(),
                "req_services": None,
                "gender": ref.emrepisode.client.get_gender_display(),
                "birthdate": ref.emrepisode.client.birthdate,
                "episode_number": ref.emrepisode.get_episode_number(),
                "start": ref.emrepisode.start,
                "end": ref.emrepisode.end,
                "closing_reason": ", ".join(ref.emrepisode.get_closing_reason()),
            })


        report_results = []
        for item in results:
            report_results.append({
                "client": item["client"],
                "referral date": item["ref_date"],
                "referral source": item["ref_source"],
                "referrer name": item["ref_name"],
                "referring agency": item["ref_agency_name"],
                "legal status": item["legal_status"],
                "client gender": item["gender"],
                "client birthdate": item["birthdate"],
                "episode number": item["episode_number"],
                "episode open date": item["start"],
                "episode close date": item["end"],
                "episode closing reason": item["closing_reason"],
            })


        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(
            report_results, cls=DjangoJSONEncoder)


        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/randi_internal_referral_report.html", {
                "title": "R & I Referral Report",
                "form": form,
                "results": results,
                "report_id": report_id,
                "pdf_report_id": pdf_report_id,
            })


        return render(request, "report/randi_external_referral_report.html", {
            "title": "R & I External Referral Report",
            "form": form,
            "results": results,
            "report_id": report_id,
            "pdf_report_id": pdf_report_id,
        })











@access_control("admin", "caseworker")
def randi_internal_referral_report(request):
    """ Case Note report allows filtering and viewing of case notes using
        varios filters including the worker
    """

    if request.method == "GET":
        form = RandiInternalReferralReportForm(request=request)

        return render(request, "report/randi_internal_referral_report.html", {
            "title": "R & I Internal Referral Report",
            "form": form,
        })

    else:
        form = RandiInternalReferralReportForm(request.POST, request=request)

        referral_date_from = request.POST.get("referral_date_from")
        referral_date_to = request.POST.get("referral_date_to")
        legal_status = request.POST.get("legal_status")
        gender = request.POST.get("client_gender")
        birthdate_from = request.POST.get("client_birthdate_from")
        birthdate_to = request.POST.get("client_brithdate_to")
        previous_referrals = request.POST.get("client_has_previous_referrals")
        req_clts = request.POST.get("requested_clts")
        req_clts_preadmit = request.POST.get("requested_clts_preadmit")
        req_crisis = request.POST.get("requested_crisis")
        req_cst = request.POST.get("requested_cst")
        req_randi = request.POST.get("requested_randi")
        req_mh = request.POST.get("requested_mh")
        req_aoda = request.POST.get("requested_aoda")
        req_dual = request.POST.get("requested_dual")
        closing_reason = request.POST.get("closing_reason")
        closing_reason2 = request.POST.get("closing_reason2")
        closing_reason3 = request.POST.get("closing_reason3")
        ref_close_date_from = request.POST.get("referral_close_date_from")
        ref_close_date_to = request.POST.get("referral_close_date_to")


        query = Q()

        must_have_previous_referrals = False

        if referral_date_from:
            query.add(Q(date__gte=referral_date_from), Q.AND)

        if referral_date_to:
            query.add(Q(date__lte=referral_date_to), Q.AND)

        if legal_status:
            query.add(Q(legal_status=legal_status), Q.AND)

        if gender:
            query.add(Q(emrepisode__client__gender=gender), Q.AND)

        if birthdate_from:
            query.add(Q(emrepisode__client__birthdate__gte=birthdate_from), Q.AND)

        if birthdate_to:
            query.add(Q(emrepisode__client__birthdate__lte=birthdate_to), Q.AND)

        if previous_referrals == "on":
            must_have_previous_referrals = True


        if req_clts:
            query.add(Q(refto_clts=True), Q.AND)

        if req_clts_preadmit:
            query.add(Q(refto_clts_preadmit=True), Q.AND)

        if req_crisis:
            query.add(Q(refto_crisis=True), Q.AND)

        if req_cst:
            query.add(Q(refto_cst=True), Q.AND)

        if req_randi:
            query.add(Q(refto_randi=True), Q.AND)

        if req_mh:
            query.add(Q(refto_mh=True), Q.AND)

        if req_aoda:
            query.add(Q(refto_res_aoda=True), Q.AND)

        if req_dual:
            query.add(Q(refto_mh=True), Q.AND)
            query.add(Q(refto_res_aoda=True), Q.AND)

        query_closing_reason = Q()
        if closing_reason:
            query_closing_reason.add(Q(emrepisode__closing_reason=closing_reason), Q.OR)
            query_closing_reason.add(Q(emrepisode__closing_reason2=closing_reason), Q.OR)
            query_closing_reason.add(Q(emrepisode__closing_reason3=closing_reason), Q.OR)

        if closing_reason2:
            query_closing_reason.add(Q(emrepisode__closing_reason=closing_reason2), Q.OR)
            query_closing_reason.add(Q(emrepisode__closing_reason2=closing_reason2), Q.OR)
            query_closing_reason.add(Q(emrepisode__closing_reason3=closing_reason2), Q.OR)

        if closing_reason3:
            query_closing_reason.add(Q(emrepisode__closing_reason=closing_reason3), Q.OR)
            query_closing_reason.add(Q(emrepisode__closing_reason2=closing_reason3), Q.OR)
            query_closing_reason.add(Q(emrepisode__closing_reason3=closing_reason3), Q.OR)

        query.add(query_closing_reason, Q.AND)

        if ref_close_date_from:
            query.add(Q(end__gte=ref_close_date_from), Q.AND)
        if ref_close_date_to:
            query.add(Q(end__lte=ref_close_date_to), Q.AND)



        results = []


        internal_refs = EmrRandiReferralForm.objects.filter(query).order_by("-date")

        for ref in internal_refs:
            # Getting previous discarges (closed R&I episodes)
            closed_epis = EmrRandiEpisode.objects.filter(
                client=ref.emrepisode.client,
                end__isnull=False
            )


            # If must_haveprevious_referrals is True then skip any that
            # do not have previous referrals,
            # same with must_have_previous_discharges
            if must_have_previous_referrals is True:
                if closed_epis.count() < 1:
                    continue


            results.append({
                "id": ref.id,
                "client": ref.emrepisode.client.name(),
                "ref_date": ref.date,
                "ref_name": ref.ref_worker,
                "legal_status": ref.get_legal_status_display(),
                "req_services": "<br>".join(ref.get_requested_services()),
                "gender": ref.emrepisode.client.get_gender_display(),
                "birthdate": ref.emrepisode.client.birthdate,
                "episode_number": ref.emrepisode.get_episode_number(),
                "start": ref.emrepisode.start,
                "end": ref.emrepisode.end,
                "closing_reason": ", ".join(ref.emrepisode.get_closing_reason()),
            })



        report_results = []
        for item in results:
            report_results.append({
                "client": item["client"],
                "referral date": item["ref_date"],
                "referrer name": item["ref_name"],
                "legal status": item["legal_status"],
                "client gender": item["gender"],
                "client birthdate": item["birthdate"],
                "episode number": item["episode_number"],
                "episode open date": item["start"],
                "episode close date": item["end"],
                "episode closing reason": item["closing_reason"],
            })



        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(
            report_results, cls=DjangoJSONEncoder)


        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/randi_internal_referral_report.html", {
                "title": "R & I Referral Report",
                "form": form,
                "results": results,
                "report_id": report_id,
                "pdf_report_id": pdf_report_id,
            })


        return render(request, "report/randi_internal_referral_report.html", {
            "title": "R & I Internal Referral Report",
            "form": form,
            "results": results,
            "report_id": report_id,
            "pdf_report_id": pdf_report_id,
        })











'''
# This view is not in use, can probably delete one day
@access_control("admin", "financial", "caseworker")
def doc_report(request):
    results = []
    report_id = None
    pdf_report_id = None

    if request.method == "GET":
        form = DocReportForm()
    else:
        form = DocReportForm(request.POST)

        date_from = request.POST.get("date_from")
        date_to = request.POST.get("date_to")
        client = request.POST.get("client")

        set_autocomplete(None, form, "client", Client, client)

        # This report can cotnain info about uploaded documents. This 
        # can be grouped by episode (date descending) 


        if client:
            docs = EmrDoc.objects.filter(
                emrepisode__start__gte=date_from, emrepisode__start__lte=date_to, emrepisode__client=client)
        else:
            docs = EmrDoc.objects.filter(
                emrepisode__start__gte=date_from, emrepisode__start__lte=date_to)


        # Grab the episode IDs and use those to group up the documents
        episode_ids = docs.order_by("-emrepisode__start").values("emrepisode").distinct()


        for epi_id in episode_ids:
            episode = get_object_or_None(EmrEpisode, id=epi_id["emrepisode"])

            docs = EmrDoc.objects.filter(emrepisode=episode)            

            results.append({
                "episode": episode,
                "docs": docs,
            })


        report_id = str(uuid.uuid4())
        csv_results = []
        for result in results:
            for doc in result["docs"]:
                csv_results.append({
                    "episode type": result["episode"].get_episode_type_display(),
                    "episode_start": result["episode"].start,
                    "episode_end": result["episode"].end,
                    "document_date": doc.date,
                    "document_uploaded": doc.upload_date,
                    "document_description": doc.description,
                    "upload_user": str(doc.uploaded_by),
                    "filename": str(doc.doc_file),
                })
        request.session[report_id] = json.dumps(
            csv_results, cls=DjangoJSONEncoder)

        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/doc_report.html", {
                "title": "Documents Report",
                "results": results,
            })


    return render(request, "report/doc_report.html", {
        "title": "Documents Report",
        "form": form,
        "report_id": report_id,
        "pdf_report_id": pdf_report_id,
        "results": results,
    })
'''




@access_control("admin", "caseworker")
def length_of_service_report(request):
    """ This report pulls information about a client and shows
        a summary of their length of enrollment (in days) per program.
        Group by episode type, order by date desc
    """
    total_service_days = 0
    report_id = None
    results = []
    client = None

    if request.method == "GET":
        form = LengthOfServiceForm()

    else:
        form = LengthOfServiceForm(request.POST)

        client_id = request.POST.get("client")
        client = get_object_or_404(Client, id=client_id)

        set_autocomplete(None, form, "client", Client, client_id)


        episodes = EmrEpisode.objects.filter(client=client).order_by("-start")
        

        for epi in episodes:
            # Create a result object that contains the epiosode and the
            # total number of days and the status of the episode (open/closed)
            if epi.end is None:
                epi_days = (timezone.now().date() - epi.start).days
            else:
                epi_days = (epi.end - epi.start).days

            total_service_days += epi_days



            results.append({
                "episode": epi,
                "epi_days": epi_days,
            })


        report_id = str(uuid.uuid4())
        csv_results = []
        for result in results:
            csv_results.append({
                "episode type": result["episode"].get_episode_type_display(),
                "start": result["episode"].start,
                "end": result["episode"].end,
                "open days": result["epi_days"],
                "closing reason": result["episode"].get_closing_reason_display()
            })

        request.session[report_id] = json.dumps(
            csv_results, cls=DjangoJSONEncoder)




    return render(request, "report/length_of_service_report.html", {
        "title": "Length of Service Report",
        "form": form,
        "report_id": report_id,
        "results": results,
        "client": client,
        "total_service_days": total_service_days,
        "request": request,

    })





@access_control("admin", "caseworker")
def current_roster_report(request):
    """ This report gives a list of the currently active episodes per client
        grouped by episode type
    """



    if request.method == "GET":
        form = CurrentRosterReportForm()

        episodes = EmrEpisode.objects.filter(end__isnull=True).order_by(
            "episode_type", 
            "client__last_name", 
            "client__first_name")


    elif request.method == "POST":
        form = CurrentRosterReportForm(request.POST)

        episode_type = request.POST.get("episode_type")

        if episode_type:
            episodes = EmrEpisode.objects.filter(
                end__isnull=True, 
                episode_type=episode_type).order_by(
                "episode_type", 
                "client__last_name", 
                "client__first_name")

        else:
            episodes = EmrEpisode.objects.filter(end__isnull=True).order_by(
                "episode_type", 
                "client__last_name", 
                "client__first_name")






    results = []
    episode_types = {}
    grand_total = 0
    for epi in episodes:
        if epi.episode_type not in episode_types:
            episode_types[epi.episode_type] = {
                "episode": epi,
                "episodes": episodes.filter(episode_type=epi.episode_type),
                "total": len(episodes.filter(episode_type=epi.episode_type)),
            }

            grand_total += len(episodes.filter(episode_type=epi.episode_type))


    for item in episode_types:
        results.append(episode_types[item])


    csv_report_id = str(uuid.uuid4())
    csv_results = []
    for result in results:
        for item in result["episodes"]:
            csv_results.append({
                "start": item.start,
                "episode type": item.get_episode_type_display(),
                "client name": item.client.name(),
                "client dob": item.client.birthdate,
                "client gender": item.client.get_gender_display(),
            })
    request.session[csv_report_id] = json.dumps(
        csv_results, cls=DjangoJSONEncoder)



    pdf_report_id = str(uuid.uuid4())
    request.session[pdf_report_id] = render_to_string(
        "report/current_roster_report.html", {
            "title": "Current Roster Report",
            "results": results,
            "grand_total": grand_total,
        })


    return render(request, "report/current_roster_report.html", {
        "title": "Current Roster Report",
        "form": form,
        "results": results,
        "grand_total": grand_total,
        "csv_report_id": csv_report_id,
        "pdf_report_id": pdf_report_id,
    })



@access_control("admin", "caseworker")
def discharge_report(request):
    """ This report pulls data from the selected program to show who 
        was admitted/had an episode opened d uring a specified date range
    """

    results = None
    report_id = None

    if request.method == "GET":
        form = DischargeReportForm()

    else:
        form = DischargeReportForm(request.POST)

        end_date_from = request.POST.get("end_date_from")
        end_date_to = request.POST.get("end_date_to")
        episode_type = request.POST.get("episode_type")

        if episode_type:
            results = EmrEpisode.objects.filter(
                episode_type=episode_type,
                end__gte=end_date_from,
                end__lte=end_date_to,
            )

        else:
            results = EmrEpisode.objects.filter(
                end__gte=end_date_from,
                end__lte=end_date_to,
            )


        report_id = str(uuid.uuid4())
        csv_results = []
        for item in results:
            csv_results.append({
                "start": item.start,
                "end": item.end,
                "episode type": item.get_episode_type_display(),
                "client name": item.client.name(),
                "client dob": item.client.birthdate,
                "client gender": item.client.get_gender_display(),
            })
        request.session[report_id] = json.dumps(
            csv_results, cls=DjangoJSONEncoder)



    return render(request, "report/discharge_report.html", {
        "title": "Discarge Report",
        "form": form,
        "results": results,
        "report_id": report_id,
    })




@access_control("admin", "caseworker")
def admission_report(request):
    """ This report pulls data from the selected program to show who 
        was admitted/had an episode opened d uring a specified date range
    """

    results = None
    report_id = None
    pdf_report_id = None

    if request.method == "GET":
        form = AdmissionReportForm()

    else:
        form = AdmissionReportForm(request.POST)

        date_from = request.POST.get("date_from")
        date_to = request.POST.get("date_to")
        episode_type = request.POST.get("episode_type")

        if episode_type:
            results = EmrEpisode.objects.filter(
                episode_type=episode_type,
                start__gte=date_from,
                start__lte=date_to,
            )

        else:
            results = EmrEpisode.objects.filter(
                start__gte=date_from,
                start__lte=date_to,
            )


        report_id = str(uuid.uuid4())
        csv_results = []
        for item in results:
            csv_results.append({
                "start": item.start,
                "end": item.end,
                "episode type": item.get_episode_type_display(),
                "client name": item.client.name(),
                "client dob": item.client.birthdate,
                "client gender": item.client.get_gender_display(),
            })
        request.session[report_id] = json.dumps(
            csv_results, cls=DjangoJSONEncoder)


        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/admission_report.html", {
                "title": "Admission Report",
                "form": form,
                "results": results,
                "report_id": report_id,
                "pdf_report_id": pdf_report_id,
            })



    return render(request, "report/admission_report.html", {
        "title": "Admission Report",
        "form": form,
        "results": results,
        "report_id": report_id,
        "pdf_report_id": pdf_report_id,
    })




@access_control("admin", "caseworker")
def emr_form_report(request, emr_form_id):
    instance = get_emrform_instance(emr_form_id)


    model_dict = model_to_dict(instance)
    # Clean out some FKs and other stuff that we don't need.
    # Doesn't need to be perfect, because I doubt people will use the
    # CSV export often if at all
    del model_dict["id"]
    del model_dict["emrepisode"]
    del model_dict["emrformtype"]
    del model_dict["signature"]
    del model_dict["supervisor_signature"]


    model_dict["linked_docs"] = instance.get_linked_docs()

    report_id = str(uuid.uuid4())
    request.session[report_id] = json.dumps(
        [model_dict, ], cls=DjangoJSONEncoder)

    pdf_report_id = str(uuid.uuid4())
    request.session[pdf_report_id] = render_to_string(
        "report/emr_form_report.html", {
            "title": f"{instance.emrformtype.name} Report",
            "agency_address": get_agency_address(dept_name="Behavioral Health Division"),
            "instance": instance,
            "report_id": report_id,
            "episode": instance.emrepisode,
            "pdf_report_id": pdf_report_id,
        })


    return render(request, "report/emr_form_report.html", {
        "title": f"{instance.emrformtype.name} Report",
        "agency_address": get_agency_address(dept_name="Behavioral Health Division"),
        "instance": instance,
        "report_id": report_id,
        "episode": instance.emrepisode,
        "pdf_report_id": pdf_report_id,
    })















@access_control("admin", "caseworker")
def case_notes(request):
    """ Case Note report allows filtering and viewing of case notes using
        varios filters including the worker
    """

    if request.method == "GET":
        form = CaseNotesForm()

        return render(request, "report/case_notes.html", {
            "title": "Case Notes Report",
            "form": form,
        })

    else:
        form = CaseNotesForm(request.POST)
        date_from = request.POST.get("date_from")
        date_to = request.POST.get("date_to")
        caseworker = request.POST.get("caseworker")
        client = request.POST.get("client")

        query = Q()

        if date_from:
            query.add(Q(date__gte=date_from), Q.AND)
        if date_to:
            query.add(Q(date__lte=date_to), Q.AND)
        if client:
            query.add(Q(client=client), Q.AND)

        results = CaseNote.objects.filter(query)

        # This happens after the query because we don't store the caseworker
        # as an object on the note but it's encoded in the signature
        if caseworker:
            results = [x for x in results if x.get_signer_user() == UserConfig.objects.get(id=caseworker).user]


        csv_results = []
        for item in results:
            csv_results.append({
                "date": item.date,
                "note": item.note,
                "caseworker": item.get_signer_full_name(),
            })
        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(
            csv_results, cls=DjangoJSONEncoder)


        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/case_notes.html", {
                "title": "Case Notes Report",
                "form": form,
                "results": results,
                "report_id": report_id,
                "pdf_report_id": pdf_report_id,
            })


        return render(request, "report/case_notes.html", {
            "title": "Case Notes Report",
            "form": form,
            "results": results,
            "report_id": report_id,
            "pdf_report_id": pdf_report_id,
        })









@access_control("admin", "financial")
def ar_ap_county(request):
    """
         AR/AP County Report requested by Carly. This report sums up 
        county accounts and shows the totals. 
        Filter by Batch (Transaction Lot)
    """


    if request.method == "GET":
        found_lot = None
        form = ArCountyForm()
        form.fields["show_fund_totals"].initial = True

        return render(request, "report/ar_ap_county.html", {
            "form": form,
        })

    else:
        form = ArCountyForm(request.POST)
        lot = request.POST.get("lot")
        show_fund_totals = request.POST.get("show_fund_totals")

        found_lot = get_object_or_None(TransactionLot, id=lot)

        form = set_autocomplete(None, form, "lot", TransactionLot, lot)
        # get all the CREDIT transactions  in this batch, 
        # group up by COUNTY account number and then
        # sum up those totals
        ar_trans = Transaction.objects.filter(
            lot=lot, account__cash_account=False).order_by("account__name")

        # First build fund groups? then build account groups under those funds
        fund_groups = {}
        account_groups = {}
        grand_total = Decimal('0.00')
        results = []

        for trans in ar_trans:
            group_name = ""
            try:
                group_name = trans.account.county_name.split(".")[0]
            except AttributeError:
                pass

            if group_name in fund_groups:
                fund_groups[group_name]["fund_total"] += trans.get_natural_amount()
            else:
                fund_groups[group_name] = { 
                    "fund_name": group_name,
                    "fund_total": trans.get_natural_amount(), 
                    "account_groups": [] 
                }


        for trans in ar_trans:
            grand_total += trans.get_natural_amount()

            if trans.account.county_name in account_groups:
                account_groups[trans.account.county_name]["account_total"] += trans.get_natural_amount()
            else:
                account_groups[trans.account.county_name] = {
                    "transaction": trans,
                    "account_total": trans.get_natural_amount()
                }

        # Loop through the account groups, and figure out which fund_group
        # they belong to
        for key, value in account_groups.items():
            fund_name = key.split(".")[0]

            fund_groups[fund_name]["account_groups"].append(account_groups[key])



        for group in fund_groups:
            results.append(fund_groups[group])

        csv_results = []
        for fund_grp in results:
            for res in fund_grp["account_groups"]:
                csv_results.append({
                    "fund": res["transaction"].account.county_name.split(".")[0],
                    "account_name": res["transaction"].account.county_name,
                    "account_description": res["transaction"].account.county_description,
                    "account_total": res["account_total"]
                })
        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(
            csv_results, cls=DjangoJSONEncoder)


        return render(request, "report/ar_ap_county.html", {
            "form": form,
            "results": results,
            "grand_total": grand_total,
            "report_id": report_id,
            "show_fund_totals": show_fund_totals,
            "found_lot": found_lot,
        })






@access_control("admin", "financial", "caseworker")
def cc_report(request, filename):
    '''
        Depending on the download type (csv or html) requested, return 
        either a CSV or HTML file. 

        Reports are automatically cleared out when the user logs out of nova

        TODO figure out how to save the report results into the database
        instead of an actual file
    '''

    # If the is a CSV, serve up a downloadable file, otherwise
    # display the results as HTML 
    with open("/srv/nova/cc_reports/{}".format(filename), "r") as file:
        if filename.endswith(".csv"):
            response = HttpResponse(content_type="text/csv")
            response["Content-Disposition"] = "attachment; filename={}".format(filename)
            response.write(file.read())
            return response
        elif filename.endswith(".html"):
            with open("/srv/nova/cc_reports/{}".format(filename), "r") as file:
                return HttpResponse(file.read())

        return render(request, "500.html")    








@access_control("admin", "financial", "caseworker")
def cc_report_watcher_ajax(request):
    # Dynamically update the report watcher listview so the user
    # doesnt have to refresh the page

    reports = CCReport.objects.filter(
        userconfig=request.user.userconfig).order_by("-submitted")[:50]

    return render(request, "report/watcher_ajax.html", {
        "cc_reports": reports
    })


@access_control("admin", "financial", "caseworker")
def cc_report_watcher(request):
    ''' This view shows the user the last 100 cc_reports history items
        Users can see the status of their report and download their report
    '''

    reports = CCReport.objects.filter(
        userconfig=request.user.userconfig).order_by("-submitted")[:50]

    return render(request, "report/watcher_index.html", {
        "title": "Nova Reports",
        "cc_reports": reports,
    })




@access_control("admin", "financial", "caseworker")
def dcs_942(request):
    '''
        Report is required for the state, workers take this info
        and plug it into a form on the PPS site. This report is designed
        to match the state's form as closely as possible.

        The goal of this report is to show expenditures for each service
        under each target group
    '''

    if request.method == "GET":
        form = Dcs942Form()

        form.fields["fiscal_year_from"].initial = get_fiscal_year()
        form.fields["fiscal_period_from"].initial = 1
        form.fields["fiscal_year_to"].initial = get_fiscal_year()
        form.fields["fiscal_period_to"].initial = 18


        return render(request, 'report/dcs_942_index.html', {
            'form': form,
            'title': 'DCS-942 Report',
        })

    elif request.method == "POST":
        form = Dcs942Form(request.POST)
        fiscal_year_from = request.POST.get("fiscal_year_from")
        fiscal_period_from = request.POST.get("fiscal_period_from")
        fiscal_year_to = request.POST.get("fiscal_year_to")
        fiscal_period_to = request.POST.get("fiscal_period_to")

        form.fields["fiscal_year_from"].initial = fiscal_year_from
        form.fields["fiscal_period_from"].initial = fiscal_period_from
        form.fields["fiscal_year_to"].initial = fiscal_year_to
        form.fields["fiscal_period_to"].initial = fiscal_period_to

        fiscal_from = int(f"{int(fiscal_year_from)}{int(fiscal_period_from):02}")
        fiscal_to = int(f"{int(fiscal_year_to)}{int(fiscal_period_to):02}")


        # list of SPC categories that we need to report on
        spcs = [
            "100", "104", "107", "200", "300", "400", "500", "600", "615",
            "950", "700", "202", "205", "506", "705", "711", "800", "305",
            "501", "507", "512", "704", "710", "900", "703", "505",
        ]

        # list of target groups we need to report on
        targets = [
            "None", "01", "31", "18", "57", "06", "61", "64", "58"
        ]


        report_grand_total = Decimal('0.00')
        report_over18_grand_total = Decimal('0.00')
        report_under18_grand_total = Decimal('0.00')
        results = []
        for spc in spcs:
            grand_total = Decimal('0.00')
            res = {
                "spc": spc,
                "totals": {},
            }

            for tgt in targets:
                total = Decimal('0.00')
                over18 = Decimal('0.00')
                under18 = Decimal('0.00')

                if tgt == "None":                    
                    trans = Transaction.objects.filter(
                        fiscal__gte=fiscal_from,
                        fiscal__lte=fiscal_to,
                        service__spc_code__istartswith=spc,
                        target_group=None,
                    )
                else:
                    trans = Transaction.objects.filter(
                        fiscal__gte=fiscal_from,
                        fiscal__lte=fiscal_to,
                        service__spc_code__istartswith=spc,
                        target_group=tgt,
                    )                


                for t in trans:                    
                    total += t.get_natural_amount()                   

                    # Figure out of the client was over 18 at the time
                    # of this service or under 18 at the time of this
                    # service. We should never run into a situation where
                    # there is not a client
                    if t.client is not None:                        
                        age = relativedelta(t.service_date, t.client.birthdate)

                        #print("Age is {}".format(age))

                        if age.years >= 18:
                            over18 += t.get_natural_amount()                            
                        else:
                            under18 += t.get_natural_amount()



                res["totals"][tgt] = total               
                res["totals"]["{}_over18".format(tgt)] = over18
                res["totals"]["{}_under18".format(tgt)] = under18
                report_over18_grand_total += over18
                report_under18_grand_total += under18

                grand_total += total

            res["grand_total"] = grand_total or ''
            report_grand_total += grand_total            


            results.append(res)



        # Calculate totals by target, also calculate a grand total
        # for the whole report

        target_totals = {}
        for r in results:
            # get the target, then loop through the rest of the results
            # and add up all the vtotals with this target, regardless of spc
            for tgt in r["totals"]:
                tgt_grand_total = Decimal('0.00')

                for x in range(len(results)):
                    if tgt in results[x]["totals"]:
                        tgt_grand_total += results[x]["totals"][tgt]


                target_totals[tgt] = tgt_grand_total



        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(
            results, cls=DjangoJSONEncoder)


        #build the report and pass it to the render machine!
        return render(request, 'report/dcs_942_index.html', {
            'form': form,
            'results': results,
            "report_id": report_id,
            'title': 'DCS-942 Report',
            "target_totals": target_totals,
            "report_grand_total": report_grand_total,
            "report_over18_grand_total": report_over18_grand_total,
            "report_under18_grand_total": report_under18_grand_total,
            "agency_phone": get_app_config("agency_phone"),
            "agency_contact": get_app_config("agency_contact"),
        })










@access_control("admin", "financial")
def budget_report(request):
    '''
        Much like the existing budget view, this report shows accounts with
        their utilization for the given year. The difference is that this
        report shows ALL the accounts, even ones that are not in any budget
    '''

    if request.method == "GET":
        form = BudgetReportForm()

        form.fields["year"].initial = get_fiscal_year(request)

        return render(request, 'report/budget_report_index.html', {
            'form': form,
            'title': 'Budget Report',
        })

    elif request.method == "POST":
        form = BudgetReportForm(request.POST)
        year = int(request.POST.get("year"))
        period = request.POST.get("period") or None

        grand_total = Decimal('0.00')
        results = []

        # loop through all the accounts.
        # get usage for each account
        # if the account is in a budget, show its utilization
        accounts = Account.objects.all().order_by("-active", "name")

        for act in accounts:
            # check if this is in a budget?? if so, get its budget utilization??
            # if there is not budget for this account, just show the account balance
            budget_item = BudgetItem.objects.filter(budget__year=year, account=act).first()                

            if budget_item:
                total, limit, percent = budget_item.get_usage(period)
                utilization = "{}%".format(percent)
            else:                
                if period:
                    total = get_account_balance_up_to_fiscal(act, year, int(period))
                else:
                    total = get_account_balance_fiscal(act, year)

                utilization = ""
                limit = ""

            results.append({
                "active": act.active,
                "account": str(act),
                "account_name": act.name,
                "account_county_name": act.county_name,
                "account_description": act.description,
                "account__county_description": act.county_description,
                "amount": total,
                "utilization": utilization,
                "limit": limit,
            })

            grand_total += total


        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(
            results, cls=DjangoJSONEncoder)



        #build the report and pass it to the render machine!
        return render(request, 'report/budget_report_index.html', {
            'form': form,
            'title': 'Budget Report',
            'results': results,
            "report_id": report_id,
            "grand_total": grand_total,
        })




@access_control("admin", "financial", "caseworker")
def funding_profile_report(request):
    '''
        Grabs a list of transaction by account with totals for a selected
        fiscal year and period. Don't list accounts that don't have transactions
        This report has 2 modes and 2 templates!
        First is the deafult just showing data up to and through the period
        Second is for the whole fiscal year, including those extra periods (13-18) that I know nothing about!!!
    '''
    # lists transactions under a client.
    # Filter by: client, date, fiscal year/period
    # group by account
    if request.method == "GET":
        form = FundingProfileReportForm()

        form.fields["fiscal_year"].initial = get_fiscal_year(request)
        form.fields["fiscal_period"].initial = get_fiscal_period(request)
        form.fields["hide_zeroes"].initial = True
        form.fields["hide_inactive"].initial = True

        return render(request, 'report/funding_profile_report_index.html', {
            'form': form,
            'title': 'Funding Profile Report',
        })

    elif request.method == "POST":
        form = FundingProfileReportForm(request.POST)
        fiscal_year = request.POST.get("fiscal_year")
        fiscal_period = request.POST.get("fiscal_period")
        hide_zeroes = request.POST.get("hide_zeroes")
        hide_inactive = bool(request.POST.get("hide_inactive") == "on")

        # DEPRECATED CODE HERE
        # ak 2023-04-06 why don't you delete this????
        if fiscal_period and False:
            print("NO LONGER HERE")
            return
            ''' This is old code that just calculated a single fiscal period
                im leaving this here just in case but we'll probably never
                need it again
            '''
            #import pdb; pdb.set_trace()
            # Get all the funding profiles
            # for each funding profile, build an object, this object will contain
            # an account number, and transaction totals for each period of
            # the specified year, and the total for the year
            results = []
            grand_total = Decimal('0.00')

            for fp in FundingProfile.objects.all().order_by("name"):
                # find all the accounts under this object and find all the
                # transactions for each account during that fiscal time
                accounts = []
                fp_total = Decimal('0.00')
                for account in Account.objects.filter(funding_profile=fp).order_by("name"):
                    total = get_account_balance_up_to_fiscal(account, fiscal_year=fiscal_year, fiscal_period=fiscal_period)

                    if hide_zeroes:
                        if total == 0:
                            continue

                    grand_total += total
                    fp_total += total
                    accounts.append({
                        "account": account,
                        "total": total,
                    })


                results.append({
                    "funding_profile": fp,
                    "accounts": accounts,   
                    "fp_total": fp_total                 
                })



            # build up a json object for the csv download
            report_id = str(uuid.uuid4())
            report_results = []
            for res in results:
                for act in res["accounts"]:
                    report_results.append({
                        "funding_profile": res["funding_profile"].name,
                        "account_name": act["account"].name,
                        "account_description": act["account"].description,
                        "total": act["total"],
                    })
            request.session[report_id] = json.dumps(report_results, cls=DjangoJSONEncoder)

            #build the report and pass it to the render machine!
            return render(request, 'report/funding_profile_report_index.html', {
                'form': form,
                'results': results,
                "grand_total": grand_total,
                "report_id": report_id,
                "title": "Funding Profile Report",
            })



        else:
            # Get all the funding profiles
            # for each funding profile, build an object, this object will contain
            # an account number, and transaction totals for each period of
            # the specified year, and the total for the year
            results = []
            grand_total = Decimal('0.00')

            for fp in FundingProfile.objects.all().order_by("name"):
                # find all the accounts under this object and find all the
                # transactions for each account during that fiscal time
                accounts = []
                fp_total = Decimal('0.00')

                # Need to be a little bit smart here. Need to calculate through
                # the period selected.

                # actually, we're going to be super dumb about this!!!
                if fiscal_period:
                    n_period = int(fiscal_period)
                else:
                    n_period = 18  # max period value is 18 that the counties use


                fp1_total = Decimal('0.00')
                fp2_total = Decimal('0.00')
                fp3_total = Decimal('0.00')
                fp4_total = Decimal('0.00')
                fp5_total = Decimal('0.00')
                fp6_total = Decimal('0.00')
                fp7_total = Decimal('0.00')
                fp8_total = Decimal('0.00')
                fp9_total = Decimal('0.00')
                fp10_total = Decimal('0.00')
                fp11_total = Decimal('0.00')
                fp12_total = Decimal('0.00')
                fp13_total = Decimal('0.00')
                fp14_total = Decimal('0.00')
                fp15_total = Decimal('0.00')
                fp16_total = Decimal('0.00')
                fp17_total = Decimal('0.00')
                fp18_total = Decimal('0.00')


                if hide_inactive:
                    found_accounts = Account.objects.filter(
                        funding_profile=fp, 
                        active=True, 
                        cash_account=False).order_by("name")
                else:                    
                    found_accounts = Account.objects.filter(
                        funding_profile=fp, 
                        cash_account=False).order_by("name")

                for account in found_accounts:
                    p1 = Decimal('0.00')
                    p2 = Decimal('0.00')
                    p3 = Decimal('0.00')
                    p4 = Decimal('0.00')
                    p5 = Decimal('0.00')
                    p6 = Decimal('0.00')
                    p7 = Decimal('0.00')
                    p8 = Decimal('0.00')
                    p9 = Decimal('0.00')
                    p10 = Decimal('0.00')
                    p11 = Decimal('0.00')
                    p12 = Decimal('0.00')
                    p13 = Decimal('0.00')
                    p14 = Decimal('0.00')
                    p15 = Decimal('0.00')
                    p16 = Decimal('0.00')
                    p17 = Decimal('0.00')
                    p18 = Decimal('0.00')


                    account_obj = {}


                    if n_period >= 1:
                        p1 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=1, make_credit_negative=True)                        

                    if n_period >= 2:
                        p2 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=2, make_credit_negative=True)                        

                    if n_period >= 3:
                        p3 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=3, make_credit_negative=True) 

                    if n_period >= 4:                        
                        p4 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=4, make_credit_negative=True) 

                    if n_period >= 5:
                        p5 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=5, make_credit_negative=True) 

                    if n_period >= 6:
                        p6 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=6, make_credit_negative=True)                         

                    if n_period >= 7:
                        p7 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=7, make_credit_negative=True) 

                    if n_period >= 8:
                        p8 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=8, make_credit_negative=True)                         

                    if n_period >= 9:
                        p9 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=9, make_credit_negative=True) 

                    if n_period >= 10:
                        p10 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=10, make_credit_negative=True) 

                    if n_period >= 11:
                        p11 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=11, make_credit_negative=True) 

                    if n_period >= 12:
                        p12 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=12, make_credit_negative=True) 

                    if n_period >= 13:
                        p13 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=13, make_credit_negative=True) 

                    if n_period >= 14:
                        p14 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=14, make_credit_negative=True) 

                    if n_period >= 15:
                        p15 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=15, make_credit_negative=True) 

                    if n_period >= 16:
                        p16 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=16, make_credit_negative=True) 

                    if n_period >= 17:
                        p17 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=17, make_credit_negative=True) 

                    if n_period >= 18:
                        p18 = get_account_balance_fiscal(account, fiscal_year=fiscal_year, fiscal_period=18, make_credit_negative=True) 


                    fp1_total += p1
                    fp2_total += p2
                    fp3_total += p3
                    fp4_total += p4
                    fp5_total += p5
                    fp6_total += p6
                    fp7_total += p7
                    fp8_total += p8
                    fp9_total += p9
                    fp10_total += p10
                    fp11_total += p11
                    fp12_total += p12
                    fp13_total += p13
                    fp14_total += p14
                    fp15_total += p15
                    fp16_total += p16
                    fp17_total += p17
                    fp18_total += p18


                    act_total = (
                        p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9 
                        + p10 + p11 + p12 + p13 + p14 + p15 + p16 
                        + p17 + p18
                    )

                    if hide_zeroes:
                        if act_total == 0:
                            continue

                    fp_total += act_total
                    grand_total += act_total

                    account_obj["p1"] = p1
                    account_obj["p2"] = p2
                    account_obj["p3"] = p3
                    account_obj["p4"] = p4
                    account_obj["p5"] = p5
                    account_obj["p6"] = p6
                    account_obj["p7"] = p7
                    account_obj["p8"] = p8
                    account_obj["p9"] = p9
                    account_obj["p10"] = p10
                    account_obj["p11"] = p11                    
                    account_obj["p12"] = p12
                    account_obj["p13"] = p13
                    account_obj["p14"] = p14
                    account_obj["p15"] = p15
                    account_obj["p16"] = p16
                    account_obj["p17"] = p17
                    account_obj["p18"] = p18

                    account_obj["account"] = account
                    account_obj["total"] = act_total

                    accounts.append(account_obj)




                results.append({
                    "funding_profile": fp,
                    "accounts": accounts,   
                    "fp_total": fp_total,
                    "fp1_total": fp1_total,
                    "fp2_total": fp2_total,
                    "fp3_total": fp3_total,
                    "fp4_total": fp4_total,
                    "fp5_total": fp5_total,
                    "fp6_total": fp6_total,
                    "fp7_total": fp7_total,
                    "fp8_total": fp8_total,
                    "fp9_total": fp9_total,
                    "fp10_total": fp10_total,
                    "fp11_total": fp11_total,
                    "fp12_total": fp12_total,
                    "fp13_total": fp13_total,
                    "fp14_total": fp14_total,
                    "fp15_total": fp15_total,
                    "fp16_total": fp16_total,
                    "fp17_total": fp17_total,
                    "fp18_total": fp18_total,
                })

            # build up a json object for the csv download
            report_id = str(uuid.uuid4())
            report_results = []
            for res in results:
                for act in res["accounts"]:
                    report_results.append({
                        "funding_profile": res["funding_profile"].name,
                        "account_name": act["account"].name,
                        "account_description": act["account"].description,
                        "jan": act["p1"],
                        "feb": act["p2"],
                        "mar": act["p3"],
                        "apr": act["p4"],
                        "may": act["p5"],
                        "jun": act["p6"],
                        "jul": act["p7"],
                        "aug": act["p8"],
                        "sep": act["p9"],
                        "oct": act["p10"],
                        "nov": act["p11"],
                        "dec": act["p12"],
                        "p13": act["p13"],
                        "p14": act["p14"],
                        "p15": act["p15"],
                        "p16": act["p16"],
                        "p17": act["p17"],
                        "p18": act["p18"],
                        "amount": act["total"],
                    })

            request.session[report_id] = json.dumps(report_results, cls=DjangoJSONEncoder)


            #build the report and pass it to the render machine!
            return render(request, 'report/funding_profile_report_year_index.html', {
                'form': form,
                'results': results,
                "grand_total": grand_total,
                "report_id": report_id,
                "title": "Funding Profile Report"
            })








@access_control("admin", "financial", "caseworker")
def client_statement(request):
    # lists transactions under a client.
    # Filter by: client, date, fiscal year/period
    # group by account
    show_zero_balances = False # Hide or show clients with zero balances
    # the actual filtering happens in the template

    if request.method == "GET":
        form = ClientStatementForm()

        form.fields["fiscal_year"].initial = get_fiscal_year(request)
        form.fields["fiscal_period"].initial = get_fiscal_period(request)

        return render(request, 'report/client_statement_index.html', {
            'form':form,
            'title':'Client Statement Report',
            })

    elif request.method == "POST":
        form = ClientStatementForm(request.POST)
        client = request.POST.get("client")
        fiscal_year = int(request.POST.get("fiscal_year"))
        fiscal_period = int(request.POST.get("fiscal_period"))
        #show_zero_balances = bool(request.POST.get("show_zero_balances") == "on")
        
        query = Q()

        query.add( (Q(client_billable=True) | Q(client_payment=True) | Q(atp_writeoff=True)), Q.AND)
        query.add(Q(client__isnull=False), Q.AND)

        if client:
            #import pdb; pdb.set_trace()
            query.add(Q(client=client), Q.AND)


        fiscal = int(f"{int(fiscal_year)}{int(fiscal_period):02}")

        if fiscal_period >= 1 and fiscal_period <=6:
            # We have 18 fiscal periods so if this client statement is running
            # up through June there may be payments and adjustments in periods
            # 13-18 of the 'previous' fiscal year
            # basically, add 12 to period and subtract 1 year
            extra_period_fiscal = int(f"{int(fiscal_year) - 1}{int(fiscal_period) + 12}")
            fiscal_query = Q()
            fiscal_query.add(Q(fiscal=fiscal), Q.OR)
            fiscal_query.add(Q(fiscal=extra_period_fiscal), Q.OR)

            query.add(fiscal_query, Q.AND)

            #import pdb; pdb.set_trace()

        else:
            query.add(Q(fiscal=fiscal), Q.AND)


        results = []

        # So this is how the values and distinct actually works.
        # Even though you set your values here, you can pull more values
        # out of the same queryset below. Try to not forget that.
        
        if client:
            # This is kind of stupud, just sayin
            clients = Transaction.objects.filter(client=client).distinct().order_by('client__last_name').values("client")
        else:
            clients = Transaction.objects.filter(client__isnull=False).distinct().order_by('client__last_name').values("client")


        # this is used to comapre against the list of clients with 
        # starting balances, so we don't have duplicates
        clients_list = list(clients.values("client__last_name", "client__id"))

        #import pdb; pdb.set_trace()

        # also need to include any clients with starting balances
        if client:
            starting_balances = Client.objects.filter(starting_balance__isnull=False, id=client).order_by("last_name").values("last_name", "id")
        else:
            starting_balances = Client.objects.filter(starting_balance__isnull=False).order_by("last_name").values("last_name", "id")


        # Also need to include any clients with previous balances.
        # if they're not already on the list        
        for clt in starting_balances:
            addme = {"client__last_name" : clt["last_name"],
                    "client__id" : clt["id"]}
            if addme not in clients_list:
                clients_list.append(addme)

        clients_list = sorted(clients_list, key = lambda i: i["client__last_name"])
                

        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(list(clients.all().values(
            "client__id", "client__first_name", "client__last_name",
            "client__birthdate",  "date", "service_date",
            "description", "trans_type", "amount")),
                cls=DjangoJSONEncoder)


        gt_balance = Decimal('0.00')
        gt_payments = Decimal('0.00')
        gt_charges = Decimal('0.00')
        client_results = []
        for _clt in clients_list:
            clt = get_object_or_None(Client,id=_clt["client__id"])
            total_debit = Decimal('0.00')
            total_credit = Decimal('0.00')

            result_object = {}

            # Since we're getting previous data, basically a starting
            # or carried-over balance, we don't want
            # to include this month's data.
            # even though there are 18 periods, we stop at period 12 so we
            # dont end up counting transactions twice
            if fiscal_period == 1:
                period_to = 12
                year_to = fiscal_year - 1;
            else:
                period_to = fiscal_period - 1
                year_to = fiscal_year


            past_bills = get_client_bills_or_payments(clt, "bills", year_to=year_to, period_to=period_to)                

            
            if clt.starting_balance:
                past_bills += clt.starting_balance
            past_payments = get_client_bills_or_payments(clt, "payments", year_to=year_to, period_to=period_to)
            
            #import pdb; pdb.set_trace()

            result_object["client"] = clt
            result_object["past_payments"] = past_payments
            result_object["past_bills"] = past_bills
            result_object["balance_forward"] = past_bills - past_payments
            balance = past_bills - past_payments
            result_object["starting_balance"] = balance
            transactions = []

            print("PAST_BILLS:    {}".format(past_bills))
            print("PAST_PAYMENTS: {}".format(past_payments))
            print("BALANCE:       {}".format(balance))

            credit_total = Decimal('0.00')
            debit_total = Decimal('0.00')
            for trans in Transaction.objects.filter(query).filter(client=clt).order_by("service_date", "atp_writeoff"):
                # depending on if this is a debit or credit, add or subtract from the blaances
                # look at whether this is a client payment or a client bill



                if trans.client_billable:
                    if trans.trans_type == "1":
                        balance += trans.amount
                        debit_total += trans.amount
                    elif trans.trans_type == "0":
                        balance -= trans.amount
                        credit_total += trans.amount
                    else:
                        raise Exception("Invalid account type")

                elif trans.client_payment or trans.atp_writeoff:
                    if trans.trans_type == "0":
                        balance -= trans.amount
                        credit_total += trans.amount                        
                    elif trans.trans_type == "1":
                        balance += trans.amount
                        debit_total += trans.amount
                    else:
                        raise Exception("Invalid account type")

                trans_object = {}
                trans_object["balance"] = balance
                trans_object["transaction"] = trans
                transactions.append(trans_object)

                print("")
                print("BILL/PAYMENT/DISCHARGE: {} {} {}".format(trans.client_billable, trans.client_payment, trans.atp_writeoff))
                print("AMOUNT & TYPE:          {} {}".format(trans.amount, trans.trans_type))
                print("BALANCE:                {}".format(balance))

            #result_object["transactions"] = Transaction.objects.filter(query).filter(client=clt).all()
            result_object["transactions"] = transactions
            result_object["credit_total"] = credit_total + past_payments
            result_object["debit_total"] = debit_total + past_bills
            result_object["balance_total"] = result_object["debit_total"] - result_object["credit_total"]
            gt_balance += result_object["debit_total"] - result_object["credit_total"]
            gt_charges += debit_total + past_bills
            gt_payments += credit_total + past_payments

            # Only need to include clients with data, I think
            # TODO! This needs some cleanup. Per Julie we only need
            # to show clients who've had transaction activity this period
            # you can probably just check if there are transactions?
            # Might need to also consider previous balance and stuff like that
            if balance != 0 or result_object["transactions"] or result_object["debit_total"] or result_object["credit_total"]:
                client_results.append(result_object)


        results_title = str(len(results)) + " Results - Client Statement Report"

        if fiscal_period > 12:            
            month_name = calendar.month_name[fiscal_period - 12]
            year_name = fiscal_year + 1
        else:
            month_name = calendar.month_name[fiscal_period]
            year_name = fiscal_year
        

        #build the report and pass it to the render machine!
        return render(request, 'report/client_statement_index.html', {
            'form':form,
            'title':'Client Statement Report',
            "client_results" : client_results,
            'results_title' : results_title,
            "agency_address" : get_agency_address(),
            "year" : year_name,
            "month" :  month_name,
            "report_id" : report_id,
            "gt_balance" : gt_balance,
            "gt_charges" : gt_charges,
            "gt_payments" : gt_payments,
            #"show_zero_balances" : show_zero_balances,
            })


@access_control("admin", "financial")
def account_transaction(request):
    if request.method == "GET" and request.GET.get('account') is None:
        form = AccountTransactionForm()
        form.fields['date_from'].initial = dt.datetime(dt.datetime.now().year, 1, 1)
        form.fields['date_to'].initial = dt.datetime.now()

        return render(request, 'report/account_transaction_index.html', {
            'form':form,
            'title':'Account Transaction Report', 
            })

    elif request.method == "POST" or request.GET.get('account'):
        query = Q()

        if request.GET.get('account'):
            date_from = None
            date_to = None
            form = AccountTransactionForm()
            form.fields['date_to'].initial = dt.datetime.now().date()

            

            account = None
            if request.GET.get("account"):
                account = get_object_or_None(Account, id=request.GET.get("account"))
                form = set_autocomplete(None, form, 'account', Account, request.GET.get('account'))

        else:
            form = AccountTransactionForm()

            date_from = request.POST.get("date_from") or None
            date_to = request.POST.get("date_to")
            account = get_object_or_None(Account, id=request.POST.get("account"))

            form = set_autocomplete(None, form, "account",  Account, account.id)


            

            if date_from:
                date_from = parse_date(date_from)
                form.fields["date_from"].initial = date_from.strftime(settings.DATE_FORMAT)
                query.add(Q(date__gte=date_from), Q.AND)

            if date_to:
                date_to = parse_date(date_to)
                form.fields["date_to"].initial=date_to.strftime(settings.DATE_FORMAT)
                query.add(Q(date__lte=date_to), Q.AND)

        query.add(Q(account=account), Q.AND)


        #sort oldest first
        #calculate blanace up to date somehow...
      
        upto_balance = get_account_balance_date(account, None, date_from)
        #loop through transactions and add up each one
        total_debit = Decimal('0.00')
        total_credit = Decimal('0.00')
        results = []
        transactions = Transaction.objects.filter(query)

        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(list(transactions.all().values(
            "date", "description", "service_date", "client__id", "client__first_name",
            "client__last_name", "client__birthdate", "vendor__id", "vendor__name", "trans_type", "amount")),
                cls=DjangoJSONEncoder)

        for t in transactions:
            #print(t.amount)
            #need to add logic for debit/credit against accountt type
            if (t.account.type == '0' and t.trans_type == '1') or (t.account.type == '1' and t.trans_type == '0'):
                upto_balance -= t.amount
            else:
                upto_balance += t.amount

            if t.trans_type == '0':#Credit
                total_credit += t.amount
            if t.trans_type == '1':#Debit
                total_debit += t.amount

            results.append([t, upto_balance])

        #build the report and pass it to the render machine!
        return render(request, 'report/account_transaction_index.html', {
            'form':form,
            'table_class' : "listview-table-no-order-no-search",
            'title':'Account Transaction Report',
            'results_title': 'Account Transactions',            
            'results' : results,
            'total_credit' : total_credit,
            'total_debit' : total_debit,
            "report_id" : report_id,
            "upto_balance" : upto_balance,
            })



@access_control("admin", "financial")
def trial_balance(request, pk=None):
    if request.method == "GET":
        form = TrialBalanceForm()
        form.fields["fiscal_year_to"].initial = get_fiscal_year()
        form.fields["fiscal_period_to"].initial = get_fiscal_period()


        return render(request, 'report/trial_balance_index.html', {
            'form': form,
            'title': 'Trial Balance Report',
        })

    elif request.method == "POST":
        form = TrialBalanceForm()
        fiscal_year_from = request.POST.get('fiscal_year_from')
        fiscal_period_from = request.POST.get("fiscal_period_from")
        fiscal_year_to = request.POST.get('fiscal_year_to')
        fiscal_period_to = request.POST.get("fiscal_period_to")



        form.fields["fiscal_year_from"].initial = fiscal_year_from
        form.fields["fiscal_period_from"].initial = fiscal_period_from
        form.fields["fiscal_year_to"].initial = fiscal_year_to
        form.fields["fiscal_period_to"].initial = fiscal_period_to

        fiscal_from = int(f"{int(fiscal_year_from)}{int(fiscal_period_from):02}")
        fiscal_to = int(f"{int(fiscal_year_to)}{int(fiscal_period_to):02}")


        #query
        #loop through all accounts, find transactions up to the date
        #add up credit and debit totals and print
        #maybe optimize the query in the future to first get a distinct list of
        #accounts within the transactions then totaling those?
        # TODO fix this so it works better, build the report bottom up insteasd you clown
        accounts = Account.objects.all()
        results = []
        grand_credit_total = Decimal('0.00')
        grand_debit_total = Decimal('0.00')
        balance_grand_total = Decimal('0.00')

        for act in accounts:
            #look up matching transactions and get debit and credit totals
            act_debit_total = Decimal('0.00')
            act_credit_total = Decimal('0.00')
            act_balance = Decimal('0.00')

            query = Q()
            query.add(Q(account=act), Q.AND)

            query.add(Q(fiscal__gte=fiscal_from), Q.AND) 
            query.add(Q(fiscal__lte=fiscal_to), Q.AND)

            query.add(Q(journal_entry__isnull=False), Q.AND)

            trans = Transaction.objects.filter(query)

            for t in trans:
                credit, debit = t.get_credit_and_debit()
                act_credit_total += credit
                act_debit_total += debit
                # updated to get_natural_amount so it matches the rest of the 
                # reports and also meets the requirements set by the folks
                # at the county
                act_balance += t.get_natural_amount()                 

            if act_credit_total != 0 or act_debit_total != 0:
                #make it look nice, dont display the zeroes
                grand_credit_total += act_credit_total
                grand_debit_total += act_debit_total
                balance_grand_total += act_balance

                # if you uncomment the code below, make sure to remove this line
                results.append(
                    [act, act_credit_total, act_debit_total, act_balance])




        # doing some quick formatting for the csv export option
        report_id = str(uuid.uuid4())
        report_results = []
        for r in results:
            #import pdb; pdb.set_trace()
            report_results.append({
                "agency_account": r[0].name,
                "county_account": r[0].county_name,
                "account_description": r[0].description,
                "account_debit": r[2],
                "account_credit": "-{}".format(r[1]),  # 2020/6/3 per Chelsea, want negatives on credits 
                "balance": r[2] - r[1],
            })

        request.session[report_id] = json.dumps(
            report_results, cls=DjangoJSONEncoder)


        #build the report and pass it to the render machine!
        return render(request, 'report/trial_balance_index.html', {
            'form': form,
            'table_class': "listview-table-no-search",
            'title': 'Trial Balance Report',
            'results_title': 'Trial Blance',
            'results': results,
            'grand_credit_total': grand_credit_total,
            'grand_debit_total': grand_debit_total,
            "balance_grand_total": balance_grand_total,
            "report_id": report_id,
        })



@access_control("admin", "financial", "caseworker")
def transaction_report(request, cc_report_id=None):   
    #print("METHOD IS: ".format(request.method)) 
    place_of_service_choices = list(client_choices.PLACE_OF_SERVICE_CODE)
    place_of_service_choices.insert(0, (None, "----------"))

    billing_code_choices = ((None, "----------"),) + accounting_choices.BILLING_CODE


    if request.method == "GET" and request.GET.get('account') is None:
        form = TransactionReportForm(request=request)
        form.fields["place_of_service"].widget.choices = place_of_service_choices
        form.fields["billing_code"].widget.choices = billing_code_choices

        if form.user_form is False:
            form.fields["fiscal_year_from"].initial = get_fiscal_year(request)
            form.fields["fiscal_year_to"].initial = get_fiscal_year(request)
            form.fields["fiscal_period_from"].initial = get_fiscal_period(request)
            form.fields["fiscal_period_to"].initial = get_fiscal_period(request)
            form.fields["journal_option"].initial = "all"

        #form.fields['date_from'].initial = dt.datetime(dt.datetime.now().year, 1, 1)
        #form.fields['date_to'].initial = dt.datetime.now()

        return render(request, 'report/transaction_report_index.html', {
            'form': form,
            'title': 'Transaction Report'
        })

    elif request.method == "POST":
        #print("METHOD IS POST")
        #print("CC REPORT IS: {}".format(cc_report_id))  

        # Generate a list of dictionary items of the form's fields
        # and values. This will be displayed at the top of the report
        # So people printing it out can see the filters they used
        filter_data = {
            "report date": timezone.now().strftime(settings.DATETIME_FORMAT)
        }
        for key, value in request.POST.items():
            if key == "csrfmiddlewaretoken" or not value:
                continue

            if key == "place_of_service":
                filter_data[key.replace('_', ' ')] = [
                    x[1] for x 
                    in client_choices.PLACE_OF_SERVICE_CODE 
                    if x[0] == value
                ][0]

            if key == "employee":
                filter_data[key] = get_object_or_None(Employee, id=value)

            else:
                filter_data[key.replace('_', ' ')] = value.replace(
                    '__', ' ').replace('_', ' ').replace("spc", "SPC")



        form = TransactionReportForm(request=request)
        form.fields["place_of_service"].widget.choices = place_of_service_choices

        datefrom = request.POST.get('date_from') or None
        dateto = request.POST.get('date_to') or None
        orderby = request.POST.get('order_by')
        groupby = request.POST.get('group_by')
        fiscal_year_from = request.POST.get("fiscal_year_from")
        fiscal_year_to = request.POST.get("fiscal_year_to")
        fiscal_period_from = request.POST.get("fiscal_period_from")
        fiscal_period_to = request.POST.get("fiscal_period_to")
        only_casenote = request.POST.get("only_casenote")
        account_type = request.POST.get("account_type")
        amount = request.POST.get("amount")
        caseworker = request.POST.get("caseworker")
        act_county = request.POST.get("act_county")
        act_dept = request.POST.get("act_dept")
        act_program = request.POST.get("act_program")
        act_object = request.POST.get("act_object")
        act_sub = request.POST.get("act_sub")
        service = request.POST.get("service")
        place_of_service = request.POST.get("place_of_service")
        billing_code = request.POST.get("billing_code")
        billing_code_startswith = request.POST.get("billing_code_startswith")
        show_billing_code = bool(request.POST.get("show_billing_code") == "on")
        atp_only = bool(request.POST.get("atp_only") == "on")
        only_batch = bool(request.POST.get("only_batch") == "on")
        journal_option = request.POST.get("journal_option")
        hide_client = bool(request.POST.get("hide_client") == "on")
        hide_vendor = bool(request.POST.get("hide_vendor") == "on")
        employee = request.POST.get("employee")
        show_cfda_number = bool(request.POST.get("show_cfda_number") == "on")
        show_target_group = bool(request.POST.get("show_target_group") == "on")
        invoice = request.POST.get("invoice")
        show_diagnosis_codes = bool(request.POST.get("show_diagnosis_codes") == "on")
        show_place_of_service = bool(request.POST.get("show_place_of_service") == "on")
        show_type_of_contact = bool(request.POST.get("show_type_of_contact") == "on")


        if datefrom:
            datefrom = parse_date(datefrom)
        if dateto:
            dateto = parse_date(dateto)


        lot = request.POST.get("lot") or None
        account = request.POST.get('account') or None
        client = request.POST.get('client') or None
        vendor = request.POST.get('vendor') or None
        fund = request.POST.get('funding_profile') or None

        form = set_autocomplete(None, form, "lot", TransactionLot, lot)
        form = set_autocomplete(None, form, 'account', Account, account)
        form = set_autocomplete(None, form, 'client', Client, client)
        form = set_autocomplete(None, form, 'vendor', Vendor, vendor)
        form = set_autocomplete(None, form, 'funding_profile', FundingProfile, fund)
        form = set_autocomplete(None, form, "service", Service, service)


        form.fields["fiscal_year_from"].initial = fiscal_year_from
        form.fields["fiscal_year_to"].initial = fiscal_year_to
        form.fields["fiscal_period_from"].initial = fiscal_period_from
        form.fields["fiscal_period_to"].initial = fiscal_period_to
        form.fields['date_from'].initial = datefrom  # service date
        form.fields['date_to'].initial = dateto  # service date
        form.fields['order_by'].initial = orderby
        form.fields['group_by'].initial = groupby
        form.fields["lot"].initial = lot
        form.fields["only_casenote"].initial = only_casenote
        form.fields["account_type"].initial = account_type
        form.fields["amount"].initial = amount
        form.fields["caseworker"].initial = caseworker
        form.fields["act_county"].initial = act_county
        form.fields["act_dept"].initial = act_dept
        form.fields["act_program"].initial = act_program
        form.fields["act_object"].initial = act_object
        form.fields["act_sub"].initial = act_sub     
        form.fields["only_batch"].initial = only_batch
        form.fields["atp_only"].initial = atp_only
        form.fields["journal_option"].initial = journal_option
        form.fields["hide_client"].initial = hide_client
        form.fields["hide_vendor"].initial = hide_vendor
        form.fields["place_of_service"].initial = place_of_service
        form.fields["billing_code"].initial = billing_code
        form.fields["billing_code_startswith"].initial = billing_code_startswith
        form.fields["employee"].initial = employee
        form.fields["show_billing_code"].initial = show_billing_code
        form.fields["show_cfda_number"].initial = show_cfda_number
        form.fields["show_target_group"].initial = show_target_group
        form.fields["invoice"].initial = invoice
        form.fields["show_diagnosis_codes"].initial = show_diagnosis_codes
        form.fields["show_place_of_service"].initial = show_place_of_service
        form.fields["show_type_of_contact"].initial = show_type_of_contact


        request.report_error = validate_report_fiscal(request) 
        if hasattr(request, "report_error") and request.report_error:
            return render(request, 'report/transaction_report_index.html', {
                'form': form,
                'title': 'Transaction Report',
            })        

        # Save the report data to be processed elsewhere,
        # Return a nice and friendly message that the user will 
        # get the report via email
        if cc_report_id is None:
            print("Creating CC_REPORT")
            create_cc_report(request)            
            return HttpResponseRedirect("/report/cc_report/watcher/")


        #build query
        query = Q()

        fiscal_from = None
        fiscal_to = None
        if fiscal_year_from and fiscal_period_from:
            fiscal_from = int(f"{int(fiscal_year_from)}{int(fiscal_period_from):02}")

        if fiscal_year_to and fiscal_period_to:
            fiscal_to = int(f"{int(fiscal_year_to)}{int(fiscal_period_to):02}")


        if fiscal_from:
            query.add(Q(fiscal__gte=fiscal_from), Q.AND)
        if fiscal_to:
            query.add(Q(fiscal__lte=fiscal_to), Q.AND)       
        if account:
            query.add(Q(account=account), Q.AND)
        if client:
            query.add(Q(client=client), Q.AND)
        if vendor:
            query.add(Q(vendor=vendor), Q.AND)
        if fund:
            query.add(Q(account__funding_profile=fund), Q.AND)
        if lot:
            query.add(Q(lot=lot), Q.AND)
        if only_casenote or caseworker:
            query.add(Q(emr_form__isnull=False), Q.AND)
        if account_type:
            query.add(Q(account__type=account_type), Q.AND)
        if amount:
            query.add(Q(amount=amount), Q.AND)
        if datefrom:
            query.add(Q(service_date__gte=datefrom), Q.AND)
        if dateto:
            query.add(Q(service_date__lte=dateto), Q.AND)        
        if act_county:
            query.add(Q(account__act_county=act_county), Q.AND)
        if act_dept:
            query.add(Q(account__act_dept=act_dept), Q.AND)
        if act_program:
            query.add(Q(account__act_program=act_program), Q.AND)
        if act_object:
            query.add(Q(account__act_object=act_object), Q.AND)
        if act_sub:
            query.add(Q(account__act_sub=act_sub), Q.AND)
        if only_batch:
            query.add(Q(lot__isnull=False), Q.AND)
        if service:
            query.add(Q(service=service), Q.AND)
        if atp_only:
            query.add(Q(atp_writeoff=True), Q.AND)
        if place_of_service:
            query.add(Q(place_of_service=place_of_service), Q.AND)
        if billing_code:
            query.add(Q(billing_code=billing_code), Q.AND)
        if billing_code_startswith:
            query.add(Q(billing_code__istartswith=billing_code_startswith), Q.AND)
        if employee:
            query.add(Q(employee=employee), Q.AND)
        if invoice:
            query.add(Q(invoice__iexact=invoice), Q.AND)


        if journal_option == "journaled":
            query.add(Q(journal_entry__isnull=False), Q.AND)
        elif journal_option == "unjournaled":
            query.add(Q(journal_entry=None), Q.AND)


        # if we're ordering by service date, can only include items`
        # with service dates (duh)
        if orderby == "service_date":
            query.add(Q(service_date__isnull=False), Q.AND)

        # if we're grouping by client or vendor, we only want to see
        # records with clients or vendors
        if groupby == "client":
            query.add(Q(client__isnull=False), Q.AND)
        elif groupby == "vendor":
            query.add(Q(vendor__isnull=False), Q.AND)
        elif groupby == "account":
            query.add(Q(account__isnull=False), Q.AND)



        # Looks up caseworker for emr notes and forms that are linked to
        # transactions
        if caseworker:
            query.add(Q(emr_form__created_by=caseworker), Q.AND)

        # This looks up caseworkers in legacy notes. I don't believe this is
        # needed any more (2023-09-26) but leaving for now just in case
        '''
        if caseworker:
            # If a caseworker is chosen, create a big OR clause
            # and add that to the existing query
            bigor = Q()
            includes = [item for item in Transaction.objects.filter(query).order_by(orderby, 'service_date', "date") if item.case_note.get_signer_user() and item.case_note.get_signer_user().id == int(caseworker)]
            for inc in includes:
                bigor.add(Q(id=inc.id), Q.OR)

            query.add(Q(bigor), Q.AND)
        '''


        # Always want to exclude cash accounts per Carly
        results = Transaction.objects.filter(
            query, 
            account__cash_account=False).order_by(
            orderby, "date", "service_date")



        report_id = str(uuid.uuid4())
        report_data = []

        for r in results:            
            batch = ""
            if r.lot:
                batch = r.lot.name

            vendor_name = ""
            vendor_number = ""
            if r.vendor:
                vendor_name = r.vendor.name
                vendor_number = r.vendor.vendor_number

            spc = ""
            if r.service:
                spc = r.service.spc_code

            first_name = ""
            last_name = ""
            if r.client:
                first_name = r.client.first_name
                last_name = r.client.last_name

            employee_first_name = ""
            employee_last_name = ""
            if r.employee:
                employee_first_name = r.employee.first_name
                employee_last_name = r.employee.last_name


            report_data.append({
                "client__first_name": first_name if not hide_client else "",
                "client__last_name": last_name if not hide_client else "",
                "account__name": r.account.name,
                "account__county_name": r.account.county_name,
                "account__description": r.account.description,
                "account__county_description": r.account.county_description,
                "account__type": r.account.type,
                "service_date": r.service_date,
                "batch": batch,
                "units": r.units,
                "unit_type": r.unit_type,
                "description": r.description,
                "invoice": r.invoice,
                "funding_profile": str(r.account.funding_profile),
                "vendor__name": vendor_name if not hide_vendor else "",
                "vendor_number": vendor_number if not hide_vendor else "",
                "employee_first_name": employee_first_name,
                "employee_last_name": employee_last_name,
                "spc_code": spc,
                "target_group": r.target_group,
                "amount": r.get_natural_amount(),  # per jackson, they want credits to show and calculate totals negative #r.get_real_amount(), # per jackson co, they want mismatched accounts/trans types to show negative
                "trans_type": r.trans_type,
                "id": r.id,
            })

        json_data = json.dumps(report_data, cls=DjangoJSONEncoder)
        request.session[report_id] = json_data


        # New orderby monthly per period summaries, by high demand!
        # Make sure to detect below and use a different template, yeeeah!!
        # Currently, this code is only grouping by account
        groups = {}
        #import pdb; pdb.set_trace()
        if orderby == "fiscal_period" or orderby == "service_date" or orderby == "date":
            # Account group object that will contain all the month_year combos
            # found depending on the query type...

            result_groups = {}
            primary_group = None
            grand_total_amount = Decimal('0.00')
            grand_total_units = Decimal('0.00')

            # building out the primary group, based on what the user picks
            if groupby == "account":
                primary_group = results.values(
                    "account", "account__name", "account__description"
                ).distinct().order_by("id")                

            elif groupby == "client":
                primary_group = results.values(
                    "client", "client__first_name", "client__last_name",
                    "client__birthdate",
                ).distinct().order_by("id")

            elif groupby == "vendor":
                primary_group = results.values(
                    "vendor", "vendor__name", "vendor__city", "vendor__state",
                    "vendor__zip", "vendor__notes"
                ).distinct().order_by("id")



            for grp in primary_group:
                if groupby == "account":
                    group_name = "{}<br>{}".format(grp["account__name"], grp["account__description"])

                elif groupby == "client":
                    client_birthdate = grp.get("client__birthdate")
                    if client_birthdate:
                        client_birthdate = client_birthdate.strftime(settings.HUMAN_DATE_FORMAT)
                    else:
                        client_birthdate = "Unknown"

                    group_name = "{}, {} - {}".format(grp["client__last_name"], grp["client__first_name"], client_birthdate)


                elif groupby == "vendor":
                    group_name = "{}".format(grp["vendor__name"])


                # if the account is already in the list, skip.
                # TODO figure out why distinct() works poorly
                if group_name not in result_groups:
                    total_amount = Decimal('0.00')
                    total_units = Decimal('0.00')

                    # depnding on the primary group type, we get the totals
                    if groupby == "account":
                        trans_total_query = Q(account=grp[groupby])                      

                    elif groupby == "client":
                        trans_total_query = Q(client=grp[groupby])                    

                    elif groupby == "vendor":
                        trans_total_query = Q(vendor=grp[groupby]) 


                    for t in results.filter(trans_total_query):
                        total_amount += t.get_natural_amount()
                        if t.units:
                            total_units += t.units

                    grand_total_amount += total_amount
                    grand_total_units += total_units

                    group_object = {
                        "group_id": grp[groupby],
                        "group_name": group_name,
                        "month_groups": [],
                        "total_amount": total_amount,
                        "total_units": total_units,
                    }
                else:
                    continue
                    #print("{} already in list, skipping".format(group_name))
                    # import pdb; pdb.set_trace()v


                # build the query to get a list of yearmonth keys depdning on
                # what the primary group is 
                if groupby == "account":
                    trans_key_query = Q(account=group_object["group_id"])                      

                elif groupby == "client":
                    trans_key_query = Q(client=group_object["group_id"])                    

                elif groupby == "vendor":
                    trans_key_query = Q(vendor=group_object["group_id"])



                # get a list of yearmonths for transactions for this account
                # will vary depending on orderby
                trans_date_keys = []


                if orderby == "service_date":                    
                    ym_results = results.filter(trans_key_query).values("service_date__month", "service_date__year").distinct()
                    get_date_key = "Service date"

                    for ym in ym_results:
                        tdk = {"year": ym["service_date__year"], "month": ym["service_date__month"]}
                        if tdk not in trans_date_keys:
                            trans_date_keys.append(tdk)
                        else:
                            continue

                elif orderby == "fiscal_period":
                    ym_results = results.filter(trans_key_query).values("fiscal_year", "fiscal_period").distinct()
                    get_date_key = "Fiscal"

                    for ym in ym_results:
                        tdk = {"year": ym["fiscal_year"], "month": ym["fiscal_period"]}
                        if tdk not in trans_date_keys:
                            trans_date_keys.append(tdk)
                        else:
                            continue



                # creating a list of transactions by month or period, depending
                # on the orderby value...
                for tdk in trans_date_keys:                    

                    if orderby == "service_date":
                        transactions = results.filter(trans_key_query, service_date__year=tdk["year"], service_date__month=tdk["month"])

                    if orderby == "fiscal_period":
                        transactions = results.filter(trans_key_query, fiscal_year=tdk["year"], fiscal_period=tdk["month"])


                    total_amount = Decimal('0.00')
                    total_units = Decimal('0.00')
                    for trans in transactions:
                        total_amount += trans.get_natural_amount()

                        if trans.units:
                            total_units += trans.units

                    month_group = {
                        "date_key": "{}{}".format(tdk["year"], tdk["month"]),
                        "get_date_key": "{} {}/{}".format(get_date_key, tdk["year"], tdk["month"]),
                        "transactions": transactions,
                        "total_amount": total_amount,
                        "total_units": total_units,
                    }

                    group_object["month_groups"].append(month_group)


                result_groups[group_name] = group_object


            new_results = []

            for item in sorted(result_groups):
                new_results.append(result_groups[item])


            if cc_report_id is not None:
                the_html = render_to_string('report/transaction_report_index2.html', {
                    'form': form,
                    'title': 'Transaction Report',
                    'results_title': 'Transaction Report results',
                    'results': new_results,
                    "grand_total_amount": grand_total_amount,
                    "grand_total_units": grand_total_units,
                    'groupby': groupby,
                    "orderby": orderby,
                    "report_id": report_id,
                    "cc_report": True,
                    "cc_report_assets": get_cc_report_assets(),
                    "hide_vendor": hide_vendor,
                    "hide_client": hide_client,
                    "filter_data": filter_data,

                    # Filters for showing addtl columns
                    "show_billing_code": show_billing_code,
                    "show_cfda_number": show_cfda_number,
                    "show_target_group": show_target_group,
                    "show_diagnosis_codes": show_diagnosis_codes,
                    "show_place_of_service": show_place_of_service,
                    "show_type_of_contact": show_type_of_contact,

                }, request)

                save_cc_report_files(
                    cc_report_id, the_html, report_data)

                return True



            else:
                return render(request, 'report/transaction_report_index2.html', {
                    'form': form,
                    'title': 'Transaction Report',
                    'results_title': 'Transaction Report results',
                    'results': new_results,
                    "grand_total_amount": grand_total_amount,
                    "grand_total_units": grand_total_units,
                    'groupby': groupby,
                    "orderby": orderby,
                    "report_id": report_id,
                    "cc_report": True,
                    "hide_vendor": hide_vendor,
                    "hide_client": hide_client,
                    "filter_data": filter_data,

                    # Filters for showing addtl columns
                    "show_billing_code": show_billing_code,
                    "show_cfda_number": show_cfda_number,
                    "show_target_group": show_target_group,
                    "show_diagnosis_codes": show_diagnosis_codes,
                    "show_place_of_service": show_place_of_service,
                    "show_type_of_contact": show_type_of_contact,
                })






        # OLD CODE BELOW, UPDATE ONE DAY. NEW CODE ABOVE
        # This is still used if they're ordering by SPC, client, or vendor

        elif groupby == "account":
            #import pdb; pdb.set_trace()
            for r in results:
                units = r.units or 0
                if r.account:
                    account_name = "{}<br>{}".format(r.account.name, r.account.description)

                else:
                    account_name = "None"               



                if account_name in groups:                        
                    groups[account_name]['total'] += r.get_natural_amount()
                    groups[account_name]["units"] += units
                    groups[account_name]['items'].append(r)                   

                else:
                    groups[account_name] = { 
                        'total': r.get_natural_amount(), 
                        'items': [r, ], 
                        "units": units,
                    }    



        elif groupby == "client":
            #import pdb; pdb.set_trace()
            for r in results:
                units = r.units or 0

                if r.client is None:
                    client_name = '(No Client)'
                else:
                    client_name = f"{r.client}"

                if client_name in groups:
                    groups[client_name]['total'] += r.get_natural_amount()
                    groups[client_name]["units"] += units
                    groups[client_name]['items'].append(r)
                else:
                    groups[client_name] = {'total': r.get_natural_amount(), 'items': [r, ], "units": units}

        elif groupby == "vendor":
            #import pdb; pdb.set_trace()
            for r in results:
                units = r.units or 0

                if r.vendor is None:
                    vendor_name = '(No Vendor)'
                else:
                    vendor_name = r.vendor.name

                if vendor_name in groups:
                    groups[vendor_name]['total'] += r.get_natural_amount()
                    groups[vendor_name]["units"] += units
                    groups[vendor_name]['items'].append(r)
                else:
                    groups[vendor_name] = {'total': r.get_natural_amount(), 'items': [r, ], "units": units}


        grand_total = Decimal('0.00')
        for g in groups:
            grand_total += groups[g]['total']

        sort_results = []
        for g in sorted(groups.keys()):
            sort_results.append([g, groups[g]])

        #print("RESULTS {}".format(sort_results))
        #import pdb; pdb.set_trace()
        if cc_report_id is not None:
            the_html = render_to_string('report/transaction_report_index.html', {
                'form': form,
                "request": request,
                'title': 'Transaction Report',
                'results_title': 'Transaction Report results',
                'results': sort_results,
                'grand_total': grand_total,
                'groupby': groupby,
                "orderby": orderby,
                "report_id": report_id,
                "cc_report_assets": get_cc_report_assets(),
                "cc_report": True,
                "hide_vendor": hide_vendor,
                "hide_client": hide_client,
                "filter_data": filter_data,

                # Filters for showing addtl columns
                "show_billing_code": show_billing_code,
                "show_cfda_number": show_cfda_number,
                "show_target_group": show_target_group,
                "show_diagnosis_codes": show_diagnosis_codes,
                "show_place_of_service": show_place_of_service,
                "show_type_of_contact": show_type_of_contact,

            }, request)

            save_cc_report_files(
                    cc_report_id, the_html, report_data)

            return True


        #build the report and pass it to the render machine!
        return render(request, 'report/transaction_report_index.html', {
            'form': form,
            'title': 'Transaction Report',
            'results_title': 'Transaction Report results',             
            'results': sort_results,
            'grand_total': grand_total,
            'groupby': groupby,
            "orderby": orderby,
            "report_id": report_id,
            "hide_vendor": hide_vendor,
            "hide_client": hide_client,
            "filter_data": filter_data,

            # Filters for showing addtl columns
            "show_billing_code": show_billing_code,
            "show_cfda_number": show_cfda_number,
            "show_target_group": show_target_group,
            "show_diagnosis_codes": show_diagnosis_codes,
            "show_place_of_service": show_place_of_service,
            "show_type_of_contact": show_type_of_contact,
        })




@access_control("admin", "financial", "caseworker")
def client_list(request):
    if request.method == "GET":
        form = ClientListForm()
        form.fields['client_gender'].initial = ''

        return render(request, 'report/client_list_index.html', {
            'form': form,
            'title': 'Client List Report',
        })

    elif request.method == "POST":
        form = ClientListForm()
        client_first = request.POST.get('client_first')
        client_last = request.POST.get('client_last')
        client_active_string = request.POST.get('client_active')
        client_birthdate_from = request.POST.get('client_birthdate_from')
        client_birthdate_to = request.POST.get('client_birthdate_to')
        client_gender = request.POST.get('client_gender')


        client_active = False
        if client_active_string == "on":
            client_active = True

        if client_birthdate_from:            
            client_birthdate_from = parse_date(client_birthdate_from)
            form.fields["client_birthdate_from"].initial = client_birthdate_from.strftime(settings.DATE_FORMAT)

        if client_birthdate_to:
            client_birthdate_to = parse_date(client_birthdate_to)
            form.fields["client_birthdate_to"].initial = client_birthdate_to.strftime(settings.DATE_FORMAT)


        form.fields["client_first"].initial = client_first
        form.fields["client_last"].initial = client_last
        form.fields["client_active"].initial = client_active             
        form.fields["client_gender"].initial = client_gender



        query = Q()
        if client_gender and client_gender.strip() != '':
            query.add(Q(gender=client_gender), Q.AND)

        if client_first:
            query.add(Q(first_name__istartswith=client_first), Q.AND)
        if client_last:
            query.add(Q(last_name__istartswith=client_last), Q.AND)
        if client_birthdate_from:
            query.add(Q(birthdate__gte=client_birthdate_from), Q.AND)
        if client_birthdate_to:
            query.add(Q(birthdate__lte=client_birthdate_to), Q.AND)


        query.add(Q(active=client_active), Q.AND)


        #query
        clients = Client.objects.filter(query)

        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(list(clients.all().values(
            "id", "active", "first_name", "last_name", "birthdate",
            "gender", "race", "hispanic", "parent_guardian_name",
            "starting_balance", "legacy_client_number")),
                cls=DjangoJSONEncoder)



        results_title = str(len(clients)) + " Results - Client List Report"

        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/client_list_index.html", {
                'form': form,
                'title': 'Client List Report',
                'results': clients,
                'results_title': results_title,
                'table_class': "listview-table-no-order-no-search",
                "report_id": report_id,
                "pdf_report_id": pdf_report_id,
            })
        

        #build the report and pass it to the render machine!
        return render(request, 'report/client_list_index.html', {
            'form': form,
            'title': 'Client Report',
            'results': clients,
            'results_title': results_title,
            "report_id": report_id,
            "pdf_report_id": pdf_report_id,
        })






@access_control("admin", "financial", "caseworker")
def casenote_time(request):
    if request.method == "GET":
        form = CaseNoteTimeForm()

        form.fields["date"].initial = dt.datetime.now().strftime(settings.DATE_FORMAT)


        return render(request, 'report/casenote_time_index.html', {
            'form': form,
        })

    elif request.method == "POST":
        query = Q()
        query.add(Q(is_open=False), Q.AND)

        form = CaseNoteTimeForm()
        date = request.POST.get("date")
        show_inactive = bool(request.POST.get("show_inactive") == "on")

        form.fields["show_inactive"].initial = show_inactive


        if date:
            date = parse_date(date)
            form.fields["date"].initial = date.strftime(settings.DATE_FORMAT)
            query.add(Q(date=date), Q.AND)


        # If this is being run by an admin, show ALL worker's case notes
        # If this is being run by a caseworker, only show thier own time

        # find the case notes for the time period.
        # If we care about what user 'owns' them, we'll have to filter
        # because the worker who created the note may not be the one
        # who signs it.


        # first, get a list of users
        # loop through users
        # get list of case notes for selected date
        # sum up the minutes and divide by 60

        if request.user.userconfig.user_type == "1":
            users = User.objects.all().exclude(is_staff=True)
        else:
            users = User.objects.filter(id=request.user.id)


        if show_inactive is False:
            users = users.exclude(is_active=False)


        results = []
        for user in users:
            # find all the case notes for this user for the specced time period
            # Was using list comprehension but the line was way too long!
            notes = []
            for note in CaseNote.objects.filter(query):
                if note.get_signer_user() == user:
                    notes.append(note)
            

            total_minutes = Decimal('0.00')
            for note in notes:
                total_minutes += note.minutes
                if note.hours:
                    total_minutes += (note.hours * 60)


            results.append({
                "user": str(user.userconfig),
                "total_minutes": total_minutes,
                "total_hours": Decimal(total_minutes) / Decimal('60.00'),
                "hours_and_minutes": str(dt.timedelta(minutes=int(total_minutes)))[:-3],
                "hours": math.floor(dt.timedelta(minutes=100).seconds / 3600),
                "minutes": total_minutes % 60,
            })



        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(
            results, cls=DjangoJSONEncoder)


        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/casenote_time_index.html", {
                'form': form,
                'results': results,
                'table_class': "listview-table",
                "report_id": report_id,
                "pdf_report_id": pdf_report_id,
            })


        #build the report and pass it to the render machine!
        return render(request, 'report/casenote_time_index.html', {
            'form': form,
            'results': results,
            "report_id": report_id,
            "pdf_report_id": pdf_report_id,
        })





@access_control("admin", "financial", "caseworker")
def emr_case_note_report_my_open(request):
    return emr_case_note_report(request, pk=None, my_open=True)




@access_control("admin", "caseworker")
def emr_case_note_report(request, pk=None, my_open=False):
    """ Case Note report allows filtering and viewing of case notes using
        varios filters including the worker
    """

    total_minutes = 0

    if request.method == "GET":
        form = EmrCaseNotesForm()

        if pk:
            results = EmrCaseNoteForm.objects.filter(id=pk)
        elif my_open is True:
            results = EmrCaseNoteForm.objects.filter(
                created_by=request.user, is_open=True)

            total_minutes = results.aggregate(Sum("total_minutes"))["total_minutes__sum"]

        else:
            results = None


        # This is needed because this report can be run with a GET request
        # if the user clicks the "view in report" button directly on
        # the case note.
        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/emr_case_note_report.html", {
                "title": "EMR Case Notes Report",
                "form": form,
                "results": results,
                "my_open": my_open,
                "total_minutes": total_minutes,
                "pdf_report_id": pdf_report_id,
            })

        return render(request, "report/emr_case_note_report.html", {
            "title": "Case Notes Report",
            "form": form,
            "results": results,
            "my_open": my_open,
            "total_minutes": total_minutes,
            "pdf_report_id": pdf_report_id,
        })

    else:
        form = EmrCaseNotesForm(request.POST)
        date_from = request.POST.get("date_from")
        date_to = request.POST.get("date_to")
        staff = request.POST.get("staff")
        client = request.POST.get("client")
        episode_type = request.POST.get("episode_type")
        note_status = request.POST.get("note_status")
        ct_collateral = bool(request.POST.get("ct_collateral") == "on")
        ct_direct = bool(request.POST.get("ct_direct") == "on")
        ct_face = bool(request.POST.get("ct_face") == "on")
        ct_home = bool(request.POST.get("ct_home") == "on")
        ct_record = bool(request.POST.get("ct_record") == "on")
        ct_other = bool(request.POST.get("ct_other") == "on")
        crisis_toc = request.POST.get("crisis_toc")
        billable = request.POST.get("billable")

        query = Q()

        if date_from:
            query.add(Q(date__gte=date_from), Q.AND)
        if date_to:
            query.add(Q(date__lte=date_to), Q.AND)
        if client:
            query.add(Q(emrepisode__client=client), Q.AND)
        if staff:
            sub_q = Q()
            sub_q.add(Q(created_by__userconfig=staff), Q.OR)
            sub_q.add(Q(signed_by__userconfig=staff), Q.OR)

            query.add(sub_q, Q.AND)
        if episode_type:
            query.add(Q(emrepisode__episode_type=episode_type), Q.AND)

        if note_status == "closed":
            query.add(Q(is_open=False), Q.AND)
        elif note_status == "open":
            query.add(Q(is_open=True), Q.AND)

        if ct_collateral:
            query.add(Q(ct_collateral=ct_collateral), Q.AND)

        if ct_direct:
            query.add(Q(ct_direct=ct_direct), Q.AND)

        if ct_face:
            query.add(Q(ct_face=ct_face), Q.AND)

        if ct_home:
            query.add(Q(ct_home=ct_home), Q.AND)

        if ct_record:
            query.add(Q(ct_record=ct_record), Q.AND)

        if ct_other:
            query.add(Q(ct_other=ct_other), Q.AND)

        if crisis_toc:
            query.add(Q(crisis_type_of_contact=crisis_toc), Q.AND)

        if billable:
            # If billable is set to Any it's value is none and no filter
            # will be applied. Only billable and unbillable will show up here
            form.fields["billable"].initial = billable

            if billable == "billable":
                query.add(~Q(billing_code=None), Q.AND)
                query.add(~Q(billing_code="unbillable"), Q.AND)
            elif billable == "unbillable":
                query.add(Q(billing_code="unbillable"), Q.AND)



        results = EmrCaseNoteForm.objects.filter(query).order_by("-date")

        total_minutes = results.aggregate(Sum("total_minutes"))["total_minutes__sum"]




        client_record = None
        if client:
            client_record = get_object_or_None(Client, id=client)

        # As of (2022-08-19) users have no need to export this in CSV

        #report_id = str(uuid.uuid4())
        #request.session[report_id] = json.dumps(
        #    results, cls=DjangoJSONEncoder)

        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/emr_case_note_report.html", {
                "title": "EMR Case Notes Report",
                "form": form,
                "results": results,
                "my_open": my_open,
                "client": client_record,
                "total_minutes": total_minutes,
                "pdf_report_id": pdf_report_id,
            })




        return render(request, "report/emr_case_note_report.html", {
            "title": "EMR Case Notes Report",
            "form": form,
            "results": results,
            "my_open": my_open,
            "pdf_report_id": pdf_report_id,
            "total_minutes": total_minutes,
            "client": client_record,
        })







@access_control("admin", "financial", "caseworker")
def emr_case_note_time(request):
    if request.method == "GET":
        form = EmrCaseNoteTimeForm()

        form.fields["date_from"].initial = dt.datetime.now().date().strftime(
            settings.DATE_FORMAT)

        form.fields["date_to"].initial = dt.datetime.now().date().strftime(
            settings.DATE_FORMAT)


        return render(request, 'report/emr_case_note_time_report.html', {
            "title": "EMR Case Note Time Report",
            'form': form,


        })

    elif request.method == "POST":
        query = Q()
        # I don't believe the workers only want to see closed
        #query.add(Q(is_open=False), Q.AND)

        form = EmrCaseNoteTimeForm()
        date_from = request.POST.get("date_from")
        date_to = request.POST.get("date_to")
        client = request.POST.get("client")
        created_by = request.POST.get("created_by")
        billable = request.POST.get("billable")
        #show_inactive = bool(request.POST.get("show_inactive") == "on")

        #form.fields["show_inactive"].initial = show_inactive
        set_autocomplete(None, form, "client", Client, client)


        if date_from:
            date_from = parse_date(date_from)
            form.fields["date_from"].initial = date_from.strftime(settings.DATE_FORMAT)
            query.add(Q(date__gte=date_from), Q.AND)

        if date_to:
            date_to = parse_date(date_to)
            form.fields["date_to"].initial = date_to.strftime(settings.DATE_FORMAT)
            query.add(Q(date__lte=date_to), Q.AND)

        if client:
            query.add(Q(emrepisode__client=client), Q.AND)

        if created_by:
            query.add(Q(created_by=created_by), Q.AND)
            form.fields["created_by"].initial = created_by

        if billable:
            # If billable is set to Any it's value is none and no filter
            # will be applied. Only billable and unbillable will show up here
            form.fields["billable"].initial = billable

            if billable == "billable":
                query.add(~Q(billing_code=None), Q.AND)
                query.add(~Q(billing_code="unbillable"), Q.AND)
            elif billable == "unbillable":
                query.add(Q(billing_code="unbillable"), Q.AND)

        # If this is being run by an admin, show ALL worker's case notes
        # If this is being run by a caseworker, only show thier own time

        # find the case notes for the time period.
        # If we care about what user 'owns' them, we'll have to filter
        # because the worker who created the note may not be the one
        # who signs it.

        # first, get a list of users
        # loop through users
        # get list of case notes for selected date
        # sum up the minutes and divide by 60

        if created_by:
            users = User.objects.filter(id=created_by)
        else:
            users = User.objects.all().exclude(is_staff=True)


        #if show_inactive is False:
        #    users = users.exclude(is_active=False)


        # For this report we need to group up the results by program name
        results = []        
        program_totals = {}
        grand_total_minutes = 0

        for user in users:
            total_minutes = 0
            programs = {}
            # find all the case notes for this user for the specced time period
            notes = EmrCaseNoteForm.objects.filter(query, created_by=user)


            for note in notes:
                epi_type = note.emrepisode.episode_type

                # Update the global counts
                if epi_type in program_totals:
                    program_totals[epi_type] += note.total_minutes
                else:
                    program_totals[epi_type] = note.total_minutes

                # Update the counts for the per-user totals by program
                if epi_type in programs:
                    programs[epi_type] += note.total_minutes
                else:
                    programs[epi_type] = note.total_minutes


                total_minutes += note.total_minutes

                grand_total_minutes += note.total_minutes        


            results.append({
                "title": "EMR Case Note Time Report",
                "user": str(user.userconfig),
                "total_minutes": total_minutes,
                "programs": programs,
            })


        # Append the program totals to the results???

        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(
            results, cls=DjangoJSONEncoder)


        pdf_report_id = str(uuid.uuid4())
        request.session[pdf_report_id] = render_to_string(
            "report/emr_case_note_time_report.html", {
                "title": "EMR Case Note Time Report",
                'form': form,
                'results': results,
                'table_class': "listview-table",
                "report_id": report_id,
                "pdf_report_id": pdf_report_id,
                "program_totals": program_totals,
                "grand_total_minutes": grand_total_minutes,
            })

        return render(request, 'report/emr_case_note_time_report.html', {
            "title": "EMR Case Note Time Report",
            'form': form,
            'results': results,
            'table_class': "listview-table",
            "report_id": report_id,
            "pdf_report_id": pdf_report_id,
            "program_totals": program_totals,
            "grand_total_minutes": grand_total_minutes,
        })



@access_control("admin", "financial", "caseworker")
def download_as_csv(request):
    report_name = request.GET.get("report_name")
    report_id = request.GET.get("report_id")
    data = json.loads(request.session[report_id])
    filename = "{}_{}_Nova.csv".format(report_name, dt.datetime.now())
    response = HttpResponse(content_type="text/csv")
    response["Content-Disposition"] = 'attachment; filename="{}"'.format(filename)

    csv_data = generate_csv_from_json(data)    

    if isinstance(csv_data, list):
        writer = csv.writer(response)
        writer.writerow(csv_data[0].keys())
        for row in csv_data:
            writer.writerow(row.values())

    # There's a chance the report will just be a single record as a dict
    # and not as a list of dicts like above. An example is the emr form report
    elif isinstance(csv_data, dict):
        writer = csv.DictWriter(response, fieldnames=csv_data.keys())
        writer.writeheader()
        writer.writerow(csv_data)

    return response


@access_control("admin", "financial", "caseworker")
def download_as_pdf(request):
    try:
        report_name = request.GET.get("report_name")
        report_id = request.GET.get("report_id")
        form_name = request.GET.get("formname")
    except KeyError:
        return HttpResponseRedirect("/report/")

    html = request.session[report_id]

    filename = "{}_{}_Nova.pdf".format(report_name, dt.datetime.now())

    # Before the html gets sent to a html -> pdf renderer, we need to clean it
    # up and make it look like a report vs a website
    soup = BeautifulSoup(html, "html.parser")

    soup.find(id="header-nav").decompose()
    soup.find(id="footer").decompose()


    for tag in soup.find_all(class_="print-hide"):
        tag.decompose()

    # Deleting tags and links, these will be added to the html string using
    # the get_cc_report_assets. Main reason we do this is because the
    # tool that converts html to css doesn't follow relative links
    for tag in soup.find_all("link"):
        tag.decompose()

    for tag in soup.find_all("script"):
        tag.decompose()


    # Because the PDF generator doesn't seem to care about media queries
    # there needs to be some CSS added. Any CSS that's in a media query
    # will get ignored so, for example, the widths in nova_responsive.css
    # must get added in here.
    extra_css = """

        .w5{width:5%} .w10{width:10%} .w14{width:14%} .w15{width:15%} .w16{width:16.66%}
        .w20{width:20%} .w25{width:25%} .w30{width:30%} .w33{width:33.3%}
        .w35{width:35%} .w40{width:40%} .w45{width:45%}
        .w50{width:50% !important;} .w55{width:55%} .w60{width:60%}
        .w65{width:65%} .w70{width:70%} .w75{width:75%}
        .w80{width:80%} .w85{width:85%} .w90{width:90%}
        .w95{width:95%} .w100{width:100%}

    """


    # Not sure why adding extra_css to this doesn't work.
    soup.body["style"] = ""
    #import pdb; pdb.set_trace()


    soup.insert(1, BeautifulSoup(f"""
        <!DOCTYPE html>
        <style>
            {extra_css}
        </style>
        {get_pdf_report_assets()}

        <meta charset="utf-8">



        <div class='container'>
            <h5><strong>{get_app_config("agency_name")}</strong></h5>
            <h5>{report_name.replace("_"," ")}</h5>            
            <p>Generated by Nova on {request.timestamp.strftime(settings.DATETIME_FORMAT)}</p>
            <span style="position: absolute; top:0; right:0;">NOVA</span>
            <hr>
        </div>

    """, features="lxml"))

    # Parse in the form_section.js file. We will use that data to figure
    # out where to insert the header HTML
    form_section_file = open("static/form_section.json", "r")

    form_sections = form_section_file.read()
    form_section_file.close()

    form_section_json = json.loads(form_sections)
    # Select the passed form name and loop through its keys and values
    # and update soup. If there is no formname passed then just render
    # like normal without adding form sections

    if form_name is not None:
        for key, value in form_section_json[form_name].items():
            #print("KEY:  ", key)
            #print("Value:", value)

            if not value["title"]:
                print(f"No title found for {key}. A title is required for the form section to display in the PDF export")
                # Was asked to not show subtitles in the reports. This may change
                # there is code below to handle adding the subtitle into an
                # h4 in the pdf view
                continue

            if "reportHide" in value and value["reportHide"] is True:
                # This is not supposed to show in the report. Likely there's
                # a report_header_row in use
                continue

            if key == "report_header_row":
                # This is a special key that is used to insert something
                # to the top of the report, it goes right after the emr_form_header
                # element
                header_ele = soup.find("div", {"id": "emr_form_header"})

                header_title = soup.new_tag("h4")
                header_title.string = value["title"]
                header_ele.insert_before(header_title)

            else:
                field_ele = soup.find("div", {"id": key})
                if field_ele:
                    new_section_title = soup.new_tag("h4")
                    if value["title"]:
                        new_section_title.string = value["title"]
                    elif value["subtitle"]:
                        new_section_title.string = value["subtitle"]

                    new_section_title.append(soup.new_tag("hr"))
                    new_section_wrapper = soup.new_tag("div")
                    new_section_wrapper["style"] = "width: 100%; clear: both;"
                    new_section_wrapper.append(new_section_title)
                    field_ele.insert_before(new_section_wrapper)

                else:
                    print(f"No element found with the ID of {key}")






    # Make a PDF from this HTML string, put into memory, then serve that as
    # a file to the user

    soup_string = str(soup).replace("’", "'").replace("“", '"').replace("”", '"')


    pdf_data = pdfkit.from_string(
        soup_string,
        options={ 

        },
    )

    response = HttpResponse(pdf_data, content_type="application/pdf")
    response["Content-Disposition"] = 'attachment; filename="{}"'.format(filename)
    response["is_pdf"] = True


    return response











