"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import datetime

from django.db import models
from django.utils import timezone



# Create your models here.
class ReportBase(models.Model):
    Text = models.CharField(blank=True, null=True, max_length=100)


class CCReport(models.Model):
    status_choices = (
        ("submitted", "Submitted"),
        ("processing", "Processing"),
        ("in_progress", "In progress"),
        ("complete", "Complete"),
        ("error", "Error"),
        ("empty", "Empty"),
        )
    
    userconfig = models.ForeignKey('config.UserConfig', null=True, on_delete=models.SET_NULL)    
    report_name = models.CharField(null=True, max_length=255)
    status = models.CharField(null=False, max_length=255, choices=status_choices)
    submitted = models.DateTimeField(null=True)
    start = models.DateTimeField(null=True)
    end = models.DateTimeField(null=True)
    post = models.TextField(null=False)
    path = models.TextField(null=True)


    def get_csv_filename(self):
        return "{}_{}_Nova.csv".format(
            self.report_name.capitalize(), str(self.id))

    def get_html_filename(self):
        return "{}_{}_Nova.html".format(
            self.report_name.capitalize(), str(self.id))


    def get_report_name(self):
        return self.report_name.capitalize().replace("_", " ")
        

    def get_runtime(self):
        if self.end is None or self.start is None:
            return None

        else:
            return round((self.end-self.start).total_seconds())


    def get_elapsed(self):
        if self.end is None:
            elapsed = timezone.now() - self.submitted
        else:
            elapsed = self.end - self.submitted

        #elapsed = str(elapsed).split(":")
        #hours = int(elapsed[0])
        #minutes = int(elapsed[1]) + (hours * 60)
        #seconds = str(round(float(elapsed[2]))).rjust(2,'0')


        return round(elapsed.total_seconds())


    def get_total_seconds(self):
        if self.end is None:
            return (timezone.now() - self.submitted).total_seconds()

        return (self.end - self.submitted).total_seconds()

