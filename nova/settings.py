"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import os
import sys
from pathlib import Path

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# SECURITY WARNING: keep the secret key used in production secret!
with open("{}/django_nova_secret.key".format(Path(BASE_DIR).parents[0]), "r") as file:
    SECRET_KEY = file.read().strip()

with open("{}/email_backend.key".format(Path(BASE_DIR).parents[0]), "r") as file:
    EMAIL_BACKEND_KEY = file.read().strip()

with open("{}/django_nova_db.key".format(Path(BASE_DIR).parents[0]), "r") as file:
    DB_KEY = file.read().strip()

# For debugging purposes. a dev's email to receive error or status emails
# from Nova. Just an email address: user@example.com
with open("{}/debug_dev_email.txt".format(Path(BASE_DIR).parents[0]), "r") as file:
    DEBUG_DEV_EMAIL = file.read().strip()


# Nova system email. This is the email address Nova will use to send out
# automated emails
with open("{}/nova_system_email.txt".format(Path(BASE_DIR).parents[0]), "r") as file:
    NOVA_SYSTEM_EMAIL = file.read().strip()


# Email address that Nova will use to email users about their completed reports
# Email should be in this format: 'Nova Reports <noreply@example.com>'
    REPORTS_EMAIL = f"Nova Reports <{NOVA_SYSTEM_EMAIL}>"

# This is the sender email account that will send out 403 incident reports to the
# people listed in RECEIVER_403_EMAILS
# Email should be in this format: 'Nova 403 Incident <noreply@example.com>'
SENDER_403_EMAIL = f"Nova 403 Incident <{NOVA_SYSTEM_EMAIL}>"

# List of emails that will receive 403 reports. One email per line
with open("{}/receiver_403_emails.txt".format(Path(BASE_DIR).parents[0]), "r") as file:
    RECEIVER_403_EMAILS = [line for line in file.read().splitlines() if line]

# This is the sender email account that will send out error reports to the
# people listed in RECEIVER_ERROR_EMAILS
# Email should be in this format: 'Nova Error <noreply@example.com>'
SENDER_ERROR_EMAIL = f"Nova Error <{NOVA_SYSTEM_EMAIL}>"

# List of emails that will receive 403 reports. One email per line
with open("{}/receiver_error_emails.txt".format(Path(BASE_DIR).parents[0]), "r") as file:
    RECEIVER_ERROR_EMAILS = [line for line in file.read().splitlines() if line]





# 1 host per line
with open("{}/allowed_hosts.txt".format(Path(BASE_DIR).parents[0]), "r") as file:
    ALLOWED_HOSTS = [line for line in file.read().splitlines() if line]


# Enables deubg if a debug file is found
try:
    with open("{}/debug.txt".format(Path(BASE_DIR).parents[0]), "r") as file:
        if file.read().strip() == "debug":
            DEBUG = True
        else:
            DEBUG = False
except FileNotFoundError:
    DEBUG = False



TESTING = 'test' in sys.argv[1:]
if TESTING:
    print('=========================')
    print('In TEST Mode - Disabling Migrations')
    print('=========================')

    class DisableMigrations(object):

        def __contains__(self, item):
            return True

        def __getitem__(self, item):
            return None

    MIGRATION_MODULES = DisableMigrations()




SITE_ID = 1


# Application definition

INSTALLED_APPS = [
    "django.contrib.sites",
    'django.contrib.admin',    
    'django.contrib.auth',
    "django.forms",
    'accounting',
    'client',
    'config',
    'report',
    'sacwis',
    "external",
    'silk',

    #Plugins
    'annoying',

    'django.contrib.humanize',

    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    # Must be first if using server-side caching
    #"django.middleware.cache.UpdateCacheMiddleware",
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',

    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    # Tells the client browser and the server to not use caching.
    'middleware.DisableClientSideCachingMiddleware',

    # This adds a timestamp to the request which is used in reports and
    # other areas that get printed often
    "middleware.RequestTimestampMiddleware",

    # Must be last, or at least after UpdateCaceMiddleware if using
    # server-side caching
    #"django.middleware.cache.FetchFromCacheMiddleware",
    #'silk.middleware.SilkyMiddleware',
]

ROOT_URLCONF = 'nova.urls'

FORM_RENDERER = 'django.forms.renderers.TemplatesSetting'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates', './templates', '/srv/nova/templates', BASE_DIR + '/templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'libraries': {
                'nova_tags': 'tags',
            },
        },
    },
]



AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
]




'''
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
        #"LOCATION": "unique-snowflake",
    }
}

CACHE_MIDDLEWARE_ALIAS = "default"
CACHE_MIDDLEWARE_SECONDS = 6000
CACHE_MIDDLEWARE_KEY_PREFIX = ''
'''

WSGI_APPLICATION = 'nova.wsgi.application'

X_FRAME_OPTIONS = "SAMEORIGIN"


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'django_nova',
        'USER': 'django_nova_user',
        'PASSWORD': DB_KEY,
        'HOST': 'localhost',
        'PORT': '5432',
        'TEST': {
            "NAME": "test_nova"
        }
    }

}


# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'loggers': {
        'django': {
            'level': 'ERROR',
        },
    },
}









# email settings for passowrd reset emails
# TODO move this to a config file or appconfig settings
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.mailgun.org'
EMAIL_HOST_USER = NOVA_SYSTEM_EMAIL
EMAIL_HOST_PASSWORD = EMAIL_BACKEND_KEY
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = f"Nova <{NOVA_SYSTEM_EMAIL}>"






DEFAULT_AUTO_FIELD = 'django.db.models.AutoField' 


# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

# TODO move to a config file or appconfig setting
TIME_ZONE = 'America/Chicago'

USE_I18N = True


USE_TZ = True

DATE_FORMAT = "%Y-%m-%d"
HUMAN_DATE_FORMAT = "%m/%d/%Y"
TIME_FORMAT = '%I:%M:%S %p'
TIME_FORMAT_24 = "%H:%M:%S"
DATETIME_FORMAT = f"{HUMAN_DATE_FORMAT} {TIME_FORMAT_24}"

DATE_INPUT_FORMATS = ['%m/%d/%Y', ]
TIME_INPUT_FORMATS = ["%H:%M", "%I:%M %p", ]

APPEND_SLASH = True

SESSION_EXPIRE_AT_BROWSER_CLOSE = False
SESSION_SAVE_EVERY_REQUEST = True
SESSION_COOKIE_AGE = 10800  # 3 Hours
if DEBUG:
    SESSION_COOKIE_SECURE = False
    CSRF_COOKIE_SECURE = False
else:
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/
#PROJECT_DIR=os.path.join(os.path.dirname(__file__), os.pardir)
#STATIC_ROOT= os.path.join(PROJECT_DIR,'static/')

STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

LOGIN_REDIRECT_URL = "/loginsort/"  # TODO convert this to lowercase sitewide
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'



