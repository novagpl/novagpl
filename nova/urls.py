"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


"""nova URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include
from django.urls import path, re_path
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
import views

admin.site.site_header = 'NOVA Administration'

handler404 = views.handler404
handler403 = views.handler403
handler500 = views.handler500

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    #path('silk/', include('silk.urls', namespace='silk')),
    re_path(r'^login/', auth_views.LoginView.as_view(), name='login'),
    path('', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='logged_out.html', next_page=None), name='logout'),
    path('logged_out/', auth_views.LogoutView.as_view(template_name="logged_out.html", next_page=None), name="logged_out"),
    re_path(r'^loginsort/', views.loginSort, name="Login Sort"),
    re_path(r'^LoginSort/', views.loginSort, name="Login Sort"),  # For some reason our login is leading us to this capitalized re_path. still need to figure out why
    re_path(r'^thanks/', views.thanks, name="Thanks"),

    path('client/', include('client.urls')),
    path('accounting/', include('accounting.urls')),
    path('config/', include('config.urls')),
    path('report/', include('report.urls')),
    path('external/', include('external.urls')),
    path('sacwis/', include('sacwis.urls')),

    #path("script.js", views.script_view, name="script_view"),

    #path("ajax_get_session_time/", views.ajax_get_session_time, name="ajax_get_session_time"),

    path("test500/", views.test500, name="test500"),
    path("testnotify/<str:message>/<int:user_id>/", views.testnotify, name="testnotify"),

    path("robots.txt", TemplateView.as_view(template_name="robots.txt", content_type="text/plain")),

    path("license", TemplateView.as_view(template_name="LICENSE", content_type="text/plain")),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
