"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import calendar
import datetime as dt
import math
import json
from decimal import Decimal, getcontext, ROUND_HALF_UP
from dateutil.parser import parse
getcontext().prec = 28

#from ClientInfo import clientinfo_choices as choices
import accounting.accounting_choices as choices
import client.client_choices as client_choices
from config.models import UserConfig
from django.conf import settings
from django.core import serializers
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models
from django.db.models import F, Q, Sum
from django.forms import DateInput, ModelForm
from django.utils import timezone
from annoying.functions import get_object_or_None

from utils import notify, sign_data, get_client_ip


# Create your models here.
class AbilityToPay(models.Model):
    client = models.ForeignKey("client.Client", on_delete=models.CASCADE)
    month = models.IntegerField(choices=choices.MONTH)
    year = models.IntegerField()
    amount = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return "{} - {} {} ${}".format(
            self.client.name(),
            calendar.month_name[int(self.month)],
            self.year,
            self.amount,
        )



class FundingProfile(models.Model):
    active = models.BooleanField(default=True)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        if self.description is not None:
            return "{} {}".format(self.name, self.description)
        else:
            return self.name

    def get_auto_name(self):
        return str(self)


class Account(models.Model):
    active = models.BooleanField(default=True)
    # TODO fix this and rename to account_type
    type = models.CharField(max_length=1, choices=choices.ACCOUNT_TYPE, default=1)
    name = models.CharField(max_length=100, verbose_name="Agency Account")
    county_name = models.CharField(max_length=100, null=True, blank=True, verbose_name='County Account')
    description = models.CharField(max_length=100, null=True, blank=True)
    county_description = models.CharField(max_length=100, null=True, blank=True, verbose_name='County Description')
    #not needed i think payment_account = models.BooleanField(default=False, verbose_name='Payment')
    funding_profile = models.ForeignKey(FundingProfile, on_delete=models.CASCADE, verbose_name="Funding Profile", related_name="funding_profile", null=True, blank=True)
    cfda_number = models.CharField(max_length=255, null=True, blank=True, verbose_name="CFDA Number")
    ma_account = models.BooleanField(default=False, verbose_name="M.A.")
    writeoff_account = models.BooleanField(default=False, verbose_name="Writeoff")
    cash_account = models.BooleanField(default=False, verbose_name="Cash")
    act_county = models.CharField(max_length=255, null=True, blank=True)
    act_dept = models.CharField(max_length=255, null=True, blank=True)
    act_program = models.CharField(max_length=255, null=True, blank=True)
    act_object = models.CharField(max_length=255, null=True, blank=True)
    act_sub = models.CharField(max_length=255, null=True, blank=True)


    def __str__(self):
        if self.description is not None:
            return "{}, {}".format(self.name, self.description)
        else:
            return "{}".format(self.name)


    def get_auto_name(self):
        auto_name = ""
        if self.description is not None:
            auto_name = "{}, {}".format(self.name, self.description)
        else:
            auto_name = self.name

        if self.active is False:
            auto_name = f"(INACT) {auto_name}"

        return auto_name

    def save(self, *args, **kwargs):
        # check if this account has the 5 divisions
        # the first 4 divisions are hyphens the last is a period

        hyphens = self.name.split("-")
        #import pdb; pdb.set_trace()
        if len(hyphens) == 4:
            # this is compatible!!
            #print("Account {} is compatible with the 5 divs".format(self.name))
            self.act_county = hyphens[0]
            self.act_dept = hyphens[1]
            self.act_program = hyphens[2]
            self.act_object = hyphens[3].split(".")[0]

            if len(hyphens[3].split(".")) == 2:
                self.act_sub = hyphens[3].split(".")[1]
            else:
                self.act_sub = None
                

            #print("Act:  {}".format(self.name))
            #print("Divs: {}-{}-{}-{}.{}".format(self.act_county, self.act_dept, self.act_program, self.act_object, self.act_sub))
            
            if self.act_sub:
                computed = "{}-{}-{}-{}.{}".format(self.act_county, self.act_dept, self.act_program, self.act_object, self.act_sub)
            else:
                computed = "{}-{}-{}-{}".format(self.act_county, self.act_dept, self.act_program, self.act_object)

            if computed != self.name:
                print("PROBLEM!!!")
                print(self.name)
                print(computed)
                #raise Exception("WTF")


        super(Account, self).save(*args, **kwargs)




class Vendor(models.Model):
    active = models.BooleanField(default=True)
    name = models.CharField(max_length=255, null=True)
    vendor_number = models.CharField(max_length=100, null=False, blank=False, verbose_name="Vendor number")
    wisconsin_provider_id = models.CharField(max_length=100, null=True, blank=True, verbose_name="WI Provider ID")
    national_provider_id = models.CharField(max_length=10, null=True, blank=True, verbose_name="National Provider ID")
    street = models.CharField(max_length=100, null=True, blank=False)
    suite = models.CharField(blank=True, max_length=100, verbose_name="Suite/room")
    city = models.CharField(max_length=50, null=True, blank=False)
    state = models.CharField(max_length=2, choices=client_choices.STATE, default="WI", null=True, blank=False)
    zipcode = models.CharField(max_length=10, null=True)
    notes = models.TextField(null=True, blank=True)
    etf_payment = models.BooleanField(default=False, verbose_name="ETF payments")

    sac_provider_id = models.IntegerField(null=True, blank=True, verbose_name="eWiSACWIS provider ID")


    def get_auto_name(self):
        return str(self)


    def __str__(self):
        fields = [self.name]
        if self.city:
            fields.append(self.city)

        if self.street:
            fields.append(self.street)



        if self.vendor_number:
            fields.append(self.vendor_number)

        if self.notes:
            fields.append(self.notes.split("\n")[0])

        return ", ".join(fields)






class Rate(models.Model):
    account = models.ForeignKey('Account', related_name='Rate_Account', on_delete=models.PROTECT)
    active = models.BooleanField(default=True)
    name = models.CharField(max_length=100)
    amount = models.DecimalField(max_digits=20, decimal_places=2)
    interval = models.CharField(max_length=1, choices=choices.RATE_INTERVAL, default=0)
    year = models.IntegerField(null=False, blank=False)
    clinic_preadmit_rate = models.BooleanField(default=False, verbose_name="Clinic pre-admit")
    clts_rate = models.BooleanField(default=False, verbose_name="CLTS")
    clts_preadmit_rate = models.BooleanField(default=False, verbose_name="CLTS pre-admit")
    crisis_rate = models.BooleanField(default=False)
    cst_rate = models.BooleanField(default=False, verbose_name="CST")
    randi_rate = models.BooleanField(default=False, verbose_name="R & I")
    tcm_rate = models.BooleanField(default=False, verbose_name="TCM")
    b3_rate = models.BooleanField(default=False, verbose_name="Birth to 3")
    b3_preadmit_rate = models.BooleanField(default=False, verbose_name="Birth to 3 pre-admit")


    def __str__(self):
        return "{} ({}/{})".format(self.name, self.amount, self.get_interval_display().lower())


    def save(self, *args, **kwargs):
        # If this has any of the rate bits set, make sure to un-tick
        # any existing ones.
        if self.clinic_preadmit_rate is True:
            Rate.objects.filter(
                clinic_preadmit_rate=True, year=self.year).exclude(id=self.id).update(clinic_preadmit_rate=False)

        if self.clts_rate is True:
            Rate.objects.filter(clts_rate=True, year=self.year).exclude(id=self.id).update(clts_rate=False)

        if self.clts_preadmit_rate is True:
            Rate.objects.filter(clts_preadmit_rate=True, year=self.year).exclude(id=self.id).update(clts_preadmit_rate=False)

        if self.crisis_rate is True:
            Rate.objects.filter(crisis_rate=True, year=self.year).exclude(id=self.id).update(crisis_rate=False)

        if self.cst_rate is True:
            Rate.objects.filter(cst_rate=True, year=self.year).exclude(id=self.id).update(cst_rate=False)

        if self.randi_rate is True:
            Rate.objects.filter(randi_rate=True, year=self.year).exclude(id=self.id).update(randi_rate=False)

        if self.tcm_rate is True:
            Rate.objects.filter(tcm_rate=True, year=self.year).exclude(id=self.id).update(tcm_rate=False)

        if self.b3_rate is True:
            Rate.objects.filter(b3_rate=True, year=self.year).exclude(id=self.id).update(b3_rate=False)

        if self.b3_preadmit_rate is True:
            Rate.objects.filter(b3_preadmit_rate=True, year=self.year).exclude(id=self.id).update(b3_preadmit_rate=False)


        super().save(*args, **kwargs)












class JournalEntry(models.Model):
    payrolldist = models.ForeignKey("accounting.PayrollDist", null=True, blank=True, on_delete=models.PROTECT)
    employee = models.ForeignKey("accounting.Employee", null=True, blank=True, on_delete=models.PROTECT)
    date = models.DateField(null=False, blank=False)
    fiscal_year = models.IntegerField(null=True)
    fiscal_period = models.IntegerField(null=True)
    fiscal = models.IntegerField(null=True)
    description = models.CharField(max_length=200)
    verified = models.BooleanField(default=False)
    last_exported = models.DateTimeField(null=True, blank=True)
    supervisor_reviewed = models.BooleanField(default=False, verbose_name="Supervisor reviewed")
    supervisor_review_date = models.DateTimeField(null=True, verbose_name="Supervisor review date")
    supervisor_user = models.ForeignKey(User, null=True, on_delete=models.PROTECT)


    def __str__(self):
        return "{} - {}".format(self.description, self.date.strftime(settings.HUMAN_DATE_FORMAT))


    def save(self, *args, **kwargs):
        current_self = get_object_or_None(JournalEntry, id=self.id)
        if current_self:
            if current_self.verified is False and self.verified is True:
                # Getting verified from an unverified state sends an alert
                # to any JE reviewers
                je_supers = UserConfig.objects.filter(
                    super_je=True, active=True)

                notify_html = f'''
                    Journal entry '{self.description}' has recently been marked as verified.
                    <a href="/accounting/journal_entry/{self.id}/">Click here to review it.</a>
                '''

                notify(je_supers, notify_html)




        if self.fiscal_year and self.fiscal_period:
            self.fiscal = int(f"{self.fiscal_year}{self.fiscal_period:02}")

        super(JournalEntry, self).save(*args, **kwargs)


    def get_je_number(self):
        return "{}{}".format(self.date.year, self.id)

    def get_export_name(self, file_extension):
        return "{}_{}_{}.{}".format(
            self.get_je_number(),
            self.description.replace(" ", "_"),
            dt.datetime.now().strftime("%Y-%m-%d_%H_%M_%S").replace(" ", "_"),
            file_extension)


    def get_balanced(self):
        credit, debit = self.get_credit_debit()
        if credit != debit:
            return "[UNBALANCED] "
        return ''

    def default_url(self):
        return "/accounting/journal_entry/{}".format(self.id)

    def transaction_count(self):
        return Transaction.objects.filter(journal_entry=self).count()


    def get_credit_debit(self):
        credit = Decimal('0.00')
        debit = Decimal('0.00')
        credit = Transaction.objects.filter(journal_entry=self, trans_type='0').aggregate(Sum('amount')).get('amount__sum', 0.00)
        debit = Transaction.objects.filter(journal_entry=self, trans_type='1').aggregate(Sum('amount')).get('amount__sum', 0.00)

        if credit is None:
            credit = Decimal('0.00')
        if debit is None:
            debit = Decimal('0.00')
            
        return credit, debit


    def get_color(self):
        #do a lookup of all transactions under this journal entry, calculate against self.Amount and get a percentage
        credit, debit = self.get_credit_debit()

        if credit != debit:
            return "red"

        return ""

    def has_transactions(self):
        # Simply return a true or false if this journal entry has any
        # transactions linking to it
        return Transaction.objects.filter(journal_entry_id=self.id).exists()


    def get_transaction_files(self):
        # Returns a list of transaction file objects that are linked to
        # transactions which in turn are linked to this journal entry
        transfiles = TransactionFile.objects.filter(transaction__journal_entry__id=self.id)

        return transfiles

        #import pdb; pdb.set_trace()






def upload_journal_file(instance, filename):
    return f"journal_files/{instance.id}/{filename}"


class JournalFile(models.Model):
    journal_entry = models.ForeignKey(JournalEntry, on_delete=models.PROTECT)
    date = models.DateField()
    uploaded_by = models.ForeignKey(User, related_name="journal_file_uploaded_by", on_delete=models.PROTECT, null=True)
    description = models.TextField(null=True, blank=True)
    jf_file = models.FileField(null=True, blank=True, upload_to=upload_journal_file, verbose_name="File")
    upload_date = models.DateTimeField(null=True, blank=True)


    def __str__(self):
        if self.description:
            return f"{self.date.strftime('%m/%d/%Y')} {self.journal_entry} '{self.description}' ({self.id})"
        else:
            return f"{self.date.strftime('%m/%d/%Y')} {self.journal_entry} ({self.id})"


    def get_filename(self):
        # Returns a string of just the file's name, not any directories
        return self.jf_file.name.split("/")[-1]

    def get_download_link(self):
        url = f"/accounting/journal_file/{self.id}/{self.jf_file.name}"
        return url







class TransactionLot(models.Model):
    batch_type = models.CharField(
        max_length=255,
        null=True,
        blank=False,
        verbose_name="Batch type",
        choices=choices.BATCH_TYPE)
    active = models.BooleanField(default=True)
    year = models.IntegerField()
    name = models.CharField(max_length=255, null=False)
    description = models.CharField(max_length=1024, null=True, blank=True)

    def __str__(self):
        if self.description is not None and str(self.description).strip() != '':
            return "{} {} ({})".format(self.year, self.name, self.description)
        else:
            return "{} {}".format(self.year, self.name)

    def get_auto_name(self):
        return self.__str__()




class Transaction(models.Model):
    journal_entry = models.ForeignKey(JournalEntry, blank=True, null=True, on_delete=models.PROTECT)
    trans_type = models.CharField(max_length=1, choices=choices.TRANS_TYPE, null=False, blank=False, verbose_name="Type")
    employee = models.ForeignKey("accounting.Employee", null=True, blank=True, on_delete=models.PROTECT)
    client = models.ForeignKey('client.Client', blank=True, null=True, on_delete=models.PROTECT)
    case_note = models.ForeignKey('client.CaseNote', null=True, blank=True, on_delete=models.PROTECT)
    emr_form = models.ForeignKey("client.EmrForm", null=True, blank=True, on_delete=models.PROTECT)
    vendor = models.ForeignKey(Vendor, blank=True, null=True, on_delete=models.PROTECT)
    account = models.ForeignKey(Account, related_name="Account", null=True, on_delete=models.PROTECT)
    service = models.ForeignKey("client.Service", related_name="Service", null=True, blank=True, on_delete=models.PROTECT)
    lot = models.ForeignKey(TransactionLot, related_name="transaction_lot", verbose_name="Transaction Batch", null=True, blank=True, on_delete=models.PROTECT)
    authorization = models.ForeignKey("accounting.Authorization", null=True, blank=True, on_delete=models.PROTECT)
    date = models.DateField()
    service_date = models.DateField(null=True, blank=True)
    place_of_service = models.CharField(max_length=255, null=True, choices=client_choices.PLACE_OF_SERVICE_CODE)
    invoice = models.CharField(max_length=256, null=True, blank=True, verbose_name="Invoice/Month")
    description = models.CharField(max_length=200, null=True, blank=True)
    amount = models.DecimalField(max_digits=20, decimal_places=2, null=True, validators=[MinValueValidator(Decimal('0.00'))])
    unit_type = models.CharField(max_length=2, choices=choices.TRANS_UNIT, null=True, blank=True, verbose_name="Unit Type")
    units = models.DecimalField(null=True, blank=True, verbose_name="Units", max_digits=6, decimal_places=2)
    fiscal_year = models.IntegerField(null=True)
    fiscal_period = models.IntegerField(null=True)
    fiscal = models.IntegerField(null=True)  # this is a combination of the year and period so we can properly filter in the db without going insane
    client_billable = models.BooleanField(default=False)
    client_payment = models.BooleanField(default=False)
    atp_writeoff = models.BooleanField(default=False)    
    target_group = models.CharField(max_length=3, choices=choices.FULL_TARGET_GROUP, null=True, blank=True)
    billing_code = models.CharField(max_length=255, null=True, blank=True, choices=choices.BILLING_CODE)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


    def recalculate_amount(self):
        # This function can be used to recalculate the amount of a transaction
        # the need for this function arose when a rate was entered incorrectly
        # and we needed a way to update a bunch of transactions amounts to
        # match the new rate
        # This should only work on NON JOURNALED transactions.
        if self.journal_entry is not None:
            raise Exception("Cannot recalculate transactions that are journaled")

        rate = self.emr_form.get_rate()

        new_amount = rate.amount * self.emr_form.units
        if new_amount == self.amount:
            print(f"{self.id} no change in amount of {self.amount}")
        else:
            print(f"{self.id} Updating amount from {self.amount} to {new_amount}")
            self.amount = new_amount

            self.save()




    def __str__(self):
        return "{} {} ({})".format(self.amount, self.description, self.get_trans_type_display())


    def get_auto_name(self):
        return self.__str__()

    def get_url(self):
        if self.journal_entry:
            return "/accounting/journal_entry/{}/".format(self.journal_entry.id)
        elif self.client_payment:
            return "/accounting/client_payment/{}/".format(self.id)
        elif self.employee:
            return "/accounting/payroll_transaction/{}/".format(self.id)
        else:
            return "/accounting/transaction/{}/".format(self.id)

    def save(self, *args, **kwargs):
        if self.fiscal_year and self.fiscal_period:
            self.fiscal = int(f"{self.fiscal_year}{self.fiscal_period:02}")

        # Use the proper rounding for currency and keep it to 2 decimal spaces
        # The amounts should be the same, just with the correct amount of
        # decimals. If they are not the same, throw an error so it can be
        # investigated
        rounded = self.amount.quantize(Decimal("0.00"), rounding=ROUND_HALF_UP)
        if rounded != self.amount:
            if self.id:
                the_id = self.id
            else:
                the_id = 'No ID'
            raise Exception(f"Rounding issue on transaction '{the_id}' Amount: {self.amount}  Rounded: {rounded}")

        self.amount = rounded

        super(Transaction, self).save(*args, **kwargs)






    def get_color(self):
        # TODO maybe check if its a positive or negative and swap color accordingly?
        # Revenue = 0, Expense = 1
        # Revenue is credit, Expense is debit
        if self.account is not None:
            if self.account.type == '0':
                #money comin in
                if self.trans_type == '0':
                    return "ac-green"
                else:
                    return "ac-red"

            elif self.account.type == '1':
                if self.trans_type == '1':
                    return 'ac-red'
                else:
                    return 'ac-green'

        else:
            return ''


    def get_balance(self):
        credit, debit = self.get_credit_and_debit()
        return credit - debit

    def get_offset_amount(self):
        # This is used in the financial export disbursement export to
        # the county screen. When the user wants to create offset
        # amounts by clicking on checkboxes, use this amount to
        # calculate the amount of the offset transaction being made
        # it's just get_balance but inversed. 
        
        return self.get_balance() * Decimal('-1.00')



    def get_description(self):
        if self.get_balance() != 0:
            return "[UNBALANCED] " + self.description
        return self.description


    '''
    def get_real_amount(self):
        # This is deprecated, use get_natural_amount()
        #returns the "real amount" of the transaction
        #if the account doesn't match the credit or debit, return the negative Number
        
        if not self.account:
            return self.amount

        if self.account.type == '0' and self.trans_type == '1':
            return self.amount * Decimal(-1.00)
        elif self.account.type == '1' and self.trans_type == '0':
            return self.amount * Decimal(-1.00)
        else:
            return self.amount
    '''


    def get_natural_amount(self):
        """
        # Attempts to return a more 'transaction natural' format for the amount.
        # Basically folks wanted to see credits as negative and debits and 
        # positive in their reports and CSV exports regardless of what
        # account type is used...
        """
        if self.trans_type == "0":
            return self.amount * Decimal('-1.00')
        else:
            return self.amount




    def get_credit_and_debit(self):
        # Does this even need to exist anymore?
        # TODO (2020/6/4 figure out if this def is needed anymore)
        # credit to revenue(0) = credit (0)
        # credit to expense(1) = debit (1)
        credit = Decimal('0.00')
        debit = Decimal('0.00')

        if self.trans_type == "0":
            credit = self.amount
            debit = 0
        elif self.trans_type == "1":
            credit = 0
            debit = self.amount
        else:
            raise Exception("Unknown transaction type")
        return credit, debit




def upload_transaction_file(instance, filename):
    return f"transaction_files/{instance.id}/{filename}"


class TransactionFile(models.Model):
    transaction = models.ForeignKey(Transaction, on_delete=models.PROTECT)
    date = models.DateField()
    uploaded_by = models.ForeignKey(User, related_name="transaction_file_uploaded_by", on_delete=models.PROTECT, null=True)
    description = models.TextField(null=True, blank=True)
    trans_file = models.FileField(null=True, blank=True, upload_to=upload_transaction_file, verbose_name="File")
    upload_date = models.DateTimeField(null=True, blank=True)


    def __str__(self):
        if self.description:
            return f"{self.date.strftime('%m/%d/%Y')} {self.transaction} '{self.description}' ({self.id})"
        else:
            return f"{self.date.strftime('%m/%d/%Y')} {self.transaction} ({self.id})"


    def get_filename(self):
        # Returns a string of just the file's name, not any directories
        return self.trans_file.name.split("/")[-1]

    def get_download_link(self):
        url = f"/accounting/transaction_file/{self.id}/{self.jf_file.name}"
        return url






class Budget(models.Model):
    active = models.BooleanField(default=True)
    name = models.CharField(max_length=1024)
    year = models.IntegerField()
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name


class BudgetItem(models.Model):
    budget = models.ForeignKey(Budget, on_delete=models.CASCADE, null=False, related_name='budget_item_set')
    account = models.ForeignKey(Account, null=True, blank=True, related_name="budgetaccount", on_delete=models.SET_NULL)
    amount = models.DecimalField(max_digits=10, decimal_places=2,)

    def get_usage(self, period=None):
        #do a lookup of all transactions under this account, calculate against self.Amount and get a percentage
        #add up all debits or credits for this expense or revenue account
        #add debits subtract credits (revenue)
        if period:
            budget_trans = Transaction.objects.filter(account=self.account, fiscal_year=self.budget.year, fiscal_period__lte=period)
        else:
            budget_trans = Transaction.objects.filter(account=self.account, fiscal_year=self.budget.year)
        #calculate transaction totals for this budget
        trans_total = Decimal('0.00')
        for x in budget_trans:
            # 2020-06-17 this was updated to calculate differently.
            # folks at the county don't care what the natrual account is
            # they only care what the transaction type is. So, credits
            # should be negative and debits should be positive, regardless
            # of what account they're under.
            trans_total += x.get_natural_amount()

        if self.amount == 0:
            return 0, 0, 0
        return (trans_total, self.amount, round(trans_total / self.amount * 100, 1))

    def get_usage_fancy(self):
        total, limit, percent = self.get_usage()
        return f"{percent} ({total:,}/{limit:,})"

    def get_usage_percent_raw(self):
        total, limit, percent = self.get_usage()
        return percent * 1000



class Contract(models.Model):
    # These need to be matched to ContractReportForm
    contract_types = (
        ("general", "General"),
        ("provider", "Provider"),
    )

    contract_type = models.CharField(max_length=255, null=True, choices=contract_types)
    year = models.IntegerField()
    vendor = models.ForeignKey(Vendor, on_delete=models.PROTECT)
    #service = models.ForeignKey('client.Service', blank=True, null=True, on_delete=models.PROTECT)
    #services = models.ManyToManyField("client.Service", related_name="services", blank=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    notes = models.TextField(blank=True, null=True)


    def __str__(self):
        fields = [str(self.year), str(self.vendor)]
        if self.notes:
            fields.append(self.notes.split("\n")[0])

        fields.append("${}".format(self.amount))

        return ", ".join(fields)

    def get_usage(self):
        # New contract system looks at transactions filtered by the contract
        # year, vendor, and its services
        #import pdb; pdb.set_trace()
        contract_services = [x.service for x in self.contractservice_set.all()]
        contract_trans = Transaction.objects.filter(
            fiscal_year=self.year,
            vendor=self.vendor,
            service__in=contract_services,
        )

        total = Decimal('0.00')
        for trans in contract_trans:
            total += trans.get_natural_amount()

        if self.amount == 0:
            return (total, self.amount, 0)
        else:
            return (total, self.amount, round(total / self.amount * 100, 1))


    def get_usage_fancy(self):
        total, limit, percent = self.get_usage()
        return f"{percent}% ({total:,}/{limit:,})"


    def get_services_html(self):
        services = []
        for service in self.contractservice_set.all().order_by("id"):
            services.append(service.get_code() or '')

        return "<br>" + "<br>".join(services)


    def get_services(self):
        services = []
        for service in self.contractservice_set.all().order_by("id"):
            services.append(service.get_code() or '')

        return services


    def get_service_amounts(self):
        service_amounts = []
        for service in self.contractservice_set.all().order_by("id"):
            service_amounts.append(str(service.amount) or '')

        return service_amounts


    def get_service_utilizations(self):
        service_utils = []

        for service in self.contractservice_set.all().order_by("id"):
            service_utils.append(service.get_usage())

        return service_utils






class ContractService(models.Model):
    contract = models.ForeignKey("accounting.Contract", on_delete=models.PROTECT)
    service = models.ForeignKey("client.Service", on_delete=models.PROTECT, null=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    rate = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=10)
    unit = models.CharField(max_length=255, null=True, blank=True, verbose_name="Unit")
    description = models.TextField(null=True, blank=True, verbose_name="Description")

    def __str__(self):
        return f"{self.service} ({self.amount})"


    def get_usage(self):
        # Loop through all the transactions under this provider for the
        # contract's year that use this service.
        contract_trans = Transaction.objects.filter(
            fiscal_year=self.contract.year,
            service=self.service,
            vendor=self.contract.vendor,
        )

        total = Decimal('0.00')
        for trans in contract_trans:
            total += trans.get_natural_amount()

        return (total, self.amount, round(total / self.amount * 100, 1))


    def get_usage_fancy(self):
        total, limit, percent = self.get_usage()
        return f"{percent}% ({total:,}/{limit:,})"


    def get_code(self):
        if not self.service:
            return None
        elif self.service.spc_code:
            return self.service.spc_code
        elif self.service.name:
            return self.service.name
        else:
            return None






class Authorization(models.Model):
    wic_choices = (
        ("admin", "Admin"),
        ("breastfeeding_promo_support", "Breastfeeding promotion support"),
        ("client_services", "Client services"),
        ("nutrition_education", "Nutrition education"),
    )

    iii_b_choices = (
        ("admin", "Admin"),
        ("adult_day_care", "Adult day care"),
        ("assisted_transport", "Assisted transportation"),
        ("case_mgmt", "Case management"),
        ("chore", "Chore"),
        ("homemaker", "Homemaker"),
        ("legal_assistance", "Legal/benefit assistance"),
        ("nutrition_counsel", "Nutrition counsel"),
        ("nutrition_education", "Nutrition education"),
        ("personal_care", "Personal care"),
        ("public_info", "Public information"),
    )

    iii_e_choices = (
        ("cc_60_plus", "Caregiver counseling (60+ and early-onset dementia)"),
        ("cc_18_and_disabled_19_59", "Caregiver counseling (18 under and disabled 19-59)"),
        ("ct_60_plus", "Caregiver training (60+ and early-onset dementia)"),
        ("ct_18_and_disabled_19_59", "Caregiver training (18 under and disabled 19-59)"),
        ("cs_60_plus", "Caregiver support group (60+ and early-onset dementia)"),
        ("cs_18_and_disabled_19_59", "Caregiver support group (18 under and disabled 19-59)"),
        ("cm_60_plus", "Case management (60+ and early-onset dementia)"),
        ("cm_18_and_disabled_19_59", "Case management (18 under and disabled 19-59)"),
        ("respite_care", "In home (60+ and early-onset dementia)"),
        ("fb_overnight_60_plus", "Facility based - overnight (60+ and early-onset dementia)"),
        ("fb_overnight_18_and_disabled_19_59", "Facility based - overnight (18 under and disabled 19-59)"),
        ("ss_60_plus", "Supplemental services (60+ and early-onset dementia)"),
        ("ss_18_and_disabled_19_59", "Supplemental services (18 under and disabled 19-59)"),
        ("aa_60_plus", "Access assistance (60+ and early-onset dementia)"),
        ("aa_18_and_disabled_19_59", "Access assistance (18 under and disabled 19-59)"),
        ("is_60_plus", "Information services (60+ and early-onset dementia)"),
        ("is_18_and_disabled_19_59", "Information services (18 under and disabled 19-59)"),
        ("public_info", "Public information"),
    )

    cst_choices = (
        ("ads", "Advertising: outreach materials"),
        ("con_fam", "Consumer/family: coord. com. consulting fee"),
        ("training_ccf_reg_adult", "Training: CCF registration (adults)"),
        ("training_ccf_reg_youth", "Training: CCF registration (youth)"),
        ("training_mh_edu", "Training: MH education/awareness"),
        ("travel_mileage", "Travel: Mileage"),
        ("travel_meals", "Travel: Meals"),
        ("travel_lodging", "Travel: Lodging"),
        ("supplies_office", "Supplies: Office supplies"),

    )

    elder_abuse_choices = (
        ("sc_assess", "SC: Assessment"),
        ("sc_case_mgmt", "SC: Case management"),
        ("sc_crisis_int", "SC: Crisis intervention"),
        ("his_supp_home_care", "HIS: Supportive home care"),
        ("his_personal_care", "HIS: Personal care"),
        ("his_home_meals", "HIS: Home delivered meals"),
        ("ls_court_intake", "LS: Court intake & studies"),
        ("ls_protective_placement", "LS: Protective placement/services"),
        ("ls_temp_restrain", "LS: Temporary restraining order/injunction"),
        ("adapt_equip", "Adaptive equipment"),
        ("adapt_aids", "Adaptive aids"),
        ("home_mod", "Home modification"),
        ("med_services", "Medical/health care services"),
        ("med_equip", "Medical/health care equipment"),
        ("med_access", "Medical/health care access"),
        ("energy_assist", "Energy/heating assistance"),
        ("energy_fuel_bill_repair", "Energy/heating fuel, utilities bill, furnace repair"),
        ("per_response", "Personal emergency response systems/Lifeline"),
        ("transport_assist", "Transportation assistance"),
        ("vehicle_repair", "Vehicle modifications/repair"),
        ("house_assist", "Housing assistance"),
        ("move_assist", "Moving assistance"),
        ("relocate_assist", "Relocation assistance"),
        ("res_care", "Residential care (adult family home, CBRF)"),
        ("respite_care", "Respite/adult day care"),
        ("counseling_services", "Counseling/therapeutic resources"),
        ("domestic_abuse_efforts", "Domestic abuse program efforts"),
        ("i_team", "I-Team"),
        ("training", "Training (registration, meals, lodging, mileage)"),
        ("outreach", "Outreach"),
        ("other", "Other: Guardianship/PP client resources"),
    )


    caseworker = models.ForeignKey("config.UserConfig", on_delete=models.PROTECT, null=True, blank=True)
    contract = models.ForeignKey("accounting.Contract", on_delete=models.PROTECT, null=True, blank=True)
    vendor = models.ForeignKey("accounting.Vendor", on_delete=models.PROTECT, null=False)
    service = models.ForeignKey("client.Service", on_delete=models.PROTECT, null=False)
    client = models.ForeignKey("client.Client", on_delete=models.PROTECT, null=True, blank=True)
    account = models.ForeignKey("accounting.Account", on_delete=models.PROTECT, null=True, blank=True)

    rate = models.DecimalField(null=True, decimal_places=2, max_digits=10)
    amount = models.DecimalField(max_digits=10, decimal_places=2, null=False, blank=False)
    date_from = models.DateField(null=False, blank=False, verbose_name="Valid from")
    date_to = models.DateField(null=False, blank=False, verbose_name="Valid to")
    unit_type = models.CharField(null=True, blank=True, max_length=2, choices=choices.TRANS_UNIT)
    units = models.DecimalField(null=True, blank=True, decimal_places=2, max_digits=6)
    target_group = models.CharField(max_length=3, choices=choices.FULL_TARGET_GROUP, null=True, blank=True)
    notes = models.TextField(null=True, blank=True, verbose_name="Notes")
    wic_cat = models.CharField(max_length=255, null=True, blank=True, choices=wic_choices, verbose_name="WIC categories")
    iii_b_cat = models.CharField(max_length=255, null=True, blank=True, choices=iii_b_choices, verbose_name="III B categories")
    iii_e_cat = models.CharField(max_length=255, null=True, blank=True, choices=iii_e_choices, verbose_name="III E categories")
    cst_cat = models.CharField(max_length=255, null=True, blank=True, choices=cst_choices, verbose_name="CST categories")
    elder_abuse_cat = models.CharField(max_length=255, null=True, blank=True, choices=elder_abuse_choices, verbose_name="Elder abuse categories")
    service_date_from = models.DateField(null=True, blank=True, verbose_name="Service date from")
    service_date_to = models.DateField(null=True, blank=True, verbose_name="Service date to")
    invoice_number = models.CharField(max_length=255, null=True, blank=True, verbose_name="Invoice number")
    week = models.CharField(max_length=255, null=True, blank=True, verbose_name="Week")

    # Sign-off fields for the mgmt and users. At this point in time, we do not
    # need cryptographic signatures. Just need to track who signed off and
    # when
    # Staff signature
    user_pwd_provided = models.BooleanField(default=False)
    user_signed_by = models.ForeignKey(User, related_name="auth_user_signed_by", on_delete=models.PROTECT, null=True, blank=True)
    user_signed_date = models.DateTimeField(null=True)

    # Supervisor signature
    super_pwd_provided = models.BooleanField(default=False)
    super_signed_by = models.ForeignKey(User, related_name="auth_super_signed_by", on_delete=models.PROTECT, null=True, blank=True)
    super_signed_date = models.DateTimeField(null=True)

    history = models.JSONField(null=True, default=list)





    def __str__(self):
        return "{}-{} {} {} {}".format(
            self.date_from, self.date_to, self.vendor, self.client, self.service)

    def reset_signatures(self, user_and_super=False):
        if user_and_super:
            self.super_pwd_provided = False
            self.super_signed_by = None
            self.super_signed_date = None

        self.user_pwd_provided = False
        self.user_signed_by = None
        self.user_signed_date = None

        self.save()





    def save_history(self, action, request):
        print("Saving model history")

        if action not in ["create", "save", "user_sign", "super_sign", "user_reopen", "super_reopen"]:
            raise Exception(f"Action of '{action}' is not supported")


        # 1. Serializing the forms and then parsing them back into a python object
        #   so they can be manipulated. if someone knows a better way to do this,
        #   please let me know
        serial_data = json.loads(serializers.serialize("json", (self, )))[0]


        new_history = {
            "fields": {}
        }

        # These fields will be excluded from being saved in the form's history
        # fields like history (do need to track itself) the signature field
        # we really just want the history of the data in the form.
        exclude_fields = ["history", ]

        # 2. Adding the serial_data's fields to the
        # history data
        for key, value in serial_data["fields"].items():
            if key in exclude_fields:
                continue
            if key not in new_history["fields"]:
                new_history["fields"][key] = value


        # 3. Incorporating extra metadata.
        new_history["user_id"] = request.user.id
        new_history["user_type"] = request.user.userconfig.user_type
        new_history["user_username"] = request.user.username
        new_history["user_email"] = request.user.email
        new_history["action"] = action
        new_history["action_at"] = timezone.now().isoformat()


        if self.history:
            self.history.append(new_history)
        else:
            self.history = [new_history, ]

        #print(f"Model history:\n{json.dumps(self.history)}")
        self.save()




    def get_history(self):
        # Returns history of the item in a simple list. Not sure how or why
        # this is better than just parsing out the json in the template?
        history_data = []

        if self.history:
            for item in self.history:
                history_data.append({
                    "action": item["action"],
                    "action_at": parse(item["action_at"]),
                    "user": get_object_or_None(User, id=item["user_id"]),
                })

        return history_data



    def generate_signature(self, request):
        sig_data = {
            "auth_id": self.id,
            "auth_number": self.get_auth_number(),
            "user": request.user.id,
            "user_email": request.user.email,
            "user_type": request.user.userconfig.user_type,
            "user_name": request.user.userconfig.full_name,
            "user_credentials": request.user.userconfig.credentials,
            "ip_address": get_client_ip(request),
            "useragent": request.headers.get("user-agent"),
        }

        signed_data = sign_data(sig_data)

        return signed_data




    def get_auth_number(self):
        return "{}{}".format(self.date_from.year, self.id)


    def get_auto_name(self):
        if self.contract:
            return "{}, {}, ${}".format(self.get_auth_number(), self.vendor, self.contract.amount)
        else:
            return "{}, {}".format(self.get_auth_number(), self.vendor)



    def get_usage(self):
        # Because the auth is right on the transaction now, we dont
        # need to do anything other that find transactions
        # that are linked to this auth

        auth_trans = Transaction.objects.filter(authorization=self)
        trans_total = Decimal('0.00')
        for trans in auth_trans:
            trans_total += trans.get_natural_amount()

        return (trans_total, self.amount, round(trans_total / self.amount * 100, 1))

    def get_usage_fancy(self):
        total, limit, percent = self.get_usage()
        return f"{percent}% ({total:,}/{limit:,})"




def upload_auth_file(instance, filename):
    return f"auth_files/{instance.id}/{filename}"


class AuthFile(models.Model):
    authorization = models.ForeignKey(Authorization, on_delete=models.PROTECT)
    date = models.DateField()
    uploaded_by = models.ForeignKey(User, related_name="auth_file_uploaded_by", on_delete=models.PROTECT, null=True)
    description = models.TextField(null=True, blank=True)
    af_file = models.FileField(null=True, blank=True, upload_to=upload_auth_file, verbose_name="File")
    upload_date = models.DateTimeField(null=True, blank=True)


    def __str__(self):
        if self.description:
            return f"{self.date.strftime('%m/%d/%Y')} {self.authorization} '{self.description}' ({self.id})"
        else:
            return f"{self.date.strftime('%m/%d/%Y')} {self.authorization} ({self.id})"


    def get_filename(self):
        # Returns a string of just the file's name, not any directories
        return self.af_file.name.split("/")[-1]

    def get_download_link(self):
        url = f"/accounting/auth_file/{self.id}/{self.af_file.name}"
        return url







class Employee(models.Model):
    """
        Configuration model for the employee record
    """
    active = models.BooleanField(default=True)
    first_name = models.CharField(max_length=255, null=False, blank=False)
    last_name = models.CharField(max_length=255, null=False, blank=False)
    home_cash_account = models.ForeignKey(Account, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return "{}, {}".format(self.last_name, self.first_name)

    def get_comp_percent(self):
        return EmployeeComp.objects.filter(employee=self).aggregate(Sum("percent"))["percent__sum"]


    def get_auto_name(self):
        return f"{self.last_name}, {self.first_name}"


class EmployeeComp(models.Model):
    """
        Configuration model for the employee compensation configuration
    """
    employee = models.ForeignKey("accounting.Employee", null=False, blank=False, on_delete=models.CASCADE)
    comp_type = models.CharField(max_length=2, null=False, blank=False, choices=choices.EMPLOYEE_COMP_TYPE)
    account = models.ForeignKey(Account, null=True, blank=False, on_delete=models.SET_NULL)
    percent = models.DecimalField(null=False, blank=False, decimal_places=2, max_digits=5)

    def __str__(self):
        return "{} {}, {}".format(self.employee, self.comp_type, self.percent)


class PayrollDist(models.Model):
    date_from = models.DateField(null=True, blank=False)
    date_to = models.DateField(null=True, blank=False)
    paycheck_date = models.DateField(null=True, blank=False)

    class Meta:
        verbose_name = "Payroll Distribution"

    def __str__(self):
        return "{} - {} payroll".format(
            self.date_from.strftime(settings.HUMAN_DATE_FORMAT),
            self.date_to.strftime(settings.HUMAN_DATE_FORMAT))

    def get_fmt_name(self):
        return "{} - {} Payroll".format(
            self.date_to.strftime("%b %d, %Y"), 
            self.date_to.strftime("%b %d, %Y")
        )








