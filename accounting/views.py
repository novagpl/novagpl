"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import os
import calendar
import datetime as dt
import json
import uuid
from decimal import Decimal, getcontext
getcontext().prec = 28

import accounting.accounting_choices
import annoying.functions
from annoying.functions import get_object_or_None
from client.models import Client
from django.conf import settings
from django.core.paginator import Paginator
from django.core.exceptions import PermissionDenied
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import F, Q, Sum, When
from django.forms import (HiddenInput, inlineformset_factory,
                          modelformset_factory)
from django.http import HttpResponse, HttpResponseRedirect, FileResponse
from django.shortcuts import get_object_or_404, render
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.views import generic
from django.views.generic.edit import FormView
from utils import (compare_signatures, delete_audit, delete_confirm,
                   docx_replace, format_errors, generate_export_acs_csv,
                   generate_export_acs_text, get_account_balance_date,
                   get_alerts, get_app_config, get_client_bills_or_payments,
                   get_client_ip, get_fiscal_period, get_fiscal_year,
                   get_fiscal_year_and_period, get_logged_in, get_model,
                   get_pps_epi, link_models, render_form, save_audit,
                   save_confirm, set_autocomplete, sign_data, verify_signature,
                   view_audit, access_control)

from .forms import (AccountForm, AuthorizationForm, BudgetForm, BudgetItemForm,
                    ClientPaymentForm, ContractForm, FundingProfileForm,
                    JournalEntryForm, JournalTransForm, LotForm, PayrollForm,
                    PayrollJeForm, RateForm, TransactionFilterForm,
                    TransactionForm, PayrollTransactionForm,
                    ContractServiceForm, JournalFileForm, TransactionFileForm,
                    NewAuthForm, AuthForm, AuthFileForm)

from .models import (AbilityToPay, Account, Authorization, Budget, BudgetItem,
                     Contract, Employee, EmployeeComp, FundingProfile,
                     JournalEntry, PayrollDist, Rate, Transaction,
                     TransactionLot, Vendor, ContractService, JournalFile,
                     TransactionFile, AuthFile)



from client.models import Service


# Create your views here.











@get_logged_in
def accounting_index(request):
    data = []

    data.append([reverse("accounting:account_index"), 'Account',
                 'Full list of financial accounts within NOVA.',

                 'type (expense or revenue), name, description, county name, '
                 'county description, funding profile.'
                 ])

    data.append([reverse("accounting:auth_index"), "Authorization",
                "Authorizations allow for tracking and limiting vendor provided "
                "services over a specified time period. "
                "Allows for linking a vendor, client, service, and rate",

                "vendor, client, service, rate, amount, date_from, date_to."
                ])

    data.append([reverse("accounting:rate_index"), 'Rate',
                 'Each case worker will need at least one rate. '
                 'Rates are used to generate costs and amounts for case note transactions.',

                 'active, name, user, amount, interval.'
                 ])

    data.append([reverse("accounting:budget_index"), 'Budget',
                 'Budgets are tracked on at least one account for the specified year. '
                 'When an account utilization nears its limit, an alert is sent to administrators.',

                 'active, name, year, description, account(s), amount, utilization.'
                 ])

    data.append([reverse('accounting:contract_index'), 'Contracts',
                 'Contracts link a provider with an account and a service. '
                 'When a contract nears its threshold (amount) an alert is sent to administrators. ',

                 'year, vendor, account, service, amount, utilization.'
                ])

    data.append([reverse('accounting:transaction_index'), 'Transaction',
                 'The transaction view provides a list of all the transactions '
                 'in NOVA.',

                 'client, vendor, account, date, service date, batch, '
                 'inventory#/month, description, amount, unit, type (credit or debit), note.',
                 ])

    data.append([reverse('accounting:lot_index'), 'Transaction Batch',
                 'Transaction Batches provide a way to group transactions. This allows '
                 'verifying and submitting several transactions at once.',

                 'year, name, description.'
                 ])

    data.append([reverse('accounting:journal_entry_index'), 'Journal Entry',
                 'The journal entry view provides a list of all journal entries '
                 'in NOVA sorted by latest date. '
                 'If a journal entry is not balanced, it will turn red and include '
                 ' an "[UNBALANCED]" tag.',

                 'date, description, account, debit amount, credit amount.'
                 ])

    data.append([reverse('accounting:funding_profile_index'), 'Funding Profile',
                 'Funding profiles allow accounts and services to be linked '
                 'which allows NOVA to associate services with specific accounts '
                 'when case note transactions are generated.',

                 'active, name, description.'
                 ])

    return render(request, 'app_index.html', {
        'title' : 'Accounting Index',
        'data' : data,
    })












@get_logged_in
def transaction_file_view(request, transaction_file_id):
    # This view basically redirects to the main listview for this
    # form type, then does the popup for the form!
    transaction_file = get_object_or_404(TransactionFile, id=transaction_file_id)

    return HttpResponseRedirect(f"/accounting/transaction_entry/{transaction_file.transaction_entry.id}/{transaction_file.id}/?show_popup={transaction_file.id}")



@get_logged_in
def transaction_file_file(request, transaction_file_id, transaction_file_id2=None, filename=None):
    # There are 2 ids because of the default way that the django file
    # widget serves up the file's link. We just need a compatible URL
    # The transaction_file_id2 and filename params are both just ignored.
    # All we really need is the emr doc's id
    # 2024-03-04 this just seems wrong. TODO look into this again

    transaction_file = get_object_or_404(TransactionFile, id=transaction_file_id)

    file_path = os.path.join(settings.MEDIA_ROOT, f"{transaction_file.trans_file.name}")

    if os.path.exists(file_path):
        return FileResponse(open(file_path, "rb"))





@get_logged_in
def transaction_file_edit(request, transaction_id=None, transaction_file_id=None):
    if transaction_file_id:
        instance = get_object_or_404(TransactionFile, id=transaction_file_id)
        transaction = instance.transaction

        title = f"Transaction File"

    else:
        transaction = get_object_or_404(Transaction, id=transaction_id)
        instance = TransactionFile(
            transaction=transaction,
            uploaded_by=request.user,
            date=timezone.now().date,
            upload_date=timezone.now(),
        )

        title = f"New Transaction File"


    if request.method == "GET":
        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"
        form = TransactionFileForm(instance=instance)



        return render(request, "client/transaction_file_edit.html", {
            "instance": instance,
            "title": title,
            "form": form,
            "transaction": transaction,
            "popup": popup,
        })


    elif request.method == "POST":
        instance = get_object_or_None(TransactionFile, id=transaction_file_id)

        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, f"/accounting/transaction_entry/{transaction.id}")



        form = TransactionFileForm(request.POST, instance=instance)


        if form.is_valid():
            # Because we're using the model's ID as a directory path in the
            # media directory (media/transaction_file_id/filename) we need to save the
            # record first without the file, get the ID, then save again with
            # the file. It's wonky but it works.
            if request.FILES:
                obj = form.save()
                form = TransactionFileForm(request.POST, request.FILES, instance=obj)
                if form.is_valid():
                    print("FILES: {}".format(request.FILES))
                    obj.upload_date = timezone.now()
                    obj = form.save(commit=False)
                else:
                    return format_errors(request, form=form)
            else:
                obj = form.save(commit=False)

            return save_confirm(request, save_audit(request, form), f"/accounting/transaction/{transaction.id}")
        else:
            return format_errors(request, form=form)












@get_logged_in
def journal_file_view(request, journal_file_id):
    # This view basically redirects to the main listview for this
    # form type, then does the popup for the form!
    journal_file = get_object_or_404(JournalFile, id=journal_file_id)

    return HttpResponseRedirect(f"/accounting/journal_entry/{journal_file.journal_entry.id}/{journal_file.id}/?show_popup={journal_file.id}")



@get_logged_in
def journal_file_file(request, journal_file_id, journal_file_id2=None, filename=None):
    # There are 2 ids because of the default way that the django file
    # widget serves up the file's link. We just need a compatible URL
    # The journal_file_id2 and filename params are both just ignored.
    # All we really need is the emr doc's id
    # 2024-03-04 this just seems wrong. TODO look into this again

    journal_file = get_object_or_404(JournalFile, id=journal_file_id)

    file_path = os.path.join(settings.MEDIA_ROOT, f"{journal_file.jf_file.name}")

    if os.path.exists(file_path):
        return FileResponse(open(file_path, "rb"))





@get_logged_in
def journal_file_edit(request, journal_id=None, journal_file_id=None):
    if journal_file_id:
        instance = get_object_or_404(JournalFile, id=journal_file_id)
        journal = instance.journal_entry

        title = f"Journal File"

    else:
        journal = get_object_or_404(JournalEntry, id=journal_id)
        instance = JournalFile(
            journal_entry=journal,
            uploaded_by=request.user,
            date=timezone.now().date,
            upload_date=timezone.now(),
        )

        title = f"New Journal File"


    if request.method == "GET":
        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"
        form = JournalFileForm(instance=instance)



        return render(request, "accounting/journal_file_edit.html", {
            "instance": instance,
            "title": title,
            "form": form,
            "journal": journal,
            "popup": popup,
        })


    elif request.method == "POST":
        instance = get_object_or_None(JournalFile, id=journal_file_id)

        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, f"/accounting/journal_entry/{journal.id}")



        form = JournalFileForm(request.POST, instance=instance)


        if form.is_valid():
            # Because we're using the model's ID as a directory path in the
            # media directory (media/journal_file_id/filename) we need to save the
            # record first without the file, get the ID, then save again with
            # the file. It's wonky but it works.
            if request.FILES:
                obj = form.save()
                form = JournalFileForm(request.POST, request.FILES, instance=obj)
                if form.is_valid():
                    print("FILES: {}".format(request.FILES))
                    obj.upload_date = timezone.now()
                    obj = form.save(commit=False)
                else:
                    return format_errors(request, form=form)
            else:
                obj = form.save(commit=False)

            return save_confirm(request, save_audit(request, form), f"/accounting/journal_entry/{journal.id}")
        else:
            return format_errors(request, form=form)











@get_logged_in
def atp_discharge(request):
    # look at all ATP transactions
    # look at each ATP's month
    # figure out if an ATP transaction is already in place. if so, do nothing
    # otherwise add up the client's bills for that month.
    # If the bills are less than or equal to the client's ATP, do nothing
    # if the bills are more than the ATP, subtract the ATP from the total and
    # generate a writeoff transaction for that amount
    # ONLY RUN THIS FOR THE YEAR AND PERIOD THE USER IS CURRENTLY IN

    writeoff_act_match = Account.objects.filter(writeoff_account=True)
    fiscal_year, fiscal_period = get_fiscal_year_and_period(request)
    fiscal = int(f"{int(fiscal_year)}{int(fiscal_period):02}")

    extended_fiscal = None
    if fiscal_period >= 13:
        # Extended periods, subtract 12 from the period and 1 from
        # the year and include transactions for that fiscal also
        extended_fiscal = int(f"{int(fiscal_year + 1)}{int(fiscal_period - 12):02}")
        print(f"Extended_fiscal: {extended_fiscal}")
        #import pdb; pdb.set_trace()


    # if there is no writeoff account, notify the user by
    # returning an erorr
    if not writeoff_act_match:
        return format_errors(
            request, None,
            "No writeoff account was found. Please designate an account as a Writeoff Account in the <a href='/accounting/account/'>Chart of Accounts</a>",
            "/accounting/transaction/"
        )
    else:
        writeoff_act = writeoff_act_match[0]

    atp_records = AbilityToPay.objects.all()
    #atp_records = AbilityToPay.objects.filter(client=600)  # 600 berg #284 Jowett #1177 Est

    for atp in atp_records:
        # First, check if there are any ATP writeoffs for this client and fiscal
        # if there are, skip
        last_day_of_month = calendar.monthrange(atp.year, int(atp.month))[1]

        q_exists = Q()

        q_exists.add(Q(client=atp.client), Q.AND)
        q_exists.add(Q(service_date__year=atp.year), Q.AND)
        q_exists.add(Q(service_date__month=atp.month), Q.AND)
        q_exists.add(Q(atp_writeoff=True), Q.AND)

        # Im still not sure if this is 100% needed. So far I havent seen any 
        # client billables happening in extended periods. Will wait to hear
        # back from julie.
        if extended_fiscal:
            q_extended_fiscal = Q()
            q_extended_fiscal.add(Q(fiscal=fiscal), Q.OR)
            q_extended_fiscal.add(Q(fiscal=extended_fiscal), Q.OR)

            q_exists.add(Q(q_extended_fiscal), Q.AND)


        already_exists = Transaction.objects.filter(q_exists)

        if already_exists:
            print("ATP already exists for this year and period {} {}. Skipping".format(fiscal_year, fiscal_period))
            continue
        #import pdb; pdb.set_trace()
        print("Ability to Pay Year/Month: {}/{} Amount: {}".format(atp.year, atp.month, atp.amount))

        # find billable trans for this client under this atp month AND under this fiscal
        # TODO Update this so it considers fiscal periods 13-18 when we're running
        # this for a fiscal of 1-6
        query = Q()

        fiscal = int(f"{int(fiscal_year)}{int(fiscal_period):02}")

        if fiscal_period >= 1 and fiscal_period <= 6:
            # there are 18 fiscal periods so if we're running the ATP discharge
            # function in periods 1-6 we also need to look at the previous
            # fiscal year's periods 13-18
            extra_period_fiscal = int(f"{int(fiscal_year) - 1}{int(fiscal_period) + 12}")
            fiscal_query = Q()
            fiscal_query.add(Q(fiscal=fiscal), Q.OR)
            fiscal_query.add(Q(fiscal=extra_period_fiscal), Q.OR)

            query.add(fiscal_query, Q.AND)

            #import pdb; pdb.set_trace()

        else:
            query.add(Q(fiscal=fiscal), Q.AND)

        query.add(Q(client=atp.client), Q.AND)
        query.add(Q(service_date__year=atp.year), Q.AND)
        query.add(Q(service_date__month=atp.month), Q.AND)
        query.add(Q(client_billable=True), Q.AND)


        bill = Transaction.objects.filter(query).aggregate(
            Sum("amount"))["amount__sum"]

        if not bill:
            bill = 0


        if bill > atp.amount:
            writeoff_amount = bill - atp.amount
            #import pdb; pdb.set_trace()

            # create a writeoff transaction
            trans = Transaction(
                client=atp.client,
                account=writeoff_act,
                trans_type="0",
                amount=writeoff_amount,
                description="ATP Discharge {}/{}".format(atp.month, atp.year),
                fiscal_year=fiscal_year,
                fiscal_period=fiscal_period,
                service_date=dt.datetime(atp.year, int(atp.month), last_day_of_month),                
                date=dt.datetime(atp.year, int(atp.month), last_day_of_month),
                atp_writeoff=True,
            )
            trans.save()
            print()
            print("ATP discharge created for month:{}, amount: {}".format(atp.month, trans.amount))
            print()

        else:
            print("Bill ({}) is less than ATP ({}). Skipping".format(bill, atp.amount))


    if request.GET.get("next"):
        return HttpResponseRedirect(request.GET.get("next"))

    else:
        return HttpResponseRedirect("/accounting/transaction/")



@get_logged_in
def payroll_dist_index(request):
    data = PayrollDist.objects.all().order_by('date_from')

    return render(request, 'accounting/payroll_index.html', {
        'title' : 'Payroll Distribution',
        'data' : data,
        'table_class' : 'listview-table',
        })



@get_logged_in
def payroll_dist_edit(request, pk=None):
    if pk:
        instance = get_object_or_404(PayrollDist, id=pk)
        title = "Payroll Distribution"
    else:
        instance = None
        title = "New Payroll Distribution"

    if request.method == "GET":
        form = PayrollForm(instance=instance)

        # Also going to need a list of all the active employees
        # Differentiate between Employees that need to be processed
        # and employees that are already processed
        # processed employees will have a journal entry that links back
        # to this payroll distribution
        employees = Employee.objects.filter(active=True).order_by("last_name").all()

        entries = []
        entries_total = Decimal('0.00')
        if instance:
            entries = JournalEntry.objects.filter(payrolldist=instance)

            for entry in entries:
                credit, debit = entry.get_credit_debit()
                entries_total += debit

            # loop through employees, if they are linked to a JE that is
            # linked to this payroll dist, exclude em
            for emp in employees:
                match = Transaction.objects.filter(employee=emp, journal_entry__payrolldist=instance)
                if match:
                    employees = employees.exclude(id=emp.id)        



        return render(request, 'accounting/payroll_edit.html', {
            'form': form,
            'title': title,
            'instance': instance,
            "employee_data": employees,
            "entries_data": entries,
            "entries_total": entries_total,
        })


    elif request.method == "POST":
        form = PayrollForm(request.POST, instance=instance)

        if request.POST.get('delete'):
            if JournalEntry.objects.filter(payrolldist=instance):
                form.add_error(None, "Cannot delete payroll distribution with linked payroll journal entries.")
                return format_errors(request, form)

            delete_audit(request, instance)
            return delete_confirm(request, instance, "/accounting/payroll/")


        if form.is_valid():
            obj = save_audit(request, form)

            for je in JournalEntry.objects.filter(payrolldist=obj):
                je.date = obj.paycheck_date
                save_audit(request, je)
                for trans in Transaction.objects.filter(journal_entry=je):
                    trans.date = obj.paycheck_date
                    save_audit(request, trans)

            return save_confirm(request, obj, "/accounting/payroll/{}/".format(obj.id))

        else:
            return format_errors(request, form)








@get_logged_in
def payroll_je_edit(request, pk=None, parent_id=None, employee_id=None, _amount=0):
    if pk:
        instance = get_object_or_404(JournalEntry, id=pk)
        title = "Payroll Journal Entry"

    else:
        instance = None
        title = "Payroll Journal Entry"
        

    if request.method == "GET":
        journaltrans_data = []

        if instance:
            parent = instance.payrolldist
            employee = instance.employee
            form = PayrollJeForm(instance=instance)

            entries = Transaction.objects.filter(journal_entry=instance)         
            # get totals calculations like in the normal journal entry
            total_credit = entries.filter(trans_type='0').aggregate(Sum('amount')).get('amount__sum', 0.00)
            total_debit = entries.filter(trans_type='1').aggregate(Sum('amount')).get('amount__sum', 0.00)        
            
            if total_credit is None:
                total_credit = Decimal('0.0')
            if total_debit is None:
                total_debit = Decimal('0.0')

            total_class = ''
            offby_credit = ''
            offby_debit = ''

            if total_credit != total_debit:
                balanced = False
                total_class = "ac-red"
                if total_credit > total_debit:
                    offby_debit = total_debit - total_credit
                else:
                    offby_credit = total_credit - total_debit
            else:
                balanced = True


            #import pdb; pdb.set_trace()
            print("JOURNALTRANS_DATA: {}".format(journaltrans_data))
            return render(request, 'accounting/payroll_je_edit.html', {

                'form':form,
                'title':title,
                "subtitle" : "{}, {}".format(employee.last_name, employee.first_name),
                "instance" : instance,
                "parent" : parent,
                "employee" : employee,   
                "journaltrans_data" : entries, 
                'total_credit' : total_credit,
                'total_debit' :total_debit,
                'total_class' : total_class,
                'offby_credit' : offby_credit,
                'offby_debit': offby_debit,
                'balanced' : balanced,
                      
                })

        else:
            # create a new one, with the transactions and errything
            parent = get_object_or_None(PayrollDist, id=parent_id)
            employee = get_object_or_None(Employee, id=employee_id)
            amount = Decimal(_amount) / Decimal('100.00')
            
            je_obj = JournalEntry(
                payrolldist=parent,
                employee=employee,
                date=parent.paycheck_date,
                fiscal_year=get_fiscal_year(request),
                fiscal_period=get_fiscal_period(request),
                )

            instance = save_audit(request, je_obj)
            
            form = PayrollJeForm(instance=instance)
            
            # look at the employee's comp data and create some transactions
            comps = EmployeeComp.objects.filter(employee=employee)
            for comp in comps:
                amount_percent = amount * (Decimal(comp.percent) / Decimal('100.00'))
                
                new_trans = Transaction(
                    journal_entry=instance,
                    account=comp.account,
                    trans_type=comp.account.type,
                    employee=employee,
                    date=parent.paycheck_date,
                    description="{} payroll transaction".format(employee),
                    amount=amount_percent,
                    fiscal_period=instance.fiscal_period,
                    fiscal_year=instance.fiscal_year,
                    )

                save_audit(request, new_trans)

            # create + save an offsetting transaction using the home cash act
            offset_trans = Transaction(
                journal_entry=instance,
                account=employee.home_cash_account,
                trans_type=employee.home_cash_account.type if employee.home_cash_account else "0",
                employee=employee,
                date=parent.paycheck_date,
                description="{} payroll transaction".format(employee),
                amount=amount,
                fiscal_period=instance.fiscal_period,
                fiscal_year=instance.fiscal_year,
                )

            save_audit(request, offset_trans)


            return HttpResponseRedirect("/accounting/payroll_je/{}/".format(instance.id))
    
        

    elif request.method == "POST":
        form = PayrollJeForm(request.POST, instance=instance)

        if request.POST.get('delete'):

            # TODO This needs to be moved to the validation module!
            if Transaction.objects.filter(journal_entry=instance):                
                form.add_error(None, "Cannot delete journal entry with linked transactions.")
                return format_errors(request, form)

            delete_audit(request, instance)
            return delete_confirm(request, instance, "/accounting/payroll/{}/".format(instance.payrolldist.id))

        form = PayrollJeForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.verified = True
            obj = save_audit(request, obj)

            return save_confirm(request, obj, "/accounting/payroll/{}/".format(obj.payrolldist.id))

        else:
            return format_errors(request, form)





@get_logged_in
def journal_trans_edit(request, pk=None):
    mod = Transaction
    if pk:
        instance = get_object_or_404(mod, id=pk)
        title = "Journal Transaction"
    else:
        instance = None
        title = "New Journal Transaction"

    if request.method == "GET":
        if pk:
            form = JournalTransForm(instance=instance)
            #parent = JournalEntry.objects.get(pk=(instance.journal_entry_id))

        else:
            # New Journal Transaction
            parent = JournalEntry.objects.get(pk=request.GET.get('entry'))
            obj = mod(journal_entry=parent)
            # We're setting some initial values on the object before we pass it to the form
            # the reason is because we're passing a partially formed model
            # instance to the form builder and it must take those nulls as the
            # values so initial value cannot be set.
            obj.date = parent.date

            obj.fiscal_year = parent.fiscal_year
            obj.fiscal_period = parent.fiscal_period

            if request.GET.get("amount"):
                obj.amount = abs(Decimal(request.GET.get("amount")))


            form = JournalTransForm(instance=obj)


            # post processing on the form. If this is an offset,
            # hide a bunch of fields. Hopefully we dont have to make a
            # different form, although that wouldn't be so bad

            if request.GET.get("offset"):
                form.fields["client_lookup"].widget = HiddenInput()
                form.fields["vendor_lookup"].widget = HiddenInput()
                form.fields["service_date"].widget = HiddenInput()
                form.fields["unit_type"].widget = HiddenInput()
                form.fields["invoice"].widget = HiddenInput()
                form.fields["description"].required = False
                form.fields["description"].widget.attrs["width"] = 100



        form = set_autocomplete(instance, form, 'account')
        form = set_autocomplete(instance, form, "lot")
        form = set_autocomplete(instance, form, "vendor")
        form = set_autocomplete(instance, form, "client")

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"


        return render(request, 'accounting/journal_trans_edit.html', {
            'form': form,
            'locked': form.locked,
            'title': title,
            'popup': popup,
            "instance": instance,
        })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance)

        form = JournalTransForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save(commit=False)
            if obj.journal_entry is None:
                obj.journal_entry = request.GET.get("entry")

            saved = save_audit(request, obj)
            return save_confirm(request, saved, f"/accounting/journal_entry/{obj.journal_entry.id}/")
        else:
            return format_errors(request, form)



@get_logged_in
def journal_entry_review(request, journal_id):
    # Make sure the user has proper userconfig bit set to do this, otherwise
    # raise permission denied
    if request.user.userconfig.super_je is not True:
        view_audit(request, "WARN")
        raise PermissionDenied


    je = get_object_or_404(JournalEntry, id=journal_id)

    # Set the reviewed bits and dates and user
    # redirect to the journal entry
    je.supervisor_reviewed = True
    je.supervisor_review_date = timezone.now()
    je.supervisor_user = request.user

    saved = save_audit(request, je)
    return save_confirm(request, saved, f"/accounting/journal_entry/{je.id}/")





@get_logged_in
def journal_entry_unverify(request, pk):
    if request.user.userconfig.je_unverify is not True:
        view_audit(request, "WARN")
        raise PermissionDenied


    entry = get_object_or_404(JournalEntry, id=pk)

    entry.supervisor_user = None
    entry.supervisor_reviewed = False
    entry.supervisor_review_date = None
    entry.verified = False
    entry.save()

    return HttpResponseRedirect(f"/accounting/journal_entry/{pk}/")


@get_logged_in
def journal_entry_edit(request, pk=None):
    mod = JournalEntry
    frm = JournalEntryForm

    if pk:
        instance = get_object_or_404(mod, id=pk)
        title = "Journal Entry"
    else:
        instance = None
        title = "New Journal Entry"

    if request.method == "GET":
        form = frm(instance=instance, request=request)

        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"
        else:
            popup = ""

        if instance is None:
            entries = None
            form.fields['date'].initial = dt.datetime.now().strftime(settings.DATE_FORMAT)
            form.fields['fiscal_year'].initial = get_fiscal_year(request)
            form.fields["fiscal_period"].initial = get_fiscal_period(request)
            total_credit = None
            total_debit = None
            disb_total = Decimal('0.0')
        else:
            entries = Transaction.objects.filter(journal_entry_id=instance.pk).order_by("account__name")
            total_credit = entries.filter(trans_type='0').aggregate(Sum('amount')).get('amount__sum', 0.00)
            total_debit = entries.filter(trans_type='1').aggregate(Sum('amount')).get('amount__sum', 0.00)

            disb_total_credit = entries.filter(trans_type='0', account__cash_account=False).aggregate(Sum('amount')).get('amount__sum', 0.00) or Decimal('0.0')
            disb_total_debit = entries.filter(trans_type='1', account__cash_account=False).aggregate(Sum('amount')).get('amount__sum', 0.00) or Decimal('0.0')
            disb_total = disb_total_debit - disb_total_credit

        #calculate totals on entries
        if total_credit is None:
            total_credit = Decimal('0.0')
        if total_debit is None:
            total_debit = Decimal('0.0')

        total_class = ''
        offby_credit = ''
        offby_debit = ''


        if total_credit != total_debit:
            balanced = False
            total_class = "ac-red"
            if total_credit > total_debit:
                offby_debit = total_debit - total_credit
            else:
                offby_credit = total_credit - total_debit
        else:
            balanced = True

        if instance and instance.last_exported:
            last_exported = instance.last_exported
        else:
            last_exported = None


        return render(request, 'accounting/journal_entry_edit.html', {
            'form': form,
            'locked': form.locked,
            'title': title,
            'popup': popup,
            'journaltrans_data': entries,
            'instance': instance,
            'total_credit': total_credit,
            'total_debit': total_debit,
            'total_class': total_class,
            'offby_credit': offby_credit,
            'offby_debit': offby_debit,
            'balanced': balanced,
            "last_exported": last_exported,
            "disb_total": disb_total,
            "request": request,
        })

    elif request.method == "POST":
        form = frm(request.POST, instance=instance, request=request)

        if request.POST.get('delete') and form.locked is False:
            # do a quick lookup to make sure this does not have any Transactions
            if Transaction.objects.filter(journal_entry=instance):
                # TODO This needs to be moved to the validation module!
                form.add_error(None, "Cannot delete journal entry with linked transactions.")
                return format_errors(request, form)

            delete_audit(request, instance)
            return delete_confirm(request, instance, '/accounting/journal_entry/')

        if form.is_valid() and form.locked is False:
            obj = form.save(commit=False)
            obj.is_journal = True
            save_audit(request, obj)
            return save_confirm(request, obj, "/accounting/journal_entry/{}/".format(obj.id))
        else:
            return format_errors(request, form)


@get_logged_in
def journal_entry_index_all(request):
    # This view was originally for showing all the journal entries but the
    # size of the data returned would be way to big so if the users need a
    # way to view all journal entries a report will have to be written
    # This view just redirects to the journal_entry_index.
    return HttpResponseRedirect("/accounting/journal_entry/")


@get_logged_in
def journal_entry_index(request, show_all=False):
    page_number = request.GET.get("page")
    table_filter = request.GET.get("filter")
    year = get_fiscal_year(request)
    je_years = JournalEntry.objects.all().values(
        "fiscal_year").distinct().order_by("-fiscal_year")

    year_filter = request.GET.get("year") or year
    #import pdb; pdb.set_trace()

    query = Q()

    if year_filter:
        query.add(Q(fiscal_year=year_filter), Q.AND)
        year = year_filter

    if table_filter:
        query.add(Q(description__icontains=table_filter), Q.AND)


    data = JournalEntry.objects.filter(query).order_by('-date', 'description')


    paginator = Paginator(data, 200)
    page = paginator.get_page(page_number)



    return render(request, 'accounting/journal_entry_index.html', {
        'title': 'Journal Entry',
        'page': page,
        "show_all": show_all,
        "year": year,
        "year_filter": [y["fiscal_year"] for y in je_years],
    })


@get_logged_in
def journal_entry_verify(request):
    # TODO this function's name needs to be changed to reflect what this
    # function actually does (example: journal_entry_add_transactions)
    """ Create a new journal entry, and add the listed transactions to it
        Then redirect the user to the newly created journal entry
    """
    ids = request.GET.get('ids').split(',')
    if not ids:
        raise Exception("No transaction IDs in list.") # should really never hit this

    # Create a new Journal Entry. Set the date. TODO think of a good description
    journal_new = JournalEntry(
        date=dt.datetime.now(),
        verified=False,
        description="",
        fiscal_year=get_fiscal_year(),
        fiscal_period=get_fiscal_period()
        )
    journal = save_audit(request, journal_new)
    save_confirm(request, journal, "/accounting/journal_entry/{}".format(journal.id))

    for id in ids:
        matched_trans = get_object_or_None(Transaction, id=id)
        if matched_trans is None:
            raise Exception("No transaction found matching this ID") # also should never really hit this
        if matched_trans.journal_entry:
            # This transaction already has a journal entry, how is it here?!
            continue

        matched_trans.journal_entry = journal
        matched_trans.save()

    return HttpResponseRedirect("/accounting/journal_entry/{}".format(journal.id))




@get_logged_in
def journal_entry_unlink_all(request, pk):
    """ Unlinks all transactions from journal entry """
    journal = get_object_or_None(JournalEntry, id=pk)
    if not journal:
        raise Exception("Cannot find journal by id: {}".format(pk))

    # find all the transactions under this journal and remove the relationship
    transaction_list = Transaction.objects.filter(journal_entry=journal)
    for trans in transaction_list:
        trans.journal_entry = None
        saved = save_audit(request, trans)

    return HttpResponseRedirect("/accounting/journal_entry/{}/".format(pk))





@get_logged_in
def journal_entry_export(request, pk):
    """
        Exports a journal entry into the format required by the user's
        accounting system.
        Current supported types: acs
    """

    entry = get_object_or_None(JournalEntry, pk=pk)

    if entry.verified is False:
        view_audit(request, "WARN")
        raise PermissionDenied
    if entry.supervisor_reviewed is not False:
        view_audit(request, "WARN")
        raise PermissionDenied


    trans = Transaction.objects.filter(journal_entry=entry)

    county_financial_system = get_app_config("county_financial_system")

    if county_financial_system == "acs":
        # generate the correct format and serve the file
        response = generate_export_acs_text(trans)
    else:
        raise Exception("Could not find export function matching county financial system {}".format(county_financial_system))


    entry.last_exported = timezone.now()
    save_audit(request, entry)

    return response



@get_logged_in
def transaction_unlink(request, pk=None):
    if not pk:
        return HttpResponseRedirect("/accounting/journal_entry/")

    trans = get_object_or_None(Transaction, id=pk)
    if not trans:
        return HttpResponseRedirect("/accounting/journal_entry/")

    journal_id = trans.journal_entry.id
    trans.journal_entry = None
    trans.save()

    return HttpResponseRedirect("/accounting/journal_entry/{}/".format(journal_id))


@get_logged_in
def transaction_edit(request, pk=None):
    mod = Transaction
    frm = TransactionForm

    if pk:
        instance = get_object_or_404(mod, id=pk)
        title = "Transaction"
    else:
        instance = None
        title = "New Transaction"

    if request.method == "GET":
        form = frm(instance=instance, request=request)   
        print("LOCKED? {}".format(form.locked))     

        form.fields["fiscal_year"].initial = get_fiscal_year(request)
        form.fields["fiscal_period"].initial = get_fiscal_period(request)
        form.fields['date'].initial = dt.datetime.now().date


        form = set_autocomplete(instance, form, 'account')
        form = set_autocomplete(instance, form, 'vendor')
        form = set_autocomplete(instance, form, 'client')
        form = set_autocomplete(instance, form, 'lot')
        form = set_autocomplete(instance, form, "service")
        form = set_autocomplete(instance, form, "authorization")

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"        


        print("LOCKED? {}".format(form.locked))
        return render(request, 'accounting/transaction_edit.html', {
            "popup_class": "popup-filter",
            "form": form,
            "locked": form.locked,
            "title": title,
            "popup": popup,
            "instance": instance,
        })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, "/accounting/transaction/")

        form = frm(request.POST, instance=instance, request=request)
        if form.is_valid():
            obj = save_audit(request, form)
            # If this is a new save, then redirect to the form itself. the
            # user may want to upload supporting documentation.
            # If this is not a new save (an update) then redirect the user
            # back to the transaction root
            if not instance:
                return save_confirm(request=request, instance=obj)
            else:
                return save_confirm(
                    request=request,
                    instance=obj,
                    path_override="/accounting/transaction/")
        else:
            return format_errors(request, form)





@get_logged_in
def transaction_index(request):
    #trans_data = Transaction.objects.all().order_by('-date', 'vendor__name', 'description', 'amount')
    form = TransactionFilterForm(request=request)


    billable = False  # set this outside because the template will use it to display different buttons

    if request.method == "POST":
        datefrom = request.POST.get("date_from")
        dateto = request.POST.get("date_to")
        journal_option = request.POST.get('journal_option')
        billable = bool(request.POST.get('billable') == 'on')
        atp_only = bool(request.POST.get("atp_only") == 'on')
        fiscal_year = request.POST.get("fiscal_year")
        fiscal_period = request.POST.get("fiscal_period")
        account_type = request.POST.get("account_type")
        caseworker = request.POST.get("caseworker")
        invoice = request.POST.get("invoice")


        form.fields['journal_option'].initial = journal_option
        form.fields["billable"].initial = billable
        form.fields["fiscal_year"].initial = fiscal_year
        form.fields["fiscal_period"].initial = fiscal_period
        form.fields["account_type"].initial = account_type
        form.fields["caseworker"].initial = caseworker
        form.fields["atp_only"].initial = atp_only
        form.fields["invoice"].initial = invoice


        # this is a bit different for filters, but still the same idea
        client = request.POST.get("client") or None
        account = request.POST.get("account") or None
        vendor = request.POST.get("vendor") or None
        lot = request.POST.get("lot") or None

        form = set_autocomplete(None, form, 'client', Client, client)
        form = set_autocomplete(None, form, 'account', Account, account)
        form = set_autocomplete(None, form, 'vendor', Vendor, vendor) 
        form = set_autocomplete(None, form, 'lot', TransactionLot, lot)

    else:
        # If there is userform_data, let's use that for the query
        # so when the user pulls up this page the data shown is based
        # on the filter data stored in the userform
        fiscal_year = form.userform_data.get("fiscal_year")
        fiscal_period = form.userform_data.get("fiscal_period")

        if not fiscal_period:
            fiscal_period = get_fiscal_period(request)
            form.fields["fiscal_period"].initial = fiscal_period

        if not fiscal_year:
            fiscal_year = get_fiscal_year(request)
            form.fields["fiscal_year"].initial = fiscal_year


        datefrom = form.userform_data.get("date_from")
        dateto = form.userform_data.get("date_to")
        journal_option = form.userform_data.get("journal_option")
        billable = form.userform_data.get("billable")
        atp_only = form.userform_data.get("atp_only")
        account_type = form.userform_data.get("account_type")
        caseworker = form.userform_data.get("caseworker")
        client = form.userform_data.get("client")
        account = form.userform_data.get("account")
        vendor = form.userform_data.get("vendor")
        lot = form.userform_data.get("lot")
        invoice = form.userform_data.get("invoice")




    query = Q()

    if billable:
        query.add((Q(client_billable=True) | Q(client_payment=True)), Q.AND)

    if journal_option == "journaled":
        query.add(Q(journal_entry__isnull=False), Q.AND)
    elif journal_option == "unjournaled":
        query.add(Q(journal_entry__isnull=True), Q.AND)


    if atp_only:
        query.add(Q(atp_writeoff=True), Q.AND)
    if fiscal_year:
        query.add(Q(fiscal_year=fiscal_year), Q.AND)
    if fiscal_period:
        query.add(Q(fiscal_period=fiscal_period), Q.AND)
    if account:
        query.add(Q(account=account), Q.AND)
    if vendor:
        query.add(Q(vendor=vendor), Q.AND)
    if client:
        query.add(Q(client=client), Q.AND)
    if lot:
        query.add(Q(lot=lot), Q.AND)
    if account_type:
        query.add(Q(account__type=account_type), Q.AND)
    if invoice:
        query.add(Q(invoice__iexact=invoice), Q.AND)





    if datefrom is None or datefrom == '':
        #datefrom = dt.datetime(get_fiscal_year(request), 1, 1)
        datefrom = None
    else:
        datefrom = dt.datetime.strptime(datefrom, settings.DATE_FORMAT)
        form.fields['date_from'].initial = datefrom.strftime(settings.DATE_FORMAT)
        query.add(Q(service_date__gte=datefrom), Q.AND)

    if dateto is None or dateto == '':
        #dateto = dt.datetime.now()
        dateto = None
    else:
        dateto = dt.datetime.strptime(dateto, settings.DATE_FORMAT)
        form.fields['date_to'].initial = dateto.strftime(settings.DATE_FORMAT)    
        query.add(Q(service_date__lte=dateto), Q.AND)


    # Looks up caseworker for emr notes and forms that are linked to
    # transactions
    if caseworker:
        query.add(Q(emr_form__created_by=caseworker), Q.AND)


    # This looks up caseworkers in legacy notes. I don't believe this is
    # needed any more (2023-09-26) but leaving for now just in case
    '''
    if caseworker:
        # If a caseworker is chosen, create a big OR clause
        # and add that to the existing query
        query.add(Q(case_note__isnull=False), Q.AND)

        bigor = Q()
        includes = [item for item in Transaction.objects.filter(query).order_by("date") if item.case_note.get_signer_user() and item.case_note.get_signer_user().id == int(caseworker)]
        for inc in includes:
            bigor.add(Q(id=inc.id), Q.OR)

        query.add(Q(bigor), Q.AND)
    '''


    # Only show the first 200 recent transactions on the GET view, but show
    # all results for filtered POST requests. The reason there's no pagination
    # right now is because the users can use a 'check all' feature to move the
    # selected transactions into a journal entry and of course all of the
    # transactions can't be selected if they're not all on the same page.
    # Maybe in the future this can be addressed.
    if request.method == "POST":
        trans_data = Transaction.objects.filter(query).order_by('-date', '-id')[:2000]
    else:
        trans_data = Transaction.objects.filter(query).order_by('-date', '-id')[:200]


    return render(request, 'accounting/transaction_index.html', {
        'title': 'Transaction',
        'form': form,
        'trans_data': trans_data,
        'table_class': 'listview-table',
        'fiscal_year': get_fiscal_year(request),
        "billable": billable,
    })




@get_logged_in
def payroll_transaction_edit(request, pk=None):
    if pk:
        instance = get_object_or_404(Transaction, id=pk)
        title = "Payroll Transaction"
    else:
        instance = None
        title = "New Payroll Transaction"



    if request.method == "GET":
        form = PayrollTransactionForm(instance=instance, request=request)    
        form.fields["fiscal_year"].initial = get_fiscal_year(request)
        form.fields["fiscal_period"].initial = get_fiscal_period(request)
        form.fields['date'].initial = dt.datetime.now().date 

        form = set_autocomplete(instance, form, "employee")
        form = set_autocomplete(instance, form, "account")
        form = set_autocomplete(instance, form, "lot")


        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"


        return render(request, 'accounting/transaction_edit.html', {
            "popup_class": "popup-filter",
            "form": form,
            "locked": form.locked,
            "title": title,
            "popup": popup,
            "instance": instance,
        })



    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, "/accounting/transaction/")

        form = PayrollTransactionForm(
            request.POST, instance=instance, request=request)

        if form.is_valid():
            obj = save_audit(request, form)
            # If this is a new save, then redirect to the form itself. the
            # user may want to upload supporting documentation.
            # If this is not a new save (an update) then redirect the user
            # back to the transaction root
            if not instance:
                return save_confirm(request=request, instance=obj)
            else:
                return save_confirm(
                    request=request,
                    instance=obj,
                    path_override="/accounting/transaction/")
        else:
            return format_errors(request, form)







@get_logged_in
def client_payment_edit(request, pk=None):
    mod = Transaction
    frm = ClientPaymentForm

    if pk:
        instance = get_object_or_404(mod, id=pk)
        title = "Client Payment"
    else:
        instance = None
        title = "New Client Payment"

    if request.method == "GET":
        if pk:
            form = frm(instance=instance, )

        else:
            form = frm()
            form.fields["fiscal_year"].initial = get_fiscal_year(request)
            form.fields["fiscal_period"].initial = get_fiscal_period(request)
            form.fields["date"].initial = dt.datetime.now().date()
            form.fields["description"].initial = "Client payment"

        form = set_autocomplete(instance, form, 'client')
        form = set_autocomplete(instance, form, 'account')
        form = set_autocomplete(instance, form, 'lot')

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"


        return render(request, 'accounting/transaction_edit.html', {
            'form': form,
            'title': title,
            'popup': popup,
            "instance": instance,
            "client_payment": True,
        })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance)

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.client_payment = True

            obj = save_audit(request, obj)
            # If this is a new save, then redirect to the form itself. the
            # user may want to upload supporting documentation.
            # If this is not a new save (an update) then redirect the user
            # back to the transaction root
            if not instance:
                return save_confirm(request=request, instance=obj)
            else:
                return save_confirm(
                    request=request,
                    instance=obj,
                    path_override="/accounting/transaction/")
        else:
            return format_errors(request, form)








@access_control("admin", "caseworker", "financial")
def auth_index(request):
    page_number = request.GET.get("page")
    table_filter = request.GET.get("filter")
    year = get_fiscal_year(request)
    auth_years = Authorization.objects.all().values(
        "date_from__year").distinct().order_by("-date_from__year")
    year_filter = request.GET.get("year")

    query = Q()

    if year_filter:
        query.add(Q(date_from__year=year_filter), Q.AND)
        year = year_filter

    if table_filter:
        # There are 2 filter modes. 1 just normal startswith for the vendor and
        # the client. The other is when the users type in the auth number
        # What the users don't know is the first 4 digits of the auth number
        # is the year the auth starts and any numbers after that are actually
        # the auth's unique ID.
        is_num = False
        if len(table_filter) >= 5:    
            try:
                auth_id = int(table_filter[4:])
                is_num = True
            except ValueError:
                pass

        if is_num:
            query.add(Q(id=auth_id), Q.AND)
        else:         
            query.add(
                (Q(vendor__name__istartswith=table_filter) | Q(client__last_name__istartswith=table_filter)), Q.AND)


    data = Authorization.objects.filter(query).order_by(
        "-date_from", "vendor__name", "client__last_name")


    '''
    Removed the CSV export for now, people aren't using it and it slows
    the page load
    report_results = []
    for item in page:
        report_results.append({
            "auth #": item.get_auth_number(),
            "date from": item.date_from.strftime(settings.DATE_FORMAT),
            "date to": item.date_to.strftime(settings.DATE_FORMAT),
            "vendor": str(item.vendor),
            "client": str(item.client),
            "service": str(item.service),
            "utilization": str(item.get_usage_fancy()),
            "target group": item.get_target_group_display(),
        }) 

    report_id = str(uuid.uuid4())
    request.session[report_id] = json.dumps(
        report_results, cls=DjangoJSONEncoder)
    '''

    paginator = Paginator(data, 200)
    page = paginator.get_page(page_number)

    return render(request, "accounting/auth_index.html", {
        "title": "Authorization",
        "auth_data": page,
        "page": page,
        "year": year,
        "filter": table_filter,
        "year_filter": [y["date_from__year"] for y in auth_years],
        #"report_id": report_id,
    })




@access_control("admin", "caseworker", "financial")
def auth_edit(request, auth_id):
    instance = get_object_or_404(Authorization, id=auth_id)
    title = "Authorization"

    if request.method == "GET":

        locked = False
        if instance.user_signed_by and request.user.userconfig.super_auth is False:
            locked = True
        elif instance.super_signed_by:
            locked = True




        form = AuthForm(instance=instance, request=request, locked=locked)



        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"

        set_autocomplete(instance, form, "account")
        set_autocomplete(instance, form, "client")
        set_autocomplete(instance, form, "service")
        set_autocomplete(instance, form, "vendor")

        return render(request, "accounting/auth_edit.html", {
            'form': form,
            'title': title,
            'popup': popup,
            "instance": instance,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, "/accounting/auth/")

        form = AuthForm(request.POST, instance=instance)
        if form.is_valid():

            saved_auth = save_audit(request, form)

            # TODO: update the save_history action to indicate if a supervisor
            # is overriding this already 'locked' document.
            saved_auth.save_history("save", request)


            return save_confirm(request, saved_auth)
        else:
            return format_errors(request, form=form)


@access_control("admin", "caseworker", "financial")
def auth_reopen(request, auth_id):
    # Verify permissions. Only someone with super_auth bit on their UserConfig
    # may remove existing supervisor signatures. All users can remove existing
    # user sigs
    # When a supervisor unlocks a form, the remove both sigs
    auth = get_object_or_404(Authorization, id=auth_id)


    if auth.super_signed_by:
        if request.user.userconfig.super_auth is True:
            auth.save_history("super_reopen", request)
            auth.reset_signatures(user_and_super=True)
        else:
            raise PermissionDenied()

    elif auth.user_signed_by:
        auth.save_history("user_reopen", request)
        auth.reset_signatures(user_and_super=False)




    return HttpResponseRedirect(f"/accounting/auth/{auth.id}")











@access_control("admin", "caseworker", "financial")
def auth_new(request):
    instance = None
    title = "New Authorization"

    if request.method == "GET":
        form = NewAuthForm(instance=instance)

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"

        #form = set_autocomplete(instance, form, "FIELD_NAME")
        form.fields["auth_caseworker"].initial = request.user


        return render(request, "accounting/auth_new.html", {
            'form': form,
            'title': title,
            'popup': popup,
            "instance": instance,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, f"/accounting/auth/")

        form = NewAuthForm(request.POST, instance=instance)
        if form.is_valid():

            # If there is new client data, create the client first, then
            # use that ID for the auth_client field
            data = form.cleaned_data
            if ("client_last_name" in data and data["client_last_name"]
                    and "client_first_name" in data and data["client_first_name"]):

                new_client = Client(
                    first_name=data["client_first_name"],
                    last_name=data["client_last_name"],
                    middle_name=data["client_middle_name"] or None,
                    birthdate=data["client_birthdate"] or None,
                    gender=data["client_gender"] or None,

                )
                saved_client = save_audit(request, new_client)

                data["auth_client"] = saved_client.id


            new_auth = Authorization(
                caseworker=data["auth_caseworker"],
                vendor=get_object_or_None(Vendor, id=data["auth_vendor"]),
                service=get_object_or_None(Service, id=data["auth_service"]),
                client=get_object_or_None(Client, id=data["auth_client"]),
                account=get_object_or_None(Account, id=data["auth_account"]),
                date_from=data["auth_date_from"],
                date_to=data["auth_date_to"],
                week=data["auth_week"],
                amount=data["auth_amount"],
                unit_type=data["auth_unit_type"],
                units=data["auth_units"],
                service_date_from=data["auth_service_date_from"],
                service_date_to=data["auth_service_date_to"],
                target_group=data["auth_target_group"],
                wic_cat=data["auth_wic_cat"],
                iii_b_cat=data["auth_iii_b_cat"],
                iii_e_cat=data["auth_iii_e_cat"],
                cst_cat=data["auth_cst_cat"],
                elder_abuse_cat=data["auth_elder_abuse_cat"],
                notes=data["auth_notes"],
            )

            saved_auth = save_audit(request, new_auth)
            saved_auth.save_history("create", request)


            return save_confirm(request, saved_auth)
        else:
            return format_errors(request, form=form)






@get_logged_in
def auth_file_view(request, auth_file_id):
    # This view basically redirects to the main listview for this
    # form type, then does the popup for the form!
    auth_file = get_object_or_404(AuthFile, id=auth_file_id)

    return HttpResponseRedirect(f"/accounting/auth/{auth_file.auth.id}/{auth_file.id}/?show_popup={auth_file.id}")



@get_logged_in
def auth_file_file(request, auth_file_id, auth_file_id2=None, filename=None):
    # There are 2 ids because of the default way that the django file
    # widget serves up the file's link. We just need a compatible URL
    # The auth_file_id2 and filename params are both just ignored.
    # All we really need is the emr doc's id
    # 2024-03-04 this just seems wrong. TODO look into this again

    auth_file = get_object_or_404(AuthFile, id=auth_file_id)

    file_path = os.path.join(settings.MEDIA_ROOT, f"{auth_file.af_file.name}")

    if os.path.exists(file_path):
        return FileResponse(open(file_path, "rb"))





@get_logged_in
def auth_file_edit(request, auth_id=None, auth_file_id=None):
    if auth_file_id:
        instance = get_object_or_404(AuthFile, id=auth_file_id)
        auth = instance.authorization

        title = "Auth File"

    else:
        auth = get_object_or_404(Authorization, id=auth_id)
        instance = AuthFile(
            authorization=auth,
            uploaded_by=request.user,
            date=timezone.now().date,
            upload_date=timezone.now(),
        )

        title = f"New auth File"


    if request.method == "GET":
        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"

        locked = False
        if auth.user_signed_by:
            locked = True

        form = AuthFileForm(instance=instance, locked=locked)



        return render(request, "accounting/auth_file_edit.html", {
            "instance": instance,
            "title": title,
            "form": form,
            "auth": auth,
            "popup": popup,
        })


    elif request.method == "POST":
        instance = get_object_or_None(AuthFile, id=auth_file_id)

        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, f"/accounting/auth/{auth.id}")



        form = AuthFileForm(request.POST, instance=instance)


        if form.is_valid():
            # Because we're using the model's ID as a directory path in the
            # media directory (media/auth_file_id/filename) we need to save the
            # record first without the file, get the ID, then save again with
            # the file. It's wonky but it works.
            if request.FILES:
                obj = form.save()
                form = AuthFileForm(request.POST, request.FILES, instance=obj)
                if form.is_valid():
                    print("FILES: {}".format(request.FILES))
                    obj.upload_date = timezone.now()
                    obj = form.save(commit=False)
                else:
                    return format_errors(request, form=form)
            else:
                obj = form.save(commit=False)

            return save_confirm(request, save_audit(request, form), f"/accounting/auth/{auth.id}")
        else:
            return format_errors(request, form=form)












@get_logged_in
def authorization_edit(request, pk=None):
    '''
        There are 2 different auth views here. One is just the standard auth
        the other is an auth as a child of a contract.
    '''
    if pk:
        instance = get_object_or_404(Authorization, id=pk)
        title = "Authorization"
        if instance.contract:
            parent = instance.contract
        else:
            parent = None
    else:
        instance = None
        title = "New Authorization"
        if request.GET.get("contract"):
            parent = get_object_or_None(Contract, id=request.GET.get("contract"))
        else:
            parent = None


    if request.method == "GET":
        form = AuthorizationForm(instance=instance, request=request)

        form = set_autocomplete(instance, form, "client")
        form = set_autocomplete(instance, form, "rate")
        form = set_autocomplete(instance, form, "account")

        if parent:
            form.fields["contract"].initial = parent
            form.fields["vendor"].initial = parent.vendor

            vendor_set = Vendor.objects.filter(
                id__in=[x.vendor.id for x in Contract.objects.filter(year=parent.year)]).order_by("name")
            form.fields["vendor"].queryset = vendor_set

            query_services = Service.objects.filter(
                id__in=[x.service.id for x in ContractService.objects.filter(contract=parent)]).order_by("name")
            form.fields["service"].queryset = query_services



        return render(request, 'accounting/authorization_edit.html', {
            "form": form,
            "title": title,
            "instance": instance,
            "popup": "popup",
            "locked": form.locked,
            "parent": parent,
        })

    elif request.method == "POST":
        if request.user.userconfig.edit_contracts is not True:
            view_audit(request, "WARN")
            raise PermissionDenied

        if request.POST.get("delete"):
            delete_audit(request, instance)
            return delete_confirm(request, instance)

        form = AuthorizationForm(request.POST, instance=instance, request=request)
        if form.is_valid():
            obj = save_audit(request, form)
            return save_confirm(request, obj, "/accounting/authorization/{}/".format(obj.id))

        else:
            return format_errors(request, form)



@get_logged_in
def budget_copy_existing(request):
    """ The idea behind this is that the users want to be able to make
        a copy of their existing budget when making a new budget
        this will save them time because it pre-fills the accounts and
        amounts and a lot of those don't change every year
    """
    cur_budget = Budget.objects.filter(active=True).order_by("-year", "-pk").first()

    new_budget = Budget(
        active=True,
        name="Newly Created Budget",
        year=cur_budget.year + 1,
        )

    new_budget.save()

    if not cur_budget:
        # if there are no results, just redirect to a new budget
        return HttpResponseRedirect("/accounting/budget/new/")

    #import pdb; pdb.set_trace()
    for item in cur_budget.budget_item_set.all():
        item.id = None
        item.budget = new_budget
        item.save()

    return HttpResponseRedirect(f"/accounting/budget/{new_budget.id}/")






@get_logged_in
def budget_index(request):
    data = Budget.objects.all().order_by('name')
    return render(request, 'accounting/budget_index.html', {
        'title' : 'Budget',
        'budget_data' : data,
        'table_class' : 'listview-table',
        })


@get_logged_in
def budget_edit(request, pk=None):
    mod = Budget
    frm = BudgetForm
    report_id = ""

    if pk:
        instance = get_object_or_404(mod, id=pk)
        title = "Budget"
        items = BudgetItem.objects.filter(budget_id=instance.id)

        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(list(items.all().values(
        "id", "account__name", "account__description",
        "account__county_name", "account__county_description",
        "amount")),
                cls=DjangoJSONEncoder)

    else:
        instance = None
        title = "New Budget"
        items = ''

    if request.method == "GET":
        form = frm(instance=instance)

        return render(request, 'accounting/budget_edit.html', {
            'form':form,
            'title':title,
            'instance' : instance,
            'budget_item_data' : items,
            "report_id" : report_id,
            'table_class' : 'listview-table',
            })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance)

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            obj = save_audit(request, form)
            return save_confirm(request, obj, "/accounting/budget/{}/".format(obj.id))

        else:
            return format_errors(request, form)



@get_logged_in
def budget_item_edit(request, pk=None):
    mod = BudgetItem
    frm = BudgetItemForm

    if pk:
        instance = get_object_or_404(mod, id=pk)
        title = "Budget Item"
    else:
        instance = None
        title = "New Budget Item"

    if request.method == "GET":
        if pk:
            form = frm(instance=instance, )
            parent = Budget.objects.get(pk=(instance.budget_id)) # not sure if this is actually needed

        else:
            parent = Budget.objects.get(pk=request.GET.get('budget'))
            obj = mod(budget=parent)
            form = frm(instance=obj)

        form.fields['account'].queryset = Account.objects.filter(active=True).order_by('name')


        form = set_autocomplete(instance, form, 'account')

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"

        return render(request, 'accounting/budget_item_edit.html', {
            'form':form,
            'title':title,
            'popup' : popup,
            })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, "/accounting/budget/")

        form = frm(request.POST, instance=instance)
        #import pdb; pdb.set_trace()
        if form.is_valid():
            obj = form.save(commit=False)
            #if obj.budget_id is None:
            #    obj.budget_id = request.GET.get("budget")

            obj = save_audit(request, obj)
            return save_confirm(request, obj)
        else:
            return format_errors(request, form)






@get_logged_in
def funding_profile_index(request,):
    data = FundingProfile.objects.order_by('active', 'name')
    return render(request, 'accounting/funding_profile_index.html', {
        'title' : 'Funding Profile',
        'funding_profile_data' : data,
        'table_class' : 'listview-table',
        })


@get_logged_in
def funding_profile_edit(request, pk=None):
    mod = FundingProfile
    frm = FundingProfileForm

    if pk:
        instance = get_object_or_404(mod, id=pk)
        title = "Funding Profile"
    else:
        instance = None
        title = "New Funding Profile"

    if request.method == "GET":
        form = frm(instance=instance)

        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"
        else:
            popup = ""

        return render(request, 'accounting/funding_profile_edit.html', {
            'form':form,
            'title':title,
            'popup':popup,
            })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance)

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            obj = save_audit(request, form)
            return save_confirm(request, obj)

        else:
            return format_errors(request, form)


@get_logged_in
def contract_index_all(request):
    return contract_index(request)


@get_logged_in
def contract_index(request):
    page_number = request.GET.get("page")
    table_filter = request.GET.get("filter")
    year = get_fiscal_year(request)


    if table_filter:
        if len(table_filter) == 4 and table_filter.isdigit():
            # the user wants to look up by year
            data = Contract.objects.filter(year=table_filter).order_by('-year', 'vendor')
        else:
            # Lookup by vendor and service
            query = Q()
            query.add(Q(vendor__name__istartswith=table_filter), Q.OR)
            query.add(Q(contractservice__service__spc_code__istartswith=table_filter), Q.OR)

            data = Contract.objects.filter(query).distinct().order_by('-year', 'vendor')


    else:
        data = Contract.objects.all().distinct().order_by('-year', 'vendor')

    paginator = Paginator(data, 100)
    page = paginator.get_page(page_number)

    '''
    This is disabled because we are moving to an actual contract report
    make sure to delete this once the contract report is live!
    csv_data = []
    for contract in page:
        csv_data.append({
            "year": contract.year,
            "vendor": str(contract.vendor),
            "services": "\n".join(contract.get_services()),
            "service amounts": "\n".join(contract.get_service_amounts()),
            "service_utilization": "\n".join(contract.get_service_utilizations()),
            "contract_amount": contract.amount,
            "contract_utlization": contract.get_usage_fancy(),
        })

        report_id = str(uuid.uuid4())
        request.session[report_id] = json.dumps(
            csv_data, cls=DjangoJSONEncoder)
    '''


    return render(request, 'accounting/contract_index.html', {
        'title': 'Contract',
        'contract_data': page,
        "page": page,
        "year": year,
    })


@get_logged_in
def contract_edit(request, pk=None):
    if pk:
        instance = get_object_or_404(Contract, id=pk)
        title = f"{instance.get_contract_type_display()} Contract"
        auth_data = Authorization.objects.filter(contract=instance)
    else:
        instance = None
        title = "New Contract"
        auth_data = None

    if request.method == "GET":
        form = ContractForm(instance=instance, request=request)

        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"
        else:
            popup = ""

        form = set_autocomplete(instance, form, 'vendor')
        form = set_autocomplete(instance, form, 'account')


        if instance:
            services = ContractService.objects.filter(contract=instance).order_by("service__name")
        else:
            services = None
            form.fields['year'].initial = dt.datetime.now().year



        return render(request, "accounting/contract_edit.html", {
            "form": form,
            "title": title,
            "popup": popup,
            "instance": instance,
            "auth_data": auth_data,
            "services": services,
            "locked": form.locked,
        })

    elif request.method == "POST":
        if request.user.userconfig.edit_contracts is not True:
            view_audit(request, "WARN")
            raise PermissionDenied

        if request.POST.get("delete"):
            delete_audit(request, instance)
            return delete_confirm(request, instance, "/accounting/contract/")

        form = ContractForm(request.POST, instance=instance, request=request)
        if form.is_valid():
            obj = save_audit(request, form)
            return save_confirm(request, obj, "/accounting/contract/{}/".format(obj.id))

        else:
            return format_errors(request, form)






@get_logged_in
def contract_service_edit(request, contract_id, contract_service_id=None):
    if contract_service_id:
        instance = get_object_or_404(ContractService, id=contract_service_id)
        title = "Contract Service"
    else:
        instance = None
        title = "New Contract Service"


    if request.method == "GET":
        form = ContractServiceForm(instance=instance, request=request)

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"

        if not instance:
            form.fields["contract"].initial = contract_id



        return render(request, "accounting/contract_service_edit.html", {
            'form': form,
            'title': title,
            'popup': popup,
            "instance": instance,
            "locked": form.locked,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, f"/accounting/contract/{{instance.id}}/")

        form = ContractServiceForm(request.POST, instance=instance, request=request)
        if form.is_valid():
            return save_confirm(request, save_audit(request, form))
        else:
            return format_errors(request, form=form)








@get_logged_in
def lot_index_all(request):
    return lot_index(request, show_all=True)




@get_logged_in
def lot_index(request, show_all=False):
    year = get_fiscal_year(request)
    if show_all:
        data = TransactionLot.objects.order_by('active', "-year", 'name')
    else:        
        data = TransactionLot.objects.filter(
            year=year).order_by('active', 'name')


    return render(request, 'accounting/lot_index.html', {
        'title': 'Transaction Batch',
        'lot_data': data,
        'table_class': 'listview-table-page',
        "show_all": show_all,
        "year": year,
    })


@get_logged_in
def lot_edit(request, pk=None):
    mod = TransactionLot
    frm = LotForm

    if pk:
        instance = get_object_or_404(mod, id=pk)
        title = "Transaction Batch"
    else:
        instance = None
        title = "New Transaction Batch"

    if request.method == "GET":
        form = frm(instance=instance)

        if instance is None:
            form.fields['year'].initial = get_fiscal_year(request)

        form.fields['year'].disabled = True


        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"
        else:
            popup = ""

        return render(request, 'accounting/lot_edit.html', {
            'form': form,
            'title': title,
            'popup': popup,
            "instance": instance,
        })

    elif request.method == "POST":
        new_post_data = request.POST.copy()
        new_post_data["year"] = get_fiscal_year(request)
        form = frm(new_post_data, instance=instance)
        if request.POST.get('delete'):
            if Transaction.objects.filter(lot=instance):
                form.add_error(None, "Cannot delete batch with linked transactions")
                return format_errors(request, form)

            delete_audit(request, instance)
            return delete_confirm(request, instance)


        if form.is_valid():
            obj = save_audit(request, form)
            return save_confirm(request, obj)

        else:
            return format_errors(request, form)




@get_logged_in
def account_index(request):
    account_data = Account.objects.all()

    report_id = str(uuid.uuid4())
    request.session[report_id] = json.dumps(list(account_data.values(
            "active", "type", "name", "county_name", "description",
            "county_description", "funding_profile__name", "writeoff_account",
        )),
        cls=DjangoJSONEncoder)

    return render(request, 'accounting/account_index.html', {
        'title' : 'Account',
        'account_data': account_data,
        'table_class' : 'listview-table',
        "report_id" : report_id,
        })


@get_logged_in
def account_edit(request, pk=None):
    mod = Account
    frm = AccountForm

    if pk:
        instance = get_object_or_404(mod, id=pk)
        title = "Account"
    else:
        instance = None
        title = "New Account"

    if request.method == "GET":
        form = frm(instance=instance)
        if pk:
            #prefil lookups
            form = set_autocomplete(instance, form, 'funding_profile')

        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"
        else:
            popup = ""

        return render(request, 'accounting/account_edit.html', {
            'form':form,
            'title':title,
            'popup' : popup,
            })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance)

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            obj = save_audit(request, form)
            return save_confirm(request, obj)
        else:
            return format_errors(request, form)




@get_logged_in
def rate_index(request):
    data = Rate.objects.order_by('active', 'name')
    return render(request, 'accounting/rate_index.html', {
        'title' : 'Rate',
        'data' : data,
        'table_class' : 'listview-table',
        })


@get_logged_in
def rate_edit(request, pk=None):
    mod = Rate
    frm = RateForm

    if pk:
        instance = get_object_or_404(mod, id=pk)
        title = "Rate"
    else:
        instance = None
        title = "New Rate"

    if request.method == "GET":
        form = frm(instance=instance)

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"

        form = set_autocomplete(instance, form, 'account')

        return render(request, 'accounting/rate_edit.html', {
            'form':form,
            'title':title,
            'popup':popup,
            })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance)

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            obj = save_audit(request, form)
            return save_confirm(request, obj)

        else:
            return format_errors(request, form)
