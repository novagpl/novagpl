"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from accounting.accounting_choices import *  # TODO FIX
from annoying.functions import get_object_or_None
from config.models import *  # TODO FIX
from django import forms
from django.conf import settings
from django.forms import (BooleanField, CharField, CheckboxInput, ChoiceField,
                          DateField, DateInput, HiddenInput, IntegerField,
                          ModelChoiceField, ModelForm, NumberInput, Select,
                          Textarea, TextInput, ClearableFileInput, DecimalField)
from report.models import *
from utils import (get_fiscal_period, get_fiscal_year, render_form, )
from validation import (
    validate_auth, validate_writeoff_account, validate_account_name,
    validate_transaction_date, validate_transaction_amount,
    validate_journal_entry_date, validate_budgetitem, validate_journal_trans)

from .models import *  # FIX THIS!!!

import client.client_choices as client_choices
import accounting.accounting_choices as accounting_choices





NULL_CHOICE = (None, "----------", )




class TransactionFileForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)
        render_form(self)


    class Meta:
        model = TransactionFile
        #exclude = ["signed_by", "caseworker_signature", "signature"]
        fields = [
            "date",
            "trans_file",
            "description",
            "uploaded_by",
            "transaction",
            "upload_date",
        ]

        widgets = {
            "date": DateInput(format=settings.DATE_FORMAT, attrs={
                'width': "auto", 'class': 'datepicker'}),
            "description": TextInput(attrs={"width": 100, "clear": "left "}),
            "trans_file": ClearableFileInput(attrs={"class": "file-upload"}),
            "uploaded_by": HiddenInput(),
            "transaction": HiddenInput(),
            "upload_date": HiddenInput(),
        }




class JournalFileForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)
        render_form(self)


    class Meta:
        model = JournalFile
        #exclude = ["signed_by", "caseworker_signature", "signature"]
        fields = [
            "date",
            "jf_file",
            "description",
            "uploaded_by",
            "journal_entry",
            "upload_date",
        ]

        widgets = {
            "date": DateInput(format=settings.DATE_FORMAT, attrs={
                'width': "auto", 'class': 'datepicker'}),
            "description": TextInput(attrs={"width": 100, "clear": "left "}),
            "jf_file": ClearableFileInput(attrs={"class": "file-upload"}),
            "uploaded_by": HiddenInput(),
            "journal_entry": HiddenInput(),
            "upload_date": HiddenInput(),
        }






class PayrollJeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PayrollJeForm, self).__init__(*args, **kwargs)
        render_form(self)


    class Meta:
        model = JournalEntry
        fields = (
            "payrolldist",
            "date",
            "fiscal_year",
            "fiscal_period",
            "description",
        )

        widgets = {
            "payrolldist": HiddenInput(),            
            "date": DateInput(format=settings.DATE_FORMAT, attrs={'width': "10rem", 'class': 'datepicker'}),
            "fiscal_year": NumberInput(attrs={"width": "6rem"}),
            "fiscal_period": NumberInput(attrs={"width": "6rem"}),
            "description": TextInput(attrs={"width": 50, "clear": "left"}),
        }




class PayrollForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PayrollForm, self).__init__(*args, **kwargs)
        render_form(self)


    class Meta:
        model = PayrollDist
        verbose_name = "Payroll Distribution"
        fields = (
            "date_from",
            "date_to",
            "paycheck_date",
        )

        widgets = {
            "date_from": DateInput(format=settings.DATE_FORMAT, attrs={'width': 50, 'class': 'datepicker'}),
            "date_to": DateInput(format=settings.DATE_FORMAT, attrs={'width': 50, 'class': 'datepicker'}),
            "paycheck_date": DateInput(format=settings.DATE_FORMAT, attrs={'width': 50, 'class': 'datepicker'}),
        }




class AccountForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AccountForm, self).__init__(*args, **kwargs)
        render_form(self)

    funding_profile_lookup = CharField(label='Funding Profile', required=False, widget=TextInput(attrs={
        "width": 50,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'FundingProfile',
        'fields': 'name, description',
        'order': 'name',
        'target': 'funding_profile',
        'autocomplete': 'off',
    }))

    class Meta:
        model = Account
        fields = (
            'active',
            'ma_account',
            "writeoff_account",
            "cash_account",
            'type',
            'name',
            'county_name',
            'description',
            'county_description',
            'funding_profile_lookup',
            'funding_profile',
            "cfda_number",
        )

        widgets = {
            'active': CheckboxInput(attrs={'width': 25}),
            'ma_account': CheckboxInput(attrs={'width': 25}),
            "writeoff_account": CheckboxInput(attrs={"width": 25}),
            "cash_account": CheckboxInput(attrs={"width": 25}),
            'name': TextInput(attrs={'width': 50}),
            'county_name': TextInput(attrs={'width': 50}),
            'description': TextInput(attrs={'width': 50}),
            'county_description': TextInput(attrs={'width': 50}),
            'funding_profile': HiddenInput(),
            "cfda_number": TextInput(attrs={"width": 50}),
        }

    def clean(self):
        self.cleaned_data = super(AccountForm, self).clean()
        validate_writeoff_account(self)
        validate_account_name(self)



class LotForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(LotForm, self).__init__(*args, **kwargs)
        render_form(self)

    class Meta:
        model = TransactionLot
        fields = ("active", "batch_type", "year", "name", "description")
        widgets = {
            "active": CheckboxInput(attrs={"width": 100}),
            "batch_type": Select(attrs={"width": 25}),
            "year": NumberInput(attrs={"width": 25}),
            "name": TextInput(attrs={"width": 50}),
            "description": TextInput(attrs={"width": 100}),
        }


class TransactionFilterForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(TransactionFilterForm, self).__init__(*args, **kwargs)
        render_form(self)

        #self.fields["account_type"].choices = ACCOUNT_TYPE

    journal_choices = (
        ("all", "All"),
        ("journaled", "Journaled"),
        ("unjournaled", "Unjournaled"),
    )

    journal_option = ChoiceField(choices=journal_choices, label="Journaled", widget=Select(attrs={"width": 50}))
    #verified = BooleanField(required=False, label="Include journal transactions", widget=CheckboxInput(attrs={'width': 50}))
    billable = BooleanField(required=False, label="Show billable and payments only", widget=CheckboxInput(attrs={"width": 50, "clear": "left"}))
    atp_only = BooleanField(required=False, label="Show ATP only", widget=CheckboxInput(attrs={"width": 50}))
    fiscal_year = IntegerField(required=False, widget=NumberInput(attrs={"width": 50}))
    fiscal_period = IntegerField(required=False, widget=NumberInput(attrs={"width": 50}))

    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        'width': 50,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name, birthdate',
        'order': 'last_name, first_name',
        'target': 'client',
        'autocomplete': 'off',
    }))

    vendor_lookup = CharField(label="Vendor", required=False, widget=TextInput(attrs={
        'width': 50,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'Vendor',
        'fields': 'name, vendor_number',
        'order': 'name',
        'target': 'vendor',
        'autocomplete': 'off',
    }))

    account_lookup = CharField(label="Account", required=False, widget=TextInput(attrs={
        'width': 50,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'Account',
        'fields': 'name, description, county_name, county_description',
        'order': 'name',
        'target': 'account',
        'autocomplete': 'off',
    }))

    account_type = ChoiceField(label="Account type", required=False, widget=Select(attrs={
        "width": 50,
    }))
    account_type.choices = (
            (None, "---------"), 
            ('0', 'Revenue'),
            ('1', 'Expense'),
    )


    lot_lookup = CharField(label="Batch", required=False, widget=TextInput(attrs={
        'width': 50,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'TransactionLot',
        'fields': 'name, description',
        'order': 'name, description',
        'target': 'lot',
        'autocomplete': 'off',
    }))

    invoice = CharField(required=False, widget=TextInput(attrs={
        "width": 50
    }))



    caseworker = ModelChoiceField(
        label="Caseworker", required=False, widget=Select(attrs={"width": 50}),
        queryset=UserConfig.objects.filter(active=True, user__is_staff=False).order_by("full_name")
    )



    date_from = DateField(required=False, label="Service date from", widget=DateInput(format=settings.DATE_FORMAT, attrs={'width': 50, 'class': 'datepicker', "clear": "left"}))
    date_to = DateField(required=False, label="Service date to", widget=DateInput(format=settings.DATE_FORMAT, attrs={'width': 50, 'class': 'datepicker'}))

    client = CharField(required=False, label='client_hidden', widget=HiddenInput())
    account = CharField(required=False, label='account_hidden', widget=HiddenInput())
    vendor = CharField(required=False, label='vendor_hidden', widget=HiddenInput())
    lot = CharField(required=False, label='lot_hidden', widget=HiddenInput())



class TransactionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(TransactionForm, self).__init__(*args, **kwargs)       

        lock_form = False

        if self.instance and self.instance.id is not None:
            if self.instance.fiscal_year < get_fiscal_year(self.request):
                lock_form = True

            if self.instance.fiscal_year <= get_fiscal_year(self.request) and self.instance.fiscal_period < get_fiscal_period(self.request):
                lock_form = True

            if self.instance.journal_entry and self.instance.journal_entry.verified:
                lock_form = True


        if lock_form:
            render_form(self, locked=True)
        else:
            render_form(self, locked=False)      



    account_lookup = CharField(label="Account", required=True, widget=TextInput(attrs={
        'width': 30,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'Account',
        'fields': 'name, description, county_name, county_description',
        'order': 'name',
        'target': 'account',
        'autocomplete': 'off',
    }))

    authorization_lookup = CharField(label="Authorization", required=False, widget=TextInput(attrs={
        "width": 35,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'Authorization',
        'fields': 'date_from__year, id, vendor',
        'order': 'name',
        'target': 'authorization',
        'autocomplete': 'off',
    }))




    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        "width": 35,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name, birthdate',
        'order': 'last_name, first_name',
        'target': 'client',
        'autocomplete': 'off',
    }))



    vendor_lookup = CharField(label="Vendor", required=False, widget=TextInput(attrs={
        "width": 35,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'Vendor',
        'fields': 'name, vendor_number, city',
        'order': 'name',
        'target': 'vendor',
        'autocomplete': 'off',
    }))

    service_lookup = CharField(label="Service", required=False, widget=TextInput(attrs={
        "width": 30,
        "class": "lookup",
        "app": "client",
        "mod": "Service",
        "fields": "name, spc_code, procedure_code",
        "target": "service",
        "autocomplete": "off",
    }))

    lot_lookup = CharField(label="Batch", required=False, widget=TextInput(attrs={
        'width': 20,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'TransactionLot',
        'fields': 'name, description',
        'order': 'name, description',
        'target': 'lot',
        'autocomplete': 'off',
    }))

    def clean(self):
        self.cleaned_data = super(TransactionForm, self).clean()

        validate_transaction_date(self)
        validate_transaction_amount(self)



    class Meta:
        model = Transaction
        fields = [
            'fiscal_year',
            'fiscal_period',
            'date',
            'amount',
            'account_lookup',
            'account',
            'trans_type',
            "authorization_lookup",
            "authorization",
            'lot_lookup',
            'description',
            'lot',
            'client_lookup',
            'client',
            'service_date',
            "service_lookup",
            "service",
            "target_group",
            'vendor_lookup',
            'vendor',
            'invoice',
            'unit_type',
            "units",
            "client_billable",
            'journal_entry',
        ]

        widgets = {
            'fiscal_year': NumberInput(attrs={"width": 10}),
            'fiscal_period': NumberInput(attrs={"width": 10}),
            'amount': NumberInput(attrs={"width": 20}),
            'date': DateInput(format=settings.DATE_FORMAT, attrs={'width': 15, 'class': 'datepicker'}),
            'trans_type': Select(attrs={'width': 15}),
            'service_date': DateInput(format=settings.DATE_FORMAT, attrs={'width': 15, 'class': 'datepicker'}),
            'invoice': TextInput(attrs={'width': 20}),
            'unit_type': Select(attrs={'width': 15}),
            "units": NumberInput(attrs={"width": 10}),
            "client_billable": CheckboxInput(attrs={"width": 25, "clear": "left"}),
            "description": TextInput(attrs={"width": 45}),
            "target_group": Select(attrs={"width": 20}),
            'account': HiddenInput(),
            'vendor': HiddenInput(),
            'client': HiddenInput(),
            'journal_entry': HiddenInput(),
            "service": HiddenInput(),
            'lot': HiddenInput(),
            "authorization": HiddenInput(),
        }




class PayrollTransactionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(PayrollTransactionForm, self).__init__(*args, **kwargs)

        render_form(self)



    employee_lookup = CharField(label="Employee", required=True, widget=TextInput(attrs={
        "width": 100,
        "class": "lookup",
        "app": "accounting",
        "mod": "Employee",
        "fields": "last_name, first_name",
        "order": "last_name, first_name",
        "target": "employee",
        "autocomplete": "off",
    }))


    lot_lookup = CharField(label="Batch", required=True, widget=TextInput(attrs={
        'width': 30,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'TransactionLot',
        'fields': 'name, description',
        'order': 'name, description',
        'target': 'lot',
        'autocomplete': 'off',
    }))

    account_lookup = CharField(label="Account", required=True, widget=TextInput(attrs={
        'width': 75,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'Account',
        'fields': 'name, description, county_name, county_description',
        'order': 'name',
        'target': 'account',
        'autocomplete': 'off',
    }))




    class Meta:
        model = Transaction
        fields = (
            "fiscal_year",
            "fiscal_period",
            "date",
            "employee",
            "employee_lookup",
            "amount",
            "account_lookup",
            "trans_type",
            "lot_lookup",
            "description",
            "lot",        
            "account",
            "journal_entry",
        )

        widgets = {
            "fiscal_year": NumberInput(attrs={"width": 25}),
            "fiscal_period": NumberInput(attrs={"width": 25}),
            "amount": NumberInput(attrs={"width": 25}),
            "date": DateInput(format=settings.DATE_FORMAT, attrs={"width": 25, "class": "datepicker"}),
            "trans_type": Select(attrs={"width": 20}),
            "description": TextInput(attrs={"width": 50}),
            "account": HiddenInput(),
            "journal_entry": HiddenInput(),
            "lot": HiddenInput(),
            "employee": HiddenInput(),
        }





class ClientPaymentForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(ClientPaymentForm, self).__init__(*args, **kwargs)

        render_form(self)


    account_lookup = CharField(label="Account", required=True, widget=TextInput(attrs={
        'width': 80,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'Account',
        'fields': 'name, description, county_name, county_description',
        'order': 'name',
        'target': 'account',
        'autocomplete': 'off',
    }))
    '''
    vendor_lookup = CharField(label="Vendor", required=False, widget=TextInput(attrs={
        "width" : 50,
        'class' : 'lookup',
        'app' : 'accounting',
        'mod' : 'Vendor',
        'fields' : 'name, vendor_number, city',
        'order' : 'name',
        'target' : 'vendor',
        'autocomplete' : 'off',
        }))
    '''

    client_lookup = CharField(label="Client", required=True, widget=TextInput(attrs={
        "width": 100,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name, birthdate',
        'order': 'last_name, first_name',
        'target': 'client',
        'autocomplete': 'off',
    }))

    '''
    service_lookup = CharField(label="Service", required=False, widget=TextInput(attrs={
        "width" : 50,
        "class" : "lookup",
        "app" : "client",
        "mod" : "Service",
        "fields" : "name, spc_code, procedure_code",
        "target" : "service",
        "autocomplete" : "off",
        }))
    '''


    lot_lookup = CharField(label="Batch", required=True, widget=TextInput(attrs={
        'width': 50,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'TransactionLot',
        'fields': 'name, description',
        'order': 'name, description',
        'target': 'lot',
        'autocomplete': 'off',
    }))


    def clean(self):
        self.cleaned_data = super(ClientPaymentForm, self).clean()

        validate_transaction_date(self)
        validate_transaction_amount(self)

    class Meta:
        model = Transaction
        fields = (
            'client',
            'client_lookup',
            'fiscal_year',
            'fiscal_period',
            'date',
            'amount',
            'account_lookup',
            'trans_type',
            'lot_lookup',
            'description',
            'lot',
            #'service_date',
            #"service",

            #"client_billable",
            #'vendor_lookup',
            #'vendor',
            #"service_lookup",
            #'invoice',
            #'unit',
            'account',
            'journal_entry',
        )

        widgets = {
            'fiscal_year': NumberInput(attrs={"width": 25}),
            'fiscal_period': NumberInput(attrs={"width": 25}),
            'amount': NumberInput(attrs={"width": 25}),
            'date': DateInput(format=settings.DATE_FORMAT, attrs={'width': 25, 'class': 'datepicker'}),
            'trans_type': Select(attrs={'width': 20}),
            #'service_date' : DateInput(format=settings.DATE_FORMAT, attrs={'width':30, 'class':'datepicker'}),
            'lot': HiddenInput(),
            #'invoice' : TextInput(attrs={'width':50}),
            #'unit' : Select(attrs={'width':50}),
            #"client_billable" : CheckboxInput(attrs={"width" : 20 }),
            'account': HiddenInput(),
            #'vendor' : HiddenInput(),
            'client': HiddenInput(),
            'journal_entry': HiddenInput(),
            #"service" : HiddenInput(),
            "description": TextInput(attrs={"width": 50}),
        }


class FundingProfileForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(FundingProfileForm, self).__init__(*args, **kwargs)
        render_form(self)

    class Meta:
        model = FundingProfile
        fields = ("active", "name", "description")

        widgets = {
            "active": CheckboxInput(attrs={"width": 100}),
            "name": TextInput(attrs={"width": 50}),
            "description": TextInput(attrs={"width": 50}),
        }





class RateForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(RateForm, self).__init__(*args, **kwargs)
        render_form(self)

    account_lookup = CharField(label="Account", required=True, widget=TextInput(attrs={
        "width": 100,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'Account',
        'fields': 'name, description, county_name, county_description',
        'order': 'name',
        'target': 'account',
        'autocomplete': 'off',
    }))

    class Meta:
        model = Rate
        fields = (
            'active',
            "b3_rate",
            "b3_preadmit_rate",
            "clinic_preadmit_rate",
            "clts_rate",
            "clts_preadmit_rate",
            "crisis_rate",
            "cst_rate",
            "randi_rate",
            "tcm_rate",
            "year",
            'name',
            'amount',
            'interval',
            'account_lookup',
            'account',
        )

        widgets = {
            'active': CheckboxInput(attrs={'width': 100}),
            'b3_rate': CheckboxInput(attrs={'width': 33}),
            'b3_preadmit_rate': CheckboxInput(attrs={'width': 33}),
            'clinic_preadmit_rate': CheckboxInput(attrs={'width': 33}),
            'clts_rate': CheckboxInput(attrs={'width': 33}),
            'clts_preadmit_rate': CheckboxInput(attrs={'width': 33}),
            'crisis_rate': CheckboxInput(attrs={'width': 33}),
            'cst_rate': CheckboxInput(attrs={'width': 33}),
            'randi_rate': CheckboxInput(attrs={'width': 33}),
            'tcm_rate': CheckboxInput(attrs={'width': 33}),
            'year': NumberInput(attrs={'width': 15, "clear": "both"}),
            "name": TextInput(attrs={"width": 45}),
            'amount': NumberInput(attrs={'width': 20}),
            'interval': Select(attrs={'width': 20}),
            'account': HiddenInput(),
        }


class ContractForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(ContractForm, self).__init__(*args, **kwargs)

        if self.request.user.userconfig.edit_contracts is True:
            render_form(self)
        elif self.request.user.userconfig.user_type == "2":
            # We want caseworkers to be able to view contracts, but they
            # should not be able to edit
            render_form(self, locked=True)
        else:
            render_form(self, locked=True)


    vendor_lookup = CharField(label="Vendor", required=True, widget=TextInput(attrs={
        "width": 100,
        "class": "lookup",
        "app": "accounting",
        "mod": "Vendor",
        "fields": "name, vendor_number, city",
        "order": "name",
        "target": "vendor",
        "autocomplete": "off",
        "clear": "both",
    }))



    class Meta:
        model = Contract
        fields = (
            "contract_type",
            "year",
            "amount",
            "vendor",
            "vendor_lookup",
            "notes",
        )

        widgets = {
            "vendor": HiddenInput(),
            "contract_type": Select(attrs={"width": 33}),
            "year": NumberInput(attrs={"width": 33}),
            "amount": NumberInput(attrs={"width": 33}),
            "notes": Textarea(attrs={"rows": 2, "width": 100}),
        }


class ContractServiceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(ContractServiceForm, self).__init__(*args, **kwargs)

        '''
        self.fields["unit"].widget.choices = (
            (None, "----------"),
            ("hour", "Hour"),
            ("day", "Day"),
            ("each", "Each"),
        )
        '''

        if self.request.user.userconfig.edit_contracts is True:
            render_form(self)
        else:
            render_form(self, locked=True)


    class Meta:
        model = ContractService

        fields = (
            "contract",
            "service",
            "rate",
            "unit",
            "amount",
            "description",
        )

        widgets = {
            "contract": HiddenInput(),
            "service": Select(attrs={"width": 40}),
            "rate": NumberInput(attrs={"width": 20}),
            "unit": TextInput(attrs={
                "width": 20, "class": "datalist",
                "datalist": "Each, Hour, Day, Week, Month, Mile, Item, Meal, Episode"}),
            "amount": NumberInput(attrs={"width": 20}),
            "description": Textarea(attrs={"rows": 2, "width": 100}),
        }





class JournalEntryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(JournalEntryForm, self).__init__(*args, **kwargs)


        if self.instance is None:
            self.fields["verified"].widget = HiddenInput()
            render_form(self)
        else:
            render_form(self, self.instance.verified)

        # Hide the verify field if there are no transactions
        if self.instance and self.instance.has_transactions() is not True:
            self.fields["verified"].widget = HiddenInput()


    def clean(self):
        if self.locked:
            # don't do anything, it's locked
            # TODO probably remove this, if the form is locked then this
            # shouldn't ever get hit
            return

        self.cleaned_data = super(JournalEntryForm, self).clean()
        validate_journal_entry_date(self)


    class Meta:
        model = JournalEntry
        fields = (
            'date',
            "fiscal_year",
            "fiscal_period",
            'description',
            'verified',
        )

        widgets = {
            'date': DateInput(format=settings.DATE_FORMAT, attrs={'width': 33, 'class': 'datepicker'}),
            "fiscal_year": NumberInput(attrs={"width": 33}),
            "fiscal_period": NumberInput(attrs={"width": 33}),
            'description': TextInput(attrs={'width': 100, 'clear': 'both'}),
            'verified': CheckboxInput(attrs={'width': 100, 'clear': 'both'}),
        }


class JournalTransForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(JournalTransForm, self).__init__(*args, **kwargs)

        if (kwargs.get("instance") and kwargs.get("instance").journal_entry and
                kwargs.get("instance").journal_entry.verified):
            render_form(self, locked=True)
        elif kwargs.get("instance") and kwargs.get("instance").case_note:
            render_form(self, locked=True)
        else:
            render_form(self)

    def clean(self):
        self.cleaned_data = super(JournalTransForm, self).clean()

        if self.cleaned_data.get("lot") == "":
            self.cleaned_data["lot"] = None
        else:
            self.cleaned_data["lot"] = get_object_or_None(TransactionLot, id=self.cleaned_data.get("lot"))

        validate_journal_trans(self)


    account_lookup = CharField(label="Account", required=True, widget=TextInput(attrs={
        "width": 80,
        "class": "lookup",
        "app": "accounting",
        "mod": "Account",
        "fields": "name, description, county_name, county_description",
        "order": "name",
        "target": "account",
        "autocomplete": "off",
    }))

    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        "width": 50,
        "class": "lookup",
        "app": "client",
        "mod": "Client",
        "fields": "last_name, first_name, birthdate",
        "order": "last_name, first_name",
        "target": "client",
        "autocomplete": "off",
    }))

    vendor_lookup = CharField(label="Vendor", required=False, widget=TextInput(attrs={
        "width": 50,
        "class": "lookup",
        "app": "accounting",
        "mod": "Vendor",
        "fields": "name, vendor_number, city",
        "order": "name",
        "target": "vendor",
        "autocomplete": "off",
    }))



    lot_lookup = CharField(label="Batch", required=False, widget=TextInput(attrs={
        "width": 50,
        "class": "lookup",
        "app": "accounting",
        "mod": "TransactionLot",
        "fields": "name, description",
        "order": "name, description",
        "target": "lot",
        "autocomplete": "off",
    }))

    lot = CharField(required=False, widget=HiddenInput())

    class Meta:
        model = Transaction
        fields = (
            "date",
            "fiscal_year",
            "fiscal_period",
            "amount",
            "journal_entry",
            "account_lookup",
            "account",
            "trans_type",
            "client_lookup",
            "client",
            "vendor_lookup",
            "vendor",
            "service_date",
            "unit_type",
            "lot",
            "lot_lookup",
            "invoice",
            "description",
        )

        widgets = {
            "date": DateInput(format=settings.DATE_FORMAT, attrs={"width": 25, "class": "datepicker"}),
            "fiscal_year": NumberInput(attrs={"width": 25}),
            "fiscal_period": NumberInput(attrs={"width": 25}),
            "amount": NumberInput(attrs={"width": 25}),
            "trans_type": Select(attrs={"width": 20}),
            "service_date": DateInput(format=settings.DATE_FORMAT, attrs={"width": 50, "class": "datepicker"}),
            "invoice": TextInput(attrs={"width": 50}),
            "unit_type": Select(attrs={"width": 50}),
            "description": TextInput(attrs={"width": 100}),
            "account": HiddenInput(),
            "vendor": HiddenInput(),
            "client": HiddenInput(),
            "journal_entry": HiddenInput(),
        }







class NewAuthForm(forms.Form):
    # This is the form for creating new authorizations. It's not a model form
    # because it handles data for optional new client and/or new vendor
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        self.instance = kwargs.pop("instance", None)
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)

        if self.locked:
            render_form(self, locked=True)
        else:
            render_form(self)



    def clean(self):
        self.cleaned_data = super().clean()
        validate_auth(self)


    # These are the regular auth fields. Typically I'd use a modelForm but
    # we want to be able to collect a whole bunch of fields for this view
    # so using a regular manually-linked form
    auth_caseworker = ModelChoiceField(
        label="Caseworker",
        required=True,
        queryset=UserConfig.objects.filter(active=True, user__is_staff=False).order_by("full_name"),
        widget=Select())

    auth_client = CharField(required=False, widget=HiddenInput())
    auth_client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name',
        'order': 'name',
        'target': 'auth_client',
        'autocomplete': 'off',
    }))

    auth_account = CharField(required=False, widget=HiddenInput())
    auth_account_lookup = CharField(label="Account", required=True, widget=TextInput(attrs={
        "class": "lookup",
        "app": "accounting",
        "mod": "Account",
        "fields": "name, description, county_name, county_description",
        "order": "name",
        "target": "auth_account",
        "autocomplete": "off",
    }))


    auth_vendor = CharField(required=False, widget=HiddenInput())
    auth_vendor_lookup = CharField(label="Vendor", required=True, widget=TextInput(attrs={
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'Vendor',
        'fields': 'name, vendor_number',
        'order': 'name',
        'target': 'auth_vendor',
        'autocomplete': 'off',
    }))






    auth_date_from = DateField(
        label="Valid from",
        required=True,
        widget=DateInput(format=settings.DATE_FORMAT, attrs={"class": "datepicker"}))

    auth_date_to = DateField(
        label="Valid to",
        required=True,
        widget=DateInput(format=settings.DATE_FORMAT, attrs={"class": "datepicker"}))


    auth_week = CharField(
        label="Week",
        required=False,
        widget=TextInput(attrs={"width": 20}))


    auth_amount = DecimalField(
        label="Amount",
        required=True,
        widget=NumberInput())

    auth_unit_type = CharField(
        label="Unit type",
        required=True,
        widget=Select(choices=(NULL_CHOICE, ) + accounting_choices.TRANS_UNIT))

    auth_units = IntegerField(
        label="Units",
        required=True,
        widget=NumberInput())

    auth_service = CharField(required=False, widget=HiddenInput())
    auth_service_lookup = CharField(label="Service", required=True, widget=TextInput(attrs={
        "class": "lookup",
        "app": "client",
        "mod": "Service",
        "fields": "name, spc_code, procedure_code",
        "target": "auth_service",
        "autocomplete": "off",
    }))

    auth_service_date_from = DateField(
        label="Service date from",
        required=True,
        widget=DateInput(format=settings.DATE_FORMAT, attrs={"class": "datepicker"}))

    auth_service_date_to = DateField(
        label="Service date to",
        required=True,
        widget=DateInput(format=settings.DATE_FORMAT, attrs={"class": "datepicker"}))






    auth_target_group = CharField(
        label="Target Group",
        required=False,
        widget=Select(choices=(NULL_CHOICE, ) + accounting_choices.FULL_TARGET_GROUP))

    auth_wic_cat = CharField(
        label="WIC",
        required=False,
        widget=Select(choices=(NULL_CHOICE, ) + Authorization.wic_choices))

    auth_iii_b_cat = CharField(
        label="III B",
        required=False,
        widget=Select(choices=(NULL_CHOICE, ) + Authorization.iii_b_choices))

    auth_iii_e_cat = CharField(
        label="III E",
        required=False,
        widget=Select(choices=(NULL_CHOICE, ) + Authorization.iii_e_choices))

    auth_cst_cat = CharField(
        label="CST",
        required=False,
        widget=Select(choices=(NULL_CHOICE, ) + Authorization.cst_choices))

    auth_elder_abuse_cat = CharField(
        label="Elder Abuse",
        required=False,
        widget=Select(choices=(NULL_CHOICE, ) + Authorization.elder_abuse_choices))





    auth_notes = CharField(
        label="Notes",
        required=False,
        widget=Textarea(attrs={"rows": 2}))








    # Need some basic fields for client. If the users need to add more
    # details for the client, they can do so at the client's face sheet
    client_last_name = CharField(
        label="Last",
        required=False,
        widget=TextInput())

    client_first_name = CharField(
        label="First",
        required=False,
        widget=TextInput())

    client_middle_name = CharField(
        label="Middle",
        required=False,
        widget=TextInput())

    client_birthdate = DateField(
        label="Birthdate",
        required=False,
        widget=DateInput(format=settings.DATE_FORMAT, attrs={
            "class": "datepicker"
        }))

    client_gender = CharField(
        label="Gender at birth",
        required=False,
        widget=Select(choices=(NULL_CHOICE, ) + client_choices.GENDER))


    # These are the vendor fields if the user wants to create a new vendor
    # when creating the auth.
    # As of (2024-08-09 we only want to be able to create clients on the fly)
    '''
    ven_name = CharField(
        label="Name",
        required=False,
        widget=TextInput())

    ven_vendor_number = CharField(
        label="Vendor number",
        required=False,
        widget=TextInput())

    ven_street = CharField(
        label="Street",
        required=False,
        widget=TextInput())

    ven_suite = CharField(
        label="Suite",
        required=False,
        widget=TextInput())

    ven_city = CharField(
        label="City",
        required=False,
        widget=TextInput())

    ven_state = CharField(
        label="State",
        required=False,
        widget=Select(choices=list((NULL_CHOICE, )) + client_choices.STATE))

    ven_zip = CharField(
        label="Zip",
        required=False,
        widget=TextInput())

    ven_etf_payment = BooleanField(
        label="ETF payments",
        required=False,
        widget=CheckboxInput())
    '''




class AuthFileForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)

        if self.locked:
            render_form(self, locked=True)
        else:
            render_form(self)



    class Meta:
        model = AuthFile

        fields = [
            "date",
            "af_file",
            "description",
            "uploaded_by",
            "authorization",
            "upload_date",
        ]

        widgets = {
            "date": DateInput(format=settings.DATE_FORMAT, attrs={
                'width': "auto", 'class': 'datepicker'}),
            "description": TextInput(attrs={"width": 100, "clear": "left "}),
            "af_file": ClearableFileInput(attrs={"class": "file-upload"}),
            "uploaded_by": HiddenInput(),
            "authorization": HiddenInput(),
            "upload_date": HiddenInput(),
        }






class AuthForm(ModelForm):
    # This form is for editing existing auths. If you're looking for the
    # non-ModelForm that is more complex and handles the creation of new
    # auths it's called NewAuthForm
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)



        if self.locked:
            render_form(self, locked=True)
        else:
            render_form(self)



    account_lookup = CharField(label="Account", required=False, widget=TextInput(attrs={
        "width": 50,
        "class": "lookup",
        "app": "accounting",
        "mod": "Account",
        "fields": "name, description, county_name, county_description",
        "order": "name",
        "target": "account",
        "autocomplete": "off",
    }))

    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        "width": 50,
        "class": "lookup",
        "app": "client",
        "mod": "Client",
        "fields": "last_name, first_name, middle_name, birthdate",
        "order": "last_name, first_name, middle_name",
        "target": "client",
        "autocomplete": "off",
    }))

    vendor_lookup = CharField(label="Vendor", required=True, widget=TextInput(attrs={
        "width": 50,
        'class': 'lookup',
        'app': 'accounting',
        'mod': 'Vendor',
        'fields': 'name, vendor_number',
        'order': 'name',
        'target': 'auth_vendor',
        'autocomplete': 'off',
    }))

    service_lookup = CharField(label="Service", required=True, widget=TextInput(attrs={
        "width": 20,
        "class": "lookup",
        "app": "client",
        "mod": "Service",
        "fields": "name, spc_code, procedure_code",
        "target": "auth_service",
        "autocomplete": "off",
    }))




    class Meta:
        model = Authorization

        fields = (
            "date_from",
            "date_to",
            "week",
            "caseworker",
            "client_lookup", "client",
            "account_lookup", "account",
            "vendor_lookup", "vendor",
            "service_date_from",
            "service_date_to",
            "service_lookup", "service",
            "amount",
            "unit_type",
            "units",

            "target_group",
            "wic_cat",
            "iii_b_cat",
            "iii_e_cat",
            "cst_cat",
            "elder_abuse_cat",
            "notes",
        )

        widgets = {
            "date_from": DateInput(format=settings.DATE_FORMAT, attrs={"width": 20, "class": "datepicker"}),
            "date_to": DateInput(format=settings.DATE_FORMAT, attrs={"width": 20, "class": "datepicker"}),
            "week": TextInput(attrs={"width": 20}),
            "caseworker": Select(attrs={"clear": "left", "width": 50}),
            # client,
            # account,
            # vendor,
            "service_date_from": DateInput(format=settings.DATE_FORMAT, attrs={"width": 20, "class": "datepicker"}),
            "service_date_to": DateInput(format=settings.DATE_FORMAT, attrs={"width": 20, "class": "datepicker"}),
            "amount": NumberInput(attrs={"width": 15}),
            "unit_type": Select(attrs={"width": 15}),
            "units": NumberInput(attrs={"width": 10}),

            "target_group": Select(attrs={"clear": "left", "width": 16}),
            "wic_cat": Select(attrs={"width": 16}),
            "iii_b_cat": Select(attrs={"width": 16}),
            "iii_e_cat": Select(attrs={"width": 16}),
            "cst_cat": Select(attrs={"width": 16}),
            "elder_abuse_cat": Select(attrs={"width": 16}),

            "notes": Textarea(attrs={"width": 100, "rows": 2}),

            "client": HiddenInput(),
            "account": HiddenInput(),
            "contract": HiddenInput(),
            "vendor": HiddenInput(),
            "service": HiddenInput(),
        }












class AuthorizationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        super(AuthorizationForm, self).__init__(*args, **kwargs)

        if self.request.user.userconfig.edit_contracts is True:
            render_form(self)
        else:
            render_form(self, locked=True)




    account_lookup = CharField(label="Account", required=False, widget=TextInput(attrs={
        "width": 50,
        "class": "lookup",
        "app": "accounting",
        "mod": "Account",
        "fields": "name, description, county_name, county_description",
        "order": "name",
        "target": "account",
        "autocomplete": "off",
    }))

    client_lookup = CharField(label="Client", required=False, widget=TextInput(attrs={
        "width": 50,
        "class": "lookup",
        "app": "client",
        "mod": "Client",
        "fields": "last_name, first_name, middle_name, birthdate",
        "order": "last_name, first_name, middle_name",
        "target": "client",
        "autocomplete": "off",
    }))



    class Meta:
        model = Authorization
        fields = (
            "date_from",
            "date_to",
            "amount",
            "vendor",
            "service",
            "account",
            "account_lookup",
            "client",
            "client_lookup",
            "rate",
            "target_group",
            "unit_type",
            "units",
            "contract",
        )

        widgets = {
            "date_from": DateInput(format=settings.DATE_FORMAT, attrs={"width": 33, "class": "datepicker"}),
            "date_to": DateInput(format=settings.DATE_FORMAT, attrs={"width": 33, "class": "datepicker"}),
            "amount": NumberInput(attrs={"width": 33}),
            "vendor": Select(attrs={"width": 50}),
            "service": Select(attrs={"width": 50}),
            "rate": NumberInput(attrs={"width": 50}),
            "target_group": Select(attrs={"width": 50}),
            "unit_type": Select(attrs={"width": 50}),
            "units": NumberInput(attrs={"width": 50}),


            "client": HiddenInput(),
            "account": HiddenInput(),
            "contract": HiddenInput(),
        }


class BudgetForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BudgetForm, self).__init__(*args, **kwargs)
        render_form(self)

    class Meta:
        model = Budget
        fields = ("active", "year", "name", "description")
        widgets = {
            "active": CheckboxInput(attrs={"width": 100}),
            "year": NumberInput(attrs={"width": 25}),
            "name": TextInput(attrs={"width": 75}),
            "description": Textarea(attrs={"width": 100, "rows": 2}),
        }


class BudgetItemForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BudgetItemForm, self).__init__(*args, **kwargs)
        render_form(self)

    account_lookup = CharField(label="Account", required=True, widget=TextInput(attrs={
        "class": "lookup",
        "app": "accounting",
        "mod": "Account",
        "fields": "name, description, county_name, county_description",
        "order": "name",
        "target": "account",
        "autocomplete": "off",
        "width": 75,
    }))

    def clean(self):
        self.cleaned_data = super(BudgetItemForm, self).clean()
        validate_budgetitem(self)

    class Meta:
        model = BudgetItem
        fields = ("account", "account_lookup", "amount", "budget")

        widgets = {
            "account": HiddenInput(),
            "budget": HiddenInput(),
            "amount": NumberInput(attrs={"width": 25}),
        }
