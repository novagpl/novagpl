"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.20 on 2024-11-07 15:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0156_rename_auth_saccheckwriteline_authorization'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transactionlot',
            name='batch_type',
            field=models.CharField(choices=[('ach', 'ACH'), ('ar', 'AR'), ('misc', 'MISC'), ('payroll', 'Payroll'), ('wisc', 'WISC')], max_length=255, null=True, verbose_name='Batch type'),
        ),
    ]
