"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.20 on 2024-10-24 22:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0151_auto_20241024_1646'),
    ]

    operations = [
        migrations.RenameField(
            model_name='saccheckwriteline',
            old_name='number_of_days',
            new_name='num_days',
        ),
        migrations.AddField(
            model_name='saccheckwriteline',
            name='county_payee_id',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='Payee ID'),
        ),
        migrations.AlterField(
            model_name='saccheckwriteline',
            name='payee_id',
            field=models.CharField(default='', max_length=10, verbose_name='Payee ID'),
            preserve_default=False,
        ),
    ]
