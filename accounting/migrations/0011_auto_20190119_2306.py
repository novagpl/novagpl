"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 2.1.4 on 2019-01-20 05:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0010_remove_account_payment_account'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='transaction',
            name='note',
        ),
        migrations.AlterField(
            model_name='account',
            name='county_description',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='County Description'),
        ),
        migrations.AlterField(
            model_name='account',
            name='county_name',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='County Name'),
        ),
    ]
