"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 2.2.4 on 2019-12-30 22:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0034_transaction_no_of_units'),
    ]

    operations = [
        migrations.RenameField(
            model_name='transaction',
            old_name='no_of_units',
            new_name='units',
        ),
        migrations.RemoveField(
            model_name='transaction',
            name='unit',
        ),
        migrations.AddField(
            model_name='transaction',
            name='unit_type',
            field=models.CharField(blank=True, choices=[('0', 'None'), ('1', 'Each'), ('2', 'Hour'), ('3', 'Day'), ('4', 'Week'), ('5', 'Month'), ('6', 'Mile'), ('7', 'Item'), ('8', 'Meal'), ('9', 'Episode')], max_length=2, null=True, verbose_name='Unit Type'),
        ),
    ]
