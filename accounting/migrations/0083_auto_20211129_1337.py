"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.9 on 2021-11-29 19:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounting', '0082_auto_20211129_1320'),
    ]

    operations = [
        migrations.RenameField(
            model_name='rate',
            old_name='clinic_default',
            new_name='clinic_rate',
        ),
        migrations.RenameField(
            model_name='rate',
            old_name='crisis_default',
            new_name='crisis_rate',
        ),
        migrations.RemoveField(
            model_name='rate',
            name='clts_default',
        ),
        migrations.RemoveField(
            model_name='rate',
            name='cst_default',
        ),
        migrations.RemoveField(
            model_name='rate',
            name='randi_default',
        ),
        migrations.RemoveField(
            model_name='rate',
            name='tcm_default',
        ),
        migrations.AddField(
            model_name='rate',
            name='clts_rate',
            field=models.BooleanField(default=False, verbose_name='CLTS rate'),
        ),
        migrations.AddField(
            model_name='rate',
            name='cst_rate',
            field=models.BooleanField(default=False, verbose_name='CST rate'),
        ),
        migrations.AddField(
            model_name='rate',
            name='randi_rate',
            field=models.BooleanField(default=False, verbose_name='R & I rate'),
        ),
        migrations.AddField(
            model_name='rate',
            name='tcm_rate',
            field=models.BooleanField(default=False, verbose_name='TCM rate'),
        ),
    ]
