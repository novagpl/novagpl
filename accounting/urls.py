"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from django.urls import path
from django.views.generic.base import RedirectView, TemplateView
from . import views

app_name = 'accounting'

urlpatterns = [
    #path('', RedirectView.as_view(url='/loginsort', permanent=False), name='accounting_index'),
    path('', views.accounting_index, name='accounting_index'),

    path('account/', views.account_index, name='account_index'),
    path('account/<int:pk>/', views.account_edit, name='account_edit'),
    path('account/new/', views.account_edit, name='account_new'),

    # "Auth" is the new (2024-08-06) authorization system. The paths and views
    # for 'authorization' exist as an interface for working with authorizations
    # within the confines of contracts. These auths have their own views
    # coming from the auth list view.
    # The users' goal is to deprecate the existing authorizations, so hopefully
    # that will happen soon.
    path("auth/", views.auth_index, name="auth_index"),
    path("auth/<int:auth_id>/", views.auth_edit, name="auth_edit"),
    path("auth/new/", views.auth_new, name="auth_new"),
    path("auth/<int:auth_id>/reopen/", views.auth_reopen, name="auth_reopen"),
    path("auth_file/new/<int:auth_id>/", views.auth_file_edit, name="auth_file_new"),
    path("auth_file/<int:auth_file_id>/", views.auth_file_edit, name="auth_file_edit"),
    path("auth_file/<int:auth_file_id>/auth_files/<int:auth_file_id2>/<str:filename>", views.auth_file_file, name="auth_file_file"),
    path("auth_file/<int:auth_file_id>/auth_files/<int:auth_file_id2>/<str:filename>/", views.auth_file_file, name="auth_file_file"),
    path("auth_file/<int:auth_file_id>/file/", views.auth_file_file, name="auth_file_file_short"),
    path("auth_file/<int:auth_file_id>/view/", views.auth_file_view, name="auth_file_view"),

    #path("authorization/", views.authorization_index, name="authorization_index"),
    path("authorization/<int:pk>/", views.authorization_edit, name="authorization_edit"),
    path("authorization/new/", views.authorization_edit, name="authorization_new"),

    path('budget/', views.budget_index, name='budget_index'),
    path('budget/<int:pk>/', views.budget_edit, name='budget_edit'),
    path('budget/new/', views.budget_edit, name='budget_new'),
    path("budget/copy_existing/", views.budget_copy_existing, name="budget_copy_existing"),

    path('budget_item/<int:pk>/', views.budget_item_edit, name='budget_item_edit'),
    path('budget_item/new/', views.budget_item_edit, name='budget_item_new'),
    #path('budgetitem/<int:pk>', RedirectView.as_view(pattern_name='accounting:budget_item_edit', query_string=True)),

    path('contract/', views.contract_index, name='contract_index'),
    path('contract/all/', views.contract_index_all, name='contract_index_all'),
    path('contract/<int:pk>/', views.contract_edit, name='contract_edit'),
    path('contract/new/', views.contract_edit, name='contract_new'),

    path("contract_service/<int:contract_id>/add/", views.contract_service_edit, name="contract_service_new"),
    path("contract_service/<int:contract_id>/<int:contract_service_id>/", views.contract_service_edit, name="contract_service_edit"),


    path('funding_profile/', views.funding_profile_index, name='funding_profile_index'),
    path('funding_profile/<int:pk>/', views.funding_profile_edit, name='funding_profile_edit'),
    path('funding_profile/new/', views.funding_profile_edit, name='funding_profile_new'),

    path('lot/', views.lot_index, name='lot_index'),
    path('lot/<int:pk>/', views.lot_edit, name='lot_edit'),
    path('lot/new/', views.lot_edit, name='lot_edit_new'),
    path("lot/all/", views.lot_index_all, name="lot_index_all"),

    path('rate/', views.rate_index, name='rate_index'),
    path('rate/<int:pk>/', views.rate_edit, name='rate_edit'),
    path('rate/new/', views.rate_edit, name='rate_edit'),

    path('transaction/', views.transaction_index, name='transaction_index'),
    path('transaction/<int:pk>/', views.transaction_edit, name='transaction_edit'),
    path('transaction/new/', views.transaction_edit, name='transaction_new'),
    path('transaction/<int:pk>/unlink/', views.transaction_unlink, name="transaction_unlink"),

    path("transaction_file/new/<int:transaction_id>/", views.transaction_file_edit, name="transaction_file_new"),
    path("transaction_file/<int:transaction_file_id>/", views.transaction_file_edit, name="transaction_file_edit"),
    path("transaction_file/<int:transaction_file_id>/transaction_files/<int:transaction_file_id2>/<str:filename>", views.transaction_file_file, name="transaction_file_file"),
    path("transaction_file/<int:transaction_file_id>/transaction_files/<int:transaction_file_id2>/<str:filename>/", views.transaction_file_file, name="transaction_file_file"),
    path("transaction_file/<int:transaction_file_id>/file/", views.transaction_file_file, name="transaction_file_file_short"),
    path("transaction_file/<int:transaction_file_id>/view/", views.transaction_file_view, name="transaction_file_view"),

    path("payroll_transaction/new/", views.payroll_transaction_edit, name="payroll_transaction_new"),
    path("payroll_transaction/<int:pk>/", views.payroll_transaction_edit, name="payroll_transaction_edit"),

    path("client_payment/new/", views.client_payment_edit, name="client_payment_edit"),
    path("client_payment/<int:pk>/", views.client_payment_edit, name="client_payment_edit"),

    path('journal_entry/', views.journal_entry_index, name='journal_entry_index'),
    path("journal_entry/all/", views.journal_entry_index_all, name="journal_entry_index_all"),
    path('journal_entry/<int:pk>/', views.journal_entry_edit, name='journal_entry_edit'),
    path('journal_entry/new/', views.journal_entry_edit, name='journal_entry_new'),
    path('journal_entry/verify/', views.journal_entry_verify, name='journal_entry_verify'),
    path('journal_entry/<int:pk>/unlink_all/', views.journal_entry_unlink_all, name="journal_entry_unlink_all"),
    path("journal_entry/<int:pk>/export/", views.journal_entry_export, name="journal_entry_export"),
    path("journal_entry/unverify/<int:pk>/", views.journal_entry_unverify, name="journal_entry_unverify"),
    path("journal_entry/review/<int:journal_id>/", views.journal_entry_review, name="journal_entry_review"),

    path("journal_file/new/<int:journal_id>/", views.journal_file_edit, name="journal_file_new"),
    path("journal_file/<int:journal_file_id>/", views.journal_file_edit, name="journal_file_edit"),
    path("journal_file/<int:journal_file_id>/journal_files/<int:journal_file_id2>/<str:filename>", views.journal_file_file, name="journal_file_file"),
    path("journal_file/<int:journal_file_id>/journal_files/<int:journal_file_id2>/<str:filename>/", views.journal_file_file, name="journal_file_file"),
    path("journal_file/<int:journal_file_id>/file/", views.journal_file_file, name="journal_file_file_short"),
    path("journal_file/<int:journal_file_id>/view/", views.journal_file_view, name="journal_file_view"),

    path('journal_trans/<int:pk>/', views.journal_trans_edit, name='journal_edit'),
    path('journal_trans/new/', views.journal_trans_edit, name='journal_new'),

    path("payroll/", views.payroll_dist_index, name="payroll_dist_index"),
    path("payroll/<int:pk>/", views.payroll_dist_edit, name="payroll_dist_edit"),
    path("payroll/new/", views.payroll_dist_edit, name="payroll_dist_new"),

    path("payroll_je/<int:pk>/", views.payroll_je_edit, name="payroll_je_edit"),
    path("payroll_je/new/<int:parent_id>/<int:employee_id>/<int:_amount>/", views.payroll_je_edit, name="payroll_je_new"),

    path("atp_discharge/", views.atp_discharge, name="atp_discharge"),
]
