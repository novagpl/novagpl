"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


ACCOUNT_TYPE = (
    ('0', 'Revenue'),
    ('1', 'Expense'),
)



BATCH_TYPE = (
    ("ach", "ACH"),
    ("ar", "AR"),
    ("misc", "MISC"),
    ("payroll", "Payroll"),
    ("wisc", "WISC"),
)



BILLING_CODE = (
    ("T1016", "T1016"),
    ("T1017", "T1017"),
    ("P9999", "P9999"),
    ("S9484", "S9484"),
    ("P9999", "P9999"),
    ("H0030", "H0030"),
    ("H2011", "H2011"),
    ("unbillable", "N0000"),
)



EMPLOYEE_COMP_TYPE = (
    ("1", "Mileage"),
    ("2", "Per diem"),
    ("3", "Wages"),
)


# This is used for transactions and auths, at least for now
# request is per Cindy. They want a way to track target group on transactions
# but without having them entered in pps_episodes. This list is just
# PPS AODA and PPS CORE target groups combined
# 2020-04-27 per Cindy, adding this as a generic option on non-pps case
# notes
FULL_TARGET_GROUP = (
    ("1", "1 - Developmental disability"),
    ("4", "4 - Alcohol abuse"),
    ("5", "5 - Drug abuse"),
    ("6", "6 - Delinquent and Status Offenders"),
    ("7", "7 - SOR Grant Participant"),
    ("8", "8 - MAT in Jails Participant"),
    ("17", "17 - Intoxicated driver"),
    ("18", "18 - Alcohol and other drug abuse"),
    ("31", "31 - Mental health (DSS use only)"),
    ("57", "57 - Physical and sensory disability"),
    ("58", "58 - Adults and elderly"),
    ("61", "61 - Abused and Neglected Children"),
    ("64", "64 - Children and family"),
    ("72", "72 - Family member / other of DD client"),
    ("74", "74 - Family member / other of AODA client"),
    ("75", "75 - Family member / other of mental health client"),
    ("76", "76 - Family member / other of P and SD client"),
    ("77", "77 - Family member / other of adults and elderly client"),    
)



MONTH = (
    (1, "January"),
    (2, "February"),
    (3, "March"),
    (4, "April"),
    (5, "May",),
    (6, "June"),
    (7, "July"),
    (8, "August"),
    (9, "September"),
    (10, "October"),
    (11, "November"),
    (12, "December"),
)



RATE_INTERVAL = (
    ("0", "None"),
    ("1", "Hour"),
    ("2", "Day"),
    ("3", "Unit")
)

SAC_RMB_CAT = (
    ("1", "Group home"),
    ("2", "RCC"),
    ("3", "Adoption"),
    ("4", "Kinship care"),
    ("5", "Foster home"),
    ("6", "Other"),
    ("7", "Institutions"),
    ("9", "Sub guard"),
    ("10", "Shelter"),
    ("11", "Detention"),
    ("12", "Trial reunification"),
    ("13", "Supervised IL"),
    ("14", "Missing from OHC"),
    ("15", "Treatment foster care"),
)

SAC_TARGET_POP = (
    ("1", "CHIPS - Other"),
    ("2", "CHIPS - Abuse and neglect"),
    ("3", "Delinquency"),
    ("4", "Voluntary placement"),
    ("5", "JIPS"),
)

SAC_PAYMENT_TYPE = (
    ("1", "Basic"),
    ("2", "Supplemental"),
    ("3", "Exceptional"),
    ("4", "Admin"),
    ("7", "Overpayment adjustments"),
)



TRANS_TYPE = (
    ('0', 'Credit'),  # Revenue
    ('1', 'Debit'),  # Expense
)



TRANS_UNIT = (
    ('0', 'None'),
    ('1', 'Each'),
    ('10', "Quarter-Hour"),
    ('2', 'Hour'),
    ('3', 'Day'),
    ('4', 'Week'),
    ('5', 'Month'),
    ('6', 'Mile'),
    ('7', 'Item'),
    ('8', 'Meal'),
    ('9', 'Episode'),

)





