"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from decimal import Decimal
from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.utils import timezone

from config.models import UserConfig, AppConfig

from accounting.models import (
    Account, Authorization, Vendor,
    Budget, BudgetItem, Contract, ContractService, Rate,
    FundingProfile, TransactionLot, Transaction, JournalEntry,
    PayrollDist, Employee, )

from client.models import Service
import utils
from .urls import urlpatterns



class UnitCalculationTest(TestCase):
    def setUp(self):
        pass

    def test_clts_units(self):
        print("testing CLTS units calculation")

        """
            CLTS units are calculated by 1 unit every 15 minutes.
            Minutes    Units
            1-15        1
            16-30       2
            31-45       3
            46-60       4
        """

        # 0 minutes should be 0 units:
        self.assertEqual(utils.calculate_clts_units(0), Decimal('0'))

        # 1 minute should be 1 unit:
        self.assertEqual(utils.calculate_clts_units(1), Decimal('1'))
        # 15 minutes should be 1 unit:
        self.assertEqual(utils.calculate_clts_units(15), Decimal('1'))

        # 16 minutes should be 2 units:
        self.assertEqual(utils.calculate_clts_units(16), Decimal('2'))
        # 30 minutes should be 2 units:
        self.assertEqual(utils.calculate_clts_units(30), Decimal('2'))

        # 31 minutes should be 3 units:
        self.assertEqual(utils.calculate_clts_units(31), Decimal('3'))

        # 45 minutes should be 3 units:
        self.assertEqual(utils.calculate_clts_units(45), Decimal('3'))

        # 46 minutes should be 4 units:
        self.assertEqual(utils.calculate_clts_units(46), Decimal('4'))

        # 60 minutes should be 4 units
        self.assertEqual(utils.calculate_clts_units(60), Decimal('4'))

        # 61 minutes should be 5 units
        self.assertEqual(utils.calculate_clts_units(61), Decimal('5'))

        # 75 minutes should be 5 units
        self.assertEqual(utils.calculate_clts_units(75), Decimal('5'))

        # 76 minutes should be 6 units
        self.assertEqual(utils.calculate_clts_units(76), Decimal('6'))

        # 90 minutes should be 6 units
        self.assertEqual(utils.calculate_clts_units(90), Decimal('6'))

        # 91 minutes should be 7 units
        self.assertEqual(utils.calculate_clts_units(91), Decimal('7'))

        # 105 minutes should be 7 units
        self.assertEqual(utils.calculate_clts_units(105), Decimal('7'))

        # 106 minutes should be 8 units
        self.assertEqual(utils.calculate_clts_units(106), Decimal('8'))

        # 120 minutes should be 8 units
        self.assertEqual(utils.calculate_clts_units(120), Decimal('8'))

        # 121 minutes should be 9 units
        self.assertEqual(utils.calculate_clts_units(121), Decimal('9'))


    def test_b3_units(self):
        print("Testing B-3 units calculation")
        # B-3 Unites are calculated to 1 unit every 15 minutes with rounding
        # for less than 15 minutes:
        # Minutes    Units
        # 1-5        0.3
        # 6-10       0.7
        # 11-15      1.0

        # 0 minutes should be 0 units
        self.assertEqual(utils.calculate_b3_units(0), Decimal('0.0'))

        # 1 minutes should be 0.3 units
        self.assertEqual(utils.calculate_b3_units(1), Decimal('0.3'))
        # 5 minutes should be 0.3 units
        self.assertEqual(utils.calculate_b3_units(5), Decimal('0.3'))

        # 6 minutes should be 0.7 units
        self.assertEqual(utils.calculate_b3_units(6), Decimal('0.7'))
        # 10 minutes should be 0.7 units
        self.assertEqual(utils.calculate_b3_units(10), Decimal('0.7'))

        # 11 minutes should be 1 units
        self.assertEqual(utils.calculate_b3_units(11), Decimal('1.0'))
        # 15 minutes should be 1 units
        self.assertEqual(utils.calculate_b3_units(15), Decimal('1.0'))

        # 16 minutes should be 1.3 units
        self.assertEqual(utils.calculate_b3_units(16), Decimal('1.3'))
        # 20 minutes should be 1.3 units
        self.assertEqual(utils.calculate_b3_units(20), Decimal('1.3'))

        # 21 minutes should be 1.7 units
        self.assertEqual(utils.calculate_b3_units(21), Decimal('1.7'))
        # 25 minutes should be 1.7 units
        self.assertEqual(utils.calculate_b3_units(25), Decimal('1.7'))

        # 30 minutes should be 2.0 units
        self.assertEqual(utils.calculate_b3_units(30), Decimal('2.0'))



    def test_crisis_units(self):
        print("Testing Crisis units calculation")

        self.assertEqual(utils.calculate_crisis_units(0), Decimal('0'))
        self.assertEqual(utils.calculate_crisis_units(7), Decimal('0'))
        self.assertEqual(utils.calculate_crisis_units(8), Decimal('1'))
        self.assertEqual(utils.calculate_crisis_units(22), Decimal('1'))
        self.assertEqual(utils.calculate_crisis_units(23), Decimal('2'))
        self.assertEqual(utils.calculate_crisis_units(37), Decimal('2'))
        self.assertEqual(utils.calculate_crisis_units(38), Decimal('3'))
        self.assertEqual(utils.calculate_crisis_units(52), Decimal('3'))
        self.assertEqual(utils.calculate_crisis_units(53), Decimal('4'))
        self.assertEqual(utils.calculate_crisis_units(67), Decimal('4'))
        self.assertEqual(utils.calculate_crisis_units(68), Decimal('5'))
        self.assertEqual(utils.calculate_crisis_units(82), Decimal('5'))
        self.assertEqual(utils.calculate_crisis_units(233), Decimal('16'))
        self.assertEqual(utils.calculate_crisis_units(247), Decimal('16'))
        self.assertEqual(utils.calculate_crisis_units(248), Decimal('17'))



class GetNotLoggedInViewTests(TestCase):
    print("Testing redirect for not logged in accounting URLs")

    def setUp(self):
        pass



not_logged_in_urls = [
    "account",
    "account/1",
    "account/new",
    "auth",
    "auth/1",
    "auth/new",
    "authorization/1",
    "authorization/new",
    "budget",
    "budget/1",
    "budget/new",
    "budget/copy_existing",
    "budget_item/1",
    "budget_item/new",
    "contract",
    "contract/all",
    "contract/1",
    "contract/new",
    "contract_service/1/add",
    "contract_service/1/2",
    "funding_profile",
    "funding_profile/1",
    "funding_profile/new",
    "lot",
    "lot/1",
    "lot/new",
    "lot/all",
    "rate",
    "rate/1",
    "rate/new",
    "transaction",
    "transaction/1",
    "transaction/new",
    "transaction/1/unlink",
    "payroll_transaction/new",
    "payroll_transaction/1",
    "client_payment/new",
    "client_payment/1",
    "journal_entry",
    "journal_entry/all",
    "journal_entry/1",
    "journal_entry/new",
    "journal_entry/verify",
    "journal_entry/1/unlink_all",
    "journal_entry/1/export",
    "journal_entry/unverify/1",
    "journal_trans/1",
    "journal_trans/new",
    "payroll",
    "payroll/1",
    "payroll/new",
    "payroll_je/1",
    "payroll_je/new/1/2/3",
    "atp_discharge",
]

def create_not_logged_in_test_method(url):
    def test_to_add(self):
        response = self.client.get(f"/accounting/{url}/")
        self.assertEqual(response.status_code, 302)

    return test_to_add

# Build methods for each URL to test so the output is easier to read
# This is dynamically adding methods to the test case class, pretty neat
for url in not_logged_in_urls:
    test_method = create_not_logged_in_test_method(url)
    test_method.__name__ = f"test_not_logged_in_redirect__{url.replace('/', '-')}"
    setattr(GetNotLoggedInViewTests, test_method.__name__, test_method)






class GetLoggedInDecoratorTest(TestCase):
    print("Checking that all required views have the @get_logged_in decorator")
    # Loops through all the urlpatterns for this app and checks that each
    # function is properly wrapped by the get_logged_in decorator
    # Some views (external ones probably) may not need the get_logged_in wrapper
    # so they can be put into the get_logged_in_exceptions list

    def setUp(self):
        pass


def create_get_logged_in_test_methods(url):
    def get_logged_in_test_to_add(self):
        found = getattr(url.callback, "__get_logged_in", False)
        self.assertEqual(found, True)

    return get_logged_in_test_to_add


get_logged_in_exceptions = []
for url in urlpatterns:
    if url.name in get_logged_in_exceptions:
        continue

    test_method = create_get_logged_in_test_methods(url)
    test_method.__name__ = f"test_get_logged_in_decorator_exists__{url.name}"
    setattr(GetLoggedInDecoratorTest, test_method.__name__, test_method)





class Status200Test(TestCase):
    # This tests all the URLs in urlpatterns to make sure the page will load
    # with a properly configured user. Basically make sure the page can load
    # a GET request without crashing.
    print("Testing accounting URLs for crashes with GET requests")

    def setUp(self):
        self.client = Client()

        fy = AppConfig(
            key="fiscal_year_open_from",
            value="2023",
        )
        fy.save()

        fp = AppConfig(
            key="fiscal_period_open_from",
            value="1",
        )
        fp.save()

        self.tester = User.objects.create_user(
            "test@test.com", "test@test.com", "testpassword")

        uc = UserConfig(
            user=self.tester,
            full_name="Test User",
            user_type="1",
        )

        uc.save()

        account = Account(
            id=1,
            name="1-22-333-4444.9",
            county_name="4444-333-22-1.9",
        )
        account.save()

        service = Service(
            id=1,
            name="Test Service",
            spc_code="111.0",
        )
        service.save()

        rate = Rate(
            id=1,
            account=account,
            name="Test Rate",
            amount=100,
            interval="3",
            year=2023,
        )
        rate.save()

        vendor = Vendor(
            id=1,
            name="Test Vendor LLC",
            vendor_number="1234",
            street="123 Test Street",
            suite="Ste 1",
            city="Test City",
            state="WI",
            zip="12345-1234",
            notes="It's a test vendor"
        )
        vendor.save()

        auth = Authorization(
            id=1,
            vendor=vendor,
            service=service,
            amount=100,
            date_from="2023-01-01",
            date_to="2023-12-31",
        )
        auth.save()

        budget = Budget(
            id=1,
            name="Test Budget",
            year=2023,
            description="It's a test budget",
        )
        budget.save()

        budgetitem = BudgetItem(
            id=1,
            budget=budget,
            account=account,
            amount=100,
        )
        budgetitem.save()

        contract = Contract(
            id=1,
            contract_type="general",
            year=2023,
            vendor=vendor,
            amount=100,
            notes="This is a note for a test contract",
        )
        contract.save()

        contractservice = ContractService(
            id=1,
            contract=contract,
            service=service,
            amount=100,
            rate=100,
            unit="Test Unit",
        )
        contractservice.save()

        fundingprofile = FundingProfile(
            id=1,
            name="TestFP",
            description="This is a funding profile for unit tests",
        )
        fundingprofile.save()


        lot = TransactionLot(
            id=1,
            year=2023,
            name="Test Batch",
            description="This batch is for unit tests",
        )
        lot.save()

        employee = Employee(
            id=1,
            first_name="Archie",
            last_name="Tester",
            home_cash_account=account,
        )
        employee.save()

        journal = JournalEntry(
            id=1,
            date=timezone.now().date(),
            fiscal_year=2023,
            fiscal_period=1,
            description="A journal entry for unit tests",
            employee=employee,


        )

        journal.save()

        trans = Transaction(
            id=1,
            journal_entry=journal,
            trans_type='0',
            vendor=vendor,
            account=account,
            service=service,
            lot=lot,
            authorization=auth,
            date=timezone.now().date(),
            service_date=timezone.now().date(),
            amount=Decimal('100.00'),
            fiscal_year=2023,
            fiscal_period=1,
        )
        trans.save()

        payrolldist = PayrollDist(
            id=1,
            date_from="2023-01-01",
            date_to="2023-12-31",
            paycheck_date="2023-01-01",
        )
        payrolldist.save()




    def test_accounting_404(self):
        self.client.login(username=self.tester.username, password="testpassword")
        response = self.client.get('/accounting/a_url_that_is_definitely_not_a_teapot/')
        self.assertEqual(response.status_code, 404)

        response = self.client.get('/a_url_that_is_definitely_not_a_teapot/a_url_that_is_definitely_not_a_teapot/')
        self.assertEqual(response.status_code, 404)

        response = self.client.get('/a_url_that_is_definitely_not_a_teapot/')
        self.assertEqual(response.status_code, 404)





def create_status200_test_method(url):
    def status200_test_to_add(self):
        self.client.login(username=self.tester.username, password="testpassword")
        response = self.client.get(f"/accounting/{url}")
        self.assertEqual(response.status_code, 200)
    return status200_test_to_add




status200_urls = [
    "",
    "account/",
    "account/1/",
    "account/new/",
    "auth/",
    "auth/1/",
    "auth/new/",
    "authorization/1/",
    "authorization/new/",
    "budget/",
    "budget/1/",
    "budget/new/",
    "budget_item/1/",
    "budget_item/new/?budget=1",
    "contract/",
    "contract/1/",
    "contract/new/",
    "contract/all/",
    "contract_service/1/add/",
    "contract_service/1/1/",
    "funding_profile/",
    "funding_profile/1/",
    "funding_profile/new/",
    "lot/",
    "lot/1/",
    "lot/new/",
    "lot/all/",
    "rate/",
    "rate/1/",
    "rate/new/",
    "transaction/",
    "transaction/1/",
    "payroll_transaction/new/",
    "payroll_transaction/1/",
    "client_payment/new/",
    "client_payment/1/",
    "journal_entry/",
    #"journal_entry/all/",  # its a redirect
    "journal_entry/1/",
    "journal_entry/new/",
    #"journal_entry/1/export/",  # This area needs its own unit tests
    "journal_trans/1/",
    "journal_trans/new/?entry=1",
    "payroll/",
    "payroll/1/",
    "payroll/new/",
    "payroll_je/1/",
    #"payroll_je/new/1/1/100",  # This ends in a 301 and should have its own tests
]

for url in status200_urls:
    test_method = create_status200_test_method(url)
    test_method.__name__ = f"test_status200__{url.replace('/','-')}"
    setattr(Status200Test, test_method.__name__, test_method)






