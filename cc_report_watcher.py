"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


"""
    CC Report Watcher looks in the database for any unprocessed reports
    When it finds one, it runs the report and notifies the user
"""

import sys
import os
import time
import json
import traceback
from django.core.mail import send_mail





import django
from django.utils import timezone
from django.conf import settings
from annoying.functions import get_object_or_None


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nova.settings")
django.setup()

from report.models import *
from config.models import AppConfig
import report.views 







class RequestLike():
    POST = {}
    session = {}
    user = ""
    path = ""
    META = {}
    method = "POST"

    # Overriding the the built-in function so it always returns requestlike
    # so the view_audit() doesn't record this as a view. We don't want to
    # audit the page self-refreshing over and over.
    def build_absolute_uri(self):
        return "requestlike"


def get_app_config(key):
    res = AppConfig.objects.get(key=key)
    if res is None:
        return None
    elif res.value.lower() == "none":
        return None
    elif res.value.lower() == "true":
        return True
    elif res.value.lower() == "false":
        return False
    else:
        return res.value


def find_and_run_reports(found):


    if found:
        print("Found report: {}".format(found.report_name))

        # In theory, we will only be dealing with reports
        # if this changes, will need to think of something else.
        target = getattr(report.views, found.report_name)

        # Build out a faux request object to fool the view
        rlike = RequestLike()
        rlike.POST = json.loads(found.post)
        rlike.user = found.userconfig.user
        rlike.path = found.path

        report_start = timezone.now()

        found.status = "processing"
        found.start = report_start

        found_id = found.id

        found = found.save()

        # have to re-query, not entirely sure, probably a session thing??
        # TODO verify if this really needed and see if we can just
        # pass the found object instead
        found = get_object_or_None(CCReport, id=found_id)

        # This runs the actual report. It's important to pass the
        # cc_report we found, so the names can be lined up later
        #import pdb; pdb.set_trace()
        target(rlike, found.id)

        report_end = timezone.now()

        found.status = "complete"
        found.end = report_end
        found.save()

        print("Report {} complete in {}. Actual runtime: {}".format(
            found.report_name,
            found.get_total_seconds(),
            found.get_runtime(),
        ))

        # TODO send an email to the client IF they have it set
        if found.userconfig.send_email_when_report_ready and found.userconfig.report_email_threshold <= found.get_runtime():
            body = "<a href='{}/report/cc_report/watcher/'>{} {} is ready to view.</a>".format(
                get_app_config("url"), found.get_report_name(), found.id)
            body += "<br><br>Processing time: {} seconds.".format(found.get_runtime())
            body += "<br><br>Sincerely,<br>The Nova CC Reports Team"

            try:
                send_mail(
                    subject=f"{found.get_report_name()} {found.id} report is ready",
                    html_message=body,
                    message=body,
                    recipient_list=[found.userconfig.user.username, ],
                    from_email=settings.REPORTS_EMAIL,
                )
            except Exception as e:
                print(f"Failed to send email\n{e}")





if __name__ == "__main__":
    print("CC Report Watcher")
    start = timezone.now()

    print("Looking for reports to process...")

    # TODO move to a queue mgmt system
    while True:

        found = CCReport.objects.filter(
            status="submitted").order_by("submitted").first()

        if found:
            try:
                find_and_run_reports(found)
            except Exception as ex:
                ex_type, value, tb = sys.exc_info()
                msg = "Report Watcher Error<br><br>".replace("    ", "&nbsp;&nbsp;&nbsp;&nbsp;")
                msg += str(value.args[0])
                msg += traceback.format_exc()

                print(msg)

                try:
                    send_mail(
                        subject="Report Watcher Error {}: {}".format(ex_type.__name__, value),
                        html_message=msg,
                        message=msg.replace("<br>", "\n"),
                        recipient_list=settings.RECEIVER_ERROR_EMAILS,
                        from_email=settings.SENDER_ERROR_EMAIL,
                    )
                except Exception as e:
                    print(f"Failed to send email\n{e}")

                found.status = "error"
                found.save()




        time.sleep(1)

    print("Time's up. Exiting.")




