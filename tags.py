"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import os
import math
import hashlib
import datetime

from django import template
from django.utils.safestring import mark_safe
from django.contrib.staticfiles import finders

# nova_tags

register = template.Library()

@register.filter
def hours_and_minutes(value):
    if value:
        hours = math.floor(value / 60)
        leftover_minutes = value - (hours * 60)
        hours_and_minutes = f"{hours}:{leftover_minutes:02}"
        return hours_and_minutes
    else:
        return ''


@register.filter
def get_class(value):
    return value.__class__.__name__

@register.filter
def yesno(value):
    if value:
        return "Yes"
    else:
        return "No"

@register.filter
def iyesno(value):
    return yesno(value).lower()


@register.filter
def yes(value, arg=None):
    if value:
        if arg:
            return arg
        return "Yes"
    else:
        return ""

@register.filter
def chk(value, arg=None):
    #import pdb; pdb.set_trace()
    if value is True:
        html = '<span class="emr-report-chk checked"></span>'
    else:
        html = '<span class="emr-report-chk unchecked"></span>'

    return mark_safe(html)


@register.filter
def default_if_zero(value, arg):
    if value == 0 or value == "0.00":
        return arg
    else:
        #print(f"Non defaulted value: {value} {type(value)}")
        return value

@register.filter
def ext(value):
    if value:
        return f" ext {value}"
    else:
        return ""

@register.filter
def list_date(value):
    #import pdb; pdb.set_trace()
    if isinstance(value, datetime.date):
        date_string = f'<span class="d-none">{value.strftime("%Y%m%d_%H%M%S")}</span>{value.strftime("%m/%d/%Y")}'

        return mark_safe(date_string)
    else:
        return None



@register.filter
def staticver(filename):
    """ Returns a hash based off the file's last modified time
        this is to try to prevent browsers from caching old js and css
    """
    file = finders.find(filename)
    if file:
        # get last modified date
        modified = os.path.getmtime(file)
        hash = hashlib.md5(str(modified).encode("UTF-8", "ignore")).hexdigest()

        return "/static/{}?ver={}".format(filename, hash)

