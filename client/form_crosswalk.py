"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import datetime as dt

from annoying.functions import get_object_or_None
from client.models import Address, Client
from django.conf import settings
from django.http import Http404
from utils import docx_replace, get_app_config, get_user_config


def form_crosswalk(request, name):
    name = name.lower()
    agency_name = get_app_config('agency_name')
    agency_phone = get_app_config('agency_phone')
    user = get_user_config(request)
    client = get_object_or_None(Client, id=request.GET.get('client'))

    address = None
    if client is not None:
        address = client.get_address()



    if address is None:
        client_address = ''
    else:
        client_address = address.get_long_address()

    rep = {
        'AGENCY_NAME': agency_name,
        'AGENCY_PHONE': agency_phone,
        'PARENT': client.parent_guardian_name or '',
        'PARENT_NAME': client.parent_guardian_name or '',
        'CLIENT_NAME': client.name_first_last(),
        'CHILD_NAME': client.name_first_last(),
        'DATE_SENT': str(dt.datetime.now().date()),
        'USER_NAME': user.full_name,
        'USER_TITLE': user.title or '',
        'CLIENT_BIRTHDATE': client.birthdate.strftime(settings.HUMAN_DATE_FORMAT),
        'ADDRESS': client_address,
        'CLIENT_ADDRESS': client_address,
        'TODAY_DATE': dt.datetime.now().strftime(settings.HUMAN_DATE_FORMAT),
    }


    dl = docx_replace(name, rep, user, client)
    return "/" + dl
