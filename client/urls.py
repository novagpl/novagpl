"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from django.urls import path

from . import views

app_name = 'client'

urlpatterns = [
    path('all/', views.client_index, name="client_index"),
    path('inactive/', views.client_inactive, name='client_inactive'),
    path('me/', views.me_index, name='me_index'),
    path('<int:client_id>/', views.client_edit, name='client_edit'),
    path('new/', views.client_edit, name='client_new'),

    path("caseworker_login/", views.caseworker_login, name="caseworker_login"),

    path("set_active/<int:client_id>/", views.client_set_active, name="client_set_active"),
    path("set_inactive/<int:client_id>/", views.client_set_inactive, name="client_set_inactive"),

    # These are the tab menu items on the client screen
    path("client_settings/<int:client_id>/", views.client_settings_index, name="client_settings_index"),

    path("emr_episodes/<int:client_id>/", views.emr_episode_edit, name="emr_episodes_index"),
    path("emr_episodes/<int:client_id>/<int:episode_id>/", views.emr_episode_edit, name="emr_episodes_edit"),
    # Build an EMR Episode view where only the episode is ID (it can redirect to the one above here)

    path("emr_episodes/<int:client_id>/<int:episode_id>/form/<int:form_type_id>/", views.emr_episode_edit, name="emr_episode_form_list"),
    path("emr_episodes/<int:client_id>/<int:episode_id>/form/<str:note_type_id>/", views.emr_episode_edit, name="emr_episode_note_list"),
    path("emr_episodes/<int:client_id>/<int:episode_id>/doc/<int:doc_type_id>/", views.emr_episode_edit, name="emr_episode_doc_list"),

    path("new_emr_episode/<str:episode_type>/<int:client_id>/", views.new_emr_episode, name="new_emr_episode"),

    path("emr_form/<int:form_type_id>/<int:episode_id>/", views.emr_form_edit, name="emr_form_new"),
    path("emr_form/<int:emr_form_id>/", views.emr_form_edit, name="emr_form_edit"),
    path("emr_form/<int:emr_form_id>/sign/", views.emr_form_sign, name="emr_form_sign"),
    path("emr_form/<int:emr_form_id>/unlock/", views.emr_form_unlock, name="emr_form_sign"),
    path("emr_form_history/<int:emr_form_id>/", views.emr_form_history, name="emr_form_history"),

    path("emr_form/<int:emr_form_id>/supervisor_sign/", views.emr_form_supervisor_sign, name="emr_form_supervisor_sign"),
    path("emr_form/<int:emr_form_id>/electronic_signature/", views.emrform_electric_sig, name="emr_form_electronic_sig_new"),
    path("emr_form/<int:emr_form_id>/electronic_signature/<int:emrform_sig_id>/", views.emrform_electric_sig, name="emr_form_electronic_sig_edit"),

    path("emr_doc/<int:doc_type_id>/<int:episode_id>/", views.emr_doc_edit, name="emr_doc_new"),
    path("emr_doc/<int:emr_doc_id>/", views.emr_doc_edit, name="emr_doc_edit"),
    path("emr_doc/<int:emr_doc_id>/emr_docs/<int:emr_doc_id2>/<str:filename>", views.emr_doc_file, name="emr_doc_file"),
    path("emr_doc/<int:emr_doc_id>/emr_docs/<int:emr_doc_id2>/<str:filename>/", views.emr_doc_file, name="emr_doc_file"),
    path("emr_doc/<int:emr_doc_id>/file/", views.emr_doc_file, name="emr_doc_file_short"),
    path("emr_doc/<int:emr_doc_id>/view/", views.emr_doc_view, name="emr_doc_view"),
    path("emr_doc/<int:emr_doc_id>/<int:emr_form_id>/unlink/", views.emr_doc_unlink, name="emr_doc_unlink"),

    path("emr_casenote/reopen/<int:emr_casenote_id>/", views.emr_casenote_reopen, name="emr_casenote_reopen"),

    path("emr_message/", views.emr_message_edit, name="emr_message_new"),
    path("emr_message/<int:message_id>/", views.emr_message_edit, name="emr_message_edit"),

    path("pps_episodes/<int:client_id>/", views.pps_episodes_index, name="pps_episodes_index"),
    path("case_notes_index/<int:client_id>/", views.case_notes_index, name="case_notes"),
    path("state_forms/<int:client_id>/", views.state_forms_index, name="state_forms_index"),

    path('address/<int:address_id>/', views.address_edit, name="address_edit"),
    path('address/new/', views.address_edit, name="address_new"),

    path('contact/<int:contact_id>/', views.contact_edit, name="contact_edit"),
    path('contact/new/', views.contact_edit, name="contact_new"),

    path('insurance/<int:insurance_id>/', views.insurance_edit, name="insurance_edit"),
    path('insurance/', views.insurance_edit, name="insurance_new"),

    path('assigned_staff/<int:assigned_staff_id>/', views.assigned_staff_edit, name='assigned_staff_edit'),
    path('assigned_staff/', views.assigned_staff_edit, name='assigned_staff_new'),

    path('casenote/<int:casenote_id>/', views.casenote_edit, name="casenote_edit"),
    path('casenote/new/', views.casenote_edit, name="casenote_new"),

    path("casenote_file/<int:file_id>/casenote_files/<int:user_id>/<int:client_id>/<str:filename>", views.casenote_files),
    path("casenote_file/<int:file_id>/casenote_files/<int:user_id>/<int:client_id>/<str:filename>/", views.casenote_files),
    path('casenote_file/<int:casenote_file_id>/', views.casenote_file, name='casenote_file'),
    path('casenote_file/new/', views.casenote_file, name='casenote_file_new'),

    path("casenote/view_all/<int:client_id>/", views.casenote_view_all, name="casenote_view_all"),

    path("casenote/reopen/<int:casenote_id>/", views.casenote_reopen, name="casenote_reopen"),

    path("legal_name/<int:client_id>/<int:legal_name_id>/", views.legal_name_edit, name="legal_name"),
    path("legal_name/<int:client_id>/new/", views.legal_name_edit, name="legal_name_new"),


    path("sacwis/import_mgmt/", views.sacwis_import_mgmt, name="sacwis_import_mgmt"),
    path("sacwis/import_mgmt/<int:sac_row_id>/", views.sacwis_import_edit, name="sacwis_import_edit"),

    # The PPS episodes are not currently in use
    #path("pps_epi_sort/<uuid>/", views.pps_epi_sort, name="pps_epi_sort"),

    #path('pps_aoda_epi/<int:pk>/', views.pps_aoda_edit, name='pps_aoda_epi_edit'),
    #path('pps_aoda_epi/new/', views.pps_aoda_edit, name='pps_aoda_epi_new'),

    #path('pps_mh_epi/<int:pk>/', views.pps_mh_edit, name='pps_mh_epi_edit'),
    #path('pps_mh_epi/new/', views.pps_mh_edit, name='pps_mh_epi_new'),

    #path('pps_core_epi/<int:pk>/', views.pps_core_edit, name='pps_core_epi_edit'),
    #path('pps_core_epi/new/', views.pps_core_edit, name='pps_mh_core_epi_new'),

    #path('pps_b3_epi/<int:pk>/', views.pps_b3_edit, name='pps_b3_epi_edit'),
    #path('pps_b3_epi/new/', views.pps_b3_edit, name='pps_mh_b3_epi_new'),


    path("atp/<int:pk>/", views.atp_edit, name="atp_edit"),
    path("atp/new/", views.atp_edit, name="atp_new"),



    path('form/<name>/', views.form_download, name='form_download'),

    path("ajax_duplicate_name_detect/", views.ajax_duplicate_name_detect, name="duplicate_name_detect"),
    path("ajax_calculate_units/<int:emr_form_id>/<int:minutes>/", views.ajax_calculate_units, name="ajax_calculate_minutes"),



]
