"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


#choices

#https://www.dhs.wisconsin.gov/sites/default/files/legacy/pps/mh-aoda/MHHandbook.pdf





CLTS_CST_ASSESSMENT_TYPE = (
    ("annual", "Annual"),
    ("initial", "Initial"),
)

CLTS_CST_RIGHTS_AND_GRIEV = (
    ("1", "Client and parent/guardian were provided information on Patient Rights and Grievances. The client appears to be able to understand the information and to assert their rights."),
    ("2", "Client and parent/guardian were provided information on Patient Rights and Grievances. It is unclear if the client is able to understand the information and to assert their rights, but parent/guardian is able to navigate the information and advocate for the client."),
)


# Contact info and contact type are different in that contact info belongs
# to the type field on the ContactInfo model. This is for tracking contacts
# in relation to the client so these would be like probation officers and
# stuff like that.
# TODO update these items in a migration to use text instead of numbers as
# their values
CONTACT_INFO = (
    (None, "----------"),
    ("11", "Aunt"),
    ("9", "Brother"),
    ("child", "Child"),
    ("6", "Emergency contact"),
    ("8", "Grandparent"),
    ("1", "Guardian of estate"),
    ("0", "Guardian of person"),
    ("7", "Parent"),
    ("3", "POA of finances"),
    ("2", "POA of healthcare"),
    ("5", "Probation officer"),
    ("4", "Provider"),
    ("spouse", "Spouse"),
    ("13", "School"),
    ("10", "Sister"),
    ("14", "Staff"),
    ("12", "Uncle"),
    ("other", "Other"),
)


# Contact type is for the EMR case notes and describes the type of contact
# the case worker has with the client
CONTACT_TYPE = (
    ("4", "Collateral"),
    ("2", "Direct"),
    ("1", "Face to face"),
    ("3", "Home visit"),
    ("5", "Record keeping"),
    ("0", "Other/unknown")
)

CRISIS_TYPE_OF_CONTACT = (
    ("audio", "Audio-only"),
    ("info_only", "Information only"),
    ("mobile", "Mobile"),
    ("mobile_team", "Mobile team response"),
    ("telehealth", "Telehealth"),
    ("walkin", "Walk-in"),
    ("other", "Other"),
)

CRISIS_OUTCOMES = (
    ("adult_crisis_bed", "Adult crisis bed"),
    ("ch_5145", "Ch. 51.45 Incapacitation Hold"),
    ("ch_5115", "Ch. 51.15 Emergency detention"),
    ("ch_55", "Ch. 55 Emergency protective placement"),
    ("community_support", "Community support plan"),
    ("home", "Home individually"),
    ("info_only", "Information only"),
    ("jail_no_precaution", "Jail - precautions NOT recommended"),
    ("jail_with_precaution", "Jail - precautions recommended"),
    ("juvenile_detention", "Juvenile detention center"),
    ("medical_primary", "Medical primary"),
    ("mobile_worker_dispatched", "Mobile worker dispatched"),
    ("phone_plan", "Telephone stabilization plan"),
    ("stab_place", "Crisis stabilization placement"),
    ("voluntary_detox", "Voluntary detox"),
    ("voluntary_inpatient", "Voluntary hospitalization"),
    ("youth_crisis_bed", "Youth crisis bed"),
    ("other", "Other"),
)


EMR_CASE_NOTE_TYPE = (
    ("annual", "Annual"),
    ("enrollment", "Enrollment"),
    ("general", "General"),
    ("six_month", "Six month"),
    ("staffing", "Staffing")
)

# Limited list of case note types for RANDI these MUST exist in
# EMR_CASE_NOTE_TYPE as well
EMR_RANDI_CASE_NOTE_TYPE = (
    ("general", "General"),
    ("staffing", "Staffing"),
)

# Limited list of case note types for CLTS these MUST exist in
# EMR_CASE_NOTE_TYPE as well
EMR_CLTS_CASE_NOTE_TYPE = (
    ("annual", "Annual"),
    ("enrollment", "Enrollment"),
    ("general", "General"),
    ("six_month", "Six month"),
)



EPISODE_CLOSE_REASON = (
    ("admit_b3", "Admitted to Birth to 3"),
    ("admit_clts", "Admitted to CLTS"),
    ("aged out", "Aged out of services"),
    ("aoda_cm", "AODA Case Management"),
    ("county discharge", "County prompted discharge"),
    ("current enroll", "Currently enrolled in another service/program that will f/u"),
    ("death", "Client death"),
    ("declined", "Declined services"),
    ("incarcerated", "Incarcerated"),
    ("ltc", "Transitioned to LTC"),
    ("moved", "Moved out of county"),
    ("no engagement", "No engagement"),
    ("not eligible", "No longer eligible "),
    ("out of county", "Transferred to same service out of county"),
    ("refer b3", "Referred to Birth to 3"),
    ("refer ccs", "Referred to CCS"),
    ("refer clinic preadmit", "Referred to Clinic Pre-admit"),
    ("refer clinic", "Referred to Clinic"),
    ("refer clts", "Referred to CLTS"),
    ("refer county", "Referred to county of residence"),
    ("refer crisis", "Referred to Crisis"),
    ("refer cst", "Referred to CST"),
    ("referred to community services", "Referred to community resources"),
    ("success discharge", "Successfully discharged"),
    ("other", "Other"),
)


EPISODE_TYPE = (
    ("clinic_preadmit", "Clinic Pre-Admit"),
    ("clts", "CLTS"),
    ("clts_preadmit", "CLTS Pre-Admit"),
    ("crisis", "Crisis"),
    ("cst", "CST"),
    ("randi", "R & I"),
    ("tcm", "TCM"),
    ("b3", "Birth to 3"),
    ("b3_preadmit", "Birth to 3 Pre-Admit"),
)


GENDER = (
    ("1", "Female"),
    ("2", "Male"),
    ("0", "Unknown"),
    ("3", "Other"),
)

GENDER_IDENTITY = (
    ("1", "Female"),
    ("2", "Male"),
    ("4", "Non-binary"),
    ("0", "Unknown"),
)

HISPANIC = (
    ("2", "Hispanic"),
    ("1", "Non-Hispanic"),
    ("0", "Unknown"),
)

ID_TYPE = (
    ("5", "Driver's license"),
    ("8", "EIN"),
    ("4", "eWiSacWis"),
    ("3", "HSRS"),
    ("2", "MCI"),
    ("6", "NPI"),
    ("0", "Other"),
    ("1", "Social security"),
    ("7", "WPI")
)


INSURANCE_TYPE = (
    ("medicaid", "Medicaid"),
    ("medicare", "Medicare"),
    ("other", "Other"),
    ("private", "Private"),
    ("tricare", "Tricare"),
)



MEDICAID_TYPE = (
    ("adoption_assist", "Adoption assistance"),
    ("badger_care_plus", "Badger Care Plus"),
    ("foster_care", "Foster care"),
    ("katie_beckett", "Katie Beckett"),
    ("other", "Other"),
    ("ssi", "SSI"),
    ("waiver", "Waiver medicaid"),
)



# Crisis, CLTS, and other programs
PLACE_OF_SERVICE_CODE = (
    ("01", "01 - Pharmacy"),
    ("02", "02 - Telehealth other than in patient's home"),
    ("03", "03 - School"),
    ("04", "04 - Homeless shelter"),
    ("05", "05 - Indian health service free-standing facility"),
    ("06", "06 - Indian health service provider-based facility"),
    ("07", "07 - Tribal 638 free-standing facility"),
    ("08", "08 - Tribal 638 provider-based facility"),
    ("09", "09 - Prison/correctional facility"),
    ("10", "10 - Telehealth provided in patient's home"),
    ("11", "11 - Office"),
    ("12", "12 - Home"),
    ("13", "13 - Assisted living facility"),
    ("14", "14 - Group home"),
    ("15", "15 - Mobile unit"),
    ("16", "16 - Temporary lodging"),
    ("17", "17 - Walk-in retail health clinic"),
    ("18", "18 - Place of employment worksite"),
    ("19", "19 - Off campus - outpatient hospital"),
    ("20", "20 - Urgent care facility"),
    ("21", "21 - Inpatient hospital"),
    ("22", "22 - On campus - outpatient hospital"),
    ("23", "23 - Emergency room - hospital"),
    ("24", "24 - Ambulatory surgical center"),
    ("25", "25 - Birthing center"),
    ("26", "26 - Military treatment facility"),
    ("27", "27 - Outreach Site/Street "),
    ("31", "31 - Skilled nursing facility"),
    ("32", "32 - Nursing facility"),
    ("33", "33 - Custodial care facility"),
    ("34", "34 - Hospice"),
    ("49", "49 - Independent clinic"),
    ("50", "50 - Federally qualified health center"),
    ("51", "51 - Inpatient psychiatric facility (below age 21 or age 65 and older)"),
    ("52", "52 - Psychiatric facility - partial hospitalization"),
    ("53", "53 - Community mental health center"),
    ("54", "54 - Intermediate care facility/individual with intellectual disabilities"),
    ("55", "55 - Residential substance abuse treatment facility"),
    ("56", "56 - Psychiatric residential treatment center"),
    ("57", "57 - Non-residential substance abuse treatment facility"),
    ("60", "60 - Mass immunization center"),
    ("61", "61 - Comprehensive inpatient rehabilitation facility"),
    ("71", "71 - Public health clinic"),
    ("72", "72 - Rural health clinic"),
    ("99", "99 - Other place of service"),
)

# B3 uses different place of service options and they don't appear to be
# tied to any state codes at this time (2023-06-15)
PLACE_OF_SERVICE_CODE_B3 = (
    ("community_center", "Community center"),
    ("day_care", "Day care"),
    ("home", "Home"),
    ("family_friend_house", "Family/friend's house"),
    ("office", "Office"),
    ("playground", "Playground"),
    ("shopping_center", "Shopping center"),
    ("other", "Other"),
)



RACE = (
    ("5", "American Indian or Alaska Native"),
    ("1", "Asian"),
    ("2", "Black or African-American"),
    ("4", "Native Hawaiian or Pacific Islander"),
    ("3", "White"),
    ("other", "Other"),
    ("0", "Unknown"),

)


# These are in a specific order for the form's flow.
RANDI_INTAKE_OUTCOME = (
    ("proceed", "Client/Parent/Guardian/POA-HC would like to proceed with intake and review"),
    ("not_reached", "Client/Parent/Guardian/POA-HC could not be reached or did not respond"),
    ("oc_not_interested", "Client/Parent/Guardian/POA-HC is not interested in proceeding with intake and review"),
)

RANDI_LEGAL_STAT_CHOICES = (
    ("self", "Responsible for self"),
    ("guardian", "Legal guardian"),
    ("poa_hc", "Activated POA-HC"),
    ("minor", "Minor"),
)


STAFF_CRED_CHOICES = (
    ("para", "Paraprofessional"),
    ("bachelors", "Bachelors"),
    ("masters", "Masters"),
    ("phd", "Doctorate"),
    ("psych", "Psychiatrist"),
    ("apnp", "APNP"),
    ("cps", "Certified Peer Specialist"),
    ("fr1", "First Responder Level 1"),
    ("fr2", "First Responder Level 2"),
)


STATE = [
    ("AL", "Alabama"),
    ("AK", "Alaska"),
    ('AR', 'Arkansas'),
    ('AZ', 'Arizona'),
    ('CA', 'California'),
    ('CO', 'Colorado'),
    ('CT', 'Connecticut'),
    ('DC', 'Washington D.C.'),
    ('DE', 'Delaware'),
    ('FL', 'Florida'),
    ('GA', 'Georgia'),
    ('HI', 'Hawaii'),
    ('IA', 'Iowa'),
    ('ID', 'Idaho'),
    ('IL', 'Illinois'),
    ('IN', 'Indiana'),
    ('KS', 'Kansas'),
    ('KY', 'Kentucky'),
    ('LA', 'Louisiana'),
    ('MA', 'Massachusetts'),
    ('MD', 'Maryland'),
    ('ME', 'Maine'),
    ('MI', 'Michigan'),
    ('MN', 'Minnesota'),
    ('MO', 'Missouri'),
    ('MS', 'Mississippi'),
    ('MT', 'Montana'),
    ('NC', 'North Carolina'),
    ('ND', 'North Dakota'),
    ('NE', 'Nebraska'),
    ('NH', 'New Hampshire'),
    ('NJ', 'New Jersey'),
    ('NM', 'New Mexico'),
    ('NV', 'Nevada'),
    ('NY', 'New York'),
    ('OK', 'Oklahoma'),
    ('OH', 'Ohio'),
    ('OR', 'Oregon'),
    ('PA', 'Pennsylvania'),
    ('RI', 'Rhode Island'),
    ('SC', 'South Carolina'),
    ('SD', 'South Dakota'),
    ('TN', 'Tennessee'),
    ('TX', 'Texas'),
    ('UT', 'Utah'),
    ('VA', 'Virginia'),
    ('VT', 'Vermont'),
    ('WA', 'Washington'),
    ('WI', 'Wisconsin'),
    ('WV', 'West Virginia'),
    ('WY', 'Wyoming'),
    ("0", "Unknown"),
]

SUFFIX = (
    ("0", "----------"),
    ("1", "Jr"),
    ("2", "Sr"),
    ("3", "III"),
)

TCM_ASSESSMENT_TYPE = (
    ("annual", "Annual"),
    ("initial", "Initial"),
    ("update", "Update"),
)


TITLE = (
    ("0", "----------"),
    ("1", "Mr"),
    ("4", "Ms"),
    ("2", "Mrs"),
    ("3", "Miss"),
)

WI_COUNTY = (
    ("1", "Adams"),
    ("2", "Ashland"),
    ("3", "Barron"),
    ("4", "Bayfield"),
    ("5", "Brown"),
    ("6", "Buffalo"),
    ("7", "Burnett"),
    ("8", "Calumet"),
    ("9", "Chippewa"),
    ("10", "Clark"),
    ("11", "Columbia"),
    ("12", "Crawford"),
    ("13", "Dane"),
    ("14", "Dodge"),
    ("15", "Door"),
    ("16", "Douglas"),
    ("17", "Dunn"),
    ("18", "Eau Claire"),
    ("19", "Florence"),
    ("20", "Fond du Lac"),
    ("21", "Forest"),
    ("22", "Grant"),
    ("23", "Green"),
    ("24", "Green Lake"),
    ("25", "Iowa"),
    ("26", "Iron"),
    ("27", "Jackson"),
    ("28", "Jefferson"),
    ("29", "Juneau"),
    ("30", "Kenosha"),
    ("31", "Kewaunee"),
    ("32", "La Crosse"),
    ("33", "Lafayette"),
    ("34", "Langlade"),
    ("35", "Lincoln"),
    ("36", "Manitowoc"),
    ("37", "Marathon"),
    ("38", "Marinette"),
    ("39", "Marquette"),
    ("40", "Milwaukee"),
    ("41", "Monroe"),
    ("42", "Oconto"),
    ("43", "Oneida"),
    ("44", "Outagamie"),
    ("45", "Ozaukee"),
    ("46", "Pepin"),
    ("47", "Pierce"),
    ("48", "Polk"),
    ("49", "Portage"),
    ("50", "Price"),
    ("51", "Racine"),
    ("52", "Richland"),
    ("53", "Rock"),
    ("54", "Rusk"),
    ("55", "St Croix"),
    ("56", "Sauk"),
    ("57", "Sawyer"),
    ("58", "Shawano"),
    ("59", "Sheboygan"),
    ("60", "Taylor"),
    ("61", "Trempealeau"),
    ("62", "Vernon"),
    ("63", "Vilas"),
    ("64", "Walworth"),
    ("65", "Washburn"),
    ("67", "Waukesha"),
    ("68", "Waupaca"),
    ("68", "Waushara"),
    ("70", "Winnebago"),
    ("71", "Wood"),
    ("72", "Menominee"),
    ("84", "Menominee Tribe"),
    ("85", "Red Cliff Tribe"),
    ("86", "Stockbridge Munsee"),
    ("87", "Forrest County Potawatomi"),
    ("88", "Lac du Flambeau"),
    ("89", "Bad River Tribe"),
    ("91", "Sokaogon Chippewa"),
    ("92", "Oneida Tribe"),
    ("93", "Ho Chunk Nation"),
    ("94", "Lac Courte Oreilles Tribe"),
    ("95", "St Croix Tribe"),
    ("out", "Out of state"),
    ("0", "Unknown"),

)
