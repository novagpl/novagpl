"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import datetime
import json
import re
import uuid
from decimal import Decimal
from dateutil.relativedelta import relativedelta
from dateutil.parser import parse
from django.core.mail import send_mail

import accounting.accounting_choices as ACT_C
import client.client_choices as CLIENT_C
import client.pps_choices as PPS_C

import utils
import pytz

from six import text_type
from annoying.functions import get_object_or_None
from django.core.signing import BadSignature, TimestampSigner, b62_decode
from django.core.exceptions import ValidationError
from django.apps import apps
from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Q
from django.utils import timezone
from multiselectfield import MultiSelectField
from django.core import serializers



YES_NO_CHOICES = (
    (None, "----------"),
    ("yes", "Yes"),
    ("no", "No"),
)

YES_NO_UNKNOWN_CHOICES = (
    ("yes", "Yes"),
    ("no", "No"),
    ("unknown", "Unknown"),
)



# Similar first names and last names
class SimFirstName(models.Model):
    value = models.CharField(max_length=255, unique=True, db_index=True)

    def __str__(self):
        return self.value


class SimFirstVar(models.Model):
    name = models.ForeignKey(SimFirstName, related_name="variants", on_delete=models.CASCADE)
    value = models.CharField(max_length=255, db_index=True)

    def __str__(self):
        return f"{self.value} -> {self.name}"




class SimLastName(models.Model):
    value = models.CharField(max_length=255, unique=True, db_index=True)

    def __str__(self):
        return self.value


class SimLastVar(models.Model):
    name = models.ForeignKey(SimLastName, related_name="variants", on_delete=models.CASCADE)
    value = models.CharField(max_length=255, db_index=True)

    def __str__(self):
        return f"{self.value} -> {self.name}"






class EmrMessage(models.Model):
    message = models.TextField()
    expires_at = models.DateTimeField(null=True)
    updated_at = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey("auth.User", null=True, on_delete=models.SET_NULL)



    def __str__(self):
        the_time = datetime.datetime.strftime(self.updated_at, "%m/%d/%Y %H:%M %P")
        return f"Last updated on {the_time} by {self.updated_by.userconfig.full_name}"




class Client(models.Model):
    active = models.BooleanField(default=True)
    title = models.CharField(max_length=1, choices=CLIENT_C.TITLE, default=0)
    first_name = models.CharField(max_length=255, verbose_name="First")
    middle_name = models.CharField(blank=True, null=True, max_length=50, verbose_name="Middle")
    last_name = models.CharField(max_length=255, verbose_name="Last")
    fmt_first_name = models.CharField(max_length=255, null=True, blank=True, db_index=True)
    fmt_last_name = models.CharField(max_length=255, null=True, blank=True, db_index=True)
    alias = models.CharField(max_length=255, null=True, blank=True, verbose_name="Preferred/alias/nickname")
    suffix = models.CharField(max_length=1, choices=CLIENT_C.SUFFIX, default=0)
    birthdate = models.DateField(null=True, blank=True, verbose_name="Birthdate")
    gender = models.CharField(max_length=1, null=True, choices=CLIENT_C.GENDER, verbose_name="Gender at birth", default=0)
    gender_identity = models.CharField(max_length=1, choices=CLIENT_C.GENDER_IDENTITY, verbose_name="Gender identity", default=0)
    race = models.CharField(max_length=255, verbose_name="Race", choices=CLIENT_C.RACE, default=0)
    hispanic = models.CharField(max_length=1, choices=CLIENT_C.HISPANIC, default=0)
    parent_guardian_name = models.CharField(max_length=255, blank=True, null=True, verbose_name="Parent or guardian name")
    assigned_staff = models.ManyToManyField('config.UserConfig', blank=True)
    starting_balance = models.DecimalField(decimal_places=2, max_digits=12, blank=True, null=True)
    legacy_client_number = models.CharField(max_length=255, null=True, blank=True)
    phone = models.CharField(max_length=255, null=True, blank=True)
    phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    alt_phone = models.CharField(max_length=255, null=True, blank=True)
    alt_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    email = models.EmailField(max_length=255, null=True, blank=True)
    # Add other id numbers as they are needed.
    ssn = models.CharField(max_length=255, null=True, blank=True, verbose_name="SSN")


    def save(self, *args, **kwargs):
        # Saving formatted copies of the first and last name to make the
        # duplicate name detection logically simpler and more performant
        # This just removes any non-alphanumeric characters for the first
        # and last names and saves them into their respective fmt_ fields
        self.fmt_first_name = re.sub(r'\W+', '', self.first_name)
        self.fmt_last_name = re.sub(r'\W+', '', self.last_name)
        super().save(*args, **kwargs)


    def __str__(self):
        # Format a string with the first and alias if there is an alias
        if self.alias:
            firstname = f'{self.first_name} "{self.alias}"'
        else:
            firstname = self.first_name


        if self.birthdate is None:
            return f"{self.last_name}, {firstname} - ({self.id})"
        else:

            dob = self.birthdate.strftime(settings.HUMAN_DATE_FORMAT)
            return f"{self.last_name}, {firstname} ({self.id}) - {dob} ({self.get_age()})"


    def name(self):
        # Format a string with the first and alias if there is an alias
        if self.alias:
            firstname = f'{self.first_name} "{self.alias}"'
        else:
            firstname = self.first_name

        return self.last_name + ", " + firstname


    def name_first_last(self):
        return "{} {}".format(self.first_name, self.last_name)





    def get_age(self):
        today = timezone.now().date()
        age = relativedelta(today, self.birthdate)

        if age.years == 0 and age.months < 1:
            return f"{age.days} day{'s' if age.days > 1 else ''}"

        if age.years < 2:
            total_months = (age.years * 12) + age.months
            return f"{total_months} month{'s' if total_months > 1 else ''}"

        return f"{age.years} year{'s' if age.years > 1 else ''}"





    def get_address(self):
        found_address = Address.objects.filter(client=self, is_physical=True, active=True).first()
        if found_address:
            return found_address

        found_address = Address.objects.filter(client=self, is_mailing=True, active=True).first()
        if found_address:
            return found_address

        found_address = Address.objects.filter(client=self, is_billing=True, active=True).first()
        if found_address:
            return found_address

        found_address = Address.objects.filter(client=self, active=True).first()
        if found_address:
            return found_address


        # Nothing found.
        return None




    def get_addresses(self):
        return Address.objects.filter(client=self).order_by('-active')



    def get_billing_address(self):
        # name
        # care of (if exists)
        # street 1  + Apt/Suite
        # City State Zip
        address_results = Address.objects.filter(
            client=self, is_billing=True)

        if len(address_results) == 0:
            return ""

        address = address_results[0]

        if address.care_of:
            fmt = "{}<br>{}<br>{} {}<br>{} {} {}".format(
                self.name_first_last(), address.care_of, address.street,
                address.apt, address.city, address.state, address.zip)
        else:
            fmt = "{}<br>{} {}<br>{} {} {}".format(
                self.name_first_last(), address.street, address.apt,
                address.city, address.state, address.zip)

        return fmt


    def get_insurance_info(self):
        insurance_items = self.insurance_set.all().order_by("-id")

        if len(insurance_items) == 0:
            return ""
        elif len(insurance_items) == 1:
            return str(insurance_items[0])
        else:
            return "\n".join([str(x) for x in insurance_items])


    def get_insurance_info_html(self):
        return self.get_insurance_info().replace("\n", "<br>")

    def get_open_episodes(self):
        open_episodes = EmrEpisode.objects.filter(client=self, end__isnull=True)
        return open_episodes


    def get_auto_name(self):
        return str(self)



class PastLegalName(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    title = models.CharField(max_length=1, choices=CLIENT_C.TITLE, default=0)
    first_name = models.CharField(max_length=50, verbose_name="First")
    middle_name = models.CharField(blank=True, max_length=50, verbose_name="Middle")
    last_name = models.CharField(max_length=50, verbose_name="Last")
    suffix = models.CharField(max_length=1, choices=CLIENT_C.SUFFIX, default=0)
    date_changed = models.DateField(auto_now_add=True)




class Address(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    street = models.CharField(max_length=100, null=True, blank=True)
    care_of = models.CharField(max_length=100, verbose_name="Care of", null=True, blank=True)
    apt = models.CharField(blank=True, max_length=10, verbose_name="Apt/suite")
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=2, choices=CLIENT_C.STATE, default="WI")
    zip = models.CharField(max_length=10, null=True, blank=True)
    county = models.CharField(max_length=3, choices=CLIENT_C.WI_COUNTY, default=0)
    is_billing = models.BooleanField(default=False, verbose_name="Billing")
    is_mailing = models.BooleanField(default=False, verbose_name="Mailing")
    is_physical = models.BooleanField(default=False, verbose_name="Physical")
    is_homeless = models.BooleanField(default=False, verbose_name="Homeless")
    note = models.TextField(null=True, blank=True)



    def __str__(self):
        if self.street is None:
            if self.is_homeless:
                return f"{self.city} (homeless)"  
            else:
                return f"{self.city}"   

        if self.street and self.city:
            if self.is_homeless:
                return f"{self.street}, {self.city} (homeless)"
            else:
                return f"{self.street}, {self.city}"




    def get_long_address(self):
        address_data = []

        if self.street:
            if self.apt:
                address_data.append(f"{self.street}, {self.apt}")
            else:
                address_data.append(self.street)

        if self.city:
            address_data.append(self.city)

        if self.state:
            address_data.append(self.state)

        if self.zip:
            address_data.append(self.zip)

        if self.is_homeless:
            address_data.append("(homeless)")

        return ", ".join(address_data)


    def get_address_type(self):
        # TODO look at the address bits 
        # (billing, mailing, physical, client.homeless)
        # and build a string to be shown
        active_types = []

        if self.is_homeless:
            active_types.append("Homeless")
        if self.is_billing:
            active_types.append("Billing")
        if self.is_mailing:
            active_types.append("Mailing")
        if self.is_physical:
            active_types.append("Physical")

        return ", ".join(active_types)








class ContactInfo(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    active = models.BooleanField(default=True)
    type_of_contact = models.CharField(max_length=255, choices=CLIENT_C.CONTACT_INFO, default=0)
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    agency_name = models.CharField(max_length=255, null=True, blank=True)
    street = models.TextField(null=True, blank=True)
    suite = models.CharField(null=True, blank=True, max_length=10, verbose_name="Unit/suite")
    city = models.CharField(max_length=50, null=True, blank=True)
    state = models.CharField(max_length=2, null=True, blank=True, choices=CLIENT_C.STATE, default="WI")
    zipcode = models.CharField(max_length=10, null=True, blank=True)
    phone = models.CharField(max_length=255, null=True, blank=True)
    phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    alt_phone = models.CharField(max_length=255, null=True, blank=True)
    alt_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    fax = models.CharField(max_length=255, null=True, blank=True)
    email = models.CharField(max_length=255, null=True, blank=True)
    notes = models.CharField(blank=True, null=True, max_length=2048)


    def __str__(self):        
        return "{} contact for {}".format(
            self.get_type_of_contact_display(), self.get_name())


    def get_name(self):
        name_string = []
        if self.first_name:
            name_string.append(self.first_name)

        if self.last_name:
            name_string.append(self.last_name)


        if self.active is False:
            name_string.append("(inactive)")

        if name_string:
            return " ".join(name_string)
        else:
            return ""


    def get_address(self):
        parts = []
        if self.street and self.suite:
            parts.append(f"{self.street} {self.suite}")

        else:
            parts.append(self.street)
            parts.append(self.suite)

        parts.append(self.city)

        if self.state and self.zipcode:
            parts.append(f"{self.state} {self.zipcode}")
        else:
            parts.append(self.state)
            parts.append(self.zipcode)

        return ", ".join(filter(None, parts))





class Insurance(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    insurance_type = models.CharField(max_length=255, choices=CLIENT_C.INSURANCE_TYPE)
    provider_name = models.CharField(max_length=255, null=True, blank=True)
    group_number = models.CharField(max_length=255, null=True, blank=True)
    subscriber_id = models.CharField(max_length=255, null=True, blank=True)
    notes = models.TextField(null=True, blank=True)


    def __str__(self):
        return f"{self.get_insurance_type_display()} {self.provider_name}"







class Diagnosis(models.Model):
    # The diagnosis model can take any sort of code + name pair by usually
    # it will be an ICD10 code
    active = models.BooleanField(default=True)
    name = models.TextField()
    code = models.CharField(max_length=255)

    def __str__(self):
        return str(self.code) + " - " + str(self.name)

    def get_auto_name(self):
        return str(self.code) + " - " + str(self.name)


    def get_crisis_icd10_codes(self):
        # This function returns a list of crisis-compatible ICD10 codes
        codes = [
            "F48.9", "F69", "F81.9", "F41.83", "F45.850", "F45.851", "Z73.3",
            "R41.83", "R45.850", "R45.851",
            "Z76.5", "Z69.010", "Z69.020", "Z69.11", "Z69.81", "Z86.59",
            "Z72.810", "Z72.811", "Z72.9",
        ]

        return Diagnosis.objects.filter(code__in=codes).order_by('code')




class Service(models.Model):
    active = models.BooleanField(default=True)
    funding_profile = models.ForeignKey('accounting.FundingProfile', null=True, blank=True, verbose_name='Funding', on_delete=models.SET_NULL)
    name = models.CharField(max_length=100)
    spc_code = models.CharField(max_length=10, verbose_name="SPC code", null=True, blank=True)
    procedure_code = models.CharField(max_length=20, verbose_name="Procedure code", null=True, blank=True)
    default_unit = models.IntegerField(verbose_name="Default units", null=True, blank=True)
    pps_aoda = models.BooleanField(default=False, verbose_name="PPS AODA")
    pps_mh = models.BooleanField(default=False, verbose_name="PPS MH")
    pps_core = models.BooleanField(default=False, verbose_name="PPS Core")
    pps_b3 = models.BooleanField(default=False, verbose_name="PPS Birth to 3")
    pps_unit = models.CharField(null=True, blank=True, max_length=1, choices=PPS_C.PPS_UNIT, verbose_name="PPS unit")
    pps_brief_service = models.BooleanField(default=False, verbose_name="PPS Brief service")


    def __str__(self):
        str = []

        if self.spc_code:
            str.append(self.spc_code)

        if self.pps_unit:
            str.append(self.get_pps_unit_display())

        str.append(self.name)

        if self.procedure_code:
            str.append(self.procedure_code)

        return ", ".join(str)

    def get_auto_name(self):
        return str(self)


        '''
        if self.pps_unit:
            str = "{} ({}) {}".format(self.spc_code, self.get_pps_unit_display(), self.name)

        elif self.spc_code and str(self.spc_code).strip() != '' and self.funding_profile:
            str = "{} - {} ({})".format((self.spc_code + (self.procedure_code or '')), self.name, self.funding_profile.name)
        elif self.spc_code and str(self.spc_code).strip() != '':
            str = "{} - {}".format(self.spc_code, self.name)
        else:
            str = self.name

        if self.procedure_code is not None and str(self.procedure_code).strip() != '':
            str = "{} - {} ({})".format(self.procedure_code, self.name, self.funding_profile.name)

        str= "{} ({})".format(self.name, self.funding_profile.name)
        #return self.spc_code + " " + (self.procedure_code or '') + " - " + self.name + " ("+self.funding_profile.name +")"
        '''
    




    class Meta:
        ordering = ["spc_code", "name"]






class PpsAodaEpi(models.Model):
    pps_epi_uuid = models.UUIDField(primary_key=False, default=uuid.uuid4, editable=False)
    client = models.ForeignKey(Client, on_delete=models.PROTECT)
    start = models.DateField()
    end = models.DateField(null=True, blank=True)
    county = models.CharField(max_length=3, choices=CLIENT_C.WI_COUNTY)
    brief_services = models.BooleanField(default=False)
    referral_source = models.CharField(max_length=2, choices=PPS_C.REF_SOURCE)
    client_char1 = models.CharField(null=True, verbose_name="Characteristic 1", blank=True, max_length=2, choices=sorted(PPS_C.CLIENT_CHAR, key=lambda x: x[1]))
    client_char2 = models.CharField(null=True, verbose_name="Characteristic 2", blank=True, max_length=2, choices=sorted(PPS_C.CLIENT_CHAR, key=lambda x: x[1]))
    client_char3 = models.CharField(null=True, verbose_name="Characteristic 3", blank=True, max_length=2, choices=sorted(PPS_C.CLIENT_CHAR, key=lambda x: x[1]))
    education = models.CharField(verbose_name="Education at Admission", max_length=2, choices=PPS_C.EDUCATION)
    group_attend = models.CharField(verbose_name="Support Group 30 Days Prior", max_length=1, choices=PPS_C.GROUP_ATTEND)
    group_attend_end = models.CharField(verbose_name="Support Group Closing Status", null=True, blank=True, max_length=1, choices=PPS_C.GROUP_ATTEND)
    employ_stat = models.CharField(verbose_name="Employment Status at Admission", max_length=2, choices=PPS_C.EMPLOY_STAT)
    employ_stat_end = models.CharField(null=True, blank=True,verbose_name="Employment Status at Closing Status", max_length=2, choices=PPS_C.EMPLOY_STAT)
    living_stat = models.CharField(verbose_name="Living Arrangement at Admission", max_length=2, choices=PPS_C.LIVING_STAT)
    living_stat_end = models.CharField(verbose_name="Living Arrangement Closing Status", blank=True, null=True, max_length=2, choices=PPS_C.LIVING_STAT)
    use_freq = models.CharField(verbose_name="Use Frequency", max_length=1, choices=PPS_C.USE_FREQ)
    # Not required for SPCs 703, 705, 603 and brief services
    use_freq_end = models.CharField(verbose_name="Use Frequency at Closing",blank=True, null=True, max_length=1, choices=PPS_C.USE_FREQ)
    substance_problem1 = models.CharField(null=True, verbose_name="Substance Problem 1", max_length=2, choices=PPS_C.SUBSTANCE_PROBLEM)
    substance_problem2 = models.CharField(null=True, blank=True, verbose_name="Substance Problem 2", max_length=2, choices=PPS_C.SUBSTANCE_PROBLEM)
    substance_problem3 = models.CharField(null=True, blank=True, verbose_name="Substance Problem 3", max_length=2, choices=PPS_C.SUBSTANCE_PROBLEM)
    route_admin1 = models.CharField(null=True, verbose_name="Usual Route of Administration 1", max_length=1, choices=PPS_C.ROUTE_ADMIN)
    route_admin2 = models.CharField(null=True, blank=True, verbose_name="Usual Route of Administration 2", max_length=1, choices=PPS_C.ROUTE_ADMIN)
    route_admin3 = models.CharField(null=True, blank=True, verbose_name="Usual Route of Administration 3", max_length=1, choices=PPS_C.ROUTE_ADMIN)
    substance_problem_end = models.CharField(null=True, blank=True, verbose_name="Substance Problem at End of Episode", max_length=2, choices=PPS_C.SUBSTANCE_PROBLEM)
    arrest_end = models.IntegerField(default=0, null=True, blank=True, verbose_name="Arrests 30 days prior to discharge, or since admin if less than 30 days.")


    def get_services(self):
        return Service.objects.filter(pps_aoda=True)

    def get_name(self):
        return "PPS AODA"



class PpsMhEpi(models.Model):
    pps_epi_uuid = models.UUIDField(primary_key=False, default=uuid.uuid4, editable=False)
    client = models.ForeignKey(Client, on_delete=models.PROTECT)
    start = models.DateField()
    end = models.DateField(null=True, blank=True)
    county = models.CharField(max_length=3, choices=CLIENT_C.WI_COUNTY)
    referral_source = models.CharField(max_length=2, choices=PPS_C.REF_SOURCE)
    client_char1 = models.CharField(null=True, verbose_name="Primary Characteristic", blank=False, max_length=2, choices=sorted(PPS_C.CLIENT_CHAR, key=lambda x: x[1]))
    client_char2 = models.CharField(null=True, verbose_name="Characteristic 2", blank=True, max_length=2, choices=sorted(PPS_C.CLIENT_CHAR, key=lambda x: x[1]))
    client_char3 = models.CharField(null=True, verbose_name="Characteristic 3", blank=True, max_length=2, choices=sorted(PPS_C.CLIENT_CHAR, key=lambda x: x[1]))
    presenting_problem1 = models.CharField(null=True, blank=False, verbose_name="Primary Presenting Problem", max_length=2, choices=sorted(PPS_C.PRESENTING_PROBLEM, key=lambda x: x[1]))
    presenting_problem2 = models.CharField(null=True, blank=True, verbose_name="2nd Presenting Problem", max_length=2, choices=PPS_C.PRESENTING_PROBLEM)
    presenting_problem3 = models.CharField(null=True, blank=True, verbose_name="3rd Presenting Problem", max_length=2, choices=PPS_C.PRESENTING_PROBLEM)
    brc_target_pop = models.CharField(verbose_name="BRC Target Population", max_length=1, choices=PPS_C.BRC_TARGET_POP)
    employ_stat = models.CharField(verbose_name="Employment Status", max_length=2, choices=PPS_C.EMPLOY_STAT)
    living_stat = models.CharField(verbose_name="Living Arrangement", max_length=2, choices=PPS_C.LIVING_STAT)
    legal_stat = models.CharField(verbose_name="Legal/Commitment Status", max_length=2, choices=PPS_C.LEGAL_STAT)
    criminal_interactions = MultiSelectField(choices=PPS_C.CRIMINAL_INTERACTION, max_choices=4, max_length=9)
    arrest_30days = models.IntegerField(default=0, verbose_name="Arrests in the past 30 days")
    arrest_6months = models.IntegerField(default=0, verbose_name="Arrests in the past 6 months")
    psy_env_stress = models.CharField(verbose_name="Psychosocial and Environmental Stressors", choices=PPS_C.PSY_ENV_STRESS, max_length=2)
    health_status = models.CharField(max_length=2, choices=PPS_C.HEALTH_STATUS)
    daily_activity = models.CharField(max_length=2, choices=PPS_C.DAILY_ACTIVITY)
    epi_end_reason = models.CharField(verbose_name="Episode End Reason", max_length=2, choices=PPS_C.MH_EPI_END_REASON, null=True, blank=True)
    diagnosis1 = models.ForeignKey(Diagnosis, on_delete=models.PROTECT, null=True, blank=False, related_name="diagnosis1", verbose_name="Primary Diagnosis")
    diagnosis2 = models.ForeignKey(Diagnosis, on_delete=models.PROTECT, null=True, blank=True, related_name="diagnosis2", verbose_name="2nd Diagnosis")
    diagnosis3 = models.ForeignKey(Diagnosis, on_delete=models.PROTECT, null=True, blank=True, related_name="diagnosis3", verbose_name="3rd Diagnosis")
    diagnosis4 = models.ForeignKey(Diagnosis, on_delete=models.PROTECT, null=True, blank=True, related_name="diagnosis4", verbose_name="4th Diagnosis")
    diagnosis5 = models.ForeignKey(Diagnosis, on_delete=models.PROTECT, null=True, blank=True, related_name="diagnosis5", verbose_name="5th Diagnosis")


    def get_services(self):
        return Service.objects.filter(pps_mh=True)

    def get_name(self):
        return "PPS Mental Health"


class PpsCoreEpi(models.Model):
    pps_epi_uuid = models.UUIDField(primary_key=False, default=uuid.uuid4, editable=False)
    client = models.ForeignKey(Client, on_delete=models.PROTECT)
    start = models.DateField()
    end = models.DateField(null=True, blank=True)
    county = models.CharField(max_length=3, choices=CLIENT_C.WI_COUNTY)
    client_char1 = models.CharField(null=True, verbose_name="Characteristic 1", blank=True, max_length=2, choices=sorted(PPS_C.CORE_CLIENT_CHAR, key=lambda x: x[1]))
    client_char2 = models.CharField(null=True, verbose_name="Characteristic 2", blank=True, max_length=2, choices=sorted(PPS_C.CORE_CLIENT_CHAR, key=lambda x: x[1]))
    client_char3 = models.CharField(null=True, verbose_name="Characteristic 3", blank=True, max_length=2, choices=sorted(PPS_C.CORE_CLIENT_CHAR, key=lambda x: x[1]))
    epi_end_reason = models.CharField(null=True, blank=True, verbose_name="Episode End Reason", max_length=2, choices=PPS_C.CORE_EPI_END_REASON)

    def get_services(self):
        return Service.objects.filter(pps_core=True)

    def get_name(self):
        return "PPS Core"


class PpsB3Epi(models.Model):
    pps_epi_uuid = models.UUIDField(primary_key=False, default=uuid.uuid4, editable=False)
    client = models.ForeignKey(Client, on_delete=models.PROTECT)
    start = models.DateField()
    end = models.DateField(null=True, blank=True)
    county = models.CharField(max_length=3, choices=CLIENT_C.WI_COUNTY)
    language_pref = models.CharField(verbose_name="Language Preference", max_length=2, choices=PPS_C.B3_LANGUAGE_PREF)
    referral_source = models.CharField(max_length=2, choices=PPS_C.B3_REFER_SOURCE)
    result_reported = models.CharField(null=True, blank=True, max_length=2, choices=PPS_C.B3_RESULT_REPORTED)


    def get_services(self):
        return Service.objects.filter(pps_b3=True)

    def get_name(self):
        return "PPS Birth to 3"







class CaseNote(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    pps_epi_uuid = models.UUIDField(primary_key=False, blank=True, null=True)
    # not using signed_by today, but maybe in the future if the sigs take too long to extract or something..
    signed_by = models.ForeignKey("config.UserConfig", blank=True, null=True, on_delete=models.PROTECT)
    pps_start = models.DateField(null=True, blank=True)
    pps_end = models.DateField(null=True, blank=True)
    pps_units = models.IntegerField(null=True, blank=True)
    # Not required for SPCs 703, 705, 603, and brief services
    pps_aoda_spc_end_reason = models.CharField(null=True, blank=True, max_length=2, choices=PPS_C.AODA_SPC_END_REASON, verbose_name="SPC end reason")
    pps_mh_spc_end_reason = models.CharField(null=True, blank=True, max_length=2, choices=PPS_C.MH_SPC_END_REASON, verbose_name="SPC end reason")
    rate = models.ForeignKey('accounting.Rate', null=True, on_delete=models.SET_NULL)
    hours = models.IntegerField(blank=True, null=True, default=0.00)
    minutes = models.IntegerField(blank=False, null=False, default=0.00, verbose_name="Minutes")
    service = models.ForeignKey(Service, null=True, blank=True, on_delete=models.SET_NULL)
    diagnosis = models.ForeignKey(Diagnosis, null=True, blank=True, on_delete=models.SET_NULL)
    date = models.DateField(null=True, verbose_name="Service date")
    note = models.TextField(null=True, blank=True)
    is_open = models.BooleanField(default=True, verbose_name="Is open")
    billable = models.BooleanField(default=False, verbose_name="Client billable")
    signature = models.TextField(null=True, blank=True)
    caseworker_signature = models.TextField(null=True, blank=True)
    pps_aoda_target_group = models.CharField(null=True, blank=True, max_length=2, choices=PPS_C.AODA_TARGET_GROUP)
    pps_core_target_group = models.CharField(null=True, blank=True, max_length=2, choices=PPS_C.CORE_TARGET_GROUP)
    target_group = models.CharField(null=True, blank=True, max_length=2, choices=ACT_C.FULL_TARGET_GROUP)


    def get_time(self):
        if self.minutes is None:
            minutes = "00"
        else:
            minutes = str(self.minutes).ljust(2,'0')

        if self.hours is None:
            hours = "0"
        else:
            hours = str(self.hours)

        return "{}:{}".format(hours, minutes)


    def get_pps_epi_name(self):
        if self.pps_epi_uuid:
            return utils.get_pps_epi(self.pps_epi_uuid).get_name()




    def get_sig_items(self):
        if self.diagnosis:
            diag_id = self.diagnosis.id
        else:
            diag_id = None

        if self.service:
            serv_id = self.service.id
        else:
            serv_id = None

        sig_items = {
            "client_id" : self.client.id,
            "rate_id" : self.rate.id,
            "service_id" : serv_id,
            "diagnosis_id" : diag_id,
            "minutes" : self.minutes,
            "note" : self.note,
            "billable" : self.billable,
            "caseworker_signature" : self.caseworker_signature,
        }
        return sig_items

    def generate_signature(self, save=False):
        signed_data = utils.sign_data(self.get_sig_items())
        if save:
            self.signature = signed_data
        return signed_data

    def verify_signature(self):
        return utils.compare_signatures(self.generate_signature(), self.signature)

    def verify_caseworker_signature(self):
        return utils.verify_signature(self.caseworker_signature)

    def get_signature_datetime(self):
        if self.caseworker_signature:
            signer = TimestampSigner(salt=None)
            try:
                unsigned = signer.unsign(self.signature)
            except BadSignature:
                print("Bad signature")
                return None

            # Best way I've found to convert a UTC unix timestamp into
            # a django timezone aware datetime. This is required because
            # when the signature is encoded the timestamp is in unix time
            unix_ts = b62_decode(unsigned.split(":")[-2])
            utc = datetime.datetime.utcfromtimestamp(unix_ts).replace(tzinfo=pytz.UTC)
            localtz = utc.astimezone(pytz.timezone(settings.TIME_ZONE))

            return localtz
        else:
            return None



    def get_short_rate(self):
        if self.rate is not None:
            return self.rate.name + " ($"+str(self.rate.amount)+")"
        else:
            return ""

    def get_signer_user(self):
        """ Returns the UserConfig of whoever closed this note """
        if self.is_open:
            return None
        #import pdb; pdb.set_trace()
        id = int(re.search('"user_id\\\\": [0-9]+', self.signature)[0].replace('"user_id\\": ',''))
        
        match = get_object_or_None(apps.get_model("auth","User"), id=id)
        #import pdb; pdb.set_trace()        
        return match


    def get_signer_full_name(self):
        signer = self.get_signer_user()

        if signer:
            return signer.userconfig.full_name
        else:
            return None


        


    def __str__(self):
        return str(self.client.name()) + " | " + str(self.service) + " " + str(self.date.strftime(settings.HUMAN_DATE_FORMAT))





def upload_to(instance, filename):
    #print("THE INSTANCE {}".format(dir(instance)))
    request = instance.request
    return "casenote_files/{}/{}/{}".format(request.user.id, instance.case_note.client_id, filename)

class CaseNoteFile(models.Model):
    case_note = models.ForeignKey(CaseNote, on_delete=models.CASCADE)
    description = models.CharField(max_length=5000)  # TODO change this to a text field 
    external_origin = models.CharField(max_length=256, blank=True, null=True, verbose_name="External Origin")
    cn_file = models.FileField(null=True, blank=True, upload_to=upload_to, verbose_name="File")


    def __str__(self):
        return self.description + " [" + str(self.cn_file.name) + "]"







class EmrEpisode(models.Model):  
    episode_type = models.CharField(max_length=255, choices=CLIENT_C.EPISODE_TYPE)
    client = models.ForeignKey(Client, on_delete=models.PROTECT)
    opened_by = models.ForeignKey(User, related_name="emr_epi_opened_by", on_delete=models.PROTECT, null=True)
    closed_by = models.ForeignKey(User, related_name="emr_epi_closed_by", on_delete=models.PROTECT, null=True)
    start = models.DateField()
    end = models.DateField(null=True, blank=True)
    closing_reason = models.CharField(
        blank=True, 
        null=True, 
        max_length=255, 
        choices=CLIENT_C.EPISODE_CLOSE_REASON)
    closing_reason2 = models.CharField(
        verbose_name="2nd closing reason",
        blank=True, 
        null=True, 
        max_length=255, 
        choices=CLIENT_C.EPISODE_CLOSE_REASON)
    notes = models.TextField(null=True, blank=True)
    closing_reason3 = models.CharField(
        verbose_name="3rd closing reason",
        blank=True,
        null=True,
        max_length=255,
        choices=CLIENT_C.EPISODE_CLOSE_REASON)

    pps_mh_end_reason = models.CharField(
        verbose_name="PPS episode closing reason",
        max_length=2,
        choices=PPS_C.MH_EPI_END_REASON,
        null=True, blank=True
    )

    clts_target_group_choices = (
        ("pd", "PD"),
        ("dd", "DD"),
        ("mh", "MH"),
        ("pd_mh", "PD/MH"),
        ("pd_dd", "PD/DD"),
        ("dd_mh", "DD/MH"),
        ("pd_dd_mh", "PD/DD/MH"),
    )

    clts_target_group = models.CharField(
        max_length=255,
        choices=clts_target_group_choices,
        null=True, blank=True,
        verbose_name="Target group",)

    notes = models.TextField(null=True, blank=True)



    def get_total_casenote_minutes(self):
        # Returns all of the minutes added up for all the case notes in
        # this episode.
        casenotes = EmrCaseNoteForm.objects.filter(
            emrepisode__id=self.id)

        minutes = 0
        for note in casenotes:
            minutes += note.total_minutes

        return minutes



    def get_closing_reason(self):
        reasons = []
        if self.closing_reason:
            reasons.append(self.get_closing_reason_display())

        if self.closing_reason2:
            reasons.append(self.get_closing_reason2_display())

        if self.closing_reason3:
            reasons.append(self.get_closing_reason3_display())

        return reasons

    def get_closing_reason_html(self):
        reasons = self.get_closing_reason()
        return "<br>".join(reasons) if reasons else ""



    def get_presenting_problem(self):
        # Finds all the emr forms linked to this episode and generates
        # a list of presenting problems found in the forms
        forms = EmrForm.objects.filter(emrepisode=self)
        p_probs = []
        for form in forms:
            inst = form.get_emrform_instance()

            if hasattr(inst, "get_presenting_problem") and inst.get_presenting_problem:
                p_probs.append(inst.get_presenting_problem_display())

            if hasattr(inst, "get_presenting_problem2") and inst.get_presenting_problem2:
                p_probs.append(inst.get_presenting_problem2_display())

            if hasattr(inst, "get_presenting_problem3") and inst.get_presenting_problem3:
                p_probs.append(inst.get_presenting_problem3_display())


        return p_probs if p_probs else None



    def get_presenting_problem_html(self):
        p_probs = self.get_presenting_problem()
        return ", ".join(p_probs) if p_probs else ""


    def get_char_code(self):
        forms = EmrForm.objects.filter(emrepisode=self)
        c_codes = []
        for form in forms:
            inst = form.get_emrform_instance()
            if (hasattr(inst, "char_code")
                    and inst.char_code
                    and inst.get_char_code_display()
                    and inst.get_char_code_display() not in c_codes):
                c_codes.append(inst.get_char_code_display())

        return sorted(c_codes)


    def get_char_code_html(self):
        c_codes = self.get_char_code()
        return ", ".join(c_codes) if c_codes else ""




    def clean_fields(self, exclude=None):
        super().clean_fields(exclude)

        if self.start:
            start_overlap = EmrEpisode.objects.filter(
                episode_type=self.episode_type,
                start__lte=self.start,
                end__gte=self.start,
                client=self.client,
            ).exclude(id=self.id).first()


            if start_overlap:
                raise ValidationError({
                    "start":
                        (f"""Start date overlaps with an existing 
                        {self.get_episode_type_pretty()} episode's start date.                       
                        """),
                })

        if self.end:
            end_overlap = EmrEpisode.objects.filter(
                episode_type=self.episode_type,
                start__lte=self.end,
                end__gte=self.end,
                client=self.client,
            ).exclude(id=self.id).first()

            if end_overlap:
                raise ValidationError({
                    "start":
                        (f"""End date overlaps with an existing 
                        {self.get_episode_type_pretty()} episode's end date.
                        """),
                })



        # Check if an existing m

        '''raise ValidationError({
                    'end':(
                        'Set status to draft if there is not a '
                        'publication date.'
                     ),
                })
        '''



    def save(self, *args, **kwargs):
        new_object = False
        closing = False
        if self.id is None:
            new_object = True
        else:
            existing = get_object_or_None(EmrEpisode, id=self.id)
            if existing and existing.end is None and self.end is not None:
                # The episode is receiving an end date while the existing
                # database copy has a blank end date.
                # The user is closing the episode.
                closing = True




        super().save(*args, **kwargs)

        if new_object:
            utils.new_or_closing_episode_notification(self, "new")

        elif closing:
            utils.new_or_closing_episode_notification(self, "closing")




    def __str__(self):
        return f"{self.get_episode_type_pretty()} {self.client} ({self.id})"


    def get_episode_number(self):
        # Look for other episodes of the same type and user. Order by
        # start date and returnt the index of this episode
        episodes = EmrEpisode.objects.filter(
            episode_type=self.episode_type, client=self.client
        ).order_by("start", "id").values("id")

        if len(episodes) == 0:
            return None

        episode_list = [x["id"] for x in episodes]

        # Adding one because users don't expect things to start at 0
        return episode_list.index(self.id) + 1


    def has_multiple_episodes(self):
        # Returns true if the client has more than 1 episode of this type
        count = EmrEpisode.objects.filter(
            episode_type=self.episode_type, client=self.client
        ).count()

        if count > 1:
            return True
        else:
            return False





    def get_episode_type_pretty(self):
        # This is not needed just use self.get_episode_type_display()!
        if self.episode_type == "clinic_preadmit":
            return "Clinic Pre-Admit"

        if self.episode_type == "clts":
            return "CLTS"

        if self.episode_type == "crisis":
            return "Crisis"

        if self.episode_type == "cst":
            return "CST"

        if self.episode_type == "randi":
            return "R & I"

        if self.episode_type == "tcm":
            return "TCM"

        if self.episode_type == "clts_preadmit":
            return "CLTS Pre-Admit"

        if self.episode_type == "b3":
            return "Birth to 3"

        if self.episode_type == "b3_preadmit":
            return "Birth to 3 Pre-Admit"


    def get_submodel(self):
        if self.episode_type == "clinic_preadmit":
            return EmrClinicPreadmitEpisode.objects.get(id=self.id)

        if self.episode_type == "clts":
            return EmrCltsEpisode.objects.get(id=self.id)

        if self.episode_type == "crisis":
            return EmrCrisisEpisode.objects.get(id=self.id)

        if self.episode_type == "cst":
            return EmrCstEpisode.objects.get(id=self.id)

        if self.episode_type == "randi":
            return EmrRandiEpisode.objects.get(id=self.id)

        if self.episode_type == "tcm":
            return EmrTcmEpisode.objects.get(id=self.id)

        if self.episode_type == "clts_preadmit":
            return EmrCltsPreadmitEpisode.objects.get(id=self.id)

        if self.episode_type == "b3":
            return EmrB3Episode.objects.get(id=self.id)

        if self.episode_type == "b3_preadmit":
            return EmrB3PreadmitEpisode.objects.get(id=self.id)






    def get_form_doc_types_query(self):
        query = Q()
        if self.episode_type == "clinic_preadmit":
            query.add(Q(type_clinic_preadmit=True), Q.OR)

        if self.episode_type == "clts":
            query.add(Q(type_clts=True), Q.OR)

        if self.episode_type == "clts_preadmit":
            query.add(Q(type_clts_preadmit=True), Q.OR)

        if self.episode_type == "crisis":
            query.add(Q(type_crisis=True), Q.OR)

        if self.episode_type == "cst":
            query.add(Q(type_cst=True), Q.OR)

        if self.episode_type == "randi":
            query.add(Q(type_randi=True), Q.OR)

        if self.episode_type == "tcm":
            query.add(Q(type_tcm=True), Q.OR)

        if self.episode_type == "b3":
            query.add(Q(type_b3=True), Q.OR)

        if self.episode_type == "b3_preadmit":
            query.add(Q(type_b3_preadmit=True), Q.OR)

        return query




    def get_doc_types(self):
        # Returns a dict list of emr documents under this episode
        # and their count.
        # This is used to generate the html select lists that show on
        # the left side of the emr_episode_edit view
        if not self.id:
            return None

        emr_doc_types = []

        query = self.get_form_doc_types_query()


        # Same as above but for document types
        emr_doc_types_result = EmrDocType.objects.filter(
            query).exclude(active=False).order_by("name")

        emr_doc_types = []
        for res_doc_type in emr_doc_types_result:
            doc_count = len(EmrDoc.objects.filter(
                emrdoctype=res_doc_type,
                emrepisode__episode_type=self.episode_type,
                emrepisode__client=self.client,
            ))

            emr_doc_types.append({
                "name": res_doc_type.name,
                "id": res_doc_type.id,
                "count": doc_count,
                "doc_type": res_doc_type,
            })

        return emr_doc_types




    def get_form_types(self):
        # Depending on the type of episode, return a list of emr forms related
        # to that episode and the number of existing forms of that type
        # This is used to generate the html select lists that show on
        # the left side of the emr_episode_edit view
        if not self.id:
            return None

        emr_form_types = []

        query = self.get_form_doc_types_query()

        # Get a list of forms available for this episode
        emr_form_types_result = EmrFormType.objects.filter(
            query).exclude(active=False).order_by("name")


        # TCM AODA and MH PPS forms are a bit different. They come in admission and
        # discharge and we want both of the forms to be grouped. The idea is
        # that the users can see all their PPS data in one place. The code
        # below groups up those forms totals and adds them to the form list.
        # there's also a check below in the normal loop to prevent any of the
        # normal PPS forms from showing up in the list - since we've already
        # added them here.
        pps_forms_to_skip = None
        # We just use the admission types as the 'base' form type, but
        # we'll pull both the admission and discharge forms and provide
        # buttons in the view and template. This honestly is just to make
        # the rest of the logic here work properly without have to refactor.
        if self.episode_type == "tcm":
            pps_forms_to_skip = [
                "pps aoda admission",
                "pps aoda discharge",
                "pps mental health admission",
                "pps mental health discharge"
            ]
            pps_aoda_form_count = EmrForm.objects.filter(
                emrformtype__name__in=["PPS AODA Admission", "PPS AODA Discharge"],
                emrepisode__episode_type=self.episode_type,
                emrepisode__client=self.client
            ).count()

            pps_mh_form_count = EmrForm.objects.filter(
                emrformtype__name__in=["PPS Mental Health Admission", "PPS Mental Health Discharge"],
                emrepisode__episode_type=self.episode_type,
                emrepisode__client=self.client
            ).count()

            pps_aoda_form_type = EmrFormType.objects.get(name__iexact="pps aoda admission", type_tcm=True)
            pps_mh_form_type = EmrFormType.objects.get(name__iexact="pps mental health admission", type_tcm=True)

            emr_form_types.append({
                "name": "PPS AODA",
                "id": pps_aoda_form_type.id,
                "count": pps_aoda_form_count,
                "form_type": pps_aoda_form_type,
            })
            emr_form_types.append({
                "name": "PPS MH",
                "id": pps_mh_form_type.id,
                "count": pps_mh_form_count,
                "form_type": pps_mh_form_type,
            })

        elif self.episode_type == "cst":
            pps_forms_to_skip = [
                "pps mental health admission",
                "pps mental health discharge"
            ]
            pps_mh_form_count = EmrForm.objects.filter(
                emrformtype__name__in=["PPS Mental Health Admission", "PPS Mental Health Discharge"],
                emrepisode__episode_type=self.episode_type,
                emrepisode__client=self.client
            ).count()

            pps_mh_form_type = EmrFormType.objects.get(name__iexact="pps mental health admission", type_cst=True)

            emr_form_types.append({
                "name": "PPS MH",
                "id": pps_mh_form_type.id,
                "count": pps_mh_form_count,
                "form_type": pps_mh_form_type,
            })









        for res_form_type in emr_form_types_result:
            # Some post processing of these form types, we want to ease-of-life
            # show how many docs this client has under each form type
            found_form_count = len(EmrForm.objects.filter(
                emrformtype=res_form_type,
                emrepisode__episode_type=self.episode_type,
                emrepisode__client=self.client,
            ))


            # This skips any of the grouped TCM or CST forms that we handled manually
            # above
            if self.episode_type in ["tcm", "cst"] and res_form_type.get_name() in pps_forms_to_skip:
                continue
            else:
                emr_form_types.append({
                    "name": res_form_type.name,
                    "id": res_form_type.id,
                    "count": found_form_count,
                    "form_type": res_form_type,
                })

            # Add the different case note types to the form list as well
            if res_form_type.get_name() == "case note":
                note_type_options = []

                # Depending on the self type we may have different
                # case note types to display...
                if self.episode_type == "randi":
                    note_type_options = CLIENT_C.EMR_RANDI_CASE_NOTE_TYPE
                elif self.episode_type == "clts":
                    note_type_options = CLIENT_C.EMR_CLTS_CASE_NOTE_TYPE


                # Generate the dict list that will show up on the left
                # side dropdown, allowing the users to select which case
                # note subtype to filter by
                for note_type in note_type_options:
                    note_form_count = len(EmrCaseNoteForm.objects.filter(
                        emrformtype=res_form_type,
                        emrepisode__episode_type=self.episode_type,
                        emrepisode__client=self.client,
                        case_note_type=note_type[0]
                    ))

                    emr_form_types.append({
                        "name": f" - {note_type[1]}",
                        "id": note_type[0],
                        "count": note_form_count,
                        "form_type": res_form_type,
                    })

        return emr_form_types







    def get_casenote_form_type(self):
        # Returns the valid form type ID for the casenotes under this episode
        form_type = None
        if self.episode_type == "clinic_preadmit":
            form_type = EmrFormType.objects.get(
                active=True,
                name__iexact="case note",
                type_clinic_preadmit=True,
            )
        elif self.episode_type == "clts":
            form_type = EmrFormType.objects.get(
                active=True,
                name__iexact="case note",
                type_clts=True,
            )
        elif self.episode_type == "clts_preadmit":
            form_type = EmrFormType.objects.get(
                active=True,
                name__iexact="case note",
                type_clts_preadmit=True,
            )
        elif self.episode_type == "cst":
            form_type = EmrFormType.objects.get(
                active=True,
                name__iexact="case note",
                type_cst=True,
            )
        elif self.episode_type == "randi":
            form_type = EmrFormType.objects.get(
                active=True,
                name__iexact="case note",
                type_randi=True,
            )
        elif self.episode_type == "tcm":
            form_type = EmrFormType.objects.get(
                active=True,
                name__iexact="case note",
                type_tcm=True,
            )

        elif self.episode_type == "b3":
            form_type = EmrFormType.objects.get(
                active=True,
                name__iexact="case note",
                type_b3=True,
            )
        elif self.episode_type == "b3_preadmit":
            form_type = EmrFormType.objects.get(
                active=True,
                name__iexact="case note",
                type_b3_preadmit=True,
            )


        if form_type is None:
            raise Exception(f"Episode type: '{self.episode_type}' not implemented for episode.get_casenote_form_type()")
        else:
            return form_type



    def needs_randi_finalize(self):
        # The purpose of this is to tell us if the client has open R&I
        # intake that is not finalized. Return a list of forms that are
        # under this episode but not finalized
        if self.episode_type == "randi":
            raw_forms = self.emrform_set.all().filter(is_finalized=False)
            # loop through and filter out and case note forms
            forms = [
                x for x in raw_forms
                if x.emrformtype.get_name() != 'case note'
            ]

            return forms
        else:
            return None


    def randi_finalize_date(self):
        # Looks at any R&I intake forms under this episode that have
        # a finalized date and returns the latest one. Typically there
        # will only ever be a single intake form under an episode
        if self.episode_type == "randi":
            raw_forms = self.emrform_set.all().filter(
                is_finalized=True).order_by("-finalized_date")

            forms = [
                x for x in raw_forms if x.emrformtype.get_name() == "intake"]
            if forms:
                return forms[0]
            else:
                return None
        else:
            return None

    def get_randi_referral_requested_services(self):
        # Get all the requested services from the external and internal
        # referral forms
        if self.episode_type == "randi":
            raw_forms = self.emrform_set.all().order_by("-date")
            forms = [
                x for x in raw_forms
                if x.emrformtype.get_name() in ["internal referral", "external referral"]
            ]

            html_data = []
            for form in forms:
                items = form.get_emrform_instance().get_requested_services()
                for item in items:
                    if item not in html_data:
                        html_data.append(item)

            if html_data:
                return "<br>".join(html_data)
            else:
                return None

        else:
            return None


    def get_clinic_preadmit_requested_services_html(self):
        # Get all the requested services from the external and internal
        # referral forms

        if self.episode_type == "clinic_preadmit":
            raw_forms = self.emrform_set.all().order_by("-date")
            forms = [
                x for x in raw_forms
                if x.emrformtype.get_name() == "clinic wait list"
            ]

            html_data = []
            for form in forms:
                items = form.get_emrform_instance().get_requested_services()
                for item in items:
                    if item not in html_data:
                        html_data.append(item)


            if html_data:
                return "<br>".join(html_data)
            else:
                return None

        else:
            return None

    def get_clinic_preadmit_legal_status(self):
        # Looks for a recent waitlist form and returns the legal_status value
        if self.episode_type == "clinic_preadmit":
            found = EmrClinicWaitlistForm.objects.filter(
                emrepisode=self).order_by("-date").first()

            if found and found.legal_status:
                return found.get_legal_status_display()
            else:
                return None

        else:
            return None

    def get_clinic_preadmit_iv_drug_use(self):
        # Looks for recent waitlist form and returns the iv_drug_use value
        if self.episode_type == "clinic_preadmit":
            found = EmrClinicWaitlistForm.objects.filter(
                emrepisode=self).order_by("-date").first()

            if found:
                return found.iv_drug_use
            else:
                return None

        else:
            return None


    def get_clinic_preadmit_pregnant(self):
        # Looks for recent waitlist form and returns the pregnant value
        if self.episode_type == "clinic_preadmit":
            found = EmrClinicWaitlistForm.objects.filter(
                emrepisode=self).order_by("-date").first()

            if found:
                return found.pregnant
            else:
                return None

        else:
            return None




















class EmrClinicPreadmitEpisode(EmrEpisode):
    # TODO put clinic preadmit-specific model data here
    pass

class EmrCltsEpisode(EmrEpisode):
    # TODO put CLTS-specific stuff here
    pass

class EmrCltsPreadmitEpisode(EmrEpisode):
    # TODO put CLTS Preadmit-specific stuff here
    pass

class EmrCrisisEpisode(EmrEpisode):
    # TODO put Crisis-specific stuff here
    pass

class EmrCstEpisode(EmrEpisode):
    # TODO put CST-specific stuff here
    pass

class EmrRandiEpisode(EmrEpisode):
    # TODO put R & I specific stuff here
    pass

class EmrTcmEpisode(EmrEpisode):
    # TODO put TCM specific stuff here
    pass

class EmrB3Episode(EmrEpisode):
    # TODO put Birth to 3 specific stuff here
    pass

class EmrB3PreadmitEpisode(EmrEpisode):
    # TODO put Birth to 3 Pre-admit specific stuff here
    pass





class EmrFormType(models.Model):
    name = models.CharField(max_length=255)
    active = models.BooleanField(default=True)
    type_clinic_preadmit = models.BooleanField(default=False, verbose_name="Clinic Pre-admit")
    type_clts = models.BooleanField(default=False, verbose_name="CLTS")
    type_crisis = models.BooleanField(default=False, verbose_name="Crisis")
    type_cst = models.BooleanField(default=False, verbose_name="CST")
    type_randi = models.BooleanField(default=False, verbose_name="R & I")
    type_tcm = models.BooleanField(default=False, verbose_name="TCM")
    type_clts_preadmit = models.BooleanField(default=False, verbose_name="CLTS Pre-admit")
    type_b3 = models.BooleanField(default=False, verbose_name="Birth to 3")
    type_b3_preadmit = models.BooleanField(default=False, verbose_name="Birth to 3 Pre-admit")
    # This form type requires a supervisor's signature. When the form is finalized
    # and signed a route is created and assigned to a supervisor and whomever
    # initially finalized the form. If the supervisor is the one finalizing, they
    # will still be required to provide their supervisor signature
    needs_supervisor_signature = models.BooleanField(default=False, verbose_name="Supervisor signature")
    electronic_sig = models.BooleanField(default=False, verbose_name="Allow electronic signatures")



    def __str__(self):
        return self.name


    def get_name(self):
        # Same as the tostring but returns the name in lowercase
        return self.name.lower()







# This will be inherited by other emr forms
class EmrForm(models.Model):
    emrepisode = models.ForeignKey(EmrEpisode, on_delete=models.PROTECT, blank=True)
    emrformtype = models.ForeignKey(EmrFormType, on_delete=models.PROTECT, blank=True)
    linked_docs = models.ManyToManyField("client.EmrDoc", blank=True, verbose_name="Link documents")
    diagnosis = models.ForeignKey(Diagnosis, null=True, blank=True, on_delete=models.PROTECT)
    service = models.ForeignKey(Service, null=True, blank=True, on_delete=models.PROTECT)
    date = models.DateField(verbose_name="Service date", null=True, blank=True)
    client_age = models.IntegerField(null=True, blank=True)

    service_start = models.TimeField(null=True, blank=True, verbose_name="Service start")
    service_end = models.TimeField(null=True, blank=True, verbose_name="Service end")
    service_start2 = models.TimeField(null=True, blank=True, verbose_name="Service start")
    service_end2 = models.TimeField(null=True, blank=True, verbose_name="Service end")
    service_start3 = models.TimeField(null=True, blank=True, verbose_name="Service start")
    service_end3 = models.TimeField(null=True, blank=True, verbose_name="Service end")
    service_start4 = models.TimeField(null=True, blank=True, verbose_name="Service start")
    service_end4 = models.TimeField(null=True, blank=True, verbose_name="Service end")
    service_start5 = models.TimeField(null=True, blank=True, verbose_name="Service start")
    service_end5 = models.TimeField(null=True, blank=True, verbose_name="Service end")
    service_start6 = models.TimeField(null=True, blank=True, verbose_name="Service start")
    service_end6 = models.TimeField(null=True, blank=True, verbose_name="Service end")
    service_start7 = models.TimeField(null=True, blank=True, verbose_name="Service start")
    service_end7 = models.TimeField(null=True, blank=True, verbose_name="Service end")
    service_start8 = models.TimeField(null=True, blank=True, verbose_name="Service start")
    service_end8 = models.TimeField(null=True, blank=True, verbose_name="Service end")
    service_start9 = models.TimeField(null=True, blank=True, verbose_name="Service start")
    service_end9 = models.TimeField(null=True, blank=True, verbose_name="Service end")
    service_start10 = models.TimeField(null=True, blank=True, verbose_name="Service start")
    service_end10 = models.TimeField(null=True, blank=True, verbose_name="Service end")

    service_minutes = models.IntegerField(default=None, null=True, blank=True)
    travel_minutes = models.IntegerField(default=None, null=True, blank=True)
    doc_minutes = models.IntegerField(default=None, null=True, blank=True)
    total_minutes = models.IntegerField(default=0, null=True, blank=True)
    units = models.DecimalField(null=True, blank=True, max_digits=5, decimal_places=1)
    place_of_service = models.CharField(max_length=255, null=True, blank=True, choices=(CLIENT_C.PLACE_OF_SERVICE_CODE) + (CLIENT_C.PLACE_OF_SERVICE_CODE_B3))
    # Contact_type is deprecated and will be removed soon. Migrating from the
    # select to the booleans is handled in 0353 migration file
    contact_type = models.CharField(max_length=255, null=True, choices=CLIENT_C.CONTACT_TYPE, blank=True)
    ct_collateral = models.BooleanField(default=False, verbose_name="Collateral")
    ct_direct = models.BooleanField(default=False, verbose_name="Direct")
    ct_face = models.BooleanField(default=False, verbose_name="Face to face")
    ct_home = models.BooleanField(default=False, verbose_name="Home visit")
    ct_record = models.BooleanField(default=False, verbose_name="Record keeping")
    ct_other = models.BooleanField(default=False, verbose_name="Other/unknown")
    crisis_type_of_contact = models.CharField(max_length=255, null=True, blank=True, verbose_name="Type of contact", choices=CLIENT_C.CRISIS_TYPE_OF_CONTACT)
    see_case_file = models.BooleanField(default=False)
    rights_offered = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    billing_code = models.CharField(max_length=255, null=True, blank=True)
    signature = models.TextField(null=True, blank=True)
    created_by = models.ForeignKey(User, related_name="emr_form_created_by", on_delete=models.PROTECT, null=True, blank=True)
    credentials = models.TextField(null=True, blank=True, verbose_name="Credentials", choices=CLIENT_C.STAFF_CRED_CHOICES)
    # TODO With the introduction of electronic sigs, this field should
    # be renamed to 'finalized_by'
    signed_by = models.ForeignKey(User, related_name="emr_form_signed_by", on_delete=models.PROTECT, null=True, blank=True)
    pwd_provided = models.BooleanField(default=False)
    note = models.TextField(null=True, blank=True)
    is_finalized = models.BooleanField(default=False)
    finalized_date = models.DateTimeField(null=True, blank=True)
    supervisor_pwd_provided = models.BooleanField(default=False)
    supervisor_signer = models.ForeignKey(User, related_name="emr_form_supervisor_signer", on_delete=models.PROTECT, null=True, blank=True)
    supervisor_signature = models.TextField(null=True, blank=True)
    supervisor_signed_date = models.DateTimeField(null=True, blank=True)
    history = models.JSONField(null=True, default=list)




    def __str__(self):
        return f"{self.emrepisode} {self.date} ({self.id})"



    def get_caseworkers(self):
        # Depending on the type of the form, there may be different fields
        # for the caseworker (and credentials) who created/worked on this form
        # This extra query may add some time, but I figured it was better than
        # checking against a hardcoded string using
        #   self.emrformtyp.get_name() == "nwc assess and response plan form"


        caseworkers = []
        instance = self.get_emrform_instance()

        if self.emrepisode.episode_type == "crisis":
            # Case notes and NWC notes can pull directly from the worker
            # who created the note, other crisis forms need to check
            # for the staff_name and staff_creds fields
            if self.emrformtype.get_name() in ["case note", "county assessment and response plan"]:
                worker = f"{self.created_by.userconfig.full_name}"
                if self.credentials:
                    worker += f" {self.get_credentials_display()}"
                elif self.created_by.userconfig.credentials:
                    worker += f" {self.created_by.userconfig.get_credentials_display()}"

                caseworkers.append(worker)

            else:
                if hasattr(instance, "staff_name1") and instance.staff_name1:
                    caseworkers.append(f"{instance.staff_name1} {instance.get_staff_creds1_display()}")

                if hasattr(instance, "staff_name2") and instance.staff_name2:
                    caseworkers.append(f"{instance.staff_name2} {instance.get_staff_creds2_display()}")




        # All other non-crisis forms can pull from the worker's credentials
        # that are saved in their userconfig
        else:
            caseworkers.append(f"{self.created_by.userconfig.full_name} {self.created_by.userconfig.get_credentials_display() or ''}")

        return caseworkers






    def get_diagnosis_codes(self):
        # Returns a list of diagnosis codes for this form. Some forms will
        # have more than 1 diagnosis code possible so always return these
        # in a list
        instance = self.get_emrform_instance()

        codes = []

        codes.append(instance.diagnosis)

        # There may be n-number of diag codes on one of these emr forms
        # this loops through the form attributes and checks for attributes
        # that start with diagnosis then checks that the value of is a
        # diagnosis model object and then adds that to the list.
        for fieldname in dir(instance):
            if fieldname.startswith("diagnosis"):
                diag_value = getattr(instance, fieldname)
                if diag_value and isinstance(diag_value, Diagnosis) and diag_value not in codes:
                    codes.append(diag_value)

        return codes

    def get_diagnosis_codes_html(self):
        codes = self.get_diagnosis_codes()
        if codes:
            return "<br>".join([str(x) for x in codes if x])



    def get_contact_types(self):
        # If this is a crisis form, it uses a different type of contact
        # type than the b3 and clts case notes. As of right now (2024-02-12)
        # b3/clts and crisis are the only case notes and emr forms that
        # use contact type information.
        if self.emrepisode.episode_type == "crisis":
            form_type = self.emrformtype.get_name()

            # Crisis type of contact is a little different. The case note
            # pulls from the EmrForm.crisis_type_of_contact field while
            # the other crisis forms ave their own type_of_contact field.
            # The only reason that it's this way is because the crisis case
            # note requires the type_of_contact field which is different
            # than the other case note's contact_type field.
            # Once all the case note forms are broken out into their own
            # forms the crisis case note will have its own type_of_contact
            # field.


            # For County Assessment, it's captured by Assessment Type.
            instance = self.get_emrform_instance()

            if form_type == "case note":
                return instance.get_crisis_type_of_contact_display()
            else:
                if hasattr(instance, "type_of_contact"):
                    return instance.get_type_of_contact_display()

        else:
            cts = []

            if self.ct_collateral:
                cts.append("Collateral")

            if self.ct_direct:
                cts.append("Direct")

            if self.ct_face:
                cts.append("Face to face")

            if self.ct_home:
                cts.append("Home visit")

            if self.ct_record:
                cts.append("Record keeping")

            if self.ct_other:
                cts.append("Other/unknown")


            return ", ".join(cts)




    def prep_for_copy(self):
        # Sometimes a newly created form will need to have its information
        # pre-filled from another existing form of the same type in the same
        # episode. This is needed because caseworkers may need to file a new
        # assessment periodically and the information in the assessment is
        # largely the same. This function clears fields needed.
        self.id = None
        self.pk = None
        self.date = None
        self.created_at = None
        self.created_by = None
        self.supervisor_pwd_provided = False
        self.supervisor_signer = None
        self.supervisor_signature = None
        self.supervisor_signed_date = None
        self.history = None
        self.reopen()




    def ready_for_signature(self):
        # Returns True if this form:
        # 1. Requires a supervisor to sign
        # 2. Is finalized by a worker
        # 3. Has a caseworker signature
        # 4. Does not have a supervisor signature
        if self.emrformtype.needs_supervisor_signature is not True:
            return False
        elif self.is_finalized is not True:
            return False
        elif not self.signature:
            return False
        elif self.supervisor_signature is not None:
            return False


        else:
            return True





    def reopen(self):
        # Reopens the emr form. This involves removing the caseworker's
        # signature and clearing out related fields. The MHP and other digital
        # signatures may remain on the form. The caseworker is going into
        # the form to make edits, and the data for the form's original state
        # is saved in the form's history field.

        self.signature = None
        self.signed_by = None
        self.pwd_provided = False
        self.is_finalized = False
        self.finalized_date = None

        # Case notes will have the is_open field
        if hasattr(self, "is_open"):
            self.is_open = True

        self.save()



    def get_emrform_sigs(self):
        return EmrFormSignature.objects.filter(emrform=self).order_by("created_at")


    def supervisor_can_sign(self, supervisor_user):
        #print("Checking if supervisor can sign")
        # Returns a true if this form type has needs_supervisor_signature and the
        # supervisor_user passed has the appropriate bit set to sign for forms
        # in this specific episode type and any other criteria are met to
        # allow for this supervisor to sign this form.

        # First check if there are any conditions that will right away prevent
        # the form from being supervisor signed. The first and most obvious is that
        # it's already been signed or it's not even a emr form type that can be
        # signed by a supervisor

        # The form must be first signed by a caseworker before it can be signed
        # by a supervisor
        if self.is_finalized is not True or not self.signed_by or not self.finalized_date:
            #print("Supervisor cannot sign because this form is not signed by a caseworker")
            return False

        # Form can't be supervisor signed, it's not set up to need supervisor sigs
        if self.emrformtype.needs_supervisor_signature is not True:
            #print("Cannot sign because this form does not need supervisor signature")
            return False

        # It's OK if the form is already signed by this user. There is a step
        # in the signing process where the user is applied before the actual
        # signature. So it's OK if there is a supervisor signer already, just
        # as long as it's the same user as is being used to sign
        # However, if there is a supervisor_signed_date, then  we know for sure
        # this cannot be signed
        if self.supervisor_signer is not None and self.supervisor_signer != supervisor_user:
            #print("Cannot sign because this form doesn't have a supervisor signer user applied")
            return False

        if self.supervisor_signed_date or self.supervisor_signature:
            #print("Cannot sign because there is already a supervisor signed date")
            return False



        # Looking at the different types of episodes, checking if the
        # supervisor_user's supervisor signer bit's match. If they do, they can
        # sign this form and True is returned.'
        bit_string = f"super_{self.emrepisode.episode_type}"

        if getattr(supervisor_user.userconfig, bit_string) is True:
            return True
        else:
            #print(f"Cannot sign because this user does not have the supervisor signature bit for {self.emrepisode.episode_type}")
            return False




    def notify_name(self):
        # a name for this model instance that is an appropriate content and
        # length to show up in a notification
        return f"{self.emrformtype.name} for {self.emrepisode.client.name()} ({self.emrepisode.client.id})"


    def get_user_routes(self, user, active_only=False):
        # Gets the routes for this object, requires the logged in user.
        # We do not want users to see routes that are not assigned to them
        # unless they are the creator of the route
        routes = apps.get_model(
            "config", "Route"
        ).objects.filter(
            resource_id=self.id,
            resource_app="client",
            resource_model="emrform",
            assigned_to=user,
        ).order_by("-created_at")

        if active_only:
            routes = routes.filter(completed_at=None)


        return routes



    def default_url(self):
        return f"/client/emr_form/{self.id}/"


    def get_linked_docs(self):
        # This would probably be better off in a template
        if len(self.linked_docs.all()) > 0:
            fmt_linked_docs = "<br>"
            for linked_doc in self.linked_docs.all():
                fmt_linked_docs += f"{linked_doc}<br>"

        else:
            fmt_linked_docs = "None provided"

        return fmt_linked_docs


    def get_emrform_instance(self):
        return utils.get_emrform_instance(self.id, self.emrepisode, self.emrformtype)


    def get_first_finalized(self):
        # Gets the data from the first time this form was finalized. Sometimes,
        # a form will need to get re-opened to fix incorrect data. The form
        # may be fixed by an admin or a manager and we still want to retain
        # who initially filled out and finalized the form, even though the
        # form has been re-finalized by someone else.
        if not self.history:
            return None

        for item in self.history:
            if item["action"] == "finalize":
                if item["fields"]["finalized_date"] and isinstance(item["fields"]["finalized_date"], str):
                    item["fields"]["finalized_date"] = parse(item["fields"]["finalized_date"])

                if item["fields"]["signed_by"] and isinstance(item["fields"]["signed_by"], int):
                    item["fields"]["signed_by"] = get_object_or_None(User, id=item["fields"]["signed_by"])
                return item




    def get_history(self):
        # Taking history into account, returns a list of the opened date(s) for
        # this model
        history_data = []

        if self.history:
            for item in self.history:
                history_data.append({
                    "action": item["action"],
                    "action_at": parse(item["action_at"]),
                    "user": get_object_or_None(User, id=item["user_id"]),
                    #"is_finalized": item["fields"]["is_finalized"],
                    #"finalized_date": parse(item["fields"]["finalized_date"]) if item["fields"]["finalized_date"] else None,
                    "reopen_reason": item["reopen_reason"],
                })

        return history_data



    def save_history(self, action, request):
        print("Saving emr form history")
        # This function is expecting the BASE EMR FORM as the form_instance.
        # When the form is re-opened, we want to record that event. There should
        # be a reason why the user is reopening the form. On the front-end,
        # the users will be able to see the form's history.
        # This method should work if we need to update a form's history without
        # re-opening (for some unforeseen future feature requirement)
        # However, if the action is 'reopen' then this method requires
        # reopen_reason in the POST data.
        # 1. Serialize this form and it subform
        # 2. Combine the fields and prevent duplicates into a single JSON
        # 3. incorporate the user's reason for the edit and other metadata
        # 4. save it to the form's history field

        if action not in ["open", "reopen", "finalize", "supervisor_sign"]:
            raise Exception(f"Action of '{action}' is not supported")

        # 1. Serializing the forms and then parsing them back into a python object
        #   so they can be manipulated. if someone knows a better way to do this,
        #   please let me know

        # if this form contains the emrform_ptr then we know it's a subform,
        # otherwise we need to do a lookup for the subform so we can
        # gather all the field data
        if hasattr(self, "emrform_ptr"):
            mainform_data = json.loads(serializers.serialize("json", (self.emrform_ptr, )))[0]
            subform_data = json.loads(serializers.serialize("json", (self.get_emrform_instance(), )))[0]
        else:
            mainform_data = json.loads(serializers.serialize("json", (self, )))[0]
            subform_data = json.loads(serializers.serialize("json", (self.get_emrform_instance(), )))[0]

        new_history = {
            "fields": {}
        }

        # These fields will be excluded from being saved in the form's history
        # fields like history (do need to track itself) the signature field
        # we really just want the history of the data in the form.
        exclude_fields = ["signature", "supervisor_signature", "history"]

        # 2. Adding the mainform_data and the subform_data's fields to the
        # history data
        for key, value in mainform_data["fields"].items():
            if key in exclude_fields:
                continue
            if key not in new_history["fields"]:
                new_history["fields"][key] = value


        for key, value in subform_data["fields"].items():
            if key == exclude_fields:
                continue
            if key not in new_history["fields"]:
                new_history["fields"][key] = value


        # 3. Incorporating extra metadata. The user doing the re-opening, a
        # timestamp, the reason the form is being reopened
        reason = request.POST.get("reopen_reason")
        if not reason and action == "reopen":
            reason = "No reason given"


        new_history["user_id"] = request.user.id
        new_history["user_username"] = request.user.username
        new_history["user_email"] = request.user.email
        new_history["action"] = action
        new_history["action_at"] = timezone.now().isoformat()
        new_history["reopen_reason"] = reason


        if self.history:
            self.history.append(new_history)
        else:
            self.history = [new_history, ]

        #print(f"Form history:\n{json.dumps(self.history)}")
        self.save()






    def save(self, *args, **kwargs):
        #print("Saving EmrForm")

        orig = get_object_or_None(EmrForm, id=self.id)
        if orig:
            orig_is_finalized = orig.is_finalized
            orig_supervisor_signature = orig.supervisor_signature
        else:
            orig_is_finalized = None
            orig_supervisor_signature = None

        self.units = utils.calculate_units(self)
        self.total_minutes = self.get_total_minutes()
        self.updated_at = timezone.now()
        if self.emrepisode.client.birthdate:
            if isinstance(self.date, datetime.datetime):
                self.date = self.date.date()

            self.client_age = relativedelta(self.date, self.emrepisode.client.birthdate).years

        super().save(*args, **kwargs)


        # If this form is newly finalized, then check for any pending routes
        # with the finalize task.
        # Also need to check if this formtype requires a supervisor signature.
        # if it does and there is a supervisor signer

        # If this form has any pending finalize routes associated with it
        # those can be cleared out.
        if orig and orig_is_finalized is False and self.is_finalized is True:
            routes = apps.get_model(
                "config", "Route"
            ).objects.filter(
                resource_id=self.id,
                resource_app="client",
                resource_model="emrform",
                assigned_to=self.signed_by,
                action_type="finalize",
                completed_at=None,
            ).order_by("-created_at")

            for route in routes:
                route.completed_at = timezone.now()
                route.save()




        # If the form is recently signed by a supervisor, then look for pending
        # routes and make those as complete. There may be more than one user
        # who can sign as a supervisor for this episode type. So when clearing
        # the routes, we don't need to check who it's assigned to, as long as
        # it's this form that we're updating routes for
        if orig and orig_supervisor_signature is None and self.supervisor_signature:
            mgr_routes = apps.get_model(
                "config", "Route"
            ).objects.filter(
                resource_id=self.id,
                resource_app="client",
                resource_model="emrform",
                action_type="supervisor_sign",
                completed_at=None,
            ).order_by("-created_at")

            for mgr_route in mgr_routes:
                mgr_route.completed_at = timezone.now()
                mgr_route.save()





    def get_label(self, fieldname):
        return text_type(self._meta.get_field(fieldname).verbose_name)


    def get_total_minutes(self):
        srv_min = self.service_minutes or 0
        trv_min = self.travel_minutes or 0
        doc_min = self.doc_minutes or 0
        return srv_min + trv_min + doc_min

    def get_total_hours_rounded(self, prec=None):
        # Returns total_minutes in hour format rounded to the precision
        # defined, default is 1 decimal space. Also support for half-hour
        # rounding.
        mins = Decimal(self.total_minutes)
        hours = mins / Decimal("60.0")
        if prec is None:
            rounded_hours = round(hours, 1)
        elif prec == "half hour":
            rounded_hours = round(hours * Decimal("2.0")) / Decimal("2.0")
        else:
            rounded_hours = round(hours, prec)

        return rounded_hours


    def get_units(self):
        return utils.calculate_units(self, None)


    def get_rate(self):
        # Depending on the episode type or the billing code being used
        # figure out and return the type of rate that this case note
        # will use to generate a transaction.

        if self.emrepisode.episode_type == "clinic_preadmit":
            rate = utils.accounting.models.Rate.objects.filter(
                active=True, clinic_preadmit_rate=True, year=self.date.year).first()

        elif self.emrepisode.episode_type == "clts":
            rate = utils.accounting.models.Rate.objects.filter(
                active=True, clts_rate=True, year=self.date.year).first()

        # Right now, the users want CLTS preadmit to use the normal CLTS
        # rates, but I added a 'clts_preadmit_rate' field to the Rage model
        # in case we need to use it in the future.
        elif self.emrepisode.episode_type == "clts_preadmit":
            rate = utils.accounting.models.Rate.objects.filter(
                active=True, clts_preadmit_rate=True, year=self.date.year).first()

        elif self.emrepisode.episode_type == "crisis":
            rate = utils.accounting.models.Rate.objects.filter(
                active=True, crisis_rate=True, year=self.date.year).first()

        elif self.emrepisode.episode_type == "cst":
            rate = utils.accounting.models.Rate.objects.filter(
                active=True, cst_rate=True, year=self.date.year).first()

        elif self.emrepisode.episode_type == "randi":
            rate = utils.accounting.models.Rate.objects.filter(
                active=True, randi_rate=True, year=self.date.year).first()

        elif self.emrepisode.episode_type == "tcm":
            rate = utils.accounting.models.Rate.objects.filter(
                active=True, tcm_rate=True, year=self.date.year).first()

        elif self.emrepisode.episode_type == "b3":
            rate = utils.accounting.models.Rate.objects.filter(
                active=True, b3_rate=True, year=self.date.year).first()
        elif self.emrepisode.episode_type == "b3_preadmit":
            rate = utils.accounting.models.Rate.objects.filter(
                active=True, b3_preadmit_rate=True, year=self.date.year).first()

        else:
            raise Exception(f"No rate found for {self.emrepisode.episode_type}")


        if not rate:
            raise Exception(f"Could not find a rate for {self.emrepisode.episode_type} with the year {self.date.year}")
        else:
            return rate




    def get_sig_items(self):
        sig_items = {}

        for key, value in self.__dict__.copy().items():
            if key in ["_state", "signature", "updated_at"]:
                continue

            sig_items[key] = str(value)

        return sig_items


    def generate_supervisor_signature(self, request):
        sig_data = {
            "emrform_id": self.id,
            "user": request.user.id,
            "user_email": request.user.email,
            "user_type": request.user.userconfig.user_type,
            "user_name": request.user.userconfig.full_name,
            "user_credentials": request.user.userconfig.credentials,
            "ip_address": utils.get_client_ip(request),
            "useragent": request.headers.get("user-agent"),
        }

        signed_data = utils.sign_data(sig_data)

        return signed_data


    def generate_signature(self, save=False):
        signed_data = utils.sign_data(self.get_sig_items())
        if save:
            self.signature = signed_data
        return signed_data

    def verify_signature(self):
        return utils.compare_signatures(self.generate_signature(), self.signature)


    def get_signature_datetime(self):
        if self.signature:
            signer = TimestampSigner(salt=None)
            try:
                unsigned = signer.unsign(self.signature)
            except BadSignature:
                print("Bad signature")
                return None

            # Best way I've found to convert a UTC unix timestamp into
            # a django timezone aware datetime
            #import pdb; pdb.set_trace()
            unix_ts = b62_decode(self.signature.split(":")[-2])
            utc = datetime.datetime.utcfromtimestamp(unix_ts).replace(tzinfo=pytz.UTC)
            localtz = utc.astimezone(pytz.timezone(settings.TIME_ZONE))

            return localtz
        else:
            return None


    def is_journaled(self):
        # Returns true if this form is attached to a transaction that has
        # been journaled.
        if self.id:
            found = apps.get_model("accounting", "Transaction").objects.filter(emr_form__id=self.id)
            if found and found.first().journal_entry:
                return True
            else:
                return False
        else:
            return False


    def get_days_to_finalized(self):
        # Returns the number of days between the service date and the
        # first finalized date of this form. If the form has no finalized date
        # then return null
        # This is only used on the Time to Note Report so it returns a
        # pre-formatted string.
        first_final = self.get_first_finalized()
        if not first_final:
            days = f'{(timezone.now().date() - self.date).days}+'

        else:
            days = str((first_final["fields"]["finalized_date"].date() - self.date).days)


        return days

    def get_days_to_finalized_int(self):
        # Similar to get_days_to_finalized above but just returns a raw
        # integer.
        first_final = self.get_first_finalized()
        if not first_final:
            days = (timezone.now().date() - self.date).days

        else:
            days = (first_final["fields"]["finalized_date"].date() - self.date).days


        return days













class EmrFormSignature(models.Model):
    # This signature model is designed to record a signature of several different
    # types and link to an emrform
    signature_types = (
        ("topaz", "Topaz signature pad"),
        ("simple_typed", "Typed"),
        ("mouse", "Mouse"),
        #"simple_scanned",
        # ...
    )

    signer_role_choices = (
        ("initiator", "Plan initiator"),
        ("mental_health_pro", "Mental health professional"),
        ("client", "Client"),
        ("parent_guardian", "Parent/guardian"),
        ("other", "Other"),
    )

    emrform = models.ForeignKey("client.EmrForm", on_delete=models.CASCADE)
    logged_in_user = models.ForeignKey("auth.User", on_delete=models.PROTECT)
    signer_first = models.CharField(
        max_length=255, verbose_name="Signer's first name")
    signer_last = models.CharField(
        max_length=255, verbose_name="Signer's last name")
    signer_middle = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name="Signer's middle name or initial")
    sig_type = models.CharField(
        max_length=255,
        choices=signature_types,
        verbose_name="Signature type")

    signer_role = models.CharField(
        max_length=255,
        choices=signer_role_choices,
        verbose_name="Signer's role")
    # JSON data of all the fields in the form at the time it was signed.
    # this will capture and 3rd party data generated by signature pads
    sig_data = models.JSONField()
    sig_image = models.BinaryField(null=True, blank=True)
    ip_address = models.GenericIPAddressField(protocol="both", unpack_ipv4=True)
    useragent = models.TextField()
    # This is different than the sig_data above. This signature field records
    # data in this model and puts it into a digital signature of its own
    # that can be verified at a later time. If any fields on this model
    # are changed, this signature won't validate
    signature = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


    def __str__(self):
        firstname = ""
        if self.signer_middle:
            firstname = f"{self.signer_first} {self.signer_middle}"
        else:
            firstname = self.signer_first

        return f"Signed by {self.signer_last}, {firstname} on {self.created_at}"


    def save(self, *args, **kwargs):
        if self.id and not self.signature:
            # Trying to save an existing signature should trigger an exception
            raise ValidationError("Updates to existing signatures are not allowed.")

        if not self.emrform:
            raise ValidationError("An EmrForm is required to create an EmrFormSignature")

        if self.emrform.is_finalized is not True:
            raise ValidationError("EmrForm must be finalized to have signatures applied")

        # Depending on the sig_type, different data must be present in the
        # sig_data JSON

        if self.sig_type == "simple_typed":
            if "typed_sig" not in self.sig_data or not self.sig_data["typed_sig"]:
                raise ValidationError("typed_sig is missing from the sig_data")
            elif len(self.sig_data["typed_sig"].strip()) < 2:
                raise ValidationError("typed_sig must be at least 3 characters")

        elif self.sig_type == "topaz":
            if "topaz_sigraw_data" not in self.sig_data or not self.sig_data["topaz_sigraw_data"]:
                raise ValidationError("topaz_sigraw_data is missing")

            if "topaz_image_data" not in self.sig_data or not self.sig_data["topaz_image_data"]:
                raise ValidationError("topaz_image_data is missing")

        elif self.sig_type == "mouse":
            if "mouse_image_data" not in self.sig_data or not self.sig_data["mouse_image_data"]:
                raise ValidationError("mouse_image_data is missing")

        if not self.signer_role:
            raise ValidationError("signer_role is missing")




        # Because the encrypted signature captures this model's created_at
        # field, it needs to be saved first and then have the sig generated
        # and then saved once again without going into an infinite save loop
        if not self.signature:
            # save first, generating the auto-generated timestamps
            super().save(*args, **kwargs)

            # Generate and set the signature
            self.signature = self.generate_signature()

            # Save again, now with the signature
            super().save(update_fields=['signature'])
        else:
            raise ValidationError("Updates to existing signatures are not allowed.")







    def get_sig_items(self):
        sig_items = {}

        exclude = ["_state", "signature", "updated_at"]


        for key in sorted(self.__dict__):
            if key in exclude:
                continue

            value = self.__dict__[key]

            if isinstance(value, (datetime.date, datetime.datetime)):
                sig_items[key] = value.isoformat()
            else:
                sig_items[key] = str(value)

        return sig_items




    def generate_signature(self):
        signed_data = utils.sign_data(self.get_sig_items())
        return signed_data

    def verify_signature(self):
        return utils.compare_signatures(self.generate_signature(), self.signature)


    def get_signature_datetime(self):
        if self.signature:
            signer = TimestampSigner(salt=None)
            try:
                unsigned = signer.unsign(self.signature)
            except BadSignature:
                print("Bad signature")
                return None

            # Best way I've found to convert a UTC unix timestamp into
            # a django timezone aware datetime
            #import pdb; pdb.set_trace()
            unix_ts = b62_decode(self.signature.split(":")[-2])
            utc = datetime.datetime.utcfromtimestamp(unix_ts).replace(tzinfo=pytz.UTC)
            localtz = utc.astimezone(pytz.timezone(settings.TIME_ZONE))

            return localtz
        else:
            return None



# TODO see if this model is actually needed or if the EmrForm model can
# handle these few fields
class EmrCaseNoteForm(EmrForm):
    case_note_type = models.CharField(
        max_length=255,
        null=True, 
        blank=True, 
        choices=CLIENT_C.EMR_CASE_NOTE_TYPE,
        default="general",
    )
    is_open = models.BooleanField(default=True)
    closed_at = models.DateTimeField(null=True, blank=True)
    individuals_present = models.TextField(null=True, blank=True)





class EmrCrisisNwcNoteForm(EmrForm):

    yesno_choices = (
        (None, "----------"),
        ("yes", "Yes"),
        ("no", "No"),
    )

    law_dept_choices = (
        ("na", "Not applicable"),
        ("jcsd", "Jackson County Sheriff’s Dept."),
        ("brpd", "Black River Falls Police Department"),
        ("hcnpd", "Ho-Chunk Nation Police Department"),
        ("other", "Other"),
    )

    day_choices = (
        ("sunday", "Sunday"),
        ("monday", "Monday"),
        ("tuesday", "Tuesday"),
        ("wednesday", "Wednesday"),
        ("thursday", "Thursday"),
        ("friday", "Friday"),
        ("saturday", "Saturday"),
    )





    # Contact summary details (includes date field from EmrForm)
    part_of_existing = models.BooleanField(default=False, verbose_name="This form is part of an existing contact")
    alcohol_substance_factor = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Was alcohol/substance use a factor during Crisis Contact?")
    law_inv = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Was law enforcement involved with the Crisis Contact?")
    law_dept = models.CharField(max_length=255, null=True, blank=True, choices=law_dept_choices, verbose_name="Law enforcement department involved")
    law_dept_other = models.CharField(max_length=255, null=True, blank=True, verbose_name="Other law enforcement department")
    outcome_of_contact = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.CRISIS_OUTCOMES)
    outcome_other = models.TextField(null=True, blank=True, verbose_name="Other outcome to contact")
    summary_highlight = models.CharField(max_length=255, null=True, blank=True, choices=yesno_choices, verbose_name="Was a summary highlight provided on the NWC Note?")
    summary_highlight_dtl = models.TextField(null=True, blank=True, verbose_name="Summary highlight narrative")
    county_of_residence = models.CharField(max_length=255, choices=CLIENT_C.WI_COUNTY, null=True, blank=True)
    county_of_incident = models.CharField(max_length=255, choices=CLIENT_C.WI_COUNTY, null=True, blank=True)
    day_of_week = models.CharField(max_length=255, null=True, blank=True, choices=day_choices)
    # Billing section
    # place of service
    # diagnosis
    # billing_code
    staff_name1 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Staff name")
    staff_creds1 = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.STAFF_CRED_CHOICES, verbose_name="Staff credentials")
    type_of_contact = models.TextField(null=True, blank=True, choices=CLIENT_C.CRISIS_TYPE_OF_CONTACT, verbose_name="Type of contact")

    # PPS
    brc_target_pop = models.CharField(null=True, blank=True, verbose_name="BRC target population", max_length=1, choices=PPS_C.BRC_TARGET_POP)
    presenting_problem = models.CharField(null=True, blank=True, verbose_name="Primary Presenting Problem", max_length=2, choices=PPS_C.PRESENTING_PROBLEM)
    char_code = models.CharField(null=True, blank=True, verbose_name="Characteristics code", max_length=2, choices=PPS_C.CLIENT_CHAR)
    ref_source = models.CharField(max_length=255, null=True, blank=True, choices=PPS_C.REF_SOURCE, verbose_name="Referral source")
    spc_end_reason = models.CharField(null=True, blank=True, max_length=2, choices=PPS_C.MH_SPC_END_REASON, verbose_name="SPC end reason")



    def save(self, *args, **kwargs):
        if not self.id:
            self.brc_target_pop = "S"

        super().save(*args, **kwargs)





class EmrCrisisResponsePlanReviewForm(EmrForm):
    brief_summary = models.TextField(
        null=True, blank=True, verbose_name="Brief summary")
    safety = models.TextField(
        null=True,
        blank=True,
        verbose_name="Safety (summarize any safety concerns since last response plan)")
    protective_factors = models.TextField(
        null=True, blank=True, verbose_name="Protective factors")
    plan = models.TextField(null=True, blank=True, verbose_name="Plan")






class EmrJacksonCountyAssesResponseForm(EmrForm):
    vet_status_choices = (
        (None, "----------"),
        ("no", "No, not a veteran"),
        ("yes", "Yes, has served on active duty"),
        ("not_obtain", "Did not obtain"),
        ("unable_to_obtain", "Unable to obtain"),
    )


    parent_contacted_choices = (
        ("na", "Not applicable"),
        ("yes", "Yes"),
        ("no", "No"),
    )


    yes_no_summary_choices = (
        ("yes", "Yes"),
        ("no", "No"),
        ("other", "Other - see summary"),
    )

    prior_hospital_history_choices = (
        ("voluntary", "Voluntary"),
        ("involuntary", "Involuntary"),
        ("both", "Both: Voluntary and involuntary"),
        ("no", "No history"),
        ("unknown", "Unable to determine"),
    )

    yes_no_refused_choices = (
        ("yes", "Yes"),
        ("no", "No"),
        ("refused_unable", "Refused/Unable"),
    )


    law_dept_choices = (
        ("na", "Not applicable"),
        ("jcsd", "Jackson County Sheriff’s Dept."),
        ("brpd", "Black River Falls Police Department"),
        ("hcnpd", "Ho-Chunk Nation Police Department"),
        ("other", "Other"),
    )

    actions_time_choices = (
        ("over_a_year", "Over a year ago"),
        ("3_mo_to_year", "Between 3 months and 1 year ago"),
        ("last_3_mo", "Within the last 3 months"),
    )

    nwc_reach_at_choices = (
        ("clark", "Clark County Daytime: 800/863-3560"),
        ("crawford", "Crawford County Daytime: 608/362-0248"),
        ("dodge", "Dodge County Daytime: 920/386-4094"),
        ("manitowoc", "Manitowoc County Daytime: 920/683-4230"),
        ("marinette", "Marinette County Daytime: 715/732-7760"),
        ("sauk", "Sauk County Daytime: 800/553-5692"),
    )

    county_dept_choices = (
        ("adams", "Adams HHS, 608-339-4505"),
        ("barron", "Barron DHHS, 715-537-5691"),
        ("buffalo", "Buffalo DHHS, 608-685-4412"),
        ("chippewa", "Chippewa DHS, 715-726-7788"),
        ("clark", "Clark Cty CS, 715-743-5208"),
        ("columbia", "Columbia HHS, 608-742-9227"),
        ("crawford", "Crawford HS, 608-326-0248"),
        ("dodge", "Dodge HSHD, 920-386-4094"),
        ("dunn", "Dunn DHS, 715-232-1116"),
        ("eau_claire", "Eau Claire DHS, 715-839-2300"),
        ("green", "Green HS, 608-328-9393"),
        ("jackson", "Jackson DHHS, 715-284-4301"),
        ("lafayette", "Lafayette DHS, 608-776-4800"),
        ("manitowoc", "Manitowoc HSD, 920-683-4230"),
        ("marinette", "Marinette HHS (ADAPT) Marinette, 715-732-7760"),
        ("Marinette_niagara", "Marinette HHS (ADAPT) Niagara, 715-251-4555"),
        ("marquette", "Marquette CHS, Behav. Health, 608-297-3181"),
        ("marquette_gen", "Marquette CHS, General, 608-297-3124"),
        ("monroe", "Monroe DHS, 608-269-8600"),
        ("pepin", "Pepin DHS, 715-672-8941"),
        ("pierce", "Pierce DHS, 715-273-6770"),
        ("polk", "Polk DHS, 715-485-8400"),
        ("richland", "Richland DHHS, 608-647-8821"),
        ("sauk", "Sauk DHS, 608-355-4200"),
        ("stcroix", "St. Croix DHHS, 715-246-6991"),
        ("trempealeau", "Trempealeau CHS, 715-538-2311"),
        ("ucs_lancaster", "Unified Community Service, Lancaster, 608-723-6357"),
        ("ucs_dodge", "Unified Comm Services, Dodgeville, 608-935-2776"),
        ("non_covered_county", "Non-Covered County"),
    )

    utl_choices = (
        ("not_utl", "and not utilized as"),
        ("utl", "and utilized; see details above"),
    )

    not_utl_reason_choices = (
        ("more_restrict", "more restrictive intervention appropriate"),
        ("less_restrict", "less restrictive intervention appropriate"),
        ("mobile_resp", "mobile response deemed appropriate"),
        ("case_transfer", "protocol indicates need to case transfer"),
        ("no_jail_deviate", "presentation not appropriate to deviate from jail"),
        ("undetermined", "undetermined due to medical need"),
    )



    rights_choices = (
        ("verbally", "Verbally"),
        ("paper_copy", "Paper copy"),
        ("both", "Both"),
    )

    mandated_reporting_choices = (
        ("na", "Not applicable"),
        ("child_protection_notified", "Child Protection notification communicated to"),
        ("adult_protection_notified", "Adult Protection notification communicated to"),
        ("duty_to_warn", "Duty to Warn communicated to"),
    )



    yes_no_na_choices = (
        (None, "----------"),
        ("yes", "Yes"),
        ("no", "No"),
        ("na", "Not applicable"),
    )



    # Demographics
    part_of_existing = models.BooleanField(default=False, verbose_name="This form is part of an existing contact")
    living_arr = models.CharField(max_length=255, null=True, blank=True, choices=PPS_C.LIVING_STAT, verbose_name="Living arrangement")
    veteran_status = models.CharField(null=True, blank=True, max_length=255, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Is individual a veteran?")
    veteran_status_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    student_status = models.CharField(null=True, blank=True, max_length=255, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Is individual a student?")
    school_name = models.TextField(null=True, blank=True, verbose_name="School name/location")
    has_insurance = models.CharField(null=True, blank=True, max_length=255, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Does the individual have insurance?")
    has_insurance_dtl = models.TextField(null=True, blank=True, verbose_name="Please add details such as insurance carrier & ID numbers")
    county_of_residence = models.CharField(max_length=255, choices=CLIENT_C.WI_COUNTY, null=True, blank=True)
    # PPS
    brc_target_pop = models.CharField(null=True, blank=True, verbose_name="BRC target population", max_length=1, choices=PPS_C.BRC_TARGET_POP, default="S")
    presenting_problem = models.CharField(max_length=255, null=True, blank=True, verbose_name="Presenting problem code", choices=PPS_C.PRESENTING_PROBLEM)
    char_code = models.CharField(null=True, blank=True, verbose_name="Characteristics code", max_length=2, choices=sorted(PPS_C.CLIENT_CHAR, key=lambda x: x[1]))
    commitment_status = models.CharField(max_length=255, null=True, blank=True, verbose_name="Commitment status code", choices=PPS_C.LEGAL_STAT)
    ref_source = models.CharField(max_length=255, null=True, blank=True, choices=PPS_C.REF_SOURCE, verbose_name="Referral source")
    spc_end_reason = models.CharField(null=True, blank=True, max_length=2, choices=PPS_C.MH_SPC_END_REASON, verbose_name="SPC end reason")


    # Billing
    # place_of_service
    # diagonsis
    # billing_code
    # minutes/time boxes
    staff_name1 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Staff name")
    staff_creds1 = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.STAFF_CRED_CHOICES, verbose_name="Staff credentials")
    staff_name2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Staff name")
    staff_creds2 = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.STAFF_CRED_CHOICES, verbose_name="Staff credentials")
    type_of_contact = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.CRISIS_TYPE_OF_CONTACT)
    caller_name = models.CharField(max_length=255, null=True, blank=True, verbose_name="Caller name")
    caller_phone1 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    caller_phone1_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="Ext")
    caller_phone2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="2nd Phone")
    caller_phone2_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="Ext")
    caller_agency_or_rel = models.CharField(max_length=255, null=True, blank=True, verbose_name="Agency or relationship")

    # Supports
    contact_name = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    contact_relationship = models.CharField(max_length=255, null=True, blank=True, verbose_name="Relationship")
    contact_address = models.TextField(null=True, blank=True, verbose_name="Address")
    contact_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_name2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    contact_relationship2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Relationship")
    contact_address2 = models.TextField(null=True, blank=True, verbose_name="Address")
    contact_phone2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_name3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    contact_relationship3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Relationship")
    contact_address3 = models.TextField(null=True, blank=True, verbose_name="Address")
    contact_phone3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_name4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    contact_relationship4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Relationship")
    contact_address4 = models.TextField(null=True, blank=True, verbose_name="Address")
    contact_phone4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_name5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    contact_relationship5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Relationship")
    contact_address5 = models.TextField(null=True, blank=True, verbose_name="Address")
    contact_phone5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    parent_guardian = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Does the individual have a parent, guardian, or Power of Attorney?")
    parent_guardian_name = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    parent_guardian_address = models.TextField(null=True, blank=True, verbose_name="Address")
    parent_guardian_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    parent_guardian_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    parent_guardian_contacted = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Was the parent/guardian/power of attorney contacted as part of this assessment?")
    parent_guardian_contacted_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    # Rights and grievance
    rights_informed = models.CharField(max_length=255, null=True, blank=True, choices=rights_choices, verbose_name="How was information related to individual rights, informed consent, and HIPAA provided?")
    # Mandated Reporting
    mandated_reporting = models.CharField(max_length=255, null=True, blank=True, choices=mandated_reporting_choices, verbose_name="Mandated reporting")
    mandated_reporting_dtl = models.TextField(null=True, blank=True, verbose_name="Name")
    # Assessment
    was_interviewed = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_CHOICES, verbose_name="Was the individual interviewed?")
    not_interviewed_reason = models.TextField(null=True, blank=True, verbose_name="If not, why?")

    law_inv = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Was law enforcement involved with the Crisis Contact?")
    law_dept = models.CharField(max_length=255, null=True, blank=True, choices=law_dept_choices, verbose_name="Law enforcement department involved")
    law_dept_other = models.CharField(max_length=255, null=True, blank=True, verbose_name="Other law enforcement department")

    previous_crisis_contacts = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Previous crisis contacts")
    previous_crisis_contacts_dtl = models.TextField(null=True, blank=True, verbose_name="Brief summary of any known prior crisis contacts")
    alcohol_sa_factor = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Was alcohol/substance use a factor in this call?")
    under_influence = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Was the individual under the influence of alcohol/other substances?")
    pbt = models.CharField(max_length=255, null=True, blank=True, verbose_name="PBT")
    pbt_time = models.CharField(max_length=255, null=True, blank=True, verbose_name="Time PBT obtained")
    bac = models.CharField(max_length=255, null=True, blank=True, verbose_name="BAC")
    bac_time = models.CharField(max_length=255, null=True, blank=True, verbose_name="Time BAC obtained")

    under_influence_dtl = models.TextField(null=True, blank=True, verbose_name="Please describe")
    sub_use_hist = models.TextField(null=True, blank=True, verbose_name="Substance use history (indicate substance, frequency, amount, method of use, and last use)")
    withdrawal_needs = models.TextField(null=True, blank=True, verbose_name="Withdrawal needs")
    od_history = models.TextField(null=True, blank=True, verbose_name="Overdose history")
    self_reported_diag = models.TextField(null=True, blank=True, verbose_name="Self or collateral reported mental health diagnoses")

    service_received = models.CharField(max_length=255, null=True, blank=True, verbose_name="Services received (i.e. therapy, psychiatrist, county programming)", choices=YES_NO_UNKNOWN_CHOICES)
    service_received_dtl = models.TextField(null=True, blank=True, verbose_name="If Yes, what services/program, where and contact information if available")
    followup_apt = models.TextField(null=True, blank=True, verbose_name="Follow-up appointments")
    cm = models.TextField(null=True, blank=True, verbose_name="Current medications (prescription/over the counter)", choices=YES_NO_UNKNOWN_CHOICES)
    cm_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    cm_as_prescribed = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Are these taken as prescribed?")
    cm_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    cm_name = models.TextField(null=True, blank=True, verbose_name="Name")
    cm_phone = models.TextField(null=True, blank=True, verbose_name="Phone")
    cm_phone_ext = models.TextField(null=True, blank=True, verbose_name="ext")
    prior_hosp_hist = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="History of prior hospitalizations")
    prior_hosp_hist_dtl = models.TextField(null=True, blank=True, verbose_name="Details (when and where)")
    legal_stat_hist = models.CharField(max_length=255, null=True, blank=True, verbose_name="Legal status and history (i.e. probation, parole, civil commitment, guardianship)", choices=YES_NO_UNKNOWN_CHOICES)
    legal_stat_hist_dtl = models.TextField(null=True, blank=True, verbose_name="Details")






    # Columbia-Suicide Severity Rating Scale
    cs_scale_wish_dead = models.CharField(max_length=255, null=True, blank=True, verbose_name="1. Have you wished you were dead or wished you could go to sleep and not wake up?", choices=yes_no_refused_choices)
    cs_scale_wish_dead_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    cs_scale_thoughts = models.CharField(max_length=255, null=True, blank=True, choices=yes_no_refused_choices, verbose_name="2. Have you actually had any thoughts of killing yourself?")
    cs_scale_thoughts_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    # Subsection:
    #   If YES to 2, ask questions 3, 4, 5 and 6.  If NO to 2, go directly to question 6
    cs_scale_how_kill = models.CharField(max_length=255, null=True, blank=True, choices=yes_no_refused_choices, verbose_name="3. Have you been thinking about how you might kill yourself?")
    cs_scale_how_kill_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    cs_scale_intent = models.CharField(max_length=255, blank=True, null=True, verbose_name="4. Have you had these thoughts and had some intention of acting on them?", choices=yes_no_refused_choices)
    cs_scale_intent_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    cs_scale_details = models.CharField(max_length=255, null=True, blank=True, choices=yes_no_refused_choices, verbose_name="5. Have you started to work out or worked out the details of how to kill yourself?")
    cs_scale_details_intent = models.CharField(max_length=255, null=True, blank=True, choices=yes_no_refused_choices, verbose_name="Do you intend to carry out this plan?")
    cs_scale_details_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    cs_scale_actions = models.CharField(max_length=255, null=True, blank=True, choices=yes_no_refused_choices, verbose_name="6. Have you ever done anything, started to do anything, or prepared to do anything to end your life?")
    cs_scale_actions_time = models.CharField(max_length=255, null=True, blank=True, choices=actions_time_choices, verbose_name="How long ago did you do any of these?")
    cs_scale_actions_dtl = models.TextField(null=True, blank=True, verbose_name="Details")

    # HOMICIDALITY / SELF-INJURY / LETHAL MEANS section
    cs_scale_thoughts_someone_else = models.CharField(max_length=255, null=True, blank=True, choices=yes_no_refused_choices, verbose_name="7. Are you having thoughts or feelings of wanting to harm or kill someone else?")
    cs_scale_thoughts_someone_else_prepare = models.CharField(max_length=255, null=True, blank=True, choices=yes_no_refused_choices, verbose_name="Have you ever done anything or prepared to do anything to harm/kill someone?")
    cs_scale_thoughts_someone_else_dtl = models.TextField(null=True, blank=True, verbose_name="Details")

    si_behavior = models.CharField(max_length=255, null=True, blank=True, choices=yes_no_refused_choices, verbose_name="Self-injury behavior (i.e. cutting, burning, etc.)")
    si_how_often = models.TextField(null=True, blank=True, verbose_name="How often do you injure yourself?")
    si_method = models.TextField(null=True, blank=True, verbose_name="How do you harm yourself (method)?")
    si_intent = models.TextField(null=True, blank=True, verbose_name="What is the intent or purpose of your self-injury?")
    si_access = models.CharField(max_length=255, null=True, blank=True, choices=yes_no_refused_choices, verbose_name="Access to firearms, potential hazards or items identified for self-injury and/or suicide")
    si_access_dtl = models.TextField(null=True, blank=True, verbose_name="Details")

    # Mental Status section
    #   Orientation
    ori_alert = models.BooleanField(default=False, verbose_name="Alert")
    ori_not_ori = models.BooleanField(default=False, verbose_name="Not oriented")
    ori_person = models.BooleanField(default=False, verbose_name="Person")
    ori_place = models.BooleanField(default=False, verbose_name="Place")
    ori_time = models.BooleanField(default=False, verbose_name="Time")
    ori_situation = models.BooleanField(default=False, verbose_name="Situation")
    #   Speech
    speech_rapid = models.BooleanField(default=False, verbose_name="Rapid")
    speech_slurred = models.BooleanField(default=False, verbose_name="Slurred")
    speech_delayed = models.BooleanField(default=False, verbose_name="Delayed")
    speech_normal = models.BooleanField(default=False, verbose_name="Normal")
    #   Though Content/Processes
    thought_delusions = models.BooleanField(default=False, verbose_name="Delusions")
    thought_disorganized = models.BooleanField(default=False, verbose_name="Disorganized")
    thought_hallucinations = models.BooleanField(default=False, verbose_name="Hallucinations")
    thought_paranoid = models.BooleanField(default=False, verbose_name="Paranoid")
    thought_unknown = models.BooleanField(default=False, verbose_name="Unknown cognitive impairment")
    thought_organized = models.BooleanField(default=False, verbose_name="Organized")
    #   Mood
    mood_depressed = models.BooleanField(default=False, verbose_name="Depressed")
    mood_elevated = models.BooleanField(default=False, verbose_name="Elevated")
    mood_anxious = models.BooleanField(default=False, verbose_name="Anxious")
    mood_guarded = models.BooleanField(default=False, verbose_name="Guarded")
    mood_irritable = models.BooleanField(default=False, verbose_name="Irritable")
    mood_normal = models.BooleanField(default=False, verbose_name="Normal")
    #   Behavior
    behave_cooperative = models.BooleanField(default=False, verbose_name="Cooperative")
    behave_impulsive = models.BooleanField(default=False, verbose_name="Impulsive")
    behave_aggressive = models.BooleanField(default=False, verbose_name="Aggressive")
    behave_uncooperative = models.BooleanField(default=False, verbose_name="Uncooperative")
    behave_withdrawn = models.BooleanField(default=False, verbose_name="Withdrawn/Guarded")
    behave_agitated = models.BooleanField(default=False, verbose_name="Agitated/Restless")
    #   Intellectual functioning
    intel_below_avg = models.BooleanField(default=False, verbose_name="Below average")
    intel_above_avg = models.BooleanField(default=False, verbose_name="Above average")
    intel_avg = models.BooleanField(default=False, verbose_name="Average")
    intel_not_noted = models.BooleanField(default=False, verbose_name="Not noted")
    #   Insight
    insight_good = models.BooleanField(default=False, verbose_name="Good")
    insight_fair = models.BooleanField(default=False, verbose_name="Fair")
    insight_poor = models.BooleanField(default=False, verbose_name="Poor")
    #   Judgement
    judgement_good = models.BooleanField(default=False, verbose_name="Good")
    judgement_fair = models.BooleanField(default=False, verbose_name="Fair")
    judgement_poor = models.BooleanField(default=False, verbose_name="Poor")
    # Summary of Assessment section
    #   Initial caller concerns, presenting needs of individual, collateral
    #   information specific to current crisis, desired outcome from all parties.
    summary_of_assessment = models.TextField(null=True, blank=True, verbose_name="Summary of assessment")
    risk_factors = models.TextField(null=True, blank=True, verbose_name="Risk factors")
    protective_factors = models.TextField(null=True, blank=True, verbose_name="Protective factors")
    coping_skills = models.TextField(null=True, blank=True, verbose_name="Coping skills")
    supports = models.TextField(null=True, blank=True, verbose_name="Supports")
    # Response Plan
    rp_outcome = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.CRISIS_OUTCOMES, verbose_name="Outcome")
    rp_con_complete = models.CharField(max_length=255, null=True, blank=True, choices=yes_no_na_choices, verbose_name="For an individual under the age of 21, was the Certification of Need form completed for an admission to WMHI?")
    rp_outcome_facility = models.CharField(max_length=255, null=True, blank=True, verbose_name="Facility")
    rp_plan = models.TextField(null=True, blank=True, verbose_name="Plan (indicate why outcome was chosen versus other outcomes)")
    rp_follow_up = models.TextField(null=True, blank=True, verbose_name="Follow-up appointments")
    rp_coping_skills = models.TextField(null=True, blank=True, verbose_name="Coping skills")
    rp_supports = models.TextField(null=True, blank=True, verbose_name="Supports")
    rp_nwc_contact_provided = models.BooleanField(default=False, verbose_name="Northwest Connection/Jackson County Crisis Line provided")
    resource_988_provided = models.BooleanField(default=False, verbose_name="988 Resource provided")
    other_resource_provided = models.BooleanField(default=False, verbose_name="Other resource provided")
    other_resource_provided_dtl = models.TextField(null=True, blank=True, verbose_name="Details")

    ed_provided = models.CharField(max_length=255, null=True, blank=True, choices=yes_no_na_choices, verbose_name="Was authorization for emergency detention provided?")
    #ed_na = models.BooleanField(default=False, verbose_name="Not applicable")
    #   Authorization provided to
    ed_title = models.CharField(max_length=255, null=True, blank=True, verbose_name="Title")
    ed_name = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    ed_dept = models.CharField(max_length=255, null=True, blank=True, verbose_name="Law enforcement agency")

    '''
    county_crisis_line = models.CharField(max_length=255, null=True, blank=True, verbose_name="County crisis line (if different)")
    county_department = models.CharField(max_length=255, null=True, blank=True, verbose_name="County department")
    home_indv_exp1 = models.CharField(max_length=255, null=True, blank=True, choices=utl_choices, verbose_name="Home individually was explored")
    home_indv_exp2 = models.CharField(max_length=255, null=True, blank=True, choices=not_utl_reason_choices, verbose_name="Home individually not utilized as")
    telephone_stab_exp = models.CharField(max_length=255, null=True, blank=True, choices=utl_choices, verbose_name="Telephone stabilization was explored")
    telephone_stab_exp2 = models.CharField(max_length=255, null=True, blank=True, choices=not_utl_reason_choices, verbose_name="Telephone stabilization not utilized as")
    csp_exp = models.CharField(max_length=255, null=True, blank=True, choices=utl_choices, verbose_name="Community support plan was explored")
    csp_exp2 = models.CharField(max_length=255, null=True, blank=True, choices=not_utl_reason_choices, verbose_name="Community support plan not utilized as")
    crisis_stab_exp = models.CharField(max_length=255, null=True, blank=True, choices=utl_choices, verbose_name="Crisis stabilization placement was explored")
    crisis_stab_exp2 = models.CharField(max_length=255, null=True, blank=True, choices=not_utl_reason_choices, verbose_name="Crisis stabilization not utilized as")
    vol_detox_exp = models.CharField(max_length=255, null=True, blank=True, choices=utl_choices, verbose_name="Voluntary detox was explored")
    vol_detox_exp2 = models.CharField(max_length=255, null=True, blank=True, choices=not_utl_reason_choices, verbose_name="Voluntary detox not utilized as")
    vol_hospital_exp = models.CharField(max_length=255, null=True, blank=True, choices=utl_choices, verbose_name="Voluntary Hospitalization was explored")
    vol_hospital_exp2 = models.CharField(max_length=255, null=True, blank=True, choices=not_utl_reason_choices, verbose_name="Voluntary Hospitalization not utilized as")
    ch5145_exp = models.CharField(max_length=255, null=True, blank=True, choices=utl_choices, verbose_name="Chapter 51.45 was explored")
    ch5145_exp2 = models.CharField(max_length=255, null=True, blank=True, choices=not_utl_reason_choices, verbose_name="Chapter 51.45 not utilized as")
    ch55_exp = models.CharField(max_length=255, null=True, blank=True, choices=utl_choices, verbose_name="Chapter 55 was explored")
    ch55_exp2 = models.CharField(max_length=255, null=True, blank=True, choices=not_utl_reason_choices, verbose_name="Chapter 55 not utilized as")
    ch5115_exp = models.CharField(max_length=255, null=True, blank=True, choices=utl_choices, verbose_name="Chapter 51.15 was explored")
    ch5115_exp2 = models.CharField(max_length=255, null=True, blank=True, choices=not_utl_reason_choices, verbose_name="Chapter 51.15 not utilized as")
    '''

    # Additional Items Addressed in Response Plan
    rec_secure_fire = models.BooleanField(default=False, verbose_name="Recommendation to secure firearms")
    rec_secure_fire_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    rec_secure_fire_comto = models.TextField(null=True, blank=True, verbose_name="Communicated to")

    rec_secure_other = models.BooleanField(default=False, verbose_name="Recommendation to secure other potential hazards")
    rec_secure_other_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    rec_secure_other_comto = models.TextField(null=True, blank=True, verbose_name="Communicated to")

    rec_other = models.BooleanField(default=False, verbose_name="Other")
    rec_other_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    rec_other_comto = models.TextField(null=True, blank=True, verbose_name="Communicated to")

    #loc_placement = models.TextField(null=True, blank=True, verbose_name="Location of placement")
    #loc_admin_conf = models.BooleanField(default=False, verbose_name="Admission confirmed")
    #loc_admin_conf_plan = models.BooleanField(default=False, verbose_name="Admission confirmed plan created")
    #transport = models.CharField(max_length=255, null=True, blank=True, verbose_name="Transportation provided by")







class EmrNwcAssesResponseForm(EmrForm):
    type_of_contact_choices = (
        ("telehealth", "Telehealth"),
        ("audio", "Audio-only"),
        ("mobile", "Mobile"),
        ("mobile_team", "Mobile team response"),
        ("walkin", "Walk-in"),
    )


    law_dept_choices = (
        ("na", "Not applicable"),
        ("jcsd", "Jackson County Sheriff’s Dept."),
        ("brpd", "Black River Falls Police Department"),
        ("hcnpd", "Ho-Chunk Nation Police Department"),
        ("other", "Other"),
    )

    day_choices = (
        ("sunday", "Sunday"),
        ("monday", "Monday"),
        ("tuesday", "Tuesday"),
        ("wednesday", "Wednesday"),
        ("thursday", "Thursday"),
        ("friday", "Friday"),
        ("saturday", "Saturday"),
    )


    part_of_existing = models.BooleanField(default=False, verbose_name="This form is part of an existing contact")

    staff_name1 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Staff name")
    staff_creds1 = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.STAFF_CRED_CHOICES, verbose_name="Staff credentials")

    staff_name2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Staff name")
    staff_creds2 = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.STAFF_CRED_CHOICES, verbose_name="Staff credentials")

    type_of_contact = models.CharField(max_length=255, null=True, blank=True, choices=type_of_contact_choices)

    alcohol_substance_factor = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Was alcohol/substance use a factor during Crisis Contact?")
    law_inv = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Was law enforcement involved with the Crisis Contact?")
    law_dept = models.CharField(max_length=255, null=True, blank=True, choices=law_dept_choices, verbose_name="Law enforcement department Involved")
    law_dept_other = models.TextField(null=True, blank=True, verbose_name="Other law enforcement department")
    outcome_of_contact = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.CRISIS_OUTCOMES)
    mobile_worker_name = models.TextField(null=True, blank=True, verbose_name="Mobile worker")
    outcome_other = models.TextField(null=True, blank=True, verbose_name="Other outcome to contact")
    county_of_residence = models.CharField(max_length=255, choices=CLIENT_C.WI_COUNTY, null=True, blank=True)
    county_of_incident = models.CharField(max_length=255, choices=CLIENT_C.WI_COUNTY, null=True, blank=True)
    hospital_location = models.TextField(null=True, blank=True, verbose_name="Facility name and location")
    day_of_week = models.CharField(max_length=255, null=True, blank=True, choices=day_choices)
    rights_informed = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="How was information related to individual rights, informed consent, and HIPAA provided verbally?")

    # pps
    brc_target_pop = models.CharField(null=True, blank=True, verbose_name="BRC target population", max_length=1, choices=PPS_C.BRC_TARGET_POP, default="S")
    presenting_problem = models.CharField(null=True, blank=True, verbose_name="Primary presenting problem", max_length=2, choices=sorted(PPS_C.PRESENTING_PROBLEM, key=lambda x: x[1]))
    char_code = models.CharField(null=True, blank=True, verbose_name="Characteristics code", max_length=2, choices=sorted(PPS_C.CLIENT_CHAR, key=lambda x: x[1]))
    legal_status = models.CharField(max_length=255, null=True, blank=True, verbose_name="Commitment status", choices=sorted(PPS_C.LEGAL_STAT))
    ref_source = models.CharField(max_length=255, null=True, blank=True, choices=PPS_C.REF_SOURCE, verbose_name="Referral source")
    spc_end_reason = models.CharField(null=True, blank=True, max_length=2, choices=PPS_C.MH_SPC_END_REASON, verbose_name="SPC end reason")







class EmrOutOfCountyAssesResponseForm(EmrForm):
    type_of_contact_choices = (
        ("telehealth", "Telehealth"),
        ("audio", "Audio-only"),
        ("mobile", "Mobile"),
        ("mobile_team", "Mobile team response"),
        ("walkin", "Walk-in"),
    )


    law_dept_choices = (
        ("na", "Not applicable"),
        ("jcsd", "Jackson County Sheriff’s Dept."),
        ("brpd", "Black River Falls Police Department"),
        ("hcnpd", "Ho-Chunk Nation Police Department"),
        ("other", "Other"),
    )

    day_choices = (
        ("sunday", "Sunday"),
        ("monday", "Monday"),
        ("tuesday", "Tuesday"),
        ("wednesday", "Wednesday"),
        ("thursday", "Thursday"),
        ("friday", "Friday"),
        ("saturday", "Saturday"),
    )



    staff_name1 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Staff name")
    staff_creds1 = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.STAFF_CRED_CHOICES, verbose_name="Staff credentials")

    staff_name2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Staff name")
    staff_creds2 = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.STAFF_CRED_CHOICES, verbose_name="Staff credentials")

    type_of_contact = models.CharField(max_length=255, null=True, blank=True, choices=type_of_contact_choices)

    alcohol_substance_factor = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Was alcohol/substance use a factor during Crisis Contact?")
    law_inv = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Was law enforcement involved with the Crisis Contact?")
    law_dept = models.CharField(max_length=255, null=True, blank=True, choices=law_dept_choices, verbose_name="Law enforcement department Involved")
    law_dept_other = models.TextField(null=True, blank=True, verbose_name="Other law enforcement department")
    outcome_of_contact = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.CRISIS_OUTCOMES)
    mobile_worker_name = models.TextField(null=True, blank=True, verbose_name="Mobile worker")
    outcome_other = models.TextField(null=True, blank=True, verbose_name="Other outcome to contact")
    county_of_residence = models.CharField(max_length=255, choices=CLIENT_C.WI_COUNTY, null=True, blank=True)
    county_of_incident = models.CharField(max_length=255, choices=CLIENT_C.WI_COUNTY, null=True, blank=True)
    hospital_location = models.TextField(null=True, blank=True, verbose_name="Facility name and location")
    day_of_week = models.CharField(max_length=255, null=True, blank=True, choices=day_choices)
    rights_informed = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="How was information related to individual rights, informed consent, and HIPAA provided verbally?")

    # pps
    brc_target_pop = models.CharField(null=True, blank=True, verbose_name="BRC target population", max_length=1, choices=PPS_C.BRC_TARGET_POP, default="S")
    presenting_problem = models.CharField(null=True, blank=True, verbose_name="Primary presenting problem", max_length=2, choices=sorted(PPS_C.PRESENTING_PROBLEM, key=lambda x: x[1]))
    char_code = models.CharField(null=True, blank=True, verbose_name="Characteristics code", max_length=2, choices=sorted(PPS_C.CLIENT_CHAR, key=lambda x: x[1]))
    legal_status = models.CharField(max_length=255, null=True, blank=True, verbose_name="Commitment status", choices=sorted(PPS_C.LEGAL_STAT))
    ref_source = models.CharField(max_length=255, null=True, blank=True, choices=PPS_C.REF_SOURCE, verbose_name="Referral source")
    spc_end_reason = models.CharField(null=True, blank=True, max_length=2, choices=PPS_C.MH_SPC_END_REASON, verbose_name="SPC end reason")







class EmrPlacementForm(EmrForm):
    start_year = models.IntegerField(null=True, blank=True)
    start_month = models.IntegerField(null=True, blank=True)
    start_day = models.IntegerField(null=True, blank=True)
    end_year = models.IntegerField(null=True, blank=True)
    end_month = models.IntegerField(null=True, blank=True)
    end_day = models.IntegerField(null=True, blank=True)
    facility = models.TextField(verbose_name="Name of facility")
    street = models.TextField(null=True, blank=True)
    suite = models.CharField(null=True, blank=True, max_length=10, verbose_name="Unit/suite")
    city = models.CharField(max_length=50, null=True, blank=True)
    state = models.CharField(max_length=2, null=True, blank=True, choices=CLIENT_C.STATE, default="WI")
    zipcode = models.CharField(max_length=10, null=True, blank=True, verbose_name="Zip code")
    phone = models.CharField(max_length=255, null=True, blank=True)
    phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    fax = models.CharField(max_length=255, null=True, blank=True)
    email = models.CharField(max_length=255, null=True, blank=True)
    notes = models.TextField(blank=True, null=True, max_length=2048)
    county_funded = models.BooleanField(default=False, verbose_name="Funded by county")


    def get_duration(self):
        # Start and stop duration strings
        dur_start = ""
        dur_end = ""

        # Building the start string
        if self.start_year and self.start_month and self.start_day:
            dur_start = f"{self.start_month}/{self.start_day}/{self.start_year}"

        elif self.start_year and self.start_month:
            dur_start = f"{self.start_month}/{self.start_year}"

        elif self.start_year:
            dur_start = self.start_year

        # Building the end string
        if self.end_year and self.end_month and self.end_day:
            dur_end = f"{self.end_month}/{self.end_day}/{self.end_year}"

        elif self.end_year and self.end_month:
            dur_end = f"{self.end_month}/{self.end_year}"

        elif self.end_year:
            dur_end = self.end_year


        # Returning the duration in a human-readable format
        if dur_start and dur_end:
            return f"{dur_start} - {dur_end}"

        if dur_start and not dur_end:
            return f"{dur_start} - Ongoing"

        elif not dur_start and dur_end:
            return f"Unknown - {dur_end}"

        else:
            return ""

    def get_sortable_duration(self):
        return f"{self.start_year}{str(self.start_month).rjust(2,'0')}{str(self.start_day).rjust(2, '0')}"




    def get_long_address(self):
        address_data = []

        if self.street:
            if self.suite:
                address_data.append(f"{self.street}, {self.suite}")
            else:
                address_data.append(self.street)

        if self.city:
            address_data.append(self.city)

        if self.state:
            address_data.append(self.state)

        if self.zipcode:
            address_data.append(self.zipcode)


        return ", ".join(address_data)



class EmrClinicWaitlistForm(EmrForm):
    legal_status = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.RANDI_LEGAL_STAT_CHOICES, verbose_name="Legal status")
    pregnant = models.BooleanField(default=False)
    pregnant_dtl = models.TextField(null=True, blank=True, verbose_name="Pregnancy details")
    iv_drug_use = models.BooleanField(default=False, verbose_name="IV drug use")
    mh = models.BooleanField(default=False, verbose_name="MH")
    aoda = models.BooleanField(default=False, verbose_name="AODA")
    mh_and_aoda = models.BooleanField(default=False, verbose_name="MH and AODA")
    insurance_info = models.TextField(null=True, blank=True, verbose_name="Insurance information")
    adtl_info = models.TextField(null=True, blank=True, verbose_name="Additional information")



    def get_requested_services(self):
        services = []
        if self.mh:
            services.append("Mental health")
        if self.aoda:
            services.append("AODA")
        if self.mh_and_aoda:
            services.append("Mental health and AODA")


        return services


    def get_requested_services_html(self):
        return "<br>".join(self.get_requested_services())



class EmrCltsCstAssessmentForm(EmrForm):
    # Assessment Description & Background Information
    assessment_type = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.CLTS_CST_ASSESSMENT_TYPE)
    assessment_date = models.DateField(null=True, blank=True)
    medicaid_type = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.MEDICAID_TYPE)
    target_group_dd = models.BooleanField(default=False, verbose_name="Target group DD")
    target_group_pd = models.BooleanField(default=False, verbose_name="Target group PD")
    target_group_mh = models.BooleanField(default=False, verbose_name="Target group MH")
    patient_rights_and_grievances = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.CLTS_CST_RIGHTS_AND_GRIEV)
    parents = models.TextField(null=True, blank=True)
    info_collected_from = models.TextField(null=True, blank=True, verbose_name="Information collected from")
    background_info = models.TextField(null=True, blank=True, verbose_name="Background information")
    # Social functioning
    strengths_needs_social = models.TextField(null=True, blank=True, verbose_name="Strengths & needs")
    formal_informal_supports = models.TextField(null=True, blank=True, verbose_name="Formal/informal supports & friendships")
    community_involvement = models.TextField(null=True, blank=True)
    beliefs = models.TextField(null=True, blank=True, verbose_name="Cultural, ethnic, and spiritual traditions and beliefs")
    # Medical/physical
    current_health = models.TextField(null=True, blank=True, verbose_name="Diagnoses & current health")
    medication = models.TextField(null=True, blank=True)
    primary_care_dr = models.TextField(max_length=255, null=True, blank=True, verbose_name="Primary care physician")
    eye_care = models.TextField(max_length=255, null=True, blank=True)
    dental_care = models.TextField(max_length=255, null=True, blank=True)
    hearing_care = models.TextField(max_length=255, null=True, blank=True)
    specialty_care = models.TextField(max_length=255, null=True, blank=True)
    # Activities of daily living
    bathing_grooming = models.TextField(null=True, blank=True, verbose_name="Bathing and grooming")
    dressing_undressing = models.TextField(null=True, blank=True, verbose_name="Dressing & undressing")
    feeding = models.TextField(null=True, blank=True)
    toileting = models.TextField(null=True, blank=True)
    mobility_transfers = models.TextField(null=True, blank=True, verbose_name="Mobility & transfers")
    communication = models.TextField(null=True, blank=True)
    daily_living_activities = models.TextField(null=True, blank=True)
    # Emotional/Behavioral (Identify behaviors, any mental health challenges, and risks)
    emo_behave = models.TextField(null=True, blank=True, verbose_name="Emotional & behavioral")
    # Cognitive functioning
    cognitive_testing = models.TextField(null=True, blank=True)
    cognitive_summary = models.TextField(null=True, blank=True, verbose_name="Summary of cognitive functioning")
    # Education/employment
    school = models.TextField(null=True, blank=True)
    grade = models.CharField(max_length=255, null=True, blank=True)
    iep_504_plan = models.TextField(null=True, blank=True, verbose_name="IEP & 504 plan")
    physical_therapy = models.TextField(null=True, blank=True)
    speech_language = models.TextField(null=True, blank=True)
    occupational_therapy = models.TextField(null=True, blank=True)
    testing_completed = models.TextField(null=True, blank=True)
    adtl_school_info = models.TextField(null=True, blank=True, verbose_name="Additional school information")
    employed = models.TextField(null=True, blank=True)
    interest_getting_job = models.TextField(null=True, blank=True, verbose_name="Interests in getting a job")
    supports_needed_job = models.TextField(null=True, blank=True, verbose_name="Supports needed on the job")
    # Current services and supports/needs (services/supports receiving or need examples;
    # supplements, adaptive aides, home modification, specialized medical therapeutic supplies,
    # therapies etc.)
    current_services_and_needs = models.TextField(null=True, blank=True, verbose_name="Current services and supports & needs")
    # Personal Preferences
    living_and_daily = models.TextField(null=True, blank=True, verbose_name="Preference on living situation, living environment, & daily activities")
    future_goals_plans = models.TextField(null=True, blank=True, verbose_name="Future goals & plans")
    ability_to_direct = models.TextField(null=True, blank=True, verbose_name="Ability to direct supports")
    # Additional Information
    addtl_information = models.TextField(null=True, blank=True, verbose_name="Additional information")


    def save(self, *args, **kwargs):
        # This form uses assessment_date and the users don't want to see
        # or use the regular date field. Pretty much all of the rest of the emr
        # forms use the date field, so I've just set this up to just copy the
        # assessment date and if that's not available, today's date.
        # This prevents having to do extra work in validation and keeps this
        # form in-line with the rest of the EMR forms. Maybe one day remove
        # the assessment_date field and update the regular date field's label
        if self.assessment_date:
            self.date = self.assessment_date
        else:
            self.date = timezone.now().date()

        super().save(*args, **kwargs)



    def get_target_groups(self):
        groups = []
        if self.target_group_dd:
            groups.append("DD")
        if self.target_group_pd:
            groups.append("PD")
        if self.target_group_mh:
            groups.append("MH")

        return groups

    def get_target_groups_html(self):
        return ", ".join(self.get_target_groups())



class EmrCrisisPlanForm(EmrForm):
    verbal_choices = (
        (None, "----------"),
        ("verbal", "Verbal"),
        ("non_verbal", "Non-verbal"),
    )

    # Service summary/known cautions/communication sections fields
    living_situation = models.TextField(null=True, blank=True)
    caution_firearms = models.BooleanField(default=False, verbose_name="Firearms")
    caution_firearms_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    caution_animals = models.BooleanField(default=False, verbose_name="Animals")
    caution_animals_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    caution_other = models.BooleanField(default=False, verbose_name="Other")
    caution_other_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    verbal = models.TextField(
        null=True, blank=True, choices=verbal_choices, verbose_name="Verbal/non-verbal")
    other_com_preferences = models.TextField(null=True, blank=True, verbose_name="Other communication preferences")
    county_of_responsibility = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.WI_COUNTY)
    county_of_placement = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.WI_COUNTY)
    plan_developer = models.TextField(null=True, blank=True, )
    agency_name = models.TextField(null=True, blank=True, )
    agency_address = models.TextField(null=True, blank=True, )
    phone_contact = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone/contact")
    phone_contact_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")


    # Crisis Plan Information section
    strengths = models.TextField(null=True, blank=True, verbose_name="Strengths")
    needs = models.TextField(null=True, blank=True, verbose_name="Needs")
    past_behavior = models.TextField(null=True, blank=True, verbose_name="Past behaviors/situations that would indicate safety concerns (include dates and outcomes of past emergency mental health services)")
    intervention_list = models.TextField(null=True, blank=True, verbose_name="Progressive list of interventions to respond to a crisis/safety situation")
    interventions_avoid = models.TextField(null=True, blank=True, verbose_name="Approaches/interventions to avoid")
    helpful_info = models.TextField(null=True, blank=True, verbose_name="Additional helpful Info")

    # Critical Support Contacts section
    contact_name = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    contact_relation = models.CharField(max_length=255, null=True, blank=True, verbose_name="Agency/relation")
    contact_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_address = models.CharField(max_length=255, null=True, blank=True, verbose_name="Address")
    contact_note = models.TextField(null=True, blank=True, verbose_name="Note")
    contact_name2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    contact_relation2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Agency/relation")
    contact_phone2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_address2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Address")
    contact_note2 = models.TextField(null=True, blank=True, verbose_name="Note")
    contact_name3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    contact_relation3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Agency/relation")
    contact_phone3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_address3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Address")
    contact_note3 = models.TextField(null=True, blank=True, verbose_name="Note")
    contact_name4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    contact_relation4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Agency/relation")
    contact_phone4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_address4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Address")
    contact_note4 = models.TextField(null=True, blank=True, verbose_name="Note")
    contact_name5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    contact_relation5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Agency/relation")
    contact_phone5 = models.TextField(null=True, blank=True, verbose_name="Phone")
    contact_phone_ext5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_address5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Address")
    contact_note5 = models.TextField(null=True, blank=True, verbose_name="Note")

    # Usual Daily Schedule section
    daily_schedule = models.TextField(null=True, blank=True, verbose_name="Usual daily schedule (consider work, school, and social activities, regularly scheduled appointments, wake/sleep times, etc.)")

    # Diagnostic impression section
    # Built-in diagnosis field for the first one.
    # Typically these EMR forms would inherit the diagnosis field on the base
    # EmrForm but because the users want crisis plan diagnosis fields to be
    # free-fill text fields, this form has to use diagnosis1 instead
    diagnosis1 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Diagnosis")
    diag_formed_by = models.TextField(null=True, blank=True, verbose_name="Diagnosis formed by")
    diag_last_date = models.DateField(null=True, blank=True, verbose_name="Diagnosis last confirmed date")
    diagnosis2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Diagnosis")
    diag_formed_by2 = models.TextField(null=True, blank=True, verbose_name="Diagnosis formed by")
    diag_last_date2 = models.DateField(null=True, blank=True, verbose_name="Diagnosis last confirmed date")
    diagnosis3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Diagnosis")
    diag_formed_by3 = models.CharField(max_length=355, null=True, blank=True, verbose_name="Diagnosis formed by")
    diag_last_date3 = models.DateField(null=True, blank=True, verbose_name="Diagnosis last confirmed date")
    diagnosis4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Diagnosis")
    diag_formed_by4 = models.TextField(null=True, blank=True, verbose_name="Diagnosis formed by")
    diag_last_date4 = models.DateField(null=True, blank=True, verbose_name="Diagnosis last confirmed date")
    diagnosis5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Diagnosis")
    diag_formed_by5 = models.TextField(null=True, blank=True, verbose_name="Diagnosis formed by")
    diag_last_date5 = models.DateField(null=True, blank=True, verbose_name="Diagnosis last confirmed date")

    past_mh_diag = models.TextField(null=True, blank=True, verbose_name="Past mental health diagnoses")
    known_needs = models.TextField(null=True, blank=True, verbose_name="Known physical or medical diagnoses and/or needs")
    prescriptions = models.TextField(null=True, blank=True, verbose_name="Psychotropic and medical medications including PRNs")
    past_emh_services = models.TextField(null=True, blank=True, verbose_name="Past E.M.H. services")

    # Providers section
    psych = models.BooleanField(default=False, verbose_name="Psychiatrist")
    psych_name = models.TextField(null=True, blank=True, verbose_name="Provider name")
    psych_clinic = models.TextField(null=True, blank=True, verbose_name="Clinic/facility")
    psych_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    psych_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    psych_note = models.TextField(null=True, blank=True, verbose_name="Notes")

    primary_provider = models.BooleanField(default=False, verbose_name="Primary Physician/NP/PA")
    primary_name = models.TextField(null=True, blank=True, verbose_name="Provider name")
    primary_clinic = models.TextField(null=True, blank=True, verbose_name="Clinic/facility")
    primary_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    primary_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    primary_note = models.TextField(null=True, blank=True, verbose_name="Notes")

    pharm = models.BooleanField(default=False, verbose_name="Pharmacy")
    pharm_name = models.TextField(null=True, blank=True, verbose_name="Provider name")
    pharm_clinic = models.TextField(null=True, blank=True, verbose_name="Clinic/facility")
    pharm_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    pharm_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    pharm_note = models.TextField(null=True, blank=True, verbose_name="Notes")

    therapist = models.BooleanField(default=False, verbose_name="Therapist")
    therapist_name = models.TextField(null=True, blank=True, verbose_name="Provider name")
    therapist_clinic = models.TextField(null=True, blank=True, verbose_name="Clinic/facility")
    therapist_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    therapist_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    therapist_note = models.TextField(null=True, blank=True, verbose_name="Notes")


    other_provider = models.BooleanField(default=False)
    other_name = models.TextField(null=True, blank=True, verbose_name="Provider name")
    other_clinic = models.TextField(null=True, blank=True, verbose_name="Clinic/facility")
    other_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    other_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    other_note = models.TextField(null=True, blank=True, verbose_name="Notes")

    # Legal Status
    ch51_comit = models.BooleanField(default=False, verbose_name="Chapter 51 commitment")
    ch51_comit_expire = models.DateField(null=True, blank=True, verbose_name="Expiration date")
    ch51_med_order = models.BooleanField(default=False, verbose_name="Med order")
    ch51_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    ch51_name = models.TextField(null=True, blank=True, verbose_name="Name")
    ch51_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    ch51_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    pp = models.BooleanField(default=False, verbose_name="Protective placement")
    pp_name = models.TextField(null=True, blank=True, verbose_name="Contact name")
    pp_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    pp_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    pp_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    guardianship = models.BooleanField(default=False, verbose_name="Guardianship")
    guardian_name = models.TextField(null=True, blank=True, verbose_name="Guardian name")
    guardian_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    guardian_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    guardian_address = models.TextField(null=True, blank=True, verbose_name="Address")

    probation = models.BooleanField(default=False, verbose_name="Probation/parole")
    prob_agent = models.TextField(null=True, blank=True, verbose_name="Agent name")
    prob_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    prob_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    prob_address = models.TextField(null=True, blank=True, verbose_name="Address")


    # Other program affiliation section
    ccs = models.BooleanField(default=False, verbose_name="CCS")
    ccs_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    ccs_name = models.TextField(null=True, blank=True, verbose_name="Name")
    ccs_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    ccs_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    csp = models.BooleanField(default=False, verbose_name="CSP")
    csp_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    csp_name = models.TextField(null=True, blank=True, verbose_name="Name")
    csp_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    csp_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    cst = models.BooleanField(default=False, verbose_name="CST")
    cst_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    cst_name = models.TextField(null=True, blank=True, verbose_name="Name")
    cst_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    cst_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    clts = models.BooleanField(default=False, verbose_name="CLTS")
    clts_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    clts_name = models.TextField(null=True, blank=True, verbose_name="Name")
    clts_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    clts_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    fc = models.BooleanField(default=False, verbose_name="Family care")
    fc_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    fc_name = models.TextField(null=True, blank=True, verbose_name="Name")
    fc_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    fc_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    iris = models.BooleanField(default=False, verbose_name="IRIS")
    iris_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    iris_name = models.TextField(null=True, blank=True, verbose_name="Name")
    iris_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    iris_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    cps = models.BooleanField(default=False, verbose_name="CPS")
    cps_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    cps_name = models.TextField(null=True, blank=True, verbose_name="Name")
    cps_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    cps_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    jj = models.BooleanField(default=False, verbose_name="Juvenile justice")
    jj_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    jj_name = models.TextField(null=True, blank=True, verbose_name="Name")
    jj_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    jj_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    aps = models.BooleanField(default=False, verbose_name="APS")
    aps_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    aps_name = models.TextField(null=True, blank=True, verbose_name="Name")
    aps_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    aps_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    adrc = models.BooleanField(default=False, verbose_name="ADRC")
    adrc_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    adrc_name = models.TextField(null=True, blank=True, verbose_name="Name")
    adrc_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    adrc_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    other = models.BooleanField(default=False, verbose_name="Other")
    other_pa_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    other_pa_name = models.TextField(null=True, blank=True, verbose_name="Name")
    other_pa_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    other_pa_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")



    def save(self, *args, **kwargs):
        if not self.id:
            self.agency_name = utils.get_app_config("agency_name")
            self.agency_address = "{}, {} {} {}".format(
                utils.get_app_config("agency_street"),
                utils.get_app_config("agency_city"),
                utils.get_app_config("agency_state"),
                utils.get_app_config("agency_zip")
            )
            self.phone_contact = utils.get_app_config("agency_phone")

        super().save(*args, **kwargs)









class EmrCrisisAlertForm(EmrForm):
    ls1_choices = (
        ("51_20", "Commitment (51.20)"),
        ("settlement", "Settlement agreement"),
    )

    ls2_choices = (
        ("guardianship", "Guardianship"),
        ("activated_poa", "Activated POA"),
    )

    verbal_choices = (
        (None, "----------"),
        ("verbal", "Verbal"),
        ("non_verbal", "Non-verbal"),
    )

    alert_end_date = models.DateField(null=True, blank=True, verbose_name="Alert end date (not to exceed 30 days)")
    county_of_responsibility = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.WI_COUNTY)
    county_of_placement = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.WI_COUNTY)
    plan_developer = models.TextField(null=True, blank=True, )
    agency_name = models.TextField(null=True, blank=True, )
    agency_address = models.TextField(null=True, blank=True, )
    phone_contact = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone/contact")
    phone_contact_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    # Known cautions
    caution_firearms = models.BooleanField(default=False, verbose_name="Firearms")
    caution_firearms_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    caution_animals = models.BooleanField(default=False, verbose_name="Animals")
    caution_animals_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    caution_other = models.BooleanField(default=False, verbose_name="Other")
    caution_other_dtl = models.TextField(null=True, blank=True, verbose_name="Details")

    # Communication
    verbal = models.TextField(
        null=True, blank=True, choices=verbal_choices, verbose_name="Verbal/non-verbal")
    other_com_preferences = models.TextField(null=True, blank=True, verbose_name="Other communication preferences")


    # Crisis alert information
    reason_for_alert = models.TextField(null=True, blank=True)
    established_plan = models.TextField(null=True, blank=True, verbose_name="Established plan to deal with crisis")
    living_situation = models.TextField(null=True, blank=True)


    # Critical support contacts
    contact_name = models.TextField(null=True, blank=True, verbose_name="Name")
    contact_relation = models.TextField(null=True, blank=True, verbose_name="Agency/relation")
    contact_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_address = models.TextField(null=True, blank=True, verbose_name="Address")
    contact_avil = models.CharField(max_length=255, null=True, blank=True, verbose_name="Availability")
    contact_name2 = models.TextField(null=True, blank=True, verbose_name="Name")
    contact_relation2 = models.TextField(null=True, blank=True, verbose_name="Agency/relation")
    contact_phone2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_address2 = models.TextField(null=True, blank=True, verbose_name="Address")
    contact_avil2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Availability")
    contact_name3 = models.TextField(null=True, blank=True, verbose_name="Name")
    contact_relation3 = models.TextField(null=True, blank=True, verbose_name="Agency/relation")
    contact_phone3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_address3 = models.TextField(null=True, blank=True, verbose_name="Address")
    contact_avil3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Availability")
    contact_name4 = models.TextField(null=True, blank=True, verbose_name="Name")
    contact_relation4 = models.TextField(null=True, blank=True, verbose_name="Agency/relation")
    contact_phone4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_address4 = models.TextField(null=True, blank=True, verbose_name="Address")
    contact_avil4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Availability")
    contact_name5 = models.TextField(null=True, blank=True, verbose_name="Name")
    contact_relation5 = models.TextField(null=True, blank=True, verbose_name="Agency/relation")
    contact_phone5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_address5 = models.TextField(null=True, blank=True, verbose_name="Address")
    contact_avil5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Availability")

    # Mental Health and Physical Health
    mh_diag_needs = models.TextField(null=True, blank=True, verbose_name="Mental health diagnoses and needs (self-report or known)")
    phys_med_needs = models.TextField(null=True, blank=True, verbose_name="Known physical or medical diagnoses and/or needs")
    medications = models.TextField(null=True, blank=True, verbose_name="List of prescribed psychotropic & medical medications (including PRNs)")

    # Past Emergency Mental Health Services
    past_emh_services = models.TextField(null=True, blank=True, verbose_name="Summary of past emergency mental health services")

    # Legal Status
    ch51_comit = models.BooleanField(default=False, verbose_name="Chapter 51 commitment")
    ch51_comit_expire = models.DateField(null=True, blank=True, verbose_name="Expiration date")
    ch51_med_order = models.BooleanField(default=False, verbose_name="Med order")
    ch51_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    ch51_name = models.TextField(null=True, blank=True, verbose_name="Name")
    ch51_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    ch51_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    ch55_pp = models.BooleanField(default=False, verbose_name="Chapter 55 protective placement")
    ch55_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    ch55_name = models.TextField(null=True, blank=True, verbose_name="Name")
    ch55_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    ch55_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    guardianship = models.BooleanField(default=False, verbose_name="Guardianship")
    guardian_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    guardian_name = models.TextField(null=True, blank=True, verbose_name="Name")
    guardian_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    guardian_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    probation = models.BooleanField(default=False, verbose_name="Probation/parole")
    probation_agent = models.CharField(max_length=255, null=True, blank=True, verbose_name="Agent name")
    probation_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    probation_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")


    # Other program affiliation section
    ccs = models.BooleanField(default=False, verbose_name="CCS")
    ccs_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    ccs_name = models.TextField(null=True, blank=True, verbose_name="Name")
    ccs_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    ccs_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    csp = models.BooleanField(default=False, verbose_name="CSP")
    csp_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    csp_name = models.TextField(null=True, blank=True, verbose_name="Name")
    csp_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    csp_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    cst = models.BooleanField(default=False, verbose_name="CST")
    cst_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    cst_name = models.TextField(null=True, blank=True, verbose_name="Name")
    cst_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    cst_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    clts = models.BooleanField(default=False, verbose_name="CLTS")
    clts_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    clts_name = models.TextField(null=True, blank=True, verbose_name="Name")
    clts_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    clts_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    fc = models.BooleanField(default=False, verbose_name="Family care")
    fc_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    fc_name = models.TextField(null=True, blank=True, verbose_name="Name")
    fc_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    fc_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    iris = models.BooleanField(default=False, verbose_name="IRIS")
    iris_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    iris_name = models.TextField(null=True, blank=True, verbose_name="Name")
    iris_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    iris_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    cps = models.BooleanField(default=False, verbose_name="CPS")
    cps_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    cps_name = models.TextField(null=True, blank=True, verbose_name="Name")
    cps_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    cps_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    jj = models.BooleanField(default=False, verbose_name="Juvenile justice")
    jj_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    jj_name = models.TextField(null=True, blank=True, verbose_name="Name")
    jj_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    jj_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    aps = models.BooleanField(default=False, verbose_name="APS")
    aps_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    aps_name = models.TextField(null=True, blank=True, verbose_name="Name")
    aps_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    aps_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    adrc = models.BooleanField(default=False, verbose_name="ADRC")
    adrc_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    adrc_name = models.TextField(null=True, blank=True, verbose_name="Name")
    adrc_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    adrc_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    other_pa = models.BooleanField(default=False, verbose_name="Other")
    other_pa_agency = models.TextField(null=True, blank=True, verbose_name="Agency")
    other_pa_name = models.TextField(null=True, blank=True, verbose_name="Name")
    other_pa_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    other_pa_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")

    # Additional Documentation
    supporting_docs = models.TextField(null=True, blank=True, verbose_name="Supporting documents to reference")



    def save(self, *args, **kwargs):
        if not self.id:
            self.agency_name = utils.get_app_config("agency_name")
            self.agency_address = "{}, {} {} {}".format(
                utils.get_app_config("agency_street"),
                utils.get_app_config("agency_city"),
                utils.get_app_config("agency_state"),
                utils.get_app_config("agency_zip")
            )
            self.phone_contact = utils.get_app_config("agency_phone")

        super().save(*args, **kwargs)









class EmrCstPpsMhAdmissionForm(EmrForm):
    agency_of_resp = models.CharField(max_length=255, null=True, blank=True, verbose_name="Agency of responsibility")
    commit_stat = models.CharField(verbose_name="Commitment status", max_length=2, null=True, blank=True, choices=PPS_C.LEGAL_STAT)

    # mental health services
    provider_wpi_npi = models.CharField(max_length=255, null=True, blank=True, verbose_name="Provider WPI/NPI")

    # Consumer status report
    commit_stat = models.CharField(verbose_name="Commitment status", max_length=2, null=True, blank=True, choices=PPS_C.LEGAL_STAT)
    employ_stat = models.CharField(max_length=2, choices=PPS_C.EMPLOY_STAT, null=True, blank=True, verbose_name="Employment status")
    living_stat = models.CharField(max_length=2, choices=PPS_C.LIVING_STAT, null=True, blank=True, verbose_name="Living arrangement")
    # Subtitle Criminal Justice System Involvement (select up to four within the last six months)
    cjs_none = models.BooleanField(default=False, verbose_name="None")
    cjs_probation = models.BooleanField(default=False, verbose_name="On probation")
    cjs_arrests = models.BooleanField(default=False, verbose_name="Arrests")
    cjs_jail = models.BooleanField(default=False, verbose_name="Jailed/Imprisoned")
    cjs_parole = models.BooleanField(default=False, verbose_name="On parole")
    cjs_juv = models.BooleanField(default=False, verbose_name="Juvenile justice system contact")
    cjs_unknown = models.BooleanField(default=False, verbose_name="Unknown")

    num_arrests = models.IntegerField(null=True, blank=True, verbose_name="Number of arrests in the past 30 days")
    num_arrests_sixmonths = models.IntegerField(null=True, blank=True, verbose_name="Number of arrests in the past 6 months")


    def get_cjs_involvement_html(self):
        items = []
        if self.cjs_none:
            items.append("None")
        if self.cjs_probation:
            items.append("On probation")
        if self.cjs_arrests:
            items.append("Arrests")
        if self.cjs_jail:
            items.append("Jailed/imprisoned")
        if self.cjs_parole:
            items.append("On parole")
        if self.cjs_juv:
            items.append("Juvenile justice system contact")
        if self.cjs_unknown:
            items.append("Unknown")

        if items:
            return "<br>".join(items)



class EmrCstPpsMhDischargeForm(EmrForm):
    commit_stat = models.CharField(verbose_name="Commitment status", max_length=2, null=True, blank=True, choices=PPS_C.LEGAL_STAT)
    employ_stat = models.CharField(max_length=2, choices=PPS_C.EMPLOY_STAT, null=True, blank=True, verbose_name="Employment status")
    living_stat = models.CharField(max_length=2, choices=PPS_C.LIVING_STAT, null=True, blank=True, verbose_name="Living arrangement")

    # Subtitle Criminal Justice System Involvement (select up to four within the last six months)
    cjs_none = models.BooleanField(default=False, verbose_name="None")
    cjs_probation = models.BooleanField(default=False, verbose_name="On probation")
    cjs_arrests = models.BooleanField(default=False, verbose_name="Arrests")
    cjs_jail = models.BooleanField(default=False, verbose_name="Jailed/imprisoned")
    cjs_parole = models.BooleanField(default=False, verbose_name="On parole")
    cjs_juv = models.BooleanField(default=False, verbose_name="Juvenile justice system contact")
    cjs_unknown = models.BooleanField(default=False, verbose_name="Unknown")
    num_arrests = models.IntegerField(null=True, blank=True, verbose_name="Number of arrests in the past 30 days")
    num_arrests_sixmonths = models.IntegerField(null=True, blank=True, verbose_name="Number of arrests in the past 6 months")
    # ADDITIONAL DATA FROM COORDINATED SERVICE TEAMS (CST) (collected throughout a child’s CST enrollment)
    pps_mh_spc_end_reason = models.CharField(null=True, blank=True, max_length=2, choices=PPS_C.MH_SPC_END_REASON, verbose_name="SPC end reason")
    # Subtitle - CST Living Arrangements
    # Instructions: Record all living arrangements while the child is enrolled in the CST.
    # No gaps in dates should exist between the end of one living arrangement and the
    # start of another
    end_date = models.DateField(null=True, blank=True, verbose_name="End date")



    def get_cjs_involvement_html(self):
        items = []
        if self.cjs_none:
            items.append("none")
        if self.cjs_probation:
            items.append("On probation")
        if self.cjs_arrests:
            items.append("Arrests")
        if self.cjs_jail:
            items.append("Jailed/imprisoned")
        if self.cjs_parole:
            items.append("On parole")
        if self.cjs_juv:
            items.append("Juvenile justice system contact")
        if self.cjs_unknown:
            items.append("Unknown")

        if items:
            return ", ".join(items)






class EmrRandiExternalReferralForm(EmrForm):
    ref_source_choices = (
        ("agency", "Agency"),
        ("self", "Self"),
        ("parent", "Parent/guardian"),
        ("other", "Other"),
    )

    ref_source = models.CharField(max_length=255, null=True, blank=True, verbose_name="Referral source", choices=ref_source_choices)
    ref_agency_name = models.TextField(null=True, blank=True, verbose_name="Agency name")
    ref_name = models.TextField(null=True, blank=True, verbose_name="Person referring")
    ref_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Referrer phone")
    ref_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    ref_email = models.CharField(max_length=255, null=True, blank=True, verbose_name="Referrer email")
    ref_other_name = models.TextField(null=True, blank=True, verbose_name="Name")
    ref_other_relation = models.TextField(null=True, blank=True, verbose_name="Relationship to client")
    legal_status = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.RANDI_LEGAL_STAT_CHOICES)
    guardian_name = models.TextField(null=True, blank=True, verbose_name="Guardian/POA/parent name")
    guardian_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Guardian/POA/parent phone")
    guardian_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    ref_for_out = models.BooleanField(default=False, verbose_name="Referral for Outpatient Counseling Only")
    ref_for_out_mh = models.BooleanField(default=False, verbose_name="Mental health")
    ref_for_out_su = models.BooleanField(default=False, verbose_name="Substance use")
    ref_for_other = models.BooleanField(default=False, verbose_name="Referral for other program services")
    ref_reason = models.TextField(null=True, blank=True, verbose_name="Reason for referral and expected outcomes")



    def get_requested_services(self):
        services = []

        # Outpatient counseling is excluded by default
        #if self.ref_for_out:
        #    services.append("Outpatient counseling")

        if self.ref_for_out_mh:
            services.append("Mental health")
        if self.ref_for_out_su:
            services.append("Substance use")
        if self.ref_for_other:
            services.append("Other program services")

        return services



    def get_requested_services_html(self):
        return "<br>".join(self.get_requested_services())



class EmrRandiReferralForm(EmrForm):
    # This is the internal referral
    ref_worker = models.TextField(null=True, blank=True, verbose_name="Referring worker")
    legal_status = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.RANDI_LEGAL_STAT_CHOICES)
    guardian_name = models.TextField(null=True, blank=True, verbose_name="Guardian/POA/parent name")
    guardian_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Guardian/POA/parent phone")
    guardian_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    refto_op_bh = models.BooleanField(default=False, verbose_name="Outpatient behavioral health clinic")
    refto_mh = models.BooleanField(default=False, verbose_name="Mental health")
    refto_sa = models.BooleanField(default=False, verbose_name="Substance use")
    refto_ccs = models.BooleanField(default=False, verbose_name="CCS")
    refto_csp = models.BooleanField(default=False, verbose_name="CSP")
    refto_clts = models.BooleanField(default=False, verbose_name="CLTS")
    refto_clts_preadmit = models.BooleanField(default=False, verbose_name="CLTS Pre-admit")
    refto_cst = models.BooleanField(default=False, verbose_name="CST")
    refto_crisis = models.BooleanField(default=False, verbose_name="Crisis")
    refto_res_aoda = models.BooleanField(default=False, verbose_name="Residential AODA treatment")
    expected_outcomes = models.TextField(null=True, blank=True, verbose_name="Expected outcomes of service")


    def get_requested_services(self):
        services = []
        if self.refto_op_bh:
            services.append("Outpatient BH clinic")
        if self.refto_mh:
            services.append("Mental health")
        if self.refto_sa:
            services.append("Substance use")
        if self.refto_ccs:
            services.append("CCS")
        if self.refto_csp:
            services.append("CSP")
        if self.refto_clts:
            services.append("CLTS")
        if self.refto_cst:
            services.append("CST")
        if self.refto_crisis:
            services.append("Crisis")
        if self.refto_res_aoda:
            services.append("Residential AODA treatment")

        return services


    def get_requested_services_html(self):
        return "<br>".join(self.get_requested_services())




class EmrRandiIntakeForm(EmrForm):
    # TODO make a cool function to create a client off of this form!    
    choices_info_from = (
        ("client", "Client"),
        ("parent", "Parent"),
        ("guardian", "Legal Guardian/POC-HC"),
        ("family", "Family member"),
        ("provider", "Provider"),
        ("other", "Other"),
    )


    living_situation_choices = (
        ("rent", "Renting"),
        ("own", "Owns own home"),
        ("with_others", "Living with others"),
        ("homeless", "Homeless"),
        ("other", "Other")
    )

    pregnant_status = (
        ("pregnant", "Pregnant"),
        ("not_pregnant", "Not pregnant"),
        ("na", "Not applicable"),
    )

    info_from = models.CharField(max_length=255, choices=choices_info_from, verbose_name="Information from", null=True, blank=True)
    contact_name = models.TextField(null=True, blank=True, verbose_name="Contact name")
    contact_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Contact phone")
    contact_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_notes = models.TextField(null=True, blank=True, verbose_name="Notes")

    info_from2 = models.CharField(max_length=255, choices=choices_info_from, verbose_name="Information from", null=True, blank=True)
    contact_name2 = models.TextField(null=True, blank=True, verbose_name="Contact name")
    contact_phone2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Contact phone")
    contact_phone_ext2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_notes2 = models.TextField(null=True, blank=True, verbose_name="Notes")

    info_from3 = models.CharField(max_length=255, choices=choices_info_from, verbose_name="Information from", null=True, blank=True)
    contact_name3 = models.TextField(null=True, blank=True, verbose_name="Contact name")
    contact_phone3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Contact phone")
    contact_phone_ext3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_notes3 = models.TextField(null=True, blank=True, verbose_name="Notes")

    info_from4 = models.CharField(max_length=255, choices=choices_info_from, verbose_name="Information from", null=True, blank=True)
    contact_name4 = models.TextField(null=True, blank=True, verbose_name="Contact name")
    contact_phone4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Contact phone")
    contact_phone_ext4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_notes4 = models.TextField(null=True, blank=True, verbose_name="Notes")

    info_from5 = models.CharField(max_length=255, choices=choices_info_from, verbose_name="Information from", null=True, blank=True)
    contact_name5 = models.TextField(null=True, blank=True, verbose_name="Contact name")
    contact_phone5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Contact phone")
    contact_phone_ext5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_notes5 = models.TextField(null=True, blank=True, verbose_name="Notes")


    # Diagnosis information
    diagnosis1 = models.TextField(null=True, blank=True, verbose_name="Diagnosis")
    age_of_onset1 = models.IntegerField(null=True, blank=True, verbose_name="Age of onset")
    source1 = models.TextField(null=True, blank=True, verbose_name="Source")

    diagnosis2 = models.TextField(null=True, blank=True, verbose_name="Diagnosis")
    age_of_onset2 = models.IntegerField(null=True, blank=True, verbose_name="Age of onset")
    source2 = models.TextField(null=True, blank=True, verbose_name="Source")

    diagnosis3 = models.TextField(null=True, blank=True, verbose_name="Diagnosis")
    age_of_onset3 = models.IntegerField(null=True, blank=True, verbose_name="Age of onset")
    source3 = models.TextField(null=True, blank=True, verbose_name="Source")

    diagnosis4 = models.TextField(null=True, blank=True, verbose_name="Diagnosis")
    age_of_onset4 = models.IntegerField(null=True, blank=True, verbose_name="Age of onset")
    source4 = models.TextField(null=True, blank=True, verbose_name="Source")

    diagnosis5 = models.TextField(null=True, blank=True, verbose_name="Diagnosis")
    age_of_onset5 = models.IntegerField(null=True, blank=True, verbose_name="Age of onset")
    source5 = models.TextField(null=True, blank=True, verbose_name="Source")

    diagnosis6 = models.TextField(null=True, blank=True, verbose_name="Diagnosis")
    age_of_onset6 = models.IntegerField(null=True, blank=True, verbose_name="Age of onset")
    source6 = models.TextField(null=True, blank=True, verbose_name="Source")

    diagnosis7 = models.TextField(null=True, blank=True, verbose_name="Diagnosis")
    age_of_onset7 = models.IntegerField(null=True, blank=True, verbose_name="Age of onset")
    source7 = models.TextField(null=True, blank=True, verbose_name="Source")

    diagnosis8 = models.TextField(null=True, blank=True, verbose_name="Diagnosis")
    age_of_onset8 = models.IntegerField(null=True, blank=True, verbose_name="Age of onset")
    source8 = models.TextField(null=True, blank=True, verbose_name="Source")

    diagnosis9 = models.ForeignKey(Diagnosis, null=True, blank=True, verbose_name="Diagnosis", on_delete=models.PROTECT, related_name="intake_diagnosis9")
    age_of_onset9 = models.IntegerField(null=True, blank=True, verbose_name="Age of onset")
    source9 = models.TextField(null=True, blank=True, verbose_name="Source")

    diagnosis10 = models.TextField(null=True, blank=True, verbose_name="Diagnosis")
    age_of_onset10 = models.IntegerField(null=True, blank=True, verbose_name="Age of onset")
    source10 = models.TextField(null=True, blank=True, verbose_name="Source")

    diagnosis11 = models.TextField(null=True, blank=True, verbose_name="Diagnosis")
    age_of_onset11 = models.IntegerField(null=True, blank=True, verbose_name="Age of onset")
    source11 = models.TextField(null=True, blank=True, verbose_name="Source")

    diagnosis12 = models.TextField(null=True, blank=True, verbose_name="Diagnosis")
    age_of_onset12 = models.IntegerField(null=True, blank=True, verbose_name="Age of onset")
    source12 = models.TextField(null=True, blank=True, verbose_name="Source")

    diag_adtl = models.TextField(null=True, blank=True, verbose_name="Additional information")


    # current or past treatments
    treat_ccs = models.BooleanField(default=False, verbose_name="CCS")
    treat_ccs_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    treat_ch51 = models.BooleanField(default=False, verbose_name="Chapter 51 Commitment/Settlement Agreement")
    treat_ch51_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    treat_csp = models.BooleanField(default=False, verbose_name="CSP")
    treat_csp_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    treat_cst = models.BooleanField(default=False, verbose_name="CST")
    treat_cst_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    treat_hospital_in = models.BooleanField(default=False, verbose_name="Inpatient Hospitalization")
    treat_hospital_in_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    treat_mh_out = models.BooleanField(default=False, verbose_name="Mental Health Outpatient Counseling")
    treat_mh_out_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    treat_mh_res = models.BooleanField(default=False, verbose_name="Mental Health Residential")
    treat_mh_res_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    treat_psych = models.BooleanField(default=False, verbose_name="Psychiatry")
    treat_psych_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    treat_sa_detox = models.BooleanField(default=False, verbose_name="Substance Use Detox")
    treat_sa_detox_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    treat_sa_out = models.BooleanField(default=False, verbose_name="Substance Use Outpatient Counseling")
    treat_sa_out_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    treat_sa_res = models.BooleanField(default=False, verbose_name="Substance Use Residential")
    treat_sa_res_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    treat_current_provider = models.BooleanField(default=False, verbose_name="Current Medical Provider")
    treat_current_provider_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    treat_medication = models.BooleanField(default=False, verbose_name="Medication")
    treat_medication_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    treat_other = models.BooleanField(default=False, verbose_name="Other")
    treat_other_dtl = models.TextField(null=True, blank=True, verbose_name="Details")    
    treat_adtl_info = models.TextField(null=True, blank=True, verbose_name="Additional information")


    # Substance Use section
    sa_none_reported = models.BooleanField(default=False, verbose_name="No substance use concerns reported")
    sa_alcohol = models.BooleanField(default=False, verbose_name="Alcohol")
    sa_alcohol_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")
    sa_benzos = models.BooleanField(default=False, verbose_name="Benzodiazepines (Xanax, Valium, etc.)")
    sa_benzos_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")
    sa_cocaine = models.BooleanField(default=False, verbose_name="Cocaine")
    sa_cocaine_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")
    sa_barb = models.BooleanField(default=False, verbose_name="Barbiturates")
    sa_barb_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")
    sa_crack = models.BooleanField(default=False, verbose_name="Crack")
    sa_crack_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")
    sa_hallucinogen = models.BooleanField(default=False, verbose_name="Hallucinogens (LSD, Mushrooms, PCP, etc.)")
    sa_hallucinogen_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")
    sa_opiate = models.BooleanField(default=False, verbose_name="Opiates (Heroin, Opium, Methadone)")
    sa_opiate_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")
    sa_inhale = models.BooleanField(default=False, verbose_name="Inhalants")
    sa_inhale_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")
    sa_marijuana = models.BooleanField(default=False, verbose_name="Marijuana")
    sa_marijuana_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")
    sa_amphetamine = models.BooleanField(default=False, verbose_name="Amphetamines (Speed, Crank, Ecstasy, etc.)")
    sa_amphetamine_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")
    sa_meth = models.BooleanField(default=False, verbose_name="Methamphetamine")
    sa_meth_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")
    sa_script = models.BooleanField(default=False, verbose_name="Prescription Medicine (Wellbutrin, Sleep Aids, etc.)")
    sa_script_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")
    sa_iv = models.BooleanField(default=False, verbose_name="IV drug use")
    sa_iv_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")
    sa_tobacco = models.BooleanField(default=False, verbose_name="Tobacco/Nicotine (Cigarettes, Chew, Vape, etc.)")
    sa_tobacco_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")
    sa_caffeine = models.BooleanField(default=False, verbose_name="Caffeine (Energy Drinks, Sodas, Coffee, etc.)")
    sa_caffeine_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")
    sa_other = models.BooleanField(default=False, verbose_name="Other")
    sa_other_dtl = models.TextField(null=True, blank=True, verbose_name="Details (First use, current use, frequency, amount, etc.)")

    sa_withdrawal = models.TextField(null=True, blank=True, verbose_name="Withdrawal History/Current Withdrawal Concerns")
    sa_adtl_info = models.TextField(null=True, blank=True, verbose_name="Additional information")


    # Mental Health
    mh_aggression = models.BooleanField(default=False, verbose_name="Aggression")
    mh_aggression_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_agitation = models.BooleanField(default=False, verbose_name="Agitation")
    mh_agitation_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_anxiety = models.BooleanField(default=False, verbose_name="Anxiety")
    mh_anxiety_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_chronic_pain = models.BooleanField(default=False, verbose_name="Chronic Pain")
    mh_chronic_pain_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_delusions = models.BooleanField(default=False, verbose_name="Delusions")
    mh_delusions_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_depressed = models.BooleanField(default=False, verbose_name="Depressed Mood")
    mh_depressed_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_concentration = models.BooleanField(default=False, verbose_name="Difficulty With Concentration")
    mh_concentration_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_eating_pattern = models.BooleanField(default=False, verbose_name="Disordered Eating Pattern")
    mh_eating_pattern_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_dissociation = models.BooleanField(default=False, verbose_name="Dissociation")
    mh_dissociation_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_body_image = models.BooleanField(default=False, verbose_name="Distorted Body Image")
    mh_body_image_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_elevated_mood = models.BooleanField(default=False, verbose_name="Elevated Mood")
    mh_elevated_mood_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_fatigue = models.BooleanField(default=False, verbose_name="Fatigued/Low Energy")
    mh_fatigue_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_guilt = models.BooleanField(default=False, verbose_name="Feelings of Guilt/Shame")
    mh_guilt_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_helpless = models.BooleanField(default=False, verbose_name="Feelings of Helplessness")
    mh_helpless_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_hopeless = models.BooleanField(default=False, verbose_name="Feelings of Hopelessness")
    mh_hopeless_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_lonely = models.BooleanField(default=False, verbose_name="Feelings of Loneliness")
    mh_lonely_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_worthless = models.BooleanField(default=False, verbose_name="Feelings of Worthlessness")
    mh_worthless_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_gender_id = models.BooleanField(default=False, verbose_name="Gender Identity Issues")
    mh_gender_id_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_hallucinations = models.BooleanField(default=False, verbose_name="Hallucinations (Auditory, Visual, Etc.)")
    mh_hallucinations_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_homicide = models.BooleanField(default=False, verbose_name="Homicidal Ideations")
    mh_homicide_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_hyper = models.BooleanField(default=False, verbose_name="Hyperactivity")
    mh_hyper_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_hypersex = models.BooleanField(default=False, verbose_name="Hypersexuality")
    mh_hypersex_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_impulse = models.BooleanField(default=False, verbose_name="Impulsivity")
    mh_impulse_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_inattention = models.BooleanField(default=False, verbose_name="Inattention")
    mh_inattention_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_irritable = models.BooleanField(default=False, verbose_name="Irritability")
    mh_irritable_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_loss_interest = models.BooleanField(default=False, verbose_name="Loss of Interest")
    mh_loss_interest_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_low_esteem = models.BooleanField(default=False, verbose_name="Low Self-Esteem")
    mh_low_esteem_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_malingering = models.BooleanField(default=False, verbose_name="Malingering")
    mh_malingering_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_mania = models.BooleanField(default=False, verbose_name="Mania")
    mh_mania_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_memory = models.BooleanField(default=False, verbose_name="Memory Difficulties")
    mh_memory_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_mood = models.BooleanField(default=False, verbose_name="Mood Fluctuations/Lability")
    mh_mood_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_nightmares = models.BooleanField(default=False, verbose_name="Nightmares")
    mh_nightmares_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_self_injury = models.BooleanField(default=False, verbose_name="Non-Suicidal Self Injury")
    mh_self_injury_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_obsessive = models.BooleanField(default=False, verbose_name="Obsessive Thoughts")
    mh_obsessive_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_other_substance = models.BooleanField(default=False, verbose_name="Other Substance Use")
    mh_other_substance_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_occupational = models.BooleanField(default=False, verbose_name="Occupational Problems")
    mh_occupational_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_paranoia = models.BooleanField(default=False, verbose_name="Paranoia")
    mh_paranoia_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_hygene = models.BooleanField(default=False, verbose_name="Poor Hygiene")
    mh_hygene_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_restless = models.BooleanField(default=False, verbose_name="Restlessness")
    mh_restless_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_risky = models.BooleanField(default=False, verbose_name="Risky/Dangerous Behavior")
    mh_risky_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_school_work = models.BooleanField(default=False, verbose_name="School/Work Difficulties")
    mh_school_work_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_sexual_problems = models.BooleanField(default=False, verbose_name="Sexual Problems")
    mh_sexual_problems_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_sleep = models.BooleanField(default=False, verbose_name="Sleep Issues")
    mh_sleep_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_fears = models.BooleanField(default=False, verbose_name="Specific Fears")
    mh_fears_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_suicide = models.BooleanField(default=False, verbose_name="Suicidal Ideations")
    mh_suicide_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_thought_disorg = models.BooleanField(default=False, verbose_name="Thought Disorganization")
    mh_thought_disorg_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_thought_block = models.BooleanField(default=False, verbose_name="Thought Blocking")
    mh_thought_block_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_weight_change = models.BooleanField(default=False, verbose_name="Weight Changes")
    mh_weight_change_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    mh_other = models.BooleanField(default=False, verbose_name="Other")
    mh_other_dtl = models.TextField(null=True, blank=True, verbose_name="Details")


    mh_adtl_info = models.TextField(null=True, blank=True, verbose_name="Additional Information")

    # perceived needs of client according to client or client's contacts
    pn_behavior_support = models.BooleanField(default=False, verbose_name="Behavior Support")
    pn_behavior_support_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    pn_coping_skill = models.BooleanField(default=False, verbose_name="Coping Skill Development")
    pn_coping_skill_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    pn_employment = models.BooleanField(default=False, verbose_name="Employment Skills Training")
    pn_employment_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    pn_community = models.BooleanField(default=False, verbose_name="Information Regarding Community Resources")
    pn_community_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    pn_life_skills = models.BooleanField(default=False, verbose_name="Life Skills Training")
    pn_life_skills_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    pn_med_monitor = models.BooleanField(default=False, verbose_name="Medication Monitoring/Assistance")
    pn_med_monitor_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    pn_mh_counseling = models.BooleanField(default=False, verbose_name="Mental Health- Counseling")
    pn_mh_counseling_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    pn_personal_care = models.BooleanField(default=False, verbose_name="Personal Care")
    pn_personal_care_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    pn_prescribe = models.BooleanField(default=False, verbose_name="Prescribing Services")
    pn_prescribe_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    pn_psy_edu = models.BooleanField(default=False, verbose_name="Psychoeducation")
    pn_psy_edu_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    pn_sub_use_counsel = models.BooleanField(default=False, verbose_name="Substance Use Counseling")
    pn_sub_use_counsel_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    pn_sub_use_res = models.BooleanField(default=False, verbose_name="Substance Use Residential")
    pn_sub_use_res_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    pn_transport = models.BooleanField(default=False, verbose_name="Transportation to Appointments")
    pn_transport_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    pn_other = models.BooleanField(default=False, verbose_name="Other")
    pn_other_dtl = models.TextField(null=True, blank=True, verbose_name="Details")

    pn_adtl_info = models.TextField(null=True, blank=True, verbose_name="Additional information")

    # Pregnancy status
    pregnant = models.CharField(max_length=255, null=True, blank=True, choices=pregnant_status)
    pregnant_due = models.DateField(null=True, blank=True, verbose_name="Due date")
    pregnant_adtl_info = models.TextField(null=True, blank=True, verbose_name="Additional information")

    # Living situation
    living_situation = models.CharField(max_length=255, choices=living_situation_choices, null=True, blank=True)
    ls_adtl_info = models.TextField(null=True, blank=True, verbose_name="Additional information")

    # Other program affiliation
    op_probation = models.BooleanField(default=False, verbose_name="Probation/Parole")
    op_probation_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    op_candf = models.BooleanField(default=False, verbose_name="Children & Families")
    op_candf_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    op_public_health = models.BooleanField(default=False, verbose_name="Public Health (WIC, PNCC)")
    op_public_health_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    op_aging = models.BooleanField(default=False, verbose_name="Aging & Disability Services")
    op_aging_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    op_treat_court = models.BooleanField(default=False, verbose_name="Treatment Court")
    op_treat_court_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    op_school_district = models.BooleanField(default=False, verbose_name="School District")
    op_school_district_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    op_day_treat = models.BooleanField(default=False, verbose_name="Day Treatment")
    op_day_treat_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    op_defer_explu = models.BooleanField(default=False, verbose_name="Deferred Expulsion Plan")
    op_defer_explu_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    op_managed_care = models.BooleanField(default=False, verbose_name="Managed Care (Inclusa, IRIS, etc.)")
    op_managed_care_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    op_mat = models.BooleanField(default=False, verbose_name="MAT Program")
    op_mat_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    op_tribal_afil = models.BooleanField(default=False, verbose_name="Tribal Affiliation")
    op_tribal_afil_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    op_other = models.BooleanField(default=False, verbose_name="Other")
    op_other_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    op_adtl_info = models.TextField(null=True, blank=True, verbose_name="Additional information")







class EmrTcmPpsMhAdmissionForm(EmrForm):
    brc_target_pop_choices = (
        ("high", "High"),
        ("low", "Low"),
        ("short_term", "Short term"),
    )

    # All the demographic information gets pulled from the face sheet
    # but it will show up in the report view for this form
    county_of_residence = models.CharField(max_length=255, choices=CLIENT_C.WI_COUNTY, null=True, blank=True)
    agency_of_resp = models.CharField(max_length=255, null=True, blank=True, verbose_name="Agency of responsibility")
    provider_wpi_npi = models.CharField(max_length=255, null=True, blank=True, verbose_name="Provider WPI/NPI")
    brc_target_pop = models.CharField(max_length=255, null=True, blank=True, choices=brc_target_pop_choices, default="short_term", verbose_name="BRC target population")
    presenting_problem = models.CharField(null=True, blank=True, verbose_name="Presenting problem", max_length=2, choices=sorted(PPS_C.PRESENTING_PROBLEM, key=lambda x: x[1]))
    client_char = models.CharField(null=True, blank=True, verbose_name="Client characteristic", max_length=2, choices=sorted(PPS_C.CLIENT_CHAR, key=lambda x: x[1]))
    commit_stat = models.CharField(verbose_name="Commitment status", max_length=2, null=True, blank=True, choices=PPS_C.LEGAL_STAT)
    referral_source = models.CharField(max_length=2, null=True, blank=True, choices=PPS_C.REF_SOURCE)
    diagnosis2 = models.ForeignKey(Diagnosis, on_delete=models.PROTECT, null=True, blank=True, related_name="tcm_pps_mh_diagnosis2", verbose_name="2nd Diagnosis")
    diagnosis3 = models.ForeignKey(Diagnosis, on_delete=models.PROTECT, null=True, blank=True, related_name="tcm_pps_mh_diagnosis3", verbose_name="3rd Diagnosis")
    diagnosis4 = models.ForeignKey(Diagnosis, on_delete=models.PROTECT, null=True, blank=True, related_name="tcm_pps_mh_diagnosis4", verbose_name="4th Diagnosis")
    diagnosis5 = models.ForeignKey(Diagnosis, on_delete=models.PROTECT, null=True, blank=True, related_name="tcm_pps_mh_diagnosis5", verbose_name="5th Diagnosis")



class EmrTcmPpsMhDischargeForm(EmrForm):
    # Closing PPS Information
    end_date = models.DateField(null=True, blank=True, verbose_name="End date")
    pps_mh_spc_end_reason = models.CharField(null=True, blank=True, max_length=2, choices=PPS_C.MH_SPC_END_REASON, verbose_name="SPC end reason")





class EmrTcmPpsAodaAdmissionForm(EmrForm):
    # Admission information
    brief_service = models.CharField(max_length=3, null=True, blank=True, choices=YES_NO_CHOICES, default="Yes", verbose_name="Brief service")
    referral_source = models.CharField(max_length=2, choices=PPS_C.REF_SOURCE, null=True, blank=True)
    cd_collat = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Co-dependent/collateral")
    agency_of_resp = models.CharField(max_length=255, null=True, blank=True, verbose_name="Agency of responsibility")
    county_of_residence = models.CharField(max_length=255, choices=CLIENT_C.WI_COUNTY, null=True, blank=True, verbose_name="County of residence")
    education = models.CharField(max_length=2, choices=PPS_C.EDUCATION, null=True, blank=True, verbose_name="Education")
    num_arrests = models.IntegerField(null=True, blank=True, verbose_name="Number of arrests in the past 30 days")
    employ_stat = models.CharField(max_length=2, choices=PPS_C.EMPLOY_STAT, null=True, blank=True, verbose_name="Employment status")
    living_stat = models.CharField(max_length=2, choices=PPS_C.LIVING_STAT, null=True, blank=True, verbose_name="Living arrangement", )
    group_attend = models.CharField(max_length=1, choices=PPS_C.GROUP_ATTEND, null=True, blank=True, verbose_name="Support group attendance")
    pregnant = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Pregnant")
    substance_problem1 = models.CharField(max_length=2, null=True, blank=True, verbose_name="Primary substance use problem", choices=PPS_C.SUBSTANCE_PROBLEM)
    route_admin1 = models.CharField(null=True, blank=True, max_length=1, choices=PPS_C.ROUTE_ADMIN, verbose_name="Usual route of administration")
    use_freq1 = models.CharField(null=True, blank=True, verbose_name="Use frequency", max_length=1, choices=PPS_C.USE_FREQ)
    age_first_use1 = models.IntegerField(null=True, blank=True, verbose_name="Age at first use")
    substance_problem2 = models.CharField(max_length=2, null=True, blank=True, verbose_name="Secondary substance use problem", choices=PPS_C.SUBSTANCE_PROBLEM)
    route_admin2 = models.CharField(null=True, blank=True, max_length=1, choices=PPS_C.ROUTE_ADMIN, verbose_name="Usual route of administration")
    use_freq2 = models.CharField(null=True, blank=True, verbose_name="Use frequency", max_length=1, choices=PPS_C.USE_FREQ)
    age_first_use2 = models.IntegerField(null=True, blank=True, verbose_name="Age at first use")
    substance_problem3 = models.CharField(max_length=2, null=True, blank=True, verbose_name="Tertiary substance use problem", choices=PPS_C.SUBSTANCE_PROBLEM)
    route_admin3 = models.CharField(null=True, blank=True, max_length=1, choices=PPS_C.ROUTE_ADMIN, verbose_name="Usual route of administration")
    use_freq3 = models.CharField(null=True, blank=True, verbose_name="Use frequency", max_length=1, choices=PPS_C.USE_FREQ)
    age_first_use3 = models.IntegerField(null=True, blank=True, verbose_name="Age at first use")
    provider_wpi_npi = models.CharField(max_length=255, null=True, blank=True, verbose_name="Provider WPI/NPI")


    def get_spc_start_date(self):
        # The "SPC start date" is really just the start date of the
        # latest episode of this type for this client
        spc_start = EmrEpisode.objects.filter(
            client=self.emrepisode.client,
            episode_type=self.emrepisode.episode_type,
            start__isnull=False,
        ).order_by("-start").first()

        return spc_start.start




class EmrTcmPpsAodaDischargeForm(EmrForm):
    # Closing PPS information
    close_use_freq = models.CharField(null=True, blank=True, verbose_name="Use frequency", max_length=1, choices=PPS_C.USE_FREQ)
    close_group_attend = models.CharField(max_length=1, choices=PPS_C.GROUP_ATTEND, null=True, blank=True, verbose_name="Support group attendance")
    close_num_arrests = models.CharField(max_length=255, null=True, blank=True, verbose_name="Number of arrests in the past 30 days")
    close_living_stat = models.CharField(max_length=2, choices=PPS_C.LIVING_STAT, null=True, blank=True, verbose_name="Living arrangement", )
    close_employ_stat = models.CharField(max_length=2, choices=PPS_C.EMPLOY_STAT, null=True, blank=True, verbose_name="Employment status")
    end_date = models.DateField(null=True, blank=True, verbose_name="End date")
    pps_aoda_spc_end_reason = models.CharField(null=True, blank=True, max_length=2, choices=PPS_C.AODA_SPC_END_REASON, verbose_name="SPC end reason")


    def get_spc_start_date(self):
        # The "SPC start date" is really just the start date of the
        # latest episode of this type for this client
        spc_start = EmrEpisode.objects.filter(
            client=self.emrepisode.client,
            episode_type=self.emrepisode.episode_type,
            start__isnull=False,
        ).order_by("-start").first()

        return spc_start.start






class EmrTcmAssessmentForm(EmrForm):
    # Assessment Description and Background Information
    assessment_date = models.DateField(null=True, blank=True)
    assessment_type = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.TCM_ASSESSMENT_TYPE)
    target_group_dd = models.BooleanField(default=False, verbose_name="Target group DD")
    target_group_pd = models.BooleanField(default=False, verbose_name="Target group PD")
    target_group_mh = models.BooleanField(default=False, verbose_name="Target group MH")
    patient_rights_and_grievances = models.CharField(max_length=255, null=True, blank=True, choices=CLIENT_C.CLTS_CST_RIGHTS_AND_GRIEV)
    presenting_problem = models.TextField(null=True, blank=True, verbose_name="Presenting problem (why is client being opened to case management services)")
    financial = models.TextField(null=True, blank=True, verbose_name="Financial (include insurance information, VA benefits, and other sources of income or assistance such as food share, energy assistance, etc.) ")

    # Living Arrangements
    living_arr = models.TextField(null=True, blank=True, verbose_name="Living arrangements (including description of physical environment as it relates to safety and mobility, who do they live with, what is the need for housing, residential support, adaptive equipment, etc.) ")


    # Mental Health
    mental_health = models.TextField(null=True, blank=True, verbose_name="Mental health diagnoses (current and past)")
    psych_meds = models.TextField(null=True, blank=True, verbose_name="Psychiatric medications")
    treat_service = models.TextField(null=True, blank=True, verbose_name="Treatment and services (current and past, including outpatient services and hospitalizations as well as chapter 51 commitments)  ")
    trauma_hist = models.TextField(null=True, blank=True, verbose_name="Trauma history")
    safety = models.TextField(null=True, blank=True, verbose_name="Safety (include suicidal thoughts, self-harm behavior, feelings or thoughts of wanting to harm or kill someone else).")
    access_to_firearms = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Access to firearms, potential hazards or items identified for self-injury and/or suicide")
    access_to_firearms_dtl = models.TextField(null=True, blank=True, verbose_name="Details")
    delusions = models.TextField(null=True, blank=True, verbose_name="Delusions and/or hallucinations")
    family_mh_hist = models.TextField(null=True, blank=True, verbose_name="History of family mental illness")
    strengths = models.TextField(null=True, blank=True, verbose_name="Strengths and protective factors")
    risks = models.TextField(null=True, blank=True, verbose_name="Risk factors")

    # Medical and Physical
    phys_diags = models.TextField(null=True, blank=True, verbose_name="Physical diagnoses")
    medications = models.TextField(null=True, blank=True, verbose_name="Medications (including prescription and over the counter meds, include the dosage, who prescribes medications, and are they being taken as prescribed)")
    primary_doc = models.TextField(null=True, blank=True, verbose_name="Primary care physician (including name and location)")

    # Substance Use History
    diags = models.TextField(null=True, blank=True, verbose_name="Diagnoses (current and past)")
    current_past_use = models.TextField(null=True, blank=True, verbose_name="Current and past use (indicate substance, frequency, amount, method of use, and last use)")
    od_history = models.TextField(null=True, blank=True, verbose_name="Overdose history")
    withdrawal_needs = models.TextField(null=True, blank=True, verbose_name="Withdrawal needs")
    current_past_treat = models.TextField(null=True, blank=True, verbose_name="Current and past treatment")

    # Social Relationships and Supports
    current_rel_stat = models.TextField(null=True, blank=True, verbose_name="Status of current relationships (positive and negative)")
    formal_service_providers = models.TextField(null=True, blank=True, verbose_name="Formal service providers (include names and agency)")

    contact_name = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    contact_relationship = models.CharField(max_length=255, null=True, blank=True, verbose_name="Relationship")
    contact_address = models.TextField(null=True, blank=True, verbose_name="Address")
    contact_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_name2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    contact_relationship2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Relationship")
    contact_address2 = models.TextField(null=True, blank=True, verbose_name="Address")
    contact_phone2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext2 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_name3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    contact_relationship3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Relationship")
    contact_address3 = models.TextField(null=True, blank=True, verbose_name="Address")
    contact_phone3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext3 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_name4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    contact_relationship4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Relationship")
    contact_address4 = models.TextField(null=True, blank=True, verbose_name="Address")
    contact_phone4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext4 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    contact_name5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    contact_relationship5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Relationship")
    contact_address5 = models.TextField(null=True, blank=True, verbose_name="Address")
    contact_phone5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    contact_phone_ext5 = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    parent_guardian = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Does the individual have a parent, guardian, or Power of Attorney?")
    parent_guardian_name = models.CharField(max_length=255, null=True, blank=True, verbose_name="Name")
    parent_guardian_address = models.TextField(null=True, blank=True, verbose_name="Address")
    parent_guardian_phone = models.CharField(max_length=255, null=True, blank=True, verbose_name="Phone")
    parent_guardian_phone_ext = models.CharField(max_length=255, null=True, blank=True, verbose_name="ext")
    parent_guardian_contacted = models.CharField(max_length=255, null=True, blank=True, choices=YES_NO_UNKNOWN_CHOICES, verbose_name="Was the parent/guardian/power of attorney contacted as part of this assessment?")
    parent_guardian_contacted_dtl = models.TextField(null=True, blank=True, verbose_name="Details")

    # Legal history
    cjs_involvement = models.TextField(null=True, blank=True, verbose_name="Involvement with Criminal Justice System (including review of CCAP history, current/pending charges, probation)")

    # Skills and Employment
    voc_edu_status = models.TextField(null=True, blank=True, verbose_name="Vocational and educational status")
    emp_status = models.TextField(null=True, blank=True, verbose_name="Employment status")
    community_resources = models.TextField(null=True, blank=True, verbose_name="Access to community resources")
    daily_living_activities = models.TextField(null=True, blank=True, verbose_name="Activities of daily living (ambulatory, caring for self, household chores, conducting personal business, etc.)")



    def save(self, *args, **kwargs):
        # This form uses assessment_date and the users don't want to see
        # or use the regular date field. Pretty much all of the rest of the emr
        # forms use the date field, so I've just set this up to just copy the
        # assessment date and if that's not available, today's date.
        # This prevents having to do extra work in validation and keeps this
        # form in-line with the rest of the EMR forms. Maybe one day remove
        # the assessment_date field and update the regular date field's label
        if self.assessment_date:
            self.date = self.assessment_date
        else:
            self.date = timezone.now().date()

        super().save(*args, **kwargs)


    def get_target_groups(self):
        groups = []
        if self.target_group_dd:
            groups.append("DD")
        if self.target_group_pd:
            groups.append("PD")
        if self.target_group_mh:
            groups.append("MH")

        return groups

    def get_target_groups_html(self):
        return ", ".join(self.get_target_groups())






class EmrDocType(models.Model):
    name = models.CharField(max_length=255)
    active = models.BooleanField(default=True)
    type_clinic_preadmit = models.BooleanField(default=False, verbose_name="Clinic Pre-admit")
    type_clts = models.BooleanField(default=False, verbose_name="CLTS")
    type_crisis = models.BooleanField(default=False, verbose_name="Crisis")
    type_cst = models.BooleanField(default=False, verbose_name="CST")
    type_randi = models.BooleanField(default=False, verbose_name="R & I")
    type_tcm = models.BooleanField(default=False, verbose_name="TCM")
    type_clts_preadmit = models.BooleanField(default=False, verbose_name="CLTS Pre-admit")
    type_b3 = models.BooleanField(default=False, verbose_name="Birth to 3")
    type_b3_preadmit = models.BooleanField(default=False, verbose_name="Birth to 3 Pre-admit")


    def __str__(self):
        return f"{self.name}"



def upload_emr_doc(instance, filename):
    return f"emr_docs/{instance.id}/{filename}"


class EmrDoc(models.Model): 
    emrepisode = models.ForeignKey(EmrEpisode, on_delete=models.PROTECT)
    emrdoctype = models.ForeignKey(EmrDocType, on_delete=models.PROTECT)
    date = models.DateField() 
    uploaded_by = models.ForeignKey(User, related_name="emr_doc_uploaded_by", on_delete=models.PROTECT, null=True)
    description = models.TextField(null=True, blank=True)
    doc_file = models.FileField(null=True, blank=True, upload_to=upload_emr_doc, verbose_name="Document Upload")
    upload_date = models.DateTimeField(null=True, blank=True)    


    def __str__(self):
        if self.description:
            return f"{self.date.strftime('%m/%d/%Y')} {self.emrdoctype} '{self.description}' ({self.id})"
        else:
            return f"{self.date.strftime('%m/%d/%Y')} {self.emrdoctype} ({self.id})"


    def get_filename(self):
        # Returns a string of just the file's name, not any directories
        return self.doc_file.name.split("/")[-1]


    def get_shared_links(self):
        SharedLink = apps.get_model("external", "SharedLink")

        found = SharedLink.objects.filter(
            model_name="emrdoc",
            app_name="client",
            ref_id=self.id,
            revoked_at__isnull=True,
            expires_at__gt=timezone.now(),
        ).order_by("-created_at")

        return found

































