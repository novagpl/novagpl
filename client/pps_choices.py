"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# https://www.dhs.wisconsin.gov/forms/f0/f00588a.pdf
NULL = (
    (None, "---------")
)

REF_SOURCE = (
    ("1", "1 - Self"),
    ("2", "2 - Family, friend, or guardian"),
    ("3", "3 - AODA program/provider (includes AA, Al-Anon)"),
    ("4", "4 - Inpatient hospital or residential facility"),
    ("5", "5 - School, college"),
    ("6", "6 - IDP - Court"),
    ("7", "7 - IDP - Division of Motor Vehicle (DMV)"),
    ("8", "8 - Corrections, probation, parole"),
    ("9", "9 - Other court, criminal or juvenile justice system"),
    ("10", "10 - Employer, Employee Assistance Program (EAP)"),
    ("11", "11 - County social services"),
    ("12", "12 - Child Protective Services agency"),
    ("13", "13 - IV drug outreach worker"),
    ("14", "14 - Other"),
    ("15", "15 - Drug court"),
    ("16", "16 - OWI court - monitors the multiple OWI offender"),
    ("17", "17 - Screening Brief Intervention Referral Treatment (SBIRT)"),
    ("18", "18 - Mental health program/provider"),
    ("19", "19 - Hospital emergency room"),
    ("20", "20 - Primary care physician or other healthcare program/provider"),
    ("21", "21 - Law enforcement, police"),
    ("22", "22 - Mental health court"),
    ("23", "23 - Homeless outreach worker"),
    ("99", "99 - Unknown"),
)

CLIENT_CHAR = (
    ("2", "02 - Mental illness (excluding SPMI)"),
    ("3", "03 - Serious and persistent mental illness (SPMI)"),
    ("4", "04 - Alcohol client"),
    ("5", "05 - Drug client"),
    ("7", "07 - Blind / visually impaired"),
    ("8", "08 - Hard of hearing"),
    ("9", "09 - Physical disability / mobility impaired"),
    ("10", "10 - Severe alcohol or other drug client"),
    ("12", "12 - Alcohol and other drug client"),
    ("14", "14 - Family member of mental health client"),
    ("16", "16 - Family member of alcohol and other drug client"),
    ("17", "17 - Intoxicated driver"),
    ("18", "18 - Alzheimer's disease / related dementia"),
    ("19", "19 - Developmental disability - brain trauma"),
    ("23", "23 - Developmental disability - cerebral palsy"),
    ("25", "25 - Developmental disability – autism spectrum"),
    ("26", "26 - Developmental disability – intellectual disability"),
    ("27", "27 - Developmental disability - epilepsy"),
    ("28", "28 - Developmental disability - other or unknown"),
    ("29", "29 - Family member of a client with a developmental disability"),
    ("32", "32 - Blind/Deaf"),
    ("33", "33 - Corrections / criminal justice client (adult only)"),
    ("36", "36 - Other disability"),
    ("37", "37 - Frail medical condition"),
    ("38", "38 - Criminal justice system involvement (alleged or adjudicated)"),
    ("39", "39 - Gambling client"),
    ("43", "43 - Migrant"),
    ("44", "44 - Refugee"),
    ("45", "45 - Cuban / Haitian entrant"),
    ("50", "50 - Regular caregiver of dependent person"),
    ("55", "55 - Frail elderly"),
    ("57", "57 - Abused / neglected elder"),
    ("59", "59 - Unmarried parent"),
    ("61", "61 - CHIPS - abuse and neglect"),
    ("62", "62 - CHIPS - abuse"),
    ("63", "63 - CHIPS - neglect"),
    ("64", "64 - Family member of abused / neglected child"),
    ("66", "66 - Delinquent"),
    ("68", "68 - CHIPS - other"),
    ("69", "69 - JIPS - status offender"),
    ("70", "70 - Family member of status offender"),
    ("71", "71 - Victim of domestic abuse"),
    ("72", "72 - Victim of abuse or neglect (alleged or adjudicated)"),
    ("73", "73 - Family member of delinquent"),
    ("74", "74 - Family member of CHIPS - other"),
    ("79", "79 - Deaf"),
    ("80", "80 - Homeless"),
    ("84", "84 - Repeated school truancy"),
    ("86", "86 - Severe emotional disturbance - child / adolescent"),
    ("90", "90 - Special study code (to be defined as need arises)"),
    ("91", "91 - Hurricane Katrina evacuee"),
    ("92", "92 - Hurricane Rita evacuee"),
    ("99", "99 - None of the above"),
)

CRIMINAL_INTERACTION = (
    ("1", "1 - None"),
    ("2", "2 - On probation"),
    ("3", "3 - Arrest(s)"),
    ("4", "4 - Jailed / imprisoned (includes Huber)"),
    ("5", "5 - On parole"),
    ("6", "6 - Juvenile justice system contact"),
    ("9", "9 - Unknown"),
)

DAILY_ACTIVITY = (
    ("1", "1 - No educational, social or planned activity"),
    ("2", "2 - Part-time educational activity"),
    ("3", "3 - Full-time educational activity"),
    ("4", "4 - Meaningful social activity"),
    ("5", "5 - Volunteer or planned formal activities"),
    ("6", "6 - Other activities"),
    ("9", "9 - Unknown"),
)

EDUCATION = (
    ("1", "1 - 1st grade"),
    ("2", "2 - 2nd grade"),
    ("3", "3 - 3rd grade"),
    ("4", "4 - 4th grade"),
    ("5", "5 - 5th grade"),
    ("6", "6 - 6th grade"),
    ("7", "7 - 7th grade"),
    ("8", "8 - 8th grade"),
    ("9", "9 - 9th grade"),
    ("10", "10 - 10th grade"),
    ("11", "11 - 11th grade"),
    ("12", "12 - High school diploma or GED"),
    ("14", "14 - Some college or vocational / technical school; assoc. degree or voc. tech. degree"),
    ("16", "16 - Bachelor’s degree"),
    ("18", "18 - Advanced degree (Master's, Ph.D.)"),
    ("99", "99 - Unknown"),
)

GROUP_ATTEND = (
    ("1", "1 - 16 or more times in the past 30 days"),
    ("2", "2 - 8-15 times in the past 30 days"),
    ("3", "3 - 4-7 times in the past 30 days"),
    ("4", "4 - 1-3 times in the past 30 days"),
    ("5", "5 - No attendance in the past 30 days"),
    ("9", "9 - Unknown"),
)

HEALTH_STATUS = (
    ("1", "1 - No health condition"),
    ("2", "2 - Stable / capable"),
    ("3", "3 - Stable / incapable"),
    ("4", "4 - Unstable / capable"),
    ("5", "5 - Unstable / incapable"),
    ("6", "6 - New symptoms / capable"),
    ("7", "7 - New symptoms / incapable"),
    ("9", "9 - Unknown"),
)

EMPLOY_STAT = (
    ("1", "1 - Full-time competitive (35 or more hrs/wk)"),
    ("2", "2 - Part-time competitive (less than 35 hrs/wk)"),
    ("3", "3 - Unemployed (looking for work past 30 days)"),
    ("4", "4 - Do Not Use, this code has been retired"),
    ("5", "5 - Not in the labor force - homemaker"),
    ("6", "6 - Not in the labor force - student"),
    ("7", "7 - Not in the labor force - retired"),
    ("8", "8 - Not in the labor force - disabled"),
    ("9", "9 - Not in the labor force - jail, prison or institution"),
    ("10", "10 - Not in the labor force - sheltered employment"),
    ("11", "11 - Not in the labor force - other reason"),
    ("12", "12 - Supported competitive employment"),
    ("98", "98 - Not applicable - Children 15 and under"),
    ("99", "99 - Unknown"),
)

LIVING_STAT = (
    ("1", "1 - Street, shelter, no fixed address, homeless"),
    ("2", "2 - Private residence or household; living alone or with others without supervision; includes persons age 18 and older living with parents (Adults Only)"),
    ("3", "3 - Supported residence (ADULTS ONLY)"),
    ("4", "4 - Supervised licensed residential facility"),
    ("5", "5 - Institutional setting, hospital, nursing home"),
    ("6", "6 - Jail or correctional facility"),
    ("7", "7 - Living with parents (UNDER AGE 18 ONLY)"),
    ("8", "8 - Living with relatives, friends (UNDER AGE 18 ONLY)"),
    ("9", "9 - Foster home"),
    ("10", "10 - Crisis stabilization home/center"),
    ("11", "11 - Other living arrangement"),
    ("99", "99 - Unknown"),
)

LEGAL_STAT = (
    ("1", "1 - Voluntary"),
    ("2", "2 - Voluntary with settlement agreement"),
    ("3", "3 - Involuntary civil - Chapter 51"),
    ("4", "4 - Involuntary civil - Chapter 55"),
    ("5", "5 - Involuntary criminal"),
    ("6", "6 - Guardianship"),
    ("9", "9 - Unknown"),
)


USE_FREQ = (
    ("1", "1 - No use in the past month"),
    ("2", "2 - 1-3 days in the past month (less often than once a week)"),
    ("3", "3 - 1-2 days per week"),
    ("4", "4 - 3-6 days per week"),
    ("5", "5 - Daily"),
    ("9", "9 - Unknown"),
)

ROUTE_ADMIN = (
    ("1", "1 - Oral (by mouth swallowing)"),
    ("2", "2 - Smoking (inhale by burning / heating substance)"),
    ("3", "3 - Inhalation (inhale or snort through the nose or mouth without burning the substance)"),
    ("4", "4 - Injection (IV or intramuscular or skin popping)"),
    ("5", "5 - Other"),
    ("9", "9 - Unknown"),
)

SUBSTANCE_PROBLEM = (
    ("1", "1 - None"),
    ("2", "2 - Alcohol"),
    ("3", "3 - Cocaine / crack"),
    ("4", "4 - Marijuana / hashish / cannabis / THC"),
    ("5", "5 - Heroin"),
    ("6", "6 - Nonprescription methadone"),
    ("7", "7 - Dilaudid / hydromorphone"),
    ("8", "8 - Other opiates and synthetics (codeine, morphine, oxycodone, demerol, opium, fentanyl, oxymorphone, etc.)"),
    ("9", "9 - PCP (phencyclidine)"),
    ("10", "10 - LSD"),
    ("11", "11 - Other hallucinogens (MDA, MDMA-ecstacy, peyote, mescaline, psilocybin, psilocin, STP, ketamine)"),
    ("12", "12 - Methamphetamine / ice; methcathinone / cat"),
    ("13", "13 - Other amphetamines (benzedrine, speed, dexedrine, methedrine, ritalin, preludin, and any other amines and related drugs)"),
    ("14", "14 - Other stimulants (phentermine, benzphetamine, mazindol, phendimetrazine, pemoline, chlortermine, etc.)"),
    ("15", "15 - Benzodiazepines (diazepam, flurazepam, chlordiazepoxide, clorazepate, lorazepam, alprazolam, oxazepam, temazepam, triazolam, clonazepam, halazepam, Rohypnol, etc.)"),
    ("16", "16 - Other tranquilizers (Meprobamate, Equanil, Miltown)"),
    ("17", "17 - Barbiturates (phenobarbital, Seconal, Nembutal, amobarbital, etc.)"),
    ("18", "18 - Other nonbarbiturate sedatives or hypnotics (methaqualone, Quaalude, glutethimide, chloral hydrate, ethchlorvynol, Placidyl, GHB, etc.)"),
    ("19", "19 - Inhalants (ether, glue, aerosols, solvents, gases, chloroform, nitrous oxide)"),
    ("20", "20 - Over-the-counter diet, alert, sleep aids, cough syrup"),
    ("21", "21 - Other"),
)

PPS_UNIT = (
    ("1", "Hours"),
    ("2", "Days"),
    ("3", "Other"),
)

PSY_ENV_STRESS = (
    ("1", "1 - None"),
    ("2", "2 - Mild"),
    ("3", "3 - Moderate"),
    ("4", "4 - Severe"),
    ("5", "5 - Extreme"),
    ("6", "6 - Catastrophic"),
    ("0", "0 - Inadequate information"),
)


PRESENTING_PROBLEM = (
    ("1", "01 - Marital / family problem"),
    ("2", "02 - Social / interpersonal (other than family problem)"),
    ("3", "03 - Problems coping with daily roles and activities (including job, school, housework, daily grooming, financial management, etc.)"),
    ("4", "04 - Medical / somatic"),
    ("5", "05 - Depressed mood and / or anxious"),
    ("6", "06 - Attempt, threat, or danger of suicide"),
    ("7", "07 - Alcohol"),
    ("8", "08 - Drugs"),
    ("9", "09 - Involvement with criminal justice system"),
    ("10", "10 - Eating disorder"),
    ("11", "11 - Disturbed thoughts"),
    ("12", "12 - Abuse / assault / rape victim"),
    ("13", "13 - Runaway behavior"),
    ("14", "14 - Emergency detention"),
    ("99", "99 - Unknown"),
)

SUICIDE_RISK = (
    ("1", "No risk factors"),
    ("2", "Presence of risk factors"),
    ("3", "High potential for suicide"),
    ("9", "Unknown"),
)

AODA_TARGET_GROUP = (
    ("4", "4 - Alcohol abuse"),
    ("5", "5 - Drug abuse"),
    ("6", "6 - OMTC Participant"),
    ("7", "7 - SOR Grant Participant"),
    ("8", "8 - MAT in Jails Participant"),
    ("17", "17 - Intoxicated driver"),
    ("18", "18 - Alcohol and other drug abuse"),
    ("74", "74 - Family member / other of AODA client"),
)

CORE_TARGET_GROUP = (
    (None, "---------"),
    ("1", "1 - Developmental disability"),
    ("18", "18 - Alcohol and other drug abuse (DSS use only)"),
    ("31", "31 - Mental health (DSS use only)"),
    ("57", "57 - Physical and sensory disability"),
    ("58", "58 - Adults and elderly"),
    ("64", "64 - Children and family"),
    ("72", "72 - Family member / other of DD client"),
    ("74", "74 - Family member / other of AODA client"),
    ("75", "75 - Family member / other of mental health client"),
    ("76", "76 - Family member / other of P and SD client"),
    ("77", "77 - Family member / other of adults and elderly client"),
)



AODA_SPC_END_REASON = (
    ("1", "1 - Completed service - major improvement"),
    ("2", "2 - Completed service - moderate improvement"),
    ("3", "3 - Completed service - no positive change"),
    ("4", "4 - Referred - to another nonalcohol / drug agency, program or service"),
    ("5", "5 - Behavioral termination - staff / program decision to terminate due to rule violation"),
    ("6", "6 - Withdrew - against staff advice"),
    ("7", "7 - Funding / authorization expired"),
    ("8", "8 - Incarcerated"),
    ("9", "9 - Death"),
    ("14", "14 - Referral to another AODA agency or program"),
    ("15", "15 - Transfer to another AODA service within an agency or program"),
    ("16", "16 - Funding / authorization expired, same service reopened"),
    ("19", "19 - Service is continuing"),
)

MH_EPI_END_REASON = (
    ("1", "1 - Completed service - major improvement"),
    ("2", "2 - Completed service - moderate improvement"),
    ("3", "3 - Completed service - no change"),
    ("4", "4 - Transferred to other community resource"),
    ("5", "5 - Administratively discontinued service"),
    ("6", "6 - Referred"),
    ("7", "7 - Withdrew against staff advice"),
    ("8", "8 - Funding / authorization expired"),
    ("9", "9 - Incarcerated"),
    ("10", "10 - Entered nursing home or institutional care"),
    ("11", "11 - No probable cause"),
    ("12", "12 - Death"),
)

MH_SPC_END_REASON = (
    ("1", "1 - Completed service - major improvement"),
    ("2", "2 - Completed service - moderate improvement"),
    ("3", "3 - Completed service - no change"),
    ("4", "4 - Transferred to other community resource"),
    ("5", "5 - Administratively discontinued"),
    ("6", "6 - Referred"),
    ("7", "7 - Withdrew against staff advice"),
    ("8", "8 - Funding / authorization expired"),
    ("9", "9 - Incarcerated"),
    ("10", "10 - Entered nursing home or institutional care"),
    ("11", "11 - No probable cause"),
    ("12", "12 - Death"),
    ("19", "19 - Service is continuing"),
)


CORE_EPI_END_REASON = (
    ("01", "01 - Assessment complete / decision not to serve"),
    ("02", "02 - Successful completion"),
    ("03", "03 - Client referred"),
    ("04", "04 - Client no longer wants service"),
    ("05", "05 - Client relocated"),
    ("06", "06 - Death of client"),
    ("07", "07 - Objectives not attained"),
    ("08", "08 - Noncompliance with the program"),
    ("09", "09 - Service not available"),
    ("10", "10 - Court dismissed"),
    ("11", "11 - Client no longer income eligible"),
    ("12", "12 - Court order expired / client not income eligible"),
    ("13", "13 - Somewhat successful completion"),
    ("98", "98 - Other reason"),
)

CORE_CLIENT_CHAR = (
    ("02", "02 - Mental illness (excluding SPMI)"),
    ("03", "03 - Serious and persistent mental illness (SPMI)"),
    ("04", "04 - Alcohol client"),
    ("05", "05 - Drug client"),
    ("07", "07 - Blind / visually impaired"),
    ("08", "08 - Hard of hearing"),
    ("09", "09 - Physical disability / mobility impaired"),
    ("10", "10 - Chronic alcoholic"),
    ("12", "12 - Alcohol and other drug client"),
    ("14", "14 - Family member of mental health client"),
    ("16", "16 - Family member of alcohol and other drug client"),
    ("17", "17 - Intoxicated driver"),
    ("18", "18 - Alzheimer’s disease / related dementia"),
    ("19", "19 - Developmental disability - brain trauma"),
    ("23", "23 - Developmental disability - cerebral palsy"),
    ("25", "25 - Developmental disability – autism spectrum"),
    ("26", "26 - Developmental disability – cognitive disability"),
    ("27", "27 - Developmental disability - epilepsy"),
    ("28", "28 - Developmental disability - other or unknown"),
    ("29", "29 - Family member of a client with a developmental disability"),
    ("32", "32 - DeafBlind"),
    ("33", "33 - Corrections / criminal justice system client (adults only)"),
    ("36", "36 - Other disability"),
    ("39", "39 - Gambling client"),
    ("43", "43 - Migrant"),
    ("44", "44 - Refugee"),
    ("45", "45 - Cuban / Haitian entrant"),
    ("50", "50 - Regular caregiver of dependent person"),
    ("55", "55 - Frail elderly"),
    ("57", "57 - Abused / neglected elder"),
    ("59", "59 - Unmarried parent"),
    ("71", "71 - Victim of domestic abuse"),
    ("79", "79 - Deaf"),
    ("86", "86 - Severe emotional disturbance"),
    ("99", "99 - None of above"),
)


BRC_TARGET_POP = (
    ("H", "H - Ongoing, high intensity, comprehensive services"),
    ("L", "L - Ongoing, low intensity services"),
    ("S", "S - Short-term situational service"),
)


B3_LANGUAGE_PREF = (
    ("01", "01 - Albanian"),
    ("02", "02 - Arabic"),
    ("03", "03 - Bosn/Croat/Serv"),
    ("04", "04 - Burmese"),
    ("05", "05 - Cambodian"),
    ("06", "06 - Chinese"),
    ("07", "07 - English"),
    ("08", "08 - Farsi"),
    ("09", "09 - French"),
    ("10", "10 - German"),
    ("11", "11 - Greek"),
    ("12", "12 - Hmong"),
    ("13", "13 - Italian"),
    ("14", "14 - Korean"),
    ("15", "15 - Laotian"),
    ("16", "16 - Native American"),
    ("17", "17 - Norwegian"),
    ("18", "18 - Polish"),
    ("19", "19 - Russian"),
    ("20", "20 - Somali"),
    ("21", "21 - Spanish"),
    ("22", "22 - Swedish"),
    ("23", "23 - Thai"),
    ("24", "24 - Ukrainian"),
    ("25", "25 - Vietnamese"),
    ("99", "99 - Other"),
)

B3_REFER_SOURCE = (
    ("01", "01 - CAPTA Referral"),
    ("02", "02 - Child Care Provider"),
    ("03", "03 - County Social Services Agency"),
    ("04", "04 - CYSHCN Regional Center"),
    ("05", "05 - Head Start Provider"),
    ("06", "06 - Hospital or Specialty Clinic"),
    ("07", "07 - Other county staff"),
    ("08", "08 - Other health care provider"),
    ("09", "09 - Parent or relative"),
    ("10", "10 - Physician"),
    ("11", "11 - Public Health Agency"),
    ("12", "12 - School District"),
    ("13", "13 - Tribal Health Center or Tribal CYSHCN"),
    ("14", "14 - Tribal School or Tribal Head Start Program"),
    ("99", "99 - Other"),
)

B3_RESULT_REPORTED = (
    ("1", "1 - Unable to Connect with Family"),
    ("2", "2 - No Evaluation Through Birth to 3 Program"),
    ("3", "3 - Child Not Eligible for Birth to 3 Program"),
    ("4", "4 - Child Eligible, Family Declined"),
    ("5", "5 - Family Declined Participation in one or more services"),
    ("6", "6 - Family Chose to discontinue a previously received service"),
)

B3_SERVICES = (
    ("01", "01 - Assistive Technology"),
    ("02", "02 - Audiology"),
    ("03", "03 - Communication Services"),
    ("04", "04 - Family education and counseling"),
    ("05", "05 - Health Services"),
    ("06", "06 - Interpreter Services"),
    ("07", "07 - Medical Services"),
    ("08", "08 - Nursing Services"),
    ("09", "09 - Nutrition Services"),
    ("10", "10 - Occupational Therapy"),
    ("11", "11 - Physical Therapy"),
    ("12", "12 - Psychological Services"),
    ("13", "13 - Service Coordination"),
    ("14", "14 - Social Work"),
    ("15", "15 - Special Instruction"),
    ("16", "16 - Transportation"),
    ("17", "17 - Vision Services"),
    ("99", "99 - Other"),
)
