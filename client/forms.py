"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from django.conf import settings
#from django.utils import timezone
from django.db.models import Q
from django.forms import (
    ModelForm, DateInput, Select, SelectMultiple,
    CharField, Textarea, TextInput, HiddenInput,
    ClearableFileInput, CheckboxInput, NumberInput,
    TimeInput, EmailInput, RadioSelect,
)


from .models import (
    Client, Address, ContactInfo, CaseNote, CaseNoteFile, 
    PpsAodaEpi, PpsMhEpi, PpsCoreEpi, PpsB3Epi, EmrForm, EmrDoc, 
    EmrClinicPreadmitEpisode, EmrCltsEpisode, EmrCrisisEpisode, EmrCstEpisode,
    EmrRandiEpisode, EmrTcmEpisode, EmrCaseNoteForm, EmrRandiIntakeForm, 
    EmrCrisisAlertForm, EmrCrisisPlanForm, EmrMessage,
    EmrCltsPreadmitEpisode, EmrCltsCstAssessmentForm, Insurance, 
    EmrRandiReferralForm, EmrRandiExternalReferralForm, PastLegalName,
    EmrClinicWaitlistForm, EmrPlacementForm, Service, EmrNwcAssesResponseForm,
    EmrJacksonCountyAssesResponseForm, EmrCrisisNwcNoteForm,
    EmrCrisisResponsePlanReviewForm, EmrFormSignature, EmrTcmAssessmentForm,
    EmrTcmPpsMhAdmissionForm, EmrTcmPpsMhDischargeForm, EmrTcmPpsAodaAdmissionForm,
    EmrTcmPpsAodaDischargeForm, EmrCstPpsMhAdmissionForm, EmrCstPpsMhDischargeForm,
    EmrB3Episode, EmrB3PreadmitEpisode,
)

from accounting.models import AbilityToPay
from utils import render_form, get_app_config, get_pps_epi



from config.models import UserConfig
from validation import (
    validate_birthdate, validate_client_name, validate_ability_to_pay,
    validate_emr_episode, validate_emrform_sig,
)

from client.models import Diagnosis

import client.pps_choices as PPS_C
import client.client_choices as CLIENT_C

#from custom_widgets import MultipleTimeEntryWidget


NULL_CHOICE = (None, "----------", )



class EmrFormSigForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.request = kwargs.pop("request", None)

        if hasattr(self, "instance") and self.instance.id:
            del self.fields["typed_sig"]

            render_form(self, locked=True)

        else:
            render_form(self)


    typed_sig = CharField(
        label="Enter full name to sign",
        required=False,
        widget=TextInput(attrs={"width": 100, "class": "optional-off"}))



    def clean(self):
        self.cleaned_data = super().clean()
        validate_emrform_sig(self)


    class Meta:
        model = EmrFormSignature

        fields = [
            "sig_data",
            "sig_type",
            "signer_role",
            "signer_first",
            "signer_middle",
            "signer_last",
            "typed_sig",
        ]

        widgets = {
            "sig_data": HiddenInput(),
            "sig_type": Select(attrs={
                "width": 33,
                "selectshow": "topaz|topaz_wrapper;simple_typed|typed_sig;mouse|mouse_sig_wrapper,mouse_sig_canvas,mouse_sig_cancel"}),
            "signer_role": Select(attrs={"width": 33}),
            "signer_first": TextInput(attrs={"clear": "left", "width": 33}),
            "signer_middle": TextInput(attrs={"width": 33}),
            "signer_last": TextInput(attrs={"width": 33}),
            "typed_sig": TextInput(attrs={"width": 100, "class": "optional-off"}),
        }




class PastLegalNameForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PastLegalNameForm, self).__init__(*args, **kwargs)
        render_form(self)


    class Meta:
        model = PastLegalName

        fields = [
            "client",
            "last_name",
            "first_name",
            "middle_name",
            "title",
            "suffix",
        ]

        widgets = {
            "client": HiddenInput(),
            "last_name": TextInput(attrs={"width": 100}),
            "first_name": TextInput(attrs={"width": 100}),
            "middle_name": TextInput(attrs={"width": 50}),
            "title": Select(attrs={"width": 25}),
            "suffix": Select(attrs={"width": 25}),
        }



class EmrMessageForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(EmrMessageForm, self).__init__(*args, **kwargs)
        render_form(self)


    class Meta:
        model = EmrMessage

        fields = [
            "message",
            "expires_at"
        ]

        widgets = {
            "expires_at": DateInput(format=settings.DATE_FORMAT, attrs={
                'width': "auto", 'class': 'datepicker'}),
        }






class EmrDocForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrDocForm, self).__init__(*args, **kwargs)
        render_form(self)


    class Meta:
        model = EmrDoc
        #exclude = ["signed_by", "caseworker_signature", "signature"]
        fields = [                        
            "date",                        
            "doc_file",
            "description",
            "uploaded_by",
            "emrepisode",
            "emrdoctype",
            "upload_date",
        ] 

        widgets = {            
            "date": DateInput(format=settings.DATE_FORMAT, attrs={
                'width': "auto", 'class': 'datepicker'}),
            "description": TextInput(attrs={"width": 100, "clear": "left "}),
            "doc_file": ClearableFileInput(attrs={"class": "file-upload"}),
            "uploaded_by": HiddenInput(),
            "emrepisode": HiddenInput(),
            "emrdoctype": HiddenInput(),
            "upload_date": HiddenInput(),
        }








# These are for the EMR forms (like case notes) below. NOT for the Emr Docs
# which are just scanned in and categorized documents
emr_form_fields = [
    "created_by",
    "signature",
    "signed_by",
    "emrepisode",
    "emrformtype",
    "date",
    "credentials",
    "crisis_type_of_contact",
    "place_of_service",
    "service_start",
    "service_end",
    "service_minutes",
    "travel_minutes",    
    "doc_minutes",
    "total_minutes",
    "units",
    "billing_code",
    "note",
    "linked_docs",
]

emr_form_widgets = {
    "created_by": HiddenInput(),
    "signature": HiddenInput(),
    "signed_by": HiddenInput(),
    "emrepisode": HiddenInput(),
    "emrformtype": HiddenInput(),
    "date": DateInput(format=settings.DATE_FORMAT, attrs={
        'width': "auto", 'class': 'datepicker'}),
    "credentials": Select(attrs={"width": 25}),
    "crisis_type_of_contact": Select(attrs={"width": 25}),
    "service_start": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 16, "type": "time", }),
    "service_end": TimeInput(attrs={"width": 16, "type": "time", }),
    "service_minutes": NumberInput(attrs={"width": 16, "min": 0}),
    "travel_minutes": NumberInput(attrs={"width": 16, "min": 0}),
    "doc_minutes": NumberInput(attrs={"width": 16, "min": 0}),
    "total_minutes": NumberInput(attrs={"width": 16, "min": 0}),
    "units": TextInput(attrs={"width": 16}),
    "billing_code": Select(attrs={"width": 16, }),
    "place_of_service": Select(attrs={"width": 50}),
    "note": Textarea(attrs={"width": 100, "clear": "left ", "rows": 10}),
    "linked_docs": SelectMultiple(attrs={
        "clear": "both",
        "width": "100%",
        "use_popup_lookup": "EmrForm",
        "popup_lookup_child_model": "EmrDoc",
    }),

}


class EmrFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrFormForm, self).__init__(*args, **kwargs)

        if hasattr(self, "instance") and hasattr(self.instance, "emrepisode"):
            docs = EmrDoc.objects.filter(
                emrepisode=self.instance.emrepisode).order_by("-date")            
            self.fields["linked_docs"].queryset = docs  

        if hasattr(self, "instance") and self.instance.is_finalized:
            render_form(self, locked=True)
        else:
            render_form(self)


    class Meta:
        model = EmrForm
        #exclude = ["signed_by", "caseworker_signature", "signature"]

        fields = emr_form_fields

        widgets = emr_form_widgets








class EmrCaseNoteFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)


        if hasattr(self, "instance") and hasattr(self.instance, "emrepisode"):
            # Depending on the episode there may be some fields that need
            # to be shown or hidden or altered
            self.fields["date"].required = False
            self.fields["place_of_service"].widget.choices = (NULL_CHOICE, ) + CLIENT_C.PLACE_OF_SERVICE_CODE


            # Saving all the default widgets and then hiding them.
            # When rendering the form, 99% of the time its fine to grab
            # the default widget in emr_form_widgets
            # All case notes start off with all hidden fields and depending on
            # the episode type and other factors some of the fields will be
            # shown
            default_widgets = {}
            for field in self.fields:
                # Saving the default widgets before hiding them so depending on
                # the type of case note the fields can be shown without having
                # to write out their widgets again
                default_widgets[field] = self.fields[field].widget

                # All fields are hidden first, then shown in each case note
                # as needed
                self.fields[field].widget = HiddenInput()


            # B3
            if self.instance.emrepisode.episode_type == "b3":
                self.fields["date"].widget = default_widgets["date"]

                self.fields["service"].widget = Select(attrs={
                    "width": 50,
                    "clear": "left",
                })

                b3_spc_codes = ["706.17", "601", "706.13", "604", ]
                b3_services = Service.objects.filter(
                    spc_code__in=b3_spc_codes
                )
                self.fields["service"].queryset = b3_services

                self.fields["ct_collateral"].widget = default_widgets["ct_collateral"]
                self.fields["ct_direct"].widget = default_widgets["ct_direct"]
                self.fields["ct_face"].widget = default_widgets["ct_face"]
                self.fields["ct_home"].widget = default_widgets["ct_home"]
                self.fields["ct_record"].widget = default_widgets["ct_record"]
                self.fields["ct_other"].widget = default_widgets["ct_other"]

                self.fields["rights_offered"].widget = default_widgets["rights_offered"]
                self.fields["see_case_file"].widget = default_widgets["see_case_file"]

                self.fields["place_of_service"].widget = default_widgets["place_of_service"]
                self.fields["place_of_service"].widget.choices = (NULL_CHOICE, ) + CLIENT_C.PLACE_OF_SERVICE_CODE_B3
                self.fields["place_of_service"].widget.attrs["clear"] = "left"

                self.fields["service_start"].widget = default_widgets["service_start"]
                self.fields["service_end"].widget = default_widgets["service_end"]
                self.fields["service_minutes"].widget = default_widgets["service_minutes"]
                self.fields["travel_minutes"].widget = default_widgets["travel_minutes"]
                self.fields["doc_minutes"].widget = default_widgets["doc_minutes"]
                self.fields["total_minutes"].widget = default_widgets["total_minutes"]
                self.fields["units"].widget = default_widgets["units"]
                self.fields["note"].widget = default_widgets["note"]
                self.fields["linked_docs"].widget = default_widgets["linked_docs"]

                self.fields["billing_code"].widget = default_widgets["billing_code"]
                self.fields["billing_code"].widget.choices = (
                    NULL_CHOICE,
                    ("T1017", "T1017"),
                )

            # B3 Pre-Admit
            if self.instance.emrepisode.episode_type == "b3_preadmit":
                self.fields["date"].widget = default_widgets["date"]

                self.fields["service"].widget = Select(attrs={
                    "width": 50,
                    "clear": "left",
                })

                b3_spc_codes = ["706.17", "601", "706.13", "604", ]
                b3_services = Service.objects.filter(
                    spc_code__in=b3_spc_codes
                )
                self.fields["service"].queryset = b3_services

                self.fields["ct_collateral"].widget = default_widgets["ct_collateral"]
                self.fields["ct_direct"].widget = default_widgets["ct_direct"]
                self.fields["ct_face"].widget = default_widgets["ct_face"]
                self.fields["ct_home"].widget = default_widgets["ct_home"]
                self.fields["ct_record"].widget = default_widgets["ct_record"]
                self.fields["ct_other"].widget = default_widgets["ct_other"]

                self.fields["rights_offered"].widget = default_widgets["rights_offered"]
                self.fields["see_case_file"].widget = default_widgets["see_case_file"]

                self.fields["place_of_service"].widget = default_widgets["place_of_service"]
                self.fields["place_of_service"].widget.choices = (NULL_CHOICE, ) + CLIENT_C.PLACE_OF_SERVICE_CODE_B3
                self.fields["place_of_service"].widget.attrs["clear"] = "left"

                self.fields["service_start"].widget = default_widgets["service_start"]
                self.fields["service_end"].widget = default_widgets["service_end"]
                self.fields["service_minutes"].widget = default_widgets["service_minutes"]
                self.fields["travel_minutes"].widget = default_widgets["travel_minutes"]
                self.fields["doc_minutes"].widget = default_widgets["doc_minutes"]
                self.fields["total_minutes"].widget = default_widgets["total_minutes"]
                self.fields["units"].widget = default_widgets["units"]
                self.fields["note"].widget = default_widgets["note"]
                self.fields["linked_docs"].widget = default_widgets["linked_docs"]

                self.fields["billing_code"].widget = default_widgets["billing_code"]
                self.fields["billing_code"].widget.choices = (
                    NULL_CHOICE,
                    ("P9999", "P9999"),
                    ("T1017", "T1017"),
                    ("unbillable", "N0000"),

                )


            # Clinic Pre-admit
            elif self.instance.emrepisode.episode_type == "clinic_preadmit":
                self.fields["date"].widget = default_widgets["date"]
                self.fields["note"].widget = default_widgets["note"]
                self.fields["linked_docs"].widget = default_widgets["linked_docs"]

                #self.fields["billing_code"].widget = default_widgets["billing_code"]
                #self.fields["billing_code"].widget.choices = (
                #    NULL_CHOICE,
                #)


            # CLTS Pre-admit
            elif self.instance.emrepisode.episode_type == "clts_preadmit":
                self.fields["date"].widget = default_widgets["date"]
                self.fields["note"].widget = default_widgets["note"]
                self.fields["service"].widget = default_widgets["service"]
                self.fields["service_lookup"].widget = default_widgets["service_lookup"]
                self.fields["place_of_service"].widget = default_widgets["place_of_service"]
                self.fields["service_start"].widget = default_widgets["service_start"]
                self.fields["service_end"].widget = default_widgets["service_end"]
                self.fields["service_minutes"].widget = default_widgets["service_minutes"]
                self.fields["travel_minutes"].widget = default_widgets["travel_minutes"]
                self.fields["doc_minutes"].widget = default_widgets["doc_minutes"]
                self.fields["total_minutes"].widget = default_widgets["total_minutes"]
                self.fields["units"].widget = default_widgets["units"]
                self.fields["note"].widget = default_widgets["note"]
                self.fields["linked_docs"].widget = default_widgets["linked_docs"]

                self.fields["billing_code"].widget = default_widgets["billing_code"]
                self.fields["billing_code"].widget.choices = (
                    NULL_CHOICE,
                    ("T1016", "T1016"),
                    ("P9999", "P9999"),
                    ("unbillable", "N0000"),
                )


            # CLTS
            elif self.instance.emrepisode.episode_type == "clts":
                self.fields["date"].widget = default_widgets["date"]
                self.fields["case_note_type"].widget = default_widgets["case_note_type"]
                self.fields["case_note_type"].widget.choices = CLIENT_C.EMR_CLTS_CASE_NOTE_TYPE
                self.fields["case_note_type"].widget.attrs["selectshow"] = "annual|individuals_present;enrollment|individuals_present;six_month|individuals_present;"
                self.fields["individuals_present"].widget = default_widgets["individuals_present"]
                self.fields["ct_collateral"].widget = default_widgets["ct_collateral"]
                self.fields["ct_direct"].widget = default_widgets["ct_direct"]
                self.fields["ct_face"].widget = default_widgets["ct_face"]
                self.fields["ct_home"].widget = default_widgets["ct_home"]
                self.fields["ct_record"].widget = default_widgets["ct_record"]
                self.fields["ct_other"].widget = default_widgets["ct_other"]
                self.fields["service"].widget = default_widgets["service"]
                self.fields["service_lookup"].widget = default_widgets["service_lookup"]
                self.fields["place_of_service"].widget = default_widgets["place_of_service"]
                self.fields["service_start"].widget = default_widgets["service_start"]
                self.fields["service_end"].widget = default_widgets["service_end"]
                self.fields["service_minutes"].widget = default_widgets["service_minutes"]
                self.fields["travel_minutes"].widget = default_widgets["travel_minutes"]
                self.fields["doc_minutes"].widget = default_widgets["doc_minutes"]
                self.fields["total_minutes"].widget = default_widgets["total_minutes"]
                self.fields["units"].widget = default_widgets["units"]
                self.fields["note"].widget = default_widgets["note"]
                self.fields["linked_docs"].widget = default_widgets["linked_docs"]

                self.fields["billing_code"].widget = default_widgets["billing_code"]
                self.fields["billing_code"].widget.choices = (
                    NULL_CHOICE,
                    ("T1016", "T1016"),
                    ("P9999", "P9999"),
                    ("unbillable", "N0000"),
                )


            # Crisis
            elif self.instance.emrepisode.episode_type == "crisis":
                self.fields["date"].widget = default_widgets["date"]
                self.fields["credentials"].widget = default_widgets["credentials"]
                self.fields["diagnosis"].widget = default_widgets["diagnosis"]
                self.fields["diagnosis_lookup"].widget = default_widgets["diagnosis_lookup"]
                self.fields["place_of_service"].widget = default_widgets["place_of_service"]

                self.fields["service_start"].widget = default_widgets["service_start"]
                self.fields["service_end"].widget = default_widgets["service_end"]
                self.fields["service_minutes"].widget = default_widgets["service_minutes"]
                self.fields["travel_minutes"].widget = default_widgets["travel_minutes"]
                self.fields["doc_minutes"].widget = default_widgets["doc_minutes"]
                self.fields["total_minutes"].widget = default_widgets["total_minutes"]
                self.fields["units"].widget = default_widgets["units"]

                self.fields["billing_code"].widget = default_widgets["billing_code"]
                self.fields["billing_code"].widget.choices = (
                    ("T1016", "T1016"),
                    ("unbillable", "N0000"),
                )

                self.fields["crisis_type_of_contact"].widget = default_widgets["crisis_type_of_contact"]
                self.fields["crisis_type_of_contact"].widget.choices = (
                    NULL_CHOICE,
                    ("audio", "Audio-only"),
                    ("telehealth", "Telehealth"),
                    ("other", "Other"),
                )

                self.fields["note"].widget = default_widgets["note"]
                self.fields["linked_docs"].widget = default_widgets["linked_docs"]

                #import pdb; pdb.set_trace()



            # CST
            elif self.instance.emrepisode.episode_type == "cst":
                self.fields["date"].widget = default_widgets["date"]
                self.fields["service"].widget = default_widgets["service"]
                self.fields["service_lookup"].widget = default_widgets["service_lookup"]
                self.fields["place_of_service"].widget = default_widgets["place_of_service"]
                self.fields["service_start"].widget = default_widgets["service_start"]
                self.fields["service_end"].widget = default_widgets["service_end"]
                self.fields["service_minutes"].widget = default_widgets["service_minutes"]
                self.fields["travel_minutes"].widget = default_widgets["travel_minutes"]
                self.fields["doc_minutes"].widget = default_widgets["doc_minutes"]
                self.fields["total_minutes"].widget = default_widgets["total_minutes"]
                self.fields["units"].widget = default_widgets["units"]
                self.fields["billing_code"].widget = default_widgets["billing_code"]
                self.fields["note"].widget = default_widgets["note"]
                self.fields["linked_docs"].widget = default_widgets["linked_docs"]

                self.fields["billing_code"].widget.choices = (
                    NULL_CHOICE,
                    ("unbillable", "N0000"),
                )


            # RandI
            elif self.instance.emrepisode.episode_type == "randi":
                self.fields["date"].widget = default_widgets["date"]
                self.fields["case_note_type"].widget = default_widgets["case_note_type"]
                self.fields["case_note_type"].widget.choices = CLIENT_C.EMR_RANDI_CASE_NOTE_TYPE

                self.fields["note"].widget = default_widgets["note"]
                self.fields["linked_docs"].widget = default_widgets["linked_docs"]

                self.fields["billing_code"].widget.choices = (
                    ("unbillable", "N0000"),
                )




            # TCM
            elif self.instance.emrepisode.episode_type == "tcm":
                self.fields["date"].widget = default_widgets["date"]
                self.fields["service"].widget = default_widgets["service"]
                self.fields["service_lookup"].widget = default_widgets["service_lookup"]
                self.fields["place_of_service"].widget = default_widgets["place_of_service"]
                self.fields["service_start"].widget = default_widgets["service_start"]
                self.fields["service_end"].widget = default_widgets["service_end"]
                self.fields["service_minutes"].widget = default_widgets["service_minutes"]
                self.fields["travel_minutes"].widget = default_widgets["travel_minutes"]
                self.fields["doc_minutes"].widget = default_widgets["doc_minutes"]
                self.fields["total_minutes"].widget = default_widgets["total_minutes"]
                self.fields["units"].widget = default_widgets["units"]
                self.fields["billing_code"].widget = default_widgets["billing_code"]
                self.fields["note"].widget = default_widgets["note"]
                self.fields["linked_docs"].widget = default_widgets["linked_docs"]

                self.fields["billing_code"].widget.choices = (
                    NULL_CHOICE,
                    ("T1016", "T1016"),
                    ("unbillable", "N0000"),
                )






        # If the case note is attached to a journal entry, we can still
        # allow editing but fields related to the transaction cannot be edited:
        # units, service_start, service_end, service_minutes, travel_minutes
        # doc_minutes, total_minutes, billing_code, service, service_lookup,
        if self.instance and self.instance.is_open is True and self.instance.is_journaled():
            self.fields["units"].disabled = True
            self.fields["service_start"].disabled = True
            self.fields["service_end"].disabled = True
            self.fields["service_minutes"].disabled = True
            self.fields["travel_minutes"].disabled = True
            self.fields["doc_minutes"].disabled = True
            self.fields["total_minutes"].disabled = True
            self.fields["billing_code"].disabled = True
            self.fields["service"].disabled = True
            self.fields["service_lookup"].disabled = True
            self.fields["service_lookup"].required = False
            #import pdb; pdb.set_trace()


        if self.locked is True:
            # Always apply the lock if it's being sent as a parameter for the
            # form. Otherwise use the normal logic.
            render_form(self, locked=True, hide_all_optional=True)
        else:
            if self.instance and self.instance.is_open is True:
                render_form(self, locked=False, hide_all_optional=True)
            else:
                render_form(self, locked=True, hide_all_optional=True)

        '''
        # Because this is a form of a child model, we need to grab that
        # record to check for its specific fields. I'm sure there is a better
        # way to get this done, but I'm not sure how often this will happen
        # and if it's even worth looking deeper into
        #import pdb; pdb.set_trace()
        case_note_form = get_object_or_None(EmrCaseNoteForm, id=self.instance.id)

        if case_note_form and case_note_form.is_open is False:
            render_form(self, locked=True)
        else:
            render_form(self, locked=False)
        '''


    def clean(self):
        # Validation happens when the form is signed/finalized
        # validate_emr_form.py validate_emr_form_finalize()
        pass
        #self.cleaned_data = super(EmrCaseNoteFormForm, self).clean()
        #validate_emr_casenote_form(self)



    service_lookup = CharField(label="Service", required=False, widget=TextInput(attrs={
        'width': 50,
        "clear": "left",
        'class': 'lookup',
        'app': 'client',
        'mod': 'Service',
        'fields': 'name, spc_code, procedure_code',
        'order': 'code',
        'target': 'service',
        'autocomplete': 'off',
    }))


    diagnosis_lookup = CharField(label="Diagnosis", required=False, widget=TextInput(attrs={
        'width': 50,
        "clear": "left",
        'class': 'lookup',
        'app': 'client',
        'fields': 'name,code',
        'order': 'code',
        'mod': 'Diagnosis',
        'target': 'diagnosis',
        'autocomplete': 'off',
    }))



    class Meta:
        model = EmrCaseNoteForm        

        fields = [
            "is_open", 
            #"service",
            #"service_lookup",
            "diagnosis",
            #"diagnosis_lookup",
        ]        

        fields += emr_form_fields

        fields.insert(fields.index("crisis_type_of_contact"), "case_note_type")
        fields.insert(fields.index("crisis_type_of_contact") + 1, "service")
        fields.insert(fields.index("crisis_type_of_contact") + 1, "service_lookup")

        fields.insert(fields.index("service") + 1, "contact_type")
        fields.insert(fields.index("service") + 1, "ct_collateral")
        fields.insert(fields.index("ct_collateral") + 1, "ct_direct")
        fields.insert(fields.index("ct_direct") + 1, "ct_face")
        fields.insert(fields.index("ct_face") + 1, "ct_home")
        fields.insert(fields.index("ct_home") + 1, "ct_record")
        fields.insert(fields.index("ct_record") + 1, "ct_other")

        fields.insert(fields.index("ct_other") + 1, "see_case_file")
        fields.insert(fields.index("see_case_file") + 1, "rights_offered")

        fields.insert(fields.index("rights_offered"), "diagnosis_lookup")
        fields.insert(fields.index("diagnosis_lookup"), "individuals_present")


        widgets = {
            "is_open": HiddenInput(),
            "diagnosis": HiddenInput(),
            "service": HiddenInput(),
            "individuals_present": Textarea(attrs={"rows": 2, "width": 100}),
            "case_note_type": Select(attrs={"width": 20}),
            "ct_collateral": CheckboxInput(attrs={"clear": "left", "width": 16}),
            "ct_direct": CheckboxInput(attrs={"width": 16}),
            "ct_face": CheckboxInput(attrs={"width": 16}),
            "ct_home": CheckboxInput(attrs={"width": 16}),
            "ct_record": CheckboxInput(attrs={"width": 16}),
            "ct_other": CheckboxInput(attrs={"width": 16}),
            "see_case_file": CheckboxInput(attrs={"width": 16}),
            "rights_offered": CheckboxInput(attrs={"width": 16}),
        }

        widgets = {**widgets, **emr_form_widgets}








class EmrCstPpsMhAdmissionFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)

        if hasattr(self, "instance") and self.instance.is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)

    class Meta:
        model = EmrCstPpsMhAdmissionForm

        fields = [
            "service",
            "agency_of_resp",
            "provider_wpi_npi",
            "commit_stat",
            "employ_stat",
            "living_stat",
            "cjs_none",
            "cjs_probation",
            "cjs_arrests",
            "cjs_jail",
            "cjs_parole",
            "cjs_juv",
            "cjs_unknown",
            "num_arrests",
            "num_arrests_sixmonths",
        ]

        widgets = {
            "service": Select(attrs={"width": 25, "clear": "left"}),
            "agency_of_resp": TextInput(attrs={"width": 25}),
            "provider_wpi_npi": TextInput(attrs={"width": 25}),
            "commit_stat": Select(attrs={"width": 50}),
            "employ_stat": Select(attrs={"width": 50}),
            "living_stat": Select(attrs={"width": 50}),

            "cjs_none": CheckboxInput(attrs={"width": "33", "clear": "left"}),
            "cjs_probation": CheckboxInput(attrs={"width": "33"}),
            "cjs_arrests": CheckboxInput(attrs={"width": "33"}),
            "cjs_jail": CheckboxInput(attrs={"width": "33"}),
            "cjs_parole": CheckboxInput(attrs={"width": "33"}),
            "cjs_juv": CheckboxInput(attrs={"width": "33"}),
            "cjs_unknown": CheckboxInput(attrs={"width": "33"}),

            "num_arrests": NumberInput(attrs={"width": 50, "min": 0, "clear": "left"}),
            "num_arrests_sixmonths": NumberInput(attrs={"width": 50, "min": 0}),
        }






class EmrCstPpsMhDischargeFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)

        if hasattr(self, "instance") and self.instance.is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)

    class Meta:
        model = EmrCstPpsMhDischargeForm

        fields = [
            "commit_stat",
            "employ_stat",
            "living_stat",
            "cjs_none",
            "cjs_probation",
            "cjs_arrests",
            "cjs_jail",
            "cjs_parole",
            "cjs_juv",
            "cjs_unknown",
            "num_arrests",
            "num_arrests_sixmonths",
            "end_date",
            "pps_mh_spc_end_reason",
        ]

        widgets = {
            "commit_stat": Select(attrs={"width": 50}),
            "employ_stat": Select(attrs={"width": 50}),
            "living_stat": Select(attrs={"width": 50}),
            "cjs_none": CheckboxInput(attrs={"width": "33", "clear": "left"}),
            "cjs_probation": CheckboxInput(attrs={"width": "33"}),
            "cjs_arrests": CheckboxInput(attrs={"width": "33"}),
            "cjs_jail": CheckboxInput(attrs={"width": "33"}),
            "cjs_parole": CheckboxInput(attrs={"width": "33"}),
            "cjs_juv": CheckboxInput(attrs={"width": "33"}),
            "cjs_unknown": CheckboxInput(attrs={"width": "33"}),
            "num_arrests": NumberInput(attrs={"width": 50, "min": 0, "clear": "left"}),
            "num_arrests_sixmonths": NumberInput(attrs={"width": 50, "min": 0}),
            "end_date": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": 25,
                "class": "datepicker",
                "clear": "left",
            }),
            "pps_mh_spc_end_reason": Select(attrs={"width": 50}),

        }










class EmrTcmAssessmentFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)

        if hasattr(self, "instance") and self.instance.is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)


    class Meta:
        model = EmrTcmAssessmentForm

        fields = [
            # Assessment Description and Background Information
            "assessment_date",
            "assessment_type",
            "patient_rights_and_grievances",
            "presenting_problem",
            "financial",

            # Living Arrangements
            "living_arr",

            # Mental Health
            "mental_health",
            "psych_meds",
            "treat_service",
            "trauma_hist",
            "safety",
            "access_to_firearms",
            "access_to_firearms_dtl",
            "delusions",
            "family_mh_hist",
            "strengths",
            "risks",

            # Medical and Physical
            "phys_diags",
            "medications",
            "primary_doc",

            # Substance Use History
            "diags",
            "current_past_use",
            "od_history",
            "withdrawal_needs",
            "current_past_treat",

            # Social Relationships and Supports
            "current_rel_stat",
            "formal_service_providers",

            "contact_name",
            "contact_relationship",
            "contact_address",
            "contact_phone",
            "contact_phone_ext",
            "contact_name2",
            "contact_relationship2",
            "contact_address2",
            "contact_phone2",
            "contact_phone_ext2",
            "contact_name3",
            "contact_relationship3",
            "contact_address3",
            "contact_phone3",
            "contact_phone_ext3",
            "contact_name4",
            "contact_relationship4",
            "contact_address4",
            "contact_phone4",
            "contact_phone_ext4",
            "contact_name5",
            "contact_relationship5",
            "contact_address5",
            "contact_phone5",
            "contact_phone_ext5",
            "parent_guardian",
            "parent_guardian_name",
            "parent_guardian_address",
            "parent_guardian_phone",
            "parent_guardian_phone_ext",
            "parent_guardian_contacted",
            "parent_guardian_contacted_dtl",

            # Legal history
            "cjs_involvement",


            # Skills and Employment
            "voc_edu_status",
            "emp_status",
            "community_resources",
            "daily_living_activities",

            "linked_docs"
        ]

        # Add these if you need, otherwise explicitly set them above
        #fields = emr_form_fields + fields

        widgets = {
            # Assessment Description and Background Information
            "assessment_date": DateInput(format=settings.DATE_FORMAT, attrs={
                'width': "auto", 'class': 'datepicker'}),
            "assessment_type": Select(attrs={"width": 25}),
            "patient_rights_and_grievances": RadioSelect(attrs={"class": "radio-select", "width": 100}),
            "presenting_problem": Textarea(attrs={"rows": 2, "width": 100}),
            "financial": Textarea(attrs={"rows": 2, "width": 100}),

            # Living Arrangements
            "living_arr": Textarea(attrs={"rows": 2, "width": 100}),

            # Mental Health
            "mental_health": Textarea(attrs={"rows": 2, "width": 100}),
            "psych_meds": Textarea(attrs={"rows": 2, "width": 100}),
            "treat_service": Textarea(attrs={"rows": 2, "width": 100}),
            "trauma_hist": Textarea(attrs={"rows": 2, "width": 100}),
            "safety": Textarea(attrs={"rows": 2, "width": 100}),
            "access_to_firearms": Select(attrs={"width": 100, "selectshow": "yes,unknown|access_to_firearms_dtl"}),
            "access_to_firearms_dtl": Textarea(attrs={"rows": 2, "width": 100}),
            "delusions": Textarea(attrs={"rows": 2, "width": 100}),
            "family_mh_hist": Textarea(attrs={"rows": 2, "width": 100}),
            "strengths": Textarea(attrs={"rows": 2, "width": 100}),
            "risks": Textarea(attrs={"rows": 2, "width": 100}),

            # Medical and Physical
            "phys_diags": Textarea(attrs={"rows": 2, "width": 100}),
            "medications": Textarea(attrs={"rows": 2, "width": 100}),
            "primary_doc": Textarea(attrs={"rows": 2, "width": 100}),

            # Substance Use History
            "diags": Textarea(attrs={"rows": 2, "width": 100}),
            "current_past_use": Textarea(attrs={"rows": 2, "width": 100}),
            "od_history": Textarea(attrs={"rows": 2, "width": 100}),
            "withdrawal_needs": Textarea(attrs={"rows": 2, "width": 100}),
            "current_past_treat": Textarea(attrs={"rows": 2, "width": 100}),

            # Social Relationships and Supports
            "current_rel_stat": Textarea(attrs={"rows": 2, "width": 100}),
            "formal_service_providers": Textarea(attrs={"rows": 2, "width": 100}),

            "contact_name": TextInput(attrs={"width": 30, "expfield": "contact_name, contact_relationship, contact_address, contact_phone,contact_phone_ext", "expcount": 5}),
            "contact_relationship": TextInput(attrs={"width": 20}),
            "contact_address": TextInput(attrs={"width": 25}),
            "contact_phone": TextInput(attrs={"width": 20, "class": "phone"}),
            "contact_phone_ext": TextInput(attrs={"width": 5}),
            "contact_name2": TextInput(attrs={"width": 30}),
            "contact_relationship2": TextInput(attrs={"width": 20}),
            "contact_address2": TextInput(attrs={"width": 25}),
            "contact_phone2": TextInput(attrs={"width": 20, "class": "phone"}),
            "contact_phone_ext2": TextInput(attrs={"width": 5}),
            "contact_name3": TextInput(attrs={"width": 30}),
            "contact_relationship3": TextInput(attrs={"width": 20}),
            "contact_address3": TextInput(attrs={"width": 25}),
            "contact_phone3": TextInput(attrs={"width": 20, "class": "phone"}),
            "contact_phone_ext3": TextInput(attrs={"width": 5}),
            "contact_name4": TextInput(attrs={"width": 30}),
            "contact_relationship4": TextInput(attrs={"width": 20}),
            "contact_address4": TextInput(attrs={"width": 25}),
            "contact_phone4": TextInput(attrs={"width": 20, "class": "phone"}),
            "contact_phone_ext4": TextInput(attrs={"width": 5}),
            "contact_name5": TextInput(attrs={"width": 30}),
            "contact_relationship5": TextInput(attrs={"width": 20}),
            "contact_address5": TextInput(attrs={"width": 25}),
            "contact_phone5": TextInput(attrs={"width": 20, "class": "phone"}),
            "contact_phone_ext5": TextInput(attrs={"width": 5}),
            "parent_guardian": Select(attrs={"width": 100, "selectshow": "yes|parent_guardian_name, parent_guardian_address, parent_guardian_phone, parent_guardian_phone_ext"}),
            "parent_guardian_name": TextInput(attrs={"width": 40}),
            "parent_guardian_address": TextInput(attrs={"width": 40}),
            "parent_guardian_phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "parent_guardian_phone_ext": TextInput(attrs={"width": 5}),
            "parent_guardian_contacted": Select(attrs={"width": 100, "selectshow": "yes|parent_guardian_contacted_dtl"}),
            "parent_guardian_contacted_dtl": Textarea(attrs={"width": 100, "rows": 2}),

            # Legal History
            "cjs_involvement": Textarea(attrs={"rows": 2, "width": 100}),

            # Skills and Employment
            "voc_edu_status": Textarea(attrs={"rows": 2, "width": 100}),
            "emp_status": Textarea(attrs={"rows": 2, "width": 100}),
            "community_resources": Textarea(attrs={"rows": 2, "width": 100}),
            "daily_living_activities": Textarea(attrs={"rows": 2, "width": 100}),

            "linked_docs": SelectMultiple(attrs={
                "clear": "both",
                "width": "100%",
                "use_popup_lookup": "EmrForm",
                "popup_lookup_child_model": "EmrDoc",
            }),
        }





class EmrTcmPpsAodaAdmissionFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)

        if hasattr(self, "instance") and self.instance.is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)


    class Meta:
        model = EmrTcmPpsAodaAdmissionForm

        fields = [
            "county_of_residence",
            "service",
            "brief_service",
            "agency_of_resp",
            "provider_wpi_npi",
            "referral_source",
            "cd_collat",
            "agency_of_resp",
            "education",
            "num_arrests",
            "employ_stat",
            "living_stat",
            "group_attend",
            "pregnant",
            "substance_problem1",
            "route_admin1",
            "use_freq1",
            "age_first_use1",
            "substance_problem2",
            "route_admin2",
            "use_freq2",
            "age_first_use2",
            "substance_problem3",
            "route_admin3",
            "use_freq3",
            "age_first_use3",
            "provider_wpi_npi",
        ]



        widgets = {
            "county_of_residence": Select(attrs={"width": 25}),
            "service": Select(attrs={"width": 25, "clear": "left"}),
            "brief_service": Select(attrs={"width": 25}),
            "agency_of_resp": TextInput(attrs={"width": 25}),
            "provider_wpi_npi": TextInput(attrs={"width": 25}),
            "referral_source": Select(attrs={"width": 25}),
            "cd_collat": Select(attrs={"width": 25}),
            "education": Select(attrs={"width": 25}),
            "num_arrests": NumberInput(attrs={"width": 25}),
            "employ_stat": Select(attrs={"width": 25}),
            "living_stat": Select(attrs={"width": 25}),
            "group_attend": Select(attrs={"width": 25}),
            "pregnant": Select(attrs={"width": 25}),
            "substance_problem1": Select(attrs={"width": 25, "clear": "left"}),
            "route_admin1": Select(attrs={"width": 25}),
            "use_freq1": Select(attrs={"width": 25}),
            "age_first_use1": NumberInput(attrs={"width": 25}),
            "substance_problem2": Select(attrs={"width": 25}),
            "route_admin2": Select(attrs={"width": 25}),
            "use_freq2": Select(attrs={"width": 25}),
            "age_first_use2": NumberInput(attrs={"width": 25}),
            "substance_problem3": Select(attrs={"width": 25}),
            "route_admin3": Select(attrs={"width": 25}),
            "use_freq3": Select(attrs={"width": 25}),
            "age_first_use3": NumberInput(attrs={"width": 25}),
        }







class EmrTcmPpsAodaDischargeFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)

        if hasattr(self, "instance") and self.instance.is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)


    class Meta:
        model = EmrTcmPpsAodaDischargeForm

        fields = [
            "close_use_freq",
            "close_group_attend",
            "close_num_arrests",
            "close_living_stat",
            "close_employ_stat",
            "end_date",
            "pps_aoda_spc_end_reason",
        ]

        widgets = {
            "close_use_freq": Select(attrs={"width": 25}),
            "close_group_attend": Select(attrs={"width": 25}),
            "close_num_arrests": NumberInput(attrs={"width": 25}),
            "close_living_stat": Select(attrs={"width": 25}),
            "close_employ_stat": Select(attrs={"width": 25}),
            "end_date": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": 25,
                "class": "datepicker",
            }),
            "pps_aoda_spc_end_reason": Select(attrs={"width": 25}),
        }








class EmrTcmPpsMhAdmissionFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)

        if hasattr(self, "instance") and self.instance.is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)


    class Meta:
        model = EmrTcmPpsMhAdmissionForm

        fields = [
            "county_of_residence",
            "service",
            "agency_of_resp",
            "provider_wpi_npi",
            "brc_target_pop",
            "presenting_problem",
            "client_char",
            "commit_stat",
            "referral_source",
            "diagnosis",
            "diagnosis2",
            "diagnosis3",
            "diagnosis4",
            "diagnosis5",
        ]

        widgets = {
            "county_of_residence": Select(attrs={"width": 25}),
            "service": Select(attrs={"width": 25, "clear": "left"}),
            "agency_of_resp": TextInput(attrs={"width": 25}),
            "provider_wpi_npi": TextInput(attrs={"width": 25}),
            "brc_target_pop": Select(attrs={"width": 25}),
            "presenting_problem": Select(attrs={"clear": "left", "width": 50}),
            "client_char": Select(attrs={"width": 50}),
            "commit_stat": Select(attrs={"width": 50}),
            "referral_source": Select(attrs={"width": 50}),
            "diagnosis": Select(attrs={"width": 100, "expfield": "diagnosis", "expcount": 5}),
            "diagnosis2": Select(attrs={"width": 100, }),
            "diagnosis3": Select(attrs={"width": 100, }),
            "diagnosis4": Select(attrs={"width": 100, }),
            "diagnosis5": Select(attrs={"width": 100, }),
        }




class EmrTcmPpsMhDischargeFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)

        if hasattr(self, "instance") and self.instance.is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)


    class Meta:
        model = EmrTcmPpsMhDischargeForm

        fields = [
            "end_date",
            "pps_mh_spc_end_reason",
        ]

        widgets = {
            "end_date": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": 25,
                "class": "datepicker",
            }),
            "pps_mh_spc_end_reason": Select(attrs={"width": 25}),
        }





class EmrCrisisNwcNoteFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)

        if kwargs.get("instance") and self.instance.is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)

        self.fields["billing_code"].widget.choices = (
            NULL_CHOICE,
            ("H0030", "H0030"),
            ("H2011", "H2011"),
            ("T1016", "T1016"),
            ("unbillable", "N0000"),
        )

        self.fields["outcome_of_contact"].widget.choices = (
            NULL_CHOICE,
            ("info_only", "Information only"),
            ("other", "Other"),
        )


        self.fields["place_of_service"].widget.choices = CLIENT_C.PLACE_OF_SERVICE_CODE

        self.fields["diagnosis"].queryset = Diagnosis().get_crisis_icd10_codes()



    class Meta:
        model = EmrCrisisNwcNoteForm

        fields = [
            # Contact summary
            "date",
            "day_of_week",
            "part_of_existing",
            "alcohol_substance_factor",
            "law_inv",
            "law_dept",
            "law_dept_other",
            "outcome_of_contact",
            "outcome_other",
            "summary_highlight",
            "summary_highlight_dtl",
            "county_of_residence",
            "county_of_incident",
            # Billing
            "place_of_service",
            "diagnosis",
            "billing_code",
            "service_minutes",
            "doc_minutes",
            "total_minutes",
            "units",
            "staff_name1",
            "staff_creds1",
            "type_of_contact",
            "type_of_contact",
            # PPS
            "brc_target_pop",
            "presenting_problem",
            "char_code",
            "ref_source",
            "spc_end_reason",

            "linked_docs",
        ]

        widgets = {
            # contact summary
            "date": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": "auto",
                "class": "datepicker",
                "onchange": "setDayOfWeek('id_day_of_week');",
            }),
            "day_of_week": Select(attrs={"width": "auto"}),
            "part_of_existing": CheckboxInput(attrs={"width": "auto"}),
            "alcohol_substance_factor": Select(attrs={"clear": "left", "width": 50}),
            "law_inv": Select(attrs={"clear": "left", "width": 50, "selectshow": "yes|law_dept"}),
            "law_dept": Select(attrs={"width": 50, "selectshow": "other|law_dept_other;"}),
            "law_dept_other": TextInput(attrs={"width": 100}),
            "outcome_of_contact": Select(attrs={"clear": "left", "width": 50, "selectshow": "other|outcome_other"}),
            "outcome_other": Textarea(attrs={"width": 100, "rows": 2}),
            "summary_highlight": Select(attrs={"width": "auto", "clear": "left", "selectshow": "yes|summary_highlight_dtl"}),
            "summary_highlight_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            "county_of_residence": Select(attrs={"width": 50, "clear": "left"}),
            "county_of_incident": Select(attrs={"width": 50}),
            # Billing
            "place_of_service": Select(attrs={"width": 40}),
            "diagnosis": Select(attrs={"width": 100}),
            "billing_code": Select(attrs={"width": 50}),
            "service_minutes": NumberInput(attrs={"width": 16, "clear": "left"}),
            "doc_minutes": NumberInput(attrs={"width": 16, "min": 0}),
            "total_minutes": NumberInput(attrs={"width": 16, "min": 0}),
            "units": TextInput(attrs={"width": 16}),
            "staff_name1": TextInput(attrs={"clear": "left", "width": 50}),
            "staff_creds1": Select(attrs={"width": 25}),
            "type_of_contact": Select(attrs={"clear": "left", "width": 25}),
            # PPS
            "brc_target_pop": Select(attrs={"clear": "left", "width": 33}),
            "presenting_problem": Select(attrs={"width": 33}),
            "char_code": Select(attrs={"width": 33}),
            "ref_source": Select(attrs={"width": 33}),
            "spc_end_reason": Select(attrs={"width": 33}),




            "linked_docs": SelectMultiple(attrs={
                "clear": "both",
                "width": 100,
                "use_popup_lookup": "EmrForm",
                "popup_lookup_child_model": "EmrDoc",
            }),

        }


        labels = {
            "date": "Date",
            "diagnosis": "ICD10",
        }





class EmrCrisisResponsePlanReviewFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)

        if kwargs.get("instance") and self.instance.is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)

    class Meta:
        model = EmrCrisisResponsePlanReviewForm

        fields = [
            "date",
            "brief_summary",
            "safety",
            "protective_factors",
            "plan",


            "linked_docs"
        ]

        widgets = {
            "date": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": "auto", "class": "datepicker"}),
            "brief_summary": Textarea(attrs={"width": 100, "rows": 2}),
            "safety": Textarea(attrs={"width": 100, "rows": 2}),
            "protective_factors": Textarea(attrs={"width": 100, "rows": 2}),
            "plan": Textarea(attrs={"width": 100, "rows": 2}),


            "linked_docs": SelectMultiple(attrs={
                "clear": "both",
                "width": "100%",
                "use_popup_lookup": "EmrForm",
                "popup_lookup_child_model": "EmrDoc",
            }),
        }

        labels = {
            "date": "Date",
        }




class EmrJacksonCountyAssesResponseFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)

        self.fields["brc_target_pop"].disabled = True
        self.fields["billing_code"].widget.choices = (
            NULL_CHOICE,
            ("H0030", "H0030"),
            ("H2011", "H2011"),
            ("unbillable", "N0000"),
        )

        self.fields["rp_outcome"].widget.choices = (
            NULL_CHOICE,
            ("home", "Home individually"),
            ("phone_plan", "Telephone stabilization plan"),
            ("community_support", "Community support plan"),
            ("stab_place", "Crisis stabilization placement"),
            ("voluntary_inpatient", "Voluntary hospitalization"),
            ("voluntary_detox", "Voluntary detox"),
            ("ch_5145", "Ch. 51.45 Incapacitation Hold"),
            ("ch_55", "Ch. 55 Emergency protective placement"),
            ("ch_5115", "Ch. 51.15 Emergency detention"),
            ("ch_5145", "Ch. 51.45 Incapacitation Hold"),
            ("medical_primary", "Medical primary"),
            ("jail_with_precaution", "Jail - precautions recommended"),
            ("jail_no_precaution", "Jail - precautions NOT recommended"),
            ("mobile_worker_dispatched", "Mobile worker dispatched"),
            ("other", "Other"),
        )

        self.fields["place_of_service"].widget.choices = CLIENT_C.PLACE_OF_SERVICE_CODE

        self.fields["units"].disabled = True

        self.fields["diagnosis"].queryset = Diagnosis().get_crisis_icd10_codes()


        if kwargs.get("instance") and self.instance.is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)


    class Meta:
        model = EmrJacksonCountyAssesResponseForm

        fields = [
            "date",
            "part_of_existing",
            # Demographics
            "county_of_residence",
            "living_arr",
            "veteran_status",
            "veteran_status_dtl",
            "student_status",
            "school_name",
            "has_insurance",
            "has_insurance_dtl",
            # PPS
            "brc_target_pop",
            "presenting_problem",
            "char_code",
            "commitment_status",
            "ref_source",
            "spc_end_reason",
            # Billing
            "place_of_service",
            "diagnosis",
            "billing_code",
            "service_start",
            "service_end",
            "service_start2",
            "service_end2",
            "service_start3",
            "service_end3",
            "service_start4",
            "service_end4",
            "service_start5",
            "service_end5",
            "service_start6",
            "service_end6",
            "service_start7",
            "service_end7",
            "service_start8",
            "service_end8",
            "service_start9",
            "service_end9",
            "service_start10",
            "service_end10",
            "service_minutes",
            "travel_minutes",
            "doc_minutes",
            "total_minutes",
            "units",
            "staff_name1",
            "staff_creds1",
            "staff_name2",
            "staff_creds2",
            "type_of_contact",
            "caller_name",
            "caller_phone1",
            "caller_phone1_ext",
            "caller_phone2",
            "caller_phone2_ext",
            "caller_agency_or_rel",
            # Supports
            "contact_name",
            "contact_relationship",
            "contact_address",
            "contact_phone",
            "contact_phone_ext",
            "contact_name2",
            "contact_relationship2",
            "contact_address2",
            "contact_phone2",
            "contact_phone_ext2",
            "contact_name3",
            "contact_relationship3",
            "contact_address3",
            "contact_phone3",
            "contact_phone_ext3",
            "contact_name4",
            "contact_relationship4",
            "contact_address4",
            "contact_phone4",
            "contact_phone_ext4",
            "contact_name5",
            "contact_relationship5",
            "contact_address5",
            "contact_phone5",
            "contact_phone_ext5",
            "parent_guardian",
            "parent_guardian_name",
            "parent_guardian_address",
            "parent_guardian_phone",
            "parent_guardian_phone_ext",
            "parent_guardian_contacted",
            "parent_guardian_contacted_dtl",
            # Rights and grievance
            "rights_informed",
            # Mandated reporting
            "mandated_reporting",
            "mandated_reporting_dtl",
            # Assessment - Document clearly who provided the information below
            "was_interviewed",
            "not_interviewed_reason",
            "law_inv",
            "law_dept",
            "law_dept_other",
            "previous_crisis_contacts",
            "previous_crisis_contacts_dtl",
            "alcohol_sa_factor",
            "under_influence",
            "pbt",
            "pbt_time",
            "bac",
            "bac_time",
            "under_influence_dtl",
            "sub_use_hist",
            "withdrawal_needs",
            "od_history",
            "self_reported_diag",
            "service_received",
            "service_received_dtl",
            "followup_apt",
            "cm",
            "cm_dtl",
            "cm_as_prescribed",
            "cm_agency",
            "cm_name",
            "cm_phone",
            "cm_phone_ext",
            "prior_hosp_hist",
            "prior_hosp_hist_dtl",

            "legal_stat_hist",
            "legal_stat_hist_dtl",

            # Columbia-Suicide Severity Rating Scale
            "cs_scale_wish_dead",
            "cs_scale_wish_dead_dtl",
            "cs_scale_thoughts",
            "cs_scale_thoughts_dtl",
            # Subsection:
            #   If YES to 2, ask questions 3, 4, 5 and 6.  If NO to 2, go directly to question 6
            "cs_scale_how_kill",
            "cs_scale_how_kill_dtl",
            "cs_scale_intent",
            "cs_scale_intent_dtl",
            "cs_scale_details",
            "cs_scale_details_intent",
            "cs_scale_details_dtl",
            "cs_scale_actions",
            "cs_scale_actions_time",
            "cs_scale_actions_dtl",
            # HOMICIDALITY / SELF-INJURY / LETHAL MEANS section
            "cs_scale_thoughts_someone_else",
            "cs_scale_thoughts_someone_else_prepare",
            "cs_scale_thoughts_someone_else_dtl",
            "si_behavior",
            "si_how_often",
            "si_method",
            "si_intent",
            "si_access",
            "si_access_dtl",
            # Mental Status section
            #   Orientation
            "ori_alert",
            "ori_not_ori",
            "ori_person",
            "ori_place",
            "ori_time",
            "ori_situation",
            #   Speech
            "speech_rapid",
            "speech_slurred",
            "speech_delayed",
            "speech_normal",
            #   Though Content/Processes
            "thought_delusions",
            "thought_disorganized",
            "thought_hallucinations",
            "thought_paranoid",
            "thought_unknown",
            "thought_organized",
            #   Mood
            "mood_depressed",
            "mood_elevated",
            "mood_anxious",
            "mood_guarded",
            "mood_irritable",
            "mood_normal",
            #   Behavior
            "behave_cooperative",
            "behave_impulsive",
            "behave_aggressive",
            "behave_uncooperative",
            "behave_withdrawn",
            "behave_agitated",
            #   Intellectual functioning
            "intel_below_avg",
            "intel_above_avg",
            "intel_avg",
            "intel_not_noted",
            #   Insight
            "insight_good",
            "insight_fair",
            "insight_poor",
            #   Judgement
            "judgement_good",
            "judgement_fair",
            "judgement_poor",
            # Summary of Assessment section
            #   Initial caller concerns, presenting needs of individual, collateral
            #   information specific to current crisis, desired outcome from all parties.
            "summary_of_assessment",
            "risk_factors",
            "protective_factors",
            "coping_skills",
            "supports",
            # Response Plan
            "rp_outcome",
            "rp_outcome_facility",
            "rp_con_complete",
            "rp_plan",
            "rp_follow_up",
            "rp_coping_skills",
            "rp_supports",
            "rp_nwc_contact_provided",
            "resource_988_provided",
            "other_resource_provided",
            "other_resource_provided_dtl",

            # Authorization for emergency detention Provided to: section
            "ed_provided",
            "ed_title",
            "ed_name",
            "ed_dept",


            #"county_crisis_line",
            #"county_department",
            #"home_indv_exp1",
            #"home_indv_exp2",
            #"telephone_stab_exp",
            #"telephone_stab_exp2",
            #"csp_exp",
            #"csp_exp2",
            #"crisis_stab_exp",
            #"crisis_stab_exp2",
            #"vol_detox_exp",
            #"vol_detox_exp2",
            #"vol_hospital_exp",
            #"vol_hospital_exp2",
            #"ch5145_exp",
            #"ch5145_exp2",
            #"ch55_exp",
            #"ch55_exp2",
            #"ch5115_exp",
            #"ch5115_exp2",


            # Additional Items Addressed in Response Plan
            "rec_secure_fire",
            "rec_secure_fire_comto",
            "rec_secure_fire_dtl",
            "rec_secure_other",
            "rec_secure_other_comto",
            "rec_secure_other_dtl",
            "rec_other",
            "rec_other_comto",
            "rec_other_dtl",
            #"loc_placement",
            #"loc_admin_conf",
            #"loc_admin_conf_plan",
            #"transport",
            "linked_docs",
        ]

        widgets = {
            "date": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }),
            "part_of_existing": CheckboxInput(attrs={"width": "auto"}),

            # Demographics
            "county_of_residence": Select(attrs={"width": 50}),
            "living_arr": Select(attrs={"clear": "left", "width": 50}),
            "veteran_status": Select(attrs={"clear": "left", "width": 50, "selectshow": "yes|veteran_status_dtl"}),
            "veteran_status_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            "student_status": Select(attrs={"clear": "left", "width": 50, "selectshow": "yes|school_name"}),
            "school_name": TextInput(attrs={"width": 100}),
            "has_insurance": Select(attrs={"clear": "left", "width": 50, "selectshow": "yes|has_insurance_dtl"}),
            "has_insurance_dtl": Textarea(attrs={"width": 100, "rows": 2}),




            # PPS
            "brc_target_pop": Select(attrs={"width": 25}),
            "presenting_problem": Select(attrs={"width": 25}),
            "char_code": Select(attrs={"width": 25}),
            "commitment_status": Select(attrs={"width": 25}),
            "ref_source": Select(attrs={"width": 25}),
            "spc_end_reason": Select(attrs={"width": 25}),


            # Billing
            "place_of_service": Select(attrs={"width": 50}),
            "diagnosis": Select(attrs={"width": 50}),
            "billing_code": Select(attrs={"width": 50}),
            "service_start": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time", "expfield": "service_start, service_end", "expcount": 10}),
            "service_end": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start2": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end2": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start3": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end3": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start4": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end4": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start5": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end5": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start6": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end6": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start7": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end7": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start8": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end8": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start9": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end9": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start10": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end10": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),

            "service_minutes": NumberInput(attrs={"clear": "left", "width": 16}),
            "travel_minutes": NumberInput(attrs={"width": 16}),
            "doc_minutes": NumberInput(attrs={"width": 16}),
            "total_minutes": NumberInput(attrs={"width": 16}),
            "units": NumberInput(attrs={"width": 16}),
            "staff_name1": TextInput(attrs={"clear": "left", "width": 50}),
            "staff_creds1": Select(attrs={"width": 25}),
            "staff_name2": TextInput(attrs={"clear": "left", "width": 50}),
            "staff_creds2": Select(attrs={"width": 25}),
            "type_of_contact": Select(attrs={"width": 50, "selectshow": "mobile,mobile_team,audio,telehealth|caller_agency_or_rel, caller_name, caller_phone1,caller_phone1_ext, caller_phone2,caller_phone2_ext"}),
            "caller_name": TextInput(attrs={"width": 20, "clear": "left"}),
            "caller_phone1": TextInput(attrs={"width": 20, "class": "phone"}),
            "caller_phone1_ext": TextInput(attrs={"width": 10}),
            "caller_phone2": TextInput(attrs={"width": 20, "class": "phone"}),
            "caller_phone2_ext": TextInput(attrs={"width": 10}),
            "caller_agency_or_rel": TextInput(attrs={"width": 20}),
            # Supports
            "contact_name": TextInput(attrs={"width": 30, "expfield": "contact_name, contact_relationship, contact_address, contact_phone,contact_phone_ext", "expcount": 5}),
            "contact_relationship": TextInput(attrs={"width": 20}),
            "contact_address": TextInput(attrs={"width": 25}),
            "contact_phone": TextInput(attrs={"width": 20, "class": "phone"}),
            "contact_phone_ext": TextInput(attrs={"width": 5}),
            "contact_name2": TextInput(attrs={"width": 30}),
            "contact_relationship2": TextInput(attrs={"width": 20}),
            "contact_address2": TextInput(attrs={"width": 25}),
            "contact_phone2": TextInput(attrs={"width": 20, "class": "phone"}),
            "contact_phone_ext2": TextInput(attrs={"width": 5}),
            "contact_name3": TextInput(attrs={"width": 30}),
            "contact_relationship3": TextInput(attrs={"width": 20}),
            "contact_address3": TextInput(attrs={"width": 25}),
            "contact_phone3": TextInput(attrs={"width": 20, "class": "phone"}),
            "contact_phone_ext3": TextInput(attrs={"width": 5}),
            "contact_name4": TextInput(attrs={"width": 30}),
            "contact_relationship4": TextInput(attrs={"width": 20}),
            "contact_address4": TextInput(attrs={"width": 25}),
            "contact_phone4": TextInput(attrs={"width": 20, "class": "phone"}),
            "contact_phone_ext4": TextInput(attrs={"width": 5}),
            "contact_name5": TextInput(attrs={"width": 30}),
            "contact_relationship5": TextInput(attrs={"width": 20}),
            "contact_address5": TextInput(attrs={"width": 25}),
            "contact_phone5": TextInput(attrs={"width": 20, "class": "phone"}),
            "contact_phone_ext5": TextInput(attrs={"width": 5}),
            "parent_guardian": Select(attrs={"width": 100, "selectshow": "yes|parent_guardian_name, parent_guardian_address, parent_guardian_phone, parent_guardian_phone_ext"}),
            "parent_guardian_name": TextInput(attrs={"width": 40}),
            "parent_guardian_address": TextInput(attrs={"width": 40}),
            "parent_guardian_phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "parent_guardian_phone_ext": TextInput(attrs={"width": 5}),
            "parent_guardian_contacted": Select(attrs={"width": 100, "selectshow": "yes|parent_guardian_contacted_dtl"}),
            "parent_guardian_contacted_dtl": Textarea(attrs={"width": 100, "rows": 2}),

            # Rights and Grievance
            "rights_informed": Select(attrs={"width": 50}),

            # Mandated Reporting
            "mandated_reporting": Select(attrs={"clear": "left", "width": 40, "selectshow": "child_protection_notified|mandated_reporting_dtl;adult_protection_notified|mandated_reporting_dtl;duty_to_warn|mandated_reporting_dtl;"}),
            "mandated_reporting_dtl": TextInput(attrs={"width": 60}),

            # Assessment
            "was_interviewed": Select(attrs={"clear": "left", "width": 30, "selectshow": "no|not_interviewed_reason"}),
            "not_interviewed_reason": TextInput(attrs={"width": 70}),
            "law_inv": Select(attrs={"clear": "left", "width": 50, "selectshow": "yes|law_dept"}),
            "law_dept": Select(attrs={"width": 50, "selectshow": "other|law_dept_other;"}),
            "law_dept_other": TextInput(attrs={"width": 100}),
            "previous_crisis_contacts": Select(attrs={"clear": "left", "width": 25, "selectshow": "yes|previous_crisis_contacts_dtl"}),
            "previous_crisis_contacts_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            "alcohol_sa_factor": Select(attrs={"clear": "left", "width": 33}),
            "under_influence": Select(attrs={
                "clear": "left",
                "width": 50,
                "selectshow": "yes|under_influence_dtl,pbt,pbt_time,bac,bac_time;unknown|under_influence_dtl"
            }),
            "pbt": TextInput(attrs={"width": 10}),
            "pbt_time": TextInput(attrs={"width": 15}),
            "bac": TextInput(attrs={"width": 10}),
            "bac_time": TextInput(attrs={"width": 15}),
            "under_influence_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            "sub_use_hist": Textarea(attrs={"width": 100, "rows": 2}),
            "withdrawal_needs": Textarea(attrs={"width": 100, "rows": 2}),
            "od_history": Textarea(attrs={"width": 100, "rows": 2}),
            "self_reported_diag": Textarea(attrs={"width": 100, "rows": 2}),
            "service_received": Select(attrs={
                "width": 100,
                "selectshow": "yes|service_received_dtl"
            }),
            "service_received_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            "followup_apt": Textarea(attrs={"width": 100, "rows": 2}),
            "cm": Select(attrs={
                "width": 100,
                "selectshow": "yes|cm_dtl,cm_as_prescribed,cm_agency,cm_name,cm_phone,cm_phone_ext"
            }),
            "cm_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            "cm_as_prescribed": Select(attrs={"width": 25}),
            "cm_agency": TextInput(attrs={"width": 25}),
            "cm_name": TextInput(attrs={"width": 30}),
            "cm_phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "cm_phone_ext": TextInput(attrs={"width": 5}),
            "prior_hosp_hist": Select(attrs={"width": 33, "selectshow": "yes|prior_hosp_hist_dtl"}),
            "prior_hosp_hist_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            "legal_stat_hist": Select(attrs={"clear": "left", "width": 50, "selectshow": "yes|legal_stat_hist_dtl"}),
            "legal_stat_hist_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            # Columbia-Suicide Severity Rating Scale
            "cs_scale_wish_dead": Select(attrs={"width": 100, "selectshow": "yes|cs_scale_wish_dead_dtl;refused_unable|cs_scale_wish_dead_dtl"}),
            "cs_scale_wish_dead_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            "cs_scale_thoughts": Select(attrs={"width": 100, "selectshow": "yes,refused_unable|cs_scale_thoughts_dtl;yes|cs_scale_how_kill,cs_scale_thoughts_dtl,cs_scale_intent,cs_scale_details"}),
            "cs_scale_thoughts_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            # Subsection
            #   If YES to 2, ask questions 3, 4, 5 and 6.  If NO to 2, go directly to question 6
            "cs_scale_how_kill": Select(attrs={"width": 100, "selectshow": "yes|cs_scale_how_kill_dtl;refused_unable|cs_scale_how_kill_dtl"}),
            "cs_scale_how_kill_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            "cs_scale_intent": Select(attrs={"width": 100, "selectshow": "yes|cs_scale_intent_dtl;refused_unable|cs_scale_intent_dtl"}),
            "cs_scale_intent_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            "cs_scale_details": Select(attrs={"clear": "left", "width": 50, "selectshow": "yes|cs_scale_details_dtl, cs_scale_details_intent"}),
            "cs_scale_details_intent": Select(attrs={"width": 50}),
            "cs_scale_details_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            "cs_scale_actions": Select(attrs={"clear": "left", "width": 60, "selectshow": "yes|cs_scale_actions_time, cs_scale_actions_dtl"}),
            "cs_scale_actions_time": Select(attrs={"width": 40}),
            "cs_scale_actions_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            # HOMICIDALITY / SELF-INJURY / LETHAL MEANS section
            "cs_scale_thoughts_someone_else": Select(attrs={"clear": "left", "width": 50, "selectshow": "yes|cs_scale_thoughts_someone_else_prepare,cs_scale_thoughts_someone_else_dtl"}),
            "cs_scale_thoughts_someone_else_prepare": Select(attrs={"width": 50}),
            "cs_scale_thoughts_someone_else_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            "si_behavior": Select(attrs={"width": 100, "selectshow": "yes|si_how_often,si_method,si_intent"}),
            "si_how_often": Textarea(attrs={"width": 100, "rows": 2}),
            "si_method": Textarea(attrs={"width": 100, "rows": 2}),
            "si_intent": Textarea(attrs={"width": 100, "rows": 2}),
            "si_access": Select(attrs={"width": 100, "selectshow": "yes|si_access_dtl;other|si_access_dtl"}),
            "si_access_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            # Mental Status section
            #   Orientation
            "ori_alert": CheckboxInput(attrs={"width": 25}),
            "ori_not_ori": CheckboxInput(attrs={"width": 25}),
            "ori_person": CheckboxInput(attrs={"width": 25}),
            "ori_place": CheckboxInput(attrs={"width": 25}),
            "ori_time": CheckboxInput(attrs={"width": 25}),
            "ori_situation": CheckboxInput(attrs={"width": 25}),
            #   Speech
            "speech_rapid": CheckboxInput(attrs={"width": 25}),
            "speech_slurred": CheckboxInput(attrs={"width": 25}),
            "speech_delayed": CheckboxInput(attrs={"width": 25}),
            "speech_normal": CheckboxInput(attrs={"width": 25}),
            #   Though Content/Processes
            "thought_delusions": CheckboxInput(attrs={"width": 25}),
            "thought_disorganized": CheckboxInput(attrs={"width": 25}),
            "thought_hallucinations": CheckboxInput(attrs={"width": 25}),
            "thought_paranoid": CheckboxInput(attrs={"width": 25}),
            "thought_unknown": CheckboxInput(attrs={"width": 25}),
            "thought_organized": CheckboxInput(attrs={"width": 25}),
            #   Mood
            "mood_depressed": CheckboxInput(attrs={"width": 25}),
            "mood_elevated": CheckboxInput(attrs={"width": 25}),
            "mood_anxious": CheckboxInput(attrs={"width": 25}),
            "mood_guarded": CheckboxInput(attrs={"width": 25}),
            "mood_irritable": CheckboxInput(attrs={"width": 25}),
            "mood_normal": CheckboxInput(attrs={"width": 25}),
            #   Behavior
            "behave_cooperative": CheckboxInput(attrs={"width": 25}),
            "behave_impulsive": CheckboxInput(attrs={"width": 25}),
            "behave_aggressive": CheckboxInput(attrs={"width": 25}),
            "behave_uncooperative": CheckboxInput(attrs={"width": 25}),
            "behave_withdrawn": CheckboxInput(attrs={"width": 25}),
            "behave_agitated": CheckboxInput(attrs={"width": 25}),
            #   Intellectual functioning
            "intel_below_avg": CheckboxInput(attrs={"width": 25}),
            "intel_above_avg": CheckboxInput(attrs={"width": 25}),
            "intel_avg": CheckboxInput(attrs={"width": 25}),
            "intel_not_noted": CheckboxInput(attrs={"width": 25}),
            #   Insight
            "insight_good": CheckboxInput(attrs={"width": 25}),
            "insight_fair": CheckboxInput(attrs={"width": 25}),
            "insight_poor": CheckboxInput(attrs={"width": 25}),
            #   Judgement
            "judgement_good": CheckboxInput(attrs={"clear": "left", "width": 25}),
            "judgement_fair": CheckboxInput(attrs={"width": 25}),
            "judgement_poor": CheckboxInput(attrs={"width": 25}),
            # Summary of Assessment section
            #   Initial caller concerns, presenting needs of individual, collateral
            #   information specific to current crisis, desired outcome from all parties
            "summary_of_assessment": Textarea(attrs={"width": 100, "rows": 3}),
            "risk_factors": Textarea(attrs={"width": 100, "rows": 2}),
            "protective_factors": Textarea(attrs={"width": 100, "rows": 2}),
            "coping_skills": Textarea(attrs={"width": 100, "rows": 2}),
            "supports": Textarea(attrs={"width": 100, "rows": 2}),
            # Response Plan
            "rp_outcome": Select(attrs={"width": 100, "selectshow": "medical_primary,adult_crisis_bed,youth_crisis_bed,voluntary_inpatient,voluntary_detox,5145_ncap,55_emergency,5115_emergency,juvenile_detention|rp_outcome_facility;5115_emergency|rp_con_complete,rp_outcome_facility, ed_provided;"}),
            "rp_outcome_facility": TextInput(attrs={"width": 100}),
            "rp_con_complete": Select(attrs={"width": 100}),
            "rp_plan": Textarea(attrs={"width": 100, "rows": 2}),
            "rp_follow_up": Textarea(attrs={"width": 100, "rows": 2}),
            "rp_coping_skills": Textarea(attrs={"width": 100, "rows": 2}),
            "rp_supports": Textarea(attrs={"width": 100, "rows": 2}),
            "rp_nwc_contact_provided": CheckboxInput(attrs={"width": 100}),
            "resource_988_provided": CheckboxInput(attrs={"width": 100}),
            "other_resource_provided": CheckboxInput(attrs={"width": 100, "chkrelated": True}),
            "other_resource_provided_dtl": Textarea(attrs={"width": 100, "rows": 2}),


            #"county_crisis_line": TextInput(attrs={"width": 20, "class": "phone"}),
            #"county_department": Select(attrs={"width": 100}),
            #"home_indv_exp1": Select(attrs={"width": 30, "clear": "left", "selectshow": "not_utl|home_indv_exp2"}),
            #"home_indv_exp2": Select(attrs={"width": 70}),
            #"telephone_stab_exp": Select(attrs={"width": 30, "clear": "left", "selectshow": "not_utl|telephone_stab_exp2"}),
            #"telephone_stab_exp2": Select(attrs={"width": 70}),
            #"csp_exp": Select(attrs={"width": 30, "clear": "left", "selectshow": "not_utl|csp_exp2"}),
            #"csp_exp2": Select(attrs={"width": 70}),
            #"crisis_stab_exp": Select(attrs={"width": 30, "clear": "left", "selectshow": "not_utl|crisis_stab_exp2"}),
            #"crisis_stab_exp2": Select(attrs={"width": 70}),
            #"vol_detox_exp": Select(attrs={"width": 30, "clear": "left", "selectshow": "not_utl|vol_detox_exp2"}),
            #"vol_detox_exp2": Select(attrs={"width": 70}),
            #"vol_hospital_exp": Select(attrs={"width": 30, "clear": "left", "selectshow": "not_utl|vol_hospital_exp2"}),
            #"vol_hospital_exp2": Select(attrs={"width": 70}),
            #"ch5145_exp": Select(attrs={"width": 30, "clear": "left", "selectshow": "not_utl|ch5145_exp2"}),
            #"ch5145_exp2": Select(attrs={"width": 70}),
            #"ch55_exp": Select(attrs={"width": 30, "clear": "left", "selectshow": "not_utl|ch55_exp2"}),
            #"ch55_exp2": Select(attrs={"width": 70}),
            #"ch5115_exp": Select(attrs={"width": 30, "clear": "left", "selectshow": "not_utl|ch5115_exp2"}),
            #"ch5115_exp2": Select(attrs={"width": 70}),

            # Authorization for emergency detention Provided to: section
            "ed_provided": Select(attrs={"clear": "left", "width": 50, "selectshow": "yes|ed_title, ed_name, ed_dept"}),
            "ed_title": TextInput(attrs={"clear": "left", "width": 20}),
            "ed_name": TextInput(attrs={"width": 40}),
            "ed_dept": TextInput(attrs={"width": 40}),

            # Additional Items Addressed in Response Plan
            "rec_secure_fire": CheckboxInput(attrs={"clear": "left", "width": 50, "chkrelated": "rec_secure_fire_comto,rec_secure_fire_dtl"}),
            "rec_secure_fire_comto": TextInput(attrs={"width": 50}),
            "rec_secure_fire_dtl": Textarea(attrs={"width": 100, "rows": 2}),

            "rec_secure_other": CheckboxInput(attrs={"clear": "left", "width": 50, "chkrelated": "rec_secure_other_dtl, rec_secure_other_comto"}),
            "rec_secure_other_comto": TextInput(attrs={"width": 50}),
            "rec_secure_other_dtl": Textarea(attrs={"width": 100, "rows": 2}),

            "rec_other": CheckboxInput(attrs={"clear": "left", "width": 50, "chkrelated": "rec_other_dtl, rec_other_comto"}),
            "rec_other_comto": TextInput(attrs={"width": 50}),
            "rec_other_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            #"loc_placement": TextInput(attrs={"width": 100}),
            #"loc_admin_conf": CheckboxInput(attrs={"width": 100}),
            #"loc_admin_conf_plan": CheckboxInput(attrs={"width": 100}),
            #"transport": TextInput(attrs={"width": 100}),

            "linked_docs": SelectMultiple(attrs={
                "clear": "both",
                "width": "100%",
                "use_popup_lookup": "EmrForm",
                "popup_lookup_child_model": "EmrDoc",
            }),
        }

        labels = {
            "date": "Date",
            "diagnosis": "ICD10"
        }








class EmrNwcAssesResponseFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)

        self.fields["date"].label = "Date"
        self.fields["diagnosis"].label = "ICD10 Code"

        self.fields["diagnosis"].queryset = Diagnosis().get_crisis_icd10_codes()

        self.fields["units"].disabled = True


        self.fields["billing_code"].widget.choices = (
            NULL_CHOICE,
            ("H0030", "H0030"),
            ("H2011", "H2011"),
            ("unbillable", "N0000"),
        )

        self.fields["outcome_of_contact"].widget.choices = (
            NULL_CHOICE,
            ("home", "Home individually"),
            ("phone_plan", "Telephone stabilization plan"),
            ("community_support", "Community support plan"),
            ("stab_place", "Crisis stabilization placement"),
            ("voluntary_inpatient", "Voluntary hospitalization"),
            ("voluntary_detox", "Voluntary detox"),
            ("ch_5145", "Chapter 51.45 Incapacitation Hold"),
            ("ch_55", "Chapter 55 Protective Placement"),
            ("ch_5115", "Chapter 51.15 Emergency Detention"),
            ("medical_primary", "Medical Primary"),
            ("jail_with_precaution", "Jail-Precautions recommended"),
            ("jail_no_precaution", "Jail-Precautions NOT recommended"),
            ("mobile_worker_dispatched", "Mobile Worker Dispatched"),
            ("other", "Other")
        )



        if kwargs.get("instance") and self.instance.is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)



    class Meta:
        model = EmrNwcAssesResponseForm

        fields = [
            "date",
            "part_of_existing",
            "alcohol_substance_factor",
            "law_inv",
            "law_dept",
            "law_dept_other",
            "outcome_of_contact",
            "outcome_other",
            "mobile_worker_name",
            "hospital_location",
            "county_of_residence",
            "county_of_incident",
            "day_of_week",
            "rights_informed",

            "place_of_service",
            "diagnosis",
            "service_start",
            "service_end",
            "service_start2",
            "service_end2",
            "service_start3",
            "service_end3",
            "service_start4",
            "service_end4",
            "service_start5",
            "service_end5",
            "service_start6",
            "service_end6",
            "service_start7",
            "service_end7",
            "service_start8",
            "service_end8",
            "service_start9",
            "service_end9",
            "service_start10",
            "service_end10",
            "service_minutes",
            "travel_minutes",
            "doc_minutes",
            "total_minutes",
            "units",
            "billing_code",
            "staff_name1",
            "staff_creds1",
            "staff_name2",
            "staff_creds2",
            "type_of_contact",

            # PPS
            "brc_target_pop",
            "presenting_problem",
            "char_code",
            "legal_status",
            "ref_source",
            "spc_end_reason",

            "note",
            "linked_docs",
        ]


        widgets = {
            # Contact Summary Details
            "date": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
                "onchange": "setDayOfWeek('id_day_of_week');",
            }),
            "part_of_existing": CheckboxInput(attrs={"width": "auto"}),

            "alcohol_substance_factor": Select(attrs={"clear": "left", "width": 50}),
            "law_inv": Select(attrs={"clear": "left", "width": 50, "selectshow": "yes|law_dept"}),
            "law_dept": Select(attrs={"width": 50, "selectshow": "other|law_dept_other;"}),
            "law_dept_other": TextInput(attrs={"width": 100}),
            "outcome_of_contact": Select(attrs={"clear": "left", "width": 50, "selectshow": "other|outcome_other; mobile_worker_dispatched|mobile_worker_name"}),
            "mobile_worker_name": TextInput(attrs={"width": 50}),
            "outcome_other": TextInput(attrs={"width": 100}),
            "hospital_location": TextInput(attrs={"width": 100}),
            "county_of_residence": Select(attrs={"width": 50, "clear": "left"}),
            "county_of_incident": Select(attrs={"width": 50}),
            "day_of_week": Select(attrs={"width": 20}),
            "rights_informed": Select(attrs={"width": 100}),

            # Billing
            "place_of_service": Select(attrs={"width": 40}),
            "diagnosis": Select(attrs={"width": 100}),
            "billing_code": Select(attrs={"clear": "left", "width": 16}),

            "service_start": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time", "expfield": "service_start, service_end", "expcount": 10}),
            "service_end": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start2": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end2": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start3": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end3": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start4": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end4": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start5": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end5": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start6": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end6": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start7": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end7": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start8": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end8": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start9": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end9": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start10": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end10": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),

            "service_minutes": NumberInput(attrs={"clear": "left", "width": 20}),
            "travel_minutes": NumberInput(attrs={"width": 20}),
            "doc_minutes": NumberInput(attrs={"width": 20}),
            "total_minutes": NumberInput(attrs={"width": 20}),
            "units": NumberInput(attrs={"width": 20}),
            "staff_name1": TextInput(attrs={"clear": "left", "width": 50}),
            "staff_creds1": Select(attrs={"width": 25}),
            "staff_name2": TextInput(attrs={"clear": "left", "width": 50}),
            "staff_creds2": Select(attrs={"width": 25}),
            "type_of_contact": Select(attrs={"clear": "left", "width": 20}),

            # PPS
            "brc_target_pop": Select(attrs={"width": 25}),
            "presenting_problem": Select(attrs={"width": 25}),
            "char_code": Select(attrs={"width": 25}),
            "legal_status": Select(attrs={"width": 25}),
            "ref_source": Select(attrs={"width": 25}),
            "spc_end_reason": Select(attrs={"width": 25}),

            # Note
            "note": Textarea(attrs={"width": 100, "rows": 4}),

            "linked_docs": SelectMultiple(attrs={
                "clear": "both",
                "width": "100%",
                "use_popup_lookup": "EmrForm",
                "popup_lookup_child_model": "EmrDoc",
            }),

        }





class EmrOutOfCountyAssesResponseFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)

        self.fields["date"].label = "Date"
        self.fields["diagnosis"].label = "ICD10 Code"

        self.fields["diagnosis"].queryset = Diagnosis().get_crisis_icd10_codes()

        self.fields["units"].disabled = True


        self.fields["billing_code"].widget.choices = (
            NULL_CHOICE,
            ("H0030", "H0030"),
            ("H2011", "H2011"),
            ("unbillable", "N0000"),
        )

        self.fields["outcome_of_contact"].widget.choices = (
            NULL_CHOICE,
            ("home", "Home individually"),
            ("phone_plan", "Telephone stabilization plan"),
            ("community_support", "Community support plan"),
            ("stab_place", "Crisis stabilization placement"),
            ("voluntary_inpatient", "Voluntary hospitalization"),
            ("voluntary_detox", "Voluntary detox"),
            ("ch_5145", "Chapter 51.45 Incapacitation Hold"),
            ("ch_55", "Chapter 55 Protective Placement"),
            ("ch_5115", "Chapter 51.15 Emergency Detention"),
            ("medical_primary", "Medical Primary"),
            ("jail_with_precaution", "Jail-Precautions recommended"),
            ("jail_no_precaution", "Jail-Precautions NOT recommended"),
            ("mobile_worker_dispatched", "Mobile Worker Dispatched"),
            ("other", "Other")
        )



        if kwargs.get("instance") and self.instance.is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)



    class Meta:
        model = EmrNwcAssesResponseForm

        fields = [
            "date",
            "alcohol_substance_factor",
            "law_inv",
            "law_dept",
            "law_dept_other",
            "outcome_of_contact",
            "outcome_other",
            "mobile_worker_name",
            "hospital_location",
            "county_of_residence",
            "county_of_incident",
            "day_of_week",
            "rights_informed",

            "place_of_service",
            "diagnosis",
            "service_start",
            "service_end",
            "service_start2",
            "service_end2",
            "service_start3",
            "service_end3",
            "service_start4",
            "service_end4",
            "service_start5",
            "service_end5",
            "service_start6",
            "service_end6",
            "service_start7",
            "service_end7",
            "service_start8",
            "service_end8",
            "service_start9",
            "service_end9",
            "service_start10",
            "service_end10",
            "service_minutes",
            "travel_minutes",
            "doc_minutes",
            "total_minutes",
            "units",
            "billing_code",
            "staff_name1",
            "staff_creds1",
            "staff_name2",
            "staff_creds2",
            "type_of_contact",

            "brc_target_pop",
            "presenting_problem",
            "char_code",
            "legal_status",
            "ref_source",
            "spc_end_reason",

            "note",
            "linked_docs",
        ]


        widgets = {
            # Contact Summary Details
            "date": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
                "onchange": "setDayOfWeek('id_day_of_week');",
            }),
            "alcohol_substance_factor": Select(attrs={"clear": "left", "width": 50}),
            "law_inv": Select(attrs={"clear": "left", "width": 50, "selectshow": "yes|law_dept"}),
            "law_dept": Select(attrs={"width": 50, "selectshow": "other|law_dept_other;"}),
            "law_dept_other": TextInput(attrs={"width": 100}),
            "outcome_of_contact": Select(attrs={"clear": "left", "width": 50, "selectshow": "other|outcome_other; mobile_worker_dispatched|mobile_worker_name"}),
            "mobile_worker_name": TextInput(attrs={"width": 50}),
            "outcome_other": TextInput(attrs={"width": 100}),
            "hospital_location": TextInput(attrs={"width": 100}),
            "county_of_residence": Select(attrs={"width": 50, "clear": "left"}),
            "county_of_incident": Select(attrs={"width": 50}),
            "day_of_week": Select(attrs={"width": 20}),
            "rights_informed": Select(attrs={"width": 100}),

            # Billing
            "place_of_service": Select(attrs={"width": 40}),
            "diagnosis": Select(attrs={"width": 100}),
            "billing_code": Select(attrs={"clear": "left", "width": 16}),

            "service_start": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time", "expfield": "service_start, service_end", "expcount": 10}),
            "service_end": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start2": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end2": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start3": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end3": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start4": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end4": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start5": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end5": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start6": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end6": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start7": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end7": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start8": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end8": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start9": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end9": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),
            "service_start10": TimeInput(format="%H:%M", attrs={"clear": "left", "width": 20, "type": "time"}),
            "service_end10": TimeInput(format="%H:%M", attrs={"width": 20, "type": "time"}),

            "service_minutes": NumberInput(attrs={"clear": "left", "width": 20}),
            "travel_minutes": NumberInput(attrs={"width": 20}),
            "doc_minutes": NumberInput(attrs={"width": 20}),
            "total_minutes": NumberInput(attrs={"width": 20}),
            "units": NumberInput(attrs={"width": 20}),
            "staff_name1": TextInput(attrs={"clear": "left", "width": 50}),
            "staff_creds1": Select(attrs={"width": 25}),
            "staff_name2": TextInput(attrs={"clear": "left", "width": 50}),
            "staff_creds2": Select(attrs={"width": 25}),
            "type_of_contact": Select(attrs={"clear": "left", "width": 20}),

            # PPS
            "brc_target_pop": Select(attrs={"width": 25}),
            "presenting_problem": Select(attrs={"width": 25}),
            "char_code": Select(attrs={"width": 25}),
            "legal_status": Select(attrs={"width": 25}),
            "ref_source": Select(attrs={"width": 25}),
            "spc_end_reason": Select(attrs={"width": 25}),


            # Note
            "note": Textarea(attrs={"width": 100, "rows": 4}),

            "linked_docs": SelectMultiple(attrs={
                "clear": "both",
                "width": "100%",
                "use_popup_lookup": "EmrForm",
                "popup_lookup_child_model": "EmrDoc",
            }),


        }















class EmrPlacementFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrPlacementFormForm, self).__init__(*args, **kwargs)


        # Service date is not required on these placement forms
        # they use start and end month/year
        self.fields["date"].required = False
        self.fields["date"].widget = HiddenInput()


        if hasattr(self, "instance") and self.instance.is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)


    class Meta:
        model = EmrPlacementForm

        fields = [
            "date",
            "start_month",
            "start_day",
            "start_year",
            "end_month",
            "end_day",
            "end_year",
            "county_funded",
            "facility",
            "street",
            "suite",
            "city",
            "state",
            "zipcode",
            "phone",
            "phone_ext",
            "fax",
            "email",
            "notes",
            #"linked_docs",
        ]

        # Add these if you need, otherwise explicitly set them above
        #fields = emr_form_fields + fields

        widgets = {
            "start_month": NumberInput(attrs={"width": "6rem"}),
            "start_day": NumberInput(attrs={"width": "6rem"}),
            "start_year": NumberInput(attrs={"width": "6rem"}),

            "end_month": NumberInput(attrs={"width": "6rem", "clear": "left"}),
            "end_day": NumberInput(attrs={"width": "6rem"}),
            "end_year": NumberInput(attrs={"width": "6rem"}),

            "county_funded": CheckboxInput(attrs={"width": 100}),
            "facility": TextInput(attrs={"width": 100, }),
            "street": TextInput(attrs={"width": 50}),
            "suite": TextInput(attrs={"width": 30}),
            "city": TextInput(attrs={"width": 30}),
            "state": Select(attrs={"width": 20}),
            "zipcode": TextInput(attrs={"width": 15}),
            "phone": TextInput(attrs={"width": 20, "class": "phone", "clear": "left"}),
            "phone_ext": TextInput(attrs={"width": 10}),
            "fax": TextInput(attrs={"width": 20, "class": "phone"}),
            "email": TextInput(attrs={"width": 50}),
            "notes": Textarea(attrs={"width": 100, "rows": 2}),

            #"linked_docs": SelectMultiple(attrs={
            #    "clear": "both",
            #    "width": "100%",
            #    "use_popup_lookup": "EmrForm",
            #    "popup_lookup_child_model": "EmrDoc",
            #}),
        }

        # Add these if you need, otherwise explicitly set them above
        #widgets = {**widgets, **emr_form_widgets}






class EmrClinicWaitlistFormForm(ModelForm):
    # This form does not use {{ form.as_p }}
    # It's written out in the template clinic_wait_list.html
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrClinicWaitlistFormForm, self).__init__(*args, **kwargs)

        self.fields["date"].label = "Wait list start date"

        if kwargs.get("instance") and self.instance.is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)


    class Meta:
        model = EmrClinicWaitlistForm

        fields = [
            "date",
            "legal_status",
            "pregnant",
            "pregnant_dtl",
            "iv_drug_use",
            "mh",
            "aoda",
            "mh_and_aoda",
            "insurance_info",
            "adtl_info",
            "linked_docs",
        ]

        # Add these if you need, otherwise explicitly set them above
        #fields = emr_form_fields + fields

        widgets = {
            "date": TextInput(attrs={"class": "datepicker"}),
            "legal_status": Select(),
            "pregnant": CheckboxInput(attrs={"width": 100, "chkrelated": True}),
            "pregnant_dtl": Textarea(attrs={"width": 100, "rows": 2}),
            "iv_drug_use": CheckboxInput(attrs={"clear": "left", "width": 100}),
            "mh": CheckboxInput(attrs={"width": 20, "clear": "left"}),
            "aoda": CheckboxInput(attrs={"width": 20}),
            "mh_and_aoda": CheckboxInput(attrs={"width": 20}),
            "insurance_info": Textarea(attrs={"width": 100, "rows": 2}),
            "adtl_info": Textarea(attrs={"width": 100, "rows": 2}),
            "linked_docs": SelectMultiple(attrs={
                "clear": "both",
                "width": "100%",
                "use_popup_lookup": "EmrForm",
                "popup_lookup_child_model": "EmrDoc",
            }),
        }

        # Add these if you need, otherwise explicitly set them above
        #widgets = {**widgets, **emr_form_widgets}




class EmrCltsCstAssessmentFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrCltsCstAssessmentFormForm, self).__init__(*args, **kwargs) 

        # This is how to remove the empty label '----------' on selects
        self.fields["patient_rights_and_grievances"].empty_label = None
        self.fields["patient_rights_and_grievances"].widget.choices = CLIENT_C.CLTS_CST_RIGHTS_AND_GRIEV


        if hasattr(self, "instance") and self.instance.is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)






    class Meta:
        model = EmrCltsCstAssessmentForm

        fields = [
            "assessment_date",
            "assessment_type",
            "medicaid_type",
            "target_group_dd",
            "target_group_pd",
            "target_group_mh",
            "patient_rights_and_grievances",
            "parents",
            "info_collected_from",
            "background_info",
            "strengths_needs_social",
            "formal_informal_supports",
            "community_involvement",
            "beliefs",
            "current_health",
            "medication",
            "primary_care_dr",
            "eye_care",
            "dental_care",
            "hearing_care",
            "specialty_care",
            "bathing_grooming",
            "dressing_undressing",
            "feeding",
            "toileting",
            "mobility_transfers",
            "communication",
            "daily_living_activities",
            "emo_behave",
            "cognitive_testing",
            "cognitive_summary",
            "school",
            "grade",
            "iep_504_plan",
            "physical_therapy",
            "speech_language",
            "occupational_therapy",
            "testing_completed",
            "adtl_school_info",
            "employed",
            "interest_getting_job",
            "supports_needed_job",
            "current_services_and_needs",
            "living_and_daily",
            "future_goals_plans",
            "ability_to_direct",
            "addtl_information",
            "linked_docs"
        ]






        widgets = {
            "assessment_date": DateInput(format=settings.DATE_FORMAT, attrs={
                'width': 20, 'class': 'datepicker'}),
            "assessment_type": Select(attrs={"width": 40}),
            "medicaid_type": Select(attrs={"width": 40, }),
            "target_group_dd": CheckboxInput(attrs={"width": 33}),
            "target_group_pd": CheckboxInput(attrs={"width": 33}),
            "target_group_mh": CheckboxInput(attrs={"width": 33}),
            "patient_rights_and_grievances": RadioSelect(attrs={"class": "radio-select"}),
            "parents": Textarea(attrs={"rows": 2, "width": 100}),
            "info_collected_from": Textarea(attrs={"rows": 2, "width": 100}),
            "background_info": Textarea(attrs={"rows": 2, "width": 100}),
            "strengths_needs_social": Textarea(attrs={"rows": 2, "width": 100}),
            "formal_informal_supports": Textarea(attrs={"rows": 2, "width": 100}),
            "community_involvement": Textarea(attrs={"rows": 2, "width": 100}),
            "beliefs": Textarea(attrs={"rows": 2, "width": 100}),
            "current_health": Textarea(attrs={"rows": 2, "width": 100}),
            "medication": Textarea(attrs={"rows": 2, "width": 100}),
            "primary_care_dr": TextInput(attrs={"width": 100}),
            "eye_care": TextInput(attrs={"width": 100}),
            "dental_care": TextInput(attrs={"width": 100}),
            "hearing_care": TextInput(attrs={"width": 100}),
            "specialty_care": TextInput(attrs={"width": 100}),
            "bathing_grooming": Textarea(attrs={"rows": 2, "width": 100}),
            "dressing_undressing": Textarea(attrs={"rows": 2, "width": 100}),
            "feeding": Textarea(attrs={"rows": 2, "width": 100}),
            "toileting": Textarea(attrs={"rows": 2, "width": 100}),
            "mobility_transfers": Textarea(attrs={"rows": 2, "width": 100}),
            "communication": Textarea(attrs={"rows": 2, "width": 100}),
            "daily_living_activities": Textarea(attrs={"rows": 2, "width": 100}),
            "emo_behave": Textarea(attrs={"rows": 2, "width": 100}),
            "cognitive_testing": Textarea(attrs={"rows": 2, "width": 100}),
            "cognitive_summary": Textarea(attrs={"rows": 2, "width": 100}),
            "school": TextInput(attrs={"width": 100}),
            "grade": TextInput(attrs={"width": 100}),
            "iep_504_plan": Textarea(attrs={"rows": 2, "width": 100}),
            "physical_therapy": TextInput(attrs={"width": 100}),
            "speech_language": TextInput(attrs={"width": 100}),
            "occupational_therapy": TextInput(attrs={"width": 100}),
            "testing_completed": Textarea(attrs={"rows": 2, "width": 100}),
            "adtl_school_info": Textarea(attrs={"rows": 2, "width": 100}),
            "employed": TextInput(attrs={"width": 100}),
            "interest_getting_job": TextInput(attrs={"width": 100}),
            "supports_needed_job": Textarea(attrs={"rows": 2, "width": 100}),
            "current_services_and_needs": Textarea(attrs={"rows": 2, "width": 100}),
            "living_and_daily": Textarea(attrs={"rows": 2, "width": 100}),
            "future_goals_plans": Textarea(attrs={"rows": 2, "width": 100}),
            "ability_to_direct": Textarea(attrs={"rows": 2, "width": 100}),
            "addtl_information": Textarea(attrs={"rows": 2, "width": 100}),
            "linked_docs": SelectMultiple(attrs={
                "clear": "both",
                "width": "100%",
                "use_popup_lookup": "EmrForm",
                "popup_lookup_child_model": "EmrDoc",
            }),
        }






class EmrCrisisPlanFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super().__init__(*args, **kwargs)


        #self.fields["diagnosis"].queryset = Diagnosis().get_crisis_icd10_codes()

        if (hasattr(self, "instance") and self.instance.is_finalized is True) or self.locked is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)



    class Meta:
        model = EmrCrisisPlanForm

        fields = [
            # Face sheet section
            "date",
            "living_situation",
            "county_of_responsibility",
            "county_of_placement",
            "plan_developer",
            "agency_name",
            "agency_address",
            "phone_contact",
            "phone_contact_ext",

            # Known cautions
            "caution_firearms",
            "caution_firearms_dtl",
            "caution_animals",
            "caution_animals_dtl",
            "caution_other",
            "caution_other_dtl",

            # Communication
            "verbal",
            "other_com_preferences",


            # Crisis plan information section
            "strengths",
            "needs",
            "past_behavior",
            "intervention_list",
            "interventions_avoid",
            "helpful_info",
            "contact_name",
            "contact_relation",
            "contact_phone",
            "contact_phone_ext",
            "contact_address",
            "contact_note",
            "contact_name2",
            "contact_relation2",
            "contact_phone2",
            "contact_phone_ext2",
            "contact_address2",
            "contact_note2",
            "contact_name3",
            "contact_relation3",
            "contact_phone3",
            "contact_phone_ext3",
            "contact_address3",
            "contact_note3",
            "contact_name4",
            "contact_relation4",
            "contact_phone4",
            "contact_phone_ext4",
            "contact_address4",
            "contact_note4",
            "contact_name5",
            "contact_relation5",
            "contact_phone5",
            "contact_phone_ext5",
            "contact_address5",
            "contact_note5",

            "daily_schedule",


            # Diagnostic Impression section
            "diagnosis1",
            "diag_formed_by",
            "diag_last_date",
            "diagnosis2",
            "diag_formed_by2",
            "diag_last_date2",
            "diagnosis3",
            "diag_formed_by3",
            "diag_last_date3",
            "diagnosis4",
            "diag_formed_by4",
            "diag_last_date4",
            "diagnosis5",
            "diag_formed_by5",
            "diag_last_date5",

            "past_mh_diag",
            "known_needs",
            "prescriptions",
            "past_emh_services",

            "psych",
            "psych_name",
            "psych_clinic",
            "psych_phone",
            "psych_phone_ext",
            "psych_note",

            "primary_provider",
            "primary_name",
            "primary_clinic",
            "primary_phone",
            "primary_phone_ext",
            "primary_note",

            "pharm",
            "pharm_name",
            "pharm_clinic",
            "pharm_phone",
            "pharm_phone_ext",
            "pharm_note",

            "therapist",
            "therapist_name",
            "therapist_clinic",
            "therapist_phone",
            "therapist_phone_ext",
            "therapist_note",

            "other_provider",
            "other_name",
            "other_clinic",
            "other_phone",
            "other_phone_ext",
            "other_note",

            # Legal status section
            "ch51_comit",
            "ch51_agency",
            "ch51_name",
            "ch51_phone",
            "ch51_phone_ext",
            "ch51_comit_expire",
            "ch51_med_order",

            "pp",
            "pp_name",
            "pp_agency",
            "pp_phone",
            "pp_phone_ext",

            "guardianship",
            "guardian_name",
            "guardian_phone",
            "guardian_phone_ext",
            "guardian_address",

            "probation",
            "prob_agent",
            "prob_phone",
            "prob_phone_ext",
            "prob_address",

            # Other program affiliation
            "ccs",
            "ccs_agency",
            "ccs_name",
            "ccs_phone",
            "ccs_phone_ext",
            "csp",
            "csp_agency",
            "csp_name",
            "csp_phone",
            "csp_phone_ext",
            "cst",
            "cst_agency",
            "cst_name",
            "cst_phone",
            "cst_phone_ext",
            "clts",
            "clts_agency",
            "clts_name",
            "clts_phone",
            "clts_phone_ext",
            "fc",
            "fc_agency",
            "fc_name",
            "fc_phone",
            "fc_phone_ext",
            "iris",
            "iris_agency",
            "iris_name",
            "iris_phone",
            "iris_phone_ext",
            "cps",
            "cps_agency",
            "cps_name",
            "cps_phone",
            "cps_phone_ext",
            "jj",
            "jj_agency",
            "jj_name",
            "jj_phone",
            "jj_phone_ext",
            "aps",
            "aps_agency",
            "aps_name",
            "aps_phone",
            "aps_phone_ext",
            "adrc",
            "adrc_agency",
            "adrc_name",
            "adrc_phone",
            "adrc_phone_ext",
            "other",
            "other_pa_agency",
            "other_pa_name",
            "other_pa_phone",
            "other_pa_phone_ext",

            # Signatures section
            "linked_docs",
        ]      



        widgets = {
            # Service summary
            "date": DateInput(format=settings.DATE_FORMAT, attrs={"width": 20, "class": "datepicker"}),
            "living_situation": TextInput(attrs={"width": 100}),
            "county_of_responsibility": Select(attrs={"width": 50}),
            "county_of_placement": Select(attrs={"width": 50}),
            "plan_developer": TextInput(attrs={"width": 50}),
            "agency_name": TextInput(attrs={"width": 50}),
            "agency_address": TextInput(attrs={"width": 50}),
            "phone_contact": TextInput(attrs={"width": 40, "class": "phone"}),
            "phone_contact_ext": TextInput(attrs={"width": 10}),

            # Known cautions
            "caution_firearms": CheckboxInput(attrs={"clear": "left", "width": 20, "chkrelated": True}),
            "caution_firearms_dtl": TextInput(attrs={"width": 80}),
            "caution_animals": CheckboxInput(attrs={"clear": "left", "width": 20, "chkrelated": True}),
            "caution_animals_dtl": TextInput(attrs={"width": 80}),
            "caution_other": CheckboxInput(attrs={"clear": "left", "width": 20, "chkrelated": True}),
            "caution_other_dtl": TextInput(attrs={"width": 80}),

            # Communication
            "verbal": Select(attrs={"width": 20}),
            "other_com_preferences": TextInput(attrs={"width": 100}),


            # Crisis Plan Information section
            "strengths": Textarea(attrs={"width": 100, "rows": 2}),
            "needs": Textarea(attrs={"width": 100, "rows": 2}),
            "past_behavior": Textarea(attrs={"width": 100, "rows": 2}),
            "intervention_list": Textarea(attrs={"width": 100, "rows": 2}),
            "interventions_avoid": Textarea(attrs={"width": 100, "rows": 2}),
            "helpful_info": Textarea(attrs={"width": 100, "rows": 2}),

            # Critical Support Contacts section
            "contact_name": TextInput(attrs={"width": 20, "expfield": "contact_name, contact_relation, contact_phone, contact_phone_ext, contact_address, contact_note", "expcount": 5}),
            "contact_relation": TextInput(attrs={"width": 20}),
            "contact_phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "contact_phone_ext": TextInput(attrs={"width": 5}),
            "contact_address": TextInput(attrs={"width": 20}),
            "contact_note": TextInput(attrs={"width": 20}),
            "contact_name2": TextInput(attrs={"width": 20}),
            "contact_relation2": TextInput(attrs={"width": 20}),
            "contact_phone2": TextInput(attrs={"width": 15, "class": "phone"}),
            "contact_phone_ext2": TextInput(attrs={"width": 5}),
            "contact_address2": TextInput(attrs={"width": 20}),
            "contact_note2": TextInput(attrs={"width": 20}),
            "contact_name3": TextInput(attrs={"width": 20}),
            "contact_relation3": TextInput(attrs={"width": 20}),
            "contact_phone3": TextInput(attrs={"width": 15, "class": "phone"}),
            "contact_phone_ext3": TextInput(attrs={"width": 5}),
            "contact_address3": TextInput(attrs={"width": 20}),
            "contact_note3": TextInput(attrs={"width": 20}),
            "contact_name4": TextInput(attrs={"width": 20}),
            "contact_relation4": TextInput(attrs={"width": 20}),
            "contact_phone4": TextInput(attrs={"width": 15, "class": "phone"}),
            "contact_phone_ext4": TextInput(attrs={"width": 5}),
            "contact_address4": TextInput(attrs={"width": 20}),
            "contact_note4": TextInput(attrs={"width": 20}),
            "contact_name5": TextInput(attrs={"width": 20}),
            "contact_relation5": TextInput(attrs={"width": 20}),
            "contact_phone5": TextInput(attrs={"width": 15, "class": "phone"}),
            "contact_phone_ext5": TextInput(attrs={"width": 5}),
            "contact_address5": TextInput(attrs={"width": 20}),
            "contact_note5": TextInput(attrs={"width": 20}),

            # Usual Daily Schedule section
            "daily_schedule": Textarea(attrs={"clear": "left", "width": 100, "rows": "2"}),

            # Diagnostic impression section
            "diagnosis1": TextInput(attrs={"width": 50}),
            "diag_formed_by": TextInput(attrs={
                "width": 30,
                "expfield": "diagnosis, diag_formed_by, diag_last_date", "expcount": 5
            }),
            "diag_last_date": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }),

            "diagnosis2": TextInput(attrs={"width": 50}),
            "diag_formed_by2": TextInput(attrs={"width": 30}),
            "diag_last_date2": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }),

            "diagnosis3": TextInput(attrs={"width": 50}),
            "diag_formed_by3": TextInput(attrs={"width": 30}),
            "diag_last_date3": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }),

            "diagnosis4": TextInput(attrs={"width": 50}),
            "diag_formed_by4": TextInput(attrs={"width": 30}),
            "diag_last_date4": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }),

            "diagnosis5": TextInput(attrs={"width": 50}),
            "diag_formed_by5": TextInput(attrs={"width": 30}),
            "diag_last_date5": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": 20,
                "class": "datepicker",
            }),



            "past_mh_diag": Textarea(attrs={"width": 100, "rows": 2}),
            "known_needs": Textarea(attrs={"width": 100, "rows": 2}),

            "prescriptions": Textarea(attrs={"width": 100, "rows": 2}),
            "past_emh_services": Textarea(attrs={"width": 100, "rows": 2}),

            # Providers section
            "psych": CheckboxInput(attrs={"width": 100, "chkrelated": "psych_name, psych_clinic, psych_phone, psych_phone_ext, psych_note", }),
            "psych_name": TextInput(attrs={"width": 40, }),
            "psych_clinic": TextInput(attrs={"width": 30, }),
            "psych_phone": TextInput(attrs={"width": 20, "class": "phone", }),
            "psych_phone_ext": TextInput(attrs={"width": 10, }),
            "psych_note": Textarea(attrs={"width": 100, "rows": 2, }),

            "primary_provider": CheckboxInput(attrs={"width": 100, "chkrelated": "primary_name, primary_clinic, primary_phone, primary_phone_ext, primary_note", }),
            "primary_name": TextInput(attrs={"width": 40, }),
            "primary_clinic": TextInput(attrs={"width": 30, }),
            "primary_phone": TextInput(attrs={"width": 20, "class": "phone", }),
            "primary_phone_ext": TextInput(attrs={"width": 10, }),
            "primary_note": Textarea(attrs={"width": 100, "rows": 2, }),

            "pharm": CheckboxInput(attrs={"width": 100, "chkrelated": "pharm_name, pharm_clinic, pharm_phone, pharm_phone_ext, pharm_note", }),
            "pharm_name": TextInput(attrs={"width": 40, }),
            "pharm_clinic": TextInput(attrs={"width": 30, }),
            "pharm_phone": TextInput(attrs={"width": 20, "class": "phone", }),
            "pharm_phone_ext": TextInput(attrs={"width": 10, }),
            "pharm_note": Textarea(attrs={"width": 100, "rows": 2, }),

            "therapist": CheckboxInput(attrs={"width": 100, "chkrelated": "therapist_name, therapist_clinic, therapist_phone, therapist_phone_ext, therapist_note", }),
            "therapist_name": TextInput(attrs={"width": 40, }),
            "therapist_clinic": TextInput(attrs={"width": 30, }),
            "therapist_phone": TextInput(attrs={"width": 20, "class": "phone", }),
            "therapist_phone_ext": TextInput(attrs={"width": 10, }),
            "therapist_note": Textarea(attrs={"width": 100, "rows": 2, }),


            "other_provider": CheckboxInput(attrs={"width": 100, "chkrelated": "other_name, other_clinic, other_phone, other_phone_ext, other_note", }),
            "other_name": TextInput(attrs={"width": 40, }),
            "other_clinic": TextInput(attrs={"width": 30, }),
            "other_phone": TextInput(attrs={"width": 20, "class": "phone", }),
            "other_phone_ext": TextInput(attrs={"width": 10, }),
            "other_note": Textarea(attrs={"width": 100, "rows": 2, }),


            # Legal Status
            "ch51_comit": CheckboxInput(attrs={"width": 100, "chkrelated": "ch51_comit_expire, ch51_med_order, ch51_agency, ch51_name, ch51_phone, ch51_phone_ext"}),
            "ch51_agency": TextInput(attrs={"width": 20}),
            "ch51_name": TextInput(attrs={"width": 20}),
            "ch51_phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "ch51_phone_ext": TextInput(attrs={"width": 5}),
            "ch51_comit_expire": DateInput(format=settings.DATE_FORMAT, attrs={"width": 15, "class": "datepicker"}),
            "ch51_med_order": CheckboxInput(attrs={"width": 15}),

            "pp": CheckboxInput(attrs={"clear": "left", "width": 100, "chkrelated": "pp_name,pp_agency,pp_phone,pp_phone_ext"}),
            "pp_name": TextInput(attrs={"width": 20}),
            "pp_agency": TextInput(attrs={"width": 20}),
            "pp_phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "pp_phone_ext": TextInput(attrs={"width": 5}),

            "guardianship": CheckboxInput(attrs={"width": 100, "clear": "left", "chkrelated": "guardian_name, guardian_phone, guardian_phone_ext, guardian_address"}),
            "guardian_name": TextInput(attrs={"width": 20}),
            "guardian_phone": TextInput(attrs={"width": 20, "class": "phone"}),
            "guardian_phone_ext": TextInput(attrs={"width": 5}),
            "guardian_address": TextInput(attrs={"width": 25}),

            "probation": CheckboxInput(attrs={"width": 100, "clear": "left", "chkrelated": "prob_agent, prob_phone, prob_phone_ext, prob_address"}),
            "prob_agent": TextInput(attrs={"width": 40}),
            "prob_phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "prob_phone_ext": TextInput(attrs={"width": 5}),
            "prob_address": TextInput(attrs={"width": 25}),

            # Other program affiliation section
            "ccs": CheckboxInput(attrs={"width": 100, "clear": "left", "chkrelated": "ccs_agency, ccs_name, ccs_phone, ccs_phone_ext", "twocols": 1}),
            "ccs_agency": TextInput(attrs={"clear": "left", "width": 30, "twocols": 1}),
            "ccs_name": TextInput(attrs={"width": 30, "twocols": 1}),
            "ccs_phone": TextInput(attrs={"width": 30, "class": "phone", "twocols": 1}),
            "ccs_phone_ext": TextInput(attrs={"width": 10, "twocols": 1}),
            "csp": CheckboxInput(attrs={"width": 100, "chkrelated": "csp_agency, csp_name, csp_phone, csp_phone_ext", "twocols": 1}),
            "csp_agency": TextInput(attrs={"clear": "left", "width": 30, "twocols": 1}),
            "csp_name": TextInput(attrs={"width": 30, "twocols": 1}),
            "csp_phone": TextInput(attrs={"width": 30, "class": "phone", "twocols": 1}),
            "csp_phone_ext": TextInput(attrs={"width": 10, "twocols": 1}),
            "cst": CheckboxInput(attrs={"width": 100, "chkrelated": "cst_agency, cst_name, cst_phone, cst_phone_ext", "twocols": 1}),
            "cst_agency": TextInput(attrs={"clear": "left", "width": 30, "twocols": 1}),
            "cst_name": TextInput(attrs={"width": 30, "twocols": 1}),
            "cst_phone": TextInput(attrs={"width": 30, "class": "phone", "twocols": 1}),
            "cst_phone_ext": TextInput(attrs={"width": 10, "twocols": 1}),
            "clts": CheckboxInput(attrs={"width": 100, "chkrelated": "clts_agency, clts_name, clts_phone, clts_phone_ext", "twocols": 1}),
            "clts_agency": TextInput(attrs={"clear": "left", "width": 30, "twocols": 1}),
            "clts_name": TextInput(attrs={"width": 30, "twocols": 1}),
            "clts_phone": TextInput(attrs={"width": 30, "class": "phone", "twocols": 1}),
            "clts_phone_ext": TextInput(attrs={"width": 10, "twocols": 1}),
            "fc": CheckboxInput(attrs={"width": 100, "chkrelated": "fc_agency, fc_name, fc_phone, fc_phone_ext", "twocols": 1}),
            "fc_agency": TextInput(attrs={"clear": "left", "width": 30, "twocols": 1}),
            "fc_name": TextInput(attrs={"width": 30, "twocols": 1}),
            "fc_phone": TextInput(attrs={"width": 30, "class": "phone", "twocols": 1}),
            "fc_phone_ext": TextInput(attrs={"width": 10, "twocols": 1}),
            "iris": CheckboxInput(attrs={"width": 100, "chkrelated": "iris_agency, iris_name, iris_phone, iris_phone_ext", "twocols": 1}),
            "iris_agency": TextInput(attrs={"clear": "left", "width": 30, "twocols": 1}),
            "iris_name": TextInput(attrs={"width": 30, "twocols": 1}),
            "iris_phone": TextInput(attrs={"width": 30, "class": "phone", "twocols": 1}),
            "iris_phone_ext": TextInput(attrs={"width": 10, "twocols": 1}),
            "cps": CheckboxInput(attrs={"width": 100, "chkrelated": "cps_agency, cps_name, cps_phone, cps_phone_ext", "twocols": 2}),
            "cps_agency": TextInput(attrs={"clear": "left", "width": 30, "twocols": 2}),
            "cps_name": TextInput(attrs={"width": 30, "twocols": 2}),
            "cps_phone": TextInput(attrs={"width": 30, "class": "phone", "twocols": 2}),
            "cps_phone_ext": TextInput(attrs={"width": 10, "twocols": 2}),
            "jj": CheckboxInput(attrs={"width": 100, "chkrelated": "jj_agency, jj_name, jj_phone, jj_phone_ext", "twocols": 2}),
            "jj_agency": TextInput(attrs={"clear": "left", "width": 30, "twocols": 2}),
            "jj_name": TextInput(attrs={"width": 30, "twocols": 2}),
            "jj_phone": TextInput(attrs={"width": 30, "class": "phone", "twocols": 2}),
            "jj_phone_ext": TextInput(attrs={"width": 10, "twocols": 2}),
            "aps": CheckboxInput(attrs={"width": 100, "chkrelated": "aps_agency, aps_name, aps_phone, aps_phone_ext", "twocols": 2}),
            "aps_agency": TextInput(attrs={"clear": "left", "width": 30, "twocols": 2}),
            "aps_name": TextInput(attrs={"width": 30, "twocols": 2}),
            "aps_phone": TextInput(attrs={"width": 30, "class": "phone", "twocols": 2}),
            "aps_phone_ext": TextInput(attrs={"width": 10, "twocols": 2}),
            "adrc": CheckboxInput(attrs={"width": 100, "chkrelated": "adrc_agency, adrc_name, adrc_phone, adrc_phone_ext", "twocols": 2}),
            "adrc_agency": TextInput(attrs={"clear": "left", "width": 30, "twocols": 2}),
            "adrc_name": TextInput(attrs={"width": 30, "twocols": 2}),
            "adrc_phone": TextInput(attrs={"width": 30, "class": "phone", "twocols": 2}),
            "adrc_phone_ext": TextInput(attrs={"width": 10, "twocols": 2}),
            "other": CheckboxInput(attrs={"width": 100, "chkrelated": "other_pa_agency, other_pa_name, other_pa_phone, other_pa_phone_ext", "twocols": 2}),
            "other_pa_agency": TextInput(attrs={"clear": "left", "width": 30, "twocols": 2}),
            "other_pa_name": TextInput(attrs={"width": 30, "twocols": 2}),
            "other_pa_phone": TextInput(attrs={"width": 30, "class": "phone", "twocols": 2}),
            "other_pa_phone_ext": TextInput(attrs={"width": 10, "twocols": 2}),


            "linked_docs": SelectMultiple(attrs={
                "clear": "both",
                "width": "100%",
                "use_popup_lookup": "EmrForm",
                "popup_lookup_child_model": "EmrDoc",
            }),
        }

        labels = {
            "date": "Date",
        }














class EmrCrisisAlertFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrCrisisAlertFormForm, self).__init__(*args, **kwargs)


        if (hasattr(self, "instance") and self.instance.is_finalized is True) or self.locked is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)



    class Meta:
        model = EmrCrisisAlertForm

        fields = [
            "date",
            "alert_end_date",
            "county_of_responsibility",
            "county_of_placement",
            "plan_developer",
            "agency_name",
            "agency_address",
            "phone_contact",
            "phone_contact_ext",

            "caution_firearms",
            "caution_firearms_dtl",
            "caution_animals",
            "caution_animals_dtl",
            "caution_other",
            "caution_other_dtl",

            "verbal",
            "other_com_preferences",


            "reason_for_alert",
            "established_plan",
            "living_situation",


            "contact_name",
            "contact_relation",
            "contact_phone",
            "contact_phone_ext",
            "contact_address",
            "contact_avil",
            "contact_name2",
            "contact_relation2",
            "contact_phone2",
            "contact_phone_ext2",
            "contact_address2",
            "contact_avil2",
            "contact_name3",
            "contact_relation3",
            "contact_phone3",
            "contact_phone_ext3",
            "contact_address3",
            "contact_avil3",
            "contact_name4",
            "contact_relation4",
            "contact_phone4",
            "contact_phone_ext4",
            "contact_address4",
            "contact_avil4",
            "contact_name5",
            "contact_relation5",
            "contact_phone5",
            "contact_phone_ext5",
            "contact_address5",
            "contact_avil5",

            "mh_diag_needs",
            "phys_med_needs",
            "medications",

            "past_emh_services",

            # Legal status section
            "ch51_comit",
            "ch51_agency",
            "ch51_name",
            "ch51_phone",
            "ch51_phone_ext",
            "ch51_comit_expire",
            "ch51_med_order",

            "ch55_pp",
            "ch55_agency",
            "ch55_name",
            "ch55_phone",
            "ch55_phone_ext",

            "guardianship",
            "guardian_agency",
            "guardian_name",
            "guardian_phone",
            "guardian_phone_ext",

            "probation",
            "probation_agent",
            "probation_phone",
            "probation_phone_ext",

            # Other program affiliation
            "ccs",
            "ccs_agency",
            "ccs_name",
            "ccs_phone",
            "ccs_phone_ext",
            "csp",
            "csp_agency",
            "csp_name",
            "csp_phone",
            "csp_phone_ext",
            "cst",
            "cst_agency",
            "cst_name",
            "cst_phone",
            "cst_phone_ext",
            "clts",
            "clts_agency",
            "clts_name",
            "clts_phone",
            "clts_phone_ext",
            "fc",
            "fc_agency",
            "fc_name",
            "fc_phone",
            "fc_phone_ext",
            "iris",
            "iris_agency",
            "iris_name",
            "iris_phone",
            "iris_phone_ext",
            "cps",
            "cps_agency",
            "cps_name",
            "cps_phone",
            "cps_phone_ext",
            "jj",
            "jj_agency",
            "jj_name",
            "jj_phone",
            "jj_phone_ext",
            "aps",
            "aps_agency",
            "aps_name",
            "aps_phone",
            "aps_phone_ext",
            "adrc",
            "adrc_agency",
            "adrc_name",
            "adrc_phone",
            "adrc_phone_ext",
            "other_pa",
            "other_pa_agency",
            "other_pa_name",
            "other_pa_phone",
            "other_pa_phone_ext",

            "supporting_docs",
            "linked_docs",
        ]      




        widgets = {
            "date": DateInput(format=settings.DATE_FORMAT, attrs={"width": 20, "class": "datepicker"}),
            "alert_end_date": DateInput(format=settings.DATE_FORMAT, attrs={"width": 20, "class": "datepicker"}),
            "county_of_responsibility": Select(attrs={"clear": "left", "width": 50}),
            "county_of_placement": Select(attrs={"width": 50}),
            "plan_developer": TextInput(attrs={"width": 50}),
            "agency_name": TextInput(attrs={"width": 50}),
            "agency_address": TextInput(attrs={"width": 50}),
            "phone_contact": TextInput(attrs={"width": 40, "class": "phone"}),
            "phone_contact_ext": TextInput(attrs={"width": 10}),

            "caution_firearms": CheckboxInput(attrs={"clear": "left", "width": 20, "chkrelated": True}),
            "caution_firearms_dtl": TextInput(attrs={"width": 80}),
            "caution_animals": CheckboxInput(attrs={"clear": "left", "width": 20, "chkrelated": True}),
            "caution_animals_dtl": TextInput(attrs={"width": 80}),
            "caution_other": CheckboxInput(attrs={"clear": "left", "width": 20, "chkrelated": True}),
            "caution_other_dtl": TextInput(attrs={"width": 80}),

            "verbal": Select(attrs={"width": 20}),
            "other_com_preferences": TextInput(attrs={"width": 100}),


            "reason_for_alert": Textarea(attrs={"width": 100, "rows": 2}),
            "established_plan": Textarea(attrs={"width": 100, "rows": 2}),
            "living_situation": TextInput(attrs={"width": 100}),


            "contact_name": TextInput(attrs={"width": 20, "expfield": "contact_name, contact_relation, contact_phone, contact_phone_ext, contact_address, contact_avil", "expcount": 5}),
            "contact_relation": TextInput(attrs={"width": 15}),
            "contact_phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "contact_phone_ext": TextInput(attrs={"width": 10}),
            "contact_address": TextInput(attrs={"width": 20}),
            "contact_avil": TextInput(attrs={"width": 20}),
            "contact_name2": TextInput(attrs={"width": 20}),
            "contact_relation2": TextInput(attrs={"width": 15}),
            "contact_phone2": TextInput(attrs={"width": 15, "class": "phone"}),
            "contact_phone_ext2": TextInput(attrs={"width": 10}),
            "contact_address2": TextInput(attrs={"width": 20}),
            "contact_avil2": TextInput(attrs={"width": 20}),
            "contact_name3": TextInput(attrs={"width": 20}),
            "contact_relation3": TextInput(attrs={"width": 15}),
            "contact_phone3": TextInput(attrs={"width": 15, "class": "phone"}),
            "contact_phone_ext3": TextInput(attrs={"width": 10}),
            "contact_address3": TextInput(attrs={"width": 20}),
            "contact_avil3": TextInput(attrs={"width": 20}),
            "contact_name4": TextInput(attrs={"width": 20}),
            "contact_relation4": TextInput(attrs={"width": 15}),
            "contact_phone4": TextInput(attrs={"width": 15, "class": "phone"}),
            "contact_phone_ext4": TextInput(attrs={"width": 10}),
            "contact_address4": TextInput(attrs={"width": 20}),
            "contact_avil4": TextInput(attrs={"width": 20}),
            "contact_name5": TextInput(attrs={"width": 20}),
            "contact_relation5": TextInput(attrs={"width": 15}),
            "contact_phone5": TextInput(attrs={"width": 15, "class": "phone"}),
            "contact_phone_ext5": TextInput(attrs={"width": 10}),
            "contact_address5": TextInput(attrs={"width": 20}),
            "contact_avil5": TextInput(attrs={"width": 20}),

            # Mental Health and Physical Health section
            "mh_diag_needs": Textarea(attrs={"width": 100, "rows": 2}),
            "phys_med_needs": Textarea(attrs={"width": 100, "rows": 2}),
            "medications": Textarea(attrs={"width": 100, "rows": 2}),

            # Past Emergency Mental Health Services
            "past_emh_services": Textarea(attrs={"width": 100, "rows": 2}),

            # Legal Status
            "ch51_comit": CheckboxInput(attrs={"width": 100, "chkrelated": "ch51_comit_expire, ch51_med_order, ch51_agency, ch51_name, ch51_phone, ch51_phone_ext"}),
            "ch51_agency": TextInput(attrs={"width": 20}),
            "ch51_name": TextInput(attrs={"width": 20}),
            "ch51_phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "ch51_phone_ext": TextInput(attrs={"width": 5}),
            "ch51_comit_expire": DateInput(format=settings.DATE_FORMAT, attrs={"width": 15, "class": "datepicker"}),
            "ch51_med_order": CheckboxInput(attrs={"width": 15}),

            "ch55_pp": CheckboxInput(attrs={"clear": "left", "width": 100, "chkrelated": "ch55_agency, ch55_name, ch55_phone, ch55_phone_ext"}),
            "ch55_agency": TextInput(attrs={"width": 20}),
            "ch55_name": TextInput(attrs={"width": 20}),
            "ch55_phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "ch55_phone_ext": TextInput(attrs={"width": 5}),

            "guardianship": CheckboxInput(attrs={
                "width": 100,
                "clear": "left",
                "chkrelated": "guardian_agency, guardian_name, guardian_phone, guardian_phone_ext"}),
            "guardian_agency": TextInput(attrs={"width": 20}),
            "guardian_name": TextInput(attrs={"width": 20}),
            "guardian_phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "guardian_phone_ext": TextInput(attrs={"width": 5}),

            "probation": CheckboxInput(attrs={
                "width": 100,
                "clear": "left",
                "chkrelated": "probation_agent, probation_phone, probation_phone_ext"}),
            "probation_agent": TextInput(attrs={"width": 40}),
            "probation_phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "probation_phone_ext": TextInput(attrs={"width": 5}),


            # Other program affiliation section
            "ccs": CheckboxInput(attrs={"width": 100, "clear": "left", "chkrelated": "ccs_agency, ccs_name, ccs_phone, ccs_phone_ext", "twocols": 1}),
            "ccs_agency": TextInput(attrs={"clear": "left", "width": 35, "twocols": 1}),
            "ccs_name": TextInput(attrs={"width": 35, "twocols": 1}),
            "ccs_phone": TextInput(attrs={"width": 20, "class": "phone", "twocols": 1}),
            "ccs_phone_ext": TextInput(attrs={"width": 10, "twocols": 1}),
            "csp": CheckboxInput(attrs={"width": 100, "clear": "left", "chkrelated": "csp_agency, csp_name, csp_phone, csp_phone_ext", "twocols": 1}),
            "csp_agency": TextInput(attrs={"clear": "left", "width": 35, "twocols": 1}),
            "csp_name": TextInput(attrs={"width": 35, "twocols": 1}),
            "csp_phone": TextInput(attrs={"width": 20, "class": "phone", "twocols": 1}),
            "csp_phone_ext": TextInput(attrs={"width": 10, "twocols": 1}),
            "cst": CheckboxInput(attrs={"width": 100, "clear": "left", "chkrelated": "cst_agency, cst_name, cst_phone, cst_phone_ext", "twocols": 1}),
            "cst_agency": TextInput(attrs={"clear": "left", "width": 35, "twocols": 1}),
            "cst_name": TextInput(attrs={"width": 35, "twocols": 1}),
            "cst_phone": TextInput(attrs={"width": 20, "class": "phone", "twocols": 1}),
            "cst_phone_ext": TextInput(attrs={"width": 10, "twocols": 1}),
            "clts": CheckboxInput(attrs={"width": 100, "clear": "left", "chkrelated": "clts_agency, clts_name, clts_phone, clts_phone_ext", "twocols": 1}),
            "clts_agency": TextInput(attrs={"clear": "left", "width": 35, "twocols": 1}),
            "clts_name": TextInput(attrs={"width": 35, "twocols": 1}),
            "clts_phone": TextInput(attrs={"width": 20, "class": "phone", "twocols": 1}),
            "clts_phone_ext": TextInput(attrs={"width": 10, "twocols": 1}),
            "fc": CheckboxInput(attrs={"width": 100, "clear": "left", "chkrelated": "fc_agency, fc_name, fc_phone, fc_phone_ext", "twocols": 1}),
            "fc_agency": TextInput(attrs={"clear": "left", "width": 35, "twocols": 1}),
            "fc_name": TextInput(attrs={"width": 35, "twocols": 1}),
            "fc_phone": TextInput(attrs={"width": 20, "class": "phone", "twocols": 1}),
            "fc_phone_ext": TextInput(attrs={"width": 10, "twocols": 1}),
            "iris": CheckboxInput(attrs={"width": 100, "clear": "left", "chkrelated": "iris_agency, iris_name, iris_phone, iris_phone_ext", "twocols": 1}),
            "iris_agency": TextInput(attrs={"clear": "left", "width": 35, "twocols": 1}),
            "iris_name": TextInput(attrs={"width": 35, "twocols": 1}),
            "iris_phone": TextInput(attrs={"width": 20, "class": "phone", "twocols": 1}),
            "iris_phone_ext": TextInput(attrs={"width": 10, "twocols": 1}),
            "cps": CheckboxInput(attrs={"width": 100, "clear": "left", "chkrelated": "cps_agency, cps_name, cps_phone, cps_phone_ext", "twocols": 2}),
            "cps_agency": TextInput(attrs={"clear": "left", "width": 35, "twocols": 2}),
            "cps_name": TextInput(attrs={"width": 35, "twocols": 2}),
            "cps_phone": TextInput(attrs={"width": 20, "class": "phone", "twocols": 2}),
            "cps_phone_ext": TextInput(attrs={"width": 10, "twocols": 2}),
            "jj": CheckboxInput(attrs={"width": 100, "clear": "left", "chkrelated": "jj_agency, jj_name, jj_phone, jj_phone_ext", "twocols": 2}),
            "jj_agency": TextInput(attrs={"clear": "left", "width": 35, "twocols": 2}),
            "jj_name": TextInput(attrs={"width": 35, "twocols": 2}),
            "jj_phone": TextInput(attrs={"width": 20, "class": "phone", "twocols": 2}),
            "jj_phone_ext": TextInput(attrs={"width": 10, "twocols": 2}),
            "aps": CheckboxInput(attrs={"width": 100, "clear": "left", "chkrelated": "aps_agency, aps_name, aps_phone, aps_phone_ext", "twocols": 2}),
            "aps_agency": TextInput(attrs={"clear": "left", "width": 35, "twocols": 2}),
            "aps_name": TextInput(attrs={"width": 35, "twocols": 2}),
            "aps_phone": TextInput(attrs={"width": 20, "class": "phone", "twocols": 2}),
            "aps_phone_ext": TextInput(attrs={"width": 10, "twocols": 2}),
            "adrc": CheckboxInput(attrs={"width": 100, "clear": "left", "chkrelated": "adrc_agency, adrc_name, adrc_phone, adrc_phone_ext", "twocols": 2}),
            "adrc_agency": TextInput(attrs={"clear": "left", "width": 35, "twocols": 2}),
            "adrc_name": TextInput(attrs={"width": 35, "twocols": 2}),
            "adrc_phone": TextInput(attrs={"width": 20, "class": "phone", "twocols": 2}),
            "adrc_phone_ext": TextInput(attrs={"width": 10, "twocols": 2}),
            "other_pa": CheckboxInput(attrs={"width": 100, "clear": "left", "chkrelated": "other_pa_agency, other_pa_name, other_pa_phone, other_pa_phone_ext", "twocols": 2}),
            "other_pa_agency": TextInput(attrs={"clear": "left", "width": 35, "twocols": 2}),
            "other_pa_name": TextInput(attrs={"width": 35, "twocols": 2}),
            "other_pa_phone": TextInput(attrs={"width": 20, "class": "phone", "twocols": 2}),
            "other_pa_phone_ext": TextInput(attrs={"width": 10, "twocols": 2}),



            "supporting_docs": Textarea(attrs={"width": 100, "rows": 2}),
            "linked_docs": SelectMultiple(attrs={
                "clear": "both",
                "width": "100%",
                "use_popup_lookup": "EmrForm",
                "popup_lookup_child_model": "EmrDoc",
            }),

        }

        labels = {
            "date": "Date",
        }

















class EmrRandiIntakeFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrRandiIntakeFormForm, self).__init__(*args, **kwargs)    

        self.fields["date"].label = "Date"    
        self.fields["date"].required = False

        if kwargs.get("instance") and kwargs["instance"].is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)



    class Meta:
        model = EmrRandiIntakeForm

        fields = [
            "date",
            "info_from",
            "contact_name",
            "contact_phone",
            "contact_phone_ext",
            "contact_notes",

            "info_from2",
            "contact_name2",
            "contact_phone2",
            "contact_phone_ext2",
            "contact_notes2",

            "info_from3",
            "contact_name3",
            "contact_phone3",
            "contact_phone_ext3",
            "contact_notes3",

            "info_from4",
            "contact_name4",
            "contact_phone4",
            "contact_phone_ext4",
            "contact_notes4",

            "info_from5",
            "contact_name5",
            "contact_phone5",
            "contact_phone_ext5",
            "contact_notes5",

            # Diagnosis information
            "diagnosis1", "age_of_onset1", "source1",
            "diagnosis2", "age_of_onset2", "source2",
            "diagnosis3", "age_of_onset3", "source3",
            "diagnosis4", "age_of_onset4", "source4",
            "diagnosis5", "age_of_onset5", "source5",
            "diagnosis6", "age_of_onset6", "source6",
            "diagnosis7", "age_of_onset7", "source7",
            "diagnosis8", "age_of_onset8", "source8",
            "diagnosis9", "age_of_onset9", "source9",
            "diagnosis10", "age_of_onset10", "source10",
            "diagnosis11", "age_of_onset11", "source11",
            "diagnosis12", "age_of_onset12", "source12",
            "diag_adtl",

            # Current or past treatments            

            "treat_ccs",
            "treat_ccs_dtl",            
            "treat_ch51",
            "treat_ch51_dtl",
            "treat_csp",
            "treat_csp_dtl",
            "treat_cst",
            "treat_cst_dtl",
            "treat_hospital_in",
            "treat_hospital_in_dtl",
            "treat_mh_out",
            "treat_mh_out_dtl",
            "treat_mh_res",
            "treat_mh_res_dtl",
            "treat_psych",
            "treat_psych_dtl",
            "treat_sa_detox",
            "treat_sa_detox_dtl",
            "treat_sa_out",
            "treat_sa_out_dtl",
            "treat_sa_res",
            "treat_sa_res_dtl",
            "treat_current_provider",
            "treat_current_provider_dtl",
            "treat_medication",
            "treat_medication_dtl",
            "treat_other",
            "treat_other_dtl",
            "treat_adtl_info",

            # Substance Use
            "sa_none_reported",
            "sa_alcohol",
            "sa_alcohol_dtl",
            "sa_tobacco",
            "sa_tobacco_dtl",
            "sa_script",
            "sa_script_dtl",
            "sa_benzos",
            "sa_benzos_dtl",
            "sa_cocaine",
            "sa_cocaine_dtl",
            "sa_barb",
            "sa_barb_dtl",
            "sa_crack",
            "sa_crack_dtl",
            "sa_hallucinogen",
            "sa_hallucinogen_dtl",
            "sa_opiate",
            "sa_opiate_dtl",
            "sa_inhale",
            "sa_inhale_dtl",
            "sa_marijuana",
            "sa_marijuana_dtl",
            "sa_amphetamine",
            "sa_amphetamine_dtl",
            "sa_meth",
            "sa_meth_dtl",
            "sa_iv",
            "sa_iv_dtl",
            "sa_caffeine",
            "sa_caffeine_dtl",
            "sa_withdrawal",
            "sa_other",
            "sa_other_dtl",
            "sa_adtl_info",


            # Mental Health
            "mh_aggression",
            "mh_aggression_dtl",
            "mh_agitation",
            "mh_agitation_dtl",
            "mh_anxiety",
            "mh_anxiety_dtl",
            "mh_chronic_pain",
            "mh_chronic_pain_dtl",
            "mh_delusions",
            "mh_delusions_dtl",
            "mh_depressed",
            "mh_depressed_dtl",
            "mh_concentration",
            "mh_concentration_dtl",
            "mh_eating_pattern",
            "mh_eating_pattern_dtl",
            "mh_dissociation",
            "mh_dissociation_dtl",
            "mh_body_image",
            "mh_body_image_dtl",
            "mh_elevated_mood",
            "mh_elevated_mood_dtl",
            "mh_fatigue",
            "mh_fatigue_dtl",
            "mh_guilt",
            "mh_guilt_dtl",
            "mh_helpless",
            "mh_helpless_dtl",
            "mh_hopeless",
            "mh_hopeless_dtl",
            "mh_lonely",
            "mh_lonely_dtl",
            "mh_worthless",
            "mh_worthless_dtl",
            "mh_gender_id",
            "mh_gender_id_dtl",
            "mh_hallucinations",
            "mh_hallucinations_dtl",
            "mh_homicide",
            "mh_homicide_dtl",
            "mh_hyper",
            "mh_hyper_dtl",
            "mh_hypersex",
            "mh_hypersex_dtl",
            "mh_impulse",
            "mh_impulse_dtl",
            "mh_inattention",
            "mh_inattention_dtl",
            "mh_irritable",
            "mh_irritable_dtl",
            "mh_loss_interest",
            "mh_loss_interest_dtl",
            "mh_low_esteem",
            "mh_low_esteem_dtl",
            "mh_malingering",
            "mh_malingering_dtl",
            "mh_mania",
            "mh_mania_dtl",
            "mh_memory",
            "mh_memory_dtl",
            "mh_mood",
            "mh_mood_dtl",
            "mh_nightmares",
            "mh_nightmares_dtl",
            "mh_self_injury",
            "mh_self_injury_dtl",
            "mh_obsessive",
            "mh_obsessive_dtl",
            "mh_other_substance",
            "mh_other_substance_dtl",
            "mh_occupational",
            "mh_occupational_dtl",
            "mh_paranoia",
            "mh_paranoia_dtl",
            "mh_hygene",
            "mh_hygene_dtl",
            "mh_restless",
            "mh_restless_dtl",
            "mh_risky",
            "mh_risky_dtl",
            "mh_school_work",
            "mh_school_work_dtl",
            "mh_sexual_problems",
            "mh_sexual_problems_dtl",
            "mh_sleep",
            "mh_sleep_dtl",
            "mh_fears",
            "mh_fears_dtl",
            "mh_suicide",
            "mh_suicide_dtl",
            "mh_thought_disorg",
            "mh_thought_disorg_dtl",
            "mh_thought_block",
            "mh_thought_block_dtl",
            "mh_weight_change",
            "mh_weight_change_dtl",
            "mh_other",
            "mh_other_dtl",
            "mh_adtl_info",

            # Pregnancy status
            "pregnant",
            "pregnant_due",
            "pregnant_adtl_info",

            # percived needs of client according to client or client's contacts
            "pn_behavior_support",
            "pn_behavior_support_dtl",
            "pn_coping_skill",
            "pn_coping_skill_dtl",
            "pn_employment",
            "pn_employment_dtl",
            "pn_community",
            "pn_community_dtl",
            "pn_life_skills",
            "pn_life_skills_dtl",
            "pn_med_monitor",
            "pn_med_monitor_dtl",
            "pn_mh_counseling",
            "pn_mh_counseling_dtl",
            "pn_personal_care",
            "pn_personal_care_dtl",
            "pn_prescribe",
            "pn_prescribe_dtl",
            "pn_psy_edu",
            "pn_psy_edu_dtl",
            "pn_sub_use_counsel",
            "pn_sub_use_counsel_dtl",
            "pn_sub_use_res",
            "pn_sub_use_res_dtl",
            "pn_transport",
            "pn_transport_dtl",
            "pn_other",
            "pn_other_dtl",
            "pn_adtl_info",



            # Living situation
            "living_situation",
            "ls_adtl_info",


            # Other program affiliation

            "op_probation",
            "op_probation_dtl",
            "op_candf",
            "op_candf_dtl",
            "op_public_health",
            "op_public_health_dtl",
            "op_aging",
            "op_aging_dtl",
            "op_treat_court",
            "op_treat_court_dtl",
            "op_school_district",
            "op_school_district_dtl",
            "op_day_treat",
            "op_day_treat_dtl",
            "op_defer_explu",
            "op_defer_explu_dtl",
            "op_managed_care",
            "op_managed_care_dtl",
            "op_mat",
            "op_mat_dtl",
            "op_tribal_afil",
            "op_tribal_afil_dtl",
            "op_other",
            "op_other_dtl",
            "op_adtl_info",

            "diagnosis1",
            "diagnosis2",
            "diagnosis3",
            "diagnosis4",
            "diagnosis5",
            "diagnosis6",
            "diagnosis7",
            "diagnosis8",
            "diagnosis9",
            "diagnosis10",
            "diagnosis11",
            "diagnosis12",
            "linked_docs",
        ]      

        # Do not need to link these up, the emr forms pretty much use their
        # own fields and don't need the base ones. If they need to be included
        # we can just add them to the list individually
        #fields = emr_form_fields + fields

        widgets = {
            "date": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": "auto", "clear": "both", "class": "datepicker"}),
            "info_from": Select(attrs={"width": 15, "expfield": "info_from, contact_name, contact_phone, contact_phone_ext, contact_notes", "expcount": 5}),
            "contact_name": TextInput(attrs={"width": 25}),
            "contact_phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "contact_phone_ext": TextInput(attrs={"width": 5, "class": "optional-off"}),
            "contact_notes": TextInput(attrs={"width": 40}),

            "info_from2": Select(attrs={"width": 15}),
            "contact_name2": TextInput(attrs={"width": 25}),
            "contact_phone2": TextInput(attrs={"width": 15, "class": "phone"}),
            "contact_phone_ext2": TextInput(attrs={"width": 5, "class": "optional-off"}),
            "contact_notes2": TextInput(attrs={"width": 40}),

            "info_from3": Select(attrs={"width": 15}),
            "contact_name3": TextInput(attrs={"width": 25}),
            "contact_phone3": TextInput(attrs={"width": 15, "class": "phone"}),
            "contact_phone_ext3": TextInput(attrs={"width": 5, "class": "optional-off"}),
            "contact_notes3": TextInput(attrs={"width": 40}),

            "info_from4": Select(attrs={"width": 15}),
            "contact_name4": TextInput(attrs={"width": 25}),
            "contact_phone4": TextInput(attrs={"width": 15, "class": "phone"}),
            "contact_phone_ext4": TextInput(attrs={"width": 5, "class": "optional-off"}),
            "contact_notes4": TextInput(attrs={"width": 40}),

            "info_from5": Select(attrs={"width": 15}),
            "contact_name5": TextInput(attrs={"width": 25}),
            "contact_phone5": TextInput(attrs={"width": 15, "class": "phone"}),
            "contact_phone_ext5": TextInput(attrs={"width": 5, "class": "optional-off"}),
            "contact_notes5": TextInput(attrs={"width": 40}), 


            # Diagnosis info
            "diagnosis1": TextInput(attrs={"width": 45, "expfield": "diagnosis, age_of_onset, source", "expcount": 12}),
            "age_of_onset1": NumberInput(attrs={"width": 10}),
            "source1": TextInput(attrs={"width": 45}),

            "diagnosis2": TextInput(attrs={"width": 45}),
            "age_of_onset2": NumberInput(attrs={"width": 10}),
            "source2": TextInput(attrs={"width": 45}),

            "diagnosis3": TextInput(attrs={"width": 45}),
            "age_of_onset3": NumberInput(attrs={"width": 10}),
            "source3": TextInput(attrs={"width": 45}),

            "diagnosis4": TextInput(attrs={"width": 45}),
            "age_of_onset4": NumberInput(attrs={"width": 10}),
            "source4": TextInput(attrs={"width": 45}),

            "diagnosis5": TextInput(attrs={"width": 45}),
            "age_of_onset5": NumberInput(attrs={"width": 10}),
            "source5": TextInput(attrs={"width": 45}),

            "diagnosis6": TextInput(attrs={"width": 45}),
            "age_of_onset6": NumberInput(attrs={"width": 10}),
            "source6": TextInput(attrs={"width": 45}),

            "diagnosis7": TextInput(attrs={"width": 45}),
            "age_of_onset7": NumberInput(attrs={"width": 10}),
            "source7": TextInput(attrs={"width": 45}),

            "diagnosis8": TextInput(attrs={"width": 45}),
            "age_of_onset8": NumberInput(attrs={"width": 10}),
            "source8": TextInput(attrs={"width": 45}),

            "diagnosis9": TextInput(attrs={"width": 45}),
            "age_of_onset9": NumberInput(attrs={"width": 10}),
            "source9": TextInput(attrs={"width": 45}),

            "diagnosis10": TextInput(attrs={"width": 45}),
            "age_of_onset10": NumberInput(attrs={"width": 10}),
            "source10": TextInput(attrs={"width": 45}),

            "diagnosis11": TextInput(attrs={"width": 45}),
            "age_of_onset11": NumberInput(attrs={"width": 10}),
            "source11": TextInput(attrs={"width": 45}),

            "diagnosis12": TextInput(attrs={"width": 45}),  
            "age_of_onset12": NumberInput(attrs={"width": 10}),
            "source12": TextInput(attrs={"width": 45}),

            "diag_adtl": Textarea(attrs={"rows": 2, "width": 100}),

            # current or past treatments
            "treat_ccs": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "treat_ccs_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "treat_ch51": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "treat_ch51_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "treat_csp": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "treat_csp_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "treat_cst": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "treat_cst_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "treat_hospital_in": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "treat_hospital_in_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "treat_mh_out": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "treat_mh_out_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "treat_mh_res": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "treat_mh_res_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "treat_psych": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "treat_psych_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "treat_sa_detox": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "treat_sa_detox_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "treat_sa_out": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "treat_sa_out_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "treat_sa_res": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "treat_sa_res_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "treat_current_provider": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "treat_current_provider_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "treat_medication": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "treat_medication_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "treat_other": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "treat_other_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "treat_adtl_info": Textarea(attrs={"rows": 2, "width": 100}),


            # Substance Use
            "sa_none_reported": CheckboxInput(attrs={"width": 100, "twocols": 1}),
            "sa_alcohol": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "sa_alcohol_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "sa_tobacco": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "sa_tobacco_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "sa_script": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "sa_script_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "sa_benzos": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "sa_benzos_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "sa_cocaine": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "sa_cocaine_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "sa_barb": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "sa_barb_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "sa_crack": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "sa_crack_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "sa_hallucinogen": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "sa_hallucinogen_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "sa_opiate": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "sa_opiate_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "sa_inhale": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "sa_inhale_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "sa_marijuana": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "sa_marijuana_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "sa_amphetamine": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "sa_amphetamine_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "sa_meth": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "sa_meth_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "sa_iv": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "sa_iv_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "sa_caffeine": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "sa_caffeine_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "sa_other": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "sa_other_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),

            "sa_withdrawal": Textarea(attrs={"width": 100, "rows": 2}),            
            "sa_adtl_info": Textarea(attrs={"width": 100, "rows": 2}),



            # Mental Health
            "mh_aggression": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_aggression_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_agitation": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_agitation_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_anxiety": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_anxiety_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_chronic_pain": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_chronic_pain_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_delusions": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_delusions_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_depressed": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_depressed_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_concentration": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_concentration_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_eating_pattern": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_eating_pattern_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_dissociation": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_dissociation_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_body_image": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_body_image_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_elevated_mood": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_elevated_mood_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_fatigue": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_fatigue_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_guilt": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_guilt_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_helpless": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_helpless_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_hopeless": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_hopeless_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_lonely": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_lonely_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_worthless": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_worthless_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_gender_id": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_gender_id_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_hallucinations": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_hallucinations_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_homicide": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_homicide_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_hyper": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_hyper_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_hypersex": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_hypersex_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_drug_use": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_drug_use_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_impulse": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_impulse_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_inattention": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_inattention_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_irritable": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "mh_irritable_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "mh_loss_interest": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_loss_interest_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_low_esteem": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_low_esteem_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_malingering": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_malingering_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_mania": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_mania_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_memory": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_memory_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_mood": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_mood_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_nightmares": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_nightmares_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_self_injury": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_self_injury_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_obsessive": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_obsessive_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_other_substance": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_other_substance_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_occupational": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_occupational_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_paranoia": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_paranoia_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_hygene": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_hygene_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_restless": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_restless_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_risky": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_risky_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_school_work": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_school_work_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_sexual_problems": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_sexual_problems_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_sleep": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_sleep_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_fears": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_fears_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_suicide": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_suicide_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_thought_disorg": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_thought_disorg_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_thought_block": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_thought_block_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_weight_change": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_weight_change_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_other": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "mh_other_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "mh_adtl_info": Textarea(attrs={"rows": 2, "width": 100}),

            # Pregnancy Status
            "pregnant": Select(attrs={
                "width": 25,
                "selectshow": "pregnant|pregnant_due, pregnant_adtl_info",
            }),
            "pregnant_due": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": "auto", "class": "datepicker"}),
            "pregnant_adtl_info": Textarea(attrs={"width": 100, "rows": 2}),

            # perceived needs of client according to client or client's contacts
            "pn_behavior_support": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "pn_behavior_support_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "pn_coping_skill": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "pn_coping_skill_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "pn_employment": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "pn_employment_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "pn_community": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "pn_community_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "pn_life_skills": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "pn_life_skills_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "pn_med_monitor": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "pn_med_monitor_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "pn_mh_counseling": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "pn_mh_counseling_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 1}),
            "pn_personal_care": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "pn_personal_care_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "pn_prescribe": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "pn_prescribe_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "pn_psy_edu": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "pn_psy_edu_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "pn_sub_use_counsel": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "pn_sub_use_counsel_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "pn_sub_use_res": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "pn_sub_use_res_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "pn_transport": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "pn_transport_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "pn_other": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "pn_other_dtl": Textarea(attrs={"width": 100, "rows": 2, "twocols": 2}),
            "pn_adtl_info": Textarea(attrs={"rows": 2, "width": 100}),



            # Living situation
            "living_situation": Select(attrs={"width": 50, "clear": "both"}),
            "ls_adtl_info": Textarea(attrs={"rows": 2, "width": 100}),

            # Other program affiliation
            "op_probation": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "op_probation_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "op_candf": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "op_candf_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "op_public_health": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "op_public_health_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "op_aging": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "op_aging_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "op_treat_court": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "op_treat_court_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "op_school_district": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 1}),
            "op_school_district_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 1}),
            "op_day_treat": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "op_day_treat_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "op_defer_explu": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "op_defer_explu_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "op_managed_care": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "op_managed_care_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "op_mat": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "op_mat_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "op_tribal_afil": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "op_tribal_afil_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "op_other": CheckboxInput(attrs={"width": 100, "chkrelated": True, "twocols": 2}),
            "op_other_dtl": Textarea(attrs={"rows": 2, "width": 100, "twocols": 2}),
            "op_adtl_info": Textarea(attrs={"rows": 2, "width": 100}),



            "linked_docs": SelectMultiple(attrs={
                "clear": "both",
                "width": "100%",
                "use_popup_lookup": "EmrForm",
                "popup_lookup_child_model": "EmrDoc",
            }),
        }









class EmrRandiExternalReferralFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrRandiExternalReferralFormForm, self).__init__(*args, **kwargs)



        # Make fields not required based on what form the user is filling out
        self.fields["date"].required = False
        self.fields["date"].label = "Date"

        if kwargs.get("instance") and kwargs["instance"].is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)




    class Meta:
        model = EmrRandiExternalReferralForm

        fields = [
            "date",
            "ref_source",
            "ref_agency_name",
            "ref_name",
            "ref_phone",
            "ref_phone_ext",
            "ref_email",
            "ref_other_name",
            "ref_other_relation",
            "legal_status",
            "guardian_name",
            "guardian_phone",
            "guardian_phone_ext",
            "ref_for_out",
            "ref_for_out_mh",
            "ref_for_out_su",
            "ref_for_other",
            "ref_reason",
            "linked_docs"
        ]

        # Optionally, include the fields for the generic EMR form
        #fields = emr_form_fields + fields

        # Some of these forms may require different data in the service summary
        # this is how to remove a field from a form that doesn't need it
        #fields.remove("date")


        widgets = {
            "date": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": "auto", "clear": "both", "class": "datepicker"}),
            "ref_source": Select(attrs={
                "width": 15,
                "clear": "both",
                "selectshow": "agency|ref_name, ref_agency_name, ref_phone, ref_phone_ext, ref_email;"
                              "parent|ref_name, ref_agency_name, ref_phone, ref_phone_ext, ref_email;"
                              "other|ref_other_name, ref_other_relation",

            }),
            "ref_agency_name": TextInput(attrs={"width": 20}),
            "ref_name": TextInput(attrs={"width": 20}),
            "ref_phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "ref_phone_ext": TextInput(attrs={"width": 5, "class": "optional-off"}),
            "ref_email": TextInput(attrs={"width": 25}),

            "ref_other_name": TextInput(),
            "ref_other_relation": TextInput(),

            "legal_status": Select(attrs={
                "width": 25,
                "clear": "left",
                "selectshow": "guardian|guardian_name, guardian_phone, guardian_phone_ext;"
                              "poa_hc|guardian_name, guardian_phone, guardian_phone_ext;"
                              "minor|guardian_name, guardian_phone, guardian_phone_ext"
            }),
            "guardian_name": TextInput(attrs={"width": 50}),
            "guardian_phone": TextInput(attrs={"width": 20, "class": "phone"}),
            "guardian_phone_ext": TextInput(attrs={"width": 5, "class": "optional-off"}),
            "ref_for_out": CheckboxInput(attrs={"width": 100}),
            "ref_for_out_mh": CheckboxInput(attrs={"width": 100, "ml": 2}),
            "ref_for_out_su": CheckboxInput(attrs={"width": 100, "ml": 2}),
            "ref_for_other": CheckboxInput(attrs={"width": 100}),
            "ref_reason": Textarea(attrs={"width": 100, "rows": 2}),
            "linked_docs": SelectMultiple(attrs={
                "clear": "both",
                "width": "100%",
                "use_popup_lookup": "EmrForm",
                "popup_lookup_child_model": "EmrDoc",
            }),
        }












class EmrRandiReferralFormForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrRandiReferralFormForm, self).__init__(*args, **kwargs)

        self.fields["date"].label = "Date"
        self.fields["date"].required = False

        if kwargs.get("instance") and kwargs["instance"].is_finalized is True:
            render_form(self, hide_all_optional=True, locked=True)
        else:
            render_form(self, hide_all_optional=True)


    class Meta:
        model = EmrRandiReferralForm

        fields = [
            "date",
            "ref_worker",
            "legal_status",
            "guardian_name",
            "guardian_phone",
            "guardian_phone_ext",
            "refto_op_bh",
            "refto_mh",
            "refto_sa",
            "refto_ccs",
            "refto_csp",
            "refto_clts",
            "refto_clts_preadmit",
            "refto_cst",
            "refto_crisis",
            "refto_res_aoda",
            "expected_outcomes",
            "linked_docs",
        ]      



        widgets = {
            "date": DateInput(format=settings.DATE_FORMAT, attrs={
                "width": "auto", "clear": "both", "class": "datepicker"}),
            "ref_worker": TextInput(attrs={"width": 50, "clear": "left"}),
            "legal_status": Select(attrs={
                "width": 20,
                "clear": "left",
                "selectshow": "guardian|guardian_name, guardian_phone, guardian_phone_ext;"
                              "poa_hc|guardian_name, guardian_phone, guardian_phone_ext;"
                              "minor|guardian_name, guardian_phone, guardian_phone_ext;",
            }),
            "guardian_name": TextInput(attrs={"width": 50}),
            "guardian_phone": TextInput(attrs={"width": 20, "class": "phone"}),
            "guardian_phone_ext": TextInput(attrs={"width": 10, "class": "optional-off"}),
            "refto_op_bh": CheckboxInput(attrs={"width": 100}),
            "refto_mh": CheckboxInput(attrs={"width": 100, "ml": 4}),
            "refto_sa": CheckboxInput(attrs={"width": 100, "ml": 4}),
            # These widths are coded into the form's template at internal_referral.html
            "refto_ccs": CheckboxInput(attrs={}),
            "refto_csp": CheckboxInput(attrs={}),
            "refto_clts": CheckboxInput(attrs={}),
            "refto_clts_preadmit": CheckboxInput(attrs={}),
            "refto_cst": CheckboxInput(attrs={}),
            "refto_crisis": CheckboxInput(attrs={}),
            "refto_res_aoda": CheckboxInput(attrs={}),
            "expected_outcomes": Textarea(attrs={"rows": 2, "width": 100}),
            "linked_docs": SelectMultiple(attrs={
                "clear": "both",
                "width": "100%",
                "use_popup_lookup": "EmrForm",
                "popup_lookup_child_model": "EmrDoc",
            }),
        }










emr_episode_widgets = {
        "episode_type": HiddenInput(),
        "client": HiddenInput(),
        "opened_by": HiddenInput(),
        "closed_by": HiddenInput(),
        "start": DateInput(format=settings.DATE_FORMAT, attrs={
            'width': "12rem", 'class': 'datepicker'}),
        "end": DateInput(format=settings.DATE_FORMAT, attrs={
            'width': "12rem", 'class': 'datepicker'}),
        "clts_target_group": Select(attrs={"width": 100, "clear": "left"}),
        "closing_reason": Select(attrs={"width": 100, "clear": "left"}),        
        "notes": Textarea(attrs={"rows": 2, "width": 100}),
}

'''
class EmrEpisodeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(EmrEpisodeForm, self).__init__(*args, **kwargs)
        render_form(self)



    class Meta:
        model = EmrEpisode
        #fields = ["start", "end", "closing_reason", "notes"]
        exclude = []
        widgets = emr_episode_widgets
'''





class EmrClinicPreadmitEpisodeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrClinicPreadmitEpisodeForm, self).__init__(*args, **kwargs)
        render_form(self)


    def clean(self):
        self.cleaned_data = super(EmrClinicPreadmitEpisodeForm, self).clean()
        validate_emr_episode(self)


    class Meta:
        model = EmrClinicPreadmitEpisode
        fields = ["start", "end", "closing_reason", "notes"]

        widgets = {
            #"closing_reason2": Select(attrs={"width": 100, "clear": "left"}),
        }

        widgets = {**widgets, **emr_episode_widgets}



class EmrCltsPreadmitEpisodeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrCltsPreadmitEpisodeForm, self).__init__(*args, **kwargs)
        render_form(self)

    def clean(self):
        self.cleaned_data = super(EmrCltsPreadmitEpisodeForm, self).clean()
        validate_emr_episode(self)

    class Meta:
        model = EmrCltsPreadmitEpisode
        fields = ["start", "end", "closing_reason", "notes"]

        widgets = {

        }

        widgets = {**widgets, **emr_episode_widgets}



class EmrCltsEpisodeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrCltsEpisodeForm, self).__init__(*args, **kwargs)
        render_form(self)

    def clean(self):
        self.cleaned_data = super(EmrCltsEpisodeForm, self).clean()
        validate_emr_episode(self)

    class Meta:
        model = EmrCltsEpisode
        fields = ["start", "end", "clts_target_group", "closing_reason", "notes"]

        widgets = {

        }

        widgets = {**widgets, **emr_episode_widgets}



class EmrCrisisEpisodeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrCrisisEpisodeForm, self).__init__(*args, **kwargs)
        render_form(self)

    def clean(self):
        self.cleaned_data = super(EmrCrisisEpisodeForm, self).clean()
        validate_emr_episode(self)

    class Meta:
        model = EmrCrisisEpisode
        fields = ["start", "end", "closing_reason", "pps_mh_end_reason", "notes"]

        widgets = {
            "pps_mh_end_reason": Select(attrs={"width": 100})
        }

        widgets = {**widgets, **emr_episode_widgets}



class EmrCstEpisodeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrCstEpisodeForm, self).__init__(*args, **kwargs)
        render_form(self)

    def clean(self):
        self.cleaned_data = super(EmrCstEpisodeForm, self).clean()
        validate_emr_episode(self)

    class Meta:
        model = EmrCstEpisode
        fields = ["start", "end", "closing_reason", "notes"]

        widgets = {

        }

        widgets = {**widgets, **emr_episode_widgets}



class EmrRandiEpisodeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrRandiEpisodeForm, self).__init__(*args, **kwargs)
        render_form(self)

    def clean(self):
        self.cleaned_data = super(EmrRandiEpisodeForm, self).clean()
        validate_emr_episode(self)

    class Meta:
        model = EmrRandiEpisode
        fields = [
            "start",
            "end",
            "closing_reason",
            "closing_reason2",
            "closing_reason3",
            "notes"
        ]

        widgets = {

        }

        widgets = {**widgets, **emr_episode_widgets}



class EmrTcmEpisodeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrTcmEpisodeForm, self).__init__(*args, **kwargs)
        render_form(self)

    def clean(self):
        self.cleaned_data = super(EmrTcmEpisodeForm, self).clean()
        validate_emr_episode(self)

    class Meta:
        model = EmrTcmEpisode
        fields = ["start", "end", "closing_reason", "pps_mh_end_reason", "notes"]

        widgets = {
            "pps_mh_end_reason": Select(attrs={"width": 100})
        }

        widgets = {**widgets, **emr_episode_widgets}



class EmrB3EpisodeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrB3EpisodeForm, self).__init__(*args, **kwargs)
        render_form(self)

    def clean(self):
        self.cleaned_data = super(EmrB3EpisodeForm, self).clean()
        validate_emr_episode(self)

    class Meta:
        model = EmrB3Episode
        fields = ["start", "end", "closing_reason", "notes"]

        widgets = {

        }

        widgets = {**widgets, **emr_episode_widgets}


class EmrB3PreadmitEpisodeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.locked = kwargs.pop("locked", False)
        super(EmrB3PreadmitEpisodeForm, self).__init__(*args, **kwargs)
        render_form(self)

    def clean(self):
        self.cleaned_data = super(EmrB3PreadmitEpisodeForm, self).clean()
        validate_emr_episode(self)

    class Meta:
        model = EmrB3PreadmitEpisode
        fields = ["start", "end", "closing_reason", "notes"]

        widgets = {

        }

        widgets = {**widgets, **emr_episode_widgets}










class AbilityToPayForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AbilityToPayForm, self).__init__(*args, **kwargs)
        render_form(self)


    def clean(self):
        self.clean_data = super(AbilityToPayForm, self).clean()
        # Check if type and number already exist.
        validate_ability_to_pay(self)
        return self.clean_data


    class Meta:
        model = AbilityToPay
        fields = (
            'client', 'month', 'year', 'amount'
        )

        widgets = {
            'client': HiddenInput(),
            'month': Select(attrs={"width": 33}),
            "year": NumberInput(attrs={"width": 33}),
            "amount": NumberInput(attrs={"width": 33}),
        }



class EditClientForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(EditClientForm, self).__init__(*args, **kwargs)
        render_form(self)

        self.id = None  # going to be using the FORM id to check for duplicates below
        if 'instance' in kwargs and hasattr(kwargs['instance'], 'id'):
            self.id = kwargs['instance'].id

        #self.fields['active'].required = True

    def clean(self):
        cleaned_data = super(EditClientForm, self).clean()
        first_name = cleaned_data.get('first_name')
        last_name = cleaned_data.get('last_name')
        birthdate = cleaned_data.get('birthdate')

        validate_birthdate(birthdate, self)
        validate_client_name(self.id, first_name, last_name, birthdate, self)


    class Meta:
        model = Client
        fields = (
            'last_name',
            'first_name',
            'middle_name',
            'alias',
            'title',
            'suffix',
            'birthdate',
            "ssn",
            'gender',
            "gender_identity",

            'race',
            'hispanic',            
            "phone",
            "phone_ext",
            "alt_phone",
            "alt_phone_ext",
            "email", 
            'parent_guardian_name',      

        )

        labels = {

        }

        widgets = {
            'last_name': TextInput(attrs={'width': 20}),
            'first_name': TextInput(attrs={'width': 20}),
            'middle_name': TextInput(attrs={'width': 20}),
            'alias': TextInput(attrs={"width": 20}),
            'title': Select(attrs={'width': 10}),
            'suffix': Select(attrs={'width': 10}),
            'birthdate': DateInput(
                format=settings.DATE_FORMAT, 
                attrs={'class': 'datepicker', 'width': 15}
            ),
            "ssn": TextInput(attrs={"width": 15}),
            'gender': Select(attrs={'width': 15}),
            'gender_identity': Select(attrs={'width': 15}),
            'race': Select(attrs={'width': 20}),
            'hispanic': Select(attrs={'width': 20}),

            "phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "phone_ext": TextInput(attrs={"width": 5, "class": "optional-off"}),
            "alt_phone": TextInput(attrs={"width": 15, "class": "phone"}),
            "alt_phone_ext": TextInput(attrs={"width": 5, "class": "optional-off"}),
            "email": EmailInput(attrs={"width": 30}),
            "parent_guardian_name": TextInput(attrs={"width": 30}),

        }


class EditAddressForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(EditAddressForm, self).__init__(*args, **kwargs)
        render_form(self)


    class Meta:
        model = Address
        fields = (
            "client",
            "active", 
            "is_homeless",
            "is_billing",
            "is_mailing",
            "is_physical",  
            "care_of",
            "street",
            "apt",
            "city",
            "state",
            "zip",
            "note",
        )

        widgets = {
            "client": HiddenInput(),
            "active": CheckboxInput(attrs={"width": 25}), 
            "is_homeless": CheckboxInput(attrs={"width": 25, "clear": "left"}), 
            "is_billing": CheckboxInput(attrs={"width": 25}), 
            "is_mailing": CheckboxInput(attrs={"width": 25}), 
            "is_physical": CheckboxInput(attrs={"width": 25}), 
            "care_of": TextInput(attrs={"width": 100}),
            "street": TextInput(attrs={"width": 70, "id": "add_street"}),
            "apt": TextInput(attrs={"width": 30, "id": "add_apt"}),
            "city": TextInput(attrs={"width": 40, "id": "add_city"}),
            "state": Select(attrs={"width": 40, "id": "add_state"}),
            "zip": TextInput(attrs={"width": 20, "id": "add_zip"}),
            "county": Select(attrs={"id": "add_county"}),
            "note": Textarea(attrs={"rows": 2, "width": 100}),
        }





class ContactInfoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ContactInfoForm, self).__init__(*args, **kwargs)
        render_form(self)


    class Meta:
        model = ContactInfo
        fields = (
            "client",
            "active", 
            "type_of_contact", 
            "first_name",
            "last_name",
            "agency_name",
            "street",
            "street",
            "city",
            "state",
            "zipcode",
            "phone",
            "phone_ext",
            "alt_phone",
            "alt_phone_ext",
            "fax",
            "email",
            "notes",
        )

        widgets = {
            "client": HiddenInput(),
            "active": CheckboxInput(attrs={"width": 30}),
            "type_of_contact": Select(attrs={"width": 70}),
            "first_name": TextInput(attrs={"width": 50}),
            "last_name": TextInput(attrs={"width": 50}),
            "agency_name": TextInput(attrs={"width": 50}),
            "street": TextInput(attrs={"width": 50}),
            "apt": TextInput(attrs={"width": 30}),
            "city": TextInput(attrs={"width": 40}),
            "state": Select(attrs={"width": 40}),
            "zipcode": TextInput(attrs={"width": 20}),
            "phone": TextInput(attrs={"width": 25, "class": "phone"}),
            "phone_ext": TextInput(attrs={"width": 10, "class": "optional-off"}),
            "alt_phone": TextInput(attrs={"width": 25, "class": "phone"}),
            "alt_phone_ext": TextInput(attrs={"width": 10, "class": "optional-off"}),
            "fax": TextInput(attrs={"width": 30}),
            "email": TextInput(attrs={"width": 50}),
            "notes": Textarea(attrs={"rows": 4, "width": 100}),
        }




class InsuranceForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(InsuranceForm, self).__init__(*args, **kwargs)
        render_form(self)


    class Meta:
        model = Insurance
        fields = (
            "client",
            "insurance_type",
            "provider_name",
            "group_number",
            "subscriber_id",
            "notes",
        )

        widgets = {
            "client": HiddenInput(),
            "insurance_type": Select(attrs={"width": 50}),
            "provider_name": TextInput(attrs={"width": 100, "clear": "left"}),
            "group_number": TextInput(attrs={"width": 100}),
            "subscriber_id": TextInput(attrs={"width": 100}),
            "notes": Textarea(attrs={"rows": 2, "width": 100}),
        }



class EditAssignedStaffForm(ModelForm):    
    def __init__(self, *args, **kwargs):
        super(EditAssignedStaffForm, self).__init__(*args, **kwargs)
        render_form(self)
        staff = UserConfig.objects.filter(Q(user_type="1") | Q(user_type='2')).exclude(active=False)

        self.fields['assigned_staff'].queryset = staff 


    class Meta:
        model = Client
        fields = ("assigned_staff",)

        widgets = {
            'assigned_staff': SelectMultiple(),
        }



class CaseNoteForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CaseNoteForm, self).__init__(*args, **kwargs)

        if "instance" in kwargs:
            self.instance = kwargs.get("instance")

        try:
            self.is_open = kwargs.get('instance').is_open
        except AttributeError:
            self.is_open = False


        if self.instance.pps_epi_uuid:
            #import pdb; pdb.set_trace()
            # Adding some extra data for the lookup at runtime
            # this is pretty slick. Allows for better filtering on the lookup
            # in this case, we only want to see services for this specific
            # PPS Episode type (aoda, mh, core, or bto3)
            pps_epi = get_pps_epi(self.instance.pps_epi_uuid)
            self.fields["service_lookup"].widget.attrs["parent_mod"] = pps_epi.__class__.__name__
            self.fields["service_lookup"].widget.attrs["parent_id"] = pps_epi.pps_epi_uuid
            self.fields["service_lookup"].widget.attrs["parent_app"] = "client"

            self.fields["pps_start"].required = True
            self.fields["pps_start"].label = "PPS Start"
            self.fields["pps_start"].widget = DateInput(format=settings.DATE_FORMAT, attrs={'width': 25, 'clear': 'left', 'class': 'datepicker'})

            self.fields["pps_end"].required = False
            self.fields["pps_end"].label = "PPS End"
            self.fields["pps_end"].widget = DateInput(format=settings.DATE_FORMAT, attrs={'width': 25, 'class': 'datepicker'})

            self.fields["pps_units"].required = False
            self.fields["pps_units"].label = "PPS Units"
            self.fields["pps_units"].widget = NumberInput(attrs={'width': 25, })


            if pps_epi.__class__.__name__.lower() == "ppsaodaepi":
                self.fields["pps_aoda_target_group"].required = True
                self.fields["pps_aoda_target_group"].label = "Target Group"
                self.fields["pps_aoda_target_group"].widget = Select(attrs={"Width": 33})
                self.fields["pps_aoda_target_group"].widget.choices = PPS_C.AODA_TARGET_GROUP


                self.fields["pps_aoda_spc_end_reason"].required = False
                self.fields["pps_aoda_spc_end_reason"].label = "AODA Service End Reason"
                self.fields["pps_aoda_spc_end_reason"].widget = Select(attrs={"width": 50, "clear": "left"})
                self.fields["pps_aoda_spc_end_reason"].widget.choices = PPS_C.AODA_SPC_END_REASON

                self.fields["target_group"].widget = HiddenInput()


            elif pps_epi.__class__.__name__.lower() == "ppsmhepi":
                self.fields["pps_mh_spc_end_reason"].required = False
                self.fields["pps_mh_spc_end_reason"].label = "Mental Health Service End Reason"
                self.fields["pps_mh_spc_end_reason"].widget = Select(attrs={"width": 50, "clear": "left"})
                self.fields["pps_mh_spc_end_reason"].widget.choices = PPS_C.MH_SPC_END_REASON

            elif pps_epi.__class__.__name__.lower() == "ppscoreepi":
                self.fields["pps_core_target_group"].required = False
                self.fields["pps_core_target_group"].label = "Target Group"
                self.fields["pps_core_target_group"].widget = Select(attrs={"Width": 33})
                self.fields["pps_core_target_group"].widget.choices = PPS_C.CORE_TARGET_GROUP

                self.fields["target_group"].widget = HiddenInput()



            self.fields["note"].widget.attrs["rows"] = 3




        self.sig_required = get_app_config("casenote_requires_signature")
        if self.sig_required and self.is_open is True and self.instance.pps_epi_uuid is None:
            #self.fields["caseworker_signature"].widget = TextInput()
            self.fields.update({"electronic_signature": CharField(
                required=False,
                label="Electronic signature",
                widget=TextInput(attrs={
                    "class": "optional-off",
                    "width": 50,
                    "clear": "both",
                }),
            )})


        if self.is_open is False:
            render_form(self, locked=True)
        else:
            render_form(self, locked=False)



    client_lookup = CharField(label="Client", required=True, widget=TextInput(attrs={
        'width': 50,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name, birthdate',
        'order': 'name',
        'target': 'client',
        'autocomplete': 'off',
        "clear": "left",
    }))


    service_lookup = CharField(label="Service", required=True, widget=TextInput(attrs={
        'width': 33,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Service',
        'fields': 'name, spc_code, procedure_code',
        'order': 'code',
        'target': 'service',
        'autocomplete': 'off',
    }))


    diagnosis_lookup = CharField(label="Diagnosis", required=False, widget=TextInput(attrs={
        'width': 33,
        'class': 'lookup',
        'app': 'client',
        'fields': 'name,code',
        'order': 'code',
        'mod': 'Diagnosis',
        'target': 'diagnosis',
        'autocomplete': 'off',
    }))



    class Meta:
        model = CaseNote
        fields = [
            'client',
            'client_lookup',
            'rate',
            'service_lookup',
            "pps_aoda_target_group",  # hidden unless changed
            "pps_core_target_group",  # hidden unless changed
            'diagnosis_lookup',
            "target_group",
            'date',
            "hours",
            'minutes',
            "billable",
            'pps_start',  # hidden unless changed in the init above
            'pps_end',  # hidden unless changed in the init above
            "pps_units",  # hidden unless changed in the init above
            "pps_mh_spc_end_reason",  # hidden unless changed
            "pps_aoda_spc_end_reason",  # hidden unless changed
            'note',
            'diagnosis',  # hidden
            'service',  # hidden
            'is_open',
            'pps_epi_uuid',  # hidden
        ]

        widgets = {
            'date': DateInput(format=settings.DATE_FORMAT, attrs={'width': "12rem", 'clear': 'left', 'class': 'datepicker'}),
            'hours': NumberInput(attrs={'width': "6rem", "placeholder": "0"}),
            'minutes': NumberInput(attrs={'width': "6rem", "placeholder": "0"}),
            "billable": CheckboxInput(attrs={"width": 100}),
            'note': Textarea(attrs={'rows': 10, 'cols': 80, 'class': 'monospace'}),
            'rate': Select(attrs={'width': 50}),
            "target_group": Select(attrs={"width": 33}),
            'diagnosis': HiddenInput(),
            'service': HiddenInput(),
            'is_open': HiddenInput(),
            'client': HiddenInput(),
            'pps_start': HiddenInput(),
            "pps_end": HiddenInput(),
            "pps_units": HiddenInput(),
            "pps_epi_uuid": HiddenInput(),
            "pps_aoda_spc_end_reason": HiddenInput(),
            "pps_mh_spc_end_reason": HiddenInput(),
            "pps_core_target_group": HiddenInput(),
            "pps_aoda_target_group": HiddenInput(),

        }







class CaseNoteFileForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CaseNoteFileForm, self).__init__(*args, **kwargs)
        render_form(self)

        try:
            isopen = kwargs.get('instance').case_note.is_open
            if isopen is False:
                self.fields['description'].widget.attrs['disabled'] = True
                self.fields['cn_file'].widget.attrs['disabled'] = True
                self.fields['external_origin'].widget.attrs['disabled'] = True
        except CaseNote.DoesNotExist:
            pass


    class Meta:
        model = CaseNoteFile
        fields = ('description', 'external_origin', 'cn_file', 'case_note')

        widgets = {
            'case_note': HiddenInput(),
            'cn_file': ClearableFileInput(attrs={'class': 'file-upload', }),
        }


class PpsAodaEpisodeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PpsAodaEpisodeForm, self).__init__(*args, **kwargs)
        render_form(self)

    client_lookup = CharField(label="Client", required=True, widget=TextInput(attrs={
        'width': 50,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name, birthdate',
        'order': 'name',
        'target': 'client',
        'autocomplete': 'off',
        "clear": "left",
    }))


    class Meta:
        model = PpsAodaEpi
        fields = (
            "client",
            "client_lookup",
            "start",
            "end",
            "brief_services",
            "county",
            "referral_source",
            "education",
            "arrest_end",
            "client_char1",
            "client_char2",
            "client_char3",
            "substance_problem1",
            "route_admin1",
            "substance_problem2",
            "route_admin2",
            "substance_problem3",
            "route_admin3",
            "use_freq",
            "use_freq_end",
            "group_attend",
            "group_attend_end",
            "living_stat",
            "living_stat_end",
            "employ_stat",
            "employ_stat_end",
        )

        widgets = {
            "client": HiddenInput(),
            "start": DateInput(format=settings.DATE_FORMAT, attrs={'width': 25, "clear": "left", 'class': 'datepicker'}),
            "end": DateInput(format=settings.DATE_FORMAT, attrs={'width': 25, 'class': 'datepicker'}),
            "brief_services": CheckboxInput(attrs={"width": 25}),
            "county": Select(attrs={"width": 25}),
            "referral_source": Select(attrs={"width": 25}),
            "education": Select(attrs={"width": 25}),
            "arrest_end": NumberInput(attrs={"width": 50, "clear": "both"}),
            "substance_problem1": Select(attrs={"width": 50, "clear": "left"}),
            "substance_problem2": Select(attrs={"width": 50}),
            "substance_problem3": Select(attrs={"width": 50}),
            "route_admin1": Select(attrs={"width": 50}),
            "route_admin2": Select(attrs={"width": 50}),
            "route_admin3": Select(attrs={"width": 50}),
            "group_attend": Select(attrs={"width": 50, "clear": "left"}),
            "group_attend_end": Select(attrs={"width": 50}),
            "employ_stat": Select(attrs={"width": 50, "clear": "left"}),
            "employ_stat_end": Select(attrs={"width": 50}),
            "living_stat": Select(attrs={"width": 50, "clear": "left"}),
            "living_stat_end": Select(attrs={"width": 50}),
            "use_freq": Select(attrs={"width": 50}),
            "use_freq_end": Select(attrs={"width": 50}),
            "client_char1": Select(attrs={"width": 33, "clear": "left"}),
            "client_char2": Select(attrs={"width": 33}),
            "client_char3": Select(attrs={"width": 33}),


        }



class PpsMhEpisodeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PpsMhEpisodeForm, self).__init__(*args, **kwargs)
        render_form(self)

    client_lookup = CharField(label="Client", required=True, widget=TextInput(attrs={
        'width': 50,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name, birthdate',
        'order': 'name',
        'target':'client',
        'autocomplete': 'off',
        "clear": "left",
    }))

    diagnosis1_lookup = CharField(label="Primary Diagnosis", required=True, widget=TextInput(attrs={
        'width': 33,
        'class': 'lookup',
        'app': 'client',
        'fields': 'name,code',
        'order': 'code',
        'mod': 'Diagnosis',
        'target': 'diagnosis1',
        'autocomplete': 'off',
    }))

    diagnosis2_lookup = CharField(label="2nd Diagnosis", required=False, widget=TextInput(attrs={
        'width': 33,
        'class': 'lookup',
        'app': 'client',
        'fields': 'name,code',
        'order': 'code',
        'mod': 'Diagnosis',
        'target': 'diagnosis2',
        'autocomplete': 'off',
    }))

    diagnosis3_lookup = CharField(label="3rd Diagnosis", required=False, widget=TextInput(attrs={
        'width': 33,
        'class': 'lookup',
        'app': 'client',
        'fields': 'name,code',
        'order': 'code',
        'mod': 'Diagnosis',
        'target': 'diagnosis3',
        'autocomplete': 'off',
    }))

    diagnosis4_lookup = CharField(label="4th Diagnosis", required=False, widget=TextInput(attrs={
        'width': 33,
        'class': 'lookup',
        'app': 'client',
        'fields': 'name,code',
        'order': 'code',
        'mod': 'Diagnosis',
        'target': 'diagnosis4',
        'autocomplete': 'off',
    }))

    diagnosis5_lookup = CharField(label="5th Diagnosis", required=False, widget=TextInput(attrs={
        'width': 33,
        'class': 'lookup',
        'app': 'client',
        'fields': 'name,code',
        'order': 'code',
        'mod': 'Diagnosis',
        'target': 'diagnosis5',
        'autocomplete': 'off',
    }))


    class Meta:
        model = PpsMhEpi
        fields = (
            "client",
            "client_lookup",
            "start",
            "end",
            "county",
            "referral_source",
            "client_char1",
            "client_char2",
            "client_char3",
            "diagnosis1",
            "diagnosis2",
            "diagnosis3",
            "diagnosis4",
            "diagnosis5",
            "diagnosis1_lookup",
            "diagnosis2_lookup",
            "diagnosis3_lookup",
            "diagnosis4_lookup",
            "diagnosis5_lookup",
            "presenting_problem1",
            "presenting_problem2",
            "presenting_problem3",
            "brc_target_pop",
            "employ_stat",
            "living_stat",
            "legal_stat",
            "criminal_interactions",
            "arrest_30days",
            "arrest_6months",
            "psy_env_stress",
            "health_status",
            "daily_activity",
            "epi_end_reason",

        )

        widgets = {
            "client": HiddenInput(),
            "start": DateInput(format=settings.DATE_FORMAT, attrs={'width': 25, "clear": "left", 'class': 'datepicker'}),
            "end": DateInput(format=settings.DATE_FORMAT, attrs={'width': 25, 'class': 'datepicker'}),
            "county": Select(attrs={"width": 25}),
            "referral_source": Select(attrs={"width": 50}),
            "client_char1": Select(attrs={"width": 33, "clear": "left"}),
            "client_char2": Select(attrs={"width": 33}),
            "client_char3": Select(attrs={"width": 33}),
            "presenting_problem1": Select(attrs={"width": 33, "clear": "left"}),
            "presenting_problem2": Select(attrs={"width": 33}),
            "presenting_problem3": Select(attrs={"width": 33}),
            "brc_target_pop": Select(attrs={"width": 50}),
            "employ_stat": Select(attrs={"width": 50}),
            "living_stat": Select(attrs={"width": 50}),
            "legal_stat": Select(attrs={"width": 50}),
            "arrest_30days": NumberInput(attrs={"width": 25}),
            "arrest_6months": NumberInput(attrs={"width": 25}),
            "psy_env_stress": Select(attrs={"width": 50}),
            "health_status": Select(attrs={"width": 50}),
            "daily_activity": Select(attrs={"width": 50}),
            "epi_end_reason": Select(attrs={"width": 50}),

            "diagnosis1": HiddenInput(),
            "diagnosis2": HiddenInput(),
            "diagnosis3": HiddenInput(),
            "diagnosis4": HiddenInput(),
            "diagnosis5": HiddenInput(),
        }



class PpsCoreEpisodeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PpsCoreEpisodeForm, self).__init__(*args, **kwargs)
        render_form(self)

    client_lookup = CharField(label="Client", required=True, widget=TextInput(attrs={
        'width': 50,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name, birthdate',
        'order': 'name',
        'target': 'client',
        'autocomplete': 'off',
        "clear": "left",
    }))


    class Meta:
        model = PpsCoreEpi
        fields = (
            "client",
            "client_lookup",
            "start",
            "end",
            "county",
            "client_char1",
            "client_char2",
            "client_char3",
            "epi_end_reason",
        )

        widgets = {
            "client": HiddenInput(),
            "start": DateInput(format=settings.DATE_FORMAT, attrs={'width': 25, "clear": "left", 'class': 'datepicker'}),
            "end": DateInput(format=settings.DATE_FORMAT, attrs={'width': 25, 'class': 'datepicker'}),
            "county": Select(attrs={"width": 25}),
            "client_char1": Select(attrs={"width": 33, "clear": "left"}),
            "client_char2": Select(attrs={"width": 33}),
            "client_char3": Select(attrs={"width": 33}),
            "epi_end_reason": Select(attrs={"width": 50})
        }



class PpsB3EpisodeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PpsB3EpisodeForm, self).__init__(*args, **kwargs)
        render_form(self)

    client_lookup = CharField(label="Client", required=True, widget=TextInput(attrs={
        'width': 50,
        'class': 'lookup',
        'app': 'client',
        'mod': 'Client',
        'fields': 'last_name, first_name, birthdate',
        'order': 'name',
        'target': 'client',
        'autocomplete': 'off',
        "clear": "left",
    }))


    class Meta:
        model = PpsB3Epi
        fields = (
            "client",
            "client_lookup",
            "start",
            "end",
            "county",
            "language_pref",
            "referral_source",
            "result_reported",

        )

        widgets = {
            "client": HiddenInput(),
            "start": DateInput(format=settings.DATE_FORMAT, attrs={'width': 25, "clear": "left", 'class': 'datepicker'}),
            "end": DateInput(format=settings.DATE_FORMAT, attrs={'width': 25, 'class': 'datepicker'}),
            "county": Select(attrs={"width": 25}),
            "language_pref": Select(attrs={"width": 50, "clear": "left"}),
            "referral_source": Select(attrs={"width": 50}),
            "result_reported": Select(attrs={"width": 50}),
        }
