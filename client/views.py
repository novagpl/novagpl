"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import os
import datetime as dt

from difflib import SequenceMatcher

from django.shortcuts import render, get_object_or_404
from django.db.models import Q, F
from django.http import (
    HttpResponseRedirect, HttpResponse, FileResponse, HttpResponseNotFound)
from annoying.functions import get_object_or_None
from client.form_crosswalk import *
from client.state_forms import get_state_forms
from django.utils import timezone
from django.conf import settings
from django.core.exceptions import PermissionDenied

from django.core.paginator import Paginator

from utils import (
    get_logged_in, save_audit, delete_audit, save_confirm,
    set_autocomplete, format_errors, get_pps_epi, generate_units,
    delete_confirm, get_emrform_instance, view_audit, calculate_units,
    get_app_config, get_client_ip, duplicate_name_detect, access_control
)
from .models import *  # TODO fix this
from .forms import *  # TODO fix this

from accounting.models import *  # TODO fix this
from config.models import *  # TODO fix this

import report_list



import validation
from validate_emr_form import validate_emr_form_finalize



@access_control("admin", "caseworker", "financial")
def sacwis_import_edit(request, sac_row_id):
    pass



@access_control("admin", "caseworker", "financial")
def sacwis_import_mgmt(request):
    if request.method == "GET":
        # The only ones the users need to see by default are the ones
        # where the clients have partial matches
        query_results = SacImportRow.objects.filter(
            nova_client_suggest__isnull=False
        )

        import pdb; pdb.set_trace()


        # Group according to the client
        grouped_rows = {}
        for row in query_results:
            if row.nova_client in grouped_rows:
                grouped_rows[row.nova_client].append(row)
            else:
                grouped_rows[row.nova_client] = [row, ]


        #for key, row in grouped_rows.items():
        #    import pdb; pdb.set_trace()


    elif request.method == "POST":
        pass



    return render(request, "client/sacwis_import_index.html", {
        "title": "eWiSACWIS Import",
        "import_rows": grouped_rows,
        "table_class": "listview-table-page",
    })





@get_logged_in
def emr_form_history(request, emr_form_id):
    # Simple view that displays the history of the emr form
    emrform = get_object_or_404(EmrForm, id=emr_form_id)

    return render(request, "client/emr_form_history.html", {
        "emrform": emrform,
    })


@get_logged_in
def emrform_electric_sig(request, emr_form_id, emrform_sig_id=None):
    emrform = get_object_or_404(EmrForm, id=emr_form_id)

    if emrform_sig_id:
        instance = get_object_or_404(EmrFormSignature, id=emrform_sig_id)
        title = "Electronic Signature"
    else:
        instance = None
        title = "New Electronic Signature"



    if request.method == "GET":
        form = EmrFormSigForm(instance=instance)

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"


        # Different setups may have different signature pads or other methods
        # of collecting electronic sigs. This checks the appconfig and attempts
        # to set a good initial value.
        default_sig = utils.get_app_config("default_emrform_sig")
        if default_sig == "topaz":
            form.fields["sig_type"].initial = "topaz"
        elif default_sig == "simple_typed":
            form.fields["sig_type"].initial = "simple_typed"


        hide_save = False
        if instance:
            hide_save = True






        return render(request, "client/emrform_sig_edit.html", {
            'form': form,
            'title': title,
            'popup': popup,
            "emrform": emrform,
            "instance": instance,
            "hide_save": hide_save,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, f"/client/emr_form/{emr_form_id}/")

        form = EmrFormSigForm(request.POST, instance=instance)


        if form.is_valid():
            obj = form.save(commit=False)

            obj.emrform = emrform
            obj.logged_in_user = request.user
            obj.ip_address = get_client_ip(request)
            obj.useragent = request.headers.get("user-agent")

            return save_confirm(request, save_audit(request, form))
        else:
            return format_errors(request, form=form)




@get_logged_in
def legal_name_edit(request, client_id, legal_name_id=None):
    # This handles updating a client's legal name. Provide the user with
    # a form to input new name details and use that information to update
    # the client's existing name and save a record of the previous name
    if legal_name_id:
        instance = get_object_or_404(PastLegalName, id=legal_name_id)
        title = "Past Legal Name"
    else:
        instance = None
        title = "New Legal Name"


    if request.method == "GET":
        form = PastLegalNameForm(instance=instance)

        if legal_name_id is None:
            client = get_object_or_None(Client, id=client_id)
            form.fields["client"].initial = client
            form.fields["last_name"].initial = client.last_name
            form.fields["first_name"].initial = client.first_name
            form.fields["middle_name"].initial = client.middle_name
            form.fields["title"].initial = client.title
            form.fields["suffix"].initial = client.suffix


        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"


        return render(request, "client/legal_name_edit.html", {
            'form': form,
            'title': title,
            'popup': popup,
            "instance": instance,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, f"/client/client_settings/{{instance.id}}/")

        form = PastLegalNameForm(request.POST, instance=instance)
        if form.is_valid():
            # this is where things get different. Instead of saving the
            # data as-is, we need to apply the new legal name to the client
            # and save their existing name as the previous legal name
            obj = form.save(commit=False)

            client = obj.client

            old_last = client.last_name
            old_first = client.first_name
            old_middle = client.middle_name
            old_suffix = client.suffix
            old_title = client.title

            new_last = obj.last_name
            new_first = obj.first_name
            new_middle = obj.middle_name
            new_suffix = obj.suffix
            new_title = obj.title

            client.last_name = new_last
            client.first_name = new_first
            client.middle_name = new_middle
            client.suffix = new_suffix
            client.title = new_title
            save_audit(request, client)

            obj.last_name = old_last
            obj.first_name = old_first
            obj.middle_name = old_middle
            obj.suffix = old_suffix
            obj.title = old_title

            return save_confirm(request, save_audit(request, obj))
        else:
            return format_errors(request, form=form)




@get_logged_in
def ajax_calculate_units(request, emr_form_id, minutes):
    # This exists so case workers can see the units update live on their
    # case note while they are working on it
    emr_form = get_object_or_None(EmrForm, id=emr_form_id)

    #import pdb; pdb.set_trace()

    if emr_form:
        units = calculate_units(emr_form=emr_form, minutes=minutes)
        return HttpResponse(units)

    else:
        return HttpResponse("")



@get_logged_in
def ajax_duplicate_name_detect(request):
    first_name = request.POST.get("first_name").strip().lower()
    last_name = request.POST.get("last_name").strip().lower()
    string_birthdate = request.POST.get("birthdate")
    birthdate = None

    if len(first_name) < 3 or len(last_name) < 3:
        return HttpResponse("OK")


    if string_birthdate:
        try:
            birthdate = dt.datetime.strptime(string_birthdate, settings.DATE_FORMAT).date()
        except ValueError:
            birthdate = None


    if len(first_name) < 3 and len(last_name) < 3 and not birthdate:
        return HttpResponse("OK")

    sorted_match_data = duplicate_name_detect(first_name, last_name, birthdate)
    if sorted_match_data:
        return HttpResponse(json.dumps(sorted_match_data))
    else:
        return HttpResponse("OK")






@get_logged_in
def emr_message_edit(request, message_id=None):
    if message_id:
        instance = get_object_or_404(EmrMessage, id=message_id)
        title = "Message"
    else:
        instance = None
        title = "New Message"


    if request.method == "GET":
        form = EmrMessageForm(instance=instance)

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"


        return render(request, "client/emr_message_edit.html", {
            'form': form,
            'title': title,
            'popup': popup,
            "instance": instance,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            return delete_confirm(request, instance, "/client/caseworker_login/")

        form = EmrMessageForm(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.updated_by = request.user
            return save_confirm(request, save_audit(request, obj))
        else:
            return format_errors(request, form=form)




@get_logged_in
def caseworker_login(request):
    # This is the main screen that caseworkers will be shown when the login
    # or when they click the Nova icon at the top left of the nav

    # Depnding on the time of the day, change the welcome message
    now = timezone.localtime()

    if now.hour >= 4 and now.hour < 12:
        title = f"Good Morning"
    elif now.hour >= 12 and now.hour < 18:
        title = f"Good Afternoon"
    else:
        title = f"Good Evening"



    open_forms = EmrForm.objects.filter(
        created_by=request.user,
        is_finalized=False,
    ).exclude(
        emrcasenoteform__is_open=False,
    ).exclude(
        # Placement forms don't get finalized so they don't need to ever
        # show up as 'open' items that the user needs to close or finalize
        emrformtype__name__iexact="placement",
    ).order_by("-updated_at")




    my_clients = Client.objects.filter(
        assigned_staff=request.user.userconfig,
        active=True).order_by("last_name", "first_name")

    recent_forms = EmrForm.objects.filter(
        created_by=request.user, emrepisode__client__active=True).order_by(
        "-updated_at")

    received_routes = Route.objects.filter(
        assigned_to=request.user,
        created_at__gte=timezone.now() - dt.timedelta(days=30)
    ).order_by("-created_at")

    #sent_routes = Route.objects.filter(
    #    created_by=request.user,
    #    created_at__gte=timezone.now() - dt.timedelta(days=30)
    #).order_by("-created_at")

    routes = received_routes



    all_reports = [
        x for x
        in report_list.reports
        if x.report_type == "financial"
    ]

    program_reports = [
        x for x
        in report_list.reports
        if x.report_type == "program"
    ]


    emr_messages = EmrMessage.objects.filter(
        Q(expires_at__gte=timezone.now()) | Q(expires_at__isnull=True)).order_by("-updated_at")

    expired_messages = EmrMessage.objects.filter(expires_at__lt=timezone.now()).order_by("-updated_at")


    return render(request, "client/caseworker_login.html", {
        "title": title,
        "open_forms": open_forms,
        "my_clients": my_clients,
        "all_reports": all_reports,
        "program_reports": program_reports,
        "emr_messages": emr_messages,
        "expired_messages": expired_messages,
        "received_routes": received_routes,
        "routes": routes,
    })





@get_logged_in
def client_index(request, pk=None, show_inactive=False):
    if show_inactive:
        client_data = Client.objects.filter(active=False).order_by("last_name")
    else:
        client_data = Client.objects.filter(active=True).order_by("last_name")

    if request.user.userconfig.user_type == "4":
        # the user is an auditor, so lets filter only clients they are allowed
        # to view
        client_data = request.user.userconfig.auditor_clients.all()


    return render(request, 'client/client_index.html', {
        'title': 'Clients',
        'data': client_data,
        'table_class': 'listview-table-page',
        "show_inactive": show_inactive,
    })


@get_logged_in
def client_inactive(request):
    return client_index(request, None, True)


@get_logged_in
def me_index(request, pk=None):
    client_data = Client.objects.filter(assigned_staff=get_user_config(request), active=True).order_by("last_name")
    casenote_data = CaseNote.objects.filter(client__assigned_staff=get_user_config(request), is_open=True, pps_epi_uuid=None).order_by('-date')
    closed_casenote_data = CaseNote.objects.filter(client__assigned_staff=get_user_config(request), is_open=False, pps_epi_uuid=None).order_by('-date')

    return render(request, 'client/me_index.html', {
        'title' : 'My Clients',
        'my_clients' : "true",
        'data' : client_data,
        'casenote_data' : casenote_data,
        'closed_casenote_data' : closed_casenote_data,
        'table_class' : 'listview-table-no-order',
        })


@get_logged_in
def client_set_active(request, client_id):
    # Not related to client_active, this function sets the active bit on
    # a client and returns the user back to the client_settings view
    client = get_object_or_404(Client, id=client_id)
    client.active = True
    return save_confirm(request, save_audit(request, client), f"/client/client_settings/{client_id}/")


@get_logged_in
def client_set_inactive(request, client_id):
    # Not related to client_active, this function sets the active bit on
    # a client and returns the user back to the client_settings view

    # This should only work if the client doesn't have any open episodes
    client = get_object_or_404(Client, id=client_id)
    epi_count = client.get_open_episodes().count()
    if epi_count == 0:
        client.active = False
        return save_confirm(request, save_audit(request, client), f"/client/client_settings/{client_id}/")



@get_logged_in
def atp_edit(request, pk=None):
    mod = AbilityToPay
    frm = AbilityToPayForm

    if pk:
        instance = get_object_or_404(mod, id=pk)
        title = "Ability to Pay"
    else:
        instance = None
        title = "New Ability to Pay"

    if request.method == "GET":
        if pk:
            form = frm(instance=instance, )

        else:
            parent = get_object_or_None(Client, pk=request.GET.get("client"))
            form = frm(instance=mod(
                client=parent,
                month=dt.datetime.now().month,
                year=dt.datetime.now().year,
            ))


        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"


        return render(request, 'client/atp_edit.html', {
            'form':form,
            'title':title,
            'popup' : popup,
            "instance" : instance,
        })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance)

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            #obj = form.save(commit=False)
            obj = save_audit(request, form)
            return save_confirm(request, obj)
        else:
            return format_errors(request, form)



@get_logged_in
def client_settings_index(request, client_id):
    # Auditors do not need access to the user settings view
    if request.user.userconfig.user_type == '4':
        print("Restricted!")
        view_audit(request, "WARN")
        raise PermissionDenied

    # assigned staff and ATP
    client = get_object_or_404(Client, id=client_id)

    assigned_staff = client.assigned_staff.all()
    atp_data = AbilityToPay.objects.filter(client__id=client_id).order_by("-year", "-month")
    legal_names = PastLegalName.objects.filter(client__id=client_id).order_by("-date_changed")


    return render(request, "client/client_settings.html", {
        "title": "Client Settings",
        "client": client,
        "client_tab": "settings",
        'assigned_staff_data': assigned_staff,
        "atp_data": atp_data,
        "legal_names": legal_names,
    })




@get_logged_in
def new_emr_episode(request, client_id, episode_type):
    # This function creates the episode based on the type and pre-fills
    # any fields that may need it then redirects the user to that newly created
    # episode

    # users with view_only should get a 403.
    if request.user.userconfig.view_only is True:
        view_audit(request, "WARN")
        raise PermissionDenied

    episode_types = [
        "b3",
        "b3_preadmit",
        "clinic_preadmit",
        "clts",
        "clts_preadmit",
        "crisis",
        "cst",
        "randi",
        "tcm",
    ]

    if episode_type not in episode_types:
        raise Exception(f"Incorrect episode type selected '{episode_type}'")

    emr_client = get_object_or_None(Client, id=client_id)
    if emr_client is None:
        raise Exception(f"No client found for '{client_id}'")


    # Check if another open episode of this type exists. If it does, show an
    # error message
    existing = EmrEpisode.objects.filter(
        episode_type=episode_type,
        end__isnull=True,
        client=emr_client,
    ).first()


    if existing:
        return format_errors(
            request,
            form=None,
            custom_error=f"""
                Unable to create {existing.get_episode_type_pretty()} episode
                because an open episode of this type already exists. You have been
                redirected to the currently open
                {existing.get_episode_type_pretty()} episode.
            """,
            custom_redirect=f"/client/emr_episodes/{existing.client.id}/{existing.id}/"
        )


    if episode_type == "clinic_preadmit":
        new_episode = EmrClinicPreadmitEpisode()

    elif episode_type == "clts":
        new_episode = EmrCltsEpisode()

    elif episode_type == "clts_preadmit":
        new_episode = EmrCltsPreadmitEpisode()

    elif episode_type == "crisis":
        new_episode = EmrCrisisEpisode()

    elif episode_type == "cst":
        new_episode = EmrCstEpisode()

    elif episode_type == "randi":
        new_episode = EmrRandiEpisode()

    elif episode_type == "tcm":
        new_episode = EmrTcmEpisode()

    elif episode_type == "b3":
        new_episode = EmrB3Episode()

    elif episode_type == "b3_preadmit":
        new_episode = EmrB3PreadmitEpisode()


    new_episode.episode_type = episode_type
    new_episode.client = emr_client
    new_episode.opened_by = request.user
    new_episode.start = dt.datetime.now()

    new_episode.save()

    #print(f"New EMR episode created with an ID of {new_episode.id}")

    return HttpResponseRedirect(
        f"/client/emr_episodes/{client_id}/{new_episode.id}/")






@get_logged_in
def emr_episode_edit(request, client_id, episode_id=None, form_type_id=None, doc_type_id=None, note_type_id=None):
    if request.method == "GET":
        page_number = request.GET.get("page")
        page = None
        client = get_object_or_404(Client, id=client_id)

        if request.user.userconfig.user_type == "4" and request.user.userconfig.auditor_episode_type is not None:
            # The user is an auditor with limited access to a specific episode type
            episodes = EmrEpisode.objects.filter(
                client__id=client_id,
                episode_type=request.user.userconfig.auditor_episode_type
            ).order_by("-start")

        else:
            episodes = EmrEpisode.objects.filter(client__id=client_id).order_by(F("end").desc(nulls_first=True), "-start")

        # Grouping up episodes and putting into a list so we can show that data
        # in the episode ribbon which sites below the nav bar and breadcrumbs
        # in the EMR views

        # The episodes need to go in order of most recent opened first then
        # the most recent closed.

        # Also need to check if any open episode of the same type exists.
        # If it does, then we pass "open_episode_exists": True,
        # to the template. There may be a situation that there's an episode
        # that is open and an episode of the same type that is closed but the
        # closed episode is later than the open one. This causes the ribbon to
        # show a closed date when it should show that there's an open episode.
        grouped_episodes = {}
        for epi in episodes:
            if epi.episode_type not in grouped_episodes:
                grouped_episodes[epi.episode_type] = {
                    "start": epi.start,
                    "end": epi.end,
                    "count": 1,
                    "id": epi.id,
                    "epi_type_pretty": epi.get_episode_type_pretty(),
                    "client_id": epi.client.id,
                }
            else:
                grouped_episodes[epi.episode_type]["count"] += 1

            if epi.end is None:
                grouped_episodes[epi.episode_type]["has_open"] = True

            if epi.end is None:
                grouped_episodes[epi.episode_type]["has_open"] = True


        grouped_episode_list = []
        for epi in grouped_episodes:
            grouped_episode_list.append(grouped_episodes[epi])




        # If an Id is passed, grab that episode.
        # But if no id is passed, grab the latest open episode
        if episode_id:
            episode = get_object_or_404(EmrEpisode, id=episode_id).get_submodel()

        else:
            episode = episodes.first()
            if episode:
                return HttpResponseRedirect(f"/client/emr_episodes/{client_id}/{episode.id}/")


        # Build up an episode history to be displayed underneath the
        # episode form. We need some way at least to show the opened/closed
        # episodes. At some point folks will probably want to see the notes
        # for a specific episode, but that's not what's been asked yet
        if episode:
            past_episodes = EmrEpisode.objects.filter(
                episode_type=episode.episode_type,
                client=episode.client,
            ).exclude(id=episode.id).order_by("-start")
        else:
            past_episodes = None


        if form_type_id:
            form_type = get_object_or_None(EmrFormType, id=form_type_id)

        elif note_type_id and form_type_id is None:
            form_type = episode.get_casenote_form_type()
        else:
            form_type = None

        if doc_type_id:
            doc_type = get_object_or_None(EmrDocType, id=doc_type_id)
        else:
            doc_type = None

        # Getting the form and doc types from the episode
        if episode:
            emr_form_types = episode.get_form_types()
            emr_doc_types = episode.get_doc_types()
        else:
            emr_form_types = None
            emr_doc_types = None


        # If this is a clinic preadmit episode and the client has an open
        # RandI episode, we want to show lists of the randi forms and documents
        # right under the list of the docs and forms for this clinic preadmit
        # episode.
        found_randi_id = None
        emr_doc_types_randi = None
        emr_form_types_randi = None
        if episode and episode.episode_type == "clinic_preadmit":
            # if there is more than 1 open randi episode for this client, just
            # grab the first one
            found_randi = EmrEpisode.objects.filter(
                episode_type="randi",
                client=episode.client,
                #end__isnull=True  # we want to show even if randi is closed
            ).order_by("-start", "-id").first()

            if found_randi:
                found_randi_id = found_randi.id
                emr_doc_types_randi = found_randi.get_doc_types()
                emr_form_types_randi = found_randi.get_form_types()

        # If this is a TCM or CST PPS admission form, we need to pull out the
        # formtype for the matching discharge form
        pps_discharge_formtype = None
        if episode and episode.episode_type == "tcm":
            if form_type and form_type.get_name() == "pps aoda admission":
                pps_discharge_formtype = EmrFormType.objects.get(name__iexact="pps aoda discharge", type_tcm=True)
            elif form_type and form_type.get_name() == "pps mental health admission":
                pps_discharge_formtype = EmrFormType.objects.get(name__iexact="pps mental health discharge", type_tcm=True)
        elif episode and episode.episode_type == "cst":
            if form_type and form_type.get_name() == "pps mental health admission":
                pps_discharge_formtype = EmrFormType.objects.get(name__iexact="pps mental health discharge", type_cst=True)





        epi_form = None
        epi_notes_page = None
        case_note_form_type = None
        if episode_id and form_type_id is None and doc_type_id is None:
            # This is the default view when the episode is selected.
            # Showing the client's case notes. In the future maybe it could
            # include other recently edited forms
            epi_form = get_subform(instance=episode)

            epi_notes = EmrForm.objects.filter(
                emrepisode__episode_type=episode.episode_type,
                emrepisode__client=episode.client,
                emrformtype__name="Case Note"
            ).order_by("-date")

            paginator = Paginator(epi_notes, 50)
            epi_notes_page = paginator.get_page(page_number)
            page = epi_notes_page

            if epi_notes:
                case_note_form_type = epi_notes[0].emrformtype


        form_list_page = None
        # If this is a case note type, only show case notes of that type
        # otherwise just filter by the normal form type
        if form_type and note_type_id:
            form_list = EmrCaseNoteForm.objects.filter(
                emrepisode__episode_type=episode.episode_type,
                emrformtype=form_type,
                emrepisode__client=episode.client,
                case_note_type=note_type_id,
            ).order_by("-date")

            paginator = Paginator(form_list, 50)
            form_list_page = paginator.get_page(page_number)
            page = form_list_page

        elif form_type:
            # display the listview for whatever these types of forms are
            # May need to expand this further to deal with the inherited
            # forms

            form_list_or = Q()
            form_type_query = Q()
            if pps_discharge_formtype:
                # If the user is viewing the PPS MH or AODA forms, we actually show
                # the admission and the discharge forms in this view
                form_list_or.add(Q(emrformtype=pps_discharge_formtype), Q.OR)
                form_list_or.add(Q(emrformtype=form_type), Q.OR)
                form_type_query.add(form_list_or, Q.AND)

            else:
                form_type_query.add(Q(emrformtype=form_type), Q.AND)

            form_type_query.add(Q(emrepisode__episode_type=episode.episode_type), Q.AND)
            form_type_query.add(Q(emrepisode__client=episode.client), Q.AND)

            form_list = EmrForm.objects.filter(
                form_type_query).order_by("-date")

            paginator = Paginator(form_list, 50)
            form_list_page = paginator.get_page(page_number)
            page = form_list_page



        doc_list = None
        if doc_type:
            # display the listview for whatever these types of documents are
            doc_list = EmrDoc.objects.filter(
                emrepisode__episode_type=episode.episode_type,
                emrdoctype=doc_type,
                emrepisode__client=episode.client,
            ).order_by("-date")


        return render(request, "client/emr_episodes_index.html", {
            "title": "EMR Episode",
            "client_tab": "emr_episodes",
            "client": client,
            "client_id": client_id,
            "episodes": episodes,
            "grouped_episode_list": grouped_episode_list,
            "episode": episode,
            "past_episodes": past_episodes,
            "emr_form_types": emr_form_types,
            "form_type": form_type,
            "form_list": form_list_page,
            "note_type_id": note_type_id,
            "note_type_id_pretty": "" if note_type_id is None else note_type_id.replace("_", " "),
            "emr_doc_types": emr_doc_types,
            "doc_type": doc_type,
            "doc_list": doc_list,
            "epi_form": epi_form,
            "epi_notes": epi_notes_page,
            "found_randi_id": found_randi_id,
            "emr_doc_types_randi": emr_doc_types_randi,
            "emr_form_types_randi": emr_form_types_randi,
            "case_note_form_type": case_note_form_type,
            "pps_discharge_formtype": pps_discharge_formtype,
            "page": page,
        })

    elif request.method == "POST":
        instance = get_object_or_None(EmrEpisode, id=episode_id)

        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, f"/client/emr_episodes/{instance.client.id}/")

        form = get_subform(request.POST, instance=instance)
        if form.is_valid():
            obj = form.save(commit=False)
            if obj.closed_by is None and obj.end is not None:
                obj.closed_by = request.user
                obj.save()
            elif obj.closed_by is not None and obj.end is None:
                obj.closed_by = None
                obj.save()

            return save_confirm(request, save_audit(request, form), f"/client/emr_episodes/{instance.client.id}/{instance.id}")
        else:
            return format_errors(request, form=form)





def get_subform(post=None, instance=None, locked=False):
    # Come up with better description of what this does
    # This useful function returns whatever inherited modelform the inherited
    # model instance is made for

    # This needs to be used carefuly and intentiontally and
    # really only for the emr forms and (possibly) docs

    post_form_type = None
    # User is saving a form.
    # If this is a form being saved for the first time, instance
    # may be none. In that case, we can look at the post to figure out
    # what type of form to return.
    if post and not instance:
        if "emrformtype" in post:
            post_form_type = EmrFormType.objects.get(id=post["emrformtype"])
            type_name = post_form_type.name.lower()

            print(f"post_form_type.name: {post_form_type.name}")
            #import pdb; pdb.set_trace()
            # TODO this needs to be updated to use different forms
            # depending on what program the case note is under
            if type_name == "case note":
                # Look below, this is how you need to setup the case notes
                # for each type of program
                return EmrCaseNoteFormForm(post, instance=instance)

            elif type_name == "annual case note":
                if post_form_type.type_clts:
                    return EmrCltsAnnualCaseNoteForm(post, instance=instance)

            elif type_name == "6 month case note":
                if post_form_type.type_clts:
                    return EmrClts6MonthCaseNoteForm(post, instance=instance)


            elif type_name == "intake":
                if post_form_type.type_randi:
                    return EmrRandiIntakeFormForm(post, instance=instance)

            elif type_name == "internal referral":
                if post_form_type.type_randi:
                    return EmrRandiReferralFormForm(post, instance=instance)

            elif type_name == "external referral":
                if post_form_type.type_randi:
                    return EmrRandiExternalReferralFormForm(post, instance=instance)


            elif type_name == "county assessment and response plan":
                if post_form_type.type_crisis:
                    return EmrJacksonCountyAssesResponseFormForm(post, instance=instance)


            elif type_name == "crisis alert":
                if post_form_type.type_crisis:
                    return EmrCrisisAlertFormForm(post, instance=instance)


            elif type_name == "crisis plan":
                if post_form_type.type_crisis:
                    return EmrCrisisPlanFormForm(post, instance=instance)

            elif type_name == "clts/cst assessment":
                if post_form_type.type_clts:
                    return EmrCltsCstAssessmentFormForm(post, instance=instance)

            elif type_name == "clinic wait list":
                if post_form_type.type_clinic_preadmit:
                    return EmrclinicWaitlistFormForm(post, instance=instance)


            elif type_name == "placement":
                if post_form_type.type_tcm:
                    return EmrclinicWaitlistFormForm(post, instance=instance)
                if post_form_type.type_crisis:
                    return EmrclinicWaitlistFormForm(post, instance=instance)

            elif type_name == "nwc assessment and response plan":
                if post_form_type.type_crisis:
                    return EmrNwcAssesResponseFormForm(post, instance=instance)

            elif type_name == "nwc note":
                if post_form_type.type.crisis:
                    return EmrCrisisNwcNoteFormForm(post, instance=instance)

            elif type_name == "out of county assessment and response plan":
                if post_form_type.type_crisis:
                    return EmrOutOfCountyAssesResponseFormForm(post, instance=instance)

            elif type_name == "response plan review":
                if post_form_type.type_crisis:
                    return EmrCrisisResponsePlanReviewFormForm(post, instance=instance)

            elif type_name == "tcm assessment":
                if post_form_type.type_tcm:
                    return EmrTcmAssessmentFormForm(post, instance=instance)

            elif type_name == "pps mental health admission":
                if post_form_type.type_tcm:
                    return EmrTcmPpsMhAdmissionForm(post, instance=instance)
                elif post_form_type.type_cst:
                    return EmrCstPpsMhAdmissionForm(post, instance=instance)

            elif type_name == "pps mental health discharge":
                if post_form_type.type_tcm:
                    return EmrTcmPpsMhDischargeForm(post, instance=instance)
                elif post_form_type.type_cst:
                    return EmrCstPpsMhDischargeForm(post, instance=instance)

            elif type_name == "pps aoda admission":
                if post_form_type.type_tcm:
                    return EmrTcmPpsAodaAdmissionForm(post, instance=instance)

            elif type_name == "pps aoda discharge":
                if post_form_type.type_tcm:
                    return EmrTcmPpsAodaDischargeForm(post, instance=instance)




            else:
                #return EmrFormForm(post, instance=instance)
                raise Exception(f"Form type {type_name} not implemented in get_subform")





    # User is opening a new blank form or an existing one
    # TODO: passing post is probably not required since we know it's None
    elif isinstance(instance, EmrForm) or isinstance(post_form_type, EmrForm):
        type_name = instance.emrformtype.name.lower()


        #print(f"Getting subform for {type_name}")

        if type_name == "case note":
            return EmrCaseNoteFormForm(post, instance=instance, locked=locked)

        elif type_name == "intake":
            if instance.emrformtype.type_randi:
                return EmrRandiIntakeFormForm(post, instance=instance, locked=locked)

        elif type_name == "internal referral":
            if instance.emrformtype.type_randi:
                return EmrRandiReferralFormForm(post, instance=instance, locked=locked)

        elif type_name == "external referral":
            if instance.emrformtype.type_randi:
                return EmrRandiExternalReferralFormForm(post, instance=instance, locked=locked)


        elif type_name == "annual case note":
            if instance.emrformtype.type_clts:
                return EmrCltsAnnualCaseNoteFormForm(post, instance=instance, locked=locked)

        elif type_name == "6 month case note":
            if instance.emrformtype.type_clts:
                return EmrClts6MonthCaseNoteFormForm(post, instance=instance, locked=locked)

        elif type_name == "enrollment case note":
            if instance.emrformtype.type_clts:
                return EmrCltsEnrollmentCaseNoteFormForm(post, instance=instance, locked=locked)


        elif type_name == "county assessment and response plan":
            if instance.emrformtype.type_crisis:
                return EmrJacksonCountyAssesResponseFormForm(post, instance=instance, locked=locked)


        elif type_name == "crisis alert":
            if instance.emrformtype.type_crisis:
                return EmrCrisisAlertFormForm(post, instance=instance, locked=locked)

        elif type_name == "crisis plan":
            if instance.emrformtype.type_crisis:
                return EmrCrisisPlanFormForm(post, instance=instance, locked=locked)

        elif type_name == "clts/cst assessment":
            if instance.emrformtype.type_clts:
                return EmrCltsCstAssessmentFormForm(post, instance=instance, locked=locked)

        elif type_name == "clinic wait list":
            if instance.emrformtype.type_clinic_preadmit:
                return EmrClinicWaitlistFormForm(post, instance=instance, locked=locked)

        elif type_name == "placement":
            if instance.emrformtype.type_tcm:
                return EmrPlacementFormForm(post, instance=instance, locked=locked)
            if instance.emrformtype.type_crisis:
                return EmrPlacementFormForm(post, instance=instance, locked=locked)

        elif type_name == "nwc assessment and response plan":
            if instance.emrformtype.type_crisis:
                return EmrNwcAssesResponseFormForm(post, instance=instance, locked=locked)

        elif type_name == "nwc note":
            if instance.emrformtype.type_crisis:
                return EmrCrisisNwcNoteFormForm(post, instance=instance, locked=locked)

        elif type_name == "out of county assessment and response plan":
            if instance.emrformtype.type_crisis:
                return EmrOutOfCountyAssesResponseFormForm(post, instance=instance, locked=locked)

        elif type_name == "response plan review":
            if instance.emrformtype.type_crisis:
                return EmrCrisisResponsePlanReviewFormForm(post, instance=instance, locked=locked)

        elif type_name == "tcm assessment":
            if instance.emrformtype.type_tcm:
                return EmrTcmAssessmentFormForm(post, instance=instance, locked=locked)

        elif type_name == "pps mental health admission":
            if instance.emrformtype.type_tcm:
                return EmrTcmPpsMhAdmissionFormForm(post, instance=instance, locked=locked)
            elif instance.emrformtype.type_cst:
                return EmrCstPpsMhAdmissionFormForm(post, instance=instance, locked=locked)

        elif type_name == "pps mental health discharge":
            if instance.emrformtype.type_tcm:
                return EmrTcmPpsMhDischargeFormForm(post, instance=instance, locked=locked)
            elif instance.emrformtype.type_cst:
                return EmrCstPpsMhDischargeFormForm(post, instance=instance, locked=locked)

        elif type_name == "pps aoda admission":
            if instance.emrformtype.type_tcm:
                return EmrTcmPpsAodaAdmissionFormForm(post, instance=instance, locked=locked)


        elif type_name == "pps aoda discharge":
            if instance.emrformtype.type_tcm:
                return EmrTcmPpsAodaDischargeFormForm(post, instance=instance, locked=locked)



        else:
            return EmrFormForm(post, instance=instance, locked=locked)


    # User is opening an existing episode
    # These are episode subforms, different than the form subforms.
    elif isinstance(instance, EmrEpisode):
        if instance.episode_type == "clinic_preadmit":
            return EmrClinicPreadmitEpisodeForm(post, instance=instance)

        if instance.episode_type == "clts":
            return EmrCltsEpisodeForm(post, instance=instance)

        if instance.episode_type == "clts_preadmit":
            return EmrCltsPreadmitEpisodeForm(post, instance=instance)

        if instance.episode_type == "crisis":
            return EmrCrisisEpisodeForm(post, instance=instance)

        if instance.episode_type == "cst":
            return EmrCstEpisodeForm(post, instance=instance)

        if instance.episode_type == "randi":
            return EmrRandiEpisodeForm(post, instance=instance)

        if instance.episode_type == "tcm":
            return EmrTcmEpisodeForm(post, instance=instance)

        if instance.episode_type == "b3":
            return EmrB3EpisodeForm(post, instance=instance)

        if instance.episode_type == "b3_preadmit":
            return EmrB3PreadmitEpisodeForm(post, instance=instance)








@get_logged_in
def emr_form_edit(request, form_type_id=None, episode_id=None, emr_form_id=None):
    if emr_form_id:
        instance = get_emrform_instance(emr_form_id)
        #instance = get_object_or_404(EmrForm, id=emr_form_id)
        emr_form_type = instance.emrformtype
        episode = instance.emrepisode
        title = f"{emr_form_type.name}"

        request.user.userconfig.add_recent_client(instance.emrepisode.client)


    else:
        # User is creating a new form
        # Because we want to save the emr form right away, we will just
        # do that and then redirect the user to the new URL

        emr_form_type = get_object_or_404(EmrFormType, id=form_type_id)
        episode = get_object_or_404(EmrEpisode, id=episode_id)
        instance = get_emrform_instance(
            emr_form_id=None, episode=episode, emr_form_type=emr_form_type
        )


        instance.emrepisode = episode
        instance.emrformtype = emr_form_type
        instance.created_by = request.user
        #instance.date = dt.datetime.now().date
        #instance.service_start = dt.datetime.now()
        #instance.service_end = dt.datetime.now() + dt.timedelta(minutes=60)


        # If the user making the note has selected credentials in their user
        # profile, then automatically apply that to the emr form
        if request.user.userconfig.credentials:
            instance.credentials = request.user.userconfig.credentials

        # If this is a crisis case note pre-fill the ICD10/Diagnosis field
        # with the same value from the first assessment (nwc or county) on
        # record. Also called DIAGNOSIS PULL FORWARD!!!
        if instance.emrepisode.episode_type == "crisis" and instance.emrformtype.get_name() == "case note":

            # This looks at both the county, NWC, out of county asses and response forms
            # and tries to find a diag code.
            found = EmrNwcAssesResponseForm.objects.filter(emrepisode=episode).order_by("date").first()
            if found and found.diagnosis:
                instance.diagnosis = found.diagnosis

            else:
                found = EmrJacksonCountyAssesResponseForm.objects.filter(emrepisode=episode).order_by("date").first()
                if found and found.diagnosis:
                    instance.diagnosis = found.diagnosis

                else:
                    found = EmrOutOfCountyAssesResponseForm.objects.filter(emrepisode=episode).order_by("date").first()
                    if found and found.diagnosis:
                        instance.diagnosis = found.diagnosis

                    else:
                        found = EmrCrisisNwcNoteForm.objects.filter(emrepisode=episode).order_by("date").first()
                        if found and found.diagnosis:
                            instance.diagnosis = found.diagnosis




        # Default to 604 for now. This will probably be different for each
        # emr episode in the future. Crisis forms do not get a service
        # assigned.
        if instance.emrepisode.episode_type != "crisis":
            found_604 = Service.objects.filter(spc_code="604").first()
            if found_604 and instance.service is None:
                #import pdb; pdb.set_trace()
                instance.service = found_604
                #form = get_subform(instance=instance)
                #form.fields["service"].initial = found_604


        # Default the billing code to unbillable if its a RandI or
        # clinic preadmit case note as these typically don't get billed out
        if instance.emrformtype.get_name() == "case note":
            if instance.emrepisode.episode_type in ["randi", "clinic_preadmit"]:
                instance.billing_code = "unbillable"


        if instance.emrepisode.episode_type in ["tcm", "cst"]:
            # Default values on the TCM and CST PPS forms
            tcm_pps_forms = [
                "pps aoda admission",
                "pps aoda discharge",
                "pps mental health admission",
                "pps mental health discharge"
            ]
            # All of the PPS TCM and CST forms start dates are the same as their parent
            # episode start dates
            if instance.emrformtype.get_name() in tcm_pps_forms:
                instance.date = instance.emrepisode.start

                # Default values on the TCM PPS admission forms
                if instance.emrformtype.get_name() in ["pps mental health admission", "pps aoda admission"]:
                    instance.agency_of_resp = get_app_config("agency_id")
                    instance.provider_wpi_npi = get_app_config("agency_npi_wpi")
                    instance.brc_target_pop = "short_term"
                    instance.county_of_residence = get_app_config("county_code")






        confirmation = save_confirm(
                request,
                save_audit(request, instance),
                f"/client/emr_form/{instance.id}/"
        )
        instance.save_history("open", request)
        return confirmation

        # No longer used because the form is saved right away and the user
        # will always get redirected to newly-saved form's URL
        #title = f"New {episode.get_episode_type_pretty()} {emr_form_type.name}"


    if request.method == "GET":
        # If this is a client wait list form, the user wants to be able to
        # see and maybe edit insurance information at a glance
        insurance_data = None
        if emr_form_type.get_name() == "clinic wait list":
            insurance_data = Insurance.objects.filter(
                client__id=episode.client.id).order_by("-id")


        if request.user.userconfig.view_only is True:
            form = get_subform(instance=instance, locked=True)
        else:
            form = get_subform(instance=instance)



        set_autocomplete(instance, form, "service")
        set_autocomplete(instance, form, "diagnosis")


        # TODO Move to emrcasenoteformform
        if "units" in form.fields:
            form.fields["units"].disabled = True

        # TODO Move to emrcasenoteformform
        if "total_minutes" in form.fields:
            form.fields["total_minutes"].disabled = True


        if "linked_docs" in form.fields:
            docs = EmrDoc.objects.filter(
                    emrepisode=episode).order_by("-date")
            form.fields["linked_docs"].queryset = docs




        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"


        routes = None
        if instance:
            routes = instance.get_user_routes(request.user, active_only=True)

        supervisor_can_sign = False
        if instance and instance.is_finalized is True:
            supervisor_can_sign = instance.supervisor_can_sign(request.user)


        return render(request, "client/emr_form_edit.html", {
            "instance": instance,
            "title": title,
            "form": form,
            "emr_form_type": emr_form_type,
            "episode": episode,
            "popup": popup,
            "insurance_data": insurance_data,
            "client": episode.client,
            "routes": routes,
            "supervisor_can_sign": supervisor_can_sign,
        })


    elif request.method == "POST":
        # If this is a TCM PPS Discharge, we actually need to
        # redirect to the TCM PPS Admission list view. Both forms
        # will show on the list view, but we need to go to the
        # Admission one for it to work properly.
        formtype_id = instance.emrformtype.id
        if instance.emrformtype.get_name() == "pps aoda discharge":
            formtype_id = EmrFormType.objects.get(name__iexact="pps aoda admission", type_tcm=True).id
        elif instance.emrformtype.get_name() == "pps mental health discharge":
            if instance.emrepisode.episode_type == "tcm":
                formtype_id = EmrFormType.objects.get(name__iexact="pps mental health admission", type_tcm=True).id
            elif instance.emrepisode.episode_type == "cst":
                formtype_id = EmrFormType.objects.get(name__iexact="pps mental health admission", type_cst=True).id

        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(
                request=request,
                instance=instance,
                path_override=f"/client/emr_episodes/{instance.emrepisode.client.id}/{instance.emrepisode.id}/form/{formtype_id}/"
            )

        form = get_subform(request.POST, instance=instance)

        #form_section = request.GET.get("form_section")
        #is_normal_save = bool(request.GET.get("normal_save") != "false")


        if form.is_valid():
            obj = form.save(commit=False)

            # Because of the funky way we are using the many-to-many field
            # we need to explicitly set the linked_docs when saving
            for linked in instance.linked_docs.all():
                instance.linked_docs.add(linked)


            # Check for autosave first, this will prevent from sending back
            # large html templates as the ajax response
            if request.POST.get("autosave") == "autosave" and obj.is_finalized is False:
                save_confirm(request, save_audit(request, obj))
                print("Auto save successful")
                return HttpResponse("OK")



            else:
                # Just a normal save. Do not save if the form is already finalized
                # TODO maybe move the is_finalized check further up??
                if obj.is_finalized is False:

                    return save_confirm(
                        request=request,
                        instance=save_audit(request, obj),
                        path_override=f"/client/emr_episodes/{instance.emrepisode.client.id}/{instance.emrepisode.id}/form/{formtype_id}/",
                    )



        else:
            print(f"EMR form has errors: {form.errors}")
            if request.POST.get("autosave") == "autosave":
                # TODO one day implement a live-validation system
                # there the errors are sent back to the autosave function
                # and the relevant fields are highlighted with their errors
                return HttpResponse("ERROR: validation error")

            return format_errors(request, form=form)




@get_logged_in
def emr_form_sign(request, emr_form_id):
    # Finalize an EMR form and store a crypto signature
    # Make sure to check the pwd_verified bit BEFORE saving anything.

    # Grab the instance right away so we can work with the proper model
    # instead of using the parent inherited EmrForm model
    emr_form = EmrForm.objects.get(id=emr_form_id).get_emrform_instance()


    if emr_form.is_finalized:
        view_audit(request, "WARN")
        raise Exception("This form is already closed/finalized")

    if hasattr(emr_form, "is_open") and emr_form.is_open is False:
        view_audit(request, "WARN")
        raise Exception("This form is already closed/finalized")

    if emr_form.pwd_provided is False:
        view_audit(request, "WARN")
        raise Exception("EmrForm needs pwd_provided before closing/signing")

    # Run validation before applying the signature.
    custom_error = validate_emr_form_finalize(emr_form)
    if custom_error:
        return format_errors(
            request=request,
            form=None,
            custom_error=custom_error,
            custom_redirect=f"/client/emr_form/{emr_form.id}/"
        )



    # B-3 Case note billing code is decided by the type of service
    # that is selected.
    # There may be a future need to move this into a more intelligent
    # crosswalk logic...
    if emr_form.emrformtype.get_name() == "case note" and emr_form.emrepisode.episode_type == "b3":
        if emr_form.service.spc_code == "706.17":
            emr_form.billing_code = "T1017"
        else:
            emr_form.billing_code = "unbillable"
        emr_form.save()


    if emr_form.emrformtype.get_name() == "case note":
        emr_form.is_open = False
        emr_form.signed_by = request.user
        emr_form.closed_at = timezone.now()

        save_audit(request, emr_form)




    # If this is a CaseNote and is billable then generate a transaction
    if emr_form.billing_code and emr_form.billing_code not in ["unbillable", "N0000"]:

        # Run get_rate() first and early so we can throw an error
        # if the rate for the episode type and billing code is missing
        emr_form.get_rate()

        # If this case note isn't already included in a journal
        # then we will go ahead and make a new transaction.
        # If this was already journaled then it's most likely it was
        # re-opened by an admin to edit some EMR-related fields and we
        # do not want to create a new transaction.
        if emr_form.is_journaled() is False:
            rate = emr_form.get_rate()
            trans_amount = rate.amount * emr_form.units
            trans_units = emr_form.units

            # If there are existing transactions linked to this form, they
            # get deleted and re-created.
            existing_trans = Transaction.objects.filter(emr_form=emr_form)
            for ext_tran in existing_trans:
                delete_audit(request, ext_tran)

            trans = Transaction()
            trans.emr_form = emr_form
            trans.date = emr_form.date
            trans.service_date = emr_form.date
            trans.service = emr_form.service
            trans.place_of_service = emr_form.place_of_service
            trans.description = "EMR transaction"
            trans.amount = trans_amount
            trans.account = rate.account
            trans.is_journal = False
            trans.verified = False
            trans.client = emr_form.emrepisode.client
            trans.fiscal_year = emr_form.date.year
            trans.fiscal_period = emr_form.date.month
            trans.units = trans_units
            trans.trans_type = rate.account.type
            trans.billing_code = emr_form.billing_code

            # If this is a B3 episode then the target group must be
            # '1 - Developmental disability'
            if emr_form.emrepisode.episode_type == "b3":
                trans.target_group = "1"

            save_audit(request, trans)

            print(f"Generated transaction ({trans.id}) from EMR form ({emr_form.id}):  {trans}")





    emr_form.is_finalized = True
    emr_form.finalized_date = timezone.now()
    emr_form.signature = emr_form.generate_signature()


    # Check if this form requires a supervisor (MHP) signature. If it does,
    # change the path_override to include a parameter to show the popup
    saved_instance = save_audit(request, emr_form)

    saved_instance.save_history("finalize", request)


    if saved_instance.emrformtype.needs_supervisor_signature is True and saved_instance.supervisor_signature is None and saved_instance.supervisor_signer is None:
        path_override = f"/client/emr_form/{saved_instance.id}/?pick_supervisor=true"
    else:
        # This looks like it needs to be refactored...
        # Because on the front-end we have combined the PPS forms for tcm,
        # here we need to check and get the correct IDs so we're redirected
        # to the proper places.
        if saved_instance.emrepisode.episode_type == "tcm":
            if saved_instance.emrformtype.get_name() == "pps aoda discharge":
                aoda_admin_formtype = client.models.EmrFormType.objects.get(name__iexact="pps aoda admission", type_tcm=True).id
                path_override = f"/client/emr_episodes/{emr_form.emrepisode.client.id}/{emr_form.emrepisode.id}/form/{aoda_admin_formtype}/"

            elif saved_instance.emrformtype.get_name() == "pps mental health discharge":
                mh_admin_formtype = client.models.EmrFormType.objects.get(name__iexact="pps mental health admission", type_tcm=True).id
                path_override = f"/client/emr_episodes/{emr_form.emrepisode.client.id}/{emr_form.emrepisode.id}/form/{mh_admin_formtype}/"
            else:
                path_override = f"/client/emr_episodes/{emr_form.emrepisode.client.id}/{emr_form.emrepisode.id}/form/{emr_form.emrformtype.id}/"

        if saved_instance.emrepisode.episode_type == "cst":
            if saved_instance.emrformtype.get_name() == "pps mental health discharge":
                mh_admin_formtype = client.models.EmrFormType.objects.get(name__iexact="pps mental health admission", type_cst=True).id
                path_override = f"/client/emr_episodes/{emr_form.emrepisode.client.id}/{emr_form.emrepisode.id}/form/{mh_admin_formtype}/"

            else:
                path_override = f"/client/emr_episodes/{emr_form.emrepisode.client.id}/{emr_form.emrepisode.id}/form/{emr_form.emrformtype.id}/"


        else:
            path_override = f"/client/emr_episodes/{emr_form.emrepisode.client.id}/{emr_form.emrepisode.id}/form/{emr_form.emrformtype.id}/"






    return save_confirm(
        request=request,
        instance=saved_instance,
        path_override=path_override,
    )



@get_logged_in
def emr_form_supervisor_sign(request, emr_form_id):
    # Check if the logged in user has the right permission bit for the
    # formtype and episode type. If they do, then apply their signature
    # to the form.
    # If they don't throw a 403 error
    emr_form = EmrForm.objects.get(id=emr_form_id).get_emrform_instance()

    if emr_form.supervisor_pwd_provided is False:
        view_audit(request, "WARN")
        raise Exception("EmrForm needs supervisor_pwd_provided before signing as a supervisor")


    if emr_form.supervisor_can_sign(request.user) is False:
        # The form is not able to be signed by this user. User's should only
        # see this link if supervisor_can_sign is True so raise warning
        view_audit(request, "WARN")
        raise Exception("This user does not have the right to sign this form as a supervisor")
    else:
        # Apply the signature and redirect to the supervisor signed form
        emr_form.supervisor_signer = request.user
        emr_form.supervisor_signature = emr_form.generate_supervisor_signature(request)
        emr_form.supervisor_signed_date = timezone.now()

        confirmation = save_confirm(
            request, save_audit(request, emr_form),
            f"/client/emr_episodes/{emr_form.emrepisode.client.id}/{emr_form.emrepisode.id}/form/{emr_form.emrformtype.id}/"
        )

        emr_form.save_history("supervisor_sign", request)

        return confirmation




@get_logged_in
def emr_form_unlock(request, emr_form_id):
    # If the user has permission, unlock the form/casenote by removing the
    # signature and setting is_fnialized and is_open bits
    if request.user.userconfig.unlock_forms is False:
        view_audit(request, "WARN")
        raise PermissionDenied


    emr_form = EmrForm.objects.get(id=emr_form_id)
    if hasattr(emr_form, "is_open"):
        emr_form.is_open = True

    emr_form.save_history("reopen", request)
    emr_form.reopen()


    trans_results = Transaction.objects.filter(emr_form=emr_form)


    if len(trans_results) > 1:
        # Also should never hit this but it could be a good way to detect
        # problems so I will leave it
        return format_errors(
            request,
            None,
            "Error reopening this case note. Please contact support.")

    if len(trans_results) == 1:
        trans = trans_results.first()

        # check if it's in a journal entry..
        if trans.journal_entry:
            # We allow reopening of forms that have been journaled, but
            # the fields related to the transaction (service times, units,
            # billing codes) will remain locked on the form.
            # The logic that handles this gets implemented right on the model
            # form using lock_financial_fields()
            pass

        else:
            # Delete the transaction and reset case note
            delete_audit(request, trans)


    return save_confirm(
        request,
        save_audit(request, emr_form),
        f"/client/emr_form/{emr_form.id}/"
    )








@get_logged_in
def emr_doc_view(request, emr_doc_id):
    # This view basically redirects to the main listview for this
    # form type, then does the popup for the form!
    emr_doc = get_object_or_404(EmrDoc, id=emr_doc_id)

    return HttpResponseRedirect(f"/client/emr_episodes/{emr_doc.emrepisode.client.id}/{emr_doc.emrepisode.id}/doc/{emr_doc.emrdoctype.id}/?show_popup={emr_doc.id}")



@get_logged_in
def emr_doc_edit(request, doc_type_id=None, episode_id=None, emr_doc_id=None):
    if emr_doc_id:
        instance = get_object_or_404(EmrDoc, id=emr_doc_id)
        emr_doc_type = instance.emrdoctype
        episode = instance.emrepisode
        title = f"{emr_doc_type.name} Document"

    else:
        emr_doc_type = get_object_or_404(EmrDocType, id=doc_type_id)
        episode = get_object_or_404(EmrEpisode, id=episode_id)
        instance = EmrDoc(
            emrepisode=episode,
            emrdoctype=emr_doc_type,
            uploaded_by=request.user,
            date=timezone.now().date,
            upload_date=timezone.now(),
        )

        title = f"New {emr_doc_type.name} Document"


    if request.method == "GET":
        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"
        form = EmrDocForm(instance=instance)



        return render(request, "client/emr_doc_edit.html", {
            "instance": instance,
            "title": title,
            "form": form,
            "emr_doc_type": emr_doc_type,
            "episode": episode,
            "popup": popup,
        })


    elif request.method == "POST":
        instance = get_object_or_None(EmrDoc, id=emr_doc_id)

        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, f"/client/emr_episodes/{instance.emrepisode.client.id}/{instance.emrepisode.id}/")



        form = EmrDocForm(request.POST, instance=instance)


        if form.is_valid():
            # Because we're using the model's ID as a directory path in the
            # media directory (media/emr_doc_id/filename) we need to save the
            # record first without the file, get the ID, then save again with
            # the file. It's wonky but it works.
            if request.FILES:
                obj = form.save()
                form = EmrDocForm(request.POST, request.FILES, instance=obj)
                if form.is_valid():
                    print("FILES: {}".format(request.FILES))
                    obj.upload_date = timezone.now()
                    obj = form.save(commit=False)
                else:
                    return format_errors(request, form=form)
            else:
                obj = form.save(commit=False)

            return save_confirm(request, save_audit(request, form), f"/client/emr_episodes/{obj.emrepisode.client.id}/{obj.emrepisode.id}/{obj.emrdoctype.id}/")
        else:
            return format_errors(request, form=form)


@get_logged_in
def emr_doc_file(request, emr_doc_id, emr_doc_id2=None, filename=None):
    # There are 2 ids because of the default way that the django file
    # widget serves up the file's link. We just need a compatible URL
    # The emr_doc_id2 and filename params are both just ignored.
    # All we really need is the emr doc's id

    emr_doc = get_object_or_404(EmrDoc, id=emr_doc_id)

    file_path = os.path.join(settings.MEDIA_ROOT, f"{emr_doc.doc_file.name}")

    if os.path.exists(file_path):
        return FileResponse(open(file_path, "rb"))



@get_logged_in
def emr_doc_unlink(request, emr_doc_id, emr_form_id):
    emr_form = get_object_or_404(EmrForm, id=emr_form_id)
    emr_doc = get_object_or_404(EmrDoc, id=emr_doc_id)

    emr_form.linked_docs.remove(emr_doc)
    return save_confirm(
        request=request,
        instance=save_audit(request, emr_form),
        path_override=f"/client/emr_form/{emr_form_id}/"
    )









@get_logged_in
def pps_episodes_index(request, client_id):
    client = get_object_or_404(Client, id=client_id)

    pps_aoda_epi = PpsAodaEpi.objects.filter(client__id=client_id).order_by("-end")
    pps_mh_epi = PpsMhEpi.objects.filter(client__id=client_id).order_by("-end")
    pps_core_epi = PpsCoreEpi.objects.filter(client__id=client_id).order_by("-end")
    pps_b3_epi = PpsB3Epi.objects.filter(client__id=client_id).order_by("-end")

    return render(request, "client/pps_episodes_index.html", {
        "title" : "PPS Episodes",
        "client" : client,
        "client_tab" : "pps_episodes",
        "pps_aoda_epi" : pps_aoda_epi,
        "pps_mh_epi" : pps_mh_epi,
        "pps_core_epi" : pps_core_epi,
        "pps_b3_epi" : pps_b3_epi,
        })


@get_logged_in
def case_notes_index(request, client_id):
    client = get_object_or_404(Client, id=client_id)
    casenotes = CaseNote.objects.filter(client__id=client_id, pps_epi_uuid=None).order_by("-date")[:25]

    # This is just for legacy b3 case notes.
    # TODO make even showing this legacy area require a setting in the app_config
    if request.user.userconfig.user_type == "4" and request.user.userconfig.auditor_episode_type is not None and request.user.userconfig.auditor_episode_type != "b3":
        # User does not have access to b3, return warn.
        print("Restricted!")
        view_audit(request, "WARN")
        raise PermissionDenied


    return render(request, "client/case_notes_index.html", {
        "title" : "Case Notes",
        "client" : client,
        "client_tab" : "case_notes",
        "casenote_data" : casenotes,
        })


@get_logged_in
def state_forms_index(request, client_id):
    client = get_object_or_404(Client, id=client_id)

    return render(request, "client/state_forms_index.html", {
        "title" : "State Forms",
        "client" : client,
        "client_tab" : "state_forms",
        'state_forms' : get_state_forms(client),
        })




@get_logged_in
def client_edit(request, client_id=None):
    frm = EditClientForm

    if client_id:
        instance = get_object_or_404(Client, id=client_id)
        subtitle = instance.name
        title = "Client"
        #assigned_staff = instance.assigned_staff.all()#.filter(client__id=client_id).order_by('user')
        addresses = Address.objects.filter(client__id=client_id).order_by("-active", "-id")
        contactinfos = ContactInfo.objects.filter(client__id=client_id).order_by("-active")
        insurance = Insurance.objects.filter(client__id=client_id).order_by("-id")

        # Add the client to the user's recently viewed clients list. this is
        # typically used for showing a list of recent clients in the
        # caseworker_login view
        request.user.userconfig.add_recent_client(instance)


        casenotes = None
        if request.user.userconfig.notes_under_demo:
            casenotes = CaseNote.objects.filter(client__id=client_id, pps_epi_uuid=None).order_by("-date")[:25]


    else:
        instance = None
        subtitle = ""
        title = "New Client"
        addresses = None
        contactinfos = None
        casenotes = None
        insurance = None


    if request.method == "GET":
        form = frm(instance=instance)


        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"
        else:
            popup = ""


        return render(request, 'client/client_edit.html', {
            "request": request,
            'form': form,
            'title': title,
            'subtitle': subtitle,
            'address_data': addresses,
            'instance': instance,
            "client": instance,
            'contact_data': contactinfos,
            'address_data': addresses,
            "insurance_data": insurance,
            'casenote_data': casenotes,
            'popup': popup,
            "client_tab": "facesheet",
        })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, path_override="/client/me")

        form = frm(request.POST, instance=instance)

        if form.is_valid():
            obj = save_audit(request, form)
            if client_id is None:
                # add the loggedin user to this client's assigned staff
                obj.assigned_staff.add(get_user_config(request))

            return save_confirm(request=request, instance=obj, path_override="/client/{}/".format(obj.pk))

        else:
            return format_errors(request, form)


@get_logged_in
def address_edit(request, address_id=None):
    app = "client"
    mod = Address
    txt = "Address"
    frm = EditAddressForm

    if address_id:
        instance = get_object_or_404(mod, id=address_id)
        title = txt
    else:
        instance = None
        title = "New " + txt

    if request.method == "GET":
        if address_id:
            #edit
            form = frm(instance=instance)
            parent = Client.objects.get(pk=(instance.client_id))
        else:
            #new
            parent = Client.objects.get(pk=request.GET.get('client'))
            obj = mod(client=parent)
            form = frm(instance=obj)

        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"
        else:
            popup = ""

        return render(request, f"{app}/{txt.lower()}_edit.html", {
            'form': form,
            'title': title,
            'subtitle': parent.name,
            'popup': popup,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            client_id = instance.client_id
            delete_audit(request, instance)
            return delete_confirm(request, instance, "/client/{}".format(client_id))

        form = frm(request.POST, instance=instance)

        if form.is_valid():
            obj = save_audit(request, form)
            # set primary address. there can only be one!
            # TODO since primary isn't being used any more, we're actually
            # checking if this is a new physical address, if it is, then
            # we need to deactivate the homeless address (if there is one)


            obj.save()

            '''
            if obj.primary is True:
                other_obj = Address.objects.filter(client=obj.client, primary=True).exclude(pk=obj.id).first()
                if other_obj is not None:
                    other_obj.primary = False
                    other_obj.save()
            else:
                other_primary = Address.objects.filter(client=obj.client, primary=True).exclude(pk=obj.id).first()
                if not other_primary:
                    obj.primary = True
                    obj.save()
            '''

            return save_confirm(request, save_audit(request, form))

        else:
            return format_errors(request, form)



@get_logged_in
def contact_edit(request, contact_id=None):
    if contact_id:
        instance = get_object_or_404(ContactInfo, id=contact_id)
        title = "Contact"
    else:
        instance = None
        title = "New Contact"

    if request.method == "GET":
        if contact_id:
            #edit
            form = ContactInfoForm(instance=instance)
        else:
            #new
            form = ContactInfoForm()

            form.fields["client"].initial = request.GET.get('client')
            form.fields["type_of_contact"].initial = None


        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"
        else:
            popup = ""

        return render(request, "client/contact_edit.html", {
            'form': form,
            'title': title,
            'popup': popup,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            client_id = instance.client_id
            delete_audit(request, instance)
            return delete_confirm(request, instance, "/client/{}".format(client_id))

        form = ContactInfoForm(request.POST, instance=instance)

        if form.is_valid():
            return save_confirm(request, save_audit(request, form))
        else:
            return format_errors(request, form)





@get_logged_in
def insurance_edit(request, insurance_id=None):
    frm = InsuranceForm

    if insurance_id:
        instance = get_object_or_404(Insurance, id=insurance_id)
        title = "Insurance"
    else:
        instance = None
        title = "New Insurance"


    if request.method == "GET":
        form = frm(instance=instance)

        form.fields["client"].initial = request.GET.get('client')

        popup = ""
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"

        #form = set_autocomplete(instance, form, "FIELD_NAME")


        return render(request, "client/insurance_edit.html", {
            'form': form,
            'title': title,
            'popup': popup,
            "instance": instance,
        })


    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance, f"/client/{instance.id}/")

        form = frm(request.POST, instance=instance)
        if form.is_valid():
            return save_confirm(request, save_audit(request, form))
        else:
            return format_errors(request, form=form)








@get_logged_in
def assigned_staff_edit(request, assigned_staff_id=None):
    #print("USERTYPE {}".format(request.usertype))

    if assigned_staff_id:
        instance = get_object_or_404(Client, id=assigned_staff_id)
        title = "Assigned Staff"
    else:
        instance = None
        title = "New Assigned Staff"

    if request.method == "GET":
        form = EditAssignedStaffForm(instance=instance)

        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"
        else:
            popup = ""

        return render(request, 'client/assignedstaff_edit.html', {
            'form':form,
            'title':title,
            'popup': popup,
            })

    elif request.method == "POST":
        form = EditAssignedStaffForm(request.POST, instance=instance)

        if form.is_valid():
            return save_confirm(request, save_audit(request, form))
        else:
            return format_errors(request, form)



@get_logged_in
def casenote_edit(request, casenote_id=None):
    app = "client"
    mod = CaseNote
    txt = "casenote"
    frm = CaseNoteForm

    if casenote_id:
        instance = get_object_or_404(mod, id=casenote_id)
        if instance.pps_epi_uuid:
            title = "{} Service".format(instance.get_pps_epi_name())
        else:
            title = "Case Note"
    else:
        instance = None
        if request.GET.get("ppsepi"):
            title = "New {} Service".format(utils.get_pps_epi(request.GET.get("ppsepi")).get_name())
        else:
            title = "New Case Note"

    if request.method == "GET":
        if casenote_id:
            #edit
            form = frm(instance=instance)

            # if the user didn't sign their name, move that field to the top
            try:
                if "electronic_signature" in request.error["errors"]:
                    form.order_fields(["electronic_signature"])
            except:
                pass

            parent = Client.objects.get(pk=(instance.client_id),)
            file_data = CaseNoteFile.objects.filter(case_note_id=instance.id)

            form = set_autocomplete(instance, form, 'diagnosis')
            form = set_autocomplete(instance, form, 'service')
            form = set_autocomplete(instance, form, 'client')

            form.fields['client_lookup'].widget.attrs['readonly'] = True

        else:
            #new
            if request.GET.get('client'):
                parent = Client.objects.get(pk=request.GET.get('client'))
            else:
                parent = None

            obj = mod(client=parent)  # should call this the instance, but whatever

            if request.GET.get("ppsepi"):
                # apply the uuid!
                obj.pps_epi_uuid = request.GET.get("ppsepi")

            # since we're creating the new instance ourselves, we need
            # to set initial values like this
            obj.hours = 0
            obj.minutes = 0

            form = frm(instance=obj)


            if parent:
                form = set_autocomplete(None, form, 'client', Client, parent.id)

            # check if the user has a default_service set in their config
            # if they do, make sure it doesn't conflict with the PPS epi
            # they're trying to use
            if request.user.userconfig.default_service:
                default_service = request.user.userconfig.default_service
                if obj.pps_epi_uuid:
                    # make sure it's compatiable with the user's service
                    episode = get_pps_epi(obj.pps_epi_uuid)

                    if isinstance(episode, PpsAodaEpi) and default_service.pps_aoda:
                        form = set_autocomplete(instance, form, "service", Service, default_service.id)

                    elif isinstance(episode, PpsMhEpi) and default_service.pps_mh:
                        form = set_autocomplete(instance, form, "service", Service, default_service.id)

                    elif isinstance(episode, PpsCoreEpi) and default_service.pps_core:
                        form = set_autocomplete(instance, form, "service", Service, default_service.id)

                    elif isinstance(episode, PpsB3Epi) and default_service.pps_b3:
                        form = set_autocomplete(instance, form, "service", Service, default_service.id)

                else:
                    # this is not a PPS episode-based service, so whatever
                    form = set_autocomplete(instance, form, "service", Service, default_service.id)

            form.fields['client_lookup'].widget.attrs['readonly'] = True

            file_data = ''

        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"
        else:
            popup = ""

        form.fields["rate"].queryset = Rate.objects.filter(active=True).order_by('name')

        #generate userconfiged QuickPicks right-click menu on the case-note note field
        uc = UserConfig.objects.filter(user=request.user).first()
        qp = ''
        if uc is not None and uc.quick_picks is not None:
            for q in uc.quick_picks.split('|'):
                fmt = q.strip()
                qp += '{title:"'+fmt+'", cmd:"'+fmt+'",},'  # TODO fix this!!



        return render(request, app+'/'+txt.lower()+'_edit.html', {
            'form':form,
            'locked' : form.locked,
            'title':title,
            'parent' : parent,
            'popup' : popup,
            'table_class': 'listview-table-page',
            'instance' : instance,
            'file_data' : file_data,
            'quickpicks' : qp,
            })

    elif request.method == "POST":
        # User cannot delete a casenote!
        try:
            instance.request = request
        except:
            pass

        if instance:
            form = frm(request.POST, instance=instance)
        else:
            form = frm(request.POST)

        if form.is_valid():
            obj = form.save(commit=False)
            if obj.is_open is False:
                # already close, don't save, dont make cahnges
                return format_errors(request, obj, "Cannot modify a closed case note.")
            if obj.client_id is None:
                obj.client_id = request.GET.get("client")
            obj = save_audit(request, form)

            # TODO maybe move this to its own definition def close_casenote
            if request.POST.get('close_casenote') and request.POST.get('close_casenote') == 'close_casenote':
                # Close the casenote
                # IF we require a signature, make sure to make that field
                # required and re-validate the form
                validation.validate_casenote_close(form)
                if form.errors:
                    return format_errors(request, form)


                if obj.is_open is True and form.is_valid():
                    obj.is_open = False

                    # depending on requireing a signature, save the caseworker's sig
                    model_data = obj.get_sig_items()
                    model_data.pop("caseworker_signature")
                    cw_signed = {
                        "user_id": request.user.id,
                        "casenote_requires_signature": get_app_config("casenote_requires_signature"),
                        "data": model_data,
                    }

                    obj.caseworker_signature = sign_data(cw_signed)
                    obj.generate_signature(save=True)

                    save_audit(request, obj)


                    if obj.hours:
                        hours = obj.hours
                    else:
                        hours = 0

                    if obj.minutes:
                        minutes = obj.minutes
                    else:
                        minutes = 0.00



                    # time to create a transaction
                    trans = Transaction()
                    trans.case_note = obj
                    trans.date = dt.datetime.now()
                    trans.service_date = obj.date
                    trans.service = obj.service
                    trans.description = "Case note transaction"
                    trans.amount = (Decimal(obj.rate.amount) * hours) + (Decimal(obj.rate.amount) * (Decimal(minutes) / Decimal('60.00')))
                    trans.account = obj.rate.account
                    trans.is_journal = False
                    trans.verified = False
                    trans.client = obj.client
                    trans.fiscal_year = obj.date.year
                    trans.fiscal_period = obj.date.month
                    trans.target_group = obj.target_group
                    # check if the service on the case note has PPS_units
                    # if those are hours, do the proper division and calculate
                    # the proper number of units
                    trans.units = generate_units(obj)

                    trans.amount = Decimal(trans.units) * Decimal(obj.rate.amount)

                    if obj.rate.account:
                        trans.trans_type = obj.rate.account.type

                    save_audit(request, trans)

                return save_confirm(request, obj, "/client/casenote/{}/".format(obj.id))


            return save_confirm(request, obj, "/client/casenote/{}/".format(obj.pk))
        else:
            return format_errors(request, form)


@get_logged_in
def casenote_reopen(request, casenote_id=None):
    """
        This is for legacy case notes (mostly b-3). Look to
        emr_casenote_reopen for the emr case notes
        Reopens a case note. This will clear out the existing sig data
        and make a log!
    """



    note = get_object_or_None(CaseNote, id=casenote_id)

    if not note:
        return HttpResponseRedirect("/client/me")

    # Need to also delete the transaction related to this note if there is one
    # Raise an error if the transaction is linked to a journal entry already
    trans_results = Transaction.objects.filter(case_note=note)

    if len(trans_results) > 1:
        return format_errors(request, None, "Error reopening this case note. Please contact support.")

    if len(trans_results) == 1:
        trans = trans_results.first()

        # check if it's in a journal entry..
        if trans.journal_entry:
            # TODO also check if the client has made any payments against
            # a transaction linked to this case note
            return format_errors(request, None, "Cannot reopen while a transaction linked to this case note is an a journal entry.", "/client/casenote/{}".format(casenote_id))

        else:
            # Delete the transaction and reset case note
            delete_audit(request, trans)
            note.signature = None
            note.caseworker_signature = None
            note.is_open = True
            saved = save_audit(request, note)

    else:
        # Reset the case note
        note.signature = None
        note.caseworker_signature = None
        note.is_open = True
        saved = save_audit(request, note)





    return HttpResponseRedirect("/client/casenote/{}".format(casenote_id))



@get_logged_in
def emr_casenote_reopen(request, emr_casenote_id):
    note = get_object_or_None(EmrCaseNoteForm, id=emr_casenote_id)

    if not note:
        raise Exception(f"Could not find casenote with the id of '{emr_casenote_id}'")


    trans_results = Transaction.objects.filter(emr_form=note)

    if len(trans_results) > 1:
        # Also should never hit this but it could be a good way to detect
        # problems so I will leave it
        return format_errors(
            request,
            None,
            "Error reopening this case note. Please contact support.")



    if len(trans_results) == 1:
        trans = trans_results.first()

        # check if it's in a journal entry..
        if trans.journal_entry:
            # We allow reopening of case note that have been journaled, but
            # the fields related to the transaction (service times, units,
            # billing codes) will remain locked on the form.
            # The logic that handles this gets implemented right on the model
            # form using lock_financial_fields()
            pass

            # Used to throw a validation error if the case note was attached
            # to a transaction that had been journaled.
            '''
            return format_errors(
                request,
                None,
                f"""Cannot reopen while a transaction linked to this case note
                is an a journal entry. Links to the journal entry and transaction are below.
                <br>
                Journal entry: <a href='/accounting/journal/entry/{trans.journal_entry.id}/'>{trans.journal_entry}</a>
                <br>
                Transaction: <a href='/accounting/journal_trans/{trans.id}/'>{trans}</a>""",
                "/client/emr_form/{}".format(emr_casenote_id))
            '''

        else:
            # Delete the transaction and reset case note
            delete_audit(request, trans)

    # Save a history on the casenote/form. Important that this happens
    # before unsign() since we're recording the original finalized info
    #utils.save_emrform_history(get_object_or_None(EmrForm, id=note.id), "reopen", request)

    note.save_history("reopen", request)
    # Very important to run this since we're modifying the note when saving
    # the history.
    #note.refresh_from_db()

    # Reset the case note

    note.reopen()



    save_audit(request, note)


    return HttpResponseRedirect(f"/client/emr_form/{emr_casenote_id}")




@get_logged_in
def casenote_view_all(request, client_id=None):
    '''
        Displays all the case notes for a selected client
    '''

    client = get_object_or_404(Client, id=client_id)
    notes = CaseNote.objects.filter(client=client).order_by("-date")

    return render(request, "client/casenote_view_all.html", {
        "title" : "All Case Notes for {}".format(client),
        "notes" : notes,
    })





@get_logged_in
def casenote_file(request, casenote_file_id=None):
    mod = CaseNoteFile
    frm = CaseNoteFileForm

    if casenote_file_id:
        instance = get_object_or_404(mod, id=casenote_file_id)
        title = "Case Note File"
    else:
        title = "New Case Note File"
        instance = mod()

    if request.method == "GET":
        if casenote_file_id:
            #edit
            form = frm(instance=instance)
            parent = CaseNote.objects.get(pk=(instance.case_note_id))
        else:
            #new
            parent = CaseNote.objects.get(pk=request.GET.get('casenote'))
            obj = mod(case_note=parent)
            form = frm(instance=obj)

        popup = ''
        if request.GET.get('popup') and request.GET.get('popup') == "true":
            popup = "popup"

        return render(request, 'client/casenote_file_edit.html', {
            'form': form,
            'title': title,
            'popup': popup,
        })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(request, instance)

        #print("FILES: {}".format(request.FILES))
        form = frm(request.POST, request.FILES, instance=instance)
        if form.is_valid():
            new_file = form.save(commit=False)
            new_file.request = request
            return save_confirm(request, save_audit(request, new_file))
        else:
            return format_errors(request, form)



@get_logged_in
def casenote_files(request, file_id, user_id, client_id, filename):
    file_path = os.path.join(settings.MEDIA_ROOT, f"casenote_files/{user_id}/{client_id}/{filename}")
    if os.path.exists(file_path):
        return FileResponse(open(file_path, "rb"))



@get_logged_in
def form_download(request, name=None):
    if name is None:
        return HttpResponse("Invalid form name")
    redirect = form_crosswalk(request, name)
    return HttpResponseRedirect(redirect)



@get_logged_in
def pps_epi_sort(request, uuid):
    episode = utils.get_pps_epi(uuid)

    if episode.__class__.__name__.lower() == "ppsaodaepi":
        return HttpResponseRedirect("/client/pps_aoda_epi/{}/".format(episode.id))

    if episode.__class__.__name__.lower() == "ppsmhepi":
        return HttpResponseRedirect("/client/pps_mh_epi/{}/".format(episode.id))

    if episode.__class__.__name__.lower() == "ppscoreepi":
        return HttpResponseRedirect("/client/pps_core_epi/{}/".format(episode.id))

    if episode.__class__.__name__.lower() == "ppsb3epi":
        return HttpResponseRedirect("/client/pps_b3_epi/{}/".format(episode.id))


'''
@get_logged_in
def ppsepi_index(request):
    pps_aoda_epi = PpsAodaEpi.objects.all().order_by("-end")
    pps_mh_epi = PpsMhEpi.objects.all().order_by("-end")
    pps_core_epi = PpsCoreEpi.objects.all().order_by("-end")
    pps_b3_epi = PpsB3Epi.objects.all().order_by("-end")

    return render(request, "accounting/authorization_index.html", {
        "title" : "PPS Episode",
        "pps_aoda_epi" : pps_aoda_epi,
        "pps_mh_epi" : pps_mh_epi,
        "pps_core_epi" : pps_core_epi,
        "pps_b3_epi" : pps_b3_epi,
        "table_class" : "listview-table",
    })
'''



@get_logged_in
def pps_aoda_edit(request, pk=None):
    if pk:
        instance = get_object_or_404(PpsAodaEpi, id=pk)
        title = "PPS AODA Episode"
        form = PpsAodaEpisodeForm(instance=instance)
    else:
        title = "New PPS AODA Episode"
        parent = Client.objects.get(pk=request.GET.get('client'))
        instance=PpsAodaEpi(client=parent)
        form = PpsAodaEpisodeForm(instance=instance)

    if request.method == "GET":
        form = set_autocomplete(instance, form, 'client')

        form.fields['client_lookup'].widget.attrs['readonly'] = True

        pps_services = CaseNote.objects.filter(pps_epi_uuid=instance.pps_epi_uuid)

        return render(request, 'client/ppsepi_edit.html', {
            'form':form,
            'title':title,
            'instance' : instance,
            "pps_services" : pps_services,
            })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(
                request, instance, f"/client/pps_episodes/{instance.client.id}")

        form = PpsAodaEpisodeForm(request.POST, instance=instance)
        if form.is_valid():
            obj = save_audit(request, form)
            return save_confirm(request, obj, "/client/pps_aoda_epi/{}/".format(obj.id))

        else:
            return format_errors(request, form)



@get_logged_in
def pps_mh_edit(request, pk=None):
    if pk:
        instance = get_object_or_404(PpsMhEpi, id=pk)
        title = "PPS Mental Health Episode"
        form = PpsMhEpisodeForm(instance=instance)
    else:
        title = "New PPS Mental Health Episode"
        parent = Client.objects.get(pk=request.GET.get('client'))
        instance=PpsMhEpi(client=parent)
        form = PpsMhEpisodeForm(instance=instance)

    if request.method == "GET":
        form = set_autocomplete(instance, form, 'client')
        form = set_autocomplete(instance, form, 'diagnosis1')
        form = set_autocomplete(instance, form, 'diagnosis2')
        form = set_autocomplete(instance, form, 'diagnosis3')
        form = set_autocomplete(instance, form, 'diagnosis4')
        form = set_autocomplete(instance, form, 'diagnosis5')

        form.fields['client_lookup'].widget.attrs['readonly'] = True

        pps_services = CaseNote.objects.filter(pps_epi_uuid=instance.pps_epi_uuid)

        return render(request, 'client/ppsepi_edit.html', {
            'form':form,
            'title':title,
            'instance' : instance,
            "pps_services" : pps_services,
            })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(
                request, instance, f"/client/pps_episodes/{instance.client.id}")

        form = PpsMhEpisodeForm(request.POST, instance=instance)
        if form.is_valid():
            obj = save_audit(request, form)
            return save_confirm(request, obj, "/client/pps_mh_epi/{}/".format(obj.id))

        else:
            return format_errors(request, form)




@get_logged_in
def pps_core_edit(request, pk=None):
    if pk:
        instance = get_object_or_404(PpsCoreEpi, id=pk)
        title = "PPS Core Episode"
        form = PpsCoreEpisodeForm(instance=instance)
    else:
        title = "New PPS Core Episode"
        parent = Client.objects.get(pk=request.GET.get('client'))
        instance=PpsCoreEpi(client=parent)
        form = PpsCoreEpisodeForm(instance=instance)

    if request.method == "GET":
        form = set_autocomplete(instance, form, 'client')

        form.fields['client_lookup'].widget.attrs['readonly'] = True

        pps_services = CaseNote.objects.filter(pps_epi_uuid=instance.pps_epi_uuid)

        return render(request, 'client/ppsepi_edit.html', {
            'form':form,
            'title':title,
            'instance' : instance,
            "pps_services" : pps_services,
            })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(
                request, instance, f"/client/pps_episodes/{instance.client.id}")

        form = PpsCoreEpisodeForm(request.POST, instance=instance)
        if form.is_valid():
            obj = save_audit(request, form)
            return save_confirm(request, obj, "/client/pps_core_epi/{}/".format(obj.id))

        else:
            return format_errors(request, form)




@get_logged_in
def pps_b3_edit(request, pk=None):
    if pk:
        instance = get_object_or_404(PpsB3Epi, id=pk)
        title = "PPS Birth to 3 Episode"
        form = PpsB3EpisodeForm(instance=instance)
    else:
        title = "New PPS Birth to 3 Episode"
        parent = Client.objects.get(pk=request.GET.get('client'))
        instance=PpsB3Epi(client=parent)
        form = PpsB3EpisodeForm(instance=instance)

    if request.method == "GET":
        form = set_autocomplete(instance, form, 'client')

        form.fields['client_lookup'].widget.attrs['readonly'] = True

        pps_services = CaseNote.objects.filter(pps_epi_uuid=instance.pps_epi_uuid)

        return render(request, 'client/ppsepi_edit.html', {
            'form':form,
            'title':title,
            'instance' : instance,
            "pps_services" : pps_services,
            })

    elif request.method == "POST":
        if request.POST.get('delete'):
            delete_audit(request, instance)
            return delete_confirm(
                request, instance, f"/client/pps_episodes/{instance.client.id}")

        form = PpsB3EpisodeForm(request.POST, instance=instance)
        if form.is_valid():
            obj = save_audit(request, form)
            return save_confirm(request, obj, "/client/pps_b3_epi/{}/".format(obj.id))

        else:
            return format_errors(request, form)


