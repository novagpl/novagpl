"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import json
from django.test import TestCase, Client

from django.contrib.auth.models import User
from django.utils import timezone
from config.models import UserConfig, AppConfig
from django.core.serializers.json import DjangoJSONEncoder
from django.core.exceptions import ValidationError
from django.test.utils import override_settings

import client.models
import config.models

from .urls import urlpatterns




# Create your tests here.


class EmrFormSignatureTest(TestCase):
    def setUp(self):

        urlmodel = config.models.AppConfig(
            key="url",
            value="http://testurl.notreal"
        )

        urlmodel.save()

        self.user = User.objects.create_user(
            "test@test.com", "test@test.com", "testpassword")

        self.userconfig = UserConfig(
            user=self.user,
            full_name="Test User",
            user_type="1",
        )
        self.userconfig.save()

        self.client = client.models.Client(
            first_name="Test client firstname",
            last_name="Testclinet lastname",
            middle_name="Testclient middle name",
            birthdate="1970-01-01",
            gender="1",
            race="4",
        )
        self.client.save()

        self.emrformtype = client.models.EmrFormType(
            name="CLTS",
            type_clts=True,
        )
        self.emrformtype.save()

        self.emrepisode = client.models.EmrEpisode(
            episode_type="clts",
            client=self.client,
            start="2020-01-01",
            opened_by=self.user,
        )

        self.emrepisode.save()


        self.emrform = client.models.EmrCrisisNwcNoteForm(
            emrepisode=self.emrepisode,
            emrformtype=self.emrformtype,
            alcohol_substance_factor="no",
            law_inv="yes",
            law_dept="Test County PD",
            law_dept_other="The Other Dept™",
            outcome_of_contact="info_only",
            outcome_other="Some text data",
            summary_highlight="yes",
            summary_highlight_dtl="Details here",
            county_of_residence="Pima County",
            county_of_incident="Dane County",
            day_of_week="tuesday",
            staff_name1="Test staff name 1",
            staff_creds1="Test staff creds 2",
            type_of_contact="audio",
            brc_target_pop="S",
            presenting_problem="1",
            char_code="2",
            is_finalized=True,
        )

        self.emrform.save()




        self.valid_emrsig_simple_typed = client.models.EmrFormSignature(
            emrform=self.emrform,
            signer_role="client",
            logged_in_user=self.user,
            signer_first="Signer's firstname",
            signer_last="Signers lastname",
            signer_middle="Signers middlename",
            sig_type="simple_typed",
            sig_data={
                "typed_sig": "someone's name typed out",
            },
            ip_address="192.168.0.1",
            useragent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36",
        )
        self.valid_emrsig_simple_typed.save()


        self.valid_emrsig_topaz = client.models.EmrFormSignature(
            emrform=self.emrform,
            signer_role="client",
            logged_in_user=self.user,
            signer_first="Signer's firstname",
            signer_last="Signers lastname",
            signer_middle="Signers middlename",
            sig_type="topaz",
            sig_data={
                "topaz_sigraw_data": "test data",
                "topaz_image_data": "test data",
            },
            ip_address="192.168.0.1",
            useragent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36",
        )
        self.valid_emrsig_topaz.save()


        self.valid_emrsig_mouse = client.models.EmrFormSignature(
            emrform=self.emrform,
            signer_role="client",
            logged_in_user=self.user,
            signer_first="Signer's firstname",
            signer_last="Signers lastname",
            signer_middle="Signers middlename",
            sig_type="mouse",
            sig_data={
                "mouse_image_data": "test data",
            },
            ip_address="192.168.0.1",
            useragent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36",
        )
        self.valid_emrsig_mouse.save()



        self.bad_emrsig_simple_typed = client.models.EmrFormSignature(
            emrform=self.emrform,
            signer_role="client",
            logged_in_user=self.user,
            signer_first="Signer's firstname",
            signer_last="Signers lastname",
            signer_middle="Signers middlename",
            sig_type="simple_typed",
            sig_data={"typed_sig": "", },
            ip_address="192.168.0.1",
            useragent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36",
        )
        # Don't save here, save in test_emr_form_bad_simple_typed_signature
        # because this should cause a validation error exception


        self.bad_emrsig_topaz = client.models.EmrFormSignature(
            emrform=self.emrform,
            signer_role="client",
            logged_in_user=self.user,
            signer_first="Signer's firstname",
            signer_last="Signers lastname",
            signer_middle="Signers middlename",
            sig_type="topaz",
            sig_data={
                "topaz_sigraw_data": "",
                "topaz_image_data": "",
            },
            ip_address="192.168.0.1",
            useragent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36",
        )
        # Don't save here, save in test_emr_form_bad_topaz_signature because
        # this should cause a validation error exception


        self.bad_emrsig_mouse = client.models.EmrFormSignature(
            emrform=self.emrform,
            signer_role="client",
            logged_in_user=self.user,
            signer_first="Signer's firstname",
            signer_last="Signers lastname",
            signer_middle="Signers middlename",
            sig_type="mouse",
            sig_data={
                "mouse_image_data": "",
            },
            ip_address="192.168.0.1",
            useragent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36",
        )
        # Don't save here, save in test_emr_form_bad_mouse_signature because
        # this should cause a validation error exception


        self.bad_emrsig_role = client.models.EmrFormSignature(
            emrform=self.emrform,
            signer_role=None,
            logged_in_user=self.user,
            signer_first="Signer's firstname",
            signer_last="Signers lastname",
            signer_middle="Signers middlename",
            sig_type="mouse",
            sig_data={
                "mouse_image_data": "testdata",
            },
            ip_address="192.168.0.1",
            useragent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36",
        )
        # Don't save here, save in test_emr_form_bad_rolesignature because
        # this should cause a validation error exception


    def test_emr_form_bad_role_signature(self):
        with self.assertRaises(ValidationError):
            self.bad_emrsig_role.save()


    def test_emr_form_bad_mouse_signature(self):
        with self.assertRaises(ValidationError):
            self.bad_emrsig_mouse.save()

    def test_emr_form_bad_topaz_signature(self):
        with self.assertRaises(ValidationError):
            self.bad_emrsig_topaz.save()

    def test_emr_form_bad_simple_typed_signature(self):
        with self.assertRaises(ValidationError):
            self.bad_emrsig_simple_typed.save()



    def test_emr_form_verify_signature(self):
        self.assertEqual(self.valid_emrsig_simple_typed.verify_signature(), True)


    def test_emr_form_save_error(self):
        # Once saved, these signatures shouldn't be able to be saved again
        with self.assertRaises(ValidationError):
            self.valid_emrsig_simple_typed.save()


    @override_settings(SECRET_KEY="badkey")
    def test_emr_form_bad_signature(self):
        # Because the form can't even be saved after it's been created, there's
        # not really a straightforward way to change the data on the signature
        # and re-validate and expect an error.
        # This test changes the secret key and then validates the signature
        # leading to a bad signature error
        # TODO test with using raw SQL to modify the data of an existing signature
        self.assertEqual(self.valid_emrsig_simple_typed.verify_signature(), False)












class FormSectionJsonTest(TestCase):
    def setUp(self):
        pass


    def test_validate_json_file(self):
        print("Testing form_section.json file")
        js_file = open("static/form_section.json", "r")

        js = js_file.read()
        js_file.close()

        json_is_valid = False

        try:
            json.loads(js)
            json_is_valid = True
        except json.decoder.JSONDecodeError:
            json_is_valid = False

        self.assertEqual(json_is_valid, True)



class GetLoggedInDecoratorTest(TestCase):
    print("Checking that all required views have the @get_logged_in decorator")
    # Loops through all the urlpatterns for this app and checks that each
    # function is properly wrapped by the get_logged_in decorator
    # Some views (external ones probably) may not need the get_logged_in wrapper
    # so they can be put into the get_logged_in_exceptions list

    def setUp(self):
        pass


def create_get_logged_in_test_methods(url):
    def get_logged_in_test_to_add(self):
        found = getattr(url.callback, "__get_logged_in", False)
        self.assertEqual(found, True)

    return get_logged_in_test_to_add


get_logged_in_exceptions = []
for url in urlpatterns:
    if url.name in get_logged_in_exceptions:
        continue

    test_method = create_get_logged_in_test_methods(url)
    test_method.__name__ = f"test_get_logged_in_decorator_exists__{url.name}"
    setattr(GetLoggedInDecoratorTest, test_method.__name__, test_method)





class GetNotLoggedInViewTests(TestCase):
    print("Testing redirect for not logged in client URLs")

    def setUp(self):
        pass


not_logged_in_urls = [
    #"",
    "all",
    "me",
    "new",
    "caseworker_login",
    "set_active/1",
    "set_inactive/1",
    "client_settings/1",
    "emr_episodes/1",
    "emr_episodes/1/2",
    "emr_episodes/1/2/form/3",
    "emr_episodes/1/2/doc/3",
    "new_emr_episode/epi_type/1",
    "emr_form/1",
    "emr_form/1/2",
    "emr_form/1/sign",
    "emr_form/1/unlock",
    "emr_doc/1",
    "emr_doc/1/2",
    "emr_doc/1/emr_docs/2/long-filename.jpg",
    "emr_doc/1/file",
    "emr_doc/1/view",
    "emr_doc/1/2/unlink",
    "emr_casenote/reopen/1",
    "emr_message",
    "emr_message/1",
    "pps_episodes/1",
    "case_notes_index/1",
    "state_forms/1",
    "address/1",
    "address/new",
    "contact/1",
    "contact/new",
    "insurance",
    "insurance/1",
    "assigned_staff",
    "assigned_staff/1",
    "casenote/new",
    "casenote/1",
    "casenote_file/1/casenote_files/2/3/long-filename.jpg",
    "casenote_file/1",
    "casenote_file/new",
    "casenote/view_all/1",
    "casenote/reopen/1",
    "legal_name/1/2",
    "legal_name/1/new",
    "atp/1",
    "atp/new",
    "form/form-download-name",
    "ajax_duplicate_name_detect",
]

def create_not_logged_in_test_method(url):
    def test_to_add(self):
        response = self.client.get(f"/client/{url}/")
        self.assertEqual(response.status_code, 302)

    return test_to_add

# Build methods for each URL to test so the output is easier to read
# This is dynamically adding methods to the test case class, pretty neat
for url in not_logged_in_urls:
    test_method = create_not_logged_in_test_method(url)
    test_method.__name__ = f"test_not_logged_in_redirect__{url.replace('/', '-')}"
    setattr(GetNotLoggedInViewTests, test_method.__name__, test_method)







class Status200Test(TestCase):
    # This tests all the URLs in urlpatterns to make sure the page will load
    # with a properly configured user. Basically make sure the page can load
    # a GET request without crashing.
    print("Testing client URLs for crashes with GET requests")

    def setUp(self):
        self.client = Client()

        fy = AppConfig(
            key="fiscal_year_open_from",
            value="2023",
        )
        fy.save()

        fp = AppConfig(
            key="fiscal_period_open_from",
            value="1",
        )
        fp.save()

        self.tester = User.objects.create_user(
            "test@test.com", "test@test.com", "testpassword")

        uc = UserConfig(
            user=self.tester,
            full_name="Test User",
            user_type="1",
        )

        uc.save()


        nova_client = client.models.Client(
            id=1,
            first_name="Ricky",
            last_name="Tester",
            birthdate="1980-06-01",
            gender="2",
        )
        nova_client.save()


        nova_client2 = client.models.Client(
            id=2,
            first_name="Micky",
            last_name="Mouse",
            birthdate="1990-05-25",
            gender="1",
        )
        nova_client2.save()


        nova_client3 = client.models.Client(
            id=2,
            first_name="Ted",
            last_name="Bundy",
            birthdate="1956-10-31",
            gender="1",
        )
        nova_client3.save()


        episode = client.models.EmrCltsEpisode(
            id=1,
            episode_type="clts",
            client=nova_client,
            opened_by=self.tester,
            start=timezone.now().date(),
            notes="This is a test episode for unit tests"


        )
        episode.save()






    def test_client_404(self):
        self.client.login(username=self.tester.username, password="testpassword")
        response = self.client.get('/client/a_url_that_is_definitely_not_a_teapot/')
        self.assertEqual(response.status_code, 404)

        response = self.client.get('/a_url_that_is_definitely_not_a_teapot/a_url_that_is_definitely_not_a_teapot/')
        self.assertEqual(response.status_code, 404)

        response = self.client.get('/a_url_that_is_definitely_not_a_teapot/')
        self.assertEqual(response.status_code, 404)


    def test_client_302(self):
        # Some client URLs are redirects so they can be tested here
        response = self.client.get("/client/emr_episodes/1/")
        self.assertEqual(response.status_code, 302)




def create_status200_test_method(url):
    def status200_test_to_add(self):
        self.client.login(username=self.tester.username, password="testpassword")
        response = self.client.get(f"/client/{url}")
        self.assertEqual(response.status_code, 200)
    return status200_test_to_add




status200_urls = [
    "all/",
    "me/",
    "1/",
    "new/",
    "caseworker_login/",
    "client_settings/1/",
    "emr_episodes/1/1/",


]

for url in status200_urls:
    test_method = create_status200_test_method(url)
    test_method.__name__ = f"test_status200__{url.replace('/','-')}"
    setattr(Status200Test, test_method.__name__, test_method)
