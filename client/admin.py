"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


from django.contrib import admin
from django.apps import apps
from django.conf import settings

# Register your models here.
from .models import *


app = apps.get_app_config('client')

if settings.DEBUG:
    skip = ["emrformtype", "emrdoctype"]

    for model_name, model in app.models.items():
        if model_name in skip:
            continue
        admin.site.register(model)


    class EmrFormTypeAdmin(admin.ModelAdmin):
        list_display = (
            "name", "active", "electronic_sig", "type_clinic_preadmit", "type_clts", "type_clts_preadmit",
            "type_crisis", "type_cst", "type_randi", "type_tcm", "type_b3", "type_b3_preadmit",
        )


    admin.site.register(EmrFormType, EmrFormTypeAdmin)


    class EmrDocTypeAdmin(admin.ModelAdmin):
        list_display = (
            "name", "active", "type_clinic_preadmit", "type_clts", "type_clts_preadmit",
            "type_crisis", "type_cst", "type_randi", "type_tcm", "type_b3", "type_b3_preadmit",
        )

    admin.site.register(EmrDocType, EmrDocTypeAdmin)



else:
    class EmrFormTypeAdmin(admin.ModelAdmin):
        list_display = (
            "name", "active", "type_clinic_preadmit", "type_clts", "type_clts_preadmit",
            "type_crisis", "type_cst", "type_randi", "type_tcm", "type_b3", "type_b3_preadmit",
        )


    admin.site.register(EmrFormType, EmrFormTypeAdmin)


    class EmrDocTypeAdmin(admin.ModelAdmin):
        list_display = (
            "name", "active", "type_clinic_preadmit", "type_clts", "type_clts_preadmit",
            "type_crisis", "type_cst", "type_randi", "type_tcm", "type_b3", "type_b3_preadmit",
        )

    admin.site.register(EmrDocType, EmrDocTypeAdmin)





