"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.20 on 2024-12-11 19:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0650_alter_emrcstppsmhdischargeform_cst_discharge_reason'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_end',
            field=models.DateField(blank=True, null=True, verbose_name='End'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_end10',
            field=models.DateField(blank=True, null=True, verbose_name='End'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_end2',
            field=models.DateField(blank=True, null=True, verbose_name='End'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_end3',
            field=models.DateField(blank=True, null=True, verbose_name='End'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_end4',
            field=models.DateField(blank=True, null=True, verbose_name='End'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_end5',
            field=models.DateField(blank=True, null=True, verbose_name='End'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_end6',
            field=models.DateField(blank=True, null=True, verbose_name='End'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_end7',
            field=models.DateField(blank=True, null=True, verbose_name='End'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_end8',
            field=models.DateField(blank=True, null=True, verbose_name='End'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_end9',
            field=models.DateField(blank=True, null=True, verbose_name='End'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_start',
            field=models.DateField(blank=True, null=True, verbose_name='Start'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_start10',
            field=models.DateField(blank=True, null=True, verbose_name='Start'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_start2',
            field=models.DateField(blank=True, null=True, verbose_name='Start'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_start3',
            field=models.DateField(blank=True, null=True, verbose_name='Start'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_start4',
            field=models.DateField(blank=True, null=True, verbose_name='Start'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_start5',
            field=models.DateField(blank=True, null=True, verbose_name='Start'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_start6',
            field=models.DateField(blank=True, null=True, verbose_name='Start'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_start7',
            field=models.DateField(blank=True, null=True, verbose_name='Start'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_start8',
            field=models.DateField(blank=True, null=True, verbose_name='Start'),
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type_start9',
            field=models.DateField(blank=True, null=True, verbose_name='Start'),
        ),
    ]
