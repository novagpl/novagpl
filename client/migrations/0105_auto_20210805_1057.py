"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.5 on 2021-08-05 15:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0104_alter_emrrandiintakeform_insurance_detail'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='emrform',
            name='hours',
        ),
        migrations.AlterField(
            model_name='ppsb3epi',
            name='result_reported',
            field=models.CharField(blank=True, choices=[('1', 'Unable to Connect with Family'), ('2', 'No Evaluation Through Birth to 3 Program'), ('3', 'Child Not Eligible for Birth to 3 Program'), ('4', 'Child Eligible, Family Declined'), ('5', 'Family Declined Participation in one or more services'), ('6', 'Family Chose to discontinue a previously received service')], max_length=2, null=True),
        ),
    ]
