"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.21 on 2024-03-06 17:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0559_auto_20240306_1113'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='emrcasenoteform',
            name='crisis_type_of_contact',
        ),
        migrations.AddField(
            model_name='emrform',
            name='crisis_type_of_contact',
            field=models.CharField(blank=True, choices=[('audio', 'Audio-only'), ('face', 'Face to face'), ('mobile', 'Mobile'), ('mobile_team', 'Mobile team response'), ('telehealth', 'Telehealth'), ('walkin', 'Walk-in'), ('other', 'Other')], max_length=255, null=True, verbose_name='Type of contact'),
        ),
    ]
