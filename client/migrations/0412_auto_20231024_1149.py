"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.21 on 2023-10-24 16:49

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0411_auto_20231023_1424'),
    ]

    operations = [
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_2_address',
            new_name='contact_address2',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_1_address',
            new_name='contact_address3',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_3_address',
            new_name='contact_address4',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_4_address',
            new_name='contact_address5',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_1_avil',
            new_name='contact_avil',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_2_avil',
            new_name='contact_avil2',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_3_avil',
            new_name='contact_avil3',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_4_avil',
            new_name='contact_avil4',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_5_avil',
            new_name='contact_avil5',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_1_name',
            new_name='contact_name',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_2_name',
            new_name='contact_name2',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_3_name',
            new_name='contact_name3',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_4_name',
            new_name='contact_name4',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_5_name',
            new_name='contact_name5',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_1_phone',
            new_name='contact_phone',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_2_phone',
            new_name='contact_phone2',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_3_phone',
            new_name='contact_phone3',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_4_phone',
            new_name='contact_phone4',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_5_phone',
            new_name='contact_phone5',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_1_relation',
            new_name='contact_relation',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_2_relation',
            new_name='contact_relation2',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_3_relation',
            new_name='contact_relation3',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_4_relation',
            new_name='contact_relation4',
        ),
        migrations.RenameField(
            model_name='emrcrisisalertform',
            old_name='contact_5_relation',
            new_name='contact_relation5',
        ),
        migrations.RemoveField(
            model_name='emrcrisisalertform',
            name='contact_5_address',
        ),
    ]
