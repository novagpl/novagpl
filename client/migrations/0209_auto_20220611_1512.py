"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.10 on 2022-06-11 20:12

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('client', '0208_rename_finalized_at_emrform_finalized_date'),
    ]

    operations = [
        migrations.RenameField(
            model_name='emrdoc',
            old_name='doc_file_upload_date',
            new_name='upload_date',
        ),
        migrations.RemoveField(
            model_name='emrdoc',
            name='created_by',
        ),
        migrations.AddField(
            model_name='emrdoc',
            name='uploaded_by',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='emr_doc_uploaded_by', to=settings.AUTH_USER_MODEL),
        ),
    ]
