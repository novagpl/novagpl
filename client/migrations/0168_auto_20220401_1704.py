"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.10 on 2022-04-01 22:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0167_address_note'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emrcrisisassessmentform',
            name='county_of_incident',
            field=models.CharField(blank=True, choices=[('1', 'Adams'), ('2', 'Ashland'), ('3', 'Barron'), ('4', 'Bayfield'), ('5', 'Brown'), ('6', 'Buffalo'), ('7', 'Burnett'), ('8', 'Calumet'), ('9', 'Chippewa'), ('10', 'Clark'), ('11', 'Columbia'), ('12', 'Crawford'), ('13', 'Dane'), ('14', 'Dodge'), ('15', 'Door'), ('16', 'Douglas'), ('17', 'Dunn'), ('18', 'Eau Claire'), ('19', 'Florence'), ('20', 'Fond du Lac'), ('21', 'Forest'), ('22', 'Grant'), ('23', 'Green'), ('24', 'Green Lake'), ('25', 'Iowa'), ('26', 'Iron'), ('27', 'Jackson'), ('28', 'Jefferson'), ('29', 'Juneau'), ('30', 'Kenosha'), ('31', 'Kewaunee'), ('32', 'La Crosse'), ('33', 'Lafayette'), ('34', 'Langlade'), ('35', 'Lincoln'), ('36', 'Manitowoc'), ('37', 'Marathon'), ('38', 'Marinette'), ('39', 'Marquette'), ('40', 'Milwaukee'), ('41', 'Monroe'), ('42', 'Oconto'), ('43', 'Oneida'), ('44', 'Outagamie'), ('45', 'Ozaukee'), ('46', 'Pepin'), ('47', 'Pierce'), ('48', 'Polk'), ('49', 'Portage'), ('50', 'Price'), ('51', 'Racine'), ('52', 'Richland'), ('53', 'Rock'), ('54', 'Rusk'), ('55', 'St Croix'), ('56', 'Sauk'), ('57', 'Sawyer'), ('58', 'Shawano'), ('59', 'Sheboygan'), ('60', 'Taylor'), ('61', 'Trempealeau'), ('62', 'Vernon'), ('63', 'Vilas'), ('64', 'Walworth'), ('65', 'Washburn'), ('67', 'Waukesha'), ('68', 'Waupaca'), ('68', 'Waushara'), ('70', 'Winnebago'), ('71', 'Wood'), ('72', 'Menominee'), ('84', 'Menominee Tribe'), ('85', 'Red Cliff Tribe'), ('86', 'Stockbridge Munsee'), ('87', 'Forrest County Potawatomi'), ('88', 'Lac du Flambeau'), ('89', 'Bad River Tribe'), ('91', 'Sokaogon Chippewa'), ('92', 'Oneida Tribe'), ('93', 'Ho Chunk Nation'), ('94', 'Lac Courte Oreilles Tribe'), ('95', 'St Croix Tribe'), ('0', 'Unknown')], max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='emrcrisisassessmentform',
            name='current_location',
            field=models.CharField(blank=True, choices=[('residential', 'Residential'), ('hospital', 'Hospital'), ('police_department', 'Police Department'), ('sheriff_department', "Sherrif's Department"), ('group_home', 'Group Home'), ('nursing_home', 'Nursing Home'), ('school', 'School'), ('other', 'Other')], max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='emrcrisisassessmentform',
            name='has_insurance',
            field=models.CharField(blank=True, choices=[('no', 'No'), ('yes', 'Yes'), ('unknown', 'Did not obtain')], max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='emrcrisisassessmentform',
            name='individual_char_code',
            field=models.CharField(blank=True, choices=[('2', 'Mental illness (excluding SPMI)'), ('3', 'Serious and persistent mental illness (SPMI)'), ('4', 'Alcohol client'), ('5', 'Drug client'), ('7', 'Blind / visually impaired'), ('8', 'Hard of hearing'), ('9', 'Physical disability / mobility impaired'), ('10', 'Severe alcohol or other drug client'), ('12', 'Alcohol and other drug client'), ('14', 'Family member of mental health client'), ('16', 'Family member of alcohol and other drug client'), ('17', 'Intoxicated driver'), ('18', "Alzheimer's disease / related dementia"), ('19', 'Developmental disability - brain trauma'), ('23', 'Developmental disability - cerebral palsy'), ('25', 'Developmental disability – autism spectrum'), ('26', 'Developmental disability – intellectual disability'), ('27', 'Developmental disability - epilepsy'), ('28', 'Developmental disability - other or unknown'), ('29', 'Family member of a client with a developmental disability'), ('32', 'Blind/Deaf'), ('33', 'Corrections / criminal justice client (adult only)'), ('36', 'Other disability'), ('37', 'Frail medical condition'), ('38', 'Criminal justice system involvement (alleged or adjudicated)'), ('39', 'Gambling client'), ('43', 'Migrant'), ('44', 'Refugee'), ('45', 'Cuban / Haitian entrant'), ('50', 'Regular caregiver of dependent person'), ('55', 'Frail elderly'), ('57', 'Abused / neglected elder'), ('59', 'Unmarried parent'), ('61', 'CHIPS - abuse and neglect'), ('62', 'CHIPS - abuse'), ('63', 'CHIPS - neglect'), ('64', 'Family member of abused / neglected child'), ('66', 'Delinquent'), ('68', 'CHIPS - other'), ('69', 'JIPS - status offender'), ('70', 'Family member of status offender'), ('71', 'Victim of domestic abuse'), ('72', 'Victim of abuse or neglect (alleged or adjudicated)'), ('73', 'Family member of delinquent'), ('74', 'Family member of CHIPS - other'), ('79', 'Deaf'), ('80', 'Homeless'), ('84', 'Repeated school truancy'), ('86', 'Severe emotional disturbance - child / adolescent'), ('90', 'Special study code (to be defined as need arises)'), ('91', 'Hurricane Katrina evacuee'), ('92', 'Hurricane Rita evacuee'), ('99', 'None of the above (codependent client only)')], max_length=255, null=True, verbose_name="Individual's characteristics"),
        ),
        migrations.AlterField(
            model_name='emrcrisisassessmentform',
            name='parent_guardian_contacted',
            field=models.CharField(blank=True, choices=[('na', 'Not applicable'), ('yes', 'Yes'), ('no', 'No')], max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='emrcrisisassessmentform',
            name='presenting_problem',
            field=models.CharField(blank=True, choices=[('1', 'Marital / family problem'), ('2', 'Social / interpersonal (other than family problem)'), ('3', 'Problems coping with daily roles and activities (including job, school, housework, daily grooming, financial management, etc.)'), ('4', 'Medical / somatic'), ('5', 'Depressed mood and / or anxious'), ('6', 'Attempt, threat, or danger of suicide'), ('7', 'Alcohol'), ('8', 'Drugs'), ('9', 'Involvement with criminal justice system'), ('10', 'Eating disorder'), ('11', 'Disturbed thoughts'), ('12', 'Abuse / assault / rape victim'), ('13', 'Runaway behavior'), ('14', 'Emergency detention'), ('99', 'Unknown')], max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='emrcrisisassessmentform',
            name='previous_crisis_contacts',
            field=models.BooleanField(blank=True, default=False, null=True),
        ),
        migrations.AlterField(
            model_name='emrcrisisassessmentform',
            name='student_status',
            field=models.CharField(blank=True, choices=[('no', 'No'), ('yes', 'Yes'), ('unknown', 'Did not obtain')], max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='emrcrisisassessmentform',
            name='veteran_status',
            field=models.CharField(blank=True, choices=[('no', 'No'), ('yes', 'Yes'), ('unknown', 'Did not obtain')], max_length=255, null=True),
        ),
    ]
