"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 2.2.4 on 2019-10-25 20:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0014_delete_targetgroup'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='pps_aoda',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='service',
            name='pps_brief_service',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='service',
            name='pps_core',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='service',
            name='pps_mh',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='service',
            name='pps_unit',
            field=models.CharField(blank=True, choices=[('1', 'Hours'), ('2', 'Days'), ('3', 'Other')], max_length=1, null=True),
        ),
        migrations.CreateModel(
            name='PpsAodaEpi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start', models.DateField()),
                ('end', models.DateField(null=True)),
                ('county', models.CharField(choices=[('1', 'Adams'), ('2', 'Ashland'), ('3', 'Barron'), ('4', 'Bayfield'), ('5', 'Brown'), ('6', 'Buffalo'), ('7', 'Burnett'), ('8', 'Calumet'), ('9', 'Chippewa'), ('10', 'Clark'), ('11', 'Columbia'), ('12', 'Crawford'), ('13', 'Dane'), ('14', 'Dodge'), ('15', 'Door'), ('16', 'Douglas'), ('17', 'Dunn'), ('18', 'Eau Claire'), ('19', 'Florence'), ('20', 'Fond du Lac'), ('21', 'Forest'), ('22', 'Grant'), ('23', 'Green'), ('24', 'Green Lake'), ('25', 'Iowa'), ('26', 'Iron'), ('27', 'Jackson'), ('28', 'Jefferson'), ('29', 'Juneau'), ('30', 'Kenosha'), ('31', 'Kewaunee'), ('32', 'La Crosse'), ('33', 'Lafayette'), ('34', 'Langlade'), ('35', 'Lincoln'), ('36', 'Manitowoc'), ('37', 'Marathon County'), ('38', 'Marinette'), ('39', 'Marquette'), ('40', 'Milwaukee'), ('41', 'Monroe'), ('42', 'Oconto'), ('43', 'Oneida'), ('44', 'Outagamie'), ('45', 'Ozaukee'), ('46', 'Pepin'), ('47', 'Pierce'), ('48', 'Polk'), ('49', 'Portage'), ('50', 'Price'), ('51', 'Racine'), ('52', 'Richland'), ('53', 'Rock'), ('54', 'Rusk'), ('55', 'St Croix'), ('56', 'Sauk'), ('57', 'Sawyer'), ('58', 'Shawano'), ('59', 'Sheboygan'), ('60', 'Taylor'), ('61', 'Trempealeau'), ('62', 'Vernon'), ('63', 'Vilas'), ('64', 'Walworth'), ('65', 'Washburn'), ('67', 'Waukesha'), ('68', 'Waupaca'), ('68', 'Waushara'), ('70', 'Winnebago'), ('71', 'Wood'), ('72', 'Menominee'), ('84', 'Menominee Tribe'), ('85', 'Red Cliff Tribe'), ('86', 'Stockbridge Munsee'), ('87', 'Forrest County Potawatomi'), ('88', 'Lac du Flambeau'), ('89', 'Bad River Tribe'), ('91', 'Sokaogon Chippewa'), ('92', 'Oneida Tribe'), ('93', 'Ho Chunk Nation'), ('94', 'Lac Courte Oreilles Tribe'), ('95', 'St Croix Tribe'), ('0', 'Unknown')], max_length=2)),
                ('brief_services', models.BooleanField(default=False)),
                ('referral_source', models.CharField(choices=[('1', 'Self'), ('2', 'Family, friend, or guardian'), ('3', 'AODA program/provider (includes AA, Al-Anon)'), ('4', 'Inpatient hospital or residential facility'), ('5', 'School, college'), ('6', 'IDP - Court'), ('7', 'IDP - Division of Motor Vehicle (DMV)'), ('8', 'Corrections, probation, parole'), ('9', 'Other court, criminal or juvenile justice system'), ('10', 'Employer, Employee Assistance Program (EAP)'), ('11', 'County social services'), ('12', 'Child Protective Services agency'), ('13', 'IV drug outreach worker'), ('14', 'Other'), ('15', 'Drug court'), ('16', 'OWI court - monitors the multiple OWI offender'), ('17', 'Screening Brief Intervention Referral Treatment (SBIRT)'), ('18', 'Mental health program/provider'), ('19', 'Hospital emergency room'), ('20', 'Primary care physician or other health careprogram/provider'), ('21', 'Law enforcement, police'), ('22', 'Mental health court'), ('23', 'Homeless outreach worker'), ('99', 'Unknown')], max_length=2)),
                ('chars', models.CharField(blank=True, choices=[('2', 'Mental illness (excluding SPMI)'), ('3', 'Serious and persistent mental illness (SPMI)'), ('4', 'Alcohol client'), ('5', 'Drug client'), ('7', 'Blind / visually impaired'), ('8', 'Hard of hearing'), ('9', 'Physical disability / mobility impaired'), ('10', 'Severe alcohol or other drug client'), ('12', 'Alcohol and other drug client'), ('14', 'Family member of mental health client'), ('16', 'Family member of alcohol and other drug client'), ('17', 'Intoxicated driver'), ('18', "Alzheimer's disease / related dementia"), ('19', 'Developmental disability - brain trauma'), ('23', 'Developmental disability - cerebral palsy'), ('25', 'Developmental disability – autism spectrum'), ('26', 'Developmental disability – intellectual disability'), ('27', 'Developmental disability - epilepsy'), ('28', 'Developmental disability - other or unknown'), ('29', 'Family member of a client with a developmental disability'), ('32', 'Blind/Deaf'), ('33', 'Corrections / criminal justice client (adult only)'), ('36', 'Other disability'), ('37', 'Frail medical condition'), ('38', 'Criminal justice system involvement (alleged or adjudicated)'), ('39', 'Gambling client'), ('43', 'Migrant'), ('44', 'Refugee'), ('45', 'Cuban / Haitian entrant'), ('50', 'Regular caregiver of dependent person'), ('55', 'Frail elderly'), ('57', 'Abused / neglected elder'), ('59', 'Unmarried parent'), ('61', 'CHIPS - abuse and neglect'), ('62', 'CHIPS - abuse'), ('63', 'CHIPS - neglect'), ('64', 'Family member of abused / neglected child'), ('66', 'Delinquent'), ('68', 'CHIPS - other'), ('69', 'JIPS - status offender'), ('70', 'Family member of status offender'), ('71', 'Victim of domestic abuse'), ('72', 'Victim of abuse or neglect (alleged or adjudicated)'), ('73', 'Family member of delinquent'), ('74', 'Family member of CHIPS - other'), ('79', 'Deaf'), ('80', 'Homeless'), ('84', 'Repeated school truancy'), ('86', 'Severe emotional disturbance - child / adolescent'), ('90', 'Special study code (to be defined as need arises)'), ('91', 'Hurricane Katrina evacuee'), ('92', 'Hurricane Rita evacuee'), ('99', 'None of the above (codependent client only)')], max_length=2, null=True, verbose_name='Characteristics')),
                ('education', models.CharField(choices=[('1', '1st grade'), ('2', '2nd grade'), ('3', '3rd grade'), ('4', '4th grade'), ('5', '5th grade'), ('6', '6th grade'), ('7', '7th grade'), ('8', '8th grade'), ('9', '9th grade'), ('10', '10th grade'), ('11', '11th grade'), ('12', 'High school diploma or GED'), ('14', 'Some college or vocational / technical school; assoc. degree or voc. tech. degree'), ('16', 'Bachelor’s degree'), ('18', "Advanced degree (Master's, Ph.D.)"), ('99', 'Unknown')], max_length=2, verbose_name='Eduation at Admission')),
                ('group_attend', models.CharField(choices=[('1', '16 or more times in the past 30 days'), ('2', '8-15 times in the past 30 days'), ('3', '4-7 times in the past 30 days'), ('4', '1-3 times in the past 30 days'), ('5', 'No attendance in the past 30 days'), ('9', 'Unknown')], max_length=1, verbose_name='Support Group Attendance 30 Days Prior to Admission')),
                ('group_attend_end', models.CharField(choices=[('1', '16 or more times in the past 30 days'), ('2', '8-15 times in the past 30 days'), ('3', '4-7 times in the past 30 days'), ('4', '1-3 times in the past 30 days'), ('5', 'No attendance in the past 30 days'), ('9', 'Unknown')], max_length=1, verbose_name='Support Group Closing Status')),
                ('employ_stat', models.CharField(choices=[('1', 'Full-time competitive (35 or more hrs/wk)'), ('2', 'Part-time competitive (less than 35 hrs/wk)'), ('3', 'Unemployed (looking for work past 30 days)'), ('4', 'Do Not Use, this code has been retired'), ('5', 'Not in the labor force - homemaker'), ('6', 'Not in the labor force - student'), ('7', 'Not in the labor force - retired'), ('8', 'Not in the labor force - disabled'), ('9', 'Not in the labor force - jail, prison or institution'), ('10', 'Not in the labor force - sheltered employment'), ('11', 'Not in the labor force - other reason'), ('12', 'Supported competitive employment'), ('98', 'Not applicable - Children 15 and under'), ('99', 'Unknown')], max_length=2, verbose_name='Employment Status at Admission')),
                ('employ_stat_end', models.CharField(choices=[('1', 'Full-time competitive (35 or more hrs/wk)'), ('2', 'Part-time competitive (less than 35 hrs/wk)'), ('3', 'Unemployed (looking for work past 30 days)'), ('4', 'Do Not Use, this code has been retired'), ('5', 'Not in the labor force - homemaker'), ('6', 'Not in the labor force - student'), ('7', 'Not in the labor force - retired'), ('8', 'Not in the labor force - disabled'), ('9', 'Not in the labor force - jail, prison or institution'), ('10', 'Not in the labor force - sheltered employment'), ('11', 'Not in the labor force - other reason'), ('12', 'Supported competitive employment'), ('98', 'Not applicable - Children 15 and under'), ('99', 'Unknown')], max_length=2, verbose_name='Employment Status at Closing Status')),
                ('living_stat', models.CharField(choices=[('1', 'Street, shelter, no fixed address, homeless'), ('2', 'Private residence w/out (ADULTS ONLY)'), ('3', 'Supported residence (ADULTS ONLY)'), ('4', 'Supervised licensed residential facility'), ('5', 'Institutional setting, hospital, nursing home'), ('6', 'Jail or correctional facility'), ('7', 'Living with parents (UNDER AGE 18 ONLY)'), ('8', 'Living with relatives, friends (UNDER AGE 18 ONLY)'), ('9', 'Foster home'), ('10', 'Crisis stabilization home/center'), ('11', 'Other living arrangement'), ('99', 'Unknown')], max_length=2, verbose_name='Living Arrangement at Admission')),
                ('living_stat_end', models.CharField(choices=[('1', 'Street, shelter, no fixed address, homeless'), ('2', 'Private residence w/out (ADULTS ONLY)'), ('3', 'Supported residence (ADULTS ONLY)'), ('4', 'Supervised licensed residential facility'), ('5', 'Institutional setting, hospital, nursing home'), ('6', 'Jail or correctional facility'), ('7', 'Living with parents (UNDER AGE 18 ONLY)'), ('8', 'Living with relatives, friends (UNDER AGE 18 ONLY)'), ('9', 'Foster home'), ('10', 'Crisis stabilization home/center'), ('11', 'Other living arrangement'), ('99', 'Unknown')], max_length=2, verbose_name='Living Arrangement Closing Status')),
                ('use_freq', models.CharField(choices=[('1', 'No use in the past month'), ('2', '1-3 days in the past month (less often than once a week)'), ('3', '1-2 days per week'), ('4', '3-6 days per week'), ('5', 'Daily'), ('9', 'Unknown')], max_length=1, verbose_name='Use Frequency')),
                ('use_freq_end', models.CharField(choices=[('1', 'No use in the past month'), ('2', '1-3 days in the past month (less often than once a week)'), ('3', '1-2 days per week'), ('4', '3-6 days per week'), ('5', 'Daily'), ('9', 'Unknown')], max_length=1, verbose_name='Use Frequency at Closing')),
                ('route_admin', models.CharField(choices=[('1', 'Oral (by mouth swallowing)'), ('2', 'Smoking (inhale by burning / heating substance)'), ('3', 'Inhalation (inhale or snort through the nose or mouth without burning the substance)'), ('4', 'Injection (IV or intramuscular or skin popping)'), ('5', 'Other'), ('9', 'Unknown')], max_length=1, verbose_name='Usual Route of Administration')),
                ('substance_problem', models.CharField(choices=[('1', 'None'), ('2', 'Alcohol'), ('3', 'Cocaine / crack'), ('4', 'Marijuana / hashish / cannabis / THC'), ('5', 'Heroin'), ('6', 'Nonprescription methadone'), ('7', 'Dilaudid / hydromorphone'), ('8', 'Other opiates and synthetics (codeine, morphine, oxycodone, demerol, opium, fentanyl, oxymorphone, etc.)'), ('9', 'PCP (phencyclidine)'), ('10', 'LSD'), ('11', 'Other hallucinogens (MDA, MDMA-ecstacy, peyote, mescaline, psilocybin, psilocin, STP, ketamine)'), ('12', 'Methamphetamine / ice; methcathinone / cat'), ('13', 'Other amphetamines (benzedrine, speed, dexedrine, methedrine, ritalin, preludin, and any other amines and related drugs)'), ('14', 'Other stimulants (phentermine, benzphetamine, mazindol, phendimetrazine, pemoline, chlortermine, etc.)'), ('15', 'Benzodiazepines (diazepam, flurazepam, chlordiazepoxide, clorazepate, lorazepam, alprazolam, oxazepam, temazepam, triazolam, clonazepam, halazepam, Rohypnol, etc.)'), ('16', 'Other tranquilizers (Meprobamate, Equanil, Miltown)'), ('17', 'Barbiturates (phenobarbital, Seconal, Nembutal, amobarbital, etc.)'), ('18', 'Other nonbarbiturate sedatives or hypnotics (methaqualone, Quaalude, glutethimide, chloral hydrate, ethchlorvynol, Placidyl, GHB, etc.)'), ('19', 'Inhalants (ether, glue, aerosols, solvents, gases, chloroform, nitrous oxide)'), ('20', 'Over-the-counter diet, alert, sleep aids, cough syrup'), ('21', 'Other')], max_length=2)),
                ('substance_problem_end', models.CharField(blank=True, choices=[('1', 'None'), ('2', 'Alcohol'), ('3', 'Cocaine / crack'), ('4', 'Marijuana / hashish / cannabis / THC'), ('5', 'Heroin'), ('6', 'Nonprescription methadone'), ('7', 'Dilaudid / hydromorphone'), ('8', 'Other opiates and synthetics (codeine, morphine, oxycodone, demerol, opium, fentanyl, oxymorphone, etc.)'), ('9', 'PCP (phencyclidine)'), ('10', 'LSD'), ('11', 'Other hallucinogens (MDA, MDMA-ecstacy, peyote, mescaline, psilocybin, psilocin, STP, ketamine)'), ('12', 'Methamphetamine / ice; methcathinone / cat'), ('13', 'Other amphetamines (benzedrine, speed, dexedrine, methedrine, ritalin, preludin, and any other amines and related drugs)'), ('14', 'Other stimulants (phentermine, benzphetamine, mazindol, phendimetrazine, pemoline, chlortermine, etc.)'), ('15', 'Benzodiazepines (diazepam, flurazepam, chlordiazepoxide, clorazepate, lorazepam, alprazolam, oxazepam, temazepam, triazolam, clonazepam, halazepam, Rohypnol, etc.)'), ('16', 'Other tranquilizers (Meprobamate, Equanil, Miltown)'), ('17', 'Barbiturates (phenobarbital, Seconal, Nembutal, amobarbital, etc.)'), ('18', 'Other nonbarbiturate sedatives or hypnotics (methaqualone, Quaalude, glutethimide, chloral hydrate, ethchlorvynol, Placidyl, GHB, etc.)'), ('19', 'Inhalants (ether, glue, aerosols, solvents, gases, chloroform, nitrous oxide)'), ('20', 'Over-the-counter diet, alert, sleep aids, cough syrup'), ('21', 'Other')], max_length=2, null=True, verbose_name='Substance Problem at End of Episode')),
                ('target_group', models.CharField(blank=True, choices=[('4', 'Alcohol abuse'), ('5', 'Drug abuse'), ('6', 'OMTC Participant'), ('7', 'SOR Grant Participant'), ('8', 'MAT in Jails Participant'), ('17', 'Intoxicated driver'), ('18', 'Alcohol and other drug abuse'), ('74', 'Family member / other of AODA client')], max_length=2, null=True)),
                ('spc_end_raeson', models.CharField(blank=True, choices=[('1', 'Completed service - major improvement'), ('2', 'Completed service - moderate improvement'), ('3', 'Completed service - no positive change'), ('4', 'Referred - to another nonalcohol / drug agency, program or service'), ('5', 'Behavioral termination - staff / program decision to terminate due to rule violation'), ('6', 'Withdrew - against staff advice'), ('7', 'Funding / authorization expired'), ('8', 'Incarcerated'), ('9', 'Death'), ('14', 'Referral to another AODA agency or program'), ('15', 'Transfer to another AODA service within an agency or program'), ('16', 'Funding / authorization expired, same service reopened'), ('19', 'Service is continuing')], max_length=2, null=True)),
                ('arrest_end', models.IntegerField(default=0, verbose_name='Number of arrests 30 days prior to discharge, or since admission if less than 30 days.')),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='client.Client')),
            ],
        ),
    ]
