"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 2.2.17 on 2021-03-04 16:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0069_auto_20210304_0958'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='emrformtype',
            name='is_clinic',
        ),
        migrations.RemoveField(
            model_name='emrformtype',
            name='is_clts',
        ),
        migrations.RemoveField(
            model_name='emrformtype',
            name='is_crisis',
        ),
        migrations.RemoveField(
            model_name='emrformtype',
            name='is_cst',
        ),
        migrations.RemoveField(
            model_name='emrformtype',
            name='is_randi',
        ),
        migrations.RemoveField(
            model_name='emrformtype',
            name='is_tcm',
        ),
        migrations.AddField(
            model_name='emrformtype',
            name='type_clinic',
            field=models.BooleanField(default=False, verbose_name='Clinic'),
        ),
        migrations.AddField(
            model_name='emrformtype',
            name='type_clts',
            field=models.BooleanField(default=False, verbose_name='CLTS'),
        ),
        migrations.AddField(
            model_name='emrformtype',
            name='type_crisis',
            field=models.BooleanField(default=False, verbose_name='Crisis'),
        ),
        migrations.AddField(
            model_name='emrformtype',
            name='type_cst',
            field=models.BooleanField(default=False, verbose_name='CST'),
        ),
        migrations.AddField(
            model_name='emrformtype',
            name='type_randi',
            field=models.BooleanField(default=False, verbose_name='R & I'),
        ),
        migrations.AddField(
            model_name='emrformtype',
            name='type_tcm',
            field=models.BooleanField(default=False, verbose_name='TCM'),
        ),
    ]
