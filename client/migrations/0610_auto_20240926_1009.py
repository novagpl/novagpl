"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.20 on 2024-09-26 15:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0609_alter_emrtcmppsmhform_commit_stat'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emrtcmppsmhform',
            name='client_char',
            field=models.CharField(blank=True, choices=[('2', '02 - Mental illness (excluding SPMI)'), ('3', '03 - Serious and persistent mental illness (SPMI)'), ('4', '04 - Alcohol client'), ('5', '05 - Drug client'), ('7', '07 - Blind / visually impaired'), ('8', '08 - Hard of hearing'), ('9', '09 - Physical disability / mobility impaired'), ('10', '10 - Severe alcohol or other drug client'), ('12', '12 - Alcohol and other drug client'), ('14', '14 - Family member of mental health client'), ('16', '16 - Family member of alcohol and other drug client'), ('17', '17 - Intoxicated driver'), ('18', "18 - Alzheimer's disease / related dementia"), ('19', '19 - Developmental disability - brain trauma'), ('23', '23 - Developmental disability - cerebral palsy'), ('25', '25 - Developmental disability – autism spectrum'), ('26', '26 - Developmental disability – intellectual disability'), ('27', '27 - Developmental disability - epilepsy'), ('28', '28 - Developmental disability - other or unknown'), ('29', '29 - Family member of a client with a developmental disability'), ('32', '32 - Blind/Deaf'), ('33', '33 - Corrections / criminal justice client (adult only)'), ('36', '36 - Other disability'), ('37', '37 - Frail medical condition'), ('38', '38 - Criminal justice system involvement (alleged or adjudicated)'), ('39', '39 - Gambling client'), ('43', '43 - Migrant'), ('44', '44 - Refugee'), ('45', '45 - Cuban / Haitian entrant'), ('50', '50 - Regular caregiver of dependent person'), ('55', '55 - Frail elderly'), ('57', '57 - Abused / neglected elder'), ('59', '59 - Unmarried parent'), ('61', '61 - CHIPS - abuse and neglect'), ('62', '62 - CHIPS - abuse'), ('63', '63 - CHIPS - neglect'), ('64', '64 - Family member of abused / neglected child'), ('66', '66 - Delinquent'), ('68', '68 - CHIPS - other'), ('69', '69 - JIPS - status offender'), ('70', '70 - Family member of status offender'), ('71', '71 - Victim of domestic abuse'), ('72', '72 - Victim of abuse or neglect (alleged or adjudicated)'), ('73', '73 - Family member of delinquent'), ('74', '74 - Family member of CHIPS - other'), ('79', '79 - Deaf'), ('80', '80 - Homeless'), ('84', '84 - Repeated school truancy'), ('86', '86 - Severe emotional disturbance - child / adolescent'), ('90', '90 - Special study code (to be defined as need arises)'), ('91', '91 - Hurricane Katrina evacuee'), ('92', '92 - Hurricane Rita evacuee'), ('99', '99 - None of the above')], max_length=2, null=True, verbose_name='Client characteristic'),
        ),
        migrations.AlterField(
            model_name='emrtcmppsmhform',
            name='presenting_problem',
            field=models.CharField(blank=True, choices=[('1', '01 - Marital / family problem'), ('2', '02 - Social / interpersonal (other than family problem)'), ('3', '03 - Problems coping with daily roles and activities (including job, school, housework, daily grooming, financial management, etc.)'), ('4', '04 - Medical / somatic'), ('5', '05 - Depressed mood and / or anxious'), ('6', '06 - Attempt, threat, or danger of suicide'), ('7', '07 - Alcohol'), ('8', '08 - Drugs'), ('9', '09 - Involvement with criminal justice system'), ('10', '10 - Eating disorder'), ('11', '11 - Disturbed thoughts'), ('12', '12 - Abuse / assault / rape victim'), ('13', '13 - Runaway behavior'), ('14', '14 - Emergency detention'), ('99', '99 - Unknown')], max_length=2, null=True, verbose_name='Presenting problem'),
        ),
    ]
