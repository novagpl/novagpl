"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.19 on 2023-06-09 17:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0359_alter_emrepisode_episode_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emrform',
            name='billing_code',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='emrform',
            name='contact_type',
            field=models.CharField(blank=True, choices=[('4', 'Collateral'), ('2', 'Direct'), ('1', 'Face to face'), ('3', 'Home visit'), ('5', 'Record keeping'), ('0', 'Other/Unknown')], max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='emrform',
            name='date',
            field=models.DateField(blank=True, null=True, verbose_name='Service date'),
        ),
        migrations.AlterField(
            model_name='emrform',
            name='note',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='emrform',
            name='place_of_service',
            field=models.CharField(blank=True, choices=[('01', 'Pharmacy'), ('02', 'Telehealth'), ('03', 'School'), ('04', 'Homeless shelter'), ('05', 'Indian health service free-standing facility'), ('06', 'Indian health service provider-based facility'), ('07', 'Tribal 638 free-standing facility'), ('08', 'Tribal 638 provider-based facility'), ('09', 'Prison/correctional facility'), ('11', 'Office'), ('12', 'Home'), ('13', 'Assisted living facility'), ('14', 'Group home'), ('15', 'Mobile unit'), ('19', 'Off campus - outpatient hospital'), ('20', 'Urgent care facility'), ('21', 'Inpatient hospital'), ('22', 'On campus - outpatient hospital'), ('23', 'Emergency room - hospital'), ('25', 'Birthing center'), ('26', 'Military treatment facility'), ('31', 'Skilled nursing facility'), ('32', 'Nursing facility'), ('33', 'Custodial care facility'), ('34', 'Hospice'), ('49', 'Independent clinic'), ('50', 'Federally qualified health center'), ('51', 'Inpatient psychiatric facility (below age 21 or age 65 and older)'), ('54', 'Intermediate care facility/individual with intellectual disabilities'), ('56', 'Psychiatric residential treatment center'), ('57', 'Non-residential substance abuse treatment facility'), ('60', 'Mass immunization center'), ('61', 'Comprehensive inpatient rehabilitation facility'), ('71', 'Public health clinic'), ('72', 'Rural health clinic'), ('99', 'Other place of service')], max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='emrform',
            name='service_minutes',
            field=models.IntegerField(blank=True, default=0, null=True),
        ),
    ]
