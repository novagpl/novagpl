"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 2.2.4 on 2019-10-27 22:02

from django.db import migrations, models
import multiselectfield.db.fields


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0026_casenote_pps_units'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='ppsaodaepi',
            name='spc_end_reason',
        ),
        migrations.AddField(
            model_name='casenote',
            name='pps_aoda_spc_end_reason',
            field=models.CharField(blank=True, choices=[('1', 'Completed service - major improvement'), ('2', 'Completed service - moderate improvement'), ('3', 'Completed service - no positive change'), ('4', 'Referred - to another nonalcohol / drug agency, program or service'), ('5', 'Behavioral termination - staff / program decision to terminate due to rule violation'), ('6', 'Withdrew - against staff advice'), ('7', 'Funding / authorization expired'), ('8', 'Incarcerated'), ('9', 'Death'), ('14', 'Referral to another AODA agency or program'), ('15', 'Transfer to another AODA service within an agency or program'), ('16', 'Funding / authorization expired, same service reopened'), ('19', 'Service is continuing')], max_length=2, null=True),
        ),
        migrations.AddField(
            model_name='casenote',
            name='pps_mh_spc_end_reason',
            field=models.CharField(blank=True, choices=[('1', 'Completed service - major improvement'), ('2', 'Completed service - moderate improvement'), ('3', 'Completed service - no change'), ('4', 'Transferred to other community resource'), ('5', 'Administratively discontinued'), ('6', 'Referred'), ('7', 'Withdrew against staff advice'), ('8', 'Funding / authorization expired'), ('9', 'Incarcerated'), ('10', 'Entered nursing home or institutional care'), ('11', 'No probable cause'), ('12', 'Death'), ('19', 'Service is continuing')], max_length=2, null=True),
        ),
        migrations.AddField(
            model_name='ppsmhepi',
            name='arrest_30days',
            field=models.IntegerField(default=0, verbose_name='Arrests in the past 30 days'),
        ),
        migrations.AddField(
            model_name='ppsmhepi',
            name='arrest_6months',
            field=models.IntegerField(default=0, verbose_name='Arrests in the past 6 months'),
        ),
        migrations.AddField(
            model_name='ppsmhepi',
            name='brc_target_pop',
            field=models.CharField(choices=[('H', 'Ongoing, high intensity, comprehensive services'), ('L', 'Ongoing, low intensity services'), ('S', 'Short-term situational service')], default='1', max_length=1, verbose_name='BRC Target Population'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ppsmhepi',
            name='chars',
            field=models.CharField(choices=[('2', 'Mental illness (excluding SPMI)'), ('3', 'Serious and persistent mental illness (SPMI)'), ('4', 'Alcohol client'), ('5', 'Drug client'), ('7', 'Blind / visually impaired'), ('8', 'Hard of hearing'), ('9', 'Physical disability / mobility impaired'), ('10', 'Severe alcohol or other drug client'), ('12', 'Alcohol and other drug client'), ('14', 'Family member of mental health client'), ('16', 'Family member of alcohol and other drug client'), ('17', 'Intoxicated driver'), ('18', "Alzheimer's disease / related dementia"), ('19', 'Developmental disability - brain trauma'), ('23', 'Developmental disability - cerebral palsy'), ('25', 'Developmental disability – autism spectrum'), ('26', 'Developmental disability – intellectual disability'), ('27', 'Developmental disability - epilepsy'), ('28', 'Developmental disability - other or unknown'), ('29', 'Family member of a client with a developmental disability'), ('32', 'Blind/Deaf'), ('33', 'Corrections / criminal justice client (adult only)'), ('36', 'Other disability'), ('37', 'Frail medical condition'), ('38', 'Criminal justice system involvement (alleged or adjudicated)'), ('39', 'Gambling client'), ('43', 'Migrant'), ('44', 'Refugee'), ('45', 'Cuban / Haitian entrant'), ('50', 'Regular caregiver of dependent person'), ('55', 'Frail elderly'), ('57', 'Abused / neglected elder'), ('59', 'Unmarried parent'), ('61', 'CHIPS - abuse and neglect'), ('62', 'CHIPS - abuse'), ('63', 'CHIPS - neglect'), ('64', 'Family member of abused / neglected child'), ('66', 'Delinquent'), ('68', 'CHIPS - other'), ('69', 'JIPS - status offender'), ('70', 'Family member of status offender'), ('71', 'Victim of domestic abuse'), ('72', 'Victim of abuse or neglect (alleged or adjudicated)'), ('73', 'Family member of delinquent'), ('74', 'Family member of CHIPS - other'), ('79', 'Deaf'), ('80', 'Homeless'), ('84', 'Repeated school truancy'), ('86', 'Severe emotional disturbance - child / adolescent'), ('90', 'Special study code (to be defined as need arises)'), ('91', 'Hurricane Katrina evacuee'), ('92', 'Hurricane Rita evacuee'), ('99', 'None of the above (codependent client only)')], default='1', max_length=2, verbose_name='Characteristics'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ppsmhepi',
            name='criminal_interactions',
            field=multiselectfield.db.fields.MultiSelectField(choices=[('1', 'None'), ('2', 'On probation'), ('3', 'Arrest(s)'), ('4', 'Jailed / imprisoned (includes Huber)'), ('5', 'On parole'), ('6', 'Juvenile justice system contact'), ('9', 'Unknown')], default='1', max_length=8),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ppsmhepi',
            name='daily_activity',
            field=models.CharField(choices=[('1', 'No educational, social or planned activity'), ('2', 'Part-time educational activity'), ('3', 'Full-time educational activity'), ('4', 'Meaningful social activity'), ('5', 'Volunteer or planned formal activities'), ('6', 'Other activities'), ('9', 'Unknown')], default='1', max_length=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ppsmhepi',
            name='employ_stat',
            field=models.CharField(choices=[('1', 'Full-time competitive (35 or more hrs/wk)'), ('2', 'Part-time competitive (less than 35 hrs/wk)'), ('3', 'Unemployed (looking for work past 30 days)'), ('4', 'Do Not Use, this code has been retired'), ('5', 'Not in the labor force - homemaker'), ('6', 'Not in the labor force - student'), ('7', 'Not in the labor force - retired'), ('8', 'Not in the labor force - disabled'), ('9', 'Not in the labor force - jail, prison or institution'), ('10', 'Not in the labor force - sheltered employment'), ('11', 'Not in the labor force - other reason'), ('12', 'Supported competitive employment'), ('98', 'Not applicable - Children 15 and under'), ('99', 'Unknown')], default='1', max_length=2, verbose_name='Employment Status'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ppsmhepi',
            name='epi_end_reason',
            field=models.CharField(blank=True, choices=[('1', 'Completed service - major improvement'), ('2', 'Completed service - moderate improvement'), ('3', 'Completed service - no change'), ('4', 'Transferred to other community resource'), ('5', 'Administratively discontinued service'), ('6', 'Referred'), ('7', 'Withdrew against staff advice'), ('8', 'Funding / authorization expired'), ('9', 'Incarcerated'), ('10', 'Entered nursing home or institutional care'), ('11', 'No probable cause'), ('12', 'Death')], max_length=2, null=True, verbose_name='Episode End Reason'),
        ),
        migrations.AddField(
            model_name='ppsmhepi',
            name='health_status',
            field=models.CharField(choices=[('1', 'No health condition'), ('2', 'Stable / capable'), ('3', 'Stable / incapable'), ('4', 'Unstable / capable'), ('5', 'Unstable / incapable'), ('6', 'New symptoms / capable'), ('7', 'New symptoms / incapable'), ('9', 'Unknown')], default='1', max_length=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ppsmhepi',
            name='legal_stat',
            field=models.CharField(choices=[('1', 'Voluntary'), ('2', 'Voluntary with settlement agreement'), ('3', 'Involuntary civil - Chapter 51'), ('4', 'Involuntary civil - Chapter 55'), ('5', 'Involuntary criminal'), ('6', 'Guardianship'), ('9', 'Unknown')], default='1', max_length=2, verbose_name='Legal/Commitment Status'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ppsmhepi',
            name='living_stat',
            field=models.CharField(choices=[('1', 'Street, shelter, no fixed address, homeless'), ('2', 'Private residence w/out (ADULTS ONLY)'), ('3', 'Supported residence (ADULTS ONLY)'), ('4', 'Supervised licensed residential facility'), ('5', 'Institutional setting, hospital, nursing home'), ('6', 'Jail or correctional facility'), ('7', 'Living with parents (UNDER AGE 18 ONLY)'), ('8', 'Living with relatives, friends (UNDER AGE 18 ONLY)'), ('9', 'Foster home'), ('10', 'Crisis stabilization home/center'), ('11', 'Other living arrangement'), ('99', 'Unknown')], default='1', max_length=2, verbose_name='Living Arrangement'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ppsmhepi',
            name='presenting_problem',
            field=models.CharField(choices=[('1', 'Marital / family problem'), ('2', 'Social / interpersonal (other than family problem)'), ('3', 'Problems coping with daily roles and activities (including job, school, housework, daily grooming, financial management, etc.)'), ('4', 'Medical / somatic'), ('5', 'Depressed mood and / or anxious'), ('6', 'Attempt, threat, or danger of suicide'), ('7', 'Alcohol'), ('8', 'Drugs'), ('9', 'Involvement with criminal justice system'), ('10', 'Eating disorder'), ('11', 'Disturbed thoughts'), ('12', 'Abuse / assault / rape victim'), ('13', 'Runaway behavior'), ('14', 'Emergency detention'), ('99', 'Unknown')], default='1', max_length=2, verbose_name='Presenting Problem'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ppsmhepi',
            name='psy_env_stress',
            field=models.CharField(choices=[('1', 'None'), ('2', 'Mild'), ('3', 'Moderate'), ('4', 'Severe'), ('5', 'Extreme'), ('6', 'Catastrophic'), ('0', 'Inadequate information')], default='1', max_length=2, verbose_name='Psychosocial and Environmental Stressors'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='ppsmhepi',
            name='referral_source',
            field=models.CharField(choices=[('1', 'Self'), ('2', 'Family, friend, or guardian'), ('3', 'AODA program/provider (includes AA, Al-Anon)'), ('4', 'Inpatient hospital or residential facility'), ('5', 'School, college'), ('6', 'IDP - Court'), ('7', 'IDP - Division of Motor Vehicle (DMV)'), ('8', 'Corrections, probation, parole'), ('9', 'Other court, criminal or juvenile justice system'), ('10', 'Employer, Employee Assistance Program (EAP)'), ('11', 'County social services'), ('12', 'Child Protective Services agency'), ('13', 'IV drug outreach worker'), ('14', 'Other'), ('15', 'Drug court'), ('16', 'OWI court - monitors the multiple OWI offender'), ('17', 'Screening Brief Intervention Referral Treatment (SBIRT)'), ('18', 'Mental health program/provider'), ('19', 'Hospital emergency room'), ('20', 'Primary care physician or other health careprogram/provider'), ('21', 'Law enforcement, police'), ('22', 'Mental health court'), ('23', 'Homeless outreach worker'), ('99', 'Unknown')], default='1', max_length=2),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='address',
            name='county',
            field=models.CharField(choices=[('1', 'Adams'), ('2', 'Ashland'), ('3', 'Barron'), ('4', 'Bayfield'), ('5', 'Brown'), ('6', 'Buffalo'), ('7', 'Burnett'), ('8', 'Calumet'), ('9', 'Chippewa'), ('10', 'Clark'), ('11', 'Columbia'), ('12', 'Crawford'), ('13', 'Dane'), ('14', 'Dodge'), ('15', 'Door'), ('16', 'Douglas'), ('17', 'Dunn'), ('18', 'Eau Claire'), ('19', 'Florence'), ('20', 'Fond du Lac'), ('21', 'Forest'), ('22', 'Grant'), ('23', 'Green'), ('24', 'Green Lake'), ('25', 'Iowa'), ('26', 'Iron'), ('27', 'Jackson'), ('28', 'Jefferson'), ('29', 'Juneau'), ('30', 'Kenosha'), ('31', 'Kewaunee'), ('32', 'La Crosse'), ('33', 'Lafayette'), ('34', 'Langlade'), ('35', 'Lincoln'), ('36', 'Manitowoc'), ('37', 'Marathon'), ('38', 'Marinette'), ('39', 'Marquette'), ('40', 'Milwaukee'), ('41', 'Monroe'), ('42', 'Oconto'), ('43', 'Oneida'), ('44', 'Outagamie'), ('45', 'Ozaukee'), ('46', 'Pepin'), ('47', 'Pierce'), ('48', 'Polk'), ('49', 'Portage'), ('50', 'Price'), ('51', 'Racine'), ('52', 'Richland'), ('53', 'Rock'), ('54', 'Rusk'), ('55', 'St Croix'), ('56', 'Sauk'), ('57', 'Sawyer'), ('58', 'Shawano'), ('59', 'Sheboygan'), ('60', 'Taylor'), ('61', 'Trempealeau'), ('62', 'Vernon'), ('63', 'Vilas'), ('64', 'Walworth'), ('65', 'Washburn'), ('67', 'Waukesha'), ('68', 'Waupaca'), ('68', 'Waushara'), ('70', 'Winnebago'), ('71', 'Wood'), ('72', 'Menominee'), ('84', 'Menominee Tribe'), ('85', 'Red Cliff Tribe'), ('86', 'Stockbridge Munsee'), ('87', 'Forrest County Potawatomi'), ('88', 'Lac du Flambeau'), ('89', 'Bad River Tribe'), ('91', 'Sokaogon Chippewa'), ('92', 'Oneida Tribe'), ('93', 'Ho Chunk Nation'), ('94', 'Lac Courte Oreilles Tribe'), ('95', 'St Croix Tribe'), ('0', 'Unknown')], default=0, max_length=2),
        ),
        migrations.AlterField(
            model_name='ppsaodaepi',
            name='county',
            field=models.CharField(choices=[('1', 'Adams'), ('2', 'Ashland'), ('3', 'Barron'), ('4', 'Bayfield'), ('5', 'Brown'), ('6', 'Buffalo'), ('7', 'Burnett'), ('8', 'Calumet'), ('9', 'Chippewa'), ('10', 'Clark'), ('11', 'Columbia'), ('12', 'Crawford'), ('13', 'Dane'), ('14', 'Dodge'), ('15', 'Door'), ('16', 'Douglas'), ('17', 'Dunn'), ('18', 'Eau Claire'), ('19', 'Florence'), ('20', 'Fond du Lac'), ('21', 'Forest'), ('22', 'Grant'), ('23', 'Green'), ('24', 'Green Lake'), ('25', 'Iowa'), ('26', 'Iron'), ('27', 'Jackson'), ('28', 'Jefferson'), ('29', 'Juneau'), ('30', 'Kenosha'), ('31', 'Kewaunee'), ('32', 'La Crosse'), ('33', 'Lafayette'), ('34', 'Langlade'), ('35', 'Lincoln'), ('36', 'Manitowoc'), ('37', 'Marathon'), ('38', 'Marinette'), ('39', 'Marquette'), ('40', 'Milwaukee'), ('41', 'Monroe'), ('42', 'Oconto'), ('43', 'Oneida'), ('44', 'Outagamie'), ('45', 'Ozaukee'), ('46', 'Pepin'), ('47', 'Pierce'), ('48', 'Polk'), ('49', 'Portage'), ('50', 'Price'), ('51', 'Racine'), ('52', 'Richland'), ('53', 'Rock'), ('54', 'Rusk'), ('55', 'St Croix'), ('56', 'Sauk'), ('57', 'Sawyer'), ('58', 'Shawano'), ('59', 'Sheboygan'), ('60', 'Taylor'), ('61', 'Trempealeau'), ('62', 'Vernon'), ('63', 'Vilas'), ('64', 'Walworth'), ('65', 'Washburn'), ('67', 'Waukesha'), ('68', 'Waupaca'), ('68', 'Waushara'), ('70', 'Winnebago'), ('71', 'Wood'), ('72', 'Menominee'), ('84', 'Menominee Tribe'), ('85', 'Red Cliff Tribe'), ('86', 'Stockbridge Munsee'), ('87', 'Forrest County Potawatomi'), ('88', 'Lac du Flambeau'), ('89', 'Bad River Tribe'), ('91', 'Sokaogon Chippewa'), ('92', 'Oneida Tribe'), ('93', 'Ho Chunk Nation'), ('94', 'Lac Courte Oreilles Tribe'), ('95', 'St Croix Tribe'), ('0', 'Unknown')], max_length=2),
        ),
        migrations.AlterField(
            model_name='ppsb3epi',
            name='county',
            field=models.CharField(choices=[('1', 'Adams'), ('2', 'Ashland'), ('3', 'Barron'), ('4', 'Bayfield'), ('5', 'Brown'), ('6', 'Buffalo'), ('7', 'Burnett'), ('8', 'Calumet'), ('9', 'Chippewa'), ('10', 'Clark'), ('11', 'Columbia'), ('12', 'Crawford'), ('13', 'Dane'), ('14', 'Dodge'), ('15', 'Door'), ('16', 'Douglas'), ('17', 'Dunn'), ('18', 'Eau Claire'), ('19', 'Florence'), ('20', 'Fond du Lac'), ('21', 'Forest'), ('22', 'Grant'), ('23', 'Green'), ('24', 'Green Lake'), ('25', 'Iowa'), ('26', 'Iron'), ('27', 'Jackson'), ('28', 'Jefferson'), ('29', 'Juneau'), ('30', 'Kenosha'), ('31', 'Kewaunee'), ('32', 'La Crosse'), ('33', 'Lafayette'), ('34', 'Langlade'), ('35', 'Lincoln'), ('36', 'Manitowoc'), ('37', 'Marathon'), ('38', 'Marinette'), ('39', 'Marquette'), ('40', 'Milwaukee'), ('41', 'Monroe'), ('42', 'Oconto'), ('43', 'Oneida'), ('44', 'Outagamie'), ('45', 'Ozaukee'), ('46', 'Pepin'), ('47', 'Pierce'), ('48', 'Polk'), ('49', 'Portage'), ('50', 'Price'), ('51', 'Racine'), ('52', 'Richland'), ('53', 'Rock'), ('54', 'Rusk'), ('55', 'St Croix'), ('56', 'Sauk'), ('57', 'Sawyer'), ('58', 'Shawano'), ('59', 'Sheboygan'), ('60', 'Taylor'), ('61', 'Trempealeau'), ('62', 'Vernon'), ('63', 'Vilas'), ('64', 'Walworth'), ('65', 'Washburn'), ('67', 'Waukesha'), ('68', 'Waupaca'), ('68', 'Waushara'), ('70', 'Winnebago'), ('71', 'Wood'), ('72', 'Menominee'), ('84', 'Menominee Tribe'), ('85', 'Red Cliff Tribe'), ('86', 'Stockbridge Munsee'), ('87', 'Forrest County Potawatomi'), ('88', 'Lac du Flambeau'), ('89', 'Bad River Tribe'), ('91', 'Sokaogon Chippewa'), ('92', 'Oneida Tribe'), ('93', 'Ho Chunk Nation'), ('94', 'Lac Courte Oreilles Tribe'), ('95', 'St Croix Tribe'), ('0', 'Unknown')], max_length=2),
        ),
        migrations.AlterField(
            model_name='ppscoreepi',
            name='county',
            field=models.CharField(choices=[('1', 'Adams'), ('2', 'Ashland'), ('3', 'Barron'), ('4', 'Bayfield'), ('5', 'Brown'), ('6', 'Buffalo'), ('7', 'Burnett'), ('8', 'Calumet'), ('9', 'Chippewa'), ('10', 'Clark'), ('11', 'Columbia'), ('12', 'Crawford'), ('13', 'Dane'), ('14', 'Dodge'), ('15', 'Door'), ('16', 'Douglas'), ('17', 'Dunn'), ('18', 'Eau Claire'), ('19', 'Florence'), ('20', 'Fond du Lac'), ('21', 'Forest'), ('22', 'Grant'), ('23', 'Green'), ('24', 'Green Lake'), ('25', 'Iowa'), ('26', 'Iron'), ('27', 'Jackson'), ('28', 'Jefferson'), ('29', 'Juneau'), ('30', 'Kenosha'), ('31', 'Kewaunee'), ('32', 'La Crosse'), ('33', 'Lafayette'), ('34', 'Langlade'), ('35', 'Lincoln'), ('36', 'Manitowoc'), ('37', 'Marathon'), ('38', 'Marinette'), ('39', 'Marquette'), ('40', 'Milwaukee'), ('41', 'Monroe'), ('42', 'Oconto'), ('43', 'Oneida'), ('44', 'Outagamie'), ('45', 'Ozaukee'), ('46', 'Pepin'), ('47', 'Pierce'), ('48', 'Polk'), ('49', 'Portage'), ('50', 'Price'), ('51', 'Racine'), ('52', 'Richland'), ('53', 'Rock'), ('54', 'Rusk'), ('55', 'St Croix'), ('56', 'Sauk'), ('57', 'Sawyer'), ('58', 'Shawano'), ('59', 'Sheboygan'), ('60', 'Taylor'), ('61', 'Trempealeau'), ('62', 'Vernon'), ('63', 'Vilas'), ('64', 'Walworth'), ('65', 'Washburn'), ('67', 'Waukesha'), ('68', 'Waupaca'), ('68', 'Waushara'), ('70', 'Winnebago'), ('71', 'Wood'), ('72', 'Menominee'), ('84', 'Menominee Tribe'), ('85', 'Red Cliff Tribe'), ('86', 'Stockbridge Munsee'), ('87', 'Forrest County Potawatomi'), ('88', 'Lac du Flambeau'), ('89', 'Bad River Tribe'), ('91', 'Sokaogon Chippewa'), ('92', 'Oneida Tribe'), ('93', 'Ho Chunk Nation'), ('94', 'Lac Courte Oreilles Tribe'), ('95', 'St Croix Tribe'), ('0', 'Unknown')], max_length=2),
        ),
        migrations.AlterField(
            model_name='ppsmhepi',
            name='county',
            field=models.CharField(choices=[('1', 'Adams'), ('2', 'Ashland'), ('3', 'Barron'), ('4', 'Bayfield'), ('5', 'Brown'), ('6', 'Buffalo'), ('7', 'Burnett'), ('8', 'Calumet'), ('9', 'Chippewa'), ('10', 'Clark'), ('11', 'Columbia'), ('12', 'Crawford'), ('13', 'Dane'), ('14', 'Dodge'), ('15', 'Door'), ('16', 'Douglas'), ('17', 'Dunn'), ('18', 'Eau Claire'), ('19', 'Florence'), ('20', 'Fond du Lac'), ('21', 'Forest'), ('22', 'Grant'), ('23', 'Green'), ('24', 'Green Lake'), ('25', 'Iowa'), ('26', 'Iron'), ('27', 'Jackson'), ('28', 'Jefferson'), ('29', 'Juneau'), ('30', 'Kenosha'), ('31', 'Kewaunee'), ('32', 'La Crosse'), ('33', 'Lafayette'), ('34', 'Langlade'), ('35', 'Lincoln'), ('36', 'Manitowoc'), ('37', 'Marathon'), ('38', 'Marinette'), ('39', 'Marquette'), ('40', 'Milwaukee'), ('41', 'Monroe'), ('42', 'Oconto'), ('43', 'Oneida'), ('44', 'Outagamie'), ('45', 'Ozaukee'), ('46', 'Pepin'), ('47', 'Pierce'), ('48', 'Polk'), ('49', 'Portage'), ('50', 'Price'), ('51', 'Racine'), ('52', 'Richland'), ('53', 'Rock'), ('54', 'Rusk'), ('55', 'St Croix'), ('56', 'Sauk'), ('57', 'Sawyer'), ('58', 'Shawano'), ('59', 'Sheboygan'), ('60', 'Taylor'), ('61', 'Trempealeau'), ('62', 'Vernon'), ('63', 'Vilas'), ('64', 'Walworth'), ('65', 'Washburn'), ('67', 'Waukesha'), ('68', 'Waupaca'), ('68', 'Waushara'), ('70', 'Winnebago'), ('71', 'Wood'), ('72', 'Menominee'), ('84', 'Menominee Tribe'), ('85', 'Red Cliff Tribe'), ('86', 'Stockbridge Munsee'), ('87', 'Forrest County Potawatomi'), ('88', 'Lac du Flambeau'), ('89', 'Bad River Tribe'), ('91', 'Sokaogon Chippewa'), ('92', 'Oneida Tribe'), ('93', 'Ho Chunk Nation'), ('94', 'Lac Courte Oreilles Tribe'), ('95', 'St Croix Tribe'), ('0', 'Unknown')], max_length=2),
        ),
    ]
