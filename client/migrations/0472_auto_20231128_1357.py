"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.21 on 2023-11-28 19:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0471_alter_emrjacksoncountyassesresponseform_presenting_problem'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emrjacksoncountyassesresponseform',
            name='commitment_status',
            field=models.CharField(blank=True, choices=[('1', '1 - Voluntary'), ('2', '2 - Voluntary with settlement agreement'), ('3', '3 - Involuntary civil - Chapter 51'), ('4', '4 - Involuntary civil - Chapter 55'), ('5', '5 - Involuntary criminal'), ('6', '6 - Guardianship'), ('9', '9 - Unknown')], max_length=255, null=True, verbose_name='Commitment status code'),
        ),
        migrations.AlterField(
            model_name='ppsmhepi',
            name='legal_stat',
            field=models.CharField(choices=[('1', '1 - Voluntary'), ('2', '2 - Voluntary with settlement agreement'), ('3', '3 - Involuntary civil - Chapter 51'), ('4', '4 - Involuntary civil - Chapter 55'), ('5', '5 - Involuntary criminal'), ('6', '6 - Guardianship'), ('9', '9 - Unknown')], max_length=2, verbose_name='Legal/Commitment Status'),
        ),
    ]
