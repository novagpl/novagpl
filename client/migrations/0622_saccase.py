"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.20 on 2024-10-11 19:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0621_sacepisode'),
    ]

    operations = [
        migrations.CreateModel(
            name='SacCase',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('sacid', models.IntegerField(unique=True)),
                ('repl_insert', models.DateTimeField(verbose_name='REPL insert')),
                ('repl_update', models.DateTimeField(verbose_name='REPL update')),
                ('start', models.DateField(null=True, verbose_name='Case start date')),
                ('end', models.DateField(null=True, verbose_name='Case end date')),
            ],
        ),
    ]
