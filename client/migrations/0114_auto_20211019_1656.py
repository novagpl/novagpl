"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.7 on 2021-10-19 21:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0113_auto_20211019_1517'),
    ]

    operations = [
        migrations.CreateModel(
            name='EmrCrisisFollowUpAndLinkageForm',
            fields=[
                ('emrform_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='client.emrform')),
                ('service_location', models.CharField(blank=True, max_length=255, null=True)),
                ('provide_evaulation', models.BooleanField(default=False, verbose_name='Provide evaluation, information, and referral to client or others involved')),
                ('provided_info', models.TextField(blank=True, null=True, verbose_name='Info type/provided to')),
                ('followup_needed', models.BooleanField(default=False, verbose_name='Determine whether any follow up contacts by program staff or linkages with other providers are necessary')),
                ('followup_info', models.TextField(blank=True, null=True, verbose_name='Responses needed')),
                ('fui_contact', models.BooleanField(default=False, verbose_name='Follow up contact to ensure that the intervention plans are carried out and meeting the client’s needs')),
                ('fui_client_phone', models.BooleanField(default=False, verbose_name='Client - phone')),
                ('fui_client_in_person', models.BooleanField(default=False, verbose_name='Client - in person')),
                ('fui_family_phone', models.BooleanField(default=False, verbose_name='Family/support/provider – phone')),
                ('fui_letter_to_address', models.BooleanField(default=False, verbose_name='Follow up letter to listed address')),
                ('fua_contact', models.BooleanField(default=False, verbose_name='Follow up contacts until the client has begun to receive assistance from ongoing service provider')),
                ('fua_client', models.BooleanField(default=False, verbose_name='Client - phone')),
                ('fua_client_in_person', models.BooleanField(default=False, verbose_name='Client - in person')),
                ('fua_family_phone', models.BooleanField(default=False, verbose_name='Family/support/provider – phone')),
                ('fua_letter_to_address', models.BooleanField(default=False, verbose_name='Follow up letter to listed address')),
                ('contact_made', models.BooleanField(default=False)),
                ('contact_made_details', models.CharField(blank=True, max_length=255, null=True, verbose_name='Contact made to')),
                ('info_provided', models.BooleanField(default=False, verbose_name='Info/Resources provided')),
                ('info_provided_details', models.CharField(blank=True, max_length=255, null=True, verbose_name='Info/Resources provided to')),
                ('referral', models.BooleanField(default=False)),
                ('referral_details', models.CharField(blank=True, max_length=255, null=True, verbose_name='Referral to')),
                ('other_referral', models.BooleanField(default=False)),
                ('other_referral_details', models.CharField(blank=True, max_length=255, null=True, verbose_name='Other referral to')),
                ('other', models.BooleanField(default=False)),
                ('other_summary', models.CharField(blank=True, max_length=255, null=True, verbose_name='Other summary')),
                ('outpatient_session', models.BooleanField(default=False, verbose_name='Outpatient session arranged')),
                ('outpatient_session_summary', models.CharField(blank=True, max_length=255, null=True, verbose_name='Outpatient summary')),
                ('closed', models.BooleanField(default=False)),
                ('closed_reason', models.CharField(blank=True, max_length=255, null=True, verbose_name='Close reason')),
                ('closed_date', models.DateField(blank=True, null=True, verbose_name='Close date')),
                ('closed_other', models.BooleanField(default=False, verbose_name='Other')),
                ('closed_other_summary', models.CharField(max_length=255, verbose_name='Other summary')),
                ('crisis_plan_on_file', models.BooleanField(default=False)),
                ('crisis_plan_created_updated', models.BooleanField(default=False, verbose_name='Crisis plan created/updated')),
                ('crisis_alert_created', models.BooleanField(default=False)),
                ('satisfaction_survey', models.BooleanField(default=False)),
                ('other_actions', models.BooleanField(default=False)),
                ('service_detail', models.TextField(blank=True, null=True, verbose_name='Service Detail (Including time of service, contact details)')),
            ],
            bases=('client.emrform',),
        ),
        migrations.RemoveField(
            model_name='emrcrisisalertform',
            name='ls1_details',
        ),
        migrations.RemoveField(
            model_name='emrcrisisalertform',
            name='ls2_details',
        ),
        migrations.AddField(
            model_name='emrcrisisalertform',
            name='ls1_summary',
            field=models.TextField(blank=True, null=True, verbose_name='Legal status summary'),
        ),
        migrations.AddField(
            model_name='emrcrisisalertform',
            name='ls2_summary',
            field=models.TextField(blank=True, null=True, verbose_name='Legal status summary'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='caution_other_summary',
            field=models.TextField(blank=True, null=True, verbose_name='Other known cautions'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_1_address',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Address'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_1_avil',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Availability'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_1_name',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_1_phone',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Phone'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_1_relation',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Agency/relation'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_2_avil',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Availability'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_2_name',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_2_phone',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Phone'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_2_relation',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Agency/relation'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_3_address',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Address'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_3_avil',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Availability'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_3_name',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_3_phone',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Phone'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_3_relation',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Agency/relation'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_4_address',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Address'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_4_avil',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Availability'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_4_name',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_4_phone',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Phone'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_4_relation',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Agency/relation'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_5_address',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Address'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_5_avil',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Availability'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_5_name',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Name'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_5_phone',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Phone'),
        ),
        migrations.AlterField(
            model_name='emrcrisisalertform',
            name='contact_5_relation',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Agency/relation'),
        ),
    ]
