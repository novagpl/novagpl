"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 2.2.4 on 2019-10-26 21:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0021_auto_20191026_1213'),
    ]

    operations = [
        migrations.CreateModel(
            name='PpsMhEpi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start', models.DateField()),
                ('end', models.DateField(blank=True, null=True)),
                ('county', models.CharField(choices=[('1', 'Adams'), ('2', 'Ashland'), ('3', 'Barron'), ('4', 'Bayfield'), ('5', 'Brown'), ('6', 'Buffalo'), ('7', 'Burnett'), ('8', 'Calumet'), ('9', 'Chippewa'), ('10', 'Clark'), ('11', 'Columbia'), ('12', 'Crawford'), ('13', 'Dane'), ('14', 'Dodge'), ('15', 'Door'), ('16', 'Douglas'), ('17', 'Dunn'), ('18', 'Eau Claire'), ('19', 'Florence'), ('20', 'Fond du Lac'), ('21', 'Forest'), ('22', 'Grant'), ('23', 'Green'), ('24', 'Green Lake'), ('25', 'Iowa'), ('26', 'Iron'), ('27', 'Jackson'), ('28', 'Jefferson'), ('29', 'Juneau'), ('30', 'Kenosha'), ('31', 'Kewaunee'), ('32', 'La Crosse'), ('33', 'Lafayette'), ('34', 'Langlade'), ('35', 'Lincoln'), ('36', 'Manitowoc'), ('37', 'Marathon County'), ('38', 'Marinette'), ('39', 'Marquette'), ('40', 'Milwaukee'), ('41', 'Monroe'), ('42', 'Oconto'), ('43', 'Oneida'), ('44', 'Outagamie'), ('45', 'Ozaukee'), ('46', 'Pepin'), ('47', 'Pierce'), ('48', 'Polk'), ('49', 'Portage'), ('50', 'Price'), ('51', 'Racine'), ('52', 'Richland'), ('53', 'Rock'), ('54', 'Rusk'), ('55', 'St Croix'), ('56', 'Sauk'), ('57', 'Sawyer'), ('58', 'Shawano'), ('59', 'Sheboygan'), ('60', 'Taylor'), ('61', 'Trempealeau'), ('62', 'Vernon'), ('63', 'Vilas'), ('64', 'Walworth'), ('65', 'Washburn'), ('67', 'Waukesha'), ('68', 'Waupaca'), ('68', 'Waushara'), ('70', 'Winnebago'), ('71', 'Wood'), ('72', 'Menominee'), ('84', 'Menominee Tribe'), ('85', 'Red Cliff Tribe'), ('86', 'Stockbridge Munsee'), ('87', 'Forrest County Potawatomi'), ('88', 'Lac du Flambeau'), ('89', 'Bad River Tribe'), ('91', 'Sokaogon Chippewa'), ('92', 'Oneida Tribe'), ('93', 'Ho Chunk Nation'), ('94', 'Lac Courte Oreilles Tribe'), ('95', 'St Croix Tribe'), ('0', 'Unknown')], max_length=2)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='client.Client')),
            ],
        ),
        migrations.CreateModel(
            name='PpsCoreEpi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start', models.DateField()),
                ('end', models.DateField(blank=True, null=True)),
                ('county', models.CharField(choices=[('1', 'Adams'), ('2', 'Ashland'), ('3', 'Barron'), ('4', 'Bayfield'), ('5', 'Brown'), ('6', 'Buffalo'), ('7', 'Burnett'), ('8', 'Calumet'), ('9', 'Chippewa'), ('10', 'Clark'), ('11', 'Columbia'), ('12', 'Crawford'), ('13', 'Dane'), ('14', 'Dodge'), ('15', 'Door'), ('16', 'Douglas'), ('17', 'Dunn'), ('18', 'Eau Claire'), ('19', 'Florence'), ('20', 'Fond du Lac'), ('21', 'Forest'), ('22', 'Grant'), ('23', 'Green'), ('24', 'Green Lake'), ('25', 'Iowa'), ('26', 'Iron'), ('27', 'Jackson'), ('28', 'Jefferson'), ('29', 'Juneau'), ('30', 'Kenosha'), ('31', 'Kewaunee'), ('32', 'La Crosse'), ('33', 'Lafayette'), ('34', 'Langlade'), ('35', 'Lincoln'), ('36', 'Manitowoc'), ('37', 'Marathon County'), ('38', 'Marinette'), ('39', 'Marquette'), ('40', 'Milwaukee'), ('41', 'Monroe'), ('42', 'Oconto'), ('43', 'Oneida'), ('44', 'Outagamie'), ('45', 'Ozaukee'), ('46', 'Pepin'), ('47', 'Pierce'), ('48', 'Polk'), ('49', 'Portage'), ('50', 'Price'), ('51', 'Racine'), ('52', 'Richland'), ('53', 'Rock'), ('54', 'Rusk'), ('55', 'St Croix'), ('56', 'Sauk'), ('57', 'Sawyer'), ('58', 'Shawano'), ('59', 'Sheboygan'), ('60', 'Taylor'), ('61', 'Trempealeau'), ('62', 'Vernon'), ('63', 'Vilas'), ('64', 'Walworth'), ('65', 'Washburn'), ('67', 'Waukesha'), ('68', 'Waupaca'), ('68', 'Waushara'), ('70', 'Winnebago'), ('71', 'Wood'), ('72', 'Menominee'), ('84', 'Menominee Tribe'), ('85', 'Red Cliff Tribe'), ('86', 'Stockbridge Munsee'), ('87', 'Forrest County Potawatomi'), ('88', 'Lac du Flambeau'), ('89', 'Bad River Tribe'), ('91', 'Sokaogon Chippewa'), ('92', 'Oneida Tribe'), ('93', 'Ho Chunk Nation'), ('94', 'Lac Courte Oreilles Tribe'), ('95', 'St Croix Tribe'), ('0', 'Unknown')], max_length=2)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='client.Client')),
            ],
        ),
        migrations.CreateModel(
            name='PpsB3Epi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start', models.DateField()),
                ('end', models.DateField(blank=True, null=True)),
                ('county', models.CharField(choices=[('1', 'Adams'), ('2', 'Ashland'), ('3', 'Barron'), ('4', 'Bayfield'), ('5', 'Brown'), ('6', 'Buffalo'), ('7', 'Burnett'), ('8', 'Calumet'), ('9', 'Chippewa'), ('10', 'Clark'), ('11', 'Columbia'), ('12', 'Crawford'), ('13', 'Dane'), ('14', 'Dodge'), ('15', 'Door'), ('16', 'Douglas'), ('17', 'Dunn'), ('18', 'Eau Claire'), ('19', 'Florence'), ('20', 'Fond du Lac'), ('21', 'Forest'), ('22', 'Grant'), ('23', 'Green'), ('24', 'Green Lake'), ('25', 'Iowa'), ('26', 'Iron'), ('27', 'Jackson'), ('28', 'Jefferson'), ('29', 'Juneau'), ('30', 'Kenosha'), ('31', 'Kewaunee'), ('32', 'La Crosse'), ('33', 'Lafayette'), ('34', 'Langlade'), ('35', 'Lincoln'), ('36', 'Manitowoc'), ('37', 'Marathon County'), ('38', 'Marinette'), ('39', 'Marquette'), ('40', 'Milwaukee'), ('41', 'Monroe'), ('42', 'Oconto'), ('43', 'Oneida'), ('44', 'Outagamie'), ('45', 'Ozaukee'), ('46', 'Pepin'), ('47', 'Pierce'), ('48', 'Polk'), ('49', 'Portage'), ('50', 'Price'), ('51', 'Racine'), ('52', 'Richland'), ('53', 'Rock'), ('54', 'Rusk'), ('55', 'St Croix'), ('56', 'Sauk'), ('57', 'Sawyer'), ('58', 'Shawano'), ('59', 'Sheboygan'), ('60', 'Taylor'), ('61', 'Trempealeau'), ('62', 'Vernon'), ('63', 'Vilas'), ('64', 'Walworth'), ('65', 'Washburn'), ('67', 'Waukesha'), ('68', 'Waupaca'), ('68', 'Waushara'), ('70', 'Winnebago'), ('71', 'Wood'), ('72', 'Menominee'), ('84', 'Menominee Tribe'), ('85', 'Red Cliff Tribe'), ('86', 'Stockbridge Munsee'), ('87', 'Forrest County Potawatomi'), ('88', 'Lac du Flambeau'), ('89', 'Bad River Tribe'), ('91', 'Sokaogon Chippewa'), ('92', 'Oneida Tribe'), ('93', 'Ho Chunk Nation'), ('94', 'Lac Courte Oreilles Tribe'), ('95', 'St Croix Tribe'), ('0', 'Unknown')], max_length=2)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='client.Client')),
            ],
        ),
    ]
