"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.10 on 2023-02-14 16:10

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('client', '0339_auto_20230214_0953'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='EmrClinicEpisode',
            new_name='EmrClinicPreadmitEpisode',
        ),
        migrations.RemoveField(
            model_name='emrdoctype',
            name='type_clinic',
        ),
        migrations.RemoveField(
            model_name='emrformtype',
            name='type_clinic',
        ),
        migrations.AddField(
            model_name='emrdoctype',
            name='type_clinic_preadmit',
            field=models.BooleanField(default=False, verbose_name='Clinic Pre-admit'),
        ),
        migrations.AddField(
            model_name='emrformtype',
            name='type_clinic_preadmit',
            field=models.BooleanField(default=False, verbose_name='Clinic Pre-admit'),
        ),
        migrations.AlterField(
            model_name='emrepisode',
            name='closing_reason',
            field=models.CharField(blank=True, choices=[('moved', 'Moved out of county'), ('declined', 'Declined services'), ('aged out', 'Aged out of services'), ('ltc', 'Transitioned to LTC'), ('county discharge', 'County prompted discharge'), ('success discharge', 'Successfully discharged'), ('referred to community services', 'Referred to community resources'), ('no engagement', 'No engagement'), ('out of county', 'Transferred to same service out of county'), ('refer clinic preadmit', 'Referred to Clinic Pre-admit'), ('refer ccs', 'Referred to CCS'), ('refer clts', 'Referred to CLTS'), ('admit_clts', 'Admitted to CLTS'), ('refer cst', 'Referred to CST'), ('refer crisis', 'Referred to Crisis'), ('aoda_cm', 'AODA Case Management'), ('not eligible', 'No longer eligible '), ('incarcerated', 'Incarcerated'), ('current enroll', 'Currently enrolled in another service/program that will f/u'), ('refer county', 'Referred to county of residence'), ('death', 'Client death'), ('other', 'Other')], max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='emrepisode',
            name='closing_reason2',
            field=models.CharField(blank=True, choices=[('moved', 'Moved out of county'), ('declined', 'Declined services'), ('aged out', 'Aged out of services'), ('ltc', 'Transitioned to LTC'), ('county discharge', 'County prompted discharge'), ('success discharge', 'Successfully discharged'), ('referred to community services', 'Referred to community resources'), ('no engagement', 'No engagement'), ('out of county', 'Transferred to same service out of county'), ('refer clinic preadmit', 'Referred to Clinic Pre-admit'), ('refer ccs', 'Referred to CCS'), ('refer clts', 'Referred to CLTS'), ('admit_clts', 'Admitted to CLTS'), ('refer cst', 'Referred to CST'), ('refer crisis', 'Referred to Crisis'), ('aoda_cm', 'AODA Case Management'), ('not eligible', 'No longer eligible '), ('incarcerated', 'Incarcerated'), ('current enroll', 'Currently enrolled in another service/program that will f/u'), ('refer county', 'Referred to county of residence'), ('death', 'Client death'), ('other', 'Other')], max_length=255, null=True, verbose_name='2nd closing reason'),
        ),
        migrations.AlterField(
            model_name='emrepisode',
            name='closing_reason3',
            field=models.CharField(blank=True, choices=[('moved', 'Moved out of county'), ('declined', 'Declined services'), ('aged out', 'Aged out of services'), ('ltc', 'Transitioned to LTC'), ('county discharge', 'County prompted discharge'), ('success discharge', 'Successfully discharged'), ('referred to community services', 'Referred to community resources'), ('no engagement', 'No engagement'), ('out of county', 'Transferred to same service out of county'), ('refer clinic preadmit', 'Referred to Clinic Pre-admit'), ('refer ccs', 'Referred to CCS'), ('refer clts', 'Referred to CLTS'), ('admit_clts', 'Admitted to CLTS'), ('refer cst', 'Referred to CST'), ('refer crisis', 'Referred to Crisis'), ('aoda_cm', 'AODA Case Management'), ('not eligible', 'No longer eligible '), ('incarcerated', 'Incarcerated'), ('current enroll', 'Currently enrolled in another service/program that will f/u'), ('refer county', 'Referred to county of residence'), ('death', 'Client death'), ('other', 'Other')], max_length=255, null=True, verbose_name='3rd closing reason'),
        ),
        migrations.AlterField(
            model_name='emrepisode',
            name='episode_type',
            field=models.CharField(choices=[('clinic_preadmit', 'Clinic Pre-admit'), ('clts', 'CLTS'), ('clts_preadmit', 'CLTS Pre-admit'), ('crisis', 'Crisis'), ('cst', 'CST'), ('randi', 'R & I'), ('tcm', 'TCM')], max_length=255),
        ),
    ]
