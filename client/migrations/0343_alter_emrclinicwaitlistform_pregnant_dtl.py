"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.10 on 2023-02-15 14:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0342_alter_emrepisode_episode_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emrclinicwaitlistform',
            name='pregnant_dtl',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Pregnancy details'),
        ),
    ]
