"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.20 on 2024-12-19 22:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0655_emrcstppsmhdischargeform_end_date'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='emrcstppsmhdischargeform',
            name='cst_living_type',
        ),
        migrations.AlterField(
            model_name='emrcstppsmhdischargeform',
            name='cjs_jail',
            field=models.BooleanField(default=False, verbose_name='Jailed/imprisoned'),
        ),
    ]
