"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Generated by Django 3.2.20 on 2024-08-09 13:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0598_alter_emrform_place_of_service'),
    ]

    operations = [
        migrations.AlterField(
            model_name='casenote',
            name='target_group',
            field=models.CharField(blank=True, choices=[('1', '1 - Developmental disability'), ('4', '4 - Alcohol abuse'), ('5', '5 - Drug abuse'), ('6', '6 - Delinquent and Status Offenders'), ('7', '7 - SOR Grant Participant'), ('8', '8 - MAT in Jails Participant'), ('17', '17 - Intoxicated driver'), ('18', '18 - Alcohol and other drug abuse'), ('31', '31 - Mental health (DSS use only)'), ('57', '57 - Physical and sensory disability'), ('58', '58 - Adults and elderly'), ('61', '61 - Abused and Neglected Children'), ('64', '64 - Children and family'), ('72', '72 - Family member / other of DD client'), ('74', '74 - Family member / other of AODA client'), ('75', '75 - Family member / other of mental health client'), ('76', '76 - Family member / other of P and SD client'), ('77', '77 - Family member / other of adults and elderly client')], max_length=2, null=True),
        ),
    ]
