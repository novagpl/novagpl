"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


def get_state_forms(instance):
    forms = []
    if not instance:
        return forms

    forms.append([
        "/client/form/f01427?client={0}".format(instance.id),
        "F-01427",
        "Birth to 3 Invitation to EI Team Eligibility Determination and IFSP Meeting",
    ])

    forms.append([
        "/client/form/f22550?client={0}".format(instance.id),
        "F-22550",
        "Birth to 3 Program Parental Cost Share",
    ])

    forms.append([
        "/client/form/f00632?client={0}".format(instance.id),
        "F-00632",
        "Birth to 3 Program System of Payments - Consent to Access Insurance and Authorization to Release Information",
    ])

    forms.append([
        "/client/form/f00043?client={0}".format(instance.id),
        "F-00043",
        "Communication to Local Educational Agency Regarding Child Referral",
    ])

    forms.append([
        "/client/form/f21336?client={0}".format(instance.id),
        "F-21336",
        "Consent for Exchange of Information with Local Educational Agency",
    ])

    forms.append([
        "/client/form/f00634?client={0}".format(instance.id),
        "F-00634",
        "County Birth to 3 Program Annual Notification of Parental Rights Regarding Records",
    ])

    forms.append([
        "/client/form/f00989?client={0}".format(instance.id),
        "F-00989",
        "Individualized Family Service Plan (IFSP)",
    ])

    forms.append([
        "/client/form/f00633?client={0}".format(instance.id),
        "F-00633",
        "Notice and Consent for Screening",
    ])

    forms.append([
        "/client/form/f00315c?client={0}".format(instance.id),
        "F-00315C",
        "Prior Notice and Consent for Evaluation - Birth to 3",
    ])

    forms.append([
        "/client/form/f00634b?client={0}".format(instance.id),
        "F-00634B",
        "Records Access Log, Birth to 3 Program",
    ])

    forms.append([
        "{% static 'forms/p23165a.pdf' %}",
        "P-23165A",
        "Birth to 3 - An Introduction to Parental Cost Share",
    ])

    forms.append([
        "{% static 'forms/p21106.pdf' %}",
        "P-21106",
        "Birth to 3 Program - Parent and Child Rights",
    ])



    return forms
