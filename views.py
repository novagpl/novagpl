"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


import traceback
import sys
from annoying.functions import get_object_or_None
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template.response import TemplateResponse
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from utils import get_client_ip, view_audit, notify
from django.core.mail import send_mail
from django.conf import settings
from django.utils import timezone
from django.contrib.sessions.models import Session


# TODO FIX!
from config.models import *  





"""
def ajax_get_session_time(request):
    # Returns the number of seconds until the session cookie expires
    if request.user.is_authenticated:
        import pdb; pdb.set_trace()

    else:
        return HttpResponse("0")
"""



def script_view(request):
    return TemplateResponse(request, "script.js", content_type='text/javascript')



def testnotify(request, message, user_id=None):
    # Sends a test notification to a user. This should only work for STAFF
    # users!!!
    if request.user.is_staff is False:
        raise PermissionDenied

    rcvr = get_object_or_None(User, id=user_id)


    notify(rcvr, message)


    return HttpResponse("OK")




def handler403(request, exception):
    #import pdb; pdb.set_trace()
    userconfig = None
    try:
        userconfig = request.user.userconfig,
    except AttributeError:
        userconfig = "User not logged in"

    html = "Time: {}\n URL: {}\nUser: {}\nPublic IP: {}\n".format(
        timezone.now(),
        request.build_absolute_uri(),
        userconfig,
        get_client_ip(request),
    )


    try:
        send_mail(
            subject=f"403 incident for {request.user.userconfig} {request.user}",
            html_message=html,
            message=html,
            recipient_list=settings.RECEIVER_403_EMAILS,
            from_email=settings.SENDER_403_EMAIL,
        )
    except Exception as e:
        print(f"Failed to send email\n{e}")



    return render(request, "403.html", {
        "exception": exception,
    }, status=403)


def handler404(request, exception):
    return render(request, "404.html", {
        "exception": exception,
    }, status=404)


def handler500(request):    
    ex_type, value, tb = sys.exc_info()

    userconfig = None
    try:
        userconfig = request.user.userconfig
    except AttributeError:
        userconfig = "User not logged in"

    try:
        user = request.user
    except AttributeError:
        user = "Unknown user"



    html = "URL: {}\nUser: {}\nUserConfig: {}\nPublic IP: {}\n\n{}".format(
        request.build_absolute_uri(),
        str(user),
        str(userconfig),
        get_client_ip(request),
        traceback.format_exc(),
    )

    # Used to return a more detailed message to the user.
    do_send_mail = True
    msg = ""
    title = "500 error"


    if ex_type == models.deletion.ProtectedError:
        title = "Cannot Delete Record"
        msg = "This record cannot be deleted because there are other records referencing it."
        # TODO provide a list and links of the items that are being referenced
        # this will probably require code for each model type, so probably only
        # start with transactions and emr forms
        #msg += f"<br/><br/>{ex_type.__name__}: {value}"
        do_send_mail = False

    elif "not implemented" in str(value):
        title = "Not Implemented"
        msg = "This feature has not yet been implemented. Please contact support for further information."
        do_send_mail = False

    elif value and value.args and "Could not find a rate for" in value.args[0]:
        title = "Missing Rate"
        msg = str(value.args[0])

        if request.user.userconfig.user_type == "1":
            msg += "<br><a href='/accounting/rate/'>Click here to go to the rates in accounting.</a><br><br>"
        else:
            msg += "<br>You may need to contact the Business Services department to have a rate set up for this episode type and year.<br><br>"



    if request.user.is_staff:
        msg += html.replace("\n", "<br>").replace("    ", "&nbsp;&nbsp;&nbsp;&nbsp;")




    if do_send_mail:
        try:
            send_mail(
                subject="{}: {}".format(ex_type.__name__, value),
                html_message=html,
                message=html.replace("<br>", "\n"),
                recipient_list=settings.RECEIVER_ERROR_EMAILS,
                from_email=settings.SENDER_ERROR_EMAIL,
            )
        except Exception as e:
            print(f"Failed to send email\n{e}")


    return render(request, "500.html", {
        "title": title,
        "msg": msg,
    }, status=500)




def test500(request):
    #return "oops"
    raise Exception("WOAH (this is a test error!)")


def loginSort(request):
    #user has logged in. Depending on UserType, send them to the correct page
    if str(request.user) == "AnonymousUser":
        logout(request)
        return HttpResponseRedirect("/login/")


    uc = UserConfig.objects.filter(user=request.user).first()
    failmsg = "User not configured. Please contact an admin.<br><a href='/login/'>Go to login</a>"
    if uc:
        if uc.user_type is None or uc.user_type == '0':
            return HttpResponse(failmsg)
        else:
            if request.GET.get('next') and "download_as" not in request.GET.get("next"):
                #redirect
                return HttpResponseRedirect("/{}".format(request.GET.get('next')))

            view_audit(request, "AUTH")

            #redirect 1=Admin, 2=Caseworker, 3=Financial, 4=Auditor
            # If the user has a login url in their profile, send them
            # there, otherwise send based on their user type
            if uc.login_url:
                return HttpResponseRedirect(uc.login_url)
            if uc.user_type == '1':
                return HttpResponseRedirect("/config/changelog/")
            if uc.user_type == '2':
                return HttpResponseRedirect("/client/caseworker_login/")
            if uc.user_type == '3':
                return HttpResponseRedirect("/accounting/transaction/")
            if uc.user_type == '4':
                return HttpResponseRedirect("/client/all/")

            #if nothing else, failmsg
            return HttpResponse(failmsg)
    else:
        return HttpResponse(failmsg)

    return HttpResponse("OK")




































































def thanks(request):
    people = (
        "Erin Knotek",
        "Olivia Knotek",
        "Fargo Knotek",
        "Chris Hovell",
        "Dallas Adams",
        "Carly Keller",
        "Heather Holcomb",
        "Jess Stinson",
    )

    return HttpResponse("<br>".join(sorted(people)))
