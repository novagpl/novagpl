"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


"""
    This is a list of reports, their types, and URLS. This list is used to generate
    the report list views and is also used to create ReportView objects

"""

from collections import namedtuple
from django.utils import timezone



reports = []



# The idea is that reports have at least 2 types. Currently there are 2,
# Financial and Program. Program reports refer to EMR programs like CLTS,
# CST, etc.. There's a good chance there will be more report types in the
# future as we continue to develop more for the users
FINANCIAL = "financial"
PROGRAM = "program"

Report = namedtuple("Report", ["url", "name", "report_type"])


# Make sure these are alphabetical
reports.append(Report("ar_ap_county", "AR/AP County", FINANCIAL))
reports.append(Report("budget", "Budget", FINANCIAL))
reports.append(Report("client_statement", "Client Statement", FINANCIAL))
reports.append(Report("contract_report", "Contract", FINANCIAL))
reports.append(Report("dcs_942", "DCS 942", FINANCIAL))
reports.append(Report("doc_search", "Document Search", FINANCIAL))
reports.append(Report("funding_profile", "Funding Profile", FINANCIAL))
reports.append(Report("transaction", "Transaction", FINANCIAL))
reports.append(Report("cc_report/watcher", "Transaction - Completed", FINANCIAL))
reports.append(Report("trial_balance", "Trial Balance", FINANCIAL))
# Users say they don't need this one so keep it hidden for now...
#reports.append(Report("account_transaction", "Account Transaction", FINANCIAL))


# Make sure these are alphabetical
reports.append(Report("admission_report", "Admission", PROGRAM))
reports.append(Report("client_list", "Client", PROGRAM))
reports.append(Report("clinic_preadmit_report", "Clinic Pre-Admit", PROGRAM))
reports.append(Report("crisis_data_report", "Crisis Data", PROGRAM))
reports.append(Report("current_roster_report", "Current Roster", PROGRAM))
reports.append(Report("discharge_report", "Discharge", PROGRAM))
reports.append(Report("emr_case_note_report", "Case Note", PROGRAM))
reports.append(Report("emr_case_note_report_my_open", "Case Note - My Open Notes", PROGRAM))
reports.append(Report("emr_case_note_time", "Case Note Time", PROGRAM))
reports.append(Report("time_to_note_report", "Time to Note", PROGRAM))
reports.append(Report("length_of_service_report", "Length of Service", PROGRAM))
reports.append(Report("pps_crisis_report", "PPS Crisis Report", PROGRAM))
reports.append(Report("pps_cst_report", "PPS CST Report", PROGRAM))
reports.append(Report("pps_tcm_report", "PPS TCM Report", PROGRAM))
reports.append(Report("program_billing_report", "Program Billing", PROGRAM))
reports.append(Report("randi_external_referral_report", "R & I External Referral", PROGRAM))
reports.append(Report("randi_internal_referral_report", "R & I Internal Referral", PROGRAM))
reports.append(Report("case_notes", "(Legacy) B-3 Case Notes", PROGRAM))
reports.append(Report("casenote_time", "(Legacy) B-3 Case Note Time", PROGRAM))
#reports.append(Report("doc_report", "Documents", PROGRAM))



def match_from_path(path):
    # Just loop through the reports and find the one matching the path
    # provided, return None if nothing found. This is an easy way to
    # use the report path to grab one of these full report objects for 
    # updating the ReportView records for the user

    # clean up the path so we can get an exact match to the report url
    if path.endswith("/"):
        path = path[:-1]

    path = path.split("/")[-1]

    found = [x for x in reports if x.url == path]
    if len(found) > 1:
        raise Exception(f"More than 1 report found for {path}")
    elif len(found) == 0:
        # There are some things in the report path that we don't need to track
        # instead of raising an error, just return none
        return None


    return found[0]



