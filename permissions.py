"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


"""
    Permissions Logic Module
    The idea is that you can call this from anywhere in the app and 
    raise a permissions error. 

    Permissions can be implemented bit-by-bit when the users need them.
"""


import inspect

from django.core.exceptions import PermissionDenied
from django.utils import timezone

import config.models


def check_perm(request):
    ''' 
        Main function to check permissions
    '''
    ok = True

    # The caller is the name of the function that's triggering this check
    # permissions request. This can be used to decide what to do
    caller = inspect.currentframe().f_back.f_code.co_name

    if check_view_only(request, caller) is False:
        ok = False


    if not ok:
        raise PermissionDenied


def check_view_only(request, caller):
    """ If the user is restricted to view_only, we do not want them to be able
        make any changes. Any calls from save_audit or delete_audit shoudl
        return a permissions error. The exception being if the user is changing
        their own preferences.
        Return True to indicate the user has access.
    """

    # If the users doesn't have view only set and the user is one of the
    # user_types that is allowed to edit, then we should be good.
    # Auditors have the user_type of '4' and they should never be able to
    # make changes
    if request.user.userconfig.view_only is False and request.user.userconfig.user_type in ["1", "2", "3"]:
        return True

    # If a save or delete isn't being called then we don't need to check
    if caller not in ["save_audit", "delete_audit"]:
        return True

    # user is changing thier own data
    if request.path == "/config/user_config/":
        return True

    # User tried to updated something they don't have access to
    warn(request)

    return False




def warn(request, action="WARN"):
    ''' 
        Saves a warning record in the audit
    '''
    audit = config.models.AuditTrail()

    uc = request.user.userconfig

    audit.date_time = timezone.now()
    if uc is not None:
        audit.user_name = "%s (%s) | %s (%s)" % (str(request.user), str(request.user.id), str(uc.full_name), str(uc.id))
    else:
        audit.user_name = "Not logged in"
    audit.action_type = action
    audit.raw_uri = request.build_absolute_uri()
    audit.ip_address = get_client_ip(request)
    audit.generate_signature(save=True)

    audit.save()



def get_client_ip(request):
    x_forwarded_for = request.headers.get('x-forwarded-for')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip














