"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2025 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



@get_logged_in
def MODEL_edit(request, OBJECT_ID=None):
    if OBJECT_ID:
        instance = get_object_or_404(OBJECT, id=OBJECT_ID)
        title = "OBJECT"
    else:
        instance = None
        title = "New OBJECT"


    if request.method == "GET":
        form = FORM_NAME(instance=instance)

        popup = ""
        if request.GET.get("popup") and request.GET.get("popup") == "true":
            popup = "popup"

        #form = set_autocomplete(instance, form, "FIELD_NAME")


        return render(request, "APP/TEMPLATE_edit.html", {
            "form": form,
            "title": title,
            "popup": popup,
            "instance": instance,
        })


    elif request.method == "POST":
        if request.POST.get("delete"):
            delete_audit(request, instance)
            return delete_confirm(request, instance, f"/APP/MODEL_URL")

        form = FORM_NAME(request.POST, instance=instance)
        if form.is_valid():
            return save_confirm(request, save_audit(request, form))
        else:
            return format_errors(request, form=form)